/* Created on Oct 19, 2005
 * 
 * 
 * This class is a quantification plugin based on Magic Tool software.
 * http://www.bio.davidson.edu/projects/magic/magic.html
 * This quantification plugin is working only for two channels images.
 * This quantification is based on a 6 pixels radius fixed circle for each rectangular spot region.
 * Results displayed into the application are : "FG Total Red" for the foreground red signal
 * "FG Total Green" for the foreground green signal and "Green / Red", the ratio green/red.
 * Notice that in this plugin other results are computed and available (but not displayed)(see GeneData class).
 * We can easily modify parameters of the radius.
 * We can also choose an other "Magic Tool quantification" (adaptative circle or seeded region).
 * An improvement could be the integration of the "Magic Tool" display panel which give users the choice of parameters.
 *   
 * Note: this plugin needs classes from package magictool.image.
 * You need to put this package into the plugins directory.	
 * 
 * @version 2005/10/20
 * @author rcathelin
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * @see magictool.image.GeneData;
 * 
 */

import ij.ImageStack;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class STagSpotPlugin extends SQuantifPlugin {
	
	String[] columnsNames = new String[1];//array of columns created by the quantification
	int[] columnsTypes = new int[1];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	//public static int id = -1;
	
	public STagSpotPlugin() {
		
		super("Add a tag to spots", null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		imagesMin = 1;
		imagesMax = 3;
		//declaration of columns of the plugin
		
		//name of quantification columns
		columnsNames[0]= "Tag";
		//type of quantification results
		columnsTypes[0] = TColumn.TYPE_INTEGER;
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		new Thread(this).start();
		
	}
	
	/**
	 * run of the quantification 
	 */
	public void run() {				
		
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages();
		
		Vector spots = getSelectedSpots();
		//Vector spots = (Vector) alignment.getGridModel().getSpots();
		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//addition of spots parameters
		for (int i=0;i<columnsNames.length;i++)		{
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}
		
		TSpot currentSpot;
		
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		
		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i);//current spot 
			//we search the number of pixels into height and width of the rectangle representing the spot
			currentSpot.addParameter(columnsNames[0], new Integer(1), true);
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
			}
		}
		
		System.out.println("Tagging done!");
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
	}
	
}