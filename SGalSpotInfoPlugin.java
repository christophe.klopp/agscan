/*
 * Created on 11th July 2008
 * The SGalSpotInfoPlugin allows to add to the alignment tab SampleId name from the .gal file.
 * 
 * 
 * @version 2008/07/11
 * @author Christophe Klopp
 * @see TPlugin
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.table.DefaultTableColumnModel;

import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.ioxml.TDefaultFileChooser;
import agscan.menu.action.TAction;
import agscan.plugins.TPlugin;

//TODO refelexion about methods called by this plugin: in fact it creates a column like Quantifications plugins
// For the moment, these methods have been copied into this class but they should probably be more general? No?
public class SGalSpotInfoPlugin extends TPlugin {
	private String columnName = "SampleId";
	private int columnType = TColumn.TYPE_TEXT;
	private boolean isAlignment = false;//true if the current element is an alignment, false if it's a grid 
	private int sampleID_columnIndex = 3;// by default, the .gal s' column we need is the 3rd.  
	
	public SGalSpotInfoPlugin() {
		//TODO externalize
		super("add Gal SampleId",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.GRID_TYPE+TAction.ALIGNMENT_TYPE); // sets when the menu is active 		
	}
	
	public void exec() {
		isAlignment = alignmentOrGrid();// test if current is alignment or grid
		File file = askGalFile();
		if (file == null) {
			//selected area is empty!
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.ERROR_DIALOG, "No file choosen");//TODO externalize
			TEventHandler.handleMessage(event);	
		}
		else {
			//TODO see if it's possible to take this column at the left of the grid
			//TODO getCurrentAlignment is copied from quantif plugin, think about an other possibility
			TAlignment alignment = getCurrentAlignment();
			TGrid grid = getCurrentGrid();
			//we get all spots in order to add them the name info
			Vector spots;
			String[][][] galInfosTab;
			if (isAlignment) {
				spots = (Vector) alignment.getGridModel().getSpots();
				//2006/03/06 grid infos
				int blocksX = alignment.getGridModel().getConfig().getLev2NbCols();
				int blocksY = alignment.getGridModel().getConfig().getLev2NbRows();
				int blocks = blocksX*blocksY;
				int spotsX = alignment.getGridModel().getConfig().getLev1NbCols();
				int spotsY = alignment.getGridModel().getConfig().getLev1NbRows();
				galInfosTab = parseGalInfoTab(blocks,spotsX,spotsY,file,sampleID_columnIndex);
				
			}
			else{
				spots = (Vector) grid.getGridModel().getSpots();
				//2006/03/06 grid infos
				int blocksX = grid.getGridModel().getConfig().getLev2NbCols();
				int blocksY = grid.getGridModel().getConfig().getLev2NbRows();
				int blocks = blocksX*blocksY;
				int spotsX = grid.getGridModel().getConfig().getLev1NbCols();
				int spotsY = grid.getGridModel().getConfig().getLev1NbRows();
				galInfosTab = parseGalInfoTab(blocks,spotsX,spotsY,file,sampleID_columnIndex);	
			}
			
			//.gal file parsing
			// this parsing method is only called in order to make test on data size...
			// that's the parseGalInfoTab that contains SampleIds
			Vector galInfos = parseGalInfo(file,sampleID_columnIndex);
			
			if (galInfos.size()==0){
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
						TDialogManager.ERROR_DIALOG, "No information retrieved!");//TODO externalize
				TEventHandler.handleMessage(event);	
			}
			else if (galInfos.size()!=spots.size()){
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
						TDialogManager.ERROR_DIALOG, "the file does not match ("+galInfos.size()+" information for "+spots.size()+ " spots)!");//TODO externalize
				TEventHandler.handleMessage(event);	
			}
			else{
				System.out.println("spots.size()= "+spots.size());
				System.out.println("galInfos.size()= "+galInfos.size());
				
				//	addDefaultParamSpot (come from) is copied from quantif plugin, see if normal to copy it irectly here
				addDefaultParamSpot(columnType,columnName);
				//we add a modif to the current model in order to ask for save if element is closed
				if (isAlignment) alignment.getGridModel().addModif(1);
				else grid.getGridModel().addModif(1);
				TSpot currentSpot;
				
				for (int i = 0; i < spots.size(); i++){
					currentSpot =(TSpot) spots.elementAt(i);//current spot
					
					int blockCol = 1 + (int)((currentSpot.getRow()-1)/currentSpot.getParent().getNbRowsInSpots());//the cast takes only the entire part of the double...
					int blockRow = 1 + (int)((currentSpot.getCol()-1)/currentSpot.getParent().getNbColumnsInSpots());
					int block = blockCol + 	(blockRow-1);	// attention FAUX!!!
					currentSpot.addParameter(columnName, galInfosTab[block][currentSpot.getLocalColumn()][currentSpot.getLocalRow()], true);
					
				}
			}		
		}
	}
	
	
	private File askGalFile() {
		File file = null;
		//TODO externalize
		TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Choix du fichier Gal associ� � cette exp�rience");
		//chooser.setCurrentDirectory(new File(AGScan.prop.getProperty("alignmentsPath")));
		chooser.addChoosableFileFilter(new ImageFilter("gal", "GAL File (*.gal)"));
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
			//TODO save or not the path?
			//AGScan.prop.setProperty("alignmentsPath",chooser.getCurrentDirectory().getAbsolutePath());
		}		
		return file;
	}
	
	/*
	 * This method allows to get values from the selected column of the Gal info file
	 * Two conditions for this parsing:
	 * - lines of interest are placed after the line containing only "Block\t"
	 * - the separator is the comma
	 * example: part of a Gal file:
	 * ...
	 * Block   Row     Column  ID      Name
	 * 1       1       1       AA406467        753234
	 * 1       1       2       AA406467        753234
	 * 1       1       3       H17047  50794
	 * 1       1       4       H17047  50794
	 * 1       1       5       W16724  302190
	 * 1       1       6       W16724  302190
	 * ...
	 *  
	 */
	private Vector parseGalInfo(File GalFile,int index){
		Vector infos = new Vector();
		boolean ok = false;// become true when the "mapping" line is found		
		try{
			BufferedReader in = new BufferedReader(new FileReader(GalFile));
			String line;
			String[] lineParsing = null;
				
			Vector v = new Vector();
		
			while((line=in.readLine())!=null){
				v.add(line);
			}
			
			for (Enumeration enu = v.elements(); enu.hasMoreElements(); ) {
			//while((line=in.readLine())!=null){
				line = (String) enu.nextElement();
				if (ok){
					line = line.replaceAll("\"","");
					line = line.replaceAll("\t",",");
					lineParsing = line.split(",");
					//special condition: if the column info is empty, we take the precedent info...
					if (lineParsing[index].length()==0)	infos.add(lineParsing[index-1]);
					else infos.add(lineParsing[index]);
				}else{
					//System.out.println(" ok = "+ok+" "+line.startsWith("Block\t"));
					if (line.startsWith("Block\t")) ok = true;
				}
			}
			in.close();
		} 
		catch(Exception e){}
		if (!ok) {
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.ERROR_DIALOG, "AGScan was not able to retrieve the gene ID from the GAL file!");//TODO externalize
			TEventHandler.handleMessage(event);	
		}
		return infos;
	}
	
	
	
	
	/**
	 * 
	 * 2006/03/06
	 * Special method that gets the Gal info and put them into a four dimensions tab
	 * The four dimanesions are the (X,Y) from the block and the (X,Y) from the spot (into the block)
	 * @param blocksX - number of blocks in column 
	 * @param blocksY - number of blocks in row
	 * @param spotsX - number of blocks in column (into a block)
	 * @param spotsY - number of blocks in column (into a row)
	 * @param file - the Gal file
	 * @param index - the index of the SampleId column into the Gal file
	 * @return the four dimension tab containing SampleIds
	 */
	private String[][][] parseGalInfoTab(int blocks,int spotsX,int spotsY,File file,int index){
		
		String[][][] GalInfosTab = new String[blocks+1][spotsX+1][spotsY+1];
		Vector infos = new Vector();
		boolean ok = false;// become true when the "mapping" line is found	
		//System.out.println("File = "+file+" "+blocks+" "+spotsX+" "+spotsY+" "+index);
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;
			String start;
			String[] lineParsing = null;
			
			int blocksVal = 0;
			int spotsXval = 0;
			int spotsYval = 0;
			Vector v = new Vector();
			
			
			while((line=in.readLine())!=null){
				v.add(line);
			}
			
			for (Enumeration enu = v.elements(); enu.hasMoreElements(); ) {
			//while((line=in.readLine())!=null){
				//start = line.substring(0,5);
				line = (String) enu.nextElement();
				//System.out.println(line);
				if (ok){
					//added 2006/01/03 in order to delete every " of data
					line = line.replaceAll("\"","");
					line = line.replaceAll("\t",",");
					lineParsing = line.split(",");
					blocksVal = Integer.parseInt(lineParsing[0]);
					spotsXval = Integer.parseInt(lineParsing[1]);
					spotsYval = Integer.parseInt(lineParsing[2]);	
					//System.out.println("<<"+index+">>");

					//					GalInfosTab[blocksXval][blocksYval][spotsXval][spotsYval]=lineParsing[index];
					if (lineParsing[index].length()==0){
						GalInfosTab[blocksVal][spotsXval][spotsYval]=lineParsing[index-1];
					}else{
						GalInfosTab[blocksVal][spotsXval][spotsYval]=lineParsing[index];
					}
					//System.out.println("arg2="+blocksVal);
					//System.out.println("arg3="+spotsXval);
					//System.out.println("arg4="+spotsYval);					
					infos.add(lineParsing[index]);
				}else{
					//System.out.println(" ok = "+ok+" "+line.startsWith("Block\t"));
					if (line.startsWith("Block\t")) ok = true;
				}
				//if (line.substring(0,6).equals("Block\t")) ok = true;
				//System.out.println(" ok = "+ok+" "+line.substring(0,6));
				//if ok is true next lines are ok for parsing!
			}
			in.close();
		} 
		catch(Exception e){}
		if (!ok) {
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.ERROR_DIALOG, "AGScan was not able to retrieve the gene ID from the GAL file!");//TODO externalize
			TEventHandler.handleMessage(event);	
		}
		return GalInfosTab;//infos;
	}
	
	
	
	
	public void actionPerformed(ActionEvent arg0) {
		this.exec();
	}
	/**
	 * 2005/12/20 - modified in order to add columns for TGrid too
	 * add a field to a spot and create the column associated in the table
	 * @param type type of the parameter:TColumn.TYPE_INTEGER, TColumn.TYPE_REAL,TColumn.TYPE_REAL, TColumn.TYPE_BOOLEAN
	 * @param name the name of the parameter ( here we give the same for the column ot the table)
	 */
	protected void addDefaultParamSpot(int type,String name){
		//CONFIGURATION
		// we don't know if the current element is a grid or an alignment
		TGrid grid = null;
		TAlignment alignment = null;
		if (isAlignment) alignment = getCurrentAlignment();
		else grid = getCurrentGrid();
		
		Vector params = new Vector();// params is the vector of the column configuration
		TEvent ev = null;
		
		//	get the columns model containing into the table
		DefaultTableColumnModel columns;
		if (isAlignment) columns = alignment.getGridModel().getConfig().getColumns();
		else columns = grid.getGridModel().getConfig().getColumns();	
		TColumn col = null;
		Enumeration enume = columns.getColumns();//the enum has got all the table columns
		boolean nameExist = false;		
		while (enume.hasMoreElements()) {
			col = (TColumn) enume.nextElement();
			//research of the model column corresponding to this algo, for example COLUMN_NAME = "Qtf. Image/Const."
			if (col.getSpotKey().equals(name)) nameExist = true; 
		}
		
		if (nameExist) return; 
		
		//COLUMNS CONFIGURATION FIELDS
		Object defaultValue = null;//default value of the field
		Object infValue = null; //allows to colorize values inferior of this value
		Object betweenValue1 = null; //min limit to allow to colorize values between two values
		Object betweenValue2 = null;//max limit to allow to colorize values between two values
		Object supValue = null;   //allows to colorize values superior of this value    
		Boolean betweenActive = new Boolean(false); // active or not "between colorization"
		Boolean infActive = new Boolean(false);// active or not "inferior colorization"
		Boolean supActive = new Boolean(false);// active or not "superior colorization"
		Color betweenColor = Color.black;// color used for "between colorization"
		Color infColor = Color.black;// color used for "inferior colorization"
		Color supColor = Color.black;// color used for "between colorization"
		
		// init values declared before according to the type
		switch (type) {
		case TColumn.TYPE_INTEGER:
			defaultValue = new Integer(-1);
		infValue = new Integer(0);
		betweenValue1 = new Integer(0);
		betweenValue2 = new Integer(0);
		supValue = new Integer(0);    
		betweenActive = new Boolean(false); 
		infActive = new Boolean(false);
		supActive = new Boolean(false);
		betweenColor = Color.black;// color used for "between colorization"
		infColor = Color.black;// color used for "inferior colorization"
		supColor = Color.black;// color used for "between colorization"		
		break;
		case TColumn.TYPE_REAL:	
			defaultValue = new Double(-1);
		infValue = new Double(0);
		betweenValue1 = new Double(0);
		betweenValue2 = new Double(0);
		supValue = new Double(0);    
		betweenActive = new Boolean(false); 
		infActive = new Boolean(false);
		supActive = new Boolean(false);
		betweenColor = Color.black;// color used for "between colorization"
		infColor = Color.black;// color used for "inferior colorization"
		supColor = Color.black;// color used for "between colorization"		
		break;
		case TColumn.TYPE_TEXT:
			defaultValue = new String("...");
		break;
		case TColumn.TYPE_BOOLEAN:
			defaultValue = new Boolean(false);
		break;	
		}
		
		switch (type) {
		case TColumn.TYPE_INTEGER:
		params.addElement(name);//name displayed of the column
		params.addElement(name);//real name of the column
		params.addElement(defaultValue);//default value of the field
		params.addElement(infActive);
		params.addElement(infColor);
		params.addElement(infValue);
		params.addElement(betweenActive);
		params.addElement(betweenColor);
		params.addElement(betweenValue1);
		params.addElement(betweenValue2);
		params.addElement(supActive);
		params.addElement(supColor);
		params.addElement(supValue);
		if (isAlignment) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_INTEGER), params);
		else ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, grid, new Integer(TColumn.TYPE_INTEGER), params);
		break;
		case TColumn.TYPE_REAL:		
			params.addElement(name);//name displayed of the column
		params.addElement(name);//real name of the column
		params.addElement(defaultValue);//default value of the field
		params.addElement(infActive);
		params.addElement(infColor);
		params.addElement(infValue);
		params.addElement(betweenActive);
		params.addElement(betweenColor);
		params.addElement(betweenValue1);
		params.addElement(betweenValue2);
		params.addElement(supActive);
		params.addElement(supColor);
		params.addElement(supValue);
		if (isAlignment) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_REAL), params);
		else ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, grid, new Integer(TColumn.TYPE_REAL), params);
		break;
		case TColumn.TYPE_TEXT:
			params.addElement(name);
		params.addElement(name);
		params.addElement(defaultValue);
		if (isAlignment) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_TEXT), params);
		else ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, grid, new Integer(TColumn.TYPE_TEXT), params);
		break;
		case TColumn.TYPE_BOOLEAN:
			params.addElement(name);
		params.addElement(name);
		params.addElement(defaultValue);
		if (isAlignment) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_BOOLEAN), params);
		else ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, grid, new Integer(TColumn.TYPE_BOOLEAN), params);
		break;
		}	
		//TColumn col = null;
		col = (TColumn) TEventHandler.handleMessage(ev)[0];
		col.setBooleanBetweenColor(false);
		col.setBooleanInfColor(false);
		col.setBooleanSupColor(false);
		col.setEditable(false);
		col.setSynchrone(true);
		
	}
	
	/**
	 * get the current element: an alignment or a grid
	 * @return the alignment or null if the current element isn't an alignment
	 */
	protected TAlignment getCurrentAlignment() {
		// alignement get 
		TAlignment alignment = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else System.out.println("TDataElement isn't a alignment!!!!");
		return alignment;
	}	
	
	/**
	 * get the current grid
	 * @return the grid or null if the current element isn't a grid
	 */
	protected TGrid getCurrentGrid() {		 
		TGrid grid = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TGrid) grid = (TGrid)currentElement;
		else System.out.println("TDataElement isn't a grid!!!!");
		return grid;
	}	
	
	
	/**
	 *return true if the current data element is an alignment, false else 
	 */
	private boolean alignmentOrGrid(){
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) return true;
		else return false;
		
	}	
	
}
