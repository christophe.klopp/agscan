AGScan
======

AGScan is a micro-array image processing software developed by the SIGENAE team. 
It enables load images, define grids, align a grid on an image, quantify the signal and export it to a tab-delimited text file.
These operations can also be performed in batch mode for a set of images charing the same grid.

User manual :
-------------
[Link to user manual](http://www.sigenae.org/fileadmin/Sigenae/Documentation/Using_AGScan.pdf)

Latest version running under java8 :
------------------------------------
[Link to latest version](http://genoweb.toulouse.inra.fr/~klopp/AGScan/AGScan_18112019.zip)

Image example file :
--------------------
[Link to image example file](http://genoweb.toulouse.inra.fr/~klopp/AGScan/2_8x22_1.tif)

Grid example file:
------------------
[Link to grid example file](http://genoweb.toulouse.inra.fr/~klopp/AGScan/newGrid_8x2_5x5.grd)




