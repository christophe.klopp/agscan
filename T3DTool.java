

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.media.j3d.BranchGroup;
import javax.swing.JDialog;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;
import agscan.menu.action.TAction;
import agscan.plugins.TPlugin;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class T3DTool extends TPlugin {
	 protected JDialog dialog;//added 2005/12/14
	 protected static BranchGroup branchGroup;
	 public static Object[] params_3D;//moved 2005/12/15
	public T3DTool() {
		super("Vue 3D",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets the menu always active 
		
	}
	  
  public void exec() {
  	
  	//moved from TModelImpl
  	 branchGroup = null;
  	params_3D = new Object[19];
    params_3D[0] = new Integer(10);//x
    params_3D[1] = new Integer(10);//y
    params_3D[2] = new Integer(100);//w
    params_3D[3] = new Integer(100);//h
    params_3D[4] = new Boolean(true);//image, cad? imageCheckBox.isSelected()
    params_3D[5] = new Boolean(false);//fitCheckBox.isSelected()
    params_3D[6] = new Boolean(false);//backgroundCheckBox.isSelected()
    params_3D[7] = new Boolean(true);//transparence?imageSurfaceCheckBox.isSelected()
    params_3D[8] = new Boolean(true);//fitSurfaceCheckBox.isSelected()
    params_3D[9] = new Boolean(true);//backgroundSurfaceCheckBox.isSelected()
    params_3D[10] = new Boolean(true);//imageColorCheckBox.isSelected()
    params_3D[11] = new Boolean(false);//fitColorCheckBox.isSelected()
    params_3D[12] = new Integer(0);//imageTranspSlider.getValue()
    params_3D[13] = new Integer(0);//fitTranspSlider.getValue()
    params_3D[14] = new Integer(0);//backgroundTranspSlider.getValue()
    params_3D[15] = new Integer(40);//zoomZSlider.getValue()
    params_3D[16] = new Integer(30);//zoomXYSlider.getValue()
    params_3D[17] = new Boolean(true);//zone3DCheckBox.isSelected
    params_3D[18] = new Boolean(false);//showSpotsCheckBox.isSelected()
  	
  	
  	
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    if (element != null) {
      System.err.println("Execution de T3DTool sur : " + element);
  ///    boolean b = ( (Boolean) element.getModel().get3DParams()[17]).booleanValue();
     /// if (b) {
     ///   ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.DEZOOM, null);
     ///   TEventHandler.handleMessage(ev);
     /// }
      dialog = TView3DDialog.getInstance(element);
      dialog.setLocation( (screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
      dialog.setModal(false);
      dialog.setAlwaysOnTop(true);
      dialog.setVisible(true);
    }
  }
  public void close() {

  }
  public void update() {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    ((TView3DDialog)dialog).init(element);
  }

/* (non-Javadoc)
 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
 * added 2005/12/14
 */
public void actionPerformed(ActionEvent arg0) {
	exec();
	// TODO Auto-generated method stub	
}
//moved 2005/12/15
public static void set3DParams(Object[] params) {
    params_3D = params;
  }
  public static Object[] get3DParams() {
    return params_3D;
  }
  public static void setBranchGroup(BranchGroup bg) {
    branchGroup = bg;
  }
  public static BranchGroup getBranchGroup() {
    return branchGroup;
  }

}
