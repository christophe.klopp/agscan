import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.element.batch.TBatch;
import agscan.data.model.batch.TBatchModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.view.graphic.TPlotsPanel;
import agscan.data.view.table.TBatchTablePanel;
import agscan.event.TEvent;
import agscan.ioxml.TReadQuantifResultsWorker;
import agscan.ioxml.TResultArray;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TPlotArrayWorker extends SwingWorker {
  private TBatch batch;
  private int[] rows;
  private File[] files;
  private String dirName;
  private boolean linear, log;

  public TPlotArrayWorker(int[] r, TBatch b, String dn, boolean linear, boolean log) {
    batch = b;
    rows = r;
    dirName = dn;
    this.linear = linear;
    this.log = log;
    files= null;
  }
  public TPlotArrayWorker(File[] files, String dn, boolean linear, boolean log) {
    batch = null;
    rows = null;
    this.files = files;
    dirName = dn;
    this.linear = linear;
    this.log = log;
  }
  public Object construct(boolean thread) {
    if (linear | log) {
      JFreeChart jfc;
      TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
      sdAlgo.initWithDefaults();
      TReadQuantifResultsWorker worker;
      TResultArray results;
      File dir = new File(dirName);
      Object[] params = new Object[7];
      params[6] = TQuantifImageConstantAlgorithm.COLUMN_NAME;
      dir.mkdir();
      String[] sks = {TQuantifImageConstantAlgorithm.COLUMN_NAME};
      LogarithmicAxis v, h;
      if (batch != null) {
        batch.setBatchRunning(true);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(true)));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_LABEL, batch, "Calcul des plots ...", Color.red));
        ((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
        TBatchModelNImages model = (TBatchModelNImages) batch.getModel();
        files = new File[rows.length];
        for (int i = 0; i < files.length; i++) {
          files[i] = new File(model.getImageName(rows[i],0));
        }
      }

      int[][] data = new int[files.length][];
      int[][] q = new int[files.length][];
      String[] n = new String[files.length];
      for (int i = 0; i < files.length; i++) {
        worker = new TReadQuantifResultsWorker(files[i], batch, sks);
        worker.setPriority(Thread.MAX_PRIORITY);
        results = (TResultArray) worker.construct(false);
        data[i] = results.getResults()[0];
        q[i] = results.getQuality();
        n[i] = files[i].getPath().substring(files[i].getPath().lastIndexOf(File.separator) + 1);
        if (batch != null)
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer((i + 1) * 100 / files.length)));
      }
      if (batch != null)
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer(0)));
      int k = 0, ntot = (files.length - 1) * files.length / 2, prev_pc = 0, pc;
      for (int i = 0; i < files.length; i++) {
        params[0] = data[i];
        params[2] = q[i];
        params[4] = n[i];
        for (int j = i + 1; j < files.length; j++) {
          params[1] = data[j];
          params[3] = q[j];
          params[5] = n[j];
          jfc = makePlot(data[i], data[j], q[i], q[j], n[i], n[j], TQuantifImageConstantAlgorithm.COLUMN_NAME);
          jfc.setTitle("");
          jfc.getXYPlot().getDomainAxis().setTickLabelsVisible(false);
          jfc.getXYPlot().getDomainAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
          jfc.getXYPlot().getRangeAxis().setTickLabelsVisible(false);
          jfc.getXYPlot().getRangeAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
          try {
            if (linear) ChartUtilities.saveChartAsPNG(new File(dirName + File.separator + n[i] + "-" + n[j] + ".png"), jfc, 150, 100);
            if (log) {
              v = new LogarithmicAxis("L " + jfc.getXYPlot().getRangeAxis().getLabel().replaceAll("L ", ""));
              v.setAllowNegativesFlag(true);
              h = new LogarithmicAxis("L " + jfc.getXYPlot().getDomainAxis().getLabel().replaceAll("L ", ""));
              h.setAllowNegativesFlag(true);
              jfc.getXYPlot().setRangeAxis(v);
              jfc.getXYPlot().setDomainAxis(h);
              jfc.getXYPlot().getDomainAxis().setTickLabelsVisible(false);
              jfc.getXYPlot().getDomainAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
              jfc.getXYPlot().getRangeAxis().setTickLabelsVisible(false);
              jfc.getXYPlot().getRangeAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
              ChartUtilities.saveChartAsPNG(new File(dirName + File.separator + n[i] + "-" + n[j] + "_LOG.png"), jfc, 150, 100);
            }
            k++;
            pc = k * 100 / ntot;
            if (pc > prev_pc) {
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer(pc)));
              prev_pc = pc;
            }
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        }
      }
      File htmlFile = new File(dirName + File.separator + "plots.html");
      try {
        BufferedWriter bw = new BufferedWriter(new FileWriter(htmlFile));
        bw.write("<HTML>");
        bw.newLine();
        bw.write("<table>");
        bw.newLine();
        for (int i = 0; i < files.length; i++) {
          bw.write("<tr>");
          bw.newLine();
          for (int j = 0; j < files.length; j++) {
            bw.write("<td>");
            if (i < j) {
              if (linear) bw.write("<img src=\"" + n[i] + "-" + n[j] + ".png\" >");
            }
            else if (i > j) {
              if (log) bw.write("<img src=\"" + n[j] + "-" + n[i] + "_LOG.png\" >");
            }
            bw.write("</td>");
            bw.newLine();
          }
          bw.write("</tr>");
          bw.newLine();
        }
        bw.write("</table>");
        bw.newLine();
        bw.write("</HTML>");
        bw.newLine();
        bw.close();
        JEditorPane jep = new JEditorPane("file:" + htmlFile.getAbsolutePath());
        jep.setEditable(false);
        TPlotsPanel pp = new TPlotsPanel(jep);
        if (batch != null) {
          batch.setShowingElement(pp);
          batch.setShowingImage(true);
        }
        else {
          JFrame frame = new JFrame();
          frame.setTitle("Matrice de plots");
          //frame.setAlwaysOnTop(true);//only with java 5.0 (removed for compatibility java 1.4.2)
          frame.getContentPane().setLayout(new BorderLayout());//used getContentPane  (for compatibilty java 1.4.2)
          frame.getContentPane().add(new JScrollPane(pp), BorderLayout.CENTER);//used getContentPane  (for compatibilty java 1.4.2)
          frame.setSize(640, 480);
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          frame.setLocation( (screenSize.width - frame.getWidth()) / 2, (screenSize.height - frame.getHeight()) / 2);
          frame.setVisible(true);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return null;
  }
  private JFreeChart makePlot(int[] v1, int[] v2, int[] vb1, int[] vb2, String n1, String n2, String sk) {
    XYSeries series_1, series_0, series_mixed;
    series_1 = new XYSeries(new Integer(0));
    series_0 = new XYSeries(new Integer(0));
    series_mixed = new XYSeries(new Integer(0));
    int q1, q2;
    for (int i = 0; i < v1.length; i++) {
      q1 = vb1[i];
      q2 = vb2[i];
      if (q1 != q2)
        series_mixed.add(v1[i], v2[i]);
      else if (q1 == 1)
        series_1.add(v1[i], v2[i]);
      else if (q1 == 0)
        series_0.add(v1[i], v2[i]);
    }
    XYSeriesCollection coll = new XYSeriesCollection(series_1);
    coll.addSeries(series_0);
    coll.addSeries(series_mixed);
    JFreeChart plot = ChartFactory.createScatterPlot(sk, n1, n2, coll, PlotOrientation.HORIZONTAL, false, false, false);
    XYDotRenderer dotRenderer = new XYDotRenderer();
    dotRenderer.setSeriesPaint(0, Color.red);
    dotRenderer.setSeriesPaint(1, Color.green);
    dotRenderer.setSeriesPaint(2, Color.orange);
    plot.getXYPlot().setRenderer(dotRenderer);
    return plot;
  }

  public void finished() {
    if (batch != null) {
      batch.setBatchRunning(false);
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, batch, Messages.getString("TPlotArrayWorker.0"), Color.blue));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
    }
    FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
}
