/* Created on Oct 19, 2005
 * 
 * 
 * This class is an example of quantification plugin 
 
 * Note: the idea here is to indicate the places where the code has to be.
 * updated or modified in order to adapt it to the need.	
 * 
 * @version 2006/10/10
 * @author cklopp
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * 
 */

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SSpotCoordinatesPlugin extends SQuantifPlugin {
	// *** size to be adjusted to the need
	
	public SSpotCoordinatesPlugin() {
		
		super("Spot coordinate analysis", null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		imagesMin = 1;
		imagesMax = 2;
		//declaration of columns of the plugin
		
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * run of the quantification 
	 */
	public void run() {				
		// infrmation about the alignment
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages(); // in the stack you have the different images 
		
		// get the selected spots 
		Vector spots = getSelectedSpots();

		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		// This part in handling the GUI before the job 
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		
		// ------ memory and processing time -----------
		// some parameter can be set for once and used after in order to be more efficient
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight(); //height of one pixel in micrometers( for example 25)
		
		// or in order not to recreate new object in the loop the object are created once and reinitialized for
		// each spot
		TSpot currentSpot;
		
		if (spots.size() > 1){
			JOptionPane.showMessageDialog(null,"More than spot selected", "Select only one spot!", JOptionPane.ERROR_MESSAGE);
		}else{
			currentSpot =(TSpot) spots.elementAt(0);
			// Init Coordinates
			
			double parentX = currentSpot.getParent().getX();
			double parentY = currentSpot.getParent().getY();
    			
			double initX = parentX+currentSpot.getInitX();
			double initY = parentY+currentSpot.getInitY();
			double X = currentSpot.getX();
			double Y = currentSpot.getY();
			double localX = parentX+currentSpot.getLocalX();
			double localY = parentY+currentSpot.getLocalY();
			
	   		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.blue, (int)((X)/pixelWidth-2), (int)((Y)/pixelHeight), (int)((X)/pixelWidth+2), (int)((Y)/pixelHeight)));
    		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.blue,(int)((X)/pixelWidth), (int)((Y)/pixelHeight-2), (int)((X)/pixelWidth), (int)((Y)/pixelHeight+2)));

	   		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.red, (int)((initX)/pixelWidth-2), (int)((initY)/pixelHeight), (int)((initX)/pixelWidth+2), (int)((initY)/pixelHeight)));
    		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.red,(int)((initX)/pixelWidth), (int)((initY)/pixelHeight-2), (int)((initX)/pixelWidth), (int)((initY)/pixelHeight+2)));

    		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.green, (int)((localX)/pixelWidth-2), (int)((localY)/pixelHeight), (int)((localX)/pixelWidth+2), (int)((localY)/pixelHeight)));
    		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
    				new TImageGraphicPanel.HotLine(Color.green,(int)((localX)/pixelWidth), (int)((localY)/pixelHeight-2), (int)((localX)/pixelWidth), (int)((localY)/pixelHeight+2)));
 
    		
    		
			String outString = "Init("+initX+","+initY+") red\n real("+X+","+Y+") blue\n Local("+localX+","+localY+") green\n Local("+parentX+","+parentY+") pink";
			JOptionPane.showMessageDialog(null, outString, "Coordinates", JOptionPane.INFORMATION_MESSAGE);
		}
		

		
		// This part in handling the GUI after the job 	
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part 
	}

}