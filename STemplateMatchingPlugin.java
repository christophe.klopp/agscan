/*
 * STemplateMatchingPlugin performs a powerfull global alignment based on the correlation of the image
 * A pattern of gaussian spot is searched.
 * Created on 20th February 2006
 * @version 2006/02/20
 * @author Remi Cathelin
 */


import ij.ImagePlus;

import java.awt.event.ActionEvent;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridConfig;
import agscan.event.TEvent;
import agscan.plugins.SAlignPlugin;

//import grid.Grid_Overlay;


public class STemplateMatchingPlugin extends SAlignPlugin{
	
	/**
	 * constructor of the class
	 */
	public STemplateMatchingPlugin () {
		super("Template Matching", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(ALIGNMENT_TYPE); 
	}
	
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void run() {
		//TODO reflechir si ca ne devrait pas etre dans le parent du plugin tout ca
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		//TAlignment alignment = (TAlignment)element;
		TAlignmentModel tim = (TAlignmentModel)element.getModel();
		TGridConfig gridConfig = tim.getGridModel().getConfig();

		inProgressStatus(true);//progress bar + lock table + grid....
					
		ImagePlus imp = tim.getImageModel().getMaxImage();
		//System.out.println(gridConfig.getLev2NbCols()+" "+gridConfig.getLev2NbRows()+" "+gridConfig.getLev1NbCols()+" "+gridConfig.getLev1NbRows()+" "+gridConfig.getSpotsHeight()+" "+gridConfig.getLev2XSpacing()+" "+gridConfig.getLev2YSpacing());
		Grid_Overlay overlay = new Grid_Overlay();
		//WARNING : (int)(gridConfig.getSpotsHeight()/tim.getPixelHeight()) => savage cast cuts the entire value!!!
		// example 346/25= 13.84 gives 13 instead of 14!!!!!
		// so use ==> Math.round
		
		overlay.setGridInfos(gridConfig.getLev2NbCols(),gridConfig.getLev2NbRows(),
				gridConfig.getLev1NbCols(),gridConfig.getLev1NbRows(),
				(int)Math.round(gridConfig.getSpotsHeight()/tim.getPixelHeight()),//TODO on donne h, pquoi pas w (pas forcement carr�e)etendre au rectangle
				(int)Math.round(gridConfig.getLev2XSpacing()/tim.getPixelWidth()),
				(int)Math.round(gridConfig.getLev2YSpacing()/tim.getPixelHeight()));
		overlay.doProcess(imp);
		
		double x= overlay.getGenStartPointx()*tim.getPixelWidth();
		double y= overlay.getGenStartPointy()*tim.getPixelHeight();
		double angle = - overlay.getAngle();//clockwise
		

		translateGrid(x,y);

		rotateSelection(angle);	

		inProgressStatus(false);//end of progress bar + unlock table + grid....
		
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		//		run();
		new Thread(this).start();// run is called in a thread of course    	
	}
}
