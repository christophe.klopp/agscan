/*
 * Created on 4th April 2008
 * The SGalToGridPlugin allows to generate a grid forom a GAL file
 * 
 * @author Alexis JULIN
 * @see SGridPlugin
 */


import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;

import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridStructureControler;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.TGridModel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.ioxml.TDefaultFileChooser;
import agscan.menu.action.TAction;
import agscan.plugins.SGridPlugin;
import agscan.data.model.grid.TGridConfig;

public class SGalToGridPlugin extends SGridPlugin {

	
	public SGalToGridPlugin() {
		//TODO externalize
		super("Generate grid from GAL file",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets when the menu is active 		
	}
	
	public void run() {
		
		
		System.out.println("Ca marche");
		String adresse;
		adresse=askGalAdresse();
		
		try{
			//recuperation des donn�es pour construire la grille
			System.out.println("etape 1 ");
			int lvl1Param[]=getLevel1Param(adresse);
			System.out.println("etape 2 ");
			int lvl2Param[]=getLevel2Param(adresse);
			System.out.println("etape 3 ");
//			
//			for(int i = 0 ; i < lvl1Param.length ; i++)
//				System.out.println(lvl1Param[i]);
////			
//			System.out.println();
//			
//			for(int i = 0 ; i < lvl2Param.length ; i++)
//				System.out.println(lvl2Param[i]);
//			
//			construction de la grille
			TGridConfig gc=new TGridConfig();
			TGrid grid=new TGrid(gc);
			
			
			//affichage de la grille
		    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, 
		    TDataManager.ADD_DATA_ELEMENT, null, grid);
		    TEventHandler.handleMessage(event);
		    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, grid.getView());
		    TEventHandler.handleMessage(event);
		    event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.NEW_GRID, grid);
		    TEventHandler.handleMessage(event);
		    
		    
		    //specification des caracteristiques de la grille
		    
		    //caract�ristiques de la grille
		    //en prend en compte le fait que la grille puisse �tre multi-niveaux ou non
		    if(lvl2Param[0]*lvl2Param[1]!=1){
		    	//nombre de colonne de la grille
		    	TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_NB_COLUMNS, null,lvl2Param[0]);
		    	TEventHandler.handleMessage(ev);
			
		    	//nombre de ligne de la grille
		    	TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_NB_ROWS, null, lvl2Param[1]);
		    	TEventHandler.handleMessage(ev2);
			
		    	//espace entre deux lignes du blocs
		    	TEvent ev3 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_ROW_SPACING, null,(double)lvl2Param[2]);
		    	TEventHandler.handleMessage(ev3);
			
		    	//espace entre deux colonnes du blocs
		    	TEvent ev4 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_COLUMN_SPACING, null,(double)lvl2Param[3]);
		    	TEventHandler.handleMessage(ev4);
		    }
			
		    //Diametre du spot
		    TEvent ev5 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_SPOT_DIAMETER, null,(double)lvl2Param[4]);
			TEventHandler.handleMessage(ev5);
		 
			//caract�ristiques d'un bloc
			
			//nombre de colonnes de spot
		    TEvent ev6 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_NB_COLUMNS, null,lvl1Param[0]);
			TEventHandler.handleMessage(ev6);
			
			//nombre de lignes de spot
		    TEvent ev7 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_NB_ROWS, null,lvl1Param[1]);
			TEventHandler.handleMessage(ev7);
			
			//espace entre deux colonnes de spots
		    TEvent ev8 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.SPOT_WIDTH, null,(double)lvl1Param[3]);
			TEventHandler.handleMessage(ev8);
			
			//espace entre deux colonnes de spots
		    TEvent ev9 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.SPOT_HEIGHT, null,(double)lvl1Param[2]);
			TEventHandler.handleMessage(ev9);
			
			//espace entre deux colonnes de spots
		    TEvent ev11 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.NB_LEVELS, null,2);
			TEventHandler.handleMessage(ev11);
			
			
			/*
			 * To clean the grid from all the column which are not generated when creating a new grid!
			 */
			TGridModel gridModel2 = new TGridModel(gc);
			TEvent ev10;
			for (int k = grid.getModel().getColumnCount()-1; k >= 10; k--){
			ev10 = new TEvent(TEventHandler.DATA_MANAGER,TGridColumnsControler.REMOVE_COLUMN, null, gridModel2.getConfig().getColumns().getColumn(k));
			TEventHandler.handleMessage(ev10);
			}
			
			
		}
		catch(Exception e){
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.MESSAGE_DIALOG,"incompatible format!" ); 
			TEventHandler.handleMessage(event);
			System.out.println("Probleme");
			
		}
		
	}
	
	
	public void actionPerformed(ActionEvent ae) {
		this.run();
	}
	
	
	
	//renvoi l'adresse absolue du fichier tam selectionn�
private String askGalAdresse() {
		String adresse="";
		//TODO externalize
		TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Choix du fichier gal associ� � cette exp�rience");
		chooser.addChoosableFileFilter(new ImageFilter("gal", "GAL File (*.gal)"));
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) 
			adresse = chooser.getSelectedFile().getAbsolutePath();
		return adresse;
}
	
//return un tableau contenant : Le nombre de spots sur une ligne
//								Le nombre de spots sur une colonne
//								L'espace entre deux lignes de spots
//								L'espace entre deux colonnes de spots
public static int[] getLevel1Param(String filename) throws Exception{

	int res[]=new int[4];
	BufferedReader br;
	StringTokenizer st,st2;
	String ligne,s,ligne2,ligne3;


	br=new BufferedReader(new FileReader(filename));
	ligne2 = br.readLine();
	ligne = ligne2.replace("\"", "");
	//on se place au debut de la description des blocs
	while(!ligne.startsWith("Block1=")){
		ligne2 = br.readLine();
		ligne = ligne2.replace("\"", "");
	}
	ligne2 = br.readLine();
	ligne3 = ligne2.replace("\"", "");
	ligne = ligne3.replace("\t", "");
	st=new StringTokenizer(ligne,"=");
	st.nextToken();
	s=st.nextToken();
	st2=new StringTokenizer(s,", ");
	/* info non pertinentes*/
	st2.nextToken();
	st2.nextToken();
	st2.nextToken();
	/*--------------------*/
	res[0]=Integer.parseInt(st2.nextToken());//nombre de spot en ligne =>nombre de colonne de spot
	res[3]=Integer.parseInt(st2.nextToken());//espace entre les colonnes de spot=largeur d'une colonne de spot
	res[1]=Integer.parseInt(st2.nextToken());//nombre de spot par colonne=>nombre de ligne de spot
	res[2]=Integer.parseInt(st2.nextToken().replace("\"",""));//espace entre les lignes=hauteur d'une ligne de spot

	br.close();



	return res;
}
	
	
	
//return un tableau contenant le nombre de ligne et le nombre de colonne de la grille
private static int[] getDimGrid(String filename)throws Exception{

	int res[]=new int[2];
	BufferedReader br;
	String ligne,s,ligne2,ligne3;
	StringTokenizer st,st2;
	int nb_bloc;
	int xOrig,yOrig;
	int nbBc=1;
	int nbBl=1;

	br=new BufferedReader(new FileReader(filename));
	ligne2 = br.readLine();
	ligne = ligne2.replace("\"", "");
	//on recupere le nbre de bloc
	while(!ligne.startsWith("BlockCount")){
		ligne2 = br.readLine();
		ligne = ligne2.replace("\"", "");
	}

	st=new StringTokenizer(ligne,"=");
	st.nextToken();
	s=st.nextToken();
	nb_bloc=Integer.parseInt(s.trim());
	ligne2 = br.readLine();
	ligne3 = ligne2.replace("\"", "");
	//on se place sur le premier bloc
	while(!ligne3.startsWith("Block1=")){
		ligne2 = br.readLine();
		ligne3 = ligne2.replace("\"", "");
	}


//	on recupere les premieres donnees
	ligne = ligne3.replace("\t", "");
	st=new StringTokenizer(ligne,"=");
	s=st.nextToken();
	s=st.nextToken();
	st2=new StringTokenizer(s,", ");
	xOrig=Integer.parseInt(st2.nextToken());
	yOrig=Integer.parseInt(st2.nextToken());


	//ligne2 = br.readLine();
	//ligne3 = ligne3.replace("\"", "");
	//on parcourt tt les blocs
	for(int i=0;i<nb_bloc-1;i++){

		ligne2=br.readLine();
		ligne3 = ligne2.replace("\"", "");

		ligne = ligne3.replace("\t", "");
		st=new StringTokenizer(ligne,"=");
		s=st.nextToken();
		s=st.nextToken();
		st2=new StringTokenizer(s,", ");
		if(Integer.parseInt(st2.nextToken())==xOrig)
			nbBc++;

		if(Integer.parseInt(st2.nextToken())==yOrig)
			nbBl++;


	}

	res[0]=nbBc;
	res[1]=nbBl;

	br.close();
	return res;




}
	
	
	
//return tout les parametres permettant de definir une grille: Nombre de blocs sur une ligne
//															   Nombre de blocs sur une colonne
//															   L'espace entre deux lignes de blocs
//															   L'espace entre deux colonnes de blocs
//															   Diametre moyen du spot
public static int[] getLevel2Param(String filename)throws Exception{

	int res[]=new int[5];
	BufferedReader br;
	StringTokenizer st,st2;
	String ligne,s,ligne2,ligne3;
	int dim[]=getDimGrid(filename);
	int param1[]=getLevel1Param(filename);
	int spot2Y=0;
	int spot0X=0;
	int spot0Y=0;
	int spot1X=0;



	res[0]=dim[1];
	res[1]=dim[0];
	br=new BufferedReader(new FileReader(filename));
	ligne2 = br.readLine();
	ligne3 = ligne2.replace("\"", "");
	//on se place sur le premier bloc
	while(!ligne3.startsWith("Block1=")){
		ligne2 = br.readLine();
		ligne3 = ligne2.replace("\"", "");
	}

	for(int i=0;i<dim[1]+1;i++){

		//donn�es necessaires pour le premier spot du premier bloc de la premier ligne
		if(i==0){

			ligne = ligne3.replace("\t", "");
			st=new StringTokenizer(ligne,"=");
			st.nextToken();
			s=st.nextToken();
			st2=new StringTokenizer(s,", ");
			spot0X=Integer.parseInt(st2.nextToken());
			spot0Y=Integer.parseInt(st2.nextToken());
			res[4]=Integer.parseInt(st2.nextToken());
		}
		//donn�e n�cessaire pour le premier spot du deu				ligne3 = ligne2.replace("\"", "")xi�me bloc de la premiere ligne
		if(i==1){
			ligne = ligne3.replace("\t", "");
			st=new StringTokenizer(ligne,"=");
			st.nextToken();
			s=st.nextToken();
			st2=new StringTokenizer(s,", ");
			spot1X=Integer.parseInt(st2.nextToken());
		}

		//donn�e n�cessaire pour le premier spot 
		if(i==dim[1]){
			ligne = ligne3.replace("\t", "");
			st=new StringTokenizer(ligne,"=");
			st.nextToken();
			s=st.nextToken();
			st2=new StringTokenizer(s,", ");
			st2.nextToken();
			spot2Y=Integer.parseInt(st2.nextToken());
		}
		ligne2=br.readLine();
		ligne3 = ligne2.replace("\"", "");

	}//fin du for

	//calcul des espaces inter-blocs
	res[2]=spot2Y-(param1[1]*param1[2]+spot0Y);
	res[3]=spot1X-(param1[0]*param1[3]+spot0X);
	br.close();

	return res;




}
}
