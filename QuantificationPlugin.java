/* Created on Oct 19, 2005
 * 
 * 
 * This class is an example of quantification plugin 
 
 * Note: the idea here is to indicate the places where the code has to be.
 * updated or modified in order to adapt it to the need.	
 * 
 * @version 2006/10/10
 * @author cklopp
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * 
 */

import ij.ImagePlus;
import ij.ImageStack;
import ij.Undo;
import ij.WindowManager;
import ij.gui.ImageWindow;
import ij.measure.Calibration;
import ij.measure.ResultsTable;
import ij.measure.Measurements;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ByteProcessor;
import ij.process.Blitter;
import ij.plugin.ImageCalculator;
import ij.plugin.Thresholder;
import ij.plugin.filter.Binary;

import ij.plugin.MeasurementsWriter;
import ij.plugin.filter.Analyzer;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.Duplicater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;
import java.lang.Math;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;

public class QuantificationPlugin extends SQuantifPlugin {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int NBCOLONNE = 8;
	
	// *** size to be adjusted to the need
	String[] columnsNames = new String[NBCOLONNE];//array of columns created by the quantification
	int[] columnsTypes = new int[NBCOLONNE];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	//public static int id = -1;
	
	/**************************************************************************************************/
	/***********************************  DECLARATIONS DES ETUDIANTS  *********************************/
	/**************************************************************************************************/
	
	/***************************************** AFFICHAGE FENETRE **************************************/
	
	private static Fenetre f; // Fenetre de lancement du plugin
	
	private static Resultats r = new Resultats(); // Fenetre de resultats du plugin
	
	/***************************************** AFFICHAGE CONSOLE **************************************/
	
	// Pour afficher des infos sur la console durant l'execution mettre true
	private static final boolean AFFICHE = false;
	
	// Pour afficher les images d'un cycle complet de l'analyse d'un spot mettre true
	private static boolean IMAGE;
	
	// Pour afficher les images binarisation,pre-traitement
	private static final boolean CYCLE1 = false;
	
	// Pour afficher les images particules
	private static final boolean CYCLE2 = false;
	
	// Pour afficher les images spot
	private static final boolean CYCLE3 = false;
	
	/***************************************** INFOS SUR L'IMAGE **************************************/
	
	// Dimension de l'image
	private int LARGEUR ;
	private int HAUTEUR ;
	// Coordonnies du centre de l'image
	private int XMILIEU ;
	private int YMILIEU ;
	
	/******************************** MODE DE L ALGORITHME DETECTIONSPOT ******************************/
	
	private static final String CENTRE_GLOBAL = "centre";
	private static final String CENTRE_GRAVITE = "gravite";
	
	/****************************************** INFOS SUR LE SPOT *************************************/
	
	// Variables pour la mise ` jour du tableau de resultats
	private static final int VERT = 0;
	private static final int ROUGE = 1;
	private static final int JAUNE = 2;
	private static final int OUI = 1;
	private static final int NON = 0;
	
	// Etiquettes X des indices de RESULTAT[X] pour simplifier la lecture du programme	
	private static final int COULEUR = 0;
	private static final int PVERT = 1;
	private static final int PROUGE = 2;
	private static final int SURFACE = 3;
	private static final int CIRCULARITE = 4;
	private static final int NBPARTICULE = 5;
	private static final int CENTRE = 6;
	private static final int FIABILITE = 7;
	
	// Distance entre le centre de graviti du spot et le centre de l'image
	// Celle ci permet de determiner si le spot est centri ou pas
	// Valeur en pixel = 25% du rayon de l'image
	private float CRITERECENTRE ;
	private int POURCENTAGE;
	
	// Poucentage au dessous duquel la forme est considerie comme non circulaire
	// Valeur entre 0 et 1
	private float CRITERECIRC;
	
	// Taille minimale d'un spot
	private static final int TMINI = 1;
	
	// Taille en dessous de laquelle la particule n'est pas considerie comme un spot
	private int TAILLESELECTION;   
	
	// Taille maxi d'un spot (Pi * R2) avec R : rayon du spot maxi
	private int TMAXI ;
	
	// Intensiti moyenne en dessous de laquelle le spot est trop sombre
	private int INTENSMOY;
	
	// Valuer du bruit estimi en nombre de particules au dessus de laquelle le spot est consideri comme vide
	private int BRUIT;
	
	// Tableau comportant les resultats des algorithmes ( Ce qu'on affichera dans AGSCAN )
	private float RESULTAT[] = new float[NBCOLONNE];
	
	/********************************************** BINARY ********************************************/
		
	// Binary permetant de realiser (erosion, dilatation, etc) sur des imagesplus
	private Binary BIN ;
	
	// Valeurs possibles pour le mode de l'agorithme binaire
	public static final String EROSION = "erode";
	public static final String DILATATION = "dilate";
	public static final String OUVERTURE = "open";
	public static final String FERMETURE = "close";
	public static final String CONTOUR = "outline";
	public static final String SQUELETISATION = "skel";
	
	/************************* LISTE DES PARAMETRES CALCULABLES DE L'IMAGE : **************************/
	
	// AREA : nombre de pixels de la forme
	// MEAN : ???
	// STD_DEV : ???
	// MODE : ???
	// MIN_MAX : Min = valeur minimal des pixels de l'image
	//			 Max = valeur maximal des pixels de l'image
	// CENTROID : ??? 
	// CENTER_OF_MASS : [XM,YM] = coordonnies du centre de graviti de la forme
	// PERIMETER : ???
	// LIMIT : pas affichi dans la table de resultats
	// RECT : infos sur la plus petite zone rectangulaire pouvant contenir la forme
	//        BX = abscisse minimale de l'ensemble des pixels de la forme
	//		  BY = ordonnie minimale de l'ensemble des pixels de la forme
	//		  [BX,BY] = coordonnies du point haut gauche de la zone
	//		  Width = largeur de la zone
	//        Height = hauteur de la zone
	// LABELS : donne la titre de l'ImagePlus 
	// ELLIPSE : ???
	// INVERT_Y : ???
	// CIRCULARITY : forme circulaire ou pas ( CIRC = 0 ` 1 avec 1 : CERCLE )
	// FERET : ???
	// INTEGRATED_DENSITY : ???
	
	// Choix des valeurs ` analyser
	private static final int MESURES = Measurements.AREA + Measurements.CENTER_OF_MASS + 
	Measurements.PERIMETER + Measurements.RECT + Measurements.CIRCULARITY;
	
	/************************* LISTE DES OPTIONS POUR LE ParticleAnalyzer : ***************************/
	
	// SHOW_RESULTS : Display results in the ImageJ console.
	// SHOW_SUMMARY : Obsolete 
	// SHOW_OUTLINES : Display image containing outlines of measured paticles.	
	// EXCLUDE_EDGE_PARTICLES : Do not measure particles touching edge of image. 
	// SHOW_SIZE_DISTRIBUTION : Display a particle size distribution histogram.
	// SHOW_PROGRESS : Display a progress bar.
	// CLEAR_WORKSHEET : Clear ImageJ console before starting.
	// RECORD_STARTS : Record starting coordinates so outline can be recreated later using doWand(x,y).
	// DISPLAY_SUMMARY : Display a summary.
	// SHOW_NONE = : Do not display particle outline image.
	
	// Choix des options de ParticleAnalyzer
	private  int OPTIONS ;	
	
	/************************************* TABLE DE RESULTATS *****************************************/
	
	// Analyzer.getResultsTable().deleteRow(0);
	// Analyzer.getResultsTable().deleteRow(1);
	// Creation de la table de resultats contenant les resultats suite ` un ParticleAnalyzer
	private ResultsTable rt ;
	
	/************************************** PARTICLE ANALYZER *****************************************/
	
	// Particule analyzer permettant d'extraire des infos utiles sur l'image	
	private ParticleAnalyzer Panalyze ;
	
	/****************************************** DUPLICATER ********************************************/
	
	// Sert ` dupliquer une imagePlus
	private Duplicater D;
	
	/**************************************** QUANTIFICATION ******************************************/
	
	// Nombre de spots rouge fiables
	private int NBSPOTROUGE ;
	
	// Nombre de spots vert fiables
	private int NBSPOTVERT ;
	
	// Nombre de spots jaune fiables
	private int NBSPOTJAUNE ;
	
	// Nombre de spots non fiables
	private int NBSPOTNOFIABLE ;
	
	//	 Nombre de spots non centri
	private int NBSPOTNOCENTRE ;
	
	// Nombre de spots non centri
	private int NBSPOTNOCIRC ;
	
	// Nombre de spots multiples
	private int NBSPOTMULT ;
	
	// Nombre de cases sans spot
	private int NBCASESVIDE ;
	
	/**************************************************************************************************/
	/************************************  FIN DU CODE DES ETUDIANTS  *********************************/
	/**************************************************************************************************/
	
	
	public QuantificationPlugin() {
		
		super("Quantification simple derniere",null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		imagesMin = 2;
		imagesMax = 2;
		
		//declaration of columns of the plugin
		
		// *** to be adjusted to the need
		//name of quantification columns
		columnsNames[0]= "Couleur";
		columnsNames[1]= "Vert (%)";
		columnsNames[2]= "Rouge (%)";
		columnsNames[3]= "Surface";		
		columnsNames[4]= "Circulariti";
		columnsNames[5]= "Nb de Particules";
		columnsNames[6]= "Centri";
		columnsNames[7]= "Fiabiliti";

		// *** to be adjusted to the need
		//type of quantification results
		columnsTypes[0] = TColumn.TYPE_INTEGER;
		columnsTypes[1] = TColumn.TYPE_INTEGER;
		columnsTypes[2] = TColumn.TYPE_INTEGER;
		columnsTypes[3] = TColumn.TYPE_INTEGER;
		columnsTypes[4] = TColumn.TYPE_REAL;
		columnsTypes[5] = TColumn.TYPE_INTEGER;
		columnsTypes[6] = TColumn.TYPE_INTEGER;		
		columnsTypes[7] = TColumn.TYPE_INTEGER;
		
		// Si on choisit l'option afficher alors on affiche dans la console les infos de l'analyse
		if(AFFICHE)
		{
			OPTIONS = ParticleAnalyzer.CLEAR_WORKSHEET + ParticleAnalyzer.SHOW_RESULTS;
		}
		else
		{
			OPTIONS = ParticleAnalyzer.CLEAR_WORKSHEET + ParticleAnalyzer.SHOW_NONE;
		}

	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * run of the quantification 
	 */	
	public void run() {				
		// information about the alignment
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages(); // in the stack you have the different images 
		
		// get the selected spots 
		Vector spots = getSelectedSpots();

		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// addition of spots parameters to each spot
		if (AFFICHE) {System.out.println("columnsNames.length = "+columnsNames.length);}
		for (int i=0;i<columnsNames.length;i++)	{
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}
		
		// ------ memory and processing time -----------
		// some parameter can be set for once and used after in order to be more efficient
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight(); //height of one pixel in micrometers( for example 25)
		
		// or in order not to recreate new object in the loop the object are created once and reinitialized for
		// each spot
		TSpot currentSpot;
		currentSpot =(TSpot) spots.elementAt(0);
		int spotWidth = (int)(currentSpot.getSpotWidth()/pixelWidth);
		int spotHeight = (int)(currentSpot.getSpotHeight()/pixelHeight);
		
		/**************************************************************************************************/
		/***********************************  DECLARATIONS DES ETUDIANTS  *********************************/
		/**************************************************************************************************/					
		
		/** Declarations supplementaires */		
		
		int X,Y,A,B,RAYON;
		ImageProcessor IPV,IPR;
		
		/** Initialisation des attributs d'analyse */
		
		// Init de la largeur et hauteur de l'image
		LARGEUR = spotWidth;
		HAUTEUR = spotHeight;
		
		// Calcul des coordonnies du centre de l'image
		XMILIEU = Math.round(LARGEUR/2);
		YMILIEU = Math.round(HAUTEUR/2);		
		
		// Init de la surface maximum du spot en pixel
		TMAXI = (int)(Math.PI*(XMILIEU*XMILIEU));
		
		/** ON AFFICHE LE MENU DU PLUGIN */
		
		f = new Fenetre(TMAXI);		
		boolean fermer = menu();
		
		// Init de CRITERECENTRE ( Distance en pixel ) = 25% du rayon du spot maxi
		CRITERECENTRE = (XMILIEU*POURCENTAGE)/100;
		
		// Initialisation de Binary 
		BIN = new Binary();
		
		// Initialisation de la table de resultats 
		rt = Analyzer.getResultsTable();
		
		// Initialisation du Particule analyzer
		// TMINI = 1 pour analyser toutes les paricules meme les plus petites
		Panalyze = new ParticleAnalyzer(OPTIONS, MESURES, rt, TMINI, TMAXI);
		
		// Creation du duplicater
		D = new Duplicater();
		
		// Init des compteurs de quantification
		NBSPOTROUGE = 0;
		NBSPOTVERT = 0;
		NBSPOTJAUNE = 0;
		NBSPOTNOFIABLE = 0;
		NBCASESVIDE = 0;
		NBSPOTNOCENTRE = 0;
		NBSPOTNOCIRC = 0;
		NBSPOTMULT = 0;
		
		/**************************************************************************************************/
		/************************************  FIN DU CODE DES ETUDIANTS  *********************************/
		/**************************************************************************************************/
		
		//System.out.println("spotWidth = "+spotWidth+" spotHeight = "+spotWidth);
		ImageProcessor ipSpot_16green = new FloatProcessor(spotWidth, spotHeight, null, null);		
		ImageProcessor ipSpot_16red = new FloatProcessor(spotWidth, spotHeight, null, null);	
		
		float[] greenSpot = new float[spotWidth*spotHeight];
		float[] redSpot = new float[spotWidth*spotHeight];
						
		// This part in handling the GUI before the job 
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part
		
		// to retrieve the data of the images (colors in 16 bits and max image in 8 bits)
		ImageProcessor ip_16red = stack.getProcessor(1);
		ImageProcessor ip_16green = stack.getProcessor(2);	
		
		if(fermer)
		{		
		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i); //to get current spot 
			
			// to fill the spot image processors
			for (int j = 0; j < spotWidth; j++){
				for (int k = 0; k < spotHeight; k++){
					greenSpot[j*spotWidth+ k] = ip_16green.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
					redSpot[j*spotWidth+ k] = ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
				}
			}
			
		/**************************************************************************************************/
		/***************************************  CODE DES ETUDIANTS  *************************************/
		/**************************************************************************************************/
			
			// to create the imageProcessors and imagePlus
			ipSpot_16green = new FloatProcessor(spotWidth, spotHeight, greenSpot, null);			
			ipSpot_16red = new FloatProcessor(spotWidth, spotHeight, redSpot, null);
			
			// Creation des images que l'on ne va pas traiter contenant les intensitis des pixels
			ImageProcessor GP = ipSpot_16green;
			ImageProcessor RP = ipSpot_16red;
			
			GP.convertToByte(true);
			RP.convertToByte(true);
			
			ImagePlus IVERTE = new ImagePlus("VIntens", GP);
			ImagePlus IROUGE = new ImagePlus("RIntens", RP);			
			
		/** PREPARATION DES IMAGES ROUGE ET VERTE */
			
			// Convertion 8 bits des ImageProcessor		
			ByteProcessor bpG = (ByteProcessor)ipSpot_16green.convertToByte(true);			
			ByteProcessor bpR = (ByteProcessor)ipSpot_16red.convertToByte(true);
			
				// Binarisation des images
					
					bpG.autoThreshold();
					bpR.autoThreshold();
					
	
			// On inverse ces deux images pour obtenir l'image sur laquelle on va travailler
			bpG.invert();
			bpR.invert();
			
			// Creation des ImagePlus
			 		
			ImagePlus impSpot_8green = new ImagePlus("Green spot", bpG);
			ImagePlus impSpot_8red = new ImagePlus("Red spot", bpR);
			
			if (IMAGE||CYCLE1) 
			{ 					
				ImagePlus IBG = D.duplicateStack(impSpot_8green, "1 Green Bin");
				IBG.show(); 
			}
			if (IMAGE||CYCLE1) 
			{  	
				ImagePlus IBR = D.duplicateStack(impSpot_8red, "1 Red Bin");
				IBR.show(); 
			}
			
		/** PRE-TRAITEMENT */
			
			/** SUPPRESSION DES PIXELS EXTERIEURS AU CERCLE DE SELECTION */
			
			IPV = impSpot_8green.getProcessor();
			IPR = impSpot_8red.getProcessor();
			
			for(X=0;X<LARGEUR;X++)	
			{
				for(Y=0;Y<HAUTEUR;Y++)
				{
					A = (X-XMILIEU)*(X-XMILIEU);
					B = (Y-YMILIEU)*(Y-YMILIEU);
					RAYON = XMILIEU*XMILIEU;
					
					// Si le pixel est en dehors du cercle on l'efface
					if(A+B>RAYON)
					{
						IPV.putPixelValue(X, Y, 255);
						IPR.putPixelValue(X, Y, 255);
					}
				}
			}
			
			impSpot_8green.setProcessor("", IPV);
			impSpot_8red.setProcessor("", IPR);
			
			/** SI L'IMAGE EST VIDE */
			
			// On lance une premiere analyse afin de detecter les images ne contenant que du bruit
			
			Panalyze.analyze(impSpot_8green);
			// S'il y a plus de 10 particules c'est que ce n'est que du bruit
			if (Analyzer.getResultsTable().getCounter()>BRUIT)
			{
				impSpot_8green = effacerImage(impSpot_8green);				
				if (AFFICHE) {System.out.println("Pre-Traitement image verte ");}
				if (IMAGE||CYCLE1) 
				{ 	
					ImagePlus IBPG = D.duplicateStack(impSpot_8green, "2 Pre Green");
					IBPG.show(); 
				}
			}
			
			Panalyze.analyze(impSpot_8red);
			// S'il y a plus de 10 particules c'est que ce n'est que du bruit
			if (Analyzer.getResultsTable().getCounter()>10)
			{
				impSpot_8red = effacerImage(impSpot_8red);
				if (AFFICHE) {System.out.println("Pre-Traitement image rouge ");}
				if (IMAGE||CYCLE1) 
				{							
					ImagePlus IBPR = D.duplicateStack(impSpot_8red, "2 Pre Red");
					IBPR.show(); 
				}
			}
			
			// Apres convertion des images sur 8-bits, on peut utiliser les plugins
			// d'ImageJ sur celles ci
			
			if (IMAGE||CYCLE1)
			{
				ImagePlus APTG = D.duplicateStack(impSpot_8green, "3 Apres Pre Green");
				ImagePlus APTR = D.duplicateStack(impSpot_8red, "3 Apres Pre Red");
				APTG.show();
				APTR.show();
			}
			
		
		/** ANALYSE DU SPOT */
			
			SpotAnalyse( impSpot_8green, impSpot_8red ,IVERTE, IROUGE);			
					
		/** AFFICHAGE DES RESULTATS CONCERNANT LE SPOT COURANT */
					
			afficheResultats( currentSpot );			
					
		/**************************************************************************************************/
		/************************************  FIN DU CODE DES ETUDIANTS  *********************************/
		/**************************************************************************************************/
	
			
			// This part in handling the GUI during the job
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
			}
			// ---- end of the GUI part
		}		
		
		r.setResultat(NBSPOTVERT, NBSPOTROUGE, NBSPOTJAUNE, NBSPOTNOFIABLE, NBSPOTNOCENTRE, NBSPOTNOCIRC, NBSPOTMULT, NBCASESVIDE);
		r.montrerRes();		
		}	
		
		// This part in handling the GUI after the job 	
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part 
	}
	
	/**************************************************************************************************/
	/************************************  FONCTIONS DES ETUDIANTS  ***********************************/
	/**************************************************************************************************/

	// afficheResultats( TSpot currentSpot ) 
	// affiche sur la fenetre d'AGSCAN les resultats contenus dans le tableau RESULTATS[]
	
	void afficheResultats( TSpot currentSpot )	{
		
			// Affichage de la couleur du spot			
			currentSpot.addParameter(columnsNames[COULEUR], new Float(RESULTAT[COULEUR]), true);
		
			// Affichage de son intensiti verte en pourcentage		
			currentSpot.addParameter(columnsNames[PVERT], new Float(RESULTAT[PVERT]), true);
		
			// Affichage de son intensiti rouge en pourcentage		
			currentSpot.addParameter(columnsNames[PROUGE], new Float(RESULTAT[PROUGE]), true);
		
			// Affichage de la surface du spot		
			currentSpot.addParameter(columnsNames[SURFACE], new Float(RESULTAT[SURFACE]), true);
		
			// Affichage de la circulariti du spot		
			currentSpot.addParameter(columnsNames[CIRCULARITE], new Float(RESULTAT[CIRCULARITE]), true);
		
			// Affichage du nombre de spot trouvi 		
			currentSpot.addParameter(columnsNames[NBPARTICULE], new Float(RESULTAT[NBPARTICULE]), true);		
		
			// Information sur la bonne position du spot ( s'il est centri )
			currentSpot.addParameter(columnsNames[CENTRE], new Float(RESULTAT[CENTRE]), true);			
		
			// Information sur la fiabiliti du spot ( s'il repond aux criteres de selections )		
			currentSpot.addParameter(columnsNames[FIABILITE], new Float(RESULTAT[FIABILITE]), true);
			
	}
	
	/**************************************************************************************************/
	
	// algoBinaire( ImagePlus Ip1, String mode) permet de realiser un algo sur une image
	// Exemple (erosion de Ip1) si le mode = EROSION
	
	// Valeurs possibles pour le mode :
	// EROSION | DILATATION | OUVERTURE | FERMETURE | CONTOUR | SQUELETISATION 	
	
	void algoBinaire( ImagePlus Ip1, String mode)	{
		
		// Initialisation du mode
		BIN.setup(mode, Ip1);
		
		// Algo sur l'image selon le mode
		BIN.run(Ip1.getProcessor());			
	}
	
	/**************************************************************************************************/
	
	// algoCirculariti(ImagePlus Spot) permet de calculer la circulariti du spot
	// Valeur allant de 0 ` 1
	
	void algoCirculariti(ImagePlus Spot){
		
		int k;
		for(k=0;k<Analyzer.getResultsTable().getCounter();k++)
		{
			RESULTAT[CIRCULARITE] = (float)Math.max(RESULTAT[CIRCULARITE],Analyzer.getResultsTable().getValue("Circ.", k));			
		}
	}
	
	/**************************************************************************************************/
	
	// algoCouleur(ImagePlus Green, ImagePlus Red) calcul la couleur du spot
	// Et egalement sont pourcentage de vert et de rouge
	
	// Variables utilisies :
	// RESULTAT[] est un attribut de la class
	// COULEUR, PVERT, PROUGE, VERT, ROUGE, JAUNE sont des constantes de la class
	
	// Resultat de l'algo :
	// RESULTAT[COULEUR] = VERT | ROUGE | JAUNE
	// RESULTAT[PVERT] = pourcentage de vert
	// RESULTAT[PROUGE] = pourcentage de rouge
	// RESULTAT[SURFACE] = surface en pixel du spot
	
	void algoCouleur(ImagePlus Green, ImagePlus Red, ImagePlus IntensitiV, ImagePlus IntensitiR) {
		
		int i,X,Y,XP,YP;
		int X_Gauche,Y_Haut,Width,Height;
		float AreaT = 0,AreaR = 0,AreaV = 0;
		float PRouge = 0,PVert = 0;
		ImagePlus ImgCompl;	
		int pixelV=0,pixelR=0,pixelJ=0;
		float valeurV=0,valeurR=0;
		
		/** On lance l'analyse sur l'image verte */
		
		Panalyze.analyze(Green);
		
		for(i=0;i<Analyzer.getResultsTable().getCounter();i++)
		{
			AreaV = AreaV + (float)Analyzer.getResultsTable().getValue("Area", i);
		}
		
		/** On lance l'analyse sur l'image rouge */
		
		Panalyze.analyze(Red);
		
		for(i=0;i<Analyzer.getResultsTable().getCounter();i++)
		{
			AreaR = AreaR + (float)Analyzer.getResultsTable().getValue("Area", i);
		}
		
		// Reconstruction de l'image totale ( binaire )
		// ( On recupere le mode dans la class Blitter )				
		ImgCompl = D.duplicateStack(calculImage( Green, Red , Blitter.AND), "7 Spot");
		
		if (IMAGE||CYCLE3){ImgCompl.show();}
		
		// On lance l'analyze sur l'image complete
		Panalyze.analyze(ImgCompl);
		
		for(i=0;i<Analyzer.getResultsTable().getCounter();i++)
		{
			AreaT = AreaT + (float)Analyzer.getResultsTable().getValue("Area", i);
		}
		
		if (Analyzer.getResultsTable().getCounter()>0)
		{
		// Mise ` jour de la surface du spot
		RESULTAT[SURFACE] = AreaT;
		
		// Mise ` jour du pourcentage de vert
		PVert = (AreaV*100)/AreaT;
		RESULTAT[PVERT] = PVert;
		
		// Mise ` jour du pourcentage de rouge
		PRouge = (AreaR*100)/AreaT;
		RESULTAT[PROUGE] = PRouge;
		}		
		
		// Detection de la couleur du spot
		
		for(i=0;i<Analyzer.getResultsTable().getCounter();i++)
		{
			Width = (int)Analyzer.getResultsTable().getValue("Width", i);
			Height = (int)Analyzer.getResultsTable().getValue("Height", i);
			X_Gauche = (int)Analyzer.getResultsTable().getValue("BX", i);
			Y_Haut = (int)Analyzer.getResultsTable().getValue("BY", i);
		
			for(X = 0; X < Width; X++)
			{
				for(Y = 0; Y < Height; Y++)
				{
					// Calcul des coordonnies de chaque pixels de la zone traitie
					XP = (X_Gauche + X);
					YP = (Y_Haut + Y);
					
					
					if(Green.getProcessor().getPixelValue(XP, YP)==0)
					{
						valeurV = (IntensitiV.getProcessor().getPixelValue(XP, YP)/257);	
					}
					if(Red.getProcessor().getPixelValue(XP, YP)==0)
					{
						valeurR	= (IntensitiR.getProcessor().getPixelValue(XP, YP)/257);
					}
					
					if(Math.abs(valeurV-valeurR)<50)
					{
						pixelJ++;
					}
					else
					{
						if(valeurV>valeurR)
						{
							pixelV++;
						}
						else
						{
							pixelR++;
						}
					}					
				}// end for
			}// end for	
			
		}
		
		if(Analyzer.getResultsTable().getCounter()>0)
		{
		if(pixelJ>pixelR)
		{
			if(pixelJ>pixelV)
			{
				RESULTAT[COULEUR]=JAUNE;
			}
			else
			{
				RESULTAT[COULEUR]=VERT;
			}
		}
		else
		{
			if(pixelR>pixelV)
			{
				RESULTAT[COULEUR]=ROUGE;
			}
			else
			{
				RESULTAT[COULEUR]=VERT;
			}
		}
		}
		else
		{
			RESULTAT[COULEUR] = 9999;
		}
	}
	
	/**************************************************************************************************/
	
	// algoDetectionParticule() supprime toutes les lignes de la table de resultats
	// correspondants ` des particules nayant pas la taille d'un spot
	// Supprime egalement les particules trop sombre
	
	// Resultat de l'algo :
	// Une ImagePlus epurie de toutes les particules inutiles
	
	ImagePlus algoDetectionParticule( ImagePlus I ,ImagePlus Intensiti) {
		
		// Declarations
		int XP,YP,DX,DY,indice,MoyenneCouleur,somme;
		int Area,X_Gauche,Y_Haut,Width,Height;
		boolean supprime;
		ImageProcessor IP = I.getProcessor();
		
		// On lance une analyse sur l'image
		Panalyze.analyze(I);
		
		// S'il y a des particules sur l'image
		if (Analyzer.getResultsTable().getCounter()>0)
		{
			// On parcourt toute la table de resultat
			for(indice=0;indice<Analyzer.getResultsTable().getCounter();indice++)
			{
				if (AFFICHE) {System.out.println("----------------- Ligne "+ indice +" -----------------");}				
				
				// Init du marqueur de suppression de ligne
				supprime = false;
				
				// Pour chaque ligne (particule) on recupere les donnies importantes
				Area = (int)Analyzer.getResultsTable().getValue("Area", indice);
				Width = (int)Analyzer.getResultsTable().getValue("Width", indice);
				Height = (int)Analyzer.getResultsTable().getValue("Height", indice);
				X_Gauche = (int)Analyzer.getResultsTable().getValue("BX", indice);
				Y_Haut = (int)Analyzer.getResultsTable().getValue("BY", indice);
				
				// Calcul de l'intensiti moyenne
				somme = 0;				
				// On parcourt la zone englobante de la particule
				for(DX = 0; DX < Width; DX++)
				{
					for(DY = 0; DY < Height; DY++)
					{
						// Calcul des coordonnies de chaque pixels de la zone traitie
						XP = (X_Gauche + DX);
						YP = (Y_Haut + DY);
						somme = somme + (int)(Intensiti.getProcessor().getPixelValue(XP, YP)/257);
					}// end for
				}// end for				
				MoyenneCouleur = (somme/(Width*Height));
				if (AFFICHE){System.out.println("Moyenne des intensitis de " + Area + " = " + MoyenneCouleur);}
				
				if(Area<TAILLESELECTION)
				{
					// On enregistre la suppression de la particule
					supprime = true;
					
					if (AFFICHE) {System.out.println("La particule "+ Area + " est trop petite ");}
				}
				
				// Si la particule est trop sombre
				if(MoyenneCouleur<INTENSMOY)
				{
					// On enregistre la suppression de la particule
					supprime = true;
					
					if (AFFICHE) {System.out.println("La particule "+ Area + " est trop sombre ");}
					
				}
			
				// Si la particule ne repond pas au critere d'un spot on l'efface
				if(supprime)
				{
					if (AFFICHE) {System.out.println("La particule "+ Area + " est supprimie ");}
					
					// On parcourt la zone englobante de la particule pour l'effacer
					for(DX = 0; DX <= Width; DX++)
					{
						for(DY = 0; DY <= Height; DY++)
						{
							// Calcul des coordonnies de chaque pixels de la zone traitie
							XP = (X_Gauche + DX);
							YP = (Y_Haut + DY);								
							// On efface le pixel ( en mettant sa valeur ` 255 )
							IP.putPixel(XP, YP, 255);							
						}// end for
					}// end for
				}// end if
				else
				{
					if (AFFICHE) {System.out.println("La particule "+ Area + " est correcte ");}
				}
				
			}// end for nb lignes
		}// end if	
		
		// Mise ` jour de l'ImagePlus avec l'image processor modifiie
		I.setProcessor(I.getTitle(), IP);
		
		return I;
		
	}
	
	/**************************************************************************************************/
	
	// algoDetectionSpot() supprime toutes les lignes de la table de resultats
	// correspondants ` des particules nayant pas la taille d'un spot
	// Supprime egalement les particules trop loin du centre de l'image
	
	// Resultat de l'algo :
	// Une ImagePlus epurie de toutes les particules inutiles
	// RESULTAT[NBPARTICULE] = nombre de particules sur la zone totale		
	
	ImagePlus algoDetectionSpot( ImagePlus I , String Mode) {
		
		// Declarations
		int XP,YP,DX,DY,XMZE,YMZE,indice;
		double DistanceCentre = 0;
		int nbLignes;
		int Area,XM,YM,X_Gauche,Y_Haut,Width,Height;
		double A,B,Circ;		
		ImageProcessor IP = I.getProcessor();
		boolean centre = false;
		
		// On lance l'analyse sur l'image
		Panalyze.analyze(I);
		
		// Init du nombre de lignes
		nbLignes = Analyzer.getResultsTable().getCounter();
		
		// S'il y a des particules sur l'image
		if (Analyzer.getResultsTable().getCounter()>0)
		{
			
			// Init de l'indice
			indice = 0;
			// On parcourt toute la table de resultat
			while(indice<nbLignes)
			{
				if (AFFICHE) {System.out.println("----------------- Ligne "+ indice +" -----------------");}				
				
				// Pour chaque ligne (particule) on recupere les donnies importantes
				Area = (int)Analyzer.getResultsTable().getValue("Area", indice);
				Width = (int)Analyzer.getResultsTable().getValue("Width", indice);
				Height = (int)Analyzer.getResultsTable().getValue("Height", indice);
				X_Gauche = (int)Analyzer.getResultsTable().getValue("BX", indice);
				Y_Haut = (int)Analyzer.getResultsTable().getValue("BY", indice);				
				XM = (int)Analyzer.getResultsTable().getValue("XM", indice);
				YM = (int)Analyzer.getResultsTable().getValue("YM", indice);
				Circ = Analyzer.getResultsTable().getValue("Circ.", indice);
				
				// Si le mode choisi est CENTRE_GLOBAL alors on calcule la distance par rapport au centre 
				// de la zone englobant la particule
				if(Mode.equals(CENTRE_GLOBAL))
				{
					// Calcul des coordonnies du milieu de la zone englobant la particule
					XMZE = Math.round((X_Gauche + Width/2));
					YMZE = Math.round((Y_Haut + Height/2));
					// Calcul de la distance 			
					A = (double)((XMILIEU -XMZE)*(XMILIEU -XMZE));
					B = (double)((YMILIEU -YMZE)*(YMILIEU -YMZE));
					DistanceCentre = Math.sqrt(A+B);
				}
				
				// Si le mode choisi est CENTRE_GRAVITE alors on calcule la distance par rapport au centre 
				// de graviti de la particule
				// Comme le spot est censi etre circulaire, son centre de graviti est le plus 
				// significatif pour calculer sa position par rapport au centre de l'image
				if(Mode.equals(CENTRE_GRAVITE))
				{
					// Calcul de la distance 			
					A = (double)((XMILIEU -XM)*(XMILIEU -XM));
					B = (double)((YMILIEU -YM)*(YMILIEU -YM));
					DistanceCentre = Math.sqrt(A+B);
				}
			
				// Si le critere sur le centrage de la particule n'est pas verifii on l'efface
				if((DistanceCentre > CRITERECENTRE))
				{
					
					if(Circ>CRITERECIRC)
					{					
						centre = false;
						// On passe ` la ligne suivante
						indice = indice + 1;
					}
					else
					{					
						// On supprime la ligne correspondante dans la table de resultats
						Analyzer.getResultsTable().deleteRow(indice);
						// On remet ` jour le nombre de lignes de la table de resultats
						nbLignes = nbLignes - 1;
					
						if (AFFICHE) {System.out.println("La particule "+ Area + " n'est pas un spot ");}
						if (AFFICHE) {System.out.println("La particule "+ Area + " est supprimie ");}
					
						// On parcourt la zone englobante de la particule pour l'effacer
						for(DX = 0; DX <= Width; DX++)
						{
							for(DY = 0; DY <= Height; DY++)
							{
								// Calcul des coordonnies de chaque pixels de la zone traitie
								XP = (X_Gauche + DX);
								YP = (Y_Haut + DY);								
								// On efface le pixel ( en mettant sa valeur ` 255 )
								IP.putPixel(XP, YP, 255);							
							}// end for
						}// end for
					}// end if
				}// end if
				else
				{
					// On passe ` la ligne suivante
					indice = indice + 1;
					centre = true;
					
					if (AFFICHE) {System.out.println("La particule "+ Area + " est correcte ");}
					
				}
				
			}// end while
		}// end if
		
		if((Analyzer.getResultsTable().getCounter()>=1)&&(centre)){RESULTAT[CENTRE]=OUI;}		
		if((Analyzer.getResultsTable().getCounter()==0)){RESULTAT[CENTRE]=9999;}
		// Mise ` jour de l'ImagePlus avec l'image processor modifiie
		I.setProcessor(I.getTitle(), IP);
		
		return I;
		
	}
	
	/**************************************************************************************************/
	
	// calculImage( ImagePlus Ip1, ImagePlus Ip2, int mode) permet de realiser un calcul entre deux images
	// Exemple (Ip1 AND Ip2) si le mode = AND
	
	// Valeurs possibles pour le mode : Blitter.X
	// X = COPY | COPY_INVERTED | COPY_TRANSPARENT | ADD | SUBTRACT | MULTIPLY | DIVIDE 
	// AVERAGE | DIFFERENCE | AND | OR | XOR | MIN | MAX 
	
	ImagePlus calculImage( ImagePlus Ip1, ImagePlus Ip2, int mode)	{
		
		ImagePlus P = D.duplicateStack(Ip1,"");
		
		// On recupere les images processor des deux ImagesPlus
		ImageProcessor image1 = P.getProcessor();
		ImageProcessor image2 = Ip2.getProcessor();
		
		// On fait le calcul entre les deux images selon le mode choisi
		image1.copyBits(image2, 0, 0, mode);
		
		// Mise ` jour de l'image
		P.setProcessor("AND", image1);
		
		return P;
	}
	
	/**************************************************************************************************/
	
	// effacerImage( ImageProcessor IP ) efface tous les pixels de l'image
	
	ImagePlus effacerImage( ImagePlus IPlus )	{
		
		// Declaration
		int DX,DY;
		ImageProcessor IP;
		IP = IPlus.getProcessor();
		
		// On parcourt toute l'image
		for(DX = 0; DX < LARGEUR; DX++)
		{
			for(DY = 0; DY < HAUTEUR; DY++)
			{							
				//	On efface le pixel ( en mettant sa valeur ` 255 )
					IP.putPixel(DX, DY, 255);
									
			}// end for
		}// end for
		
		IPlus.setProcessor(IPlus.getTitle(), IP);		
		
		return IPlus;
			
	}
	
	/**************************************************************************************************/
	
	// menu() affiche une fenetre permettant de lancer l'analyse
	
	// On peut regler :
	// CRITERECENTRE allant de 0 ` XMILIEU  (XMILIEU correspondant au rayon du cercle de selection)
	// CRITERECIRC allant de 0 ` 1  ( 1 signifie cercle parfait )
	// TAILLESELECTION allant de 1 ` TMAXI
	// INTENSMOY allant de 0 ` 255
	
	// Valeurs par defaut :
	// CRITERECENTRE = (XMILIEU*25)/100;  ( soit 25 % du rayon par rapport au centre )
	// CRITERECIRC = 0.60
	// TAILLESELECTION  = 9
	// INTENSMOY = 70
	
	
	boolean menu()	{
		
		r.cacherRes();
		f.montrer();
		f.defaut();
		while(f.b){}
		
		BRUIT = f.bruit;
		INTENSMOY = f.int_moy;
		TAILLESELECTION = f.taille_sel;
		CRITERECIRC = (f.crit_circ)/100;
		POURCENTAGE = f.pourcent;
		IMAGE = f.image;	
		
		return f.fermer;
			
	}
	
	
	/**************************************************************************************************/
	
	// SpotAnalyse( ImagePlus Green, ImagePlus Red, int MV, int MR) met ` jour le tableau resultat[]	
	
	// Valeurs calculies :
	// CIRCULARITE | CENTRE | NBPARTICULE | FIABILITE 	
	
	void SpotAnalyse( ImagePlus Green, ImagePlus Red, ImagePlus MV, ImagePlus MR)	{
		
		// Declaration
		ImagePlus ImgParticuleV,ImgParticuleR,IMGPART,GreenSpot,RedSpot;		
		
	/** On initialise tout les resultats avant de faire les analyses */
		RESULTAT[COULEUR] = 9999;
		RESULTAT[PVERT] = 9999;
		RESULTAT[PROUGE] = 9999;
		RESULTAT[NBPARTICULE] = 0;
		RESULTAT[SURFACE] = 0;		
		RESULTAT[CIRCULARITE] = 0;
		RESULTAT[CENTRE] = NON ;
		RESULTAT[FIABILITE] = NON ;
	
	/** On lance une detection des particules sur l'image verte */
		
		if (AFFICHE) {System.out.println("----------------- SPOT VERT -----------------");}		
		if (AFFICHE) {System.out.println("----------------- Detection des particules -----------------");}
		if (AFFICHE) {System.out.println("Moyenne verte = " + MV);}
		
		Green = algoDetectionParticule( Green ,MV);		
		
		if (IMAGE||CYCLE2)
		{
			ImgParticuleV = D.duplicateStack(Green, "4 Part Vertes");
			ImgParticuleV.show();
		}	
		
	/** On lance une detection des particules sur l'image rouge */
		
		if (AFFICHE) {System.out.println("----------------- SPOT ROUGE -----------------");}
		if (AFFICHE) {System.out.println("----------------- Detection des particules -----------------");}
		if (AFFICHE) {System.out.println("Moyenne rouge = " + MR);}		
		
		Red = algoDetectionParticule( Red ,MR);
		
		if (IMAGE||CYCLE2)
		{
			ImgParticuleR = D.duplicateStack(Red , "4 Part Rouges");
			ImgParticuleR.show();
		}	
		
		if (AFFICHE) {System.out.println("");}
		if (AFFICHE) {System.out.println("----------------- Fin de L'analyse -----------------");}
	
	/** On met ` jour le nombre de particules */
	 
		// Reconstruction de l'image totale PARTICULES( binaire )
		// ( On recupere le mode dans la class Blitter )
		
		IMGPART = D.duplicateStack(calculImage( Green, Red, Blitter.AND), "5 Particules");
		
		if (IMAGE||CYCLE2)
		{
			IMGPART.show();
		}	
	
		Panalyze.analyze(IMGPART);
		if (AFFICHE) {System.out.println("Nombre de particules = " + Analyzer.getResultsTable().getCounter());}
		RESULTAT[NBPARTICULE]=Analyzer.getResultsTable().getCounter();
	
	/** On lance une detection des spot sur l'image particule verte */
	
		// On detecte le spot en supprimant tous les pixels n'appartenant pas au spot
		// On choisi le mode CENTRE_GLOBAL car on detecte le spot sur une image ne correspondant
		// qu'` une couleur du spot
	
		if (AFFICHE) {System.out.println("----------------- SPOT VERT -----------------");}	
		if (AFFICHE) {System.out.println("----------------- Detection du spot -----------------");}
		
		Green = algoDetectionSpot(Green,CENTRE_GLOBAL);		
		
		if (IMAGE||CYCLE3) 
		{
			GreenSpot = D.duplicateStack(Green , "6 Spot Vert");
			GreenSpot.show();
		}
		
	/** On lance une detection des spot sur l'image particule rouge */
	
		// On detecte le spot en supprimant tous les pixels n'appartenant pas au spot
		// On choisi le mode CENTRE_GLOBAL car on detecte le spot sur une image ne correspondant
		// qu'` une couleur du spot
	
		if (AFFICHE) {System.out.println("----------------- SPOT ROUGE -----------------");}
		if (AFFICHE) {System.out.println("----------------- Detection du spot -----------------");}
		
		Red = algoDetectionSpot(Red,CENTRE_GLOBAL);
		
		if (IMAGE||CYCLE3) 
		{
			RedSpot = D.duplicateStack(Red , "6 Spot Rouge");
			RedSpot.show();
		}
		
		if (AFFICHE) {System.out.println("");}
		if (AFFICHE) {System.out.println("----------------- Fin de L'analyse -----------------");}
	
	
	/** Calcul de la couleur de l'image */
	 
		algoCouleur(Green,Red,MV,MR);
		
		ImagePlus IMC = D.duplicateStack(algoDetectionSpot(IMGPART,CENTRE_GLOBAL),"SPOT FINAL");
		if (IMAGE||CYCLE3) 
		{
			IMC.show();
		}
		
		if (RESULTAT[CENTRE]==NON) { 	// On incremente NBSPOTNOCENTRE;
											NBSPOTNOCENTRE++ ;
										}
		
	/** Calcul de la circulariti du spot */
		
		algoCirculariti(IMC);		
		
	/** Mise ` jour du critere de FIABILITE */	
		
		if((RESULTAT[CIRCULARITE]>CRITERECIRC)&&(RESULTAT[CENTRE]==OUI)&&(RESULTAT[NBPARTICULE]==1)){ RESULTAT[FIABILITE] = OUI ;}	
		
	/** Quantification */
		
		
		
		if(RESULTAT[FIABILITE] == OUI)
		{
			if(RESULTAT[COULEUR]==VERT)
			{
				NBSPOTVERT++;
			}
			if(RESULTAT[COULEUR]==ROUGE)
			{
				NBSPOTROUGE++;
			}
			if(RESULTAT[COULEUR]==JAUNE)
			{
				NBSPOTJAUNE++;
			}
		}
		else
		{
			if(RESULTAT[NBPARTICULE]==0)
			{
				NBCASESVIDE++;
			}
			else
			{
				if(RESULTAT[NBPARTICULE]>1)
				{
					NBSPOTMULT++ ;
				}
				else
				{
					if (RESULTAT[CIRCULARITE] <= CRITERECIRC)
					{					
						NBSPOTNOCIRC++ ;											
					}
				}
				NBSPOTNOFIABLE++;
			}
		}
		
		
		
		
			
	}
	/**************************************************************************************************/
	/********************************  FIN DES FONCTIONS DES ETUDIANTS  *******************************/
	/**************************************************************************************************/	
	
	
}