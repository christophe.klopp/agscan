import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.File;

import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.GraphicsContext3D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Raster;
import javax.media.j3d.View;
//import javax.media.jai.JAI;
import javax.swing.JPanel;
import javax.vecmath.Point3f;

import agscan.data.element.TDataElement;

import com.sun.j3d.utils.universe.SimpleUniverse;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TView3DPanel extends JPanel {
  private Canvas3D canvas;
  private SimpleUniverse universe;
  public TView3DPanel(TDataElement element) {
    super();
    setPreferredSize(new Dimension(800, 600));
    GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
    canvas = new Canvas3D(config);
    canvas.setSize(new Dimension(800, 600));//taille de l'image
    universe = new SimpleUniverse(canvas);
    universe.getViewingPlatform().setNominalViewingTransform();
    add(canvas);
  }
  public void init(TDataElement element) {
  BranchGroup bg = null;
 if (element != null) bg = T3DTool.getBranchGroup();
    universe.cleanup();
    universe = new SimpleUniverse(canvas);
    universe.getViewingPlatform().setNominalViewingTransform();
    canvas.getView().setTransparencySortingPolicy(View.TRANSPARENCY_SORT_GEOMETRY);
    canvas.getView().setVisibilityPolicy(View.VISIBILITY_DRAW_VISIBLE);
  if (bg != null) universe.addBranchGraph(bg);
  }
  public void save(File file, String desc) {
    BufferedImage bi = createBufferedImageFromCanvas3D(canvas);
    ParameterBlock pb = new ParameterBlock();
    if (desc.equals("JPEG images (*.jpg)"))
      pb.add(file.getAbsolutePath()).add("jpeg");
    else if (desc.equals("TIFF images (*.tif)"))
      pb.add(file.getAbsolutePath()).add("tiff");
    else if (desc.equals("PNG images (*.png)"))
      pb.add(file.getAbsolutePath()).add("png");
    else if (desc.equals("BMP images (*.bmp)"))
      pb.add(file.getAbsolutePath()).add("bmp");
    pb.addSource(bi);
    //JAI.create("filestore", pb);
    add(canvas);
  }
  public static BufferedImage createBufferedImageFromCanvas3D(Canvas3D canvas) {
    canvas.waitForOffScreenRendering();
    GraphicsContext3D  ctx = canvas.getGraphicsContext3D();
    int w = canvas.getWidth();
    int h = canvas.getHeight();
    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    ImageComponent2D im = new ImageComponent2D(ImageComponent.FORMAT_RGB, bi);
    Raster ras = new Raster(new Point3f( -1.0f, -1.0f, -1.0f ), Raster.RASTER_COLOR, 0, 0, w, h, im, null );
    ctx.flush(true);
    ctx.readRaster( ras );
    return ras.getImage().getImage();
  }
}
