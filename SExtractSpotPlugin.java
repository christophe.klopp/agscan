/*
 * STemplateMatchingPlugin performs a powerfull global alignment based on the correlation of the image
 * A pattern of gaussian spot is searched.
 * Created on 20th February 2006
 * @version 2006/02/20
 * @author Remi Cathelin
 */


import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.data.controler.TGridPositionControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TImageModel;
import agscan.dialog.TOpenNImagesDialog;
import agscan.plugins.SAlignPlugin;
import agscan.AGScan;
import agscan.TEventHandler;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SExtractSpotPlugin extends SQuantifPlugin{
	
	/**
	 * constructor of the class
	 */
	private int size;
	private String[] colors;
	
	public SExtractSpotPlugin () {
		super("Extract spot", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(ALIGNMENT_TYPE); 
		setImagesMin(1);
		setImagesMax(20);
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void actionPerformed(ActionEvent e) {
		//thread associated launched only if quantification is possible with the current stack of images
		this.run();

	}


	/**		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
	 * run of the quantification 
	 */
	public void run() {				
		// infrmation about the alignment
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageProcessor ip = model.getInitialProcessor();
		TSpot currentSpot;
		int windowSize = 20;
		
		ij.plugin.Zoom zoom = new ij.plugin.Zoom();

		Vector spots = getSelectedSpots();
		
		for (int i = 0; i < spots.size(); i++){
			 //to get current spot
			currentSpot =(TSpot) spots.elementAt(i); //to get current spot
			int x1 = (int)Math.rint(currentSpot.getXCenter()/alignment.getModel().getPixelWidth()) - windowSize/2;
			int y1 = (int)Math.rint(currentSpot.getYCenter()/alignment.getModel().getPixelHeight()) - windowSize/2;
			 ip.setRoi(x1, y1, windowSize , windowSize);
			 ImageProcessor ip2 = ip.crop();
			 ip2.drawDot(windowSize/2, windowSize/2);
			 //ip2.autoThreshold();
			 ImagePlus imp2 = new ImagePlus("crop",ip2);
			 imp2.show();
			 zoom.run("in");
			 zoom.run("in");
			 zoom.run("in");
		}
		
	}
	

}
