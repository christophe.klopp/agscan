import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;

import agscan.TEventHandler;
import agscan.plugins.SAlignPlugin;
import agscan.data.TDataManager;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;
import agscan.menu.action.TAction;
import agscan.data.element.alignment.*;
import agscan.data.model.grid.*;
import agscan.data.element.image.*; 
import agscan.data.model.*;



public class SResetBlockPlugin extends SAlignPlugin{


	
	public SResetBlockPlugin() {
		//TODO externalize
		super("Restore current block",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ALIGNMENT_TYPE); // sets when the menu is active 	
	}
	
	public void run(){
		TAlignment alignment;
		
		
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		
		if (element instanceof TAlignment){
			alignment = getCurrentAlignment();

			//recuperation du bloc selectionné
			TEvent ev1 = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK1_SELECTION, null);
			Vector v = (Vector)(TEventHandler.handleMessage(ev1)[0]);
			
			if(v.size()==0){
				System.out.println("aucun bloc selectionné");
				JOptionPane.showConfirmDialog(null, "No selected bloc", "Error", JOptionPane.OK_OPTION,JOptionPane.ERROR_MESSAGE);
				
			}
			else{
				TGridConfig gc = alignment.getGridModel().getConfig();
				
				double spotDiam = gc.getSpotsDiam();
				double spotH = gc.getSpotsHeight();
				double spotW = gc.getSpotsWidth();
				
				
				
				
				int nb_spot_row = gc.getLev1NbCols();
				int nb_spot_col = gc.getLev1NbRows();
				//System.out.println("nb spot ligne "+nb_spot_row);
				//System.out.println("nb spot colonne "+nb_spot_col);
				for(int i = 0 ; i < v.size(); i++){
					
					
					TGridBlock gb= (TGridBlock)v.get(i);
					Vector spots = gb.getSpots();
					//System.out.println("nb spots "+spots.size());
					double posY = 0.0D;
					for(int j = 0 ; j < nb_spot_col ; j++){
						double posX = 0.0D;
						
						for(int m = 0 ; m < nb_spot_row ; m++){
							
							//System.out.println(j*nb_spot_col+m);
							TSpot curr_spot = (TSpot)spots.get(j*nb_spot_row+m);

							//on remet le diametre
							curr_spot.setComputedDiameter(spotDiam);
							curr_spot.updateDiameterWithComputed();
							

							//rangement a la bonne position
							
							curr_spot.setX(posX);
							curr_spot.setY(posY);


							
							posX = posX + spotW;
						}
						posY = posY + spotH;
					}
				}
			}
		
		
		
		}
		
	}
	
	
	public void actionPerformed(ActionEvent ae){
		this.run();
	}
}
