import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import agscan.FenetrePrincipale;
import agscan.ioxml.TDefaultFileChooser;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TRBCPlotsDialog extends JDialog {
  public TRBCPlotsDialog(String rep, String name) {
    super(FenetrePrincipale.application);
    try {
      jbInit();
      initListeners();
      setModal(true);
      setTitle("RBC Plots");
      setResizable(false);
      dirTextField.setText(rep);
      nameTextField.setText(name);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    this.getContentPane().setLayout(new BorderLayout());
    nameLabel.setText("Nom :");
    dirLabel.setText("R�pertoire :");
    nameTextField.setText("nom");
    dirTextField.setEditable(false);
    dirTextField.setText("repertoire");
    dirButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/openFile.gif")));
    centerPanel.setLayout(new GridBagLayout());
    okButton.setText("OK");
    cancelButton.setText("Annuler");
    checkBoxPanel.setLayout(borderLayout1);
    linearCheckBox.setText("Plots lin�aires");
    logCheckBox.setText("Plots logarithmiques");
    checkBoxPanel.setBorder(
      BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    this.getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);
    this.getContentPane().add(southPanel, java.awt.BorderLayout.SOUTH);
    southPanel.add(okButton);
    southPanel.add(cancelButton);
    centerPanel.add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(dirLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    centerPanel.add(dirButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    centerPanel.add(nameTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 0), 0, 0));
    centerPanel.add(dirTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
    centerPanel.add(checkBoxPanel, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));
    checkBoxPanel.add(logCheckBox, java.awt.BorderLayout.CENTER);
    checkBoxPanel.add(linearCheckBox, java.awt.BorderLayout.NORTH);
    linearCheckBox.setSelected(true);
    logCheckBox.setSelected(true);
  }
  private void initListeners() {
    dirButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Choisir un r�pertoire");
        chooser.setCurrentDirectory(new File(dirTextField.getText()));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          dirTextField.setText(chooser.getSelectedFile().getAbsolutePath());
        }
      }
    });
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        okOut = true;
        dispose();
      }
    });
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        okOut = false;
        dispose();
      }
    });
  }
  public String getName() {
    return nameTextField.getText();
  }
  public String getDir() {
    return dirTextField.getText();
  }
  public boolean getLinear() {
    return linearCheckBox.isSelected();
  }
  public boolean getLog() {
    return logCheckBox.isSelected();
  }
  public boolean getOkOut() {
    return okOut;
  }
  JLabel nameLabel = new JLabel();
  JLabel dirLabel = new JLabel();
  JTextField nameTextField = new JTextField();
  JTextField dirTextField = new JTextField();
  JButton dirButton = new JButton();
  JPanel centerPanel = new JPanel() {
    public Insets getInsets() {
      return new Insets(5, 5, 5, 5);
    }
  };
  JPanel southPanel = new JPanel();
  JButton okButton = new JButton();
  JButton cancelButton = new JButton();
  JPanel checkBoxPanel = new JPanel();
  JCheckBox linearCheckBox = new JCheckBox();
  JCheckBox logCheckBox = new JCheckBox();
  BorderLayout borderLayout1 = new BorderLayout();
  private boolean okOut;
}
