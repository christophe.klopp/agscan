
import plugins.*;

import ij.ImagePlus;
import agscan.plugins.SFormatPlugin;

/**
 * 
 *  Created on 2005
 * 	Modified on August 24, 2005
 * 		allows to save an image in INF/IMG format
 *  Modified on Sep 26, 2005
 * 		creation of the plugin hiearchy: TinfReaderPlugin extends now SImagePlugin
 *  Modified on Dec 14, 2005
 * 		SImagePlugin is renamed SFormatPlugin (be careful an other SImagePlugin exists)
 * 
 * This class allows to open/save an ISAC format image using the ij ISAC plugin
 * @version 26/09/05
 * @author rcathelin
 * @see agscan2.plugins.SFormatPlugin
 * 
 */

public class TInfReaderPlugin extends SFormatPlugin {
	
////	public static int id = -1;
		
	public TInfReaderPlugin() {
		super("ISAC_READER", null); //SFormatPlugin => TPlugin => TAction
		this.extension = "inf";
		id = id0;//creation of the next id
		}
	
	/**
	 * @return the action ID associated of the plugin
	 */
/*	public static int getID() {
		return id;
	}
	cf le getMenuId de la classe Tplugin
	*/
	/**
	 * Create an ImagePlus with a path
	 * @return ImagePlus associated with the image path
	 * @param path String absolute path of the image file
	 */
	public ImagePlus getImagePlus(String path) {
		System.out.println("image path= "+path); 
		ImagePlus imagePlus = ISAC_Reader.getImagePlus(path);
			return imagePlus;
	}
	
	/**
	 *
	 * @return String the extension of the image this plugin can open
	 */
	public String getExtension() {
		return extension;
	}
	
	/**
	 *save an ImagePlus in a path in this format of image
	 * @return true if all is OK
	 * @param path String absolute path of the image file
	 */
	public boolean saveImagePlus(ImagePlus ip, String path){
		return ISAC_Writer.saveAsIMG(ip,path); 	
	}
	
}
