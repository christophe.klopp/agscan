
import javax.swing.JFrame;
import javax.swing.JTextArea;



public class Resultats
{
	
	private JTextArea textArea1 = new JTextArea(12,30);	
	public JFrame frame = new JFrame();	
	
	public Resultats()	{
		
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
		textArea1.setVisible(true);
		textArea1.setText("");	
		textArea1.setLineWrap(true);
		textArea1.setEditable(false);
		frame.getContentPane().add(textArea1);		
		frame.pack();
		frame.setResizable(false);
	}
	
	public void montrerRes() { 
		frame.setVisible(true);		
	}
	
	public void cacherRes() { 
		frame.setVisible(false);		
	}
	
	public void setResultat(int V, int R, int J, int SNF, int SNC, int SNCIR, int SMP, int SV) { 
		textArea1.setEditable(true);
		textArea1.setText("");
		textArea1.append("----------- NOMBRE DE SPOTS FIABLES -----------" + "\n");		
		textArea1.append("VERT : " + V + "\n");
		textArea1.append("ROUGE : " + R + "\n");
		textArea1.append("JAUNE : " + J + "\n");
		textArea1.append("--------- NOMBRE DE SPOTS NON FIABLES ---------" + "\n");
		textArea1.append("SPOTS NON FIABLES : " + SNF + "\n");
		textArea1.append("AVEC : " + "\n");
		textArea1.append("SPOTS NON CENTRES : " + SNC + "\n");
		textArea1.append("SPOTS NON CIRCULAIRES : " + SNCIR + "\n");
		textArea1.append("SPOTS MULTI PARTICULES : " + SMP + "\n");
		textArea1.append("------------ NOMBRE DE SPOTS VIDES ------------" + "\n");
		textArea1.append("SPOTS VIDES : " + SV + "\n");
		textArea1.setEditable(false);
	}
	
	
}