/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribue
 * @version 1.0
 */

package agscan;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.dialog.TDialogManager;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TStatusBarAction;
import agscan.plugins.TPluginManager;
import agscan.statusbar.TStatusBar;

public class FenetrePrincipale extends JFrame implements TBZMessageListener {
	public static final int HIDE = 1;
	public static final int SHOW = 2;
	public static final int QUIT = 3;
	public static final int SHOW_STATUS_BAR = 4;
	public static String title = Messages.getString("FenetrePrincipale.0"); //$NON-NLS-1$
	
	private JPanel contentPane;
	private boolean zoomMode = false;
	
	private TPluginManager pluginManager;
	private TDataManager dataManager;
	private TMainPane mainPane;
	private TEventHandler eventHandler;
	private TMenuManager menuManager;
	private TDialogManager dialogManager;
	private TStatusBar statusBar;

	public static FenetrePrincipale application;
	
	public FenetrePrincipale() {
		application = this;
		this.setIconImage(new ImageIcon(getClass().getResource("ressources/bzscan.gif")).getImage()); //$NON-NLS-1$
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			menuManager = new TMenuManager();
			dataManager = new TDataManager();
			mainPane = new TMainPane();
			dialogManager = new TDialogManager();
			statusBar = new TStatusBar();
			eventHandler = new TEventHandler(this, mainPane, dialogManager, dataManager, menuManager, statusBar);
			//   pluginManager = new TPluginManager();//init des plugins
			menuManager.initActions(this);
			menuManager.init(null);

			jbInit();
			TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TStatusBarAction.getID()),
					new Boolean(true));// pour selectionner l'option status bar dans le menu
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.BZSCAN, FenetrePrincipale.SHOW_STATUS_BAR, null, new Boolean(true));
			TEventHandler.handleMessage(event);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
				TEvent event = new TEvent(TEventHandler.BZSCAN, QUIT, null);
				BZMessageResponse(event);
			}
		});
	}
	private void jbInit() throws Exception {
		contentPane = (JPanel)this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		setSize(new Dimension(640, 480));
		setTitle(title);
		setJMenuBar(menuManager.getMenuBar());
		contentPane.add(menuManager.getToolBar(), BorderLayout.NORTH);
		contentPane.add(mainPane, BorderLayout.CENTER);
		contentPane.add(statusBar.getStatusBarPanel(), BorderLayout.SOUTH);
		statusBar.getStatusBarPanel().setVisible(false);
	}
	public boolean getZoomMode() {
		return zoomMode;
	}
	public Object[] BZMessageResponse(TEvent event) {
		int action = event.getAction();
		Object[] ret = null;
		switch (action) {
		case HIDE:
			hide();
			break;
		case SHOW:
			show();
			break;
		case QUIT:
			TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.CLOSE_ALL, null);
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
			TDataElement elem = (TDataElement)TEventHandler.handleMessage(ev)[0];
			if (elem == null) {
				// BEFORE TO QUIT, SAVE OF PARAMETERS FILE 
				//sauvegarde du fichier de parametres
				// On n'enregiste que si les properties ont subies des modifications
				
				// verif des differences
				// d'abord recuperer les prop de base
				Properties prop_before = new Properties();
				try {
					
					FileInputStream in = new FileInputStream("parameters.properties");
					prop_before.load(in);
					in.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
				// comparer prop et prop_before
				//si different on sauvegarde le nouveau fichier de parametres
				if (AGScan.prop.equals(prop_before)) System.out.println("SAVE");
				else{
					System.out.println("DIFF");
					//	comparaison prop aux proprietes par defaut
					//System.out.println(AGScan.prop.toString());					
					for (Enumeration e = AGScan.prop.keys() ; e.hasMoreElements() ;) {
						String currentKey = (String)e.nextElement();
						System.out.println("currentKey="+currentKey);
						String currentValue = AGScan.prop.getProperty(currentKey);
						System.out.println("currentValue="+currentValue);
						System.out.println("defaultValue="+AGScan.defaultProp.getProperty(currentKey));
						// si egalite prop et default, on elimine dans prop ce champ
						if (currentValue.equals(AGScan.defaultProp.getProperty(currentKey))){
							System.out.println("IDEM defaults!!!");
							AGScan.prop.remove(currentKey);
						}
					}
					// sauvegarde du fichier de config avec les modifs
					try {
						FileOutputStream out = new FileOutputStream("parameters.properties"); //$NON-NLS-1$
						AGScan.prop.store(out,Messages.getString("AGScan.7")); //$NON-NLS-1$
						out.close();
					} catch (FileNotFoundException e2) {
						e2.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				// pour l'instant enregistre les differences par rapport a celui qui est charge au depart...
				//TODO ne sauvegarder que les modifs par rapport a une reference
				// exemple: si on modifie 2 fois les valeurs on peut se retrouver avec dans le fichier des valeurs =  celles
				// par defaut donc il faut comparer les valeurs avec celles du Default et effacer celles en commun...
				// on peut pour resoudre ca virer les lignes du fichiers egales aux valeurs de Default
				
				System.exit(0);
			}
			break;
		case SHOW_STATUS_BAR:
			boolean b = ((Boolean)event.getParam(1)).booleanValue();
			statusBar.getStatusBarPanel().setVisible(b);
			break;
		}
		return ret;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
