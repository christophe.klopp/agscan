package agscan.factory;

import ij.ImageStack;

import java.awt.Rectangle;

import agscan.data.model.TImageModel;
import agscan.data.model.TModelImpl;

public abstract class TImageModelFactory implements TModelFactory {
	protected String fileFormat, unit, ipType;
	protected double pixelWidth, pixelHeight, sensitivity, latitude;
	//protected int nBits, width, height;
	
	public TImageModelFactory(String ff) {
		fileFormat = ff;//extension
		unit = "";
		pixelWidth = 0.0D; pixelHeight = 0.0D;
		//sensitivity =0.0D;
		latitude = 0.0D;
		//nBits = width = height = 0;
		ipType = "20*25";
	}
	//public abstract TImageModel createImageModel(ImagePlus ip);//modif 2005/10/12
	//public abstract TImageModel createImageModel(Vector imagesVector);//modif 2005/10/14
	public abstract TImageModel createImageModel(ImageStack images);
	
	public static TImageModelFactory getImageModelFactory(String fileFormat) {
		TImageModelFactory timf = new TStandardImageModelFactory(fileFormat);
		return timf;
	}
	
	//public TImageModel createImageModel(ImagePlus pi, int[] data, int pixelWidth, int pixelHeight, int pixelImageWidth,
	//                                  int nBits, int colorSpaceType, String unit, String ipt, double latitude,
	//                                String fileFormat, Rectangle cropRect) {
//	modif 2005/10/11 => passage en arg d'un vecteur d'images au lieu d'une seule image
//	modif 2005/10/14 => modif vecteur => imageStack
	public TImageModel createImageModel(ImageStack stack, int[] data, int pixelWidth, int pixelHeight, int pixelImageWidth,
			int nBits, String unit, String ipt,double latitude, String fileFormat, Rectangle cropRect) {
		
		//int w = pi.getWidth();
		//int w = ((ImagePlus)(imagesVector.get(0))).getWidth();//modif 2005/10/11
		int w = stack.getWidth();//modif 2005/10/14
		//int h = pi.getHeight();
		//int h = ((ImagePlus)(imagesVector.get(0))).getHeight();//modif 2005/10/11
		int h = stack.getHeight();//modif 2005/10/14
		int[] _data = new int[w * h];
		int i = 0;
		
		for (int y = cropRect.y; y < (cropRect.y + cropRect.height); y++)
			for (int x = cropRect.x; x < (cropRect.x + cropRect.width); x++)
				_data[i++] = data[y * pixelImageWidth + x];
		return new TImageModel(stack, pixelWidth, pixelHeight, nBits, unit, ipt, latitude, fileFormat);
	}
	
	//public TModelImpl createModel(Object param) {
	public TModelImpl createModel(Object param) {//modif 2005/10/12
		//return createImageModel((ImagePlus)param);
		//return createImageModel((Vector)param);//modif 2005/10/12
		return createImageModel((ImageStack)param);//modif 2005/10/14
	}
	
	// ajout des commentaires des m�thodes ci-dessous : 02/09/05
	
	/*
	 * convert a PSL intensity value into a QL intensity value
	 * @return the intensity converted into QL unit
	 * @param psl psl intensity value
	 * @param range range of intensities (for example 65535 for 16bits images) 
	 * @param lat latitude (dynamic range of the scanner used)
	 */
	public static double psl2ql(double psl, double range, double lat) {
	    if (psl == 0) return 0;//exception: if PSL=0 then QL defines as QL=0
	    //return (Math.log(psl * Math.pow(10.0D, lat / 2.0D) / range) / Math.log(10) / lat + 0.5D) * range;//valeur avec mise a l'echelle
	    return range * (Math.log(psl) / lat + 0.5D);//valeur sans mise à l'echelle
	}
	
	
	/*
	 * convert a QL intensity value into a PSL intensity value
	 * @return the intensity converted into PSL unit
	 * @param ql ql intensity value
	 * @param range range of intensities (for example 65535 for 16bits images) 
	 * @param lat latitude (dynamic range of the scanner used)
	 */
	public static double ql2psl(double ql, double range, double lat) {
		if (ql == 0) return 0;//exception: if QL=0 then PSL defines as PSL=0
		//return Math.pow(10.0D, lat * (ql / range - 0.5D)) * range / Math.pow(10.0D, lat / 2.0D);//valeur avec mise a l'echelle
		return Math.pow(10.0D, lat * (ql / range - 0.5D));//valeur sans mise à l'echelle
	}
	
	/*
	 * convert a PSL intensity value into a QL intensity value
	 * @return the intensity converted into QL unit
	 * @param psl psl intensity value
	 * @param maxValue max value of intensity  
	 * @param lat latitude (dynamic range of the scanner used)
	 */
	//TODO comprendre ce qu'est exactement maxValue ici et le mettre dans la javadoc 
	public static double psl2ql(int psl, int maxValue, int lat) {
		return psl2ql((double)psl, (double)maxValue, (double)lat);
	}
	
	/*
	 * convert a QL intensity value into a PSL intensity value
	 * @return the intensity converted into PSL unit
	 * @param ql ql intensity value
	 * @param maxValue max value of intensity  
	 * @param lat latitude (dynamic range of the scanner used)
	 */ 
	//TODO comprendre ce qu'est exactement maxValue ici et le mettre dans la javadoc
	public static double ql2psl(int ql, int maxValue, int lat) {
		return ql2psl((double)ql, (double)maxValue, (double)lat);
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
