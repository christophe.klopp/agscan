package agscan.factory;

/**
* Modification : 2005/10/26: suppression of TImageInfoDialog call and creation of 
* the image model thanks to current props 
* @version 2005/10/26

*/

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import agscan.AGScan;
import agscan.data.model.TImageModel;

public class TStandardImageModelFactory extends TImageModelFactory {
	public TStandardImageModelFactory(String ff) {
		super(ff);
	}
	//modif remi le constructeur ne prend plus une imagePlus mais un vecteur de SImagePlus 2005/10/11
	//public TImageModel createImageModel(ImagePlus ip) {
	//modif remi le constructeur ne prend plus un un vecteur de SImagePlus mais unh ImageStack 2005/10/14
	//	public TImageModel createImageModel(Vector imagesVector) {
	public TImageModel createImageModel(ImageStack imagesStack) {
		TImageModel model = null;
		//ImagePlus ip = (ImagePlus)imagesVector.get(0);//2005/10/14
		ImageProcessor iproc = imagesStack.getProcessor(1);
		ImagePlus ip1 = new ImagePlus(imagesStack.getSliceLabel(1),iproc);
		int imageType = ip1.getType();// Returns the current image type (ImagePlus.GRAY8, ImagePlus.GRAY16, ImagePlus.GRAY32, ImagePlus.COLOR_256 or ImagePlus.COLOR_RGB).
		int pixelSize = ip1.getBitDepth();// le nb de bits
		//ATTENTION travailler maintenant avec le vecteur imagesVector
		
		// TODO en dessous conditions a modifier: c'est fait , a rendre propre maintenant
		//if (((pixelSize == 24) && (imageType == ImagePlus.COLOR_RGB)) ||//n'existe plus puisqu'on filtre si c'est du RGB
		//		(((pixelSize == 8) || (pixelSize == 16)) && ((imageType == ImagePlus.GRAY16) || (imageType == ImagePlus.GRAY8)|| (imageType == ImagePlus.GRAY32))))// on filtre aussi si c'est 8 bits
		//	{
		//TODO virer cette boite de dialogues dans les parametrages
		// ne l'ouvrir qu'une fois car elle concerne toutes les images
		//boite de dialogue
		
		//TImageInfoDialog dialog = new TImageInfoDialog();
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
		//dialog.setVisible(true);
		//modif 2005/10/26 mis ailleurs les lignes du dessus
		
		
		short[] dada = (short[]) iproc.getPixels();//fait maison...ne marche que pour du 16 bits (car short)
		//Arrays.sort(dada);
		//System.out.println("dadaMax="+dada[dada.length-1]);
		//System.out.println("dadaMin="+dada[0]);
		//if (!dialog.isCancelled()) {	//modif 2005/10/26
		System.err.print(" Doing 8bits conversion ..."); //$NON-NLS-1$
		//int nbPixels = ip.getWidth() * ip.getHeight();//2005/10/14
		int nbPixels = imagesStack.getWidth() * imagesStack.getHeight();
		byte[] dataImage = new byte[nbPixels];// pour stocker les valeurs de l'image en 8 bits
		// ip.show();
		//System.out.println("processor H="+iproc.getHeight()); //$NON-NLS-1$
		//System.out.println("processor W="+iproc.getWidth()); //$NON-NLS-1$
		try{
			
						
			//conversion en 8 bits
			if (pixelSize == 16) {// normalement que 16 bits de toute facon
				int max = 0;
				for (int i = 0; i < dada.length; i++) if (max < dada[i]) max = dada[i];
				for (int i = 0; i < dada.length; i++)
					if (max > 255)
						dataImage[i] = (byte)(dada[i] / 256);
					else
						dataImage[i] = (byte)dada[i];
			}
		}
		catch(ClassCastException e)		{
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
			
		};
		
		//int[] dada = bImage.getData().getPixels(0, 0, ip.getWidth(), ip.getHeight(), (int[])null);
		/*
		 * plus utile mais a garder la conversion RGB => 8 bits
		 if ((imageType == ColorSpace.TYPE_RGB) && (pixelSize == 24))
		 for (int i = 0; i < dada.length; i += 3)
		 dataImage[i / 3] = (byte)((dada[i] + dada[i + 1] + dada[i + 2]) / 3);
		 else if (imageType == ColorSpace.TYPE_GRAY)
		 if (pixelSize == 8)
		 for (int i = 0; i < dada.length; i++) dataImage[i] = (byte)dada[i];
		 else if (pixelSize == 16) {
		 int max = 0;
		 for (int i = 0; i < dada.length; i++) if (max < dada[i]) max = dada[i];
		 for (int i = 0; i < dada.length; i++)
		 if (max > 255)
		 dataImage[i] = (byte)(dada[i] / 256);
		 else
		 dataImage[i] = (byte)dada[i];
		 }
		 */
		
		
		//creation de l'image en 8 bits
		DataBufferByte dbb = new DataBufferByte(dataImage, nbPixels);
		BufferedImage bi = new BufferedImage(iproc.getWidth(), iproc.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster wr = Raster.createWritableRaster(bi.getSampleModel(), dbb, new Point(0, 0));
		bi.setData(wr);
			
		
		// plus utile mais a GARDER la conversion 8 bits => 16 bits
		//BufferedImage bi2 = null;
		/*if (pixelSize != 16) {
		 System.err.print(" Doing 16bits conversion ...");
		 short[] data_16 = new short[nbPixels];
		 for (int i = 0; i < dataImage.length; i++) data_16[i] = (short)((dataImage[i] << 8) & 0xFFFF);
		 DataBufferUShort dbus = new DataBufferUShort(data_16, nbPixels);
		 bi2 = new BufferedImage(bImage.getWidth(), bImage.getHeight(), BufferedImage.TYPE_USHORT_GRAY);
		 wr = Raster.createWritableRaster(bi2.getSampleModel(), dbus, new Point(0, 0));
		 bi2.setData(wr);
		 System.err.println("  done.");
		 }
		 else
		 */
		//bi2 = bImage;
		// creation de l'image en 16 bits
		
		// debut mis en commentaire le 20/09/05
		//	short[] data_16 = new short[nbPixels];
		//	DataBufferUShort dbus = new DataBufferUShort(data_16, nbPixels);
		//	bi2 =  new BufferedImage(ip.getWidth(), ip.getHeight(), BufferedImage.TYPE_USHORT_GRAY);
		//wr = Raster.createWritableRaster(bi2.getSampleModel(), dbus, new Point(0, 0));
		//bi2.setData(wr);
		// fin mis en commentaire le 20/09/05				
		
		System.err.print(" Getting image data ..."); //$NON-NLS-1$
		int[] data = new int[dada.length];
		for (int i=0;i<dada.length;i++){
			//	data[i]=(int)dada[i];commentaire 20/09/05 
			data[i]=(dada[i]+65535)%65535;;	
		}
		
		for (int i = 1646988; i < 1646998; i++) {
			//System.out.println("data["+i+"]="+data[i]);
			//System.out.println("mod_data["+i+"]="+(data[i]+65536)%65536);
			//double test = TImageModelFactory.ql2psl(data[i],5,65535);
			//System.out.println("PSL="+test);
		}
		
		//test 29/08/05
		int maxDada = -999999999;
		int minDada = 10000000;
		int maxData = -999999999;
		int minData = 10000000;
		/*
		System.out.println("Avant voila le dada et le data:");
		for (int j=0;j<dada.length;j++){
			//	if (j%pImage.getWidth() == 0){
			if (dada[j]>=maxDada) {maxDada = dada[j];}
			if (data[j]>=maxData) {maxData = data[j];}
			if (dada[j]<=minDada) {minDada = dada[j];}
			if (data[j]<=minData) {minData = data[j];}//System.out.println("*"+j);}
			/*	
			 if (j%4 == 0){
			 System.out.println();	
			 }
			 else System.out.print(dada[j]+"="+data[j] +" - ");
			 
		}
		System.out.println("maxDada="+maxDada);
		System.out.println("maxData="+maxData);
		System.out.println("minDada="+minDada);
		System.out.println("minData="+minData);
		System.out.println("fin des données dada/data");
		// fin test 01/09/05
		*/
		
		/*
		 int[] data = bi2.getData().getPixels(0, 0, ip.getWidth(), ip.getHeight(), (int[])null);
		 for (int i = 0; i < data.length; i++) 
		 {
		 data[i] = ~data[i] & 0xFFFF;
		 
		 }
		 */
		
		System.err.println("  done."); //$NON-NLS-1$
		
		
		
		//test 01/09/05
		/* on vire la conversion pour voir...
		 if (dialog.getUnit().equals("PSL")) { //$NON-NLS-1$
		 System.err.print(" Converting PSL to QL ..."); //$NON-NLS-1$
		 for (int i = 0; i < data.length; i++)
		 data[i] = (int)psl2ql(data[i] & 0xFFFF, 65535, (int) dialog.getLatitude()) + ((data[i] << 16) & 0xFFFF0000);
		 System.err.println("  done."); //$NON-NLS-1$
		 }
		 else {
		 System.err.print(" Converting QL to PSL ..."); //$NON-NLS-1$
		 for (int i = 0; i < data.length; i++)
		 data[i] = data[i] + (((int)ql2psl(data[i] & 0xFFFF, 65535, (int) dialog.getLatitude()) << 16) & 0xFFFF0000);
		 System.err.println("  done."); //$NON-NLS-1$
		 }
		 
		 */
		
		/*
		 model = new TImageModel(ip, data, dialog.getPixelWidth(), dialog.getPixelHeight(),
		 pixelSize, ip.getType(), dialog.getUnit(), dialog.getIPType(),
		 dialog.getLatitude(), fileFormat);
		 */
		
		
		// modif 05/10/12
		// model = new TImageModel(ip, dialog.getPixelWidth(), dialog.getPixelHeight(),
		// 		pixelSize, dialog.getUnit(), dialog.getIPType(),
		// 		dialog.getLatitude(), fileFormat);
		
		//modif 2005/10/26
		/*	model = new TImageModel(imagesStack, dialog.getPixelWidth(), dialog.getPixelHeight(),
		 pixelSize, dialog.getUnit(), dialog.getIPType(),
		 dialog.getLatitude(), fileFormat);
		 */
		model = new TImageModel(imagesStack,
				AGScan.prop.getDoubleProperty("TImageInfoDialog.pixelWidth"),
				AGScan.prop.getDoubleProperty("TImageInfoDialog.pixelHeight"),
				pixelSize,
				AGScan.prop.getProperty("TImageInfoDialog.unit"),
				AGScan.prop.getProperty("TImageInfoDialog.ipType"),
				AGScan.prop.getDoubleProperty("TImageInfoDialog.latitude"),
				fileFormat);
		
		
		//		}
		//	}
		//	else
		//TODO recuperer le nom de l'image?
		//		JOptionPane.showMessageDialog(null, Messages.getString("TStandardImageModelFactory.14")/* + filename +*/+ //$NON-NLS-1$
		//				Messages.getString("TStandardImageModelFactory.15"), //$NON-NLS-1$
		//				Messages.getString("TStandardImageModelFactory.16"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
		
		
		
		return model;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
