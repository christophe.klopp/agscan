package agscan.factory;

import ij.ImagePlus;
import agscan.data.model.TImageModel;

public abstract class TRawImageModelFactory extends TImageModelFactory {
  public TRawImageModelFactory(String ff) {
    super(ff);
  }
  protected abstract boolean readParams(ImagePlus ip);
  public abstract TImageModel createImageModel(ImagePlus ip) ;
  //TODO coordonner la barre de progression à l'ouverture des images
    /*TImageModel model = null;
    ParameterBlock pb = new ParameterBlock();
    BasDecodeParam param = new BasDecodeParam("filename");
    //pb.add(filename);
    //pb.add(param);

    System.err.print("Lecture de l'image ...");
    JAI.create("fileload", pb).getAsBufferedImage();
    System.err.println(" done.");

    if (readParams(filename)) {
      if ((nBits == 8) || (nBits == 16)) {
        if (RawImageDecoder.getLastDecodedImage() != null) {
          BufferedImage bImage = RawImageDecoder.getLastDecodedImage();
          int[] dada = bImage.getData().getPixels(0, 0, bImage.getWidth(), bImage.getHeight(), (int[])null);
          int nbPixels = bImage.getWidth() * bImage.getHeight();
          BufferedImage bi = null;
          if (nBits == 16) {
            System.err.print(" Starting 8bits conversion ...");
            byte[] data_8 = new byte[nbPixels];
            for (int i = 0; i < dada.length; i++) data_8[i] = (byte)((dada[i] >> 8) & 0xFFFF);
            DataBufferByte dbb = new DataBufferByte(data_8, nbPixels);
            bi = new BufferedImage(bImage.getWidth(), bImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
            WritableRaster wr = Raster.createWritableRaster(bi.getSampleModel(), dbb, new Point(0, 0));
            wr = Raster.createWritableRaster(bi.getSampleModel(), dbb, new Point(0, 0));
            bi.setData(wr);
            System.err.println("  done.");
          }
          else
            bi = bImage;
          TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(80));
          TEventHandler.handleMessage(event);
          int[] data = new int[width * height];
          BufferedImage bi2 = null;
          if (nBits == 8) {
            System.err.print(" Doing 16bits conversion ...");
            short[] data_16 = new short[nbPixels];
            for (int i = 0; i < dada.length; i++) data_16[i] = (short)((dada[i] << 8) & 0xFFFF);
            DataBufferUShort dbus = new DataBufferUShort(data_16, nbPixels);
            bi2 = new BufferedImage(bImage.getWidth(), bImage.getHeight(), BufferedImage.TYPE_USHORT_GRAY);
            WritableRaster wr = Raster.createWritableRaster(bi2.getSampleModel(), dbus, new Point(0, 0));
            wr = Raster.createWritableRaster(bi2.getSampleModel(), dbus, new Point(0, 0));
            bi2.setData(wr);
            System.err.println("  done.");
          }
          else
            bi2 = bImage;
          event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(90));
          TEventHandler.handleMessage(event);
          System.err.print(" Getting image data ...");
          bi2.getData().getPixels(0, 0, width, height, data);
          System.err.println("  done.");
          event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(100));
          TEventHandler.handleMessage(event);

          if (unit.equals("PSL")) {
            System.err.print(" Converting PSL to QL ...");
            for (int i = 0; i < data.length; i++)
              data[i] = (int)psl2ql(~data[i] & 0xFFFF, 65536, (int)latitude) + ((~data[i] << 16) & 0xFFFF0000);
            System.err.println("  done.");
          }
          else {
            System.err.print(" Converting QL to PSL ...");
            for (int i = 0; i < data.length; i++)
              data[i] = ~data[i] & 0xFFFF + (((int)ql2psl(~data[i] & 0xFFFF, 65536, (int)latitude) << 16) & 0xFFFF0000);
            System.err.print(" " + latitude + " " + (data[0] & 0xFFFF) + " " + ((data[0] >> 16) & 0xFFFF));
            System.err.println("  done.");
          }
          model = new TImageModel(PlanarImage.wrapRenderedImage(bi), data, pixelWidth, pixelHeight, nBits,
                                  bImage.getColorModel().getColorSpace().getType(), unit, ipType, latitude, sensitivity, fileFormat);
        }
      }
      else
        JOptionPane.showMessageDialog(null, "<html><P>Le fichier <font color=red><b>" + filename +
                                    "</b></font> ne contient pas d'image<br>pouvant �tre ouverte avec ce logiciel.</P></html>",
                                    "Erreur", JOptionPane.WARNING_MESSAGE);

    }
    else
      JOptionPane.showMessageDialog(null, "<html><P>Le fichier <font color=red><b>" + filename +
                                    "</b></font> ne contient pas d'image<br>pouvant �tre ouverte avec ce logiciel.</P></html>",
                                    "Erreur", JOptionPane.WARNING_MESSAGE);
                                    
   return model;
  }*/
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
