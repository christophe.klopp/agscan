/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribu�
 * @version 1.0
 */

package agscan;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.UIManager;

import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.plastic.Plastic3DLookAndFeel;


public class AGScan {
	private boolean packFrame = false;
	
	//Construire l'application
	public AGScan() {
		FenetrePrincipale frame = new FenetrePrincipale();
		//Valider les cadres ayant des tailles predefinies
		//Compacter les cadres ayant des infos de taille preferees - ex. depuis leur disposition
		if (packFrame)
			frame.pack();
		else
			frame.validate();
		//Centrer la fenetre
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = frame.getSize();
		if (frameSize.height > screenSize.height) frameSize.height = screenSize.height;
		if (frameSize.width > screenSize.width) frameSize.width = screenSize.width;
		frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
		frame.setVisible(true);
	}
	//Methode main
	public static final DefaultProperties defaultProp = new DefaultProperties();
	public static MyProperties prop = new MyProperties(defaultProp);
	public static Messages messagesLanguage;//added 2005/12/06
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
			UIManager.put("ClassLoader", LookUtils.class.getClassLoader()); //$NON-NLS-1$
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//ImageCodec.registerCodec(new BasCodec());// retiré le 25/08/05
			//ImageCodec.registerCodec(new PcbCodec());// retiré le 25/08/05
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		FileInputStream in;
		
		// initialisation des parametres de l'application
		try {
			
			in = new FileInputStream("parameters.properties"); //$NON-NLS-1$
			prop.load(in);
			prop.chekPaths();//remi added 2005/12/01
			in.close();			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		
		messagesLanguage = new Messages();//initilaization of messages strings of the application according to the language 
		Messages.display();//test 2005/12/05
		
		// launch of AGScan!
		new AGScan();
	
	}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
