package agscan;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import agscan.data.TDataManager;
import agscan.data.element.alignment.TAlignment;
import agscan.data.view.TViewImpl;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.data.view.graphic.TGridGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TRoiInteractor;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.dialog.TDialogManager;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * eee
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */
public class TMainPane extends JPanel implements TBZMessageListener, MouseListener {
  class TabbedPaneChangeListener implements ChangeListener {
    public void stateChanged(ChangeEvent e) {
      int index = tabbedPane.getSelectedIndex();
      if (index >= 0) {
        currentView = (TViewImpl) views.elementAt(index);
        TRoiInteractor.getInstance().reset();
        TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.SET_CURRENT_DATA, null, currentView.getReference());
        TEventHandler.handleMessage(event);
        TEventHandler.handleMessage(new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, currentView.getReference(), null));
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, currentView.getReference()));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
      else {
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    }
  }

  class DesktopPaneChangeListener extends InternalFrameAdapter {
    public void internalFrameActivated(InternalFrameEvent e) {
      JInternalFrame jif = e.getInternalFrame();
      Set set = frames.entrySet();
      Iterator it = set.iterator();
      Map.Entry entry;
      TRoiInteractor.getInstance().reset();
      while (it.hasNext()) {
        entry = (Map.Entry) it.next();
        if (entry.getValue().equals(jif)) {
          currentView = (TViewImpl) entry.getKey();
          TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.SET_CURRENT_DATA, null, currentView.getReference());
          TEventHandler.handleMessage(event);
          TEventHandler.handleMessage(new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, currentView.getReference(), null));
          TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, currentView.getReference()));
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          break;
        }
      }
    }
    public void internalFrameClosing(InternalFrameEvent e) {
      if (frames.entrySet().size() == 1) {
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.REMOVE_CURRENT_DATA, null);
      TEventHandler.handleMessage(event);
    }
    
  }

  public static final int DISPLAY_TABS = 1;
  public static final int DISPLAY_WINDOWS = 2;
  public static final int DISPLAY_FULLSCREEN = 3;
  public static final int ADD_VIEW = 4;
  public static final int REMOVE_VIEW = 5;
  public static final int ZOOM_MODE = 6;
  public static final int GET_ZOOM = 7;
  public static final int CLOSE_ALL = 8;
  public static final int REMOVE_ZOOM_MODE = 9;
  public static final int INC_ZOOM = 10;
  public static final int DEC_ZOOM = 11;
  public static final int GET_GRAPHIC_SCROLL_PANE = 12;
  public static final int GET_GRAPHIC_VIEW_SIZE = 13;
  public static final int SET_ZOOM = 14;
  public static final int CENTERED_ZOOM = 15;
  public static final int VIEWPORT_POSITION = 16;
  public static final int ROI_MODE = 17;//CROP_MODE before 2005/12/19
  public static final int SET_RULES = 18;
  public static final int UPDATE_VIEW_NAME = 19;
  public static final int SELECT_VIEW = 20;
  public static final int REPAINT_VIEW = 21;
  public static final int CHANGE_VIEW = 22;
  public static final int SNAPSHOT = 23;//added 2005/12/01
  public static final int CROP_MODE = 24;
  public static final int GET_NAME_TAB = 25;

  private JTabbedPane tabbedPane;
  private JDesktopPane desktopPane;
  private JFrame fullScreenFrame;
  private TViewImpl currentView;
  private Vector views;
  private Hashtable frames;
  private int displayMode;
  private JComponent currentComponent;
  private TabbedPaneChangeListener tabbedPaneChangeListener;
 
  
  public TMainPane() { 
    tabbedPane = new JTabbedPane();
    desktopPane = new JDesktopPane();
    fullScreenFrame = new JFrame();
    currentView = null;
    views = new Vector();
    frames = new Hashtable(); 
    displayMode = DISPLAY_TABS;
    setLayout(new BorderLayout());
    add(tabbedPane, BorderLayout.CENTER);
    currentComponent = tabbedPane;
    tabbedPaneChangeListener = new TabbedPaneChangeListener();
    tabbedPane.addChangeListener(tabbedPaneChangeListener);


  }
  
  public Object[] BZMessageResponse(TEvent event) {
    int action = event.getAction();
    Object[] ret = null;
    TGraphicPanel gp;
    TViewImpl view;
    Enumeration enume;
    boolean sel;
    TEvent ev;
    switch (action) {
      case DISPLAY_TABS:
        if (displayMode != DISPLAY_TABS) {
          tabbedPane.removeAll();
          if (fullScreenFrame != null) {
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            gd.setFullScreenWindow(null);
            fullScreenFrame.dispose();
            fullScreenFrame = null;
          }
          fullScreenFrame = null;
          TViewImpl v;
          for (enume = views.elements(); enume.hasMoreElements(); ) {
            v = (TViewImpl)enume.nextElement();
            tabbedPane.add(v.getReference().getName(), v.getViewPanel());
          }
          remove(currentComponent);
          add(tabbedPane, BorderLayout.CENTER);
          currentComponent = tabbedPane;
          displayMode = DISPLAY_TABS;
          ev = new TEvent(TEventHandler.BZSCAN, FenetrePrincipale.SHOW, null);
          TEventHandler.handleMessage(ev);
          if (currentView != null) {
            tabbedPane.setSelectedComponent(currentView.getViewPanel());
            if (currentView.getGraphicPanel() != null) // modif integration //14/09/05
            	currentView.getGraphicPanel().removeMouseListener(this);
          }
          
          
          tabbedPane.addChangeListener(tabbedPaneChangeListener);
          tabbedPane.repaint();
          repaint();
        }
        break;
      case DISPLAY_WINDOWS:
        if (displayMode != DISPLAY_WINDOWS) {
          tabbedPane.removeChangeListener(tabbedPaneChangeListener);
          desktopPane.removeAll();
          if (fullScreenFrame != null) {
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            gd.setFullScreenWindow(null);
            fullScreenFrame.dispose();
            fullScreenFrame = null;
          }
          fullScreenFrame = null;
          int i = 0, w = getWidth() / 2, h = getHeight() / 2;
          TViewImpl v;
          JInternalFrame selectedFrame = null;
          frames.clear();
          for (enume = views.elements(); enume.hasMoreElements(); i++) {
            v = (TViewImpl)enume.nextElement();
            JInternalFrame jif = new JInternalFrame(v.getReference().getName(), true);
            jif.setResizable(true);
            jif.setIconifiable(true);
            jif.setClosable(true);
            jif.setMaximizable(true);
            if (v.getViewPanel().getWidth() < (w * 2)) w = v.getViewPanel().getWidth();
            if (v.getViewPanel().getHeight() < (h * 2)) h = v.getViewPanel().getHeight();
            jif.setBounds(20 * (i % 10), 20 * (i % 10), w, h);
            jif.setContentPane(v.getViewPanel());
            jif.addInternalFrameListener(new DesktopPaneChangeListener());
            if (v == currentView) selectedFrame = jif;
            desktopPane.add(jif, JLayeredPane.DEFAULT_LAYER);
            frames.put(v, jif);
            jif.show();
          }
          remove(currentComponent);
          add(desktopPane, BorderLayout.CENTER);
          currentComponent = desktopPane;
          displayMode = DISPLAY_WINDOWS;
          if (selectedFrame != null) {
            try {
              selectedFrame.setSelected(true);
            }
            catch (PropertyVetoException ex) {
              ex.printStackTrace();
            }
          }
          ev = new TEvent(TEventHandler.BZSCAN, FenetrePrincipale.SHOW, null);
          TEventHandler.handleMessage(ev);
        //  if (currentView != null) currentView.getGraphicPanel().removeMouseListener(this);// integration - remplace le 14/09/05
          if ((currentView != null) && (currentView.getGraphicPanel() != null)) currentView.getGraphicPanel().removeMouseListener(this);
          desktopPane.repaint();
          repaint();
        }
        break;
      case DISPLAY_FULLSCREEN:// modifie integration 14/09/05
      	 if (displayMode != DISPLAY_FULLSCREEN) {
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            if (gd.isFullScreenSupported()) {
              ev = new TEvent(TEventHandler.BZSCAN, FenetrePrincipale.HIDE, null);
              TEventHandler.handleMessage(ev);
              JPanel panel;
              fullScreenFrame = new JFrame();
              fullScreenFrame.getContentPane().add(currentView.getViewPanel());
              fullScreenFrame.setUndecorated(true);
              fullScreenFrame.setVisible(true);
              gd.setFullScreenWindow(fullScreenFrame);
              displayMode = DISPLAY_FULLSCREEN;
              currentView.getGraphicPanel().addMouseListener(this);
            }
          }
        break;
      case ADD_VIEW:
        addView((TViewImpl)event.getParam(0));
        break;
      case CHANGE_VIEW:
        changeView((TViewImpl)event.getParam(0));
        break;
      case REMOVE_VIEW:
        removeView((TViewImpl)event.getParam(0));
        break;

      case SNAPSHOT://added 2005/12/01
           	BufferedImage bi = currentView.getGraphicPanel().getSnapshot(10000);//10000 is the dimMax for the image (if the w or h is higher max is used)
      	 ret = new Object[1];
         ret[0] = bi;
        break;
      case ZOOM_MODE:// modifie integration 14/09/05
        enume = views.elements();
        sel = ((Boolean)event.getParam(0)).booleanValue();
        while (enume.hasMoreElements()) {
          gp = ((TViewImpl)enume.nextElement()).getGraphicPanel();
          if (gp != null) {
            if (sel)
              gp.setCurrentInteractor(TGraphicInteractor.ZOOM);
            else {
              gp.removeInteractors();
              if (gp instanceof TGridGraphicPanel)
                gp.setCurrentInteractor(TGraphicInteractor.GRID);
              else if (gp instanceof TAlignmentGraphicPanel)
                if (! ( (TAlignment) gp.getView().getReference()).isAlgoRunning())
                  gp.setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
            }
          }
        }
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, (currentView == null) ? null : currentView.getReference(), null);
        TEventHandler.handleMessage(ev);
        if (currentView.getGraphicPanel() != null) currentView.getGraphicPanel().repaint();
        break;
      case ROI_MODE:
        enume = views.elements();
        sel = ((Boolean)event.getParam(0)).booleanValue();
        while (enume.hasMoreElements()) {
          gp = ((TViewImpl)enume.nextElement()).getGraphicPanel();
          if (gp instanceof TImageGraphicPanel)
            if (sel)
              gp.setCurrentInteractor(TGraphicInteractor.ROI);
            else
              gp.removeInteractors();
        }
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, (currentView == null) ? null : currentView.getReference(), null);
        TEventHandler.handleMessage(ev);
        break;
      case SET_ZOOM:
        currentView.setZoom(((Integer)event.getParam(0)).intValue());
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null);
        TEventHandler.handleMessage(event);
        break;
      case CENTERED_ZOOM:
        currentView.centeredZoom(((Integer)event.getParam(0)).intValue());
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null);
        TEventHandler.handleMessage(event);
        break;
      case GET_ZOOM://modification integration 14/09/05
        ret = new Object[1];
        if (currentView.getGraphicPanel() != null)
          ret[0] = new Integer(currentView.getGraphicPanel().getZoom());
        else
          ret[0] = new Integer(1);
        break;
      case INC_ZOOM:
        currentView.incZoom();
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null);
        TEventHandler.handleMessage(event);
        break;
      case DEC_ZOOM:
        currentView.decZoom();
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null);
        TEventHandler.handleMessage(event);
        break;
      case CLOSE_ALL:
        removeAll();
        break;
      case GET_GRAPHIC_SCROLL_PANE:
        ret = new Object[1];
        ret[0] = currentView.getGraphicScrollPane();
        break;
      case GET_GRAPHIC_VIEW_SIZE:
        ret = new Object[1];
        ret[0] = currentView.getGraphicScrollPane().getPreferredSize();
        break;
      case VIEWPORT_POSITION:
        Rectangle rect = (Rectangle)event.getParam(0);
        Dimension dim = (Dimension)event.getParam(1);
        int x = (int)((double)rect.x * currentView.getGraphicPanel().getPreferredSize().getWidth() / (double)dim.width);
        int y = (int)((double)rect.y * currentView.getGraphicPanel().getPreferredSize().getHeight() / (double)dim.height);
        ChangeListener cl = currentView.getGraphicScrollPane().getViewport().getChangeListeners()[0];
        currentView.getGraphicScrollPane().getViewport().removeChangeListener(cl);
        currentView.getGraphicScrollPane().getViewport().setViewPosition(new Point(x, y));
        currentView.getGraphicScrollPane().getViewport().addChangeListener(cl);
        break;
      case SET_RULES:
        enume = views.elements();
        while (enume.hasMoreElements()) {
          view = (TViewImpl)enume.nextElement();
          view.setRules(((Boolean)event.getParam(0)).booleanValue());
        }
        break;
      case UPDATE_VIEW_NAME:
        switch (displayMode) {
          case DISPLAY_TABS:
            tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), (String)event.getParam(0));
            tabbedPane.setToolTipTextAt(tabbedPane.getSelectedIndex(), (String)event.getParam(1));
            tabbedPane.validate();
            break;
          case DISPLAY_WINDOWS:
            desktopPane.getSelectedFrame().setTitle((String)event.getParam(0));
            desktopPane.validate();
            break;
        }
        break;
      case SELECT_VIEW:
        switch (displayMode) {
          case DISPLAY_TABS:
            tabbedPane.setSelectedComponent(((TViewImpl)event.getParam(0)).getViewPanel());
            tabbedPane.validate();
            break;
          case DISPLAY_WINDOWS:
            try {
              ((JInternalFrame)frames.get((TViewImpl)event.getParam(0))).toFront();
              ((JInternalFrame)frames.get((TViewImpl)event.getParam(0))).setSelected(true);
            }
            catch (PropertyVetoException ex) {
              ex.printStackTrace();
            }
            desktopPane.validate();
            break;
        }
        break;
      case REPAINT_VIEW:
        // TODO NMARY : ajout du if voir si c est necessaire
    	if (currentView.getGraphicPanel()!=null)currentView.getGraphicPanel().repaint();
        currentView.getTablePanel().repaint();
        break;
        
      case GET_NAME_TAB :
    	  Vector name = getNameTab();
    	  ret = new Object[1];
    	  ret[0] = name;
    	  break;  
        
    }
    
    return ret;
  }
  
  public Vector getNameTab(){
	  Vector res = new Vector();
	  if (displayMode == DISPLAY_TABS){
		  for(int i = 0 ; i< tabbedPane.getTabCount() ; i++)
		  res.add(tabbedPane.getTitleAt(i));
	  }else{
		  JInternalFrame jif[] = desktopPane.getAllFrames();
		  for(int i = 0 ; i< jif.length ; i++)
			  res.add(jif[i].getTitle());
	  }
	  System.out.println("getNameTab = "+res.toString());
	  return res;
  }
  
  
  public void addView(TViewImpl view) {
    views.addElement(view);
    currentView = view;
    switch (displayMode) {
      case DISPLAY_TABS:
    	tabbedPane.add(view.getReference().getName(), view.getViewPanel());
    	tabbedPane.setSelectedComponent(view.getViewPanel());
    	tabbedPane.setToolTipTextAt(tabbedPane.getComponentCount() - 1, view.getReference().getPath());
    	tabbedPane.validate();
        break;
      case DISPLAY_WINDOWS:
        int w = getWidth() / 2, h = getHeight() / 2;
        JInternalFrame jif = new JInternalFrame(view.getReference().getName(), true);
        jif.setResizable(true);
        jif.setIconifiable(true);
        jif.setClosable(true);
        jif.setMaximizable(true);
        if (view.getViewPanel().getPreferredSize().width < (w * 2)) w = view.getViewPanel().getPreferredSize().width + 6;
        if (view.getViewPanel().getPreferredSize().height < (h * 2)) h = view.getViewPanel().getPreferredSize().height + 26;
        jif.setBounds(20 * (desktopPane.getAllFrames().length % 10), 20 * (desktopPane.getAllFrames().length % 10), w, h);
        jif.setContentPane(view.getViewPanel());
        jif.addInternalFrameListener(new DesktopPaneChangeListener());
        desktopPane.add(jif, JLayeredPane.DEFAULT_LAYER);
        frames.put(view, jif);
        jif.show();
        desktopPane.setSelectedFrame(jif);
        break;
      case DISPLAY_FULLSCREEN:
        break;
    }
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    boolean iz = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    if (iz && (view.getGraphicPanel() != null)) view.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ZOOM);//integration - modifie le 14/09/05
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ROI, null);
    iz = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    if (iz && (view.getGraphicPanel() != null)) view.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ROI);//integration - modifie le 14/09/05
    ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, view.getReference(), null);
    TEventHandler.handleMessage(ev);
  }
  
  public void changeView(TViewImpl view) {
    int index = views.indexOf(currentView);
    views.removeElement(currentView);
    switch (displayMode) {
      case DISPLAY_TABS:
        tabbedPane.remove(currentView.getViewPanel());
        views.insertElementAt(view, index);
        currentView = view;
        tabbedPane.insertTab(view.getReference().getName(), null, view.getViewPanel(), view.getReference().getPath(), index);
        tabbedPane.setSelectedComponent(view.getViewPanel());
        tabbedPane.validate();
        break;
      case DISPLAY_WINDOWS:
        desktopPane.remove((JInternalFrame)frames.get(currentView));
        frames.remove(currentView);
        views.insertElementAt(view, index);
        currentView = view;
        int w = getWidth() / 2, h = getHeight() / 2;
        JInternalFrame jif = new JInternalFrame(view.getReference().getName(), true);
        jif.setResizable(true);
        jif.setIconifiable(true);
        jif.setClosable(true);
        jif.setMaximizable(true);
        if (view.getViewPanel().getPreferredSize().width < (w * 2)) w = view.getViewPanel().getPreferredSize().width + 6;
        if (view.getViewPanel().getPreferredSize().height < (h * 2)) h = view.getViewPanel().getPreferredSize().height + 26;
        jif.setBounds(20 * (desktopPane.getAllFrames().length % 10), 20 * (desktopPane.getAllFrames().length % 10), w, h);
        jif.setContentPane(view.getViewPanel());
        jif.addInternalFrameListener(new DesktopPaneChangeListener());
        desktopPane.add(jif, JLayeredPane.DEFAULT_LAYER);
        frames.put(view, jif);
        jif.show();
        desktopPane.setSelectedFrame(jif);
        break;
    }
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    boolean iz = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    if (iz) view.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ZOOM);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ROI, null);
    iz = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    if (iz) view.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ROI);
    ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, view.getReference(), null);
    TEventHandler.handleMessage(ev);
  }
  
  public void removeView(TViewImpl view) {
    int index = views.indexOf(view);
    if (index >= 0) {
      views.removeElement(view);
      if (views.size() > 0)
        if (index > 0)
          currentView = (TViewImpl)views.elementAt(index - 1);
        else
          currentView = (TViewImpl)views.elementAt(0);
      else {
        currentView = null;
      }
      switch (displayMode) {
        case DISPLAY_TABS:
          tabbedPane.remove(view.getViewPanel());
          if (currentView != null) {
            tabbedPane.setSelectedComponent(currentView.getViewPanel());
            TRoiInteractor.getInstance().reset();
            TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.SET_CURRENT_DATA, null, currentView.getReference());
            TEventHandler.handleMessage(event);
            TEventHandler.handleMessage(new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, currentView.getReference(), null));
            TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, currentView.getReference()));
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          }
          break;
        case DISPLAY_WINDOWS:
          desktopPane.remove((JInternalFrame)frames.get(view));
          frames.remove(view);
          if (currentView != null) {
            try {
              ((JInternalFrame)frames.get(currentView)).setSelected(true);
            }
            catch (PropertyVetoException e) {
              e.printStackTrace();
            }
          }
          else
            TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null));
          desktopPane.repaint();
          break;
        case DISPLAY_FULLSCREEN:
          break;
      }
      if (currentView != null)
        TEventHandler.handleMessage(new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, currentView.getReference(), null));
      else
        TEventHandler.handleMessage(new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, null, null));
    }
  }
  
  public void removeAll() {
    while (views.size() > 0) removeView((TViewImpl)views.firstElement());
  }
  public TViewImpl getCurrentView() {
    return currentView;
  }
  public int getDisplayMode() {
    return displayMode;
  }
  public void mouseClicked(MouseEvent e) {
  }
  public void mouseEntered(MouseEvent e) {
  }
  public void mouseExited(MouseEvent e) {
  }
  public void mousePressed(MouseEvent e) {
    if (e.getButton() == MouseEvent.BUTTON3) {
      TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_POPUP, currentView.getReference());
      JPopupMenu popupMenu = (JPopupMenu)TEventHandler.handleMessage(event)[0];
      popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
  }
  public void mouseReleased(MouseEvent e) {
  }

public void keyPressed(KeyEvent e) {
	// TODO Auto-generated method stub
	int keyCode = e.getKeyCode();
	System.out.println("Key "+keyCode);
	
}

public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	System.out.println("Key "+e);
}

public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	System.out.println("Key "+e);
}



}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
