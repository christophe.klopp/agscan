/*
 * Created on 18 avr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
/**
 * Interface générique que doivent implémenter tous les plugins.
 * Les classes qui implémentent cette interface devront aussi fournir
 * un constructeur sans paramètre (d'une façon implicite ou non).
 *
 * @author Rémi Cathelin (modifié de la version de Richard Grin (modifié de la version de Michel Buffa))
 * @version 2.0 7/12/02
 */
package agscan.plugins;

public interface Plugin {
    /**
       * @return le nom du plugin
       */
    public String getName();

    /**
     * @return le type du plugin
     */
    public Class getType();
    
}
