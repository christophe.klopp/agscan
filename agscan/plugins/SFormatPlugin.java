/**
 * 
 *  Created on Sep 26, 2005
 * 
 * 
 * This class is the abstract class used to define image format plugins.
 * SImageClass is a skeletton for creating image open/save plugins.
 * Classes that extend this class allow to the application to know others image file format that
 * default one (16bits TIFF images).
 * Images Plugins are called during opening, saving images.
 * @version 26/09/05
 * @author rcathelin
 * @see agscan.plugins.TPlugin
 * 
 */

package agscan.plugins;

import java.awt.event.ActionEvent;

import ij.ImagePlus;

import javax.swing.ImageIcon;



public abstract class SFormatPlugin extends TPlugin{
	
	/**
	 * constructor of the image format plugin.
	 * It calls the generic plugin constructor.
	 * @param title plugin's name
	 * @param icon icon associated - null for an image format plugin
	 */
	public SFormatPlugin(String title, ImageIcon icon) {
		super(title, icon);
		setActiveType(ACTIVE_TYPE); //all time active
		// menuPlugin allows to create or not a menu to this plugin
		// for an image format plugin, we don't need to add a specific menu ( attribute value is false)
		setMenuPlugin(false); 
	}
	
	// associated extension of the image format
	protected String extension;
	
	/**
	 * return the name of the plugin (given to the constructor)
	 */
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * Returns an ImagePlus with a path
	 * @return ImagePlus associated with the image path
	 * @param path String absolute path of the image file
	 */
	public abstract ImagePlus getImagePlus(String path);
	
	
	/**
	 * return the extension associated with this plugin
	 * @return String the extension of the image this plugin can open
	 */
	public abstract String getExtension();
	
	/**
	 *save an ImagePlus in a path in this format of image
	 * @return true if all is OK
	 * @param path String absolute path of the image file
	 */
	public abstract boolean saveImagePlus(ImagePlus ip, String path);
	
	/*
	 * actionPerformed never used because this kind of plugins are not associated with a menu
	 */
	public void actionPerformed(ActionEvent e) {
		System.out.println("nothing to do!");
	}
	
	//méthode déplacée le 28/09/05 de TPluginManager à ici, bien fait?
	// call of the method getImagePlus without the knowledge of its name ( just with the image path and extension)
	public static ImagePlus getImagePlus(String extension,String fileAbsolutePath) {
		ImagePlus ip = null;
		
		//System.out.println("alors="+TPluginManager.extensions.get(extension).toString());
		Integer i1 = (Integer) TPluginManager.extensions.get(extension); //index of the plugin associated with the extension
		
		// we just have one parameter: the path of the image
		Object[] params = new Object[1];
		params[0] = fileAbsolutePath;
		
		// call of the method
		try {
			ip = (ImagePlus) TPluginManager.callPluginMethod(TPluginManager.getPluginInstance(i1.intValue()),
					params, "getImagePlus");
			
			//ip.show();// test d'ouverture de l'image dans une fenetre independante
		} catch (Exception e) {
			System.out.println("erreur du vendredi juste ici la dans le try ca craque!!!");
			System.out.println("SFormatPlugin.getImagePlugin() error :" +
					e.toString());
			System.out.println("cause="+e.getCause());
		}		
		return ip;
	}
	
	
	//méthode déplacée le 28/09/05 de TPluginManager à ici, bien fait?
    //TODO virer cette methode directement dans SFormatPlugin!
    // call of the method getImagePlus without the knowledge of its name ( just with the image path and extension)
    public static void callSaveImagePlus(ImagePlus ip,String fileAbsolutePath) {
    	//on recupere l'extension
    	int i = fileAbsolutePath.lastIndexOf((int)'.');//-1 if no extension
    	String extension = fileAbsolutePath.substring(i+1,fileAbsolutePath.length());
        Integer i1 = (Integer) TPluginManager.extensions.get(extension); //index of the plugin associated with the extension
        // we have 2 parameters: the imagePlus and the path 
        Object[] params = new Object[2];
        params[0] = ip;
        params[1] = fileAbsolutePath;
        // call of the method
        try {
        	TPluginManager.callPluginMethod(TPluginManager.getPluginInstance(i1.intValue()),params, "saveImagePlus");
            
        } catch (Exception e) {
            System.out.println("TPluginManager.saveImagePlus() error :" +
                e.toString());
        }
    }

	
	
}
