/*
 * Created on may,26 2005
 * 
 * This class is the abstract class of plugins.
 * TPlugin is a skeletton for creating plugins.
 * Differents sub-types of plugins are defined 
 * @version 26/09/05
 * @author rcathelin
 * @see agscan.plugins.SFormatPlugin
 * 
 */
package agscan.plugins;

import javax.swing.ImageIcon;

import agscan.menu.action.TAction;


public abstract class TPlugin extends TAction {
    protected boolean menuPlugin = false;
    protected boolean batchPlugin = false;// true if a menu comes with the plugin, by default false
    protected int id = -111;
    
    public TPlugin(String title, ImageIcon icon) {
        super(title, icon); // ==> TAction
        //System.out.println("TAction plug==> id0=" + id0);
        //TODO les plugins ont tous le meme id!
        setActiveType(TAction.INACTIVE_TYPE); //at firt a plugin is inactive
        // creation de l'id suivant est fait dans le constructeur de TAction
    }

    /**
     * @return the name of the plugin that will be displayed  in the menu 
     */
    public String getName() {
        return null;
    }

    /**
     * @return the id of the plugin = the id of the menu where the plugin is
     */
    public  int getId() {
    	
    	return id;//modif 2005/14/12
    }

    /**
     * TEST 205/12/14
     * les id sont utilis� par le vecteur d'actions du TMenuManager
     * Ils sont tous crees au depart pour les menus "normaux"
     * et ensuite pour les plugins sauf que les plugins sont cr��s avant l'affectation
     * de leur menu et donc de la modif des id d'ou tous les plugins se retrouvent 
     * avec le meme id qui est l'id suivant des menus d'avant.
     * Il faut donc mettre a jour id0 a chaque ajout de plugin 
     */
   //// public void setMenuId(int id) {
       //// id0 = id;
   //// }

    /**
    * setMenuPlugin defines if the plugin has a menu or not
    * @param bool true if the plugin will have a menu, false else
    */
    public void setMenuPlugin(boolean bool) {
        menuPlugin = bool;
    }

    /**
     * @return true if the plugin has got a menu, false else
     */
    public boolean isMenuPlugin() {
        return menuPlugin;
    }
    
    /**
     * setMenuPlugin defines if the plugin could be used in batch mode
     * @param bool true if the plugin will have a menu, false else
     */
    public void setBatchPlugin(boolean bool) {
        batchPlugin = bool;
    }

    /**
     * @return true if the plugin could be used in batch mode
     */
    public boolean isBatchPlugin() {
        return batchPlugin;
    }

    /**
     * getType 
     * @return the Class type of the plugin...
     */
    public Class getType() {
        return this.getClass();
    }
    
}
