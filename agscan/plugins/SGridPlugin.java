/**
 * Created on Dec 14, 2005
 *
 * @author rcathelin
 * modified 2006/02/20: addition of several grid manipulation methods
 *
 */
package agscan.plugins;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.event.TEvent;


//TODO voir en quoi different SAlignPlugin et SGridPlugin par rapport aux modifs de grille...
public abstract class SGridPlugin extends TPlugin implements Runnable {
	
	protected String pluginName = null;//name of the plugin
	
	public SGridPlugin(String pluginName,ImageIcon icon) {
		super(pluginName, icon); //de TPlugin => de TAction
		this.pluginName = pluginName;//name of the quantif
		setActiveType(GRID_TYPE); // quand le menu est actif...
		setMenuPlugin(true); // pour dire si on definit un menu associe a ce plugin
	}
	
	/**
	 * return the name of the plugin (given to the constructor)
	 */
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * get the current alignment
	 * @return the alignment or null if the current element isn't an alignment
	 * @version 2006/02/20
	 */
	protected TAlignment getCurrentAlignment() {
		TAlignment alignment = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else System.out.println("PROBLEM!!!!");
		return alignment;
	}	
	
	/**
	 * apply a clockwise rotation to the selected grid (or sub-grid). 
	 * @param angle given in degrees
	 */
	protected void rotateSelection(double angle){
		System.out.println("rotate of: "+angle);
		//conversion degrees=> radians :The angle must be in degrees
		double radian_angle = angle*(Math.PI / 180.0D);
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, null, new Double(radian_angle));
		TEventHandler.handleMessage(ev);
	}
	
	/**
	 * apply an horizontal translation to the selected grid (or sub-grid). 
	 * @param x horizontal translation in micrometer
	 * @param y vertical translation in micrometer
	 */
	
	protected void translateSelection(double x, double y){
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, null,
				new Double(x),new Double(y));
		TEventHandler.handleMessage(ev);
		updateView();//refresh the displayed grid
	}
	/**
	 * apply an horizontal translation to the selected grid (or sub-grid). 
	 * @param x horizontal translation in micrometer
	 */
	protected void translateXSelection(double x){
		translateSelection(x,0.0D);
	}
	/**
	 * apply a vertical translation to the selected grid (or sub-grid). 
	 * @param y vertical translation in micrometer
	 */
	protected void translateYSelection(double y){
		translateSelection(0.0D,y);
	}
	
	/**
	 * This method allows to select the entire grid
	 *
	 */
	protected void selectEntireGrid(){
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, null);
		TEventHandler.handleMessage(ev);
	}
	
	/**
	 * apply a vertical translation to the entire grid. 
	 * @param y vertical translation in micrometer
	 */
	protected void translateYGrid(double y){
		selectEntireGrid();
		translateSelection(0.0D,y);
	}
	/**
	 * apply an horizontal translation to the entire grid. 
	 * @param x horizontal translation in micrometer
	 */
	protected void translateXGrid(double x){
		selectEntireGrid();
		translateSelection(x,0.0D);
	}
	
	/**
	 * apply a translation to the entire grid. 
	 * @param x horizontal translation in micrometer
	 * @param y vertical translation in micrometer
	 */
	
	protected void translateGrid(double x, double y){
		selectEntireGrid();
		translateSelection(x,y);
	}
	/**
	 * call the refresh of the main window
	 *
	 */
	private void updateView(){
		TEvent ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
		TEventHandler.handleMessage(ev);		
	}
	
	
}
