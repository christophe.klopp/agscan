/*
 * Created on Dec 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.plugins;

import java.awt.Color;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.model.grid.TGridConfig;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

//TODO voir en quoi different SAlignPlugin et SGridPlugin par rapport aux modifs de grille...
/**
 * @author rcathelin
 * @since 2005/12/14
 */
public abstract class SAlignPlugin extends TPlugin implements Runnable {
	
	protected String pluginName = null;//name of the plugin
	protected int imagesMin;//number min of images that this plugin needs to perform quantification
	protected int imagesMax;//number max of images that this plugin can accept to perform quantification
	
	public SAlignPlugin(String pluginName,ImageIcon icon) {
		super(pluginName, icon); //de TPlugin => de TAction
		this.pluginName = pluginName;//name of the quantif
		setActiveType(ALIGNMENT_TYPE); // quand le menu est actif...
		setMenuPlugin(true); // pour dire si on definit un menu associe a ce plugin
	}
	/**
	 * For example a alignment for a fluorescent experiment needs a minimum of 2 images
	 * @return the number min of images that this plugin needs to perform alignment
	 * @since 2005/10/19
	 */
	public int getImagesMin() {
		return imagesMin;
	}
	
	/**
	 * 
	 * @param imagesMin the number min of images that this plugin needs to perform alignment
	 * @since 2005/10/19
	 */
	public void setImagesMin(int imagesMin) {
		this.imagesMin = imagesMin;
	}
	
	/**
	 * For example a alignment for a radio experiment needs a maximum of 1 image
	 * @return number max of images that this plugin can accept to perform alignment
	 * @since 2005/10/19
	 */
	public int getImagesMax() {
		return imagesMax;		
	}
	
	/**
	 * 
	 * @param imagesMax  number max of images that this plugin can accept to perform alignment
	 * @since 2005/10/19
	 */
	public void setImagesMax(int imagesMax) {
		this.imagesMax = imagesMax;
	}
	/**
	 * return the name of the plugin (given to the constructor)
	 */
	public String getName() {
		return (String) this.getValue(NAME);
	}
	/**
	 * manages the status bar at the beginning and at the end of the alignment 
	 * and lock/unlock the grid and the table....
	 * @param active true for begin and false for end
	 */
	public void inProgressStatus(boolean active){
		TEvent ev; //= new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		//TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		TAlignment alignment = (TAlignment)getCurrentAlignment();//element;
		
		if (active){//beginning of the process
			alignment.getView().getTablePanel().setSelectable(false);//locks the table
			alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
			alignment.setAlgoRunning(true);//asked by the status-bar
			///	alignment.setName(pluginName + " is running ...");// we replace it by the current name into the status-bar
			///	ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);//lance la status bar avec le nom de l'algo et le % � zero
			///	TEventHandler.handleMessage(ev);		
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,pluginName+" is running", Color.red); //TODO externalize
			TEventHandler.handleMessage(ev);//action_label (bottom left)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, alignment, new Boolean(true));
			TEventHandler.handleMessage(ev);
		}
		else {//end of the process
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, alignment, new Boolean(false));
			TEventHandler.handleMessage(ev);// remove from the status bar (1/2)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false));
			TEventHandler.handleMessage(ev);// remove from the status bar (2/2)
			alignment.getView().getTablePanel().setSelectable(true);
			alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
			alignment.setAlgoRunning(false);
			alignment.setAlgorithm(null);		
			//	alignment.setName(alignmentName);//we give back the real name to the alignment
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);//TODO externalize
			TEventHandler.handleMessage(ev);//action_label (bottom left)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
			TEventHandler.handleMessage(ev);
		}
	}
	
	
	/**
	 * Returns the grid config used by the current alignment 
	 * @return
	 */
	protected TGridConfig getCurrentGridConfig(){
		TAlignment ali = getCurrentAlignment();
		return ali.getGridModel().getConfig();
	}
	
	/**
	 * get the current alignment
	 * @return the alignment or null if the current element isn't an alignment
	 * @version 2006/02/20
	 */
	protected TAlignment getCurrentAlignment() {
		// recuperation de l'alignement 
		TAlignment alignment = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else if (currentElement instanceof TBatch) 
			 {
				alignment = ((TBatch)currentElement).getWorker().getCurrentAlignment();
				if (alignment == null)System.out.println("PROBLEM : alignment == null!!!");
			 }
		else System.out.println("PROBLEM : currentElement == null!!!");
		return alignment;
	}
	/**
	 * apply a clockwise rotation to the selected grid (or sub-grid). 
	 * @param angle given in degrees
	 */
	protected void rotateSelection(double angle){
		System.out.println("rotate of: "+angle);
		//conversion degrees=> radians :The angle must be in degrees
		double radian_angle = angle*(Math.PI / 180.0D);
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, null, new Double(radian_angle));
		TEventHandler.handleMessage(ev);
	}
	
	/**
	 * apply an horizontal translation to the selected grid (or sub-grid). 
	 * @param x horizontal translation in micrometer
	 * @param y vertical translation in micrometer
	 */
	
	protected void translateSelection(double x, double y){
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, null,
				new Double(x),new Double(y));
		TEventHandler.handleMessage(ev);
		updateView();//refresh the displayed grid
	}
	/**
	 * apply an horizontal translation to the selected grid (or sub-grid). 
	 * @param x horizontal translation in micrometer
	 */
	protected void translateXSelection(double x){
		translateSelection(x,0.0D);
	}
	/**
	 * apply a vertical translation to the selected grid (or sub-grid). 
	 * @param y vertical translation in micrometer
	 */
	protected void translateYSelection(double y){
		translateSelection(0.0D,y);
	}
	
	/**
	 * This method allows to select the entire grid
	 *
	 */
	protected void selectEntireGrid(){
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, null);
		TEventHandler.handleMessage(ev);
	}
	
	/**
	 * apply a vertical translation to the entire grid. 
	 * @param y vertical translation in micrometer
	 */
	protected void translateYGrid(double y){
		selectEntireGrid();
		translateSelection(0.0D,y);
	}
	/**
	 * apply an horizontal translation to the entire grid. 
	 * @param x horizontal translation in micrometer
	 */
	protected void translateXGrid(double x){
		selectEntireGrid();
		translateSelection(x,0.0D);
	}
	
	/**
	 * apply a translation to the entire grid. 
	 * @param x horizontal translation in micrometer
	 * @param y vertical translation in micrometer
	 */
	
	protected void translateGrid(double x, double y){
		selectEntireGrid();
		translateSelection(x,y);
	}
	/**
	 * call the refresh of the main window
	 *
	 */
	private void updateView(){
		TEvent ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
		TEventHandler.handleMessage(ev);		
	}
	
	/**
	 * apply a size modification to the entire grid  
	 * @param direction four possibilities : 
	 * TGridPositionControler.RESIZE_NORTH
	 * TGridPositionControler.RESIZE_SOUTH
	 * TGridPositionControler.RESIZE_EAST
	 * TGridPositionControler.RESIZE_WEST
	 * @param value in micrometers
	 */
	protected void resizeGrid(int direction, double value){
		selectEntireGrid();
		resizeSelection(direction,value);		
	}
	/**
	 * apply a size modification to the selection
	 * @param direction four possibilities : 
	 * TGridPositionControler.RESIZE_NORTH,
	 * TGridPositionControler.RESIZE_SOUTH
	 * TGridPositionControler.RESIZE_EAST,
	 * TGridPositionControler.RESIZE_WEST
	 * @param value in micrometers
	 */
	protected void resizeSelection(int direction, double value){
		//conversion into pixels (because the TGridPositionControler uses pixels 
		if ((direction == TGridPositionControler.RESIZE_NORTH) || (direction == TGridPositionControler.RESIZE_SOUTH)) value = value/getCurrentAlignment().getModel().getPixelHeight();
	        else value = value/getCurrentAlignment().getModel().getPixelWidth();		
		
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, direction, null, new Double(value), new Double(getCurrentAlignment().getModel().getPixelWidth()), new Double(getCurrentAlignment().getModel().getPixelHeight()));
		TEventHandler.handleMessage(ev);
		updateView();//refresh the displayed grid	
	}
	
}

