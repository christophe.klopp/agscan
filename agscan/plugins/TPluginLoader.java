/*
 * Created on 18 avr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
/**
 * Charge des classes de plugins dont les fichiers classe sont placés
 * dans des URL donnés, et crée une instance de chaque plugin chargé.
 * Cette classe délègue à des ClassLoader le chargement des classes avant de
 * créer les instances.
 * <P>
 * On peut parcourir les URL pour charger "à chaud" de nouveaux plugins
 * qui y auraient été nouvellement installés (méthode loadPlugins).
 * En ce cas, les anciens plugins ne sont pas rechargés.
 * On peut même récupérer de nouvelles versions des plugins avec
 * les méthodes reloadPlugins.
 * <P>
 * Normalement cette classe est utilisée par un PluginManager mais pas
 * directement par les clients qui veulent charger des plugins.
 *@author Rémi Cathelin (modifié de la version de Richard Grin )
 * @version 1.0
 */
package agscan.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import agscan.menu.TMenuManager;


public class TPluginLoader {
	/**
	 * Le chargeur de classes qui va charger les plugins.
	 */
	private ClassLoader loader;
	
	/**
	 * Le répertoire contenant les plugins.
	 */
	private String pluginDirectory;
	
	/**
	 * Liste des instances des plugins qui ont été chargées par loadPlugins.
	 */
	private List loadedPluginInstances = new ArrayList();
	
	/**
	 * Cree une instance qui va chercher les plugins dans le repertoire dont
	 * on passe le nom en parametre.
	 * @param urls tableau completement rempli par les URLs (pas d'elements null).
	 */
	public TPluginLoader(String directory) throws MalformedURLException {
		// On verifie que l'URL correspond bien à un répertoire.
		System.out.println("DIRECTORY = " + directory);
		
		File dirFile = new File(directory);
		
		if ((dirFile == null) || !dirFile.isDirectory()) {
			System.err.println(directory + " n'est pas un repertoire");
			System.exit(1);
			throw new IllegalArgumentException(directory +
			" n'est pas un repertoire");
		}
		
		// Si c'est un repertoire mais que l'URL ne se termine pas par un "/",
		// on ajoute un "/" a la fin (car URL ClassLoader oblige a donner
		// un URL qui se termine par un "/" pour les repertoires).
		if (!directory.endsWith("/")) {
			directory += "/";
		}
		
		this.pluginDirectory = directory;
		
		// Cree le chargeur de classes.
		createNewClassLoader();
	}
	
	/**
	 * Charge les instances des plugins d'un certain type placés dans le
	 * repertoire indiqué a la création du PluginLoader. Ces instances
	 * sont chargées en plus de celles qui ont déjà été chargées.
	 * Si on ne veut que les instances des plugins qui vont etre chargées
	 * dans cette méthode, et avec les nouvelles versions s'il y en a, il faut
	 * utiliser {@link #reloadPlugins(Class)}
	 * On peut recuperer ces plugins par la methode {@link #getPluginInstances}.
	 * Si un plugin a deja ete charge, il n'est pas
	 * recharge, meme si une nouvelle version est rencontrée.
	 * @param type type des plugins recherchés. Si null, charge les plugins
	 * de tous les types.
	 */
	public void loadPlugins(Class type) {
		// En prévision d'un chargement ailleurs que d'un répertoire, on fait
		// cette indirection. On pourrait ainsi charger d'un jar.
		loadFromDirectory(type);
	}
	
	/**
	 * Charge les instances de tous les plugins.
	 * On peut récupérer ces plugins par la méthode {@link #getPluginInstances}.
	 * Si un plugin a déjà été chargé, il n'est pas
	 * rechargé, méme si une nouvelle version est rencontrée.
	 */
	public void loadPlugins() {
		loadPlugins(null);
	}
	
	/**
	 * Recharge tous les plugins.
	 * Charge les nouvelles versions des plugins s'il les rencontre.
	 */
	public void reloadPlugins() {
		reloadPlugins(null);
	}
	
	/**
	 * Recharge tous les plugins d'un type donné.
	 * Charge  les nouvelles versions des plugins s'il les rencontre.
	 * @param type type des plugins à charger.
	 */
	public void reloadPlugins(Class type) {
		// Crée un nouveau chargeur pour charger les nouvelles versions.
		try {
			createNewClassLoader();
		} catch (MalformedURLException ex) {
			// Ne devrait jamais arriver car si l'URL était mal formée,
			// on n'aurait pu créer "this".
			ex.printStackTrace();
		}
		
		// Et efface tous les plugins du type déjé chargés.
		erasePluginInstances(type);
		
		// Recharge les plugins du type
		loadPlugins(type);
	}
	
	/**
	 * Renvoie les instances de plugins qui ont été récupérées cette fois-ci et
	 * les fois d'avant (si on n'a pas effacé les plugins chargés avant lors de la
	 * dernière recherche de plugins {@link #loadPlugins(boolean)}.
	 * @return les instances récupérées. Le tableau est plein.
	 */
	public TPlugin[] getPluginInstances() {
		return getPluginInstances(null);
	}
	
	public TPlugin[] getPluginInstances(Class type) {
		List loadedPluginInstancesOfThatType = new ArrayList();
		
		for (int i = 0; i < loadedPluginInstances.size(); i++) {
			TPlugin plugin = (TPlugin) loadedPluginInstances.get(i);
			
			if ((type == null) || plugin.getType().equals(type)) {
				loadedPluginInstancesOfThatType.add(plugin);
			}
		}
		
		return (TPlugin[]) loadedPluginInstancesOfThatType.toArray(new TPlugin[0]);
	}
	
	/**
	 * Efface le chargeur de classes.
	 */
	private void eraseClassLoader() {
		loader = null;
	}
	
	/**
	 * Crée un nouveau chargeur. Permettra ensuite de charger de nouvelles
	 * versions des plugins.
	 */
	private void createNewClassLoader() throws MalformedURLException {
		loader = URLClassLoader.newInstance(new URL[] { getURL(pluginDirectory) });
	}
	
	/**
	 * Efface tous le plugins d'un certain type déjé chargés.
	 * @param type type des plugins é effacer. Efface tous les plugins si null.
	 */
	private void erasePluginInstances(Class type) {
		if (type == null) {
			loadedPluginInstances.clear();
		} else {
			for (int i = 0; i < loadedPluginInstances.size(); i++) {
				TPlugin plugin = (TPlugin) loadedPluginInstances.get(i);
				
				if (plugin.getType().equals(type)) {
					loadedPluginInstances.remove(i);
				}
			}
		}
	}
	
	/**
	 * Charge les plugins d'un certain type placés dans un répertoire
	 * qui n'est pas dans un jar.
	 * @param urlBase URL du répertoire de base ; la classe du plugin doit
	 * se trouver sous ce répertoire dans un sous-répertoire qui correspond
	 * au nom de son paquetage.
	 * Exemple d'URL : file:rep/fichier
	 * @param type type des plugins. Charge tous les plugins si <code>null</code>.
	 * @param cl le chargeur de classes qui va charger le plugin.
	 */
	private void loadFromDirectory(Class type) {
		// Pour trouver le nom complet des plugins trouvés : c'est la partie
		// du chemin qui est en plus du répertoire de base donné au loader.
		// Par exemple, si le chemin de base est rep1/rep2, le plugin
		// de nom machin.truc.P1 sera dans rep1/rep2/machin/truc/P1.class
		loadFromSubdirectory(new File(pluginDirectory), type, pluginDirectory);
	}
	
	/**
	 * Charge les plugins placés directement sous un sous-répertoire
	 * d'un répertoire de base. Les 2 répertoires ne sont pas dans un jar.
	 * @param baseName nom du répertoire de base (sert pour avoir le nom
	 * du paquetage des plugins trouvés).
	 * @param dir sous-répertoire. Le nom du paquetage du plugin devra
	 * correspondre à la position relative du sous-répertoire par rapport
	 * au répertoire de base.
	 * @param type type de plugins à charger.
	 * @param urlBase URL de base du chargeur de classes (pour savoir d'où
	 * viennent les instances de plugins trouvées
	 * Charge tous les plugins si <code>null</code>.
	 */
	private void loadFromSubdirectory(File dir, Class type, String baseName) {
		System.out.println("Chargement dans le sous-repertoire " + dir +
				" avec nom de base " + baseName);
		
		int baseNameLength = baseName.length();
		
		// On parcourt toute l'arborescence é la recherche de classes
		// qui pourraient étre des plugins.
		// Quand on l'a trouvé, on en déduit son paquetage avec sa position
		// relativement é l'URL de recherche.
		File[] files = dir.listFiles();
		
		//System.out.println("Le listing : " + files);
		// On trie pour que les plugins apparaissent dans le menu
		// par ordre alphabétique
		//    Arrays.sort(list);
		System.out.println("nb de fichiers dans plugins = " + files.length);
		
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			
			if (file.isDirectory()) {
				loadFromSubdirectory(file, type, baseName);
				
				continue;
			}
			
			// Ce n'est pas un répertoire
			System.out.println("Examen du fichier " + file.getPath() + ";" +
					file.getName());
			
			String path = file.getPath();
			String qualifiedClassName = getQualifiedName(baseNameLength, path);
			
			// On obtient une instance de cette classe
			if (qualifiedClassName != null) {
				TPlugin plugin = (TPlugin) getInstance(qualifiedClassName, type);
				
				//Plugin plugin = getInstance(path, type);//essai remi le 20.04.05
				if (plugin != null) {
					System.out.println("Classe " + qualifiedClassName +
					" est bien un plugin !");
					
					// S'il n'y a pas déjà un plugin de la même classe, on ajoute
					// l'instance de plugin que l'on vient de créer.
					boolean alreadyLoaded = false;
					
					for (int j = 0; j < loadedPluginInstances.size(); j++) {
						TPlugin loadedPlugin = (TPlugin) loadedPluginInstances.get(j);
						
						if (loadedPlugin.getClass().equals(plugin.getClass())) {
							alreadyLoaded = true;
							
							break;
						}
					}
					
					if (!alreadyLoaded) {
						loadedPluginInstances.add(plugin);
					}
				}
			}
		} // for
	}
	
	/**
	 * Dans le cas où un chemin correspond à un fichier .class,
	 * calcule le nom complet d'une classe à partir du nom d'un répertoire
	 * de base et du chemin de la classe, les 2 chemins étant ancré au même
	 * répertoire racine.
	 * Le répertoire de base se termine par "/" (voir classe URLClassLoader).
	 * Par exemple, a/b/c/ (c'est-é-dire 6 pour baseNameLength)
	 * et a/b/c/d/e/F.class donneront d.e.F
	 * @param baseNameLength nombre de caractéres du nom du répertoire de base.
	 * @param classPath chemin de la classe.
	 * @return le nom complet de la classe, ou null si le nom ne correspond
	 * pas é une classe externe.
	 */
	private String getQualifiedName(int baseNameLength, String classPath) {
		//System.out.println("Calcul du nom qualifié de " + classPath + " en enlevant " + baseNameLength + " caractéres au début");
		// Un plugin ne peut étre une classe interne
		if ((!classPath.endsWith(".class")) || (classPath.indexOf('$') != -1)) {
			return null;
		}
		
		// C'est bien une classe externe
		classPath = classPath.substring(baseNameLength).replace(File.separatorChar,
		'.');
		
		// On enléve le .class final pour avoir le nom de la classe
		//System.out.println("Nom complet de la classe : " + classPath);
		return classPath.substring(0, classPath.lastIndexOf('.'));
	}
	
	/**
	 * Transforme le nom du répertoire en URL si le client n'a pas donné
	 * un bon format pour l'URL (pour pouvoir créer un URLClassLoader).
	 * @param dir nom du répertoire.
	 * @return l'URL avec le bon format.
	 * @throws MalformedURLException lancé si on ne peut deviner de quel URL il
	 * s'agit.
	 */
	private static URL getURL(String dir) throws MalformedURLException {
		System.out.println("URL non transformée : " + dir);
		
		/* On commence par transformer les noms absolus de Windows en URL ;
		 * par exemple, transformer C:\rep\machin en file:/C:/rep/machin
		 */
		if (dir.indexOf("\\") != -1) {
			// on peut soupéonner un nom Windows !
			// 4 \ pour obtenir \\ pour l'expression réguliére !
			dir = dir.replaceAll("\\\\", "/");
		} // Nom Windows
		
		/* C'est un répertoire ; plusieurs cas :
		 *   1. S'il y a le protocole "file:", on ne fait rien ; par exemple,
		 *      l'utilisateur indique que les plugins sont dans un répertoire
		 *      avec un nom absolu et, dans ce cas, il doit mettre le protocole
		 *      "file:" au début ;
		 *   2. S'il n'y a pas de protocole "file:", on le rajoute.
		 */
		if (!dir.startsWith("file:")) {
			/* On considére que c'est le nom d'une ressource ; si le nom est
			 * absolu, c'est un nom par rapport au classpath.
			 */
			dir = "file:" + dir;
		}
		
		System.out.println("URL transformee : " + dir);
		
		return new URL(dir);
	}
	
	/**
	 * Retourne une instance de plugin d'un type donné.
	 * @param nomClasse Nom de la classe du plugin
	 * @param type type de plugin
	 * @param cl chargeur de classes qui va faire le chargement de la classe
	 * de plugin
	 * @return une instance de plugin. Retourne null si problème.
	 * Par exemple, si le plugin n'a pas le bon type.
	 */
	private TPlugin getInstance(String nomClasse, Class type) {
		TPlugin plugin = null; //REMI
		
		try {
			// C'est ici que se passe le chargement de la classe par le
			// chargeur de classes.
			System.out.println("Demande de chargement de la classe " +
					nomClasse + " par " + this);
			
			// ***** DU CODE A ECRIRE ICI !!!!!
			Class c = null; //REMI
			
			//c = Class.forName("agscan.plugins."+nomClasse, true, this.getClass().getClassLoader());//REMI
			c = Class.forName(nomClasse); //REMI            
			
			try {
				// On crée une instance de la classe
				System.out.println("Création d'une instance de " + c);
				
				// ***** DU CODE A ECRIRE ICI !!!!!
				//c = loader.loadClass("agscan.plugins."+nomClasse);//TEST AJOUT
				Object o = c.newInstance(); //REMI
				plugin = (TPlugin) o; //REMI
				////
				TMenuManager.actions.addElement(plugin);//added (plugins modification) 14/12/05
				return plugin; //REMI
			} catch (ClassCastException e) {
				e.printStackTrace();
				
				//Le fichier  n'est pas un plugin 'Plugin'
				System.err.println("La classe " + nomClasse +
				" n'est pas un TPlugin");
				
				return null;
			} catch (InstantiationException e) {
				//e.printStackTrace();
				System.err.println("La classe " + nomClasse +
				" ne peut pas etre instancié");				
				return null;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				System.err.println("La classe " + nomClasse +
				" est interdite d'accees");
				
				return null;
			} catch (NoClassDefFoundError e) {
				// Survient si la classe n'a pas le bon nom
				//e.printStackTrace();
				System.err.println("La classe " + nomClasse +
				" ne peut etre trouvee");
				
				return null;
			} // Fin des catchs pour le try pour création instance
		}
		
		// Test si plug est du bon type
		// ***** DU CODE A ECRIRE ICI !!!!!
		
		/*
		 
		 if (type == null || type.isInstance(plugin)) {
		 //logger.info(String.valueOf(String.valueOf((new StringBuffer("Plugin de nom ")).append(plugin.getName()).append(" trouvé")))));
		  return plugin;
		  }
		  else {
		  logger.info("Plugin de nom " + plugin.getName()
		  + " n'est pas du bon type " + type.getName());
		  return null;
		  }
		  }
		  */
		
		// Les catchs pour le 1er try pour chargement de la classe
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.err.println("Le plugin " + nomClasse + " est introuvable");			
			return null;
		} catch (NoClassDefFoundError e) {
			// Survient si la classe n'a pas le bon nom
			e.printStackTrace();
			System.err.println("La classe " + nomClasse +
			" ne peut etre trouvee");
			
			return null;
		}		
	}
}
