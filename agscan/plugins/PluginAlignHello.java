/*
 * Created on 18 avr. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.plugins;

import agscan.TEventHandler;
import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.event.TEvent;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PluginAlignHello extends PluginAlign {
    public String getName() {
        return "pluginAlignHello";
    }

    public Class getType() {
        return PluginAlign.class;
    }

    public void act() /*actionPerformed(ActionEvent e)*/ {
        TSnifferGlobalAlignmentAlgorithm gaAlgo = new TSnifferGlobalAlignmentAlgorithm();
        gaAlgo.initWithDefaults();

        TEvent event = new TEvent(TEventHandler.DATA_MANAGER,
                TAlignmentControler.GLOBAL_ALIGNMENT, null, gaAlgo);
        TEventHandler.handleMessage(event);
    }
}
