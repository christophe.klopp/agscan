/*
 * Created on Oct 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.plugins;

import java.awt.Color;
import java.util.Enumeration;
import java.util.Vector;

import ij.ImageStack;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableColumnModel;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;


/**
 * @author rcathelin
 * @since 2005/10/19
 */
public abstract class SQuantifPlugin extends TPlugin implements Runnable {
	
	protected String pluginName = null;//name of the plugin
	protected int imagesMin;//number min of images that this plugin needs to perform quantification
	protected int imagesMax;//number max of images that this plugin can accept to perform quantification
	
	// TODO NMARY : remplir ces attributs pour chaque plugin
	// ajout des colonnes et types pour le PLOT le 02/05/07 
	public String[] columnsNames ;//array of columns created by the quantification
	public int[] columnsTypes;//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	
	public SQuantifPlugin(String pluginName,ImageIcon icon) {
		super(pluginName, icon); //de TPlugin => de TAction
		this.pluginName = pluginName;//name of the quantif
		setActiveType(ALIGNMENT_TYPE); // quand le menu est actif...
		setMenuPlugin(true); // pour dire si on definit un menu associe a ce plugin
	}
	
	/**
	 * return the name of the plugin (given to the constructor)
	 */
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * For example a quantification for a fluorescent experiment needs a minimum of 2 images
	 * @return the number min of images that this plugin needs to perform quantification
	 * @since 2005/10/19
	 */
	public int getImagesMin() {
		return imagesMin;
	}
	
	/**
	 * 
	 * @param imagesMin the number min of images that this plugin needs to perform quantification
	 * @since 2005/10/19
	 */
	public void setImagesMin(int imagesMin) {
		this.imagesMin = imagesMin;
	}
	
	/**
	 * For example a quantification for a radio experiment needs a maximum of 1 image
	 * @return number max of images that this plugin can accept to perform quantification
	 * @since 2005/10/19
	 */
	public int getImagesMax() {
		return imagesMax;		
	}
	
	/**
	 * 
	 * @param imagesMax  number max of images that this plugin can accept to perform quantification
	 * @since 2005/10/19
	 */
	public void setImagesMax(int imagesMax) {
		this.imagesMax = imagesMax;
	}
	
	/**
	 * This method verify if the number of images is compatible wirh the plugin
	 * @param stack the images stack of the curent experiment to quantify
	 * @return true if the number of images is between min and max images number supported by the plugin, false else
	 */
	
	public boolean isQuantificationPossible(ImageStack stack){		
		if (stack.getSize()>=imagesMin && stack.getSize()<=imagesMax) return true;
		else return false;
		
	}
	/**
	 * This method returns a vector containing the TSpots selected  
	 * @return a vector of TSpots, null if no selection
	 * @see agscan.data.model.grid.TSpot
	 */
	public Vector getSelectedSpots(){	
		TAlignment alignment = getCurrentAlignment();
		TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment.getModel().getReference());
		Vector spots = (Vector) TEventHandler.handleMessage(event)[0];
		return spots;
	}
	
	/**
	 * add a field to a spot and create the column associated in the table
	 * @param type type of the parameter:TColumn.TYPE_INTEGER, TColumn.TYPE_REAL,TColumn.TYPE_REAL, TColumn.TYPE_BOOLEAN
	 * @param name the name of the parameter ( here we give the same for the column ot the table)
	 */
	protected void addDefaultParamSpot(int type,String name){
		//CONFIGURATION
		TAlignment alignment = getCurrentAlignment();
		Vector params = new Vector();// params is the vector of the column configuration
		TEvent ev = null;
		
		
		//		 recuperation du model des colonnes deja ds la table
		DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
		TColumn col = null;
		Enumeration enume = columns.getColumns();//l'enumeration contient toutes les colonnes de la table
		boolean nameExist = false;		
		while (enume.hasMoreElements()) {
			col = (TColumn) enume.nextElement();
			//recherche de la colonne du model correpondant a cet algo, ex ici  COLUMN_NAME = "Qtf. Image/Const."
			if (col.getSpotKey().equals(name)) nameExist = true; 
		}
		
		if (nameExist) return; 
		
		//COLUMNS CONFIGURATION FIELDS
		Object defaultValue = null;//default value of the field
		Object infValue = null; //allows to colorize values inferior of this value
		Object betweenValue1 = null; //min limit to allow to colorize values between two values
		Object betweenValue2 = null;//max limit to allow to colorize values between two values
		Object supValue = null;   //allows to colorize values superior of this value    
		Boolean betweenActive = new Boolean(false); // active or not "between colorization"
		Boolean infActive = new Boolean(false);// active or not "inferior colorization"
		Boolean supActive = new Boolean(false);// active or not "superior colorization"
		Color betweenColor = Color.black;// color used for "between colorization"
		Color infColor = Color.black;// color used for "inferior colorization"
		Color supColor = Color.black;// color used for "between colorization"
		
		// init values declared before according to the type
		switch (type) {
		case TColumn.TYPE_INTEGER:
			defaultValue = new Integer(-1);
		infValue = new Integer(0);
		betweenValue1 = new Integer(0);
		betweenValue2 = new Integer(0);
		supValue = new Integer(0);    
		betweenActive = new Boolean(false); 
		infActive = new Boolean(false);
		supActive = new Boolean(false);
		betweenColor = Color.black;// color used for "between colorization"
		infColor = Color.black;// color used for "inferior colorization"
		supColor = Color.black;// color used for "between colorization"		
		break;
		case TColumn.TYPE_REAL:	
			defaultValue = new Double(-1);
		infValue = new Double(0);
		betweenValue1 = new Double(0);
		betweenValue2 = new Double(0);
		supValue = new Double(0);    
		betweenActive = new Boolean(false); 
		infActive = new Boolean(false);
		supActive = new Boolean(false);
		betweenColor = Color.black;// color used for "between colorization"
		infColor = Color.black;// color used for "inferior colorization"
		supColor = Color.black;// color used for "between colorization"		
		break;
		case TColumn.TYPE_TEXT:
			defaultValue = new String("...");
		break;
		case TColumn.TYPE_BOOLEAN:
			defaultValue = new Boolean(false);
		break;	
		}
		
		switch (type) {
		case TColumn.TYPE_INTEGER:
			params.addElement(name);//name displayed of the column
		params.addElement(name);//real name of the column
		params.addElement(defaultValue);//default value of the field
		params.addElement(infActive);
		params.addElement(infColor);
		params.addElement(infValue);
		params.addElement(betweenActive);
		params.addElement(betweenColor);
		params.addElement(betweenValue1);
		params.addElement(betweenValue2);
		params.addElement(supActive);
		params.addElement(supColor);
		params.addElement(supValue);
		ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_INTEGER), params);
		break;
		case TColumn.TYPE_REAL:		
			params.addElement(name);//name displayed of the column
		params.addElement(name);//real name of the column
		params.addElement(defaultValue);//default value of the field
		params.addElement(infActive);
		params.addElement(infColor);
		params.addElement(infValue);
		params.addElement(betweenActive);
		params.addElement(betweenColor);
		params.addElement(betweenValue1);
		params.addElement(betweenValue2);
		params.addElement(supActive);
		params.addElement(supColor);
		params.addElement(supValue);
		ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_REAL), params);
		break;
		case TColumn.TYPE_TEXT:
			params.addElement(name);
		params.addElement(name);
		params.addElement(defaultValue);
		ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_TEXT), params);
		break;
		case TColumn.TYPE_BOOLEAN:
			params.addElement(name);
		params.addElement(name);
		params.addElement(defaultValue);
		ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_BOOLEAN), params);
		break;
		
		}	
		
		//TColumn col = null;
		try {
			col = (TColumn) TEventHandler.handleMessage(ev)[0];
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//JFrame newWindow = new JFrame("User message");
			//JLabel message = new JLabel("Could you relaunch the plug-in");
			//newWindow.add(message);
			//newWindow.setVisible(true);
			System.out.println("NullPointerException in addDefaultParamSpot");
		}
		col.setBooleanBetweenColor(false);
		col.setBooleanInfColor(false);
		col.setBooleanSupColor(false);
		col.setEditable(false);
		col.setSynchrone(true);
		
	}
	
	
	
	/** modif 23/04 NMARY : protected -> public
	 * get the current alignment
	 * @return the alignment or null if the current element isn't an alignment
	 */
	public TAlignment getCurrentAlignment() {
		// recuperation de l'alignement 
		TAlignment alignment = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else if (currentElement instanceof TBatch) 
			 {
				alignment = ((TBatch)currentElement).getWorker().getCurrentAlignment();
				if (alignment == null)System.out.println("PROBLEM : alignment == null!!!");
			 }
		else System.out.println("PROBLEM : currentElement == null!!!");
		return alignment;
	}	
	
}
