/*
 * Created on Dec 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.plugins;

import ij.ImagePlus;

import java.awt.Color;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.model.TImageModel;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;


/**
 * @author rcathelin
 * @since 2005/12/14
 */
public abstract class SImagePlugin extends TPlugin implements Runnable {
	
	protected String pluginName = null;//name of the plugin
	
	public SImagePlugin(String pluginName,ImageIcon icon) {
		super(pluginName, icon); //de TPlugin => de TAction
		this.pluginName = pluginName;//name of the quantif
		setActiveType(IMAGE_TYPE); // quand le menu est actif...
		setMenuPlugin(true); // pour dire si on definit un menu associe a ce plugin
			
	}
	
	/**
	 * return the name of the plugin (given to the constructor)
	 */
	public String getName() {
		return (String) this.getValue(NAME);
	}
	/**
	 * manages the status bar at the beginning and at the end of the alignment 
	 * and lock/unlock the grid and the table....
	 * @param active true for begin and false for end
	 */
	public void inProgressStatus(boolean active){
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		TImageModel tim = (TImageModel)element.getModel();
		ImagePlus imp = tim.getMaxImage();		
		
		if (active){//beginning of the process
			element.getView().getGraphicPanel().removeInteractors();//locks  the image panel
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, element,pluginName+" is running", Color.red); //TODO externalize
			TEventHandler.handleMessage(ev);//action_label (bottom left)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, element, new Boolean(true));
			TEventHandler.handleMessage(ev);
		}
		else {//end of the process
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, element, new Boolean(false));
			TEventHandler.handleMessage(ev);// remove from the status bar (1/2)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, element, new Boolean(false));
			TEventHandler.handleMessage(ev);// remove from the status bar (2/2)
				ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, element,"Ready", Color.blue);//TODO externalize
			TEventHandler.handleMessage(ev);//action_label (bottom left)
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, element);
			TEventHandler.handleMessage(ev);
		}
	}
	
}
