/*
 * Created on Jul 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.plugins;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TPluginManager {
    public static TPlugin[] plugs; // tab of TPlugins
    public static Hashtable extensions = new Hashtable(); // Hashtable of (key=extension/value=index of the plugin) known by plugins
    private TPluginLoader pluginLoader;

    // constructor: instanciation of the plugins tab with the directory
    public TPluginManager() {
        try {
            pluginLoader = new TPluginLoader("./plugins"); //directory of plugins
        } catch (MalformedURLException malex) {
            System.out.println(
                "exception in TPluginManager() caused by the creation of TPluginLoader");
            System.out.println("bouh the malformed URL" + malex.getMessage());
        }

        //loading of all the plugins given into the URL
        pluginLoader.loadPlugins();
        plugs = pluginLoader.getPluginInstances();
        initExtensionsHashtable();
    }

    // return an array of the keys of the extensions hashtable
    public static String[] getExtensionsIntoArray() {
        //String[] array = new String[extensions.size()];
    	String[] array = new String[10];
        
        int i = 0;

        for (Enumeration enume = TPluginManager.extensions.keys();
        	enume.hasMoreElements();) {
        	String current = (String) enume.nextElement();
        	array[i] = current;
        	i++;
        }

        return array;
    }

    // init of the extensions hashtable 
    private void initExtensionsHashtable() {
        for (int i = 0; i < plugs.length; i++) {
            try {
                Method mTemp = plugs[i].getType().getMethod("getExtension", null); //test of this method existence

                // we stock the extension of the plugin tested
                extensions.put(mTemp.invoke(plugs[i].getType().newInstance(),
                        null), new Integer(i));
            }
            // we are in the catch if this method isn't define for the plugin tested
            catch (NoSuchMethodException ex) {
                // nothing to do
                //System.out.println("method getExtension not found!");
                //System.out.println(ex.toString());
            } catch (Exception ex2) {
                // others exceptions associated to the "invoke"
            }
        }
    }

    public static boolean isKnownExtension(String ext) {
        if (TPluginManager.extensions.containsKey(ext)) {
            return true;
        } else {
            return false;
        }
    }

    public static Object getPluginInstance(int pluginNumber)
        throws InstantiationException, IllegalAccessException {
        return TPluginManager.plugs[pluginNumber].getType().newInstance();
    }


    
    /*
     * call a method of a plugin
     * @return Object the Object resulting of the method call
     * @param Object the plugin concerned
     * @param Object[] args of the method
     * @param String methodName
     */
    public static Object callPluginMethod(Object obj, Object[] args, String methodName) throws Exception {
        // we must find type of each arg
        Class[] paramTypes = null;

        if (args != null) {
            paramTypes = new Class[args.length];

            for (int i = 0; i < args.length; ++i) {
                paramTypes[i] = args[i].getClass();
            }
        }

        // we can create the method with its name and parameter types
        Method m = obj.getClass().getMethod(methodName, paramTypes);

        // after creating, calling (=invoke)
        return m.invoke(obj, args);
    }
}
