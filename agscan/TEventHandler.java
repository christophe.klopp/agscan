package agscan;

import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TEventHandler {
	public static final int MAIN_PANE = 1;
	public static final int DATA_MANAGER = 2;
	public static final int DIALOG_MANAGER = 3;
	public static final int BZSCAN = 4;
	public static final int MENU_MANAGER = 5;
	public static final int STATUS_BAR = 6;
	
	private static TBZMessageListener mainPane;
	private static TBZMessageListener dialogManager;
	private static TBZMessageListener dataManager;
	private static TBZMessageListener bzscan;
	private static TBZMessageListener menuManager;
	private static TBZMessageListener statusBar;
	
	public TEventHandler(TBZMessageListener bz, TBZMessageListener mainP, TBZMessageListener dialogM, TBZMessageListener dataM,
			TBZMessageListener menuM, TStatusBar stBar) {
		bzscan = bz;
		mainPane = mainP;
		dialogManager = dialogM;
		dataManager = dataM;
		menuManager = menuM;
		statusBar = stBar;
	}
	public static Object[] handleMessage(TEvent event) {
		int dest = event.getDest();
		Object[] ret = null;
		switch (dest) {
		case MAIN_PANE:
			//System.out.println("MAIN_PANE");
			if (mainPane != null)  ret = mainPane.BZMessageResponse(event);//modified 2005/12/07
			break;
		case DATA_MANAGER:
			//System.out.println("DATA_MANAGER");
			if (dataManager != null)  
				ret = dataManager.BZMessageResponse(event);//modified 2005/12/07
			break;
		case BZSCAN:
			//System.out.println("BZSCAN");
			if (bzscan != null) ret = bzscan.BZMessageResponse(event);//modified 2005/12/07
			break;
		case MENU_MANAGER:
			//	System.out.println("MENU_MANAGER");
			if (menuManager != null)  ret = menuManager.BZMessageResponse(event);//modified 2005/12/07
			break;
		case DIALOG_MANAGER:
			//	System.out.println("DIALOG_MANAGER");
			if (dialogManager != null) ret = dialogManager.BZMessageResponse(event);//modified 2005/12/07
			break;
		case STATUS_BAR:
			// 	System.out.println("STATUS_BAR");
			if (statusBar != null) ret = statusBar.BZMessageResponse(event);//modified 2005/12/07
			break;
		}
		return ret;
	}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
