package agscan.ioxml;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import agscan.AGScan;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchFileChooser extends JFileChooser {
  private JCheckBox imageCheckBox = new JCheckBox("Enregistrer les images avec les alignements", false);
  public TBatchFileChooser() {
    super(AGScan.prop.getProperty("batchPath"));//modified 2005/11/29; now we call the parent class with a current directory
  }
  protected JDialog createDialog(Component parent) throws HeadlessException {
    Frame frame = parent instanceof Frame ? (Frame) parent : (Frame)SwingUtilities.getAncestorOfClass(Frame.class, parent);

    String title = getUI().getDialogTitle(this);
    getAccessibleContext().setAccessibleDescription(title);

    JDialog dialog = new JDialog(frame, title, true);

    Container contentPane = dialog.getContentPane();
    contentPane.setLayout(new BorderLayout(0, 0));
    contentPane.add(this, BorderLayout.CENTER);

    JPanel northPanel = new JPanel() {
      public Insets getInsets() {
        return new Insets(5, 10, 0, 0);
      }
    };
    northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.X_AXIS));
    northPanel.add(imageCheckBox);
    //contentPane.add(northPanel, BorderLayout.NORTH);

    if (JDialog.isDefaultLookAndFeelDecorated()) {
      boolean supportsWindowDecorations = UIManager.getLookAndFeel().getSupportsWindowDecorations();
      if (supportsWindowDecorations)
        dialog.getRootPane().setWindowDecorationStyle(JRootPane.FILE_CHOOSER_DIALOG);
    }

    dialog.pack();
    dialog.setLocationRelativeTo(parent);

    return dialog;
  }
  public boolean isImageCheckBoxSelected() {
    return imageCheckBox.isSelected();
  }
}
