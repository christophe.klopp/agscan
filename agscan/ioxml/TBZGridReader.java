/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 */

package agscan.ioxml;

import java.awt.Color;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.table.factory.TColumnFactory;

public class TBZGridReader extends DefaultHandler implements TGridReader {
  private String filename;
  private TGrid grid;
  private TGridConfig config;
  private LinkedList baliseList;
  private Hashtable elements;
  private String name, localization, value, spotKey, defaultValue, infValue, betweenValue1, betweenValue2, supValue, infColor, betweenColor, supColor,
      synchrone;
  private boolean editable, removable, infColorUsed, betweenColorUsed, supColorUsed, sync;
  private Vector values, typedValues;
  private int spotIndex;

  public TBZGridReader(String s) {
    filename = s;
    grid = null;
    config = null;
    baliseList = new LinkedList();
    elements = new Hashtable();
  }
  public void startDocument() throws SAXException {
    config = new TGridConfig();
  }
  public void endDocument() throws SAXException {
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(100));
    TEventHandler.handleMessage(event);
  }
  public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
  	//System.err.println("  " + qName);
    if (!qName.equalsIgnoreCase("model")) {
      baliseList.addLast(qName);
    }
    if (qName.equals("values"))
      values = new Vector();
    else if (qName.equals("spot")) {
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("index")) spotIndex = Integer.parseInt(attrs.getValue(i));
      }
    }
    else if (qName.equals("file")) {
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("name")) name = attrs.getValue(i);
        if (aName.equals("localization")) localization = attrs.getValue(i);
      }
      filename = localization + name;
    }
  }
  public void endElement(String namespaceURI, String sName, String qName) throws SAXException {

  	// System.err.println("**  " + qName);
    //System.err.println("***  " + betweenColor);
    Vector params;
    StringTokenizer strtok;
    TSpot spot;
    int r, g, b;
    if ((baliseList.size() > 0) && !qName.equalsIgnoreCase("model")) baliseList.removeLast();
    if (qName.equals("params")) {
      //System.err.println("coucou1");
      config.setNbLevels(Integer.parseInt((String)elements.get("grid,params,level")));
      //System.err.println("coucou2");
      config.setLevel1Params(Integer.parseInt((String)elements.get("grid,params,level1,nbRows")),
                             Integer.parseInt((String)elements.get("grid,params,level1,nbColumns")),
                             Double.parseDouble((String)elements.get("grid,params,level1,columnWidth")),
                             Double.parseDouble((String)elements.get("grid,params,level1,rowHeight")),
                             Double.parseDouble((String)elements.get("grid,params,level1,spotDiameter")),
                             Integer.parseInt((String)elements.get("grid,params,level1,startElement")),
                             Integer.parseInt((String)elements.get("grid,params,level1,elementID")));
      //System.err.println("coucou3");
      config.setLevel2Params(Integer.parseInt((String)elements.get("grid,params,level2,nbRows")),
                             Integer.parseInt((String)elements.get("grid,params,level2,nbColumns")),
                             Double.parseDouble((String)elements.get("grid,params,level2,columnSpacing")),
                             Double.parseDouble((String)elements.get("grid,params,level2,rowSpacing")),
                             Integer.parseInt((String)elements.get("grid,params,level2,startElement")),
                             Integer.parseInt((String)elements.get("grid,params,level2,elementID")));
//    level3 removed - 20005/10/28
     /*
      config.setLevel3Params(Integer.parseInt((String)elements.get("grid,params,level3,nbRows")),
                             Integer.parseInt((String)elements.get("grid,params,level3,nbColumns")),
                             Double.parseDouble((String)elements.get("grid,params,level3,columnSpacing")),
                             Double.parseDouble((String)elements.get("grid,params,level3,rowSpacing")),
                             Integer.parseInt((String)elements.get("grid,params,level3,startElement")),
                             Integer.parseInt((String)elements.get("grid,params,level3,elementID")));
     */
      grid = new TGrid(config);
           grid.getModel().clearModifs();
           String pathSeparator = System.getProperty("file.separator");
      int ix = filename.lastIndexOf(pathSeparator);
      if (ix > -1) {
        grid.setPath(filename.substring(0, ix + 1));
        grid.setName(filename.substring(ix + 1));
      }
      else {
        grid.setPath("");
        grid.setName("");
      }
      //TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null,
      //                   new Boolean(true));   
      TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBZGridReader.0"), Color.red); //$NON-NLS-1$
      TEventHandler.handleMessage(event);
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(20));
      TEventHandler.handleMessage(event);
    }
   
    else if ((qName.equals("integerColumn")) && (!name.equals(TGridConfig.COLUMN_ROW)) && (!name.equals(TGridConfig.COLUMN_COL))
    		//modif added 2005/11/18
    		&& (!name.equals(TGridConfig.COLUMN_BLOCK))&& (!name.equals(TGridConfig.COLUMN_METAROW))&& (!name.equals(TGridConfig.COLUMN_METACOL))) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      params.addElement(Integer.valueOf(defaultValue));
      params.addElement(new Boolean(infColorUsed));
      strtok = new StringTokenizer(infColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!infValue.equals("null"))
        params.addElement(Integer.valueOf(infValue));
      else
        params.addElement(null);
      params.addElement(new Boolean(betweenColorUsed));
      strtok = new StringTokenizer(betweenColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!betweenValue1.equals("null"))
        params.addElement(Integer.valueOf(betweenValue1));
      else
        params.addElement(null);
      if (!betweenValue2.equals("null"))
        params.addElement(Integer.valueOf(betweenValue2));
      else
        params.addElement(null);
      params.addElement(new Boolean(supColorUsed));
      strtok = new StringTokenizer(supColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!supValue.equals("null"))
        params.addElement(Integer.valueOf(supValue));
      else
        params.addElement(null);
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_INTEGER, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if ((qName.equals("realColumn")) && (!name.equals(TGridConfig.COLUMN_X)) && (!name.equals(TGridConfig.COLUMN_Y)) && (!name.equals(TGridConfig.COLUMN_DIAM))) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      params.addElement(Double.valueOf(defaultValue));
      params.addElement(new Boolean(infColorUsed));
      strtok = new StringTokenizer(infColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!infValue.equals("null"))
        params.addElement(Double.valueOf(infValue));
      else
        params.addElement(null);
      params.addElement(new Boolean(betweenColorUsed));
      strtok = new StringTokenizer(betweenColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!betweenValue1.equals("null"))
        params.addElement(Double.valueOf(betweenValue1));
      else
        params.addElement(null);
      if (!betweenValue2.equals("null"))
        params.addElement(Double.valueOf(betweenValue2));
      else
        params.addElement(null);
      params.addElement(new Boolean(supColorUsed));
      //System.out.println("supColor="+supColor);
      strtok = new StringTokenizer(supColor, ",");
      r = Integer.parseInt(strtok.nextToken());
     // System.out.println("r vaut="+r);
      g = Integer.parseInt(strtok.nextToken());
     // System.out.println("g vaut="+g);
      b = Integer.parseInt(strtok.nextToken());
      //System.out.println("b vaut="+b);
      params.addElement(new Color(r, g, b));
      if (!supValue.equals("null"))
        params.addElement(Double.valueOf(supValue));
      else
        params.addElement(null);
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_REAL, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if ((qName.equals("textColumn")) && (!name.equals(TGridConfig.COLUMN_NAME))) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      params.addElement(defaultValue);
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_TEXT, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if (qName.equals("probaColumn")) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      params.addElement(Double.valueOf(defaultValue));
      params.addElement(new Boolean(infColorUsed));
      strtok = new StringTokenizer(infColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!infValue.equals("null"))
        params.addElement(Double.valueOf(infValue));
      else
        params.addElement(null);
      params.addElement(new Boolean(betweenColorUsed));
      strtok = new StringTokenizer(betweenColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!betweenValue1.equals("null"))
        params.addElement(Double.valueOf(betweenValue1));
      else
        params.addElement(null);
      if (!betweenValue2.equals("null"))
        params.addElement(Double.valueOf(betweenValue2));
      else
        params.addElement(null);
      params.addElement(new Boolean(supColorUsed));
      strtok = new StringTokenizer(supColor, ",");
      r = Integer.parseInt(strtok.nextToken());
      g = Integer.parseInt(strtok.nextToken());
      b = Integer.parseInt(strtok.nextToken());
      params.addElement(new Color(r, g, b));
      if (!supValue.equals("null"))
        params.addElement(Double.valueOf(supValue));
      else
        params.addElement(null);
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_PROBA, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if (qName.equals("urlColumn")) {
      try {
        params = new Vector();
        params.addElement(name);
        params.addElement(spotKey);
        params.addElement(TColumn.makeValue(TColumn.TYPE_URL, defaultValue, null));
        TColumn column = TColumnFactory.createColumn(TColumn.TYPE_URL, params);
        if (column != null) {
          column.setRemovable(removable);
          column.setEditable(editable);
          column.setSynchrone(sync);
          grid.getGridModel().getConfig().addColumn(column);
          grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    else if (qName.equals("setOfIntegersColumn")) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      String list = "";
      for (Enumeration enume = values.elements(); enume.hasMoreElements(); ) {
        list += (String)enume.nextElement();
        if (enume.hasMoreElements()) list += ";";
      }
      typedValues = TColumn.makeList(TColumn.TYPE_INTEGER_LIST, list);
      params.addElement(typedValues.elementAt(Integer.valueOf(defaultValue).intValue()));
      params.addElement(typedValues);
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_INTEGER_LIST, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if (qName.equals("setOfRealsColumn")) {
      try {
        params = new Vector();
        params.addElement(name);
        params.addElement(spotKey);
        String list = "";
        for (Enumeration enume = values.elements(); enume.hasMoreElements(); ) {
          list += (String)enume.nextElement();
          if (enume.hasMoreElements()) list += ";";
        }
        typedValues = TColumn.makeList(TColumn.TYPE_REAL_LIST, list);
        params.addElement(typedValues.elementAt(Integer.valueOf(defaultValue).intValue()));
        params.addElement(typedValues);
        TColumn column = TColumnFactory.createColumn(TColumn.TYPE_REAL_LIST, params);
        if (column != null) {
          column.setRemovable(removable);
          column.setEditable(editable);
          column.setSynchrone(sync);
          grid.getGridModel().getConfig().addColumn(column);
          grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    else if (qName.equals("setOfTextsColumn")) {
      try {
        params = new Vector();
        params.addElement(name);
        params.addElement(spotKey);
        String list = "";
        for (Enumeration enume = values.elements(); enume.hasMoreElements(); ) {
          list += (String)enume.nextElement();
          if (enume.hasMoreElements()) list += ";";
        }
        typedValues = TColumn.makeList(TColumn.TYPE_TEXT_LIST, list);
        params.addElement(typedValues.elementAt(Integer.valueOf(defaultValue).intValue()));
        params.addElement(typedValues);
        TColumn column = TColumnFactory.createColumn(TColumn.TYPE_TEXT_LIST, params);
        if (column != null) {
          column.setRemovable(removable);
          column.setEditable(editable);
          column.setSynchrone(sync);
          grid.getGridModel().getConfig().addColumn(column);
          grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    else if (qName.equals("booleanColumn")) {
      params = new Vector();
      params.addElement(name);
      params.addElement(spotKey);
      params.addElement(Boolean.valueOf(defaultValue));
      TColumn column = TColumnFactory.createColumn(TColumn.TYPE_BOOLEAN, params);
      if (column != null) {
        column.setRemovable(removable);
        column.setEditable(editable);
        column.setSynchrone(sync);
        grid.getGridModel().getConfig().addColumn(column);
        grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      }
    }
    else if (qName.equals("columns")) {
      grid.getView().getTablePanel().initColumnSizes(true);
      TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(90));
      TEventHandler.handleMessage(event);
    }
  }
  public void characters(char buf[], int offset, int len) throws SAXException {
    if (baliseList.size() > 0) {
      String value = new String(buf, offset, len);
      String key = "";
      if (!value.trim().equals("")) {
        Iterator it = baliseList.iterator();
        while (it.hasNext()) {
          key += it.next();
          if (it.hasNext()) key += ",";
        }
        elements.put(key, value);
      }
      String currentElement = (String)baliseList.getLast();
      if (currentElement.equals("name"))
        name = value;
      else if (currentElement.equals("defaultValue"))
        defaultValue = value;
      else if (currentElement.equals("spotKey"))
        spotKey = value;
      else if (currentElement.equals("editable"))
        editable = Boolean.valueOf(value).booleanValue();
      else if (currentElement.equals("removable"))
        removable = Boolean.valueOf(value).booleanValue();
      else if (currentElement.equals("synchronized"))
        sync = Boolean.valueOf(value).booleanValue();
      else if (baliseList.size() >= 2) {
        if (baliseList.get(baliseList.size() - 2).equals("inf")) {
          if (currentElement.equals("used"))
            infColorUsed = Boolean.valueOf(value).booleanValue();
          else if (currentElement.equals("value"))
            infValue = value;
          else if (currentElement.equals("color")) {
            infColor = value;
          }
        }
        else if (baliseList.get(baliseList.size() - 2).equals("between")) {
          if (currentElement.equals("used"))
            betweenColorUsed = Boolean.valueOf(value).booleanValue();
          else if (currentElement.equals("value1"))
            betweenValue1 = value;
          else if (currentElement.equals("value2"))
            betweenValue2 = value;
          else if (currentElement.equals("color")) {
            betweenColor = value;
          }
        }
        else if (baliseList.get(baliseList.size() - 2).equals("sup")) {
          if (currentElement.equals("used"))
            supColorUsed = Boolean.valueOf(value).booleanValue();
          else if (currentElement.equals("value"))
            supValue = value;
          else if (currentElement.equals("color")) {
            supColor = value;
          }
        }
        else if (baliseList.get(baliseList.size() - 2).equals("values")) {
          values.addElement(value);
        }
      }
    }
  }
  public TGrid getGrid() {
    return grid;
  }
  public String getName() {
    return name;
  }
  public String getDefaultValue() {
    return defaultValue;
  }
  public String getSpotKey() {
    return spotKey;
  }
  public boolean getEditable() {
    return editable;
  }
  public boolean getRemovable() {
    return removable;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
