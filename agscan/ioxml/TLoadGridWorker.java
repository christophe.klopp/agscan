/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.ioxml;

import java.awt.Color;
import java.awt.Cursor;
import java.io.File;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TAlignmentControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TLoadGridWorker extends SwingWorker {
	private File file;
	private TImage image;
	private TGridModel gridModel;
	private TAlignment alignment = null;
	
	public TLoadGridWorker(File file, TImage image) {
		super();
		this.file = file;
		this.image = image;
		gridModel = null;
	}
	public Object construct(boolean thread) {
		TGridReader gridReader = null;
		if (file.getAbsolutePath().endsWith(".grd"))
			gridReader = new TBZGridReader(file.getAbsolutePath());
		else if (file.getAbsolutePath().endsWith(".xml"))
			gridReader = new TMageMlGridReader(file.getAbsolutePath());
		SAXParserFactory factory = SAXParserFactory.newInstance();
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, image, Messages.getString("TLoadGridWorker.1"), Color.red);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, image);
		TEventHandler.handleMessage(event);
		FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(file, (DefaultHandler)gridReader);
			gridModel = gridReader.getGrid().getGridModel();
			TAlignmentModel alignmentModel = new TAlignmentModel(image.getImageModel(), gridModel, false);
			alignment = new TAlignment(alignmentModel, image, gridReader.getGrid());
			String pathSeparator = System.getProperty("file.separator");
			String filename = file.getAbsolutePath();
			int ix = filename.lastIndexOf(pathSeparator);
			alignment.setName(image.getName().substring(0,image.getName().length()-4) + ".zaf");//TEST REMI
			//alignment.setName("ALI_" + new Date().getTime() + ".zaf");//removed R�mi
			alignment.setPath(image.getPath());
		}
		catch (Throwable t) {
			t.printStackTrace();
		}
		return null;
	}
	public void finished() {
		if (alignment != null) {    	
			TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.CHANGE_CURRENT_DATA, null, alignment);
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.CHANGE_VIEW, alignment.getView());
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, alignment, new Integer(0));
			TEventHandler.handleMessage(event);
			alignment.getView().setDividerLocation(0.9);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false));
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment, Messages.getString("TLoadGridWorker.0"), Color.blue);
			TEventHandler.handleMessage(event);
			FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
			
						Vector params = new Vector();
			////modified 2005/11/17 "Computed Diameter"
			//params.addElement("Diamètre calculé");      //TSpot.PARAMETER_COMPUTED_DIAMETER = ("Diamètre calculé")
			//params.addElement("Diamètre calculé");      //TSpot.PARAMETER_COMPUTED_DIAMETER = ("Diamètre calculé")
			params.addElement("Computed Diameter");
			params.addElement("Computed Diameter");
			params.addElement(new Double(-1));
			params.addElement(new Boolean(false));
			params.addElement(Color.lightGray);
			params.addElement(null);
			params.addElement(new Boolean(false));
			params.addElement(Color.lightGray);
			params.addElement(null);
			params.addElement(null);
			params.addElement(new Boolean(false));
			params.addElement(Color.lightGray);
			params.addElement(null);
			TColumn col = ((TAlignmentControler)alignment.getControler()).getGridColumnsControler().addColumn(TColumn.TYPE_REAL, params, alignment);
			alignment.getGridModel().addParameter(col.getSpotKey(), col.getDefaultValue(), false);
			col.setEditable(false);
			col.setRemovable(false);
			col.setSynchrone(true);
			
			Enumeration enume = alignment.getGridModel().getSpots().elements();
			TSpot spot;
			while (enume.hasMoreElements()) {
				spot = (TSpot)enume.nextElement();
				spot.setLeft(alignment.getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent()));
				spot.setRight(alignment.getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent()));
				spot.setTop(alignment.getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent()));
				spot.setBottom(alignment.getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent()));
				spot.setGlobalLeft(alignment.getGridModel().getLeftSpot(spot, alignment.getGridModel().getRootBlock()));
				spot.setGlobalRight(alignment.getGridModel().getRightSpot(spot, alignment.getGridModel().getRootBlock()));
				spot.setGlobalTop(alignment.getGridModel().getTopSpot(spot, alignment.getGridModel().getRootBlock()));
				spot.setGlobalBottom(alignment.getGridModel().getBottomSpot(spot, alignment.getGridModel().getRootBlock()));
			}
		}
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
