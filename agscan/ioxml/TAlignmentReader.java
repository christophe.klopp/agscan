package agscan.ioxml;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFileChooser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TImageControler;
import agscan.data.controler.worker.TTransposeWorker;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
//import agscan.dialog.histogram.transfercurve.TTransferCurve;
//import agscan.dialog.histogram.transfercurve.TTransferCurveFactory;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;
import agscan.dialog.TDialogManager;

public class TAlignmentReader extends DefaultHandler {
	private TBZGridReader gridReader;
	private String filename, imagePath, imageName, gridPath, gridName, imageFile, alignmentFile, min, max, /*tc, par,*/ n, na;//tc et par removed (histogram) 2005/11/18
	private TAlignment alignment;
	private TGrid grid;
	private TImage image;
	private boolean imageSegment, gridSegment, saveImage;
	private Hashtable imageModifs;
	private int level, r, c, ix;//, channelsNB = -1;
	private Vector imageNames;//Vector of channels names
	private double x, y, da, sw, sh, dia;
	private double x_shift, y_shift;//shift from the global alignement 
	
	public TAlignmentReader(String s, String in, String an) {
		super();
		filename = s;
		alignment = null;
		grid = null;
		gridReader = new TBZGridReader(filename);
		imageSegment = gridSegment = saveImage = false;
		imageModifs = new Hashtable();
		imagePath = imageName = gridPath = gridName = "";
		imageFile = in;
		alignmentFile = an;
		imageNames = new Vector();
		x_shift = 0.0D;
		y_shift = 0.0D;
		
	}
	public void startDocument() throws SAXException {
		gridReader.startDocument();
	}
	public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
		String key, value = "", spotKey = "", sync = "";
		String aName, s;
		TSpot spot;
		
		if (qName.equals("image")) {
			imageSegment = true;
			
		}
		//  channels added 2005/11/03
		/* if (qName.equals("channels")) {
		 channelsNB = Integer.parseInt(attrs.getValue(0));
		 System.err.println("NB="+channelsNB);        
		 }
		 */
		else if (qName.equals("grid")) {
			gridSegment = true;
		}
		
		else if (qName.equals("file")) {
			if (imageSegment)
				for (int i = 0; i < attrs.getLength(); i++) {
					aName = attrs.getLocalName(i);
					if ("".equals(aName)) aName = attrs.getQName(i);
					if (aName.equals("name")) imageNames.add(attrs.getValue(i));
					if (aName.equals("localization")) imagePath = attrs.getValue(i);
				}
			else
				for (int i = 0; i < attrs.getLength(); i++) {
					aName = attrs.getLocalName(i);
					if ("".equals(aName)) aName = attrs.getQName(i);
					if (aName.equals("name")) gridName = attrs.getValue(i);
					if (aName.equals("localization")) gridPath = attrs.getValue(i);
				}
		}
		else if (qName.equals("modif")) {
			key = value = "";
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if ("".equals(aName)) aName = attrs.getQName(i);
				if (aName.equals("type")) key = attrs.getValue(i);
				if (aName.equals("value")) value = attrs.getValue(i);
			}
			if (!key.equals("")) imageModifs.put(key, value);
		}
		else if (qName.equals("block")) {
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if ("".equals(aName)) aName = attrs.getQName(i);
				if (aName.equals("level")) level = Integer.parseInt(attrs.getValue(i));
				if (aName.equals("r")) r = Integer.parseInt(attrs.getValue(i));
				if (aName.equals("c")) c = Integer.parseInt(attrs.getValue(i));
				if (aName.equals("x")) x = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("y")) y = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("da")) da = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("sw")) sw = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("sh")) sh = Double.parseDouble(attrs.getValue(i));
			}
		}
		else if (qName.equals("spot")) {
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if ("".equals(aName)) aName = attrs.getQName(i);
				if (aName.equals("n")) n = attrs.getValue(i);
				if (aName.equals("r")) r = Integer.parseInt(attrs.getValue(i));
				if (aName.equals("c")) c = Integer.parseInt(attrs.getValue(i));
				if (aName.equals("x")) x = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("y")) y = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("d")) dia = Double.parseDouble(attrs.getValue(i));
			}
		}
		else if (qName.equals("feature")) {
		//	System.out.println("alors? l= "+attrs.getLength());	
			// i vaut 0!
			
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				//System.out.println("aName= "+aName);
				if ("".equals(aName)) aName = attrs.getQName(i);
				//System.out.println("on y est ix="+ix);//test remi
				if (aName.equals("index")) {
					ix = Integer.parseInt(attrs.getValue(i));// ne passe jamais! donc ix vaut tjrs 0!
				}
				if (aName.equals("name")) na = attrs.getValue(i);
			}
		}
		
		else if(qName.equals("shift")){
			System.out.println("attrs.getLength "+attrs.getLength());
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if (aName.equals("shift_x")) x_shift = Double.parseDouble(attrs.getValue(i));
				if (aName.equals("shift_y")) y_shift = Double.parseDouble(attrs.getValue(i));
				
			}
			System.out.println("decalage IOXML = "+x_shift+" ; "+y_shift);
		}
		
		else if (qName.equals("column") && !gridSegment) {
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if ("".equals(aName)) aName = attrs.getQName(i);
				if (aName.equals("name")) spotKey = attrs.getValue(i);
				if (aName.equals("value")) value = attrs.getValue(i);
				if (aName.equals("synchronized")) sync = attrs.getValue(i);
				//here spotKey corresponds to quantif name like "Qtf. Fit/Calc" "QM" "Qtf. Image/Const." ...
		
			}
			TColumn column = null;
			try {
				//System.out.println("spotKey= "+spotKey);//test remi
				//System.out.println("ix="+ix);//test remi il vaut tt le tps 0!!!!
				spot = grid.getGridModel().getSpot(ix);
				//System.out.println("spotnum="+spot.getIndex());//test remi
				column = grid.getGridModel().getConfig().getColumnBySpotKey(spotKey);
				spot.addParameter(spotKey, TColumn.makeValue(column.getType(), value, column.getValues()), column.isSynchrone());
				spot.setSynchro(spotKey, Boolean.valueOf(sync).booleanValue());
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		else if (qName.equals("fit")) {
			spot = grid.getGridModel().getSpot(ix);
			for (int i = 0; i < attrs.getLength(); i++) {
				aName = attrs.getLocalName(i);
				if ("".equals(aName)) aName = attrs.getQName(i);
				if (aName.equals("name")) {
					s = attrs.getValue(i);
					if (!s.equals("")) {
						if (s.equals("Fit Gauss-Newton")) {
							spot.setFitAlgorithm(new TGaussNewtonFitAlgorithm());
						}
						else if (s.equals("Fit normal")) {
							spot.setFitAlgorithm(new TNormalFitAlgorithm());
						}
						else
							spot.setFitAlgorithm(null);
					}
					else
						spot.setFitAlgorithm(null);
				}
				if (aName.equals("params")) {
					s = attrs.getValue(i);
					if ((spot.getFitAlgorithm() != null) && (!s.equals(""))) {
						spot.getFitAlgorithm().setParameters(spot, s);
					}
				}
			}
		}
		if (gridSegment) {
			gridReader.startElement(namespaceURI, sName, qName, attrs);
		}
	}
	public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
		String key, value;
		TEvent event;
		StringTokenizer strtok;
		TOpenFileWorker openFileWorker = null;
		TTransposeWorker transposeWorker;
		
		if (gridSegment)
			gridReader.endElement(namespaceURI, sName, qName);
		else if (qName.equals("image")) {
			String imageAbsolutePath;
		
			if (imagePath.equals(".")){
				imageAbsolutePath = imageName;
				imagePath = "";//added 2005/12/02 if we use the image of the zip, the path is here
			}
			//TODO verif the imagePath, probably not used, often empty here ....review all !
			else{
				imageAbsolutePath = imagePath + imageName;			
			}
			//modified for multi-channels image
			File[] filesTab = new File[imageNames.size()];// we create a tab of files corresponding to each channel
			File file = null;
			
			/*for (int i =1;i<=channelsNB;i++){
			 file = new File(imageAbsolutePath);
			 }
			 */
		//	System.out.println("imageNames.size()="+imageNames.size());
			for (int i = 0;i<imageNames.size();i++){
				System.out.println("imagePath="+imagePath);
				System.out.println("imageName="+imageNames.get(i).toString());		
				file = new File(imagePath+imageNames.get(i).toString());
				
	
				//here //2005/12/02 added: the test of existence and the possibility to give a path if not found
				//TODO do something for the eror if a bad file is choosen
				if (!file.exists()){
					event = new TEvent(TEventHandler.DIALOG_MANAGER,
								TDialogManager.MESSAGE_DIALOG,Messages.getString("TAlignmentReader.0")); //externalized since 2006/02/20
					TEventHandler.handleMessage(event);
					JFileChooser chooser = new JFileChooser();
					int returnVal = chooser.showOpenDialog(null);					
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						//TODO bcp d'embetement pourquoi garder tout ces names....
						imagePath = chooser.getSelectedFile().getParentFile().getAbsolutePath();
						imageNames.set(i,chooser.getSelectedFile().getName());
						file = new File(imagePath+File.separator+imageNames.get(i).toString());
						System.out.println("imagePath choosed= "+imagePath);
						System.out.println("imageName choosed= "+imageNames.get(i).toString());
						//TODO connecter ce chooser avec les chemins retenus par les propri�t�s
					}	
					
				}
				
				
				
				filesTab[i]  = file;
				System.out.println("file.getAbsolutePath()="+file.getAbsolutePath());
			}
			
			//TODO utiliser le vecteur de File pour l'ouverture des images! = plus generique!!
			openFileWorker = new TOpenFileWorker(filesTab);
			
			openFileWorker.setPriority(Thread.MIN_PRIORITY);
			image = (TImage)openFileWorker.construct(false);
			image.getImageModel().setReference(image);
			Enumeration keys = imageModifs.keys();
			min = "0";
			max = "255";
		//	tc = "Linear";//removed 2005/11/18
			//par = "1";//removed 2005/11/18
			while (keys.hasMoreElements()) {
				key = (String)keys.nextElement();
				value = (String)imageModifs.get(key);
				if (key.equalsIgnoreCase("rotation")) {
					if (value.equals("90")) {
						// 12/09/05 - modification du TImageControler pour pouvoir modifier l'image dans le meme thread ou dans 
						// un autre thread selon le parametre supplementaire "true" ou "false". 
						event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.ROTATE_PLUS_90, image,"true");
						TEventHandler.handleMessage(event);
					}
					else if (value.equals("-90")) {
						event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.ROTATE_MOINS_90, image,"true");
						TEventHandler.handleMessage(event);
					}
					else if (value.equals("180")) {
						event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.ROTATE_180, image,"true");
						TEventHandler.handleMessage(event);
					}
				}
				else if (key.equalsIgnoreCase("flip")) {
					if (value.equalsIgnoreCase("horizontal")) {
						event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.HORIZONTAL_FLIP, image,"true");
						TEventHandler.handleMessage(event);
					}
					else if (value.equalsIgnoreCase("vertical")) {
						event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.VERTICAL_FLIP, image,"true");
						TEventHandler.handleMessage(event);
						//image.getImageControler().processEvent(event);
					}
				}
				else if (key.equalsIgnoreCase("histogram")) {
					strtok = new StringTokenizer(value);
					min = strtok.nextToken();
					max = strtok.nextToken();
					System.out.println("min="+min+".");
					System.out.println("max="+max+".");
					//TODO supprimer carrement histogram et aussi dans l'ecriture du xml car il y est? a voir
					//tc = strtok.nextToken();//deleted 2005/11/17
					//par = strtok.nextToken();//deleted 2005/11/17
				}
			}
			imageSegment = false;
			saveImage = imagePath.equals(".");
		}
		if (qName.equals("grid")) {
			gridSegment = false;
			grid = gridReader.getGrid();
			//System.err.println("*** " + grid.getGridModel().getColumnCount());
		}
		if (qName.equals("block")) {
			double w = 0, h = 0;
			if (level == 3) {
				gridReader.getGrid().getGridModel().getRootBlock().setPosition(x, y);
				gridReader.getGrid().getGridModel().getRootBlock().addAngle(da);
				gridReader.getGrid().getGridModel().getRootBlock().setSpotSize(sw, sh);
			}
			else if (level == 2) {
				TGridBlock gb = gridReader.getGrid().getGridModel().getLevel2Element(r, c);
				gb.setPosition(x, y);
				gb.addAngle(da);
				gb.setSpotSize(sw, sh);
			}
			else if (level == 1) {
				TGridBlock gb = gridReader.getGrid().getGridModel().getLevel1Element(r, c);
				gb.setPosition(x, y);
				gb.addAngle(da);
				gb.setSpotSize(sw, sh);
			}
			level = -1;
		}
		if (qName.equals("spot")) {
			TSpot sp = gridReader.getGrid().getGridModel().getSpot(r, c);
			if (sp != null) {
				sp.setPosition(x, y);
				sp.addParameter(TSpot.PARAMETER_DIAMETER, new Double(dia), true);
				sp.addParameter(TSpot.PARAMETER_NAME, n, true);
			}
		}
		/*    if (qName.equals("column")) {
		 if (aName.equals("name")) spotKey = gridReader.getName();
		 if (aName.equals("value")) value = attrs.getValue(i);
		 if (aName.equals("synchrone")) synchrone = attrs.getValue(i);
		 if (aName.equals("synchronized")) sync = attrs.getValue(i);
		 }*/
	}
	public void endDocument() throws SAXException {
		TImageModel imageModel = image.getImageModel();
		TGridModel gridModel = grid.getGridModel();
		TAlignmentModel model = new TAlignmentModel(imageModel, gridModel, saveImage);
		alignment = new TAlignment(model, image, grid);
		alignment.getGridModel().getConfig().setDeca_X(x_shift);
		alignment.getGridModel().getConfig().setDeca_Y(y_shift);
		//TTransferCurve transferCurve = TTransferCurveFactory.getInstance().getTransferCurve(tc);
	//	transferCurve.setParam(Double.parseDouble(par));
		TEvent event = null;//new TEvent(TEventHandler.DATA_MANAGER, TImageControler.TRANSFER_CURVE, alignment, transferCurve);//2005/11/16 transfer curves remove
	//	TEventHandler.handleMessage(event);
		//alignment.getControler().processEvent(event);
		if (!min.equals("0") || !max.equals("255")) {
			event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.MIN_AND_MAX, alignment, Double.valueOf(min), Double.valueOf(max));
			TEventHandler.handleMessage(event);
			//alignment.getControler().processEvent(event);
			event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.ADD_MEMENTO, alignment);
			TEventHandler.handleMessage(event);
			//alignment.getControler().processEvent(event);
			event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
			TEventHandler.handleMessage(event);
			//alignment.getControler().processEvent(event);
		}
		event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, alignment);
		TEventHandler.handleMessage(event);
		//alignment.getControler().processEvent(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(event);
		//alignment.getControler().processEvent(event);
		gridReader.endDocument();
	}
	public void characters(char buf[], int offset, int len) throws SAXException {
		gridReader.characters(buf, offset, len);
	}
	public TAlignment getAlignment() {
		return alignment;
	}
	public String getImagePath() {
		return imagePath;
	}
	public String getGridName() {
		return gridName;
	}
	public String getGridPath() {
		return gridPath;
	}
	public String getImageName() {
		return imageName;
	}
	public Hashtable getImageModifs() {
		return imageModifs;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
