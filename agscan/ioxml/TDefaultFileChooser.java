
/*
 * Created on 8th December 2005
 * This class comes from BZScan2. 
 * It has been added into AGScan the December 8 of 2005
 * TDefaultFileChooser allows to call a chooser with a given title
 * @version 2005/12/08
 * @author Fabrice Lopez
 */

package agscan.ioxml;

import java.awt.Component;
import java.awt.HeadlessException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;

public class TDefaultFileChooser extends JFileChooser {
  public Component p;
  String t;
  public TDefaultFileChooser(Component p, String t) {
    super();
    this.p = p;
    this.t = t;
  }
  protected JDialog createDialog(Component parent) throws HeadlessException {
    JDialog dialog = super.createDialog(p);
    dialog.setTitle(t);
    return dialog;
  }
}
