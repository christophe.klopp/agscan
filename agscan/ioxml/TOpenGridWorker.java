/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.ioxml;

import java.awt.Color;
import java.awt.Cursor;
import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;

public class TOpenGridWorker extends SwingWorker {
  private File file;
  private TGrid grid;

  public TOpenGridWorker(File file) {
    super();
    this.file = file;
    grid = null;
  }
  public Object construct(boolean thread) {
    TGridReader gridReader = null;
    if (file.getAbsolutePath().endsWith(".grd"))
      gridReader = new TBZGridReader(file.getAbsolutePath());
    else if (file.getAbsolutePath().endsWith(".xml"))
      gridReader = new TMageMlGridReader(file.getAbsolutePath());
    SAXParserFactory factory = SAXParserFactory.newInstance();
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TOpenGridWorker.2"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    try
    {
      SAXParser saxParser = factory.newSAXParser();
      saxParser.parse(file, (DefaultHandler)gridReader);
      grid = gridReader.getGrid();
      String pathSeparator = System.getProperty("file.separator");
      String p = pathSeparator;
      String filename = file.getAbsolutePath();
      int ix = filename.lastIndexOf(pathSeparator);
      if (ix > -1) p = filename.substring(0, ix + 1);
      grid.setPath(p);
      grid.setName(filename.substring(ix + 1));
    }
    catch (Throwable t) {
      t.printStackTrace();
    }
    return null;
  }
  public void finished() {
    if (grid != null) {
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, grid);
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, grid.getView());
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TOpenGridWorker.0"), Color.blue);
      TEventHandler.handleMessage(event);
      FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
