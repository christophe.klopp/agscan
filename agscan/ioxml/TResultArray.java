package agscan.ioxml;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TResultArray {
  private int[][] results;
  private int[] quality;
  private int nb_series, len;
  public TResultArray(int nb_series, int len) {
    super();
    this.nb_series = nb_series;
    this.len = len;
    results = new int[nb_series][len];
    quality = new int[len];
  }
  public int[][] getResults() {
    return results;
  }
  public int[] getQuality() {
    return quality;
  }
  public void setValue(int serie, int index, int value) {
    results[serie][index] = value;
  }
  public void setQuality(int index, int value) {
    quality[index] = value;
  }
  public int getLen() {
    return len;
  }
  public int getNbSeries() {
    return nb_series;
  }
}
