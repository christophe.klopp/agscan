package agscan.ioxml;

import java.awt.Color;
import java.awt.Cursor;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TReadQuantifResultsWorker extends SwingWorker {
  private File zipFile;
  private TResultArray results;
  private TDataElement reference;
  private String[] spotKeys;

  public TReadQuantifResultsWorker(File file, TDataElement ref, String[] sks) {
    super();
    zipFile = file;
    results = null;
    reference = ref;
    spotKeys = sks;
  }
  public Object construct(boolean thread) {
    String zipAbsolutePath = zipFile.getAbsolutePath();
    String imageName = "", alignmentName = "";
    SAXParserFactory factory = SAXParserFactory.newInstance();
    FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    try {
      SAXParser saxParser = factory.newSAXParser();
      ZipFile zFile = new ZipFile(zipAbsolutePath);
      Enumeration enume = zFile.entries();
      ZipEntry zEntry;
      File f;
      while (enume.hasMoreElements()) {
        zEntry = (ZipEntry)enume.nextElement();
        f = new File(zEntry.getName());
        f.deleteOnExit();
        copyInputStream(zFile.getInputStream(zEntry), new BufferedOutputStream(new FileOutputStream(f)));
      }
      BufferedReader br = new BufferedReader(new FileReader(new File("manifest")));
      while (br.ready()) {
        String s = br.readLine();
        StringTokenizer strtok = new StringTokenizer(s, "=");
        String s2 = strtok.nextToken().trim();
        if (s2.equalsIgnoreCase("image")) {
          s2 = strtok.nextToken().trim();
          imageName = s2;
        }
        else if (s2.equalsIgnoreCase("ali")) {
          s2 = strtok.nextToken().trim();
          alignmentName = s2;
        }
      }

      if (!imageName.equals("") && !alignmentName.equals("")) {      
        TQuantifResultsReader quantifResultsReader = new TQuantifResultsReader(spotKeys.length, spotKeys);
        saxParser.parse(new File(alignmentName), (DefaultHandler)quantifResultsReader);
        results = quantifResultsReader.getResults();
      }
    }
    catch (Throwable t) {
      t.printStackTrace();
    }
    return results;
  }
  public void finished() {  	
    if (results != null) {
      TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, reference, new Integer(0));
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, reference, new Boolean(false));
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, reference, Messages.getString("TReadQuantifResultsWorker.0"), Color.blue);
      TEventHandler.handleMessage(event);
      FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
    }
  }

  public void copyInputStream(InputStream in, OutputStream out) {
    int len = 0;
    try {
      byte[] buffer = new byte[1024];
      while ((len = in.read(buffer)) >= 0) out.write(buffer, 0, len);
      in.close();
      out.close();
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
