package agscan.ioxml;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TQuantifResultsReader extends DefaultHandler {
  private Vector spotKeys;
  private TResultArray results;
  private int nb_series, nb_features, ix, q;
  private boolean gridSegment = false, tomult = false;

  public TQuantifResultsReader(int nbs, String[] sks) {
    super();
    results = null;
    nb_series = nbs;
    nb_features = 1;
    spotKeys = new Vector();
    for (int i = 0; i < sks.length; i++) spotKeys.addElement(sks[i]);
  }
  public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
    String value = "", spotKey = "";
    String aName;

    if (qName.equals("nbColumns") || qName.equals("nbRows")) {
      tomult = true;
    }
    else if (qName.equals("grid")) {
      gridSegment = true;
      nb_features = 1;
    }
    else if (qName.equals("feature")) {
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("index")) ix = Integer.parseInt(attrs.getValue(i));
        if (aName.equals("quality")) q = Integer.parseInt(attrs.getValue(i));
      }
    }
    else if (qName.equals("column") && !gridSegment) {
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("name")) spotKey = attrs.getValue(i);
        if (aName.equals("value")) value = attrs.getValue(i);
      }
      if (spotKeys.contains(spotKey)) {      
        results.setValue(spotKeys.indexOf(spotKey), ix,(int)Float.parseFloat(value));//Integer.parseInt(value));
        results.setQuality(ix, q);
      }
    }
  }
  public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
    if (qName.equals("grid")) {
      gridSegment = false;
      results = new TResultArray(nb_series, nb_features);
    }
    else if (qName.equals("nbColumns") || qName.equals("nbRows")) {
      tomult = false;
    }
  }
  public void characters(char buf[], int offset, int len) throws SAXException {
    String value;
    if (tomult) {
      value = new String(buf, offset, len);
      nb_features *= Integer.parseInt(value);
    }
  }
  public TResultArray getResults() {
    return results;
  }
}
