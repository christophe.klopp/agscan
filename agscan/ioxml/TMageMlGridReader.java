package agscan.ioxml;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import agscan.TEventHandler;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.table.factory.TColumnFactory;

public class TMageMlGridReader extends DefaultHandler implements TGridReader {
  private String filename;
  private TGrid grid;
  private TGridConfig config;
  private String name, value, spotKey, defaultValue, column, row;
  private Vector values, typedValues;
  private int spotIndex, l2NbRows, l2NbCols, l1NbRows, l1NbCols;
  private double spotDiameter, l2ColSpacing, l2RowSpacing, l1ColSpacing, l1RowSpacing;
  private Hashtable features;

  public TMageMlGridReader(String s) {
    filename = s;
    grid = null;
    config = null;
    features = new Hashtable();
  }
  public void startDocument() throws SAXException {
    System.err.println("startDocument");
    config = new TGridConfig();
  }
  public void endDocument() throws SAXException {
    String key, type, clone, gene;
    StringTokenizer strtok;
    int r, c;
    TSpot spot;
    Vector urlVector, listVector;
    config.setNbLevels(2);
    config.setLevel1Params(l1NbRows, l1NbCols, l1ColSpacing, l1RowSpacing, spotDiameter, 1, 1);
    config.setLevel2Params(l2NbRows, l2NbCols, l2ColSpacing, l2RowSpacing, 1, 1);
  //  config.setLevel3Params(1, 1, 0, 0, 1, 1);//level3 removed - 20005/10/28
    grid = new TGrid(config);
    Vector params = new Vector();
    params.addElement("Type");
    params.addElement("empty");
    listVector = new Vector();
    listVector.addElement("bact");
    listVector.addElement("pcr");
    listVector.addElement("oligo");
    listVector.addElement("plasmid");
    listVector.addElement("control");
    listVector.addElement("empty");
    listVector.addElement("exclude");
    listVector.addElement("void");
    params.addElement(listVector);
    TColumn columnType = TColumnFactory.createColumn(TColumn.TYPE_TEXT_LIST, params);
    params = new Vector();
    params.addElement("Clone");
    urlVector = new Vector();
    urlVector.addElement("Link");
    urlVector.addElement(" ");
    params.addElement(urlVector);
    TColumn columnClone = TColumnFactory.createColumn(TColumn.TYPE_URL, params);
    params = new Vector();
    params.addElement("G�ne");
    urlVector = new Vector();
    urlVector.addElement("Link");
    urlVector.addElement(" ");
    params.addElement(urlVector);
    TColumn columnGene = TColumnFactory.createColumn(TColumn.TYPE_URL, params);
    columnType.setRemovable(true);
    columnType.setEditable(true);
    columnClone.setRemovable(true);
    columnClone.setEditable(true);
    columnGene.setRemovable(true);
    columnGene.setEditable(true);
    grid.getGridModel().getConfig().addColumn(columnType);
    grid.getGridModel().getConfig().addColumn(columnClone);
    grid.getGridModel().getConfig().addColumn(columnGene);
    for (Enumeration enume = features.keys(); enume.hasMoreElements(); ) {
      key = (String)enume.nextElement();
      r = Integer.parseInt(key.substring(0, key.indexOf(",")));
      c = Integer.parseInt(key.substring(key.indexOf(",") + 1));
      spot = grid.getGridModel().getSpot(r, c);
      strtok = new StringTokenizer((String)features.get(key), "/");
      type = clone = gene = " ";
      try {
        type = strtok.nextToken();
        clone = new String(strtok.nextToken());
        if (clone.indexOf(":") > 0) clone = clone.substring(clone.indexOf(":") + 1);
        gene = new String(strtok.nextToken());
      }
      catch (Exception ex) {}
      spot.addParameter(columnType.getSpotKey(), type, true);
      urlVector = new Vector();
      urlVector.addElement(clone);
      urlVector.addElement("http://genome-www5.stanford.edu/cgi-bin/source/sourceResult?option=CloneID&criteria=" + clone + "&choice=cDNA");
      spot.addParameter(columnClone.getSpotKey(), urlVector, true);
      urlVector = new Vector();
      urlVector.addElement(gene);
      urlVector.addElement("http://www.ncbi.nlm.nih.gov/LocusLink/list.cgi?Q=" + gene + "&ORG=&V=0");
      spot.addParameter(columnGene.getSpotKey(), urlVector, true);
    }
    grid.getModel().clearModifs();
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(true));
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(100));
    TEventHandler.handleMessage(event);
  }
  public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
    if (qName.equals("FeatureGroup"))
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("featureLength")) spotDiameter = Double.parseDouble(attrs.getValue(i));
      }
    else if (qName.equals("ZoneGroup"))
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("zonesPerX")) l2NbCols = Integer.parseInt(attrs.getValue(i));
        if (aName.equals("zonesPerY")) l2NbRows = Integer.parseInt(attrs.getValue(i));
        if (aName.equals("spacingsBetweenZonesX")) l2ColSpacing = Double.parseDouble(attrs.getValue(i));
        if (aName.equals("spacingsBetweenZonesY")) l2RowSpacing = Double.parseDouble(attrs.getValue(i));
      }
    else if (qName.equals("ZoneLayout"))
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("numFeaturesPerCol")) l1NbRows = Integer.parseInt(attrs.getValue(i));
        if (aName.equals("numFeaturesPerRow")) l1NbCols = Integer.parseInt(attrs.getValue(i));
        if (aName.equals("spacingBetweenCols")) l1ColSpacing = Double.parseDouble(attrs.getValue(i));
        if (aName.equals("spacingBetweenRows")) l1RowSpacing = Double.parseDouble(attrs.getValue(i));
      }
    else if (qName.equals("Feature"))
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("name")) name = attrs.getValue(i);
      }
    else if (qName.equals("FeatureLocation"))
      for (int i = 0; i < attrs.getLength(); i++) {
        String aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("column")) column = attrs.getValue(i);
        if (aName.equals("row")) row = attrs.getValue(i);
      }
  }
  public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
    String key = "";
    if (qName.equals("Feature")) {
      key = row + "," + column;
      features.put(key, name);
    }
  }
  public void characters(char buf[], int offset, int len) throws SAXException {
  }
  public TGrid getGrid() {
    return grid;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
