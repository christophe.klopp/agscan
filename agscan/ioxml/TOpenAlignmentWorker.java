package agscan.ioxml;

import java.awt.Color;
import java.awt.Cursor;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TOpenAlignmentWorker extends SwingWorker {
  private File zipFile;
  private TAlignment alignment;

  public TOpenAlignmentWorker(File file) {
    super();
    zipFile = file;
    alignment = null;
  }
  public Object construct(boolean thread) {
    String zipAbsolutePath = zipFile.getAbsolutePath();
    String aliAbsolutePath = zipAbsolutePath.substring(0, zipAbsolutePath.lastIndexOf((int)'.'));
    String imageName = "", alignmentName = "";
    SAXParserFactory factory = SAXParserFactory.newInstance();
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TOpenAlignmentWorker.0"), Color.red);//externalized since 2006/02/20
    TEventHandler.handleMessage(event);
    FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    try {
      SAXParser saxParser = factory.newSAXParser();
      ZipFile zFile = new ZipFile(zipAbsolutePath);
      Enumeration enume = zFile.entries();
      
      ZipEntry zEntry;
      File f;
      while (enume.hasMoreElements()) {
        zEntry = (ZipEntry)enume.nextElement();
        f = new File(zEntry.getName());
        f.deleteOnExit();
        copyInputStream(zFile.getInputStream(zEntry), new BufferedOutputStream(new FileOutputStream(f)));
      }
      BufferedReader br = new BufferedReader(new FileReader(new File("manifest")));
      while (br.ready()) {
        String s = br.readLine();
        StringTokenizer strtok = new StringTokenizer(s, "=");
        String s2 = strtok.nextToken().trim();
        if (s2.equalsIgnoreCase("image")) {
          s2 = strtok.nextToken().trim();
          imageName = s2;
        }
        else if (s2.equalsIgnoreCase("ali")) {
          s2 = strtok.nextToken().trim();
          alignmentName = s2;
        }
      }

      if (!imageName.equals("") && !alignmentName.equals("")) {   
       	System.out.println("OPEN ALI TEST");
       	System.out.print("aliAbsolutePath= "+aliAbsolutePath);
     	System.out.print(" *** imageName= "+imageName);
     	System.out.println(" *** alignmentName= "+alignmentName);
        TAlignmentReader alignmentReader = new TAlignmentReader(aliAbsolutePath, imageName, alignmentName);
        saxParser.parse(new File(alignmentName), (DefaultHandler)alignmentReader);
        alignment = alignmentReader.getAlignment();
        String pathSeparator = System.getProperty("file.separator");
        String p = pathSeparator;
        String filename = aliAbsolutePath;
        int ix = filename.lastIndexOf(pathSeparator);
        if (ix > -1) p = filename.substring(0, ix + 1);
        alignment.setPath(p);
        alignment.setName(filename.substring(ix + 1) + ".zaf");
      }
    }
    catch (Throwable t) {
      t.printStackTrace();
    }
    return alignment;
  }
  public void finished() {
    if (alignment != null) {
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, alignment);
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, alignment.getView());
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, alignment, new Integer(0));
      TEventHandler.handleMessage(event);
      alignment.getView().setDividerLocation(0.9);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false));
      TEventHandler.handleMessage(event);
      event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment, Messages.getString("TOpenAlignmentWorker.11"), Color.blue); //$NON-NLS-1$
      TEventHandler.handleMessage(event);
      FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
      Enumeration enume = alignment.getGridModel().getSpots().elements();
      TSpot spot;
      while (enume.hasMoreElements()) {
        spot = (TSpot)enume.nextElement();
        spot.setLeft(alignment.getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent()));
        spot.setRight(alignment.getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent()));
        spot.setTop(alignment.getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent()));
        spot.setBottom(alignment.getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent()));
        spot.setGlobalLeft(alignment.getGridModel().getLeftSpot(spot, alignment.getGridModel().getRootBlock()));
        spot.setGlobalRight(alignment.getGridModel().getRightSpot(spot, alignment.getGridModel().getRootBlock()));
        spot.setGlobalTop(alignment.getGridModel().getTopSpot(spot, alignment.getGridModel().getRootBlock()));
        spot.setGlobalBottom(alignment.getGridModel().getBottomSpot(spot, alignment.getGridModel().getRootBlock()));
      }
      
      // mise a jour du nombre de canaux 
      int nbc = ((TImageModel)alignment.getImage().getImageModel()).getNumberOfChannels();
      AGScan.prop.setProperty("TColorParametersAction.nbcolors",nbc);
      
    }
  }
  public void copyInputStream(InputStream in, OutputStream out) {
    int len = 0;
    try {
      byte[] buffer = new byte[1024];
      while ((len = in.read(buffer)) >= 0) out.write(buffer, 0, len);
      in.close();
      out.close();
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
