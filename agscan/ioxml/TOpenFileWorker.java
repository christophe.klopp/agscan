/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @version 1.0
 */

package agscan.ioxml;

import ij.ImagePlus;
import ij.ImageStack;
import ij.io.FileInfo;
import ij.plugin.RGBStackMerge;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JOptionPane;

import net.sf.ij.jaiio.JAIReader;
import net.sf.ij.jaiio.JAIWriter;

import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TImageControler;
import agscan.data.element.image.TImage;
import agscan.data.model.TImageModel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.factory.TStandardImageModelFactory;
import agscan.plugins.SFormatPlugin;
import agscan.plugins.TPluginManager;
import agscan.statusbar.TStatusBar;

public class TOpenFileWorker extends SwingWorker {
	//private Vector filesVector = null;	//pour + tard generalisation ==> 29/03/07 modif
	String pathSeparator = System.getProperty("file.separator");
	private File[] files;
	private TImageModel model;
	private TImage image;
	//TODO le nom affiche est maintenant la 1ere image et un suffixe. A voir si c'est le mieux( de toute facon l'image a un nom)
	private String displayName = null;//added the 2005/11/02 - name used for the image in window tab
	TEvent event;
	String absolutPath = null ;
	
	
	public TOpenFileWorker(File[] f)
	{
		super();
		files = f;
		model = null;
		image = null;
	}
	
	
	//renvoie la TImage...
	public Object construct(boolean thread) {
		// on ecrit dans la status bar que l'on ouvre l'image
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TOpenFileWorker.0"), Color.red); //$NON-NLS-1$
		TEventHandler.handleMessage(event);
		// on met le curseur en mode "pourcentage effectue"
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(true));
		TEventHandler.handleMessage(event);
		
		TImage result = null;
		System.out.println("Open all dyes");
		result = alldyes();// unique case forall
		return result;
	}
	/**
	 * @return TImage of a radio experiment
	 */

	private TImage alldyes() {
		
		int i = files[0].getAbsolutePath().lastIndexOf((int)'.');//-1 if no extension
		//displayName =  files[0].getAbsolutePath().substring(0,i)+"_"+files.length+"DYES."+files[0].getAbsolutePath().substring(i+1,files[0].getAbsolutePath().length());
		displayName =  files[0].getAbsolutePath().substring(0,i)+"."+files[0].getAbsolutePath().substring(i+1,files[0].getAbsolutePath().length());
		ImagePlus[] imagePlus = new ImagePlus[files.length];
		for(int j=0;j<files.length;j++)
		{
			imagePlus[j] = new ImagePlus(files[j].getAbsolutePath());
		}
		//imagePlus[0].show();
		boolean ok = true;
		int wi = imagePlus[0].getWidth();
		int he = imagePlus[0].getHeight();
		for(int j=1;j<files.length;j++)
		{
			if(wi != imagePlus[j].getWidth()
			|| he != imagePlus[j].getHeight())ok = false;
		}
		// D'abord vérifier que les images sont compatibles en tailles 
		if (!ok) {
			event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.MESSAGE_DIALOG, Messages.getString("TOpenFileWorker.20")); //$NON-NLS-1$
			TEventHandler.handleMessage(event);			
			return null;
		}			
		
		// two cases: 
		//  CASE1- imageJ doesn't know it and the imagePlus is false!
		//	two subcases : 
		//		SUBCASE1- a plugin is installed for this kind of file
		//		SUBCASE2- this file type is unknown
		//  CASE2- imageJ knows this kind of file
		
		//CASE1
		if (imagePlus[0].getHeight()==0 && imagePlus[0].getWidth()==0) {
			System.out.println("ce n'est pas une image reconnue sans plugin ou dimensions 0x0 "); //$NON-NLS-1$
			// the imagePlus is false ( the basic one created for all files)
			// we have to search if there is a plugin that allow to create the imagePlus for
			// this kind of file
			
			//SUBCASE1
			// we have to know the extension of the file we want to open
			
		
			//    	no extension!
			if (i==-1) {
				System.out.println("no extension!!!"); //$NON-NLS-1$
				JOptionPane.showMessageDialog(null, Messages.getString("TOpenFileWorker.23") + files[0].getName() + //$NON-NLS-1$
						Messages.getString("TOpenFileWorker.24"), //$NON-NLS-1$
						Messages.getString("TOpenFileWorker.25"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
				return null;	
			}
			else {
				String extension = files[0].getAbsolutePath().substring(i+1,files[0].getAbsolutePath().length());
				// on prend l'extension de la 1ere image (on suppose qd meme que les deux images ont la meme)
				String[] imagePath = new String[files.length];
				for (int j=0;j<files.length;j++)
				{
					imagePath[j] = files[j].getAbsolutePath();
				}
				Hashtable exts = TPluginManager.extensions;// hashtable of extensions known by plugins
				System.out.println("extension = "+extension); //$NON-NLS-1$
				
				if (TPluginManager.isKnownExtension(extension))	{
					System.out.println(extension +" = known extension !");	 //$NON-NLS-1$
										
					//stockage LUT INVERSE				
					//	22/08/05... en fluo :lut inversé aussi sauf pour l'affichage		
					ImagePlus[] ip = new ImagePlus[files.length];
					System.out.println("Nombre d'images = "+files.length);
					for (int j=0;j<files.length;j++)
					{
						ip[j] = (ImagePlus)SFormatPlugin.getImagePlus(extension,imagePath[j]);
						ip[j] = TImageControler.setInvertedLUT(ip[j],true);
					}
					TStandardImageModelFactory factory = new TStandardImageModelFactory(extension);					  
					

					ImageStack images = new ImageStack(ip[0].getWidth(),ip[0].getHeight());				

					int j; //-1 if no extension
					String directoryPathName  =  files[0].getAbsolutePath().substring(0,1)+File.separator;
					String shortName = files[0].getAbsolutePath().substring(2,files[0].getAbsolutePath().length());
					System.out.println("directoryPathName="+directoryPathName);
					System.out.println("shortName="+shortName);
					for (int k=0;k<files.length;k++)
					{
						j = files[k].getAbsolutePath().lastIndexOf(File.separator);
						shortName = files[k].getAbsolutePath().substring(j+1,files[k].getAbsolutePath().length());
						images.addSlice(shortName,ip[k].getProcessor());	
					}		
					
					
					//affichage LUT NON INVERSE										
					// 22/08/05 on veut le LUT non-inversé pour l'image RGB a afficher
					// 29/07/08 ajout de la non inversion 
					if (files.length > 1){
						for (int k=0;k<files.length;k++)
						{
							ip[k] = TImageControler.setInvertedLUT(ip[k],false);
						}
					}
					
					RGBStackMerge stackMerge = new RGBStackMerge();
					// utilisation de mergeStacks(Width,Height,stackSize,R,G,B,keep);
					//ImageStack rgbStack = stackMerge.mergeStacks(ip[0].getWidth(),ip[0].getHeight(),ip[0].getStackSize(),ip[0].getStack(),ip[1].getStack(),ip[2].getStack(),true);
					ImageStack rgbStack = new ImageStack();
					if (files.length > 2){
						rgbStack = stackMerge.mergeStacks(imagePlus[0].getWidth(),imagePlus[0].getHeight(),imagePlus[0].getStackSize(),imagePlus[0].getStack(),imagePlus[1].getStack(),imagePlus[2].getStack(),true);
					}
					if (files.length == 2){
						rgbStack = stackMerge.mergeStacks(imagePlus[0].getWidth(),imagePlus[0].getHeight(),imagePlus[0].getStackSize(),imagePlus[0].getStack(),imagePlus[1].getStack(),null,true);
					}
					if (files.length == 1){
						rgbStack = images;
					}
					// creation de l'imagePlus RGB
					ImagePlus rgb = new ImagePlus(displayName, rgbStack); //TODO rename??
					//rgb.show();
					model = factory.createImageModel(images);
					event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(80));
					TEventHandler.handleMessage(event);
					//if (model != null) image = new TImage(imagePath1+"__"+imagePath2, model);// a mettre en vrai :pas test
					if (model != null) image = new TImage(displayName, model);//test //$NON-NLS-1$
					image.getImageView().setDisplayImage(rgb); // pour du fluo, on met la rgb en image 8 bits
					// on stoppe le curseur du mode cherche
					event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, null, new Boolean(false));
					TEventHandler.handleMessage(event);
					return image;// it's OK
					
				}
				else 	{
					System.out.println(extension +" = unknown extension !"); //$NON-NLS-1$
					// if we are here we don't return the image = no plugin available for this image
					JOptionPane.showMessageDialog(null, Messages.getString("TOpenFileWorker.38") + files[0].getName() + //$NON-NLS-1$
							Messages.getString("TOpenFileWorker.39"), //$NON-NLS-1$
							Messages.getString("TOpenFileWorker.40"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
					return null;
				}
			}
		}
		else { 
			//CASE2
			// imageJ knows the kind of image 
			// we just have to verify if it's a 16 bits unsigned (only )
			System.out.println("FLUO3 + TYPE IMAGE CONNU"); //$NON-NLS-1$
			boolean ok16B = true;
			for(int j=0;j<files.length;j++)
			{
				if (imagePlus[j].getType()!=ImagePlus.GRAY16)ok16B = false;
			}
			if (!ok16B){
				JOptionPane.showMessageDialog(null, Messages.getString("TOpenFileWorker.42") + files[0].getName() + //$NON-NLS-1$
						Messages.getString("TOpenFileWorker.43") + files[1].getName() + //$NON-NLS-1$
						Messages.getString("TOpenFileWorker.43") + files[2].getName() + //$NON-NLS-1$						
						Messages.getString("TOpenFileWorker.44"), //$NON-NLS-1$
						Messages.getString("TOpenFileWorker.45"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
				return null;	
			}
			else {
				event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, null, new Boolean(true));
				TEventHandler.handleMessage(event);
				// TODO definir l'extension si on veut la passer en param
				//TStandardImageModelFactory factory = new TStandardImageModelFactory("ext_a_def"); //$NON-NLS-1$
				boolean isMultiTiff = false;
				ImagePlus[] tab=null;

				System.out.println("MultiTiff = "+isMultiTiff+" "+files.length);

				if (files.length == 1){
					int w=0;
					int h=0;
					FileInfo info;
					ImagePlus im;

					try{
						tab = JAIReader.read(new File(files[0].getAbsolutePath()) ,null);
						// get absolute path
						int index = files[0].getAbsolutePath().lastIndexOf(pathSeparator);
						absolutPath = files[0].getAbsolutePath().substring(0,index+1);
						System.out.println("absolutPath = "+absolutPath);
						System.out.println("TOpenFileWorker File name = "+files[0].getAbsolutePath()+" "+tab.length);

						if((tab[0].getStackSize() > 1) || (tab.length> 1)){
							isMultiTiff = true;
							im=tab[0];
							Vector v=new Vector();
							System.out.println("TOpenFileWorker etStackSize ="+tab[0].getStackSize());
							if(tab[0].getStackSize() != 1){
								System.out.println("Cas d'un multi tif");
								tab = stackToImages(im,files[0].getAbsolutePath());
							}else{
								//recherche des dimensions max qui correspondent aux bonnes images
								for(int j=0;j<tab.length;j++){
									info=tab[j].getFileInfo();
									if(info.width>w)
										w=info.width;
									if(info.height>h)
										h=info.height;
								}
								//on stocke les bonnes images
								for(int j=0;j<tab.length;j++){
									info=tab[j].getFileInfo();
									if((info.width == w) && (info.height == h))
										v.add(tab[j]);
								}
								//on repasse sous la forme de tableau
//								tab=null;
								tab=new ImagePlus[v.size()];
								int no=65;
								for(int j=0;j<v.size();j++){
									tab[j]=(ImagePlus)v.elementAt(j);
									String s=tab[j].getTitle();
									System.out.println("getTitle = "+s);
									//tab[j].setTitle(createName(files[0].getAbsolutePath(),s,no+j,true));
									//tab[j].setTitle(tab[j].getTitle());

									//
									//System.out.println("getTitle = "+s);
									index = s.lastIndexOf(' ');
									if (index != -1){
										s = s.substring(0, index);
									}
									String s2 = createName(files[0].getAbsolutePath(),s,no+j,true);
									tab[j].setTitle(s2); 
									System.out.println("createName(files[0].getAbsolutePath()"+s2);
									//tab[j].setTitle(createName("./",s,no+j,true));
								}
							}

						}
						saveTiffs(tab);
					}
					catch(Exception e){
							System.err.println("Excpetion non geree dans le PlugIn ImageJ");
					}
					
				}
				
				System.out.println("MultiTiff = "+isMultiTiff);
				TStandardImageModelFactory factory = new TStandardImageModelFactory("tif"); //$NON-NLS-1$
				RGBStackMerge stackMerge = new RGBStackMerge();
				// utilisation de mergeStacks(Width,Height,stackSize,R,G,B,keep);
				ImageStack rgbStack = new ImageStack();
				ImageStack images = null;
				
				if (isMultiTiff == false){
					images = new ImageStack(imagePlus[0].getWidth(),imagePlus[0].getHeight());
					String[] imagePath = new String[files.length];
					for (int j=0;j<files.length;j++)
					{
						imagePath[j] = files[j].getAbsolutePath();
					}

					// stockage de la LUT INVERSE
					for(int j=0;j<files.length;j++){
						imagePlus[j] = TImageControler.setInvertedLUT(imagePlus[j],true);
					}
					
						
					int j; 
					String directoryPathName  =  files[0].getAbsolutePath().substring(0,1)+File.separator;
					String shortName = files[0].getAbsolutePath().substring(2,files[0].getAbsolutePath().length());
					System.out.println("directoryPathName = "+directoryPathName);
					System.out.println("shortName = "+shortName);
					System.out.println("test = "+shortName);
					
					for (int k=0;k<files.length;k++)
					{
						j = files[k].getAbsolutePath().lastIndexOf(File.separator);
						shortName = files[k].getAbsolutePath().substring(j+1,files[k].getAbsolutePath().length());
						images.addSlice(shortName,imagePlus[k].getProcessor());
					}

					//affichage LUT NON INVERSE				
					//22/08/05 on veut le LUT non-inversé pour l'image RGB a afficher
					
					//				stockage LUT INVERSE
					System.out.println("multiple mono tif / getBackground(imagePlus[0]) ="+getBackground(imagePlus[0]));
					if ((files.length > 1) || (getBackground(imagePlus[0]) < 32000)){	

					}
					
					if ((files.length > 1) && (getBackground(imagePlus[0]) > 32000)){
						//nothing
					}else{
						for (int k=0;k<files.length;k++)
						{
							imagePlus[k] = TImageControler.setInvertedLUT(imagePlus[k],false);
//							//imagePlus[k].getProcessor().invertLut();
						}
					}
				
					if (files.length > 2){
						rgbStack = stackMerge.mergeStacks(imagePlus[0].getWidth(),imagePlus[0].getHeight(),imagePlus[0].getStackSize(),imagePlus[0].getStack(),imagePlus[1].getStack(),imagePlus[2].getStack(),true);
					}
					if (files.length == 2){
						rgbStack = stackMerge.mergeStacks(imagePlus[0].getWidth(),imagePlus[0].getHeight(),imagePlus[0].getStackSize(),imagePlus[0].getStack(),imagePlus[1].getStack(),null,true);
					}
					if (files.length == 1){
						rgbStack = images;
					}

				}else{
					images = new ImageStack(tab[0].getWidth(),tab[0].getHeight());
					// filing the stack
					
					for (int k=0;k<tab.length;k++)
					{
						images.addSlice(tab[k].getTitle(),tab[k].getProcessor());
					}
					// inverting LUT
					System.out.println("multi tiff / getBackground(tab[0]) ="+getBackground(tab[0]));
					if (getBackground(tab[0]) > 32000){
					//if (2 == 2){
						for (int k=0;k<tab.length;k++)
						{
							//tab[k] = TImageControler.setInvertedLUT(tab[k],false);
							tab[k].getProcessor().invertLut();
						}
					}
					
					// creating RGB stack
					if (tab.length > 2){
						rgbStack = stackMerge.mergeStacks(tab[0].getWidth(),tab[0].getHeight(),tab[0].getStackSize(),tab[0].getStack(),tab[1].getStack(),tab[2].getStack(),true);
					}
					if (tab.length == 2){
						rgbStack = stackMerge.mergeStacks(tab[0].getWidth(),tab[0].getHeight(),tab[0].getStackSize(),tab[0].getStack(),tab[1].getStack(),null,true);
					}
					

				}
				ImagePlus rgb = new ImagePlus(displayName, rgbStack); //$NON-NLS-1$
				model = factory.createImageModel(images);
				event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(80));
				TEventHandler.handleMessage(event);
				//if (model != null) image = new TImage(imagePath1+"__"+imagePath2, model);// a mettre en vrai :pas test
				if (model != null) image = new TImage(displayName, model);//test //$NON-NLS-1$
				image.getImageView().setDisplayImage(rgb); // pour du fluo, on met la rgb en image 8 bits
				// on stoppe le curseur du mode cherche
				event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, null, new Boolean(false));
				TEventHandler.handleMessage(event);
				return image;// it's OK
			}
		}
		
	}
	
	
	
	public void finished() {
		if (image != null) {
			model.setReference(image);
			TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, image);
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, image.getView());
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TOpenFileWorker.50"), Color.blue); //$NON-NLS-1$
			TEventHandler.handleMessage(event);
		}
	}
	
	private static String createName(String adresse,String origine,int no,boolean compress){
		//origine de la forme XXXXXX.tif
		String nom=null;
		String part0=null;
		String part1=null;
		String part2=null;
		//if(!compress){
			part0=adresse.substring(0, adresse.length()-origine.length());
			part1=origine.substring(0,origine.length()-4);
			part2=origine.substring(origine.length()-4,origine.length());
//		}
//		else{
//			part0=adresse.substring(0, adresse.length()-origine.length()+6);
//			part1=origine.substring(0,origine.length()-10);
//			part2=origine.substring(origine.length()-10,origine.length()-6);
//		}
			
		nom=part1+"_"+(char)no+part2;
		
		return nom;
		
	}
	
	private static ImagePlus[] stackToImages(ImagePlus imp,String adress){
		
		ImagePlus [] tab=null;
		String sLabel = imp.getTitle();
        String sImLabel = "";
        ImageProcessor ip,newip;
        ImageStack stack = imp.getStack();

        int sz = stack.getSize();
        System.out.println("stack.getSize() = "+stack.getSize());
        tab=new ImagePlus[sz];
     
        int currentSlice = imp.getCurrentSlice();  // to reset ***
        int m=64+sz;//title

        for(int n=1;n<=sz;++n) {
        	imp.setSlice(n);   // activate next slice ***

            // Get current image processor from stack.  What ever is
            // used here should do a COPY pixels from old processor to
            // new. For instance, ImageProcessor.crop() returns copy.
            ip = imp.getProcessor(); // ***
            newip = ip.createProcessor(ip.getWidth(),ip.getHeight());
            newip.setPixels(ip.getPixelsCopy());
            //newip.invert();
            // Create a suitable label, using the slice label if possible
            sImLabel = imp.getStack().getSliceLabel(n);
            if (sImLabel == null || sImLabel.length() < 1) {
            	
            	sImLabel = createName(adress,sLabel,m,false);
            	System.out.println("sImLabel"+sImLabel);
            }

            // Create new image corresponding to this slice.
            ImagePlus im = new ImagePlus(sImLabel, newip);
            im.setCalibration(imp.getCalibration());

            // Stocke l'image
            tab[n-1]=im;
            tab[n-1].setTitle(sImLabel);
            m--;
        }

        // Reset original stack state.
        imp.setSlice(currentSlice); // ***
        if(imp.isProcessor()) {
            ip = imp.getProcessor();
            ip.setPixels(ip.getPixels()); //***
        }
        imp.setSlice(currentSlice);

		return tab;
	}
	
	private void saveTiffs ( ImagePlus [] tab ){
		
		//sauvegarde des images decompressees et decomposees
		JAIWriter jw=new JAIWriter();
		try{
			//ecriture du fichier
			for(int i=0;i<tab.length;i++){
				jw.write(absolutPath+tab[i].getTitle(),tab[i] );
				System.out.println("Ecriture reussie"+absolutPath+tab[i].getTitle());
			}
			System.out.println("Sauvegarde des images terminees");
		}
		catch(FileNotFoundException fnfe){
			System.out.println("Probleme fichier non trouve ");
		}
		catch(IOException ioe){
			System.out.println("Impossible d'ecrire dans le fichier");
		}
		
	}
	
	private double getBackground(ImagePlus im) {
		// background of the first 2 lines
		//tab[]
		float tab[][] =  im.getProcessor().getFloatArray();
		double bachground = 0;
		System.out.println("dim = "+im.getWidth()+" x "+im.getHeight());
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < im.getWidth(); j++) {
				bachground = bachground + tab[j][i];
			}
		}
		return bachground / (im.getWidth() * 2);
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
