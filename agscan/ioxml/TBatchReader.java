package agscan.ioxml;

import java.util.StringTokenizer;
import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import agscan.AGScan;
import agscan.algo.factory.TGlobalAlignmentAlgorithmFactory;
import agscan.algo.factory.TLocalAlignmentAlgorithmFactory;
import agscan.algo.globalalignment.TTemplateMatchingGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithmPanel;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.data.controler.TImageControler;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.batch.job.TExportResultsJob;
import agscan.data.model.batch.job.TJobBuilder;
import agscan.data.view.TBatchComPanel;
import agscan.data.view.TBatchView;
import agscan.data.view.table.TBatchTablePanel;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.menu.TMenuManager;
import agscan.menu.action.TAction;
import agscan.menu.action.TGlobalAlignmentSnifferAction;
import agscan.menu.action.TGlobalAlignmentTemplateMatchingAction;
import agscan.menu.action.TLocalAlignmentBlocAction;
import agscan.menu.action.TLocalAlignmentBlocTemplateAction;
import agscan.menu.action.TLocalAlignmentGridAction;
import agscan.menu.action.TProcessChooserAction;
import agscan.plugins.TPluginManager;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchReader extends DefaultHandler {
  private TBZGridReader gridReader;
  private boolean gridSegment, saveImage;
  private TGrid grid;
  private TAbstractJob job;
  private Vector[] images;
  private int nbCh;
  private Vector stat, parameters;
  private TBatch batch;
  private String dir = null;
  Vector<String> v_al= new Vector<String>();
  Vector<String> v_qu= new Vector<String>();
  boolean snap = false;
  boolean tiff = false;
  boolean export = false;
  
  public TBatchReader(String s) {
    super();
    gridReader = new TBZGridReader(s);
    gridSegment = saveImage = false;
    stat = new Vector();
    batch = null;
  }
  public void startDocument() throws SAXException {
    gridReader.startDocument();
  }
  public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
    String aName, id, params, type, name, status, channel;
    StringTokenizer strtok;
    int algoId;
    Object[] globPar, locPar, sdPar;

    if (qName.equals("channels")) {
    	for (int i = 0; i < attrs.getLength(); i++) {
            aName = attrs.getLocalName(i);
            if ("".equals(aName)) aName = attrs.getQName(i);
            if (aName.equals("nbchan")) 
            {
            	nbCh = Integer.parseInt(attrs.getValue(i));
            	images = new Vector[nbCh];
            	for (int k=0;k<nbCh;k++)
            	{
            		images[k] = new Vector();
            	}
            }
        }
    }
    else if (qName.equals("grid")) {
      gridSegment = true;
    }
    else if (qName.equals("output")) {
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("dir")) dir = attrs.getValue(i);
      }
    }
    else if (qName.equals("saveImages")) {
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("value")) saveImage = new Boolean(attrs.getValue(i)).booleanValue();
      }
    }
    else if (qName.equals("snapShot")) {
      for (int i = 0; i < attrs.getLength(); i++) {
          aName = attrs.getLocalName(i);
          if ("".equals(aName)) aName = attrs.getQName(i);
          if (aName.equals("value")) snap = new Boolean(attrs.getValue(i)).booleanValue();
      }
    }
    else if (qName.equals("tiffSave")) {
          for (int i = 0; i < attrs.getLength(); i++) {
              aName = attrs.getLocalName(i);
              if ("".equals(aName)) aName = attrs.getQName(i);
              if (aName.equals("value")) tiff = new Boolean(attrs.getValue(i)).booleanValue();
          }
    }
    else if (qName.equals("job")) {
      TJobBuilder.createJob();
    }
    else if (qName.equals("job_elem")) {
      id = params = "";
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("id")) id = attrs.getValue(i);
        if (aName.equals("params")) params = attrs.getValue(i);
      }
      if (id.startsWith("AP")) {
    	algoId = Integer.parseInt(id.substring(2,4));
    	v_al.addElement(TPluginManager.plugs[algoId].getName());
      }
      else if (id.startsWith("QP")) {
      	algoId = Integer.parseInt(id.substring(2,4));
      	v_qu.addElement(TPluginManager.plugs[algoId].getName());
      }
      else if (id.startsWith("AG")) {
    	//parameters = new Vector();
        algoId = Integer.parseInt(id.substring(2));
        switch (algoId) {
          case 1:
            /*strtok = new StringTokenizer(params, "|");
            parameters.addElement(new Double(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Double(strtok.nextToken()));
            parameters.addElement(strtok.nextToken());
            sdPar = new Object[3];
            sdPar[0] = new Integer(strtok.nextToken());
            sdPar[1] = new Integer(strtok.nextToken());
            sdPar[2] = new Integer(strtok.nextToken());
            parameters.addElement(sdPar);*/
        	break;
            
          case 2:
            /*strtok = new StringTokenizer(params, "|");
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Double(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            globPar = new Object[11];
            globPar[0] = new Integer(strtok.nextToken());
            globPar[1] = new Integer(strtok.nextToken());
            globPar[2] = new Integer(strtok.nextToken());
            globPar[3] = new Integer(strtok.nextToken());
            globPar[4] = new Integer(strtok.nextToken());
            globPar[5] = new Integer(strtok.nextToken());
            globPar[6] = new Integer(strtok.nextToken());
            globPar[7] = new Integer(strtok.nextToken());
            globPar[8] = new Double(strtok.nextToken());
            globPar[9] = new Boolean(strtok.nextToken());
            locPar = new Object[4];
            locPar[0] = new Double(strtok.nextToken());
            locPar[1] = new Double(strtok.nextToken());
            locPar[2] = strtok.nextToken();
            sdPar = new Object[3];
            sdPar[0] = new Integer(strtok.nextToken());
            sdPar[1] = new Integer(strtok.nextToken());
            sdPar[2] = new Integer(strtok.nextToken());
            locPar[3] = sdPar;
            globPar[10] = locPar;
            parameters.addElement(globPar);*/
        	v_al.addElement(((TAction)TMenuManager.getAction(TGlobalAlignmentSnifferAction.getID())).getValue(TProcessChooserAction.NAME).toString());
            break;
          case 3:
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastSearchAngle));
//        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastGridCenterCross));
//       	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastSpotCicularity));
//        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastKernelSize));
//        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMinSpotSize));
//        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMaxSpotSize));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.laststddevh));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.laststddevv));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastpercentcol));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastpercentrow));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastmaxratioh));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastmaxratiov));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMaxAreaSize));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMinAreaSize));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastKindKernel));
        	parameters.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastActiSize));
        	
        	v_al.addElement(((TAction)TMenuManager.getAction(TGlobalAlignmentTemplateMatchingAction.getID())).getValue(TProcessChooserAction.NAME).toString());
        	break;
        }
        //TJobBuilder.addGlobalAlignmentJob(TGlobalAlignmentAlgorithmFactory.getAlgoName(algoId), parameters);
      }
      else if (id.startsWith("AL")) {
        parameters = new Vector();
        algoId = Integer.parseInt(id.substring(2));
        switch (algoId) {
          case 0:
        	/*parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapSensitivity));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapIterations));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunSensitivity));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunIterations));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunSensitivity));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunIterations));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunSensitivity));
            parameters.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunIterations));
            parameters.addElement(new Double(TLocalAlignmentBlockAlgorithm.lastSensitivityThreshold));
            parameters.addElement(new Boolean(TLocalAlignmentBlockAlgorithm.lastGridCheck));
            locPar = new Object[4];
            locPar[0] = new Double(TLocalAlignmentBlockAlgorithm.lastLocT);
            locPar[1] = new Double(TLocalAlignmentBlockAlgorithm.lastLocC);
            locPar[2] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmName;
            locPar[3] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmParameters;
            parameters.addElement(locPar);*/
        	v_al.addElement(((TAction)TMenuManager.getAction(TLocalAlignmentBlocTemplateAction.getID())).getValue(TProcessChooserAction.NAME).toString());
            break;
          case 1:
            /*strtok = new StringTokenizer(params, "|");
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Double(strtok.nextToken()));
            locPar = new Object[4];
            locPar[0] = new Double(strtok.nextToken());
            locPar[1] = new Double(strtok.nextToken());
            locPar[2] = strtok.nextToken();
            sdPar = new Object[3];
            sdPar[0] = new Integer(strtok.nextToken());
            sdPar[1] = new Integer(strtok.nextToken());
            sdPar[2] = new Integer(strtok.nextToken());
            locPar[3] = sdPar;
            parameters.addElement(locPar);*/
        	v_al.addElement(((TAction)TMenuManager.getAction(TLocalAlignmentGridAction.getID())).getValue(TProcessChooserAction.NAME).toString());
            break;
          case 2:
            /*strtok = new StringTokenizer(params, "|");
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Integer(strtok.nextToken()));
            parameters.addElement(new Double(strtok.nextToken()));
            parameters.addElement(new Boolean(strtok.nextToken()));
            locPar = new Object[4];
            locPar[0] = new Double(strtok.nextToken());
            locPar[1] = new Double(strtok.nextToken());
            locPar[2] = strtok.nextToken();
            sdPar = new Object[3];
            sdPar[0] = new Integer(strtok.nextToken());
            sdPar[1] = new Integer(strtok.nextToken());
            sdPar[2] = new Integer(strtok.nextToken());
            locPar[3] = sdPar;
            parameters.addElement(locPar);*/
        	v_al.addElement(((TAction)TMenuManager.getAction(TLocalAlignmentBlocAction.getID())).getValue(TProcessChooserAction.NAME).toString());
            break;
        }
        
        //TJobBuilder.addLocalAlignmentJob(TLocalAlignmentAlgorithmFactory.getAlgoName(algoId), parameters);
      }/*
      else if (id.equals("R+90"))
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_PLUS_90);
      else if (id.equals("R-90"))
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_MOINS_90);
      else if (id.equals("R180"))
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_180);
      else if (id.equals("FH")) {
        TJobBuilder.addTreatmentJob(TImageControler.HORIZONTAL_FLIP);
      }
      else if (id.equals("FV")) {
        TJobBuilder.addTreatmentJob(TImageControler.VERTICAL_FLIP);
      }*/
      else if (id.equals("QIC"))
        v_qu.addElement("BZScan quantification");
      /*else if (id.equals("QIV"))
        TJobBuilder.addComputeJob(TQuantifImageComputedAlgorithm.NAME);
      else if (id.equals("QFC"))
        TJobBuilder.addComputeJob(TQuantifFitConstantAlgorithm.NAME);
      else if (id.equals("QFV"))
        TJobBuilder.addComputeJob(TQuantifFitComputedAlgorithm.NAME);
      else if (id.equals("QM"))
        TJobBuilder.addComputeJob(TComputeQMAlgorithm.NAME);
      else if (id.equals("CF"))
        TJobBuilder.addComputeJob(TComputeFitCorrectionAlgorithm.NAME);
      else if (id.equals("CO"))
        TJobBuilder.addComputeJob(TComputeOvershiningCorrectionAlgorithm.NAME);*/
      else if (id.equals("EX1"))
    	export = true;
        //TJobBuilder.addExportResultsJob(TExportResultsJob.TEXT);
      else if (id.equals("EX2"))
    	export = true;  
        //TJobBuilder.addExportResultsJob(TExportResultsJob.MAGE_ML);
    }
    else if (qName.equals("image")) {
      type = name = status = channel = "";
      for (int i = 0; i < attrs.getLength(); i++) {
        aName = attrs.getLocalName(i);
        if ("".equals(aName)) aName = attrs.getQName(i);
        if (aName.equals("type")) type = attrs.getValue(i);
        if (aName.equals("name")) name = attrs.getValue(i);
        if (aName.equals("status")) status = attrs.getValue(i);
        if (aName.equals("channel")) channel = attrs.getValue(i);
      }
      if (type.equals("image") || type.equals("alignment")) 
      {
    		int nb = Integer.parseInt(channel);
    		images[nb].addElement(name);
            stat.addElement(new Integer(Integer.parseInt(status)));
      }
    }
    if (gridSegment) {
      gridReader.startElement(namespaceURI, sName, qName, attrs);
    }
  }
  public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
    if (gridSegment)
      gridReader.endElement(namespaceURI, sName, qName);
    if (qName.equals("grid")) {
      gridSegment = false;
      grid = gridReader.getGrid();
    }
  }
  public void endDocument() throws SAXException {
    gridReader.endDocument();
    TAbstractJob job = TJobBuilder.getJob();
    batch = new TBatch(grid, images, stat.elements());
    
    /* ajoute a la liste de config les taches qu'il faut */
    TBatchView view = (TBatchView)batch.getView();
    TBatchTablePanel tabpan = (TBatchTablePanel)view.getTablePanel();
    TBatchComPanel bcp = tabpan.getBCP();
    //bcp.setNbChannels(nbCh);
    AGScan.prop.setProperty("TColorParametersAction.nbcolors",nbCh);
    for (int i=0;i<v_al.size();i++)
    {
    	bcp.addFromAlToConfig(v_al.elementAt(i));
    }
    for (int i=0;i<v_qu.size();i++)
    {
    	bcp.addFromQuToConfig(v_qu.elementAt(i));
    }
    if (snap)bcp.addSnapToConfig();
    if (tiff)bcp.addTiffToConfig();
    if(export)bcp.addExportToConfig();
    /**/
    
    batch.setOutputDirectory(dir);
    TBatchModelNImages batchModel = (TBatchModelNImages)batch.getModel();
    System.err.println("TACHES : "+ job.getStringID());
    batchModel.setJob(job);
    batchModel.setSaveImages(saveImage);
    if(batchModel.getRowCount()>0)
    {
    		bcp.setChannels(false);
  	}
    ((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
//    for (Enumeration enu = images.elements(); enu.hasMoreElements(); )
//      batchModel.addImage((String)enu.nextElement());
  }
  public void characters(char buf[], int offset, int len) throws SAXException {
    gridReader.characters(buf, offset, len);
  }
  public TBatch getBatch() {
    return batch;
  }
}
