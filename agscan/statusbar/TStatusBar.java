/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.statusbar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;

public class TStatusBar implements TBZMessageListener {
	public final static int GLOBAL_ACTION_LABEL = 1;
	public final static int GLOBAL_ACTION_PROGRESS_BAR = 2;
	public final static int GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE = 3;
	public final static int GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT = 4;
	public final static int UPDATE = 5;
	public final static int MOUSE_POSITION = 6;
	public final static int IMAGE_ACTION_LABEL = 7;
	public final static int IMAGE_ACTION_PROGRESS_BAR = 8;
	public final static int IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT = 9;
	public final static int GRID_ACTION_LABEL = 10;
	public final static int SPOT_PERCENT_LABEL = 11;
	public static final int ALGO_PROGRESS = 12;
	
	private JPanel statusBarPanel;
	private JLabel globalActionLabel;
	private JProgressBar globalActionProgressBar;
	private JLabel imageActionLabel;
	private JProgressBar imageActionProgressBar;
	private JLabel mousePositionLabel;
	private JLabel zoomLabel;
	private JLabel modifiedDataLabel;
	private JLabel gridActionLabel;
	private JLabel spotPercentLabel;
	private JProgressBar algorithmProgress;
	private JButton cancelAlgoButton;
	private JSlider prioritySlider;
	private Border border = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
	
	public TStatusBar() {
		statusBarPanel = new JPanel();
		statusBarPanel.setLayout(new BoxLayout(statusBarPanel, BoxLayout.X_AXIS));
		globalActionLabel = new JLabel(Messages.getString("TStatusBar.0")); //$NON-NLS-1$
		globalActionLabel.setForeground(Color.blue);
		globalActionLabel.setBorder(border);
		globalActionLabel.setPreferredSize(new Dimension(150, 20));
		globalActionLabel.setMaximumSize(new Dimension(150, 20));
		globalActionProgressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
		globalActionProgressBar.setValue(0);
		globalActionProgressBar.setStringPainted(false);
		globalActionProgressBar.setBorder(border);
		globalActionProgressBar.setPreferredSize(new Dimension(100, 20));
		globalActionProgressBar.setMaximumSize(new Dimension(100, 20));
		imageActionLabel = new JLabel(""); //$NON-NLS-1$
		imageActionLabel.setForeground(Color.blue);
		imageActionLabel.setBorder(border);
		imageActionLabel.setPreferredSize(new Dimension(150, 20));
		imageActionLabel.setMaximumSize(new Dimension(150, 20));
		imageActionProgressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
		imageActionProgressBar.setValue(0);
		imageActionProgressBar.setStringPainted(false);
		imageActionProgressBar.setBorder(border);
		imageActionProgressBar.setPreferredSize(new Dimension(100, 20));
		imageActionProgressBar.setMaximumSize(new Dimension(100, 20));
		mousePositionLabel = new JLabel(""); //$NON-NLS-1$
		mousePositionLabel.setForeground(Color.blue);
		mousePositionLabel.setBorder(border);
		mousePositionLabel.setPreferredSize(new Dimension(80, 20));
		mousePositionLabel.setMaximumSize(new Dimension(80, 20));
		zoomLabel = new JLabel(""); //$NON-NLS-1$
		zoomLabel.setForeground(Color.blue);
		zoomLabel.setBorder(border);
		zoomLabel.setPreferredSize(new Dimension(50, 20));//35 avant integration, 50 apres - 14/09/05
		zoomLabel.setMaximumSize(new Dimension(50, 20));//35 avant integration, 50 apres - 14/09/05
		modifiedDataLabel = new JLabel(""); //$NON-NLS-1$
		modifiedDataLabel.setBorder(border);
		modifiedDataLabel.setPreferredSize(new Dimension(20, 20));
		modifiedDataLabel.setMaximumSize(new Dimension(20, 20));
		gridActionLabel = new JLabel(""); //$NON-NLS-1$
		gridActionLabel.setForeground(Color.blue);
		gridActionLabel.setBorder(border);
		gridActionLabel.setPreferredSize(new Dimension(300, 20));//150 avant integration, 300 apres - 14/09/05
		gridActionLabel.setMaximumSize(new Dimension(300, 20));//150 avant integration, 300 apres - 14/09/05
		spotPercentLabel = new JLabel(""); //$NON-NLS-1$
		spotPercentLabel.setForeground(Color.blue);
		spotPercentLabel.setBorder(border);
		spotPercentLabel.setPreferredSize(new Dimension(50, 20));
		spotPercentLabel.setMaximumSize(new Dimension(50, 20));
		algorithmProgress = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
		algorithmProgress.setValue(0);
		algorithmProgress.setStringPainted(true);
		algorithmProgress.setBorder(border);
		algorithmProgress.setForeground(Color.blue);
		algorithmProgress.setPreferredSize(new Dimension(100, 20));
		algorithmProgress.setMaximumSize(new Dimension(100, 20));
		init(null);
	}
	public void init(TDataElement element) {
		ImageIcon icon;
		statusBarPanel.removeAll();
		statusBarPanel.add(globalActionLabel);
		statusBarPanel.add(globalActionProgressBar);
		statusBarPanel.add(Box.createHorizontalGlue());
		if (element instanceof TImage) {
			statusBarPanel.add(imageActionLabel);
			statusBarPanel.add(imageActionProgressBar);
			statusBarPanel.add(mousePositionLabel);
			statusBarPanel.add(zoomLabel);
			statusBarPanel.add(modifiedDataLabel);
			if (element.getModel().getModif() == 0)
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_obj.gif")); //$NON-NLS-1$
			else
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_red_obj.gif")); //$NON-NLS-1$
			modifiedDataLabel.setIcon(icon);
			if (element.getView().getGraphicPanel().getZoom() > 0)
				zoomLabel.setText(" ".concat(String.valueOf(element.getView().getGraphicPanel().getZoom())).concat("x"));
			else
				zoomLabel.setText(" 1/".concat(String.valueOf(-element.getView().getGraphicPanel().getZoom())).concat(" x"));
		}
		else if (element instanceof TGrid) {
			statusBarPanel.add(gridActionLabel);
			statusBarPanel.add(mousePositionLabel);
			statusBarPanel.add(zoomLabel);
			statusBarPanel.add(modifiedDataLabel);
			if (element.getModel().getModif() == 0)
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_obj.gif")); //$NON-NLS-1$
			else
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_red_obj.gif")); //$NON-NLS-1$
			modifiedDataLabel.setIcon(icon);
			if (element.getView().getGraphicPanel().getZoom() > 0)
				zoomLabel.setText(" ".concat(String.valueOf(element.getView().getGraphicPanel().getZoom())).concat("x"));
			else
				zoomLabel.setText(" 1/".concat(String.valueOf(-element.getView().getGraphicPanel().getZoom())).concat(" x"));
		}
		else if (element instanceof TAlignment) {
			statusBarPanel.add(gridActionLabel);
			if (!((TAlignment)element).isAlgoRunning()) {
				gridActionLabel.setText(""); //$NON-NLS-1$
				statusBarPanel.add(spotPercentLabel);
				statusBarPanel.add(mousePositionLabel);
			}
			else {
				gridActionLabel.setForeground(Color.red);
				//modif 06/10/05
				try{
					gridActionLabel.setText(((TAlignment)element).getAlgorithm().getMessage());
				}
				catch (NullPointerException ex){
					gridActionLabel.setText(((TAlignment)element).getName());//cas des plugins on recupere le nom de l'alignement utilisé
					// pour mettre le nom de l'algo...un peu bourrin: le plugin echange le temps de l'algo le nom de l'alignement
				}
				//gridActionLabel.setText("coucou!");//test 06/10/05
				statusBarPanel.add(algorithmProgress);
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/delete_edit.gif")); //$NON-NLS-1$
				cancelAlgoButton = new JButton(icon);
				cancelAlgoButton.setMargin(new Insets(0, 0, 0, 0));
				statusBarPanel.add(cancelAlgoButton);
				cancelAlgoButton.addActionListener(((TAlignment)element).getAlgorithm());
				prioritySlider = new JSlider(JSlider.HORIZONTAL, 0, 2, 0);
				prioritySlider.setPreferredSize(new Dimension(50, 20));
				prioritySlider.setMaximumSize(new Dimension(50, 20));
				prioritySlider.setBorder(border);
				prioritySlider.setSnapToTicks(true);
				statusBarPanel.add(prioritySlider);
				prioritySlider.addChangeListener(((TAlignment)element).getAlgorithm());
			}
			statusBarPanel.add(zoomLabel);
			statusBarPanel.add(modifiedDataLabel);
			if (element.getModel().getModif() == 0)
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_obj.gif")); //$NON-NLS-1$
			else
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_red_obj.gif")); //$NON-NLS-1$
			modifiedDataLabel.setIcon(icon);
			if (element.getView().getGraphicPanel().getZoom() > 0)
				zoomLabel.setText(" ".concat(String.valueOf(element.getView().getGraphicPanel().getZoom())).concat("x"));
			else
				zoomLabel.setText(" 1/".concat(String.valueOf(-element.getView().getGraphicPanel().getZoom())).concat(" x"));
		}
		//ajout integration 14/09/05
		else if (element instanceof TBatch) {
			statusBarPanel.add(gridActionLabel);
			if (!((TBatch)element).isBatchRunning()) {
				gridActionLabel.setText("");
				statusBarPanel.add(spotPercentLabel);
				statusBarPanel.add(mousePositionLabel);
			}
			else {
				gridActionLabel.setForeground(Color.red);
				gridActionLabel.setText(((TBatch)element).getMessage());
				statusBarPanel.add(algorithmProgress);
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/delete_edit.gif"));
				cancelAlgoButton = new JButton(icon);
				cancelAlgoButton.setMargin(new Insets(0, 0, 0, 0));
				statusBarPanel.add(cancelAlgoButton);
				cancelAlgoButton.addActionListener(((TBatch)element).getWorker());
				prioritySlider = new JSlider(JSlider.HORIZONTAL, 0, 2, 0);
				prioritySlider.setPreferredSize(new Dimension(50, 20));
				prioritySlider.setMaximumSize(new Dimension(50, 20));
				prioritySlider.setBorder(border);
				prioritySlider.setSnapToTicks(true);
				statusBarPanel.add(prioritySlider);
				prioritySlider.addChangeListener(((TBatch)element).getWorker());
			}
			statusBarPanel.add(zoomLabel);
			statusBarPanel.add(modifiedDataLabel);
			if (element.getModel().getModif() == 0)
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_obj.gif"));
			else
				icon = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/pubfield_red_obj.gif"));
			modifiedDataLabel.setIcon(icon);
			if (element.getView().getGraphicPanel() != null)
				if (element.getView().getGraphicPanel().getZoom() > 0)
					zoomLabel.setText(" ".concat(String.valueOf(element.getView().getGraphicPanel().getZoom())).concat("x"));
				else
					zoomLabel.setText(" 1/".concat(String.valueOf(-element.getView().getGraphicPanel().getZoom())).concat(" x"));
		}
		//fin ajout integration 14/09/05
		statusBarPanel.revalidate();
		statusBarPanel.repaint();
	}
	public Object[] BZMessageResponse(TEvent event) {
		Object[] ret = null;
		switch (event.getAction()) {
		case GLOBAL_ACTION_LABEL:
			globalActionLabel.setText(" ".concat((String)event.getParam(1))); //$NON-NLS-1$
			globalActionLabel.setForeground((Color)event.getParam(2));
			globalActionLabel.revalidate();
			globalActionLabel.paintImmediately(0, 0, 150, 20);
			break;
		case GLOBAL_ACTION_PROGRESS_BAR:
			globalActionProgressBar.setValue(((Integer)event.getParam(1)).intValue());
			globalActionProgressBar.revalidate();
			globalActionProgressBar.paintImmediately(0, 0, 100, 20);
			break;
		case GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE:
			globalActionProgressBar.setIndeterminate(((Boolean)event.getParam(1)).booleanValue());
			globalActionProgressBar.setStringPainted(!((Boolean)event.getParam(1)).booleanValue());
			globalActionProgressBar.paintImmediately(0, 0, 100, 20);
			break;
		case GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT:
			globalActionProgressBar.setStringPainted(((Boolean)event.getParam(1)).booleanValue());
			break;
		case UPDATE:	
			Object element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if ((element == event.getParam(0)) || (event.getParam(0) == null)) {
				init((TDataElement)element);
				statusBarPanel.revalidate();
			}
			break;
		case MOUSE_POSITION:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if (element == event.getParam(0)) {
				Point p = (Point) event.getParam(1);
				mousePositionLabel.setText(" " + (int) p.getX() + ", " + (int) p.getY()); //$NON-NLS-1$ //$NON-NLS-2$
			}
			break;
		case IMAGE_ACTION_LABEL:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if (element == event.getParam(0)) {
				imageActionLabel.setText(" " + (String) event.getParam(1)); //$NON-NLS-1$
				imageActionLabel.setForeground( (Color) event.getParam(2));
				imageActionLabel.revalidate();
				imageActionLabel.paintImmediately(0, 0, 150, 20);
			}
			break;
		case IMAGE_ACTION_PROGRESS_BAR:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if (element == event.getParam(0)) {
				imageActionProgressBar.setValue( ( (Integer) event.getParam(1)).intValue());
				imageActionProgressBar.revalidate();
				imageActionProgressBar.paintImmediately(0, 0, 100, 20);
			}
			break;
		case IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if (element == event.getParam(0)) {
				imageActionProgressBar.setStringPainted( ( (Boolean) event.getParam(1)).booleanValue());
			}
			break;
		case GRID_ACTION_LABEL:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if ((element == event.getParam(0)) || (event.getParam(0) == null)) {
				gridActionLabel.setText(" " + (String) event.getParam(1)); //$NON-NLS-1$
				gridActionLabel.setForeground( (Color) event.getParam(2));
				gridActionLabel.revalidate();
				gridActionLabel.paintImmediately(0, 0, 150, 20);
			}
			break;
		case SPOT_PERCENT_LABEL:
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			if ((element == event.getParam(0)) || (event.getParam(0) == null)) {
				spotPercentLabel.setText(" " + (String) event.getParam(1)); //$NON-NLS-1$
				spotPercentLabel.revalidate();
				spotPercentLabel.paintImmediately(0, 0, 50, 20);
			}
			break;
		case ALGO_PROGRESS:			
			element = TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null))[0];
			//	System.out.println("element="+element);
			//	System.out.println("event.getParam(0)="+event.getParam(0));
			//	System.out.println("event.getParam(1)="+event.getParam(1));
			if (element == event.getParam(0)) {
				//		System.out.println("ca passe!");				
				algorithmProgress.setValue( ( (Integer) event.getParam(1)).intValue());				
				algorithmProgress.revalidate();
				algorithmProgress.paintImmediately(0, 0, 100, 20);			
			}			
			break;
		}
		return ret;
	}
	public JPanel getStatusBarPanel() {
		return statusBarPanel;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
