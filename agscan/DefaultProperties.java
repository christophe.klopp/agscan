/*
 * Created on Aug 11, 2005
 * Modification : 2005/10/26: add of image properties  
 * Modification : 2005/11/29: add of currents directories paths  
 * @version 2005/11/29
 * @author rcathelin
 * 
 */
package agscan;

import java.io.File;

public class DefaultProperties extends MyProperties {
	public DefaultProperties(){
		super();
		//this.setProperty("key","value");
		//defaults.setProperties...?
		
		
		//2005/11/29 - defaults directories paths
		//see MyProperties to verify the existence of these paths
		this.setProperty("imagesPath","."+File.separator);
		this.setProperty("gridsPath","."+File.separator);
		this.setProperty("batchPath","."+File.separator);
		this.setProperty("alignmentsPath","."+File.separator);
		this.setProperty("columnsPath","."+File.separator);
		this.setProperty("exportsPath","."+File.separator);
		
		//26/04/07
		this.setProperty("TShorcutParameters.ga",0);
		this.setProperty("TShorcutParameters.la",0);
		
		//28/03/07
		this.setProperty("TColorParametersAction.colors1","2:Cy5,Cy3");
		this.setProperty("TColorParametersAction.colors2","1:P33");
		this.setProperty("TColorParametersAction.colors3","4:Violet,Bleu,Vert,Jaune");
		this.setProperty("TColorParametersAction.nbconfig","3");
		this.setProperty("TColorParametersAction.nbcolors","2");
		this.setProperty("TColorParametersAction.colors","2:Cy5,Cy3");
		
		//2005/10/26 - defaults images properties
		this.setProperty("TImageInfoDialog.pixelWidth","25.0");
		this.setProperty("TImageInfoDialog.pixelHeight","25.0");
		this.setProperty("TImageInfoDialog.unit","QL");
		this.setProperty("TImageInfoDialog.ipType","20x25");
		this.setProperty("TImageInfoDialog.latitude","5.0");
		
		// in  agscan.algo.spotdetection.TTwoProfilesSpotDetectionAlgorithm.java
		// menu Alignement > Detection des spots > 2 profils
		this.setProperty("TTwoProfilesSpotDetectionAlgorithm.maxSearchZone","40");//int
		this.setProperty("TTwoProfilesSpotDetectionAlgorithm.maxComputeZone","10");//int
		this.setProperty("TTwoProfilesSpotDetectionAlgorithm.qualityThreshold","30");//int
		
		// in  agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm.java
		// menu Alignement > Detection des spots > 4 profils
		this.setProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone","40");//int
		this.setProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone","10");//int
		this.setProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold","30");//int

		// in  agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithm.java
		// menu Alignement > Detection des spots > use of pattern matching
		this.setProperty("TMultiChannelSpotDetectionAlgorithm.gridCenterCross","1");//boolean
		this.setProperty("TMultiChannelSpotDetectionAlgorithm.spotCircularity","85");//int
		this.setProperty("TMultiChannelSpotDetectionAlgorithm.kernelSize","6");//int
		this.setProperty("TMultiChannelSpotDetectionAlgorithm.minSpotSize","3");//int
		this.setProperty("TMultiChannelSpotDetectionAlgorithm.maxSpotSize","80");//int

		
		// in agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm.java
		// menu Alignement > Alignement global > Renifleur 
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.searchAngle","10");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.contrastPercent","50");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.searchAngleIncrement", "0.2D");//double
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.snifferSize","13");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity","20");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapIterations","5");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunSensitivity","30");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunIterations","50");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunSensitivity","20");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunIterations","10");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunSensitivity","10");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunIterations","10");//int
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locSensitivityThreshold","0.0000001");//double
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locT","0.1");//double
		this.setProperty("TSnifferGlobalAlignmentAlgorithm.locC","0.7");//double
		
		// in agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm.java
		// menu Alignement > Alignement global > Template Matching 
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.searchAngle","10");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevh","50");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevv","50");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentcol","40");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentrow","40");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratioh","20");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratiov","20");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MaxAreaSize","150");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MinAreaSize","50");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.KindKernel","2");//int
		this.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.ActiSize","0");//int
		
		
		// in agscan.algo.globalalignment.TEdgesFinderGlobalAlignmentAlgorithm.java
		// menu Alignement > Alignement global > Detection des bords
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZone","1.5");//double
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZoneIncrement","2");//int
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.edgesSearchZone","5");//int
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngle","10");//int
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.contrastPercent","50");//int
		this.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngleIncrement","0.2D");//double
		
		// in agscan.algo.localalignment.TLocalAlignmentGridAlgorithm.java
		// Alignement > Alignement local > Alignement de la grille entiere		
		this.setProperty("TLocalAlignmentGridAlgorithm.bootstrapSensitivity","30");//int
		this.setProperty("TLocalAlignmentGridAlgorithm.bootstrapIterations","5"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.firstRunSensitivity","30"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.firstRunIterations","200"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.secondRunSensitivity","20"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.secondRunIterations","200"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.thirdRunSensitivity","10"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.thirdRunIterations","100"); //int
		this.setProperty("TLocalAlignmentGridAlgorithm.sensitivityThreshold","0.0000001"); //double
		this.setProperty("TLocalAlignmentGridAlgorithm.locT","0.1"); //double
		this.setProperty("TLocalAlignmentGridAlgorithm.locC","0.7"); //double
		
		// in agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm.java
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.MaxAreaSize","150");//int
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.MinAreaSize","50");//int
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.KindKernel","2");//int
		
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.CSS","1");//int
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.adaptDiam","1");//int
		this.setProperty("TLocalAlignmentBlockTemplateAlgorithm.seuilCircu","90");//int
		

		// in agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm.java
		// Alignement > Alignement local > Alignement bloc par bloc
		this.setProperty("TLocalAlignmentBlockAlgorithm.bootstrapSensitivity","30");//int
		this.setProperty("TLocalAlignmentBlockAlgorithm.bootstrapIterations","5"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.firstRunSensitivity","30"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.firstRunIterations","200"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.secondRunSensitivity","20"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.secondRunIterations","200"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.thirdRunSensitivity","10"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.thirdRunIterations","100"); //int
		this.setProperty("TLocalAlignmentBlockAlgorithm.sensitivityThreshold","0.0000001"); //double
		this.setProperty("TLocalAlignmentBlockAlgorithm.locT","0.1"); //double
		this.setProperty("TLocalAlignmentBlockAlgorithm.locC","0.7"); //double				
	}	
}
