package agscan.algo.localalignment;

import ij.ImagePlus;
import ij.ImageStack;
import ij.measure.ResultsTable;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Polygon;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.swing.JOptionPane;
import plugins.ImageTools;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TUndoLocalAlignmentBlock;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TSpot;
import agscan.dialog.TOpenNImagesDialog;


public class TLocalAlignmentBlockTemplateWorker extends SwingWorker {
	private TLocalAlignmentBlockTemplateAlgorithm laAlgo;
	private TUndoLocalAlignmentBlock ulab = null;
	private int progressRange;

	private static double pixelW; //width of a pixel in µm
	private static double pixelH;//height of a pixel in µm

	private static TAlignment alignment;

	private static float[][] kernel; //current kernel for all alignment
	private static int spotSize;//=spotWidth=spotHeight
	private static int spotDiameter;//diameter of a spot
	private static ImagePlus outimpSpot;//Image complete de correlation
	private static ImagePlus imageMax_tot;//Image complete max


	public TLocalAlignmentBlockTemplateWorker(TLocalAlignmentAlgorithm laAlgo) {
		this.laAlgo = (TLocalAlignmentBlockTemplateAlgorithm)laAlgo;
	}


	public Object construct(boolean thread) {
		TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
		alignment = (TAlignment)aliView.getReference();



		aliView.getTablePanel().setSelectable(false);
		if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning())))
			ulab = new TUndoLocalAlignmentBlock(aliView.getReference());
		alignment.setAlgoRunning(true);
		if (thread) alignment.setAlgorithm(laAlgo);
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		aliView.getGraphicPanel().removeInteractors();
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
		Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
		ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		TGridModel gridModel = (TGridModel)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL);
		Vector selection = gridModel.getSelectionModel().getBlockSelection();
		TGridBlock gb;
		progressRange = laAlgo.getEndProgress() - laAlgo.getStartProgress();
		pc_done = 0.0D;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ laAlgo.getStartProgress())));
		
		pixelH = alignment.getGridModel().getPixelHeight();
		pixelW = alignment.getGridModel().getPixelWidth();

		/*
		 *  getting the image data and 
		 *  define the Template matching kernel 
		 */
		TImageModel imodel = alignment.getImage().getImageModel();
		TGridModel gmodel = alignment.getGridModel();
		int nbImages = imodel.getNumberOfChannels();
		int pixelWidth = (int) imodel.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) imodel.getPixelHeight(); //height of one pixel in micrometers( for example 25)

		// getting Max image data 
		int imageWidth = (int)(imodel.getImageWidth()/imodel.getPixelWidth());
		int imageHeight = (int)(imodel.getImageHeight()/imodel.getPixelWidth());
		int[] imageMax = imodel.getMaxImageData();
		ImageProcessor ip1 = new FloatProcessor(imageWidth,imageHeight,imageMax);
		ip1.invertLut();
		imageMax_tot = new ImagePlus("Image max total", ip1);
		//imageMax_tot.show();



		//getting kernel
		kernel = generateKernel(false);


		//getting image max
		int halfKernelSize = (int) (kernel.length / 2);
		double background = getBackground(imageWidth, imageMax);

		float[] spotData = new float[(imageWidth + kernel.length)
		                             * (imageHeight + kernel.length)];


		for (int j = 0; j < (imageWidth + kernel.length); j++) {
			for (int k = 0; k < (imageHeight + kernel.length); k++) {
				if (j <= halfKernelSize
						|| j >= (imageWidth + kernel.length - halfKernelSize - 1)
						|| k <= halfKernelSize
						|| k >= (imageHeight + kernel.length - halfKernelSize - 1)) {
					// origimg[j][k] = 32768;
					spotData[k * (imageWidth + kernel.length) + j] = (float) background;
				} else {
					spotData[k * (imageWidth + kernel.length) + j] = imageMax[(k - halfKernelSize)
					                                                          * imageWidth + j - halfKernelSize - 1];
				}
			}
		}

		FloatProcessor ipSpot = new FloatProcessor(imageWidth + kernel.length,
				imageHeight + kernel.length, spotData, null);
		float[][] origimg = new float[ipSpot.getWidth()][ipSpot.getHeight()];

		for (int j = 0; j < ipSpot.getWidth(); j++) {
			for (int k = 0; k < ipSpot.getHeight(); k++) {
				origimg[j][k] = ipSpot.getPixelValue(j, k);
			}
		}
		/*
		 * Getting the correlation image
		 */

		float[][] corrimg = null;
		corrimg = new float[imageWidth + 1][imageHeight + 1];

		corrimg = ImageTools.statsCorrelation(origimg, kernel);
		//System.out.println("Correlation Image Hight " + corrimg.length
		//		+ " imageWidth " + corrimg[0].length + " kernel.length "
		//		+ kernel.length);
		float[] outData = new float[corrimg.length * corrimg[0].length];
		for (int k = 0; k < corrimg.length; k++) {
			for (int p = 0; p < corrimg[0].length; p++) {
				outData[p * corrimg.length + k] = (corrimg[k][p] * 32167) + 32167;
			}
		}
		ImageProcessor outSpot = new FloatProcessor(corrimg.length,
				corrimg[0].length, outData, null);
		outimpSpot = new ImagePlus("correlation Image", outSpot);

		outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();

		outSpot.autoThreshold();

		//outimpSpot.show();

		//Ajout Alexis JULIN

		double angle_radian = -alignment.getGridModel().getAngle();
		double angle_degre = angle_radian * 180.0D / (Math.PI);

		/*detection d'alignement de spot en dehors de la grille*/

		//rotation de l'image (par rapport au centre)
		ImageProcessor ip = outimpSpot.getProcessor();
		ImageProcessor ip2 = ip.convertToByte(true);
		ip2.setInterpolate(true);
		ip2.rotate(angle_degre);
		ip2.autoThreshold();

		ImagePlus im2 = new ImagePlus("reference",ip2);
		//im2.show();




//		//----------nouveau traitement

		//rotation
		TEvent ev1 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.ROTATE, null, new Double(
						angle_radian));
		TEventHandler.handleMessage(ev1);

		//translation
		//deca_X et deca_Y viennent de l'alignment global et sont sauvegardés dans le zaf
		double deca_X = alignment.getGridModel().getConfig().getDeca_X();
		double deca_Y = alignment.getGridModel().getConfig().getDeca_Y();


		TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.TRANSLATE, null,-(deca_X*pixelW), -(deca_Y*pixelH));
		TEventHandler.handleMessage(ev2);

		//traitement
		correctionPosition(im2);


		//on remet la grille a l'orientation d'origine
		TEvent ev3 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.ROTATE, null, new Double(
						-angle_radian));
		TEventHandler.handleMessage(ev3);

		//translation inverse
		TEvent ev4 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.TRANSLATE, null,(deca_X*pixelW), (deca_Y*pixelH));
		TEventHandler.handleMessage(ev4);

		//--------fin du nouveau traitement

		//pour traiter le spot par spot on remet l'image et la grille à leur position d'origine
		int css_activate= (Integer)laAlgo.getParameter(laAlgo.CSS);
		if(css_activate == 1){
			System.out.println("Correction spot par spot activée");
			adaptativeCircle();
		}

		return null;
	}

	public void finished() {
		TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
		TAlignment alignment = (TAlignment)aliView.getReference();
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
		Vector v = (Vector)TEventHandler.handleMessage(ev)[0];
		TSpot spot;
		for (Enumeration enu = v.elements(); enu.hasMoreElements(); ) {
			spot = (TSpot)enu.nextElement();
			spot.setFitAlgorithm(null);
			spot.setSynchro(false, aliView.getTablePanel());
		}
		if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
			ulab.addFinalPosition();
			ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment, ulab);
		}
		TEventHandler.handleMessage(ev);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		aliView.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
		Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
		ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
		TEventHandler.handleMessage(ev);
		aliView.getTablePanel().setSelectable(true);
	}



	public void correctionPosition(ImagePlus im){


		//	System.out.println("profondeur = "+im.getBitDepth());
		boolean TEST = false;


		TGridConfig gc = alignment.getGridModel().getConfig();
		double spotH = gc.getSpotsHeight();
		double spotW = gc.getSpotsWidth();
		//recuperation des blocs
		Vector v = alignment.getGridModel().getLevel1Elements();

		//vecteur trié de pair : gauche a droite ; impaire de droite a gauche
		Vector w = sort(v,gc.getLev2NbRows(),gc.getLev2NbCols());



		double deca_X = 0.0D;
		double deca_Y = 0.0D;

		//pc_done = 0.0D;
		
		//pour chaque bloc on recupere les 4 zones de recherches qui l'entourent
		for(int i = 0 ; i < w.size() ; i++){
			TEST = false;
			boolean suiv = false;

			System.out.println("Current block "+i);

			pc_done = ((double)(i+1) / (double)(w.size())) *100.0D;
			System.out.println(i+"pc_done "+pc_done);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
							* progressRange / 100.0D)
							+ laAlgo.getStartProgress())));
			
			
			
			TGridBlock gb = (TGridBlock)w.get(i);
			TGridBlock next = null;
			if(i != w.size()-1){
				suiv = true;
				next = (TGridBlock)w.get(i+1);
			}


			//calage de chacun des blocs
			double deca[] = first_Align(im,i,gb);
			//calage du bloc courant
			TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
					TGridPositionControler.TRANSLATE_BLOCK, null, gb,deca[0],deca[1]);
			TEventHandler.handleMessage(ev);


			//calage pour le bloc suivant par propagation de la translation courante
			deca_X = deca_X + deca[0];
			deca_Y = deca_Y + deca[1];



//			//dectection des colonnes ou lignes vides sur les extremités du bloc courant
			Vector col_vide = emptyCol(im, gb,i);
			Vector row_vide = emptyRow(im, gb,i);


			TSpot sp1 = (TSpot)gb.getFirstElement();
			TSpot sp2 = (TSpot)gb.getLastElement();
			double x1 = sp1.getXCenter() - spotW/2;
			double y1 = sp1.getYCenter() - spotH/2;
			double x2 = sp2.getXCenter() + spotW/2;
			double y2 = sp2.getYCenter() + spotH/2;

			zone zh1,zh2,zv1,zv2;
			zh1 = new zone(x1,y1 - spotH , x2,y1, alignment.getGridModel());
			zh2 = new zone(x1 ,y2, x2, y2 + spotH,alignment.getGridModel());
			zv1 = new zone(x1 - spotW, y1,x1,y2,alignment.getGridModel());
			zv2 = new zone(x2, y1,x2 + spotW , y2,alignment.getGridModel());

			//System.out.println("etape 1");
			while(row_vide.contains(gc.getLev1NbRows()-1)){
				//extraction de l'image correspondant à la zone zh1
				ImagePlus im_zh1 = getZone(zh1,im);
				im_zh1.setTitle("ZH1 bloc "+i);

				if(TEST)
					im_zh1.show();


				if(analyseZone(im_zh1,gc.getLev1NbCols())){
					TEvent e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, gb,
							0.0D	, -spotH);
					TEventHandler.handleMessage(e);

					//translation du suivant
					if(suiv){
						deca_Y = deca_Y - spotH;
					}

					row_vide = emptyRow(im ,gb,i);
				}
				else
					row_vide.remove(new Integer(gc.getLev1NbRows()-1));
			}
			//System.out.println("etape 2");
			while(row_vide.contains(0)){
				//extraction de l'image correspondant à la zone zh2
				ImagePlus im_zh2 = getZone(zh2,im);
				im_zh2.setTitle("ZH2 bloc "+i);

				if(TEST)
					im_zh2.show();

				if(analyseZone(im_zh2,gc.getLev1NbCols())){
					TEvent e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, gb,
							0.0D , spotH);
					TEventHandler.handleMessage(e);

					//translation du suivant
					if(suiv){
						deca_Y = deca_Y + spotH;
					}
					row_vide = emptyRow(im,gb ,i);
				}
				else{
					row_vide.remove(new Integer(0));
				}
			}
			//System.out.println("etape 3");
			while(col_vide.contains(gc.getLev1NbCols()-1)){
				//extraction de l'image correspondant à la zone zv1
				ImagePlus im_zv1 = getZone(zv1,im);
				im_zv1.setTitle("ZV1 bloc "+i);

				if(TEST)
					im_zv1.show();

				if(analyseZone(im_zv1,gc.getLev1NbRows())){
					TEvent e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, gb,
							-spotW	, 0.0D);
					TEventHandler.handleMessage(e);

					//translation du suivant
					if(suiv){
						deca_X = deca_X - spotW;
					}
					col_vide = emptyCol(im,gb, i);
				}
				else
					col_vide.remove(new Integer(gc.getLev1NbCols()-1));

			}

			//System.out.println("etape 4");
			while(col_vide.contains(0)){

				//extraction de l'image correspondant à la zone zv2
				ImagePlus im_zv2 = getZone(zv2,im);


				im_zv2.setTitle("ZV2 bloc "+i);

				if(TEST)
					im_zv2.show();

				if(analyseZone(im_zv2,gc.getLev1NbRows())){
					System.out.println("translate ");
					TEvent e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, gb,
							spotW	, 0.0D);
					TEventHandler.handleMessage(e);

					//translation du suivant
					if(suiv){
						deca_X = deca_X + spotW;
					}
					col_vide = emptyCol(im,gb, i);
				}
				else
					col_vide.remove(new Integer(0));

			}



			//translation du bloc suivant
			if(suiv){
				TEvent e_next = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, next,
						deca_X	, deca_Y);
				TEventHandler.handleMessage(e_next);

			}
			

			

		}



	}


	/**
	 * 
	 * @param z
	 * @param im
	 * @return an ImagePlus which contains zone z
	 */
	public ImagePlus getZone(zone z, ImagePlus im){
		ImagePlus res;
		im.setRoi(z.getX1(), z.getY1(), 
				z.getX2()-z.getX1(),z.getY2()-z.getY1());
		ImageProcessor ip1 =im.getProcessor();
		ImageProcessor ip2 = ip1.crop();
		res = new ImagePlus("",ip2);
		im.killRoi();
		return res;
	}



	/**
	 * 
	 * @param im
	 * @param nb_max
	 * @return true if there are some particle in zone z false if not
	 */
	public boolean analyseZone(ImagePlus im, int nb_max){
		boolean res  = false;
		TGridConfig gc = alignment.getGridModel().getConfig();

		//analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();

		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		double b_max = (double)((Integer)laAlgo.getParameter(laAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)laAlgo.getParameter(laAlgo.MIN_AREA_SIZE))/100.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im);
		double [][] coord = new double[2][rt.getCounter()];

		//System.out.println("Spots");
		for (int o = 0; o < rt.getCounter(); o++) {
			coord[0][o] = (double)(rt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o]);
			coord[1][o] = (double)(rt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o]);
			//System.out.println(coord[1][o]);
		}
		System.out.println("nombre de particules trouvées " + coord[0].length);


		if((double)coord[0].length > ((double)nb_max) * 0.2D)
			return true;

		return res;
	}





	/**
	 * @author ajulin
	 * @param imp
	 * @param no_bloc
	 * @return un vecteur contenant les colonnes vides
	 */
	public Vector emptyCol(ImagePlus imp,TGridBlock gb,int no_bloc){
		Vector res = new Vector();

		TGridConfig gc = alignment.getGridModel().getConfig();
		double larg_spot = (double)gc.getSpotsWidth();
		double long_spot = (double)gc.getSpotsHeight();


		TSpot deb = (TSpot)gb.getFirstElement();
		TSpot fin = (TSpot)gb.getLastElement();

		//on extrait la partie d'image concernée par le bloc
		ImageProcessor ip = imp.getProcessor().duplicate().convertToByte(true);
		//on la copie
		ImagePlus im2 = new ImagePlus("Resultat", ip);

		double largeur = (fin.getXCenter() - deb.getXCenter() + larg_spot)/pixelW;
		double longueur = (fin.getYCenter() - deb.getYCenter() + long_spot)/pixelH;
		im2.setRoi((int)((deb.getXCenter()-larg_spot/2.0D)/pixelW), (int)((deb.getYCenter()-long_spot/2.0D)/pixelH), (int)largeur, (int)longueur);
		ImageProcessor ip2 = im2.getProcessor();
		ImageProcessor ip3 = ip2.crop();
		ImagePlus im3 = new ImagePlus( "bloc "+no_bloc,ip3);


		Vector moyen []=centreMoyen(gc, gb);
		Vector moyen_X = moyen[0];
		int debut_X = (int)(((TSpot)gb.getFirstElement()).getXCenter() - larg_spot/2)/(int)pixelW;


		int curr_1 = (Integer)moyen_X.get(0) - debut_X- (int)larg_spot/50;

		im3.setRoi(curr_1,0,(int)gc.getSpotsWidth()/(int)pixelW,im3.getHeight());
		ImageProcessor ip4 = im3.getProcessor();
		ImageProcessor ip5 = ip4.crop();
		ImagePlus im4 = new ImagePlus("premiere colonne du bloc "+no_bloc,ip5);
		//im4.show();


		im3.killRoi();

		int curr_2 = (Integer)moyen_X.get(moyen_X.size()-1) - debut_X- (int)larg_spot/50;
		im3.setRoi(curr_2,0,(int)gc.getSpotsWidth()/(int)pixelW,im3.getHeight());
		ip4 = im3.getProcessor();
		ip5 = ip4.crop();
		ImagePlus im5 = new ImagePlus("derniere colonne du bloc "+no_bloc,ip5);
		//im5.show();

		//analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		double b_max = (double)((Integer)laAlgo.getParameter(laAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)laAlgo.getParameter(laAlgo.MIN_AREA_SIZE))/100.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im4);
		//System.out.println("1ere col "+rt.getCounter());
		int col1 = rt.getCounter();

		rt = new ResultsTable();
		analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im5);
		//System.out.println("derniere col "+rt.getCounter());
		int col2 = rt.getCounter();

		if((col1==0) && (col2 == 0)){
			System.out.println("mouvement indeterminé");
		}
		else{
			if(col1 == 0)
				res.add(0);
			if(col2 == 0)
				res.add(gc.getLev1NbCols()-1);

		}


		//System.out.println("colonne vide "+res.toString());;
		return res;
	}


	/**
	 * @author ajulin
	 * @param imp
	 * @param no_bloc
	 * @return un vecteur contenant les lignes vides
	 */
	public Vector emptyRow(ImagePlus imp,TGridBlock gb,int no_bloc){
		Vector res = new Vector();

		TGridConfig gc = alignment.getGridModel().getConfig();
		double larg_spot = (double)gc.getSpotsWidth();
		double long_spot = (double)gc.getSpotsHeight();


		TSpot deb = (TSpot)gb.getFirstElement();
		TSpot fin = (TSpot)gb.getLastElement();



		//on extrait la partie d'image concernée par le bloc
		ImageProcessor ip = imp.getProcessor().duplicate().convertToByte(true);
		//on la copie
		ImagePlus im2 = new ImagePlus("Resultat", ip);


		double largeur = (fin.getXCenter() - deb.getXCenter() + larg_spot)/pixelW;
		double longueur = (fin.getYCenter() - deb.getYCenter() + long_spot)/pixelH;
		im2.setRoi((int)((deb.getXCenter()-larg_spot/2.0D)/pixelW), (int)((deb.getYCenter()-long_spot/2.0D)/pixelH), (int)largeur, (int)longueur);
		ImageProcessor ip2 = im2.getProcessor();
		ImageProcessor ip3 = ip2.crop();
		ImagePlus im3 = new ImagePlus( "bloc "+no_bloc,ip3);


		Vector moyen []=centreMoyen(gc, gb);
		Vector moyen_Y = moyen[1];
		int debut_Y = (int)(((TSpot)gb.getFirstElement()).getYCenter() - long_spot/2)/(int)pixelH;


		int curr_1 = (Integer)moyen_Y.get(0) - debut_Y - (int)long_spot/(2*(int)pixelH);

		im3.setRoi(0,curr_1,im3.getWidth(),(int)gc.getSpotsHeight()/(int)pixelH);
		ImageProcessor ip4 = im3.getProcessor();
		ImageProcessor ip5 = ip4.crop();
		ImagePlus im4 = new ImagePlus("premiere ligne du bloc "+no_bloc,ip5);
		//im4.show();


		im3.killRoi();

		int curr_2 = (Integer)moyen_Y.get(moyen_Y.size()-1) - debut_Y - (int)long_spot/(2*(int)pixelH);
		im3.setRoi(0,curr_2,im3.getWidth(),(int)gc.getSpotsHeight()/(int)pixelH);
		ip4 = im3.getProcessor();
		ip5 = ip4.crop();
		ImagePlus im5 = new ImagePlus("derniere ligne du bloc "+no_bloc,ip5);
		//im5.show();

		//analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		double b_max = (double)((Integer)laAlgo.getParameter(laAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)laAlgo.getParameter(laAlgo.MIN_AREA_SIZE))/100.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im4);
		//System.out.println("1ere col "+rt.getCounter());
		int row1 = rt.getCounter();

		rt = new ResultsTable();
		analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im5);
		//System.out.println("derniere col "+rt.getCounter());
		int row2 = rt.getCounter();

		if((row1==0) && (row2 == 0)){
			System.out.println("mouvement indeterminé");
		}
		else{
			if(row1 == 0)
				res.add(0);
			if(row2 == 0)
				res.add(gc.getLev1NbRows()-1);

		}



		//System.out.println("ligne vide "+res.toString());
		return res;
	}

	/**
	 * @author ajulin
	 * @param im
	 * @param nb index of the current block
	 * @param gb current block
	 * @return
	 */
	public double[] first_Align(ImagePlus im, int nb,TGridBlock gb){

		double res [] = new double[2];

		//extraction de l'image
		TGridConfig gc = alignment.getGridModel().getConfig();
		TSpot sp1 = (TSpot)gb.getFirstElement();
		TSpot sp2 = (TSpot)gb.getLastElement();
		double spotH = gc.getSpotsHeight()/pixelH;
		double spotW = gc.getSpotsWidth()/pixelW;
		int x1 = (int)(sp1.getXCenter()/pixelW - spotW/2.0D);
		int y1 = (int)(sp1.getYCenter()/pixelH - spotH/2.0D);
		int x2 = (int)(sp2.getXCenter()/pixelW + spotW/2.0D);
		int y2 = (int)(sp2.getYCenter()/pixelH + spotH/2.0D);

		//extraire la partie d'image qui correspond a ce bloc
		im.setRoi(x1,y1,x2-x1,y2-y1);
		ImageProcessor ip = im.getProcessor();
		ImageProcessor ip2 = ip.crop();
		ImagePlus im2 = new ImagePlus("bloc "+nb , ip2);
		//im2.show();
		im.killRoi();
//		analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		double b_max = (double)((Integer)laAlgo.getParameter(laAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)laAlgo.getParameter(laAlgo.MIN_AREA_SIZE))/100.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, b_min * partSize,b_max * partSize);
		analyse.analyze(im2);

		double [][] coord = new double[2][rt.getCounter()];

		//System.out.println("Spots");
		for (int o = 0; o < rt.getCounter(); o++) {
			coord[0][o] = (double)(rt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o]);
			coord[1][o] = (double)(rt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o]);
			//System.out.println(coord[0][o]);
		}

		double m = Double.POSITIVE_INFINITY;
		int indice = -1;
		for(int i = 0 ; i < 0.5 * coord[0].length ; i++){
			double d = Math.sqrt(coord[0][i]*coord[0][i] + coord[1][i]*coord[1][i]);
			if(d < m){
				indice = i;
				m = d;
			}

		}
		int first_X = (int)(coord[0][indice]);
		int first_Y = (int)(coord[1][indice]);

		//System.out.println("element image = "+first_X+" ; "+first_Y);



		//on cherche le spot le plus proche
		Vector cMo []  = centreMoyen_2(gc,gb);

		Vector x_grid = cMo[0];
		Vector y_grid = cMo[1];


		int col = -1;
		int row = -1;

		int min = Integer.MAX_VALUE;

		//on cherche la colonne de la grille dont le spot est le plus proche
		for(int i =0 ; i < x_grid.size() ; i++){
			int col_grid = (Integer)x_grid.get(i);
			if(Math.abs(first_X - col_grid) < min){
				col = i;
				min = Math.abs(first_X - col_grid);
			}

		}
		min = Integer.MAX_VALUE;

		//on cherche la ligne de la grille dont le spot est le plus proche
		for(int i =0 ; i < y_grid.size() ; i++){
			int row_grid = (Integer)y_grid.get(i);

			if(Math.abs(first_Y - row_grid) < min){
				row = i;
				min = Math.abs(first_Y - row_grid);
			}

		}
		res[0]= (double)((first_X - (Integer)x_grid.get(col))* pixelW);
		res[1]= (double)((first_Y - (Integer)y_grid.get(row))* pixelH);
		return res;
	}



	/**
	 * @author ajulin
	 * @param gc
	 * @param gb current block
	 * @return coordonates of lines and columns for the block gb in the complete image
	 */
	public static Vector [] centreMoyen(TGridConfig gc, TGridBlock gb){
		Vector res [] = new Vector[2];
		Vector centre_X = new Vector();
		Vector centre_Y = new Vector();


		Vector spots = gb.getElements();
		for(int i = 0 ; i < gc.getLev1NbCols() ; i++){
			TSpot curr = (TSpot)spots.get(i);
			int abs = (int)(curr.getXCenter()/pixelW);
			centre_X.add(abs);
		}

		for(int i = 0 ; i < spots.size() ; i = i + gc.getLev1NbCols()){
			TSpot curr = (TSpot)spots.get(i);
			int ord = (int)(curr.getYCenter()/pixelH);
			centre_Y.add(ord);
		}

		res [0] = centre_X;
		res [1] = centre_Y;

		return res;

	}

	/**
	 * @author ajulin
	 * @param gc
	 * @param gb current block
	 * @return coordonates of lines and columns for the block gb in the block's image
	 */
	public static Vector [] centreMoyen_2(TGridConfig gc, TGridBlock gb){
		Vector res [] = new Vector[2];
		Vector centre_X = new Vector();
		Vector centre_Y = new Vector();


		Vector spots = gb.getElements();
		for(int i = 0 ; i < gc.getLev1NbCols() ; i++){
			TSpot curr = (TSpot)spots.get(i);
			int abs = (int)(curr.getXCenter()/pixelW) - (int)(gb.getX()/pixelW);
			centre_X.add(abs);
		}

		for(int i = 0 ; i < spots.size() ; i = i + gc.getLev1NbCols()){
			TSpot curr = (TSpot)spots.get(i);
			int ord = (int)(curr.getYCenter()/pixelH) - (int)(gb.getY()/pixelH);
			centre_Y.add(ord);
		}

		res [0] = centre_X;
		res [1] = centre_Y;

		return res;

	}

	private static double getBackground(int imageWidth, int[] imageMax) {
		// background of the first 2 lines
		double bachground = 0;
		for (int j = 0; j < imageWidth * 2; j++) {
			bachground = bachground + imageMax[j];
		}
		return bachground / (imageWidth * 2);
	}






	/**
	 * 
	 * @param affich
	 * @return
	 */
	private  float[][] generateKernel(boolean affich){

		float res[][] = null; 

		//System.out.println(alignment.getGridModel().getConfig().getSpotsDiam());

		spotDiameter = (int)(alignment.getGridModel().getConfig().getSpotsDiam() / pixelW);
		spotSize = (int)(alignment.getGridModel().getConfig().getSpotsWidth() / pixelW);
		//System.out.println("local spotDiameter "+spotDiameter);
		//System.out.println("local spotSize "+spotSize);


		if(spotSize %2 ==0)
			spotSize++;

		int typeKern = (Integer)laAlgo.getParameter(laAlgo.KIND_KERNEL);
		switch(typeKern){
		case 1 :{ 
			res = alignment.calcGaussAGScanKernel(spotDiameter,spotSize);
			System.out.println("kernel gaussien choisi");
			break;

		}
		case 2 :{
			res = alignment.calcCircAGScanKernel(spotDiameter,spotSize);
			System.out.println("kernel circulaire choisi");
			break;
		}
		case 3 :{
			res = alignment.calcConicAGScanKernel(spotDiameter,spotSize);
			System.out.println("kernel conique choisi ");
			break;
		}
		default : {
			res = alignment.calcCircAGScanKernel(spotDiameter,spotSize);
			System.out.println("kernel circulaire choisi");
			break;
		}

		}
		if(affich){
			ImageProcessor ip_kern = new FloatProcessor(res);
			ImagePlus im_kern = new ImagePlus("Local Kernel", ip_kern);
			im_kern.show();
		}

		return res;
	}

	/**
	 * 
	 * @param ali
	 * 
	 * compute new position and new diameter for each spot
	 */
	public void adaptativeCircle(){

		int size;

		int TEST = 0;

		//recuperation de l'alignement et de la grille
		TImageModel model = alignment.getImage().getImageModel();
		TGridModel gm = alignment.getGridModel();
		TGridConfig gc = gm.getConfig();



		//recuperation de l'image max
		int[] imageMax = model.getMaxImageData();
		int imageWidth = (int)(model.getImageWidth()/model.getPixelWidth());

		//recuperation des spots
		TEvent ev1 = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment);
		TEventHandler.handleMessage(ev1);
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
		Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];

		//creation de la structure des blocs
		Vector n_block = new Vector();
		int nb_spot = gc.getLev1NbCols() * gc.getLev1NbRows();
		int nb_block = gc.getLev2NbCols() * gc.getLev2NbRows();

		//rangement des spots dans des blocs 
		for(int i = 0 ; i < nb_block ; i++){
			TSpot curr_element = (TSpot)spots.get(i);
			Vector curr = new Vector();

			for(int j = 0 ; j < nb_spot ; j++){
				curr.add(spots.get(i * nb_spot + j));


			}
			n_block.add(curr);

		}
		Vector n_block_sort = new Vector(n_block.size());

		//tri du bloc impair : gauche -> droite et pair droite->gauche
		for(int i = 0 ; i < n_block.size() ; i++){
			Vector v = (Vector)n_block.get(i);
			Vector v_sort = sort(v,gc.getLev1NbRows(), gc.getLev1NbCols());
			n_block_sort.add(v_sort);
		}


		int cpt = 0;
		//debut de la boucle principale
		int diamAdapt_activate = (Integer)laAlgo.getParameter(laAlgo.ADAPT_DIAM);
		if(diamAdapt_activate == 1)
			System.out.println("Correction du diamètre du spot activée");
		for(int i =0 ; i < n_block_sort.size(); i++ ){
			Vector current_block = (Vector)n_block_sort.get(i);
			TSpot first = (TSpot)current_block.get(0);
			double deca_X = 0.0D;
			double deca_Y = 0.0D;

			//System.out.println("coord first "+deca_X+" ; "+deca_Y);

			for(int o = 0 ; o < current_block.size() ; o++){
				//for(int o = 0 ; o < 12 ; o++){

				TSpot curr_spot =(TSpot) current_block.get(o); //to get current spot 

				//transimission de la translation venant du spot precedent
				curr_spot.setX(curr_spot.getLocalX() - deca_X);
				curr_spot.setY(curr_spot.getLocalY() - deca_Y);

				double[] coords = computeSpotCenter_bis(spotSize, kernel, imageMax, imageWidth, curr_spot );

				/*
				 * Spot position calculation and 
				 */


				double seuilCircu = (double)((Integer)laAlgo.getParameter(laAlgo.SEUIL_CIRCU))/100.0D;
				if(coords[3] > seuilCircu){
					/*
					 * Sets the computed diameter
					 */
					
					if(diamAdapt_activate == 1){
						double diameter = Math.sqrt(coords[2]*pixelW*pixelH/Math.PI)*2.0D;
						curr_spot.setComputedDiameter(diameter);
						curr_spot.updateDiameterWithComputed();
					}
					cpt++;
					//mise a jour du decalage
					deca_X =curr_spot.getXCenter() - coords[0]*pixelW;
					deca_Y =curr_spot.getYCenter() - coords[1]*pixelH;

					curr_spot.setX(curr_spot.getLocalX() - deca_X);
					curr_spot.setY(curr_spot.getLocalY() - deca_Y);

				}

				//plotSpot_Border(Color.green,currentSpot);




			}//fin du parcours d'un block
		}//fin du parcours du vecteur de blocs
		//fin du parcours de tout les spots
		System.out.println("fin de la correction par spot");
		System.out.println("nombre de spot déplacés "+cpt);
	}




	/**
	 * @author ajulin
	 * @param kernelsize
	 * @param kernel
	 * @param imageMax
	 * @param imageWidth
	 * @param spot
	 * @return the X,Y coordinates, the area and the circularity
	 */
	public  double[] computeSpotCenter_bis(int kernelsize, float[][] kernel , int[] imageMax, double imageWidth,  TSpot spot) {
		TGridModel model = spot.getModel();
		TGridConfig gc = alignment.getGridModel().getConfig();
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight();

		//l'image pour le traitement a besoin de bordures de la taille d'un demi-kernel
		int spotWidth = (int)((spot.getSpotWidth()/pixelWidth)+kernelsize);
		int spotHeight = (int)((spot.getSpotHeight()/pixelHeight)+kernelsize);
		//definition du point de point de depart de remplissage de l'image
		int xSpotPosition = (int)((spot.getX()/pixelWidth)-kernelsize/2);
		int ySpotPosition = (int)((spot.getY()/pixelHeight)-kernelsize/2);


		float[][] origimg = new float[spotWidth][spotHeight];
		for (int j = 0; j < spotWidth; j++){
			for (int k = 0; k < spotHeight; k++){
				//spotData[k*spotWidth+ j] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];
				int index = (int)((ySpotPosition+k)*imageWidth+j+xSpotPosition);
				if ((imageMax.length > index) && (index >= 0)){
					origimg[j][k] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];	
				}
			}
		}
//		ImageProcessor test1 = new FloatProcessor(origimg);
//		ImagePlus i1 = new ImagePlus("image ori" ,test1);
//		i1.show();

		/**
		 * Template matching  
		 */  	

		float[][] corrimg = null;
		corrimg = new float[spotWidth][spotHeight];
		corrimg = ImageTools.statsCorrelation(origimg, kernel);


		ImageProcessor outSpot = new FloatProcessor(corrimg);
		ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);


		outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();

		/**
		 * Thresholding  
		 */  

		outSpot.autoThreshold();

		//outimpSpot.show();



		//imageMax_tot.show();

//		//extraction de la zone dans l'image max
		int xc = (int)(spot.getXCenter()/pixelW);
		int yc = (int)(spot.getYCenter()/pixelH);
		int spotW = (int)(gc.getSpotsWidth()/(2.0D * pixelW));
		int spotH = (int)(gc.getSpotsHeight()/(2.0D * pixelH));
		ImageProcessor ip1 = imageMax_tot.getProcessor();
		ip1.setRoi(xc - spotW, yc - spotH, 2*spotW, 2*spotH);
		ImageProcessor ip_max3 = ip1.crop();
		ImageProcessor ip_max4 = ip_max3.convertToByte(false);
		ip_max4.autoThreshold();
		ImagePlus im_max = new ImagePlus("Image max", ip_max4);




		double coords [] = analyse(outimpSpot, im_max,spot);

		return coords; 

	}
	/**
	 * @author ajulin
	 * @param imCorr image de correlation extraite de l'image de correlation totale
	 * @param imMax image max extraite de l'image max totale
	 * @param spot
	 * @return the X,Y coordinates, the area and the circularity
	 */
	public double[] analyse(ImagePlus imCorr, ImagePlus imMax, TSpot spot){
		TGridConfig gc = alignment.getGridModel().getConfig();


		//on cherche a recuperer les centres des spots de l'images
		//L'image est trait�e pour l'alignement =>un simple seuillage suffit
		ImageProcessor ip = imCorr.getProcessor();
		ip.autoThreshold();

		//analyse de l'image seuille�
		//conversion en une image 8 bits
		ImagePlus im2;

		if(imCorr.getBitDepth() !=8 ){
			ImageProcessor ip2;
			ip2=ip.convertToByte(false);
			im2 = new ImagePlus ("resultat",(ImageProcessor)ip2);
		}
		else{
			im2 = new ImagePlus ("resultat",ip);
		}


		//analyse de l'image de correlation on analyse seulement les centres de masse
		ResultsTable rt = new ResultsTable();
		ParticleAnalyzer mypart = new ParticleAnalyzer(0,64, rt,0.0D, Double.POSITIVE_INFINITY);
		mypart.analyze(im2);
		double coord[][] = new double[4][rt.getCounter()];
		//System.out.println("Image corr "+rt.getCounter());
		for (int o = 0; o < rt.getCounter(); o++) {
			coord[0][o] = (double)(rt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o])+spot.getX()/pixelW;
			coord[1][o] = (double)(rt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o])+spot.getY()/pixelH;
			//System.out.println(coord[0][o]+" ; "+coord[1][o]);


		}



		//analyse de l'image max => circularité et aire

		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI;
		double b_max = (double)((Integer)laAlgo.getParameter(laAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)laAlgo.getParameter(laAlgo.MIN_AREA_SIZE))/100.0D;
		double coord_fin[] = new double[4];
		coord_fin[0] = 0.0D;
		coord_fin[1] = 0.0D;
		coord_fin[2] = 0.0D;
		coord_fin[3] = 0.0D;

		//System.out.println("profondeur "+imMax.getBitDepth());
		if(rt.getCounter() == 1){	

			ResultsTable rt_max = new ResultsTable();
			ParticleAnalyzer mypart_max = new ParticleAnalyzer(0,1+8192, rt_max, partSize*b_min, partSize*b_max);
			mypart_max.analyze(imMax);
			//imMax.show();
			//System.out.println("Image max "+rt_max.getCounter());
			if(rt_max.getCounter() == 1){
				coord_fin[0] = coord[0][0];
				coord_fin[1] = coord[1][0];
				coord_fin[2] = (double)(rt_max.getColumn(ij.measure.ResultsTable.AREA)[0]);
				coord_fin[3] = (double)(rt_max.getColumn(ij.measure.ResultsTable.CIRCULARITY)[0]);
				//plot_cross(alignment,(int)coord_fin[0] ,(int)coord_fin[1], Color.green);
			}
			else{
				coord_fin[0] = 0.0D;
				coord_fin[1] = 0.0D;
				coord_fin[2] = 0.0D;
				coord_fin[3] = 0.0D;

			}
		}


		return coord_fin;
	}


	/**
	 * @author ajulin
	 * @param v vector to sort
	 * @param nb_row number of row in a block
	 * @param nb_spot number of spot in a column
	 * @return v sorted like that  -----<>
	 * 							   <------|
	 * 							   |------>
	 * 
	 */
	public static Vector sort( Vector v, int nb_row, int nb_spot){
		Vector res = new Vector();
		for(int i = 0 ; i < nb_row ; i++){
			if(i%2 == 0){
				for(int j = 0 ; j < nb_spot ; j++)
					res.add(v.get(i * nb_spot + j));


			}
			else{
				for(int j = nb_spot-1 ; j >-1 ; j--)
					res.add(v.get(i * nb_spot + j));
			}


		}

		return res;
	}


	private static double moyenne(Vector v){
		double moy = 0.0D;
		double sum = 0.0D;
		for(int i = 0 ; i < v.size() ; i++){
			double curr = (Double)v.get(i);
			sum = sum + curr;

		}
		moy = sum / (double)v.size();


		return moy;
	}

	/**
	 * @author ajulin
	 * @param v
	 * @return standard deviation computes for the vector v
	 */
	private static double ecartType(Vector v){
		double ecart = 0.0D;
		double sum = 0.0D;
		double moyenne = moyenne(v);

		for(int i= 0 ; i < v.size() ; i++){
			double curr = (Double)v.get(i);
			sum = sum + (curr - moyenne)*(curr - moyenne);
		}
		ecart = Math.sqrt(sum / (double)v.size());

		return ecart;
	}


	/**
	 * @author ajulin
	 * @param x
	 * @param y
	 * @param color
	 * 
	 * draw a cross on AGScan view at the position (x,y) in pixel
	 */
	private static void plot_cross(int x, int y, Color color){

		System.out.println(x+" ; "+y);
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x-2, y, x+2, y));

		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x, y-2, x, y+2));

	}

	/**
	 * @author ajulin
	 * @param sp
	 * @param color
	 * 
	 * draw a cross on AGScan view at center of sp
	 */
	private static void plot_cross(TSpot sp, Color color){

		int x=(int)(sp.getXCenter()/pixelW);
		int y=(int)(sp.getYCenter()/pixelH);
		System.out.println(x+" ; "+y);
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x-2, y, x+2, y));

		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x, y-2, x, y+2));

	}	
	
	
	/**
	 * @author ajulin
	 * @param sp
	 * @param color
	 * 
	 * draw a window around sp
	 */
	private static void plotSpot_Border(Color c, TSpot spot){

		int spotWidth = (int)((spot.getSpotWidth()/pixelW));
		int spotHeight = (int)((spot.getSpotHeight()/pixelH));



		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(spot.getX()/pixelW), (int)(spot.getY()/pixelH)+1,
				(int)(spot.getX()/pixelW)+spotWidth, (int)(spot.getY()/pixelH)+1));


		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(spot.getX()/pixelW)+spotWidth, (int)(spot.getY()/pixelH+1),
				(int)(spot.getX()/pixelW)+spotWidth, (int)(spot.getY()/pixelH)+spotHeight+1));

		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(spot.getX()/pixelW)+spotWidth, (int)(spot.getY()/pixelH)+spotHeight+1,
				(int)(spot.getX()/pixelW), (int)(spot.getY()/pixelH)+spotHeight+1));



		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(spot.getX()/pixelW), (int)(spot.getY()/pixelH)+1,
				(int)(spot.getX()/pixelW), (int)(spot.getY()/pixelH)+spotHeight+1));



	}
	
	
	/**
	 * @author ajulin
	 * @param ip
	 * @param c
	 * 
	 * draw in ImagePlus the border of the current grid
	 */
	private static void drawIJGrid(ImageProcessor ip, Color c){
		int nb_bloc_row = alignment.getGridModel().getConfig().getLev2NbCols();
		int nb_bloc_col =  alignment.getGridModel().getConfig().getLev2NbRows();

		int nb_spot_row = alignment.getGridModel().getConfig().getLev1NbCols();
		int nb_spot_col =  alignment.getGridModel().getConfig().getLev1NbRows();

		int nb_spot_bloc = nb_spot_row * nb_spot_col;

		Vector v = alignment.getGridModel().getSpots();
		TSpot sp1 = (TSpot)v.get(0);

		TSpot sp2 = (TSpot)v.get((nb_bloc_row-1) * nb_spot_bloc + nb_spot_row - 1); 

		TSpot sp3 = (TSpot)v.get(nb_bloc_row * nb_spot_bloc * (nb_bloc_col-1)- nb_spot_row + nb_spot_bloc );

		TSpot sp4 = (TSpot)v.get(v.size()-1);
		ip.setColor(c);

		Polygon p = new Polygon();
		p.addPoint((int)(sp1.getXCenter()/pixelW), (int)(sp1.getYCenter()/pixelH));
		p.addPoint((int)(sp2.getXCenter()/pixelW), (int)(sp2.getYCenter()/pixelH));
		p.addPoint((int)(sp4.getXCenter()/pixelW), (int)(sp4.getYCenter()/pixelH));
		p.addPoint((int)(sp3.getXCenter()/pixelW), (int)(sp3.getYCenter()/pixelH));

		ip.drawPolygon(p);


	}
	
	/**
	 * @author ajulin
	 * @param ip
	 * @param c
	 * @param nb index of the current block
	 * 
	 * draw in the current ImagePlus the border of the block indexed by nb
	 */
	private static void drawIJBlock(ImageProcessor ip,Color c ,int nb){
		TGridModel gm = alignment.getGridModel();
		TGridConfig gc = gm.getConfig();
		int spotH = (int)(gc.getSpotsHeight()/(2*pixelH));
		int spotW = (int)(gc.getSpotsWidth()/(2*pixelW));

		Vector v = gm.getLevel1Elements();
		TGridBlock gb = (TGridBlock)v.get(nb);
		Vector spots = gb.getSpots();

		TSpot sp1 = (TSpot)gb.getFirstElement();
		TSpot sp2 = (TSpot)spots.get(gc.getLev1NbCols()-1);
		TSpot sp3 = (TSpot)gb.getLastElement();
		TSpot sp4 = (TSpot)spots.get(gc.getLev1NbCols() * gc.getLev1NbRows() - gc.getLev1NbCols());

		Polygon p = new Polygon();
		p.addPoint((int)(sp1.getXCenter()/pixelW) - spotW, (int)(sp1.getYCenter()/pixelH) - spotH);
		p.addPoint((int)(sp2.getXCenter()/pixelW) + spotW, (int)(sp2.getYCenter()/pixelH) - spotH);
		p.addPoint((int)(sp3.getXCenter()/pixelW) + spotW, (int)(sp3.getYCenter()/pixelH) + spotH);
		p.addPoint((int)(sp4.getXCenter()/pixelW) - spotW, (int)(sp4.getYCenter()/pixelH) + spotH);
		ip.setColor(c);
		ip.drawPolygon(p);

	}
	
	/**
	 * @author ajulin
	 * @param ip
	 * @param c
	 * 
	 * Draw in an ImagePlus borders of all blocks 
	 */
	private static void drawIJAllBlock(ImageProcessor ip,Color c){
		int nb_bloc_row = alignment.getGridModel().getConfig().getLev2NbCols();
		int nb_bloc_col =  alignment.getGridModel().getConfig().getLev2NbRows();
		int nb_bloc = nb_bloc_row * nb_bloc_col;

		for(int i = 0 ; i < nb_bloc ; i++){
			drawIJBlock(ip,c,i);
		}

	}
	
	/**
	 * 
	 * @param ip
	 * @param xcenter
	 * @param ycenter
	 * @param c
	 * 
	 * Draw in ImagePlus a cross at position (xcenter,ycenter) in pixel
	 */
	private static void drawIJ_Cross(ImageProcessor ip, int xcenter, int ycenter, Color c){
		ip.setColor(c);
		ip.drawLine(xcenter-2,ycenter , xcenter+2, ycenter);
		ip.drawLine(xcenter,ycenter-2 , xcenter, ycenter+2);

	}


}




class zone{
	double x1;
	double x2;
	double y1;
	double y2;
	double spotH;
	double spotW;

	zone(double a, double b, double c, double d,TGridModel gm){
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
		spotH = gm.getPixelHeight();
		spotW = gm.getPixelWidth();
		//System.out.println("spotH = "+spotH);
		//System.out.println("spotW = "+spotW);
	}

	boolean appartientA(double x, double y){
		boolean res = false;

		if((x > x1) && (x < x2) && (y > y1) && (y < y2))
			res = true;

		return res;
	}

	public int getX1() {
		return (int)(x1/spotW);

	}

	public int getX2() {
		return (int)(x2/spotW);
	}

	public int getY1() {
		return (int)(y1/spotH);	/**
		 * @author ajulin
		 * @param ip
		 * @param c
		 * 
		 * draw in ImagePlus the border of the current grid
		 */
	}

	public int getY2() {
		return (int)(y2/spotH);
	}


}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 IINRA 2007 - SIGENAE
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
