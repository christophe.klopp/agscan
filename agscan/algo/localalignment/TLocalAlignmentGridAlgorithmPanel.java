package agscan.algo.localalignment;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;

public class TLocalAlignmentGridAlgorithmPanel extends TAlgorithmControlPanel {
  private JTextField firstRunSensitivityTextField, secondRunSensitivityTextField, thirdRunSensitivityTextField,
                     sensitivityThresholdTextField, firstRunIterationNumberTextField, secondRunIterationNumberTextField,
                     thirdRunIterationNumberTextField, bootstrapIterationNumberTextField, bootstrapSensitivityTextField;
  private static TLocalAlignmentGridAlgorithmPanel instance = null;
  private TAlgorithmControlPanel gridBlockAlignmentAlgorithmControlPanel;

  protected TLocalAlignmentGridAlgorithmPanel(TLocalAlignmentGridAlgorithm algo) {
    super();
    gridBlockAlignmentAlgorithmControlPanel = algo.getBlockAlgorithm().getControlPanel();
    jbInit();
  }
  private void jbInit() {
    removeAll();
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(gridBlockAlignmentAlgorithmControlPanel);
    JPanel panel = new JPanel();
    firstRunSensitivityTextField = new JTextField();
    secondRunSensitivityTextField = new JTextField();
    thirdRunSensitivityTextField = new JTextField();
    sensitivityThresholdTextField = new JTextField();
    firstRunIterationNumberTextField = new JTextField();
    secondRunIterationNumberTextField = new JTextField();
    thirdRunIterationNumberTextField = new JTextField();
    bootstrapIterationNumberTextField = new JTextField();
    bootstrapSensitivityTextField = new JTextField();
    Border border1 = BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140));
    TitledBorder titledBorder1 = new TitledBorder(border1,Messages.getString("TLocalAlignmentGridAlgorithmPanel.0"), //$NON-NLS-1$
                                                  TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
                                                  new java.awt.Font("Dialog", 1, 11), Color.black); //$NON-NLS-1$
    JLabel firstRunSensitivityLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.2")); //$NON-NLS-1$
    panel.setBorder(titledBorder1);
    panel.setLayout(new GridBagLayout());
    JLabel secondRunSensitivityLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.3")); //$NON-NLS-1$
    JLabel thirdRunSensitivityLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.4")); //$NON-NLS-1$
    JLabel sensitivityThresholdLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.5")); //$NON-NLS-1$
    JLabel firstRunIterationNumberLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.6")); //$NON-NLS-1$
    JLabel secondRunIterationNumberLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.7")); //$NON-NLS-1$
    JLabel thirdRunIterationNumberLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.8")); //$NON-NLS-1$
    JLabel bootstrapIterationNumberLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.9")); //$NON-NLS-1$
    JLabel bootstrapSensitivityLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.10")); //$NON-NLS-1$
    JLabel bootstrapLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.11")); //$NON-NLS-1$
    bootstrapLabel.setFont(new java.awt.Font("Dialog", 2, 11)); //$NON-NLS-1$
    JLabel firstRunLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.13")); //$NON-NLS-1$
    firstRunLabel.setFont(new java.awt.Font("Dialog", 2, 11)); //$NON-NLS-1$
    JLabel secondRunLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.15")); //$NON-NLS-1$
    secondRunLabel.setFont(new java.awt.Font("Dialog", 2, 11)); //$NON-NLS-1$
    JLabel thirdRunLabel = new JLabel(Messages.getString("TLocalAlignmentGridAlgorithmPanel.17")); //$NON-NLS-1$
    thirdRunLabel.setFont(new java.awt.Font("Dialog", 2, 11)); //$NON-NLS-1$

    panel.add(bootstrapSensitivityLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(bootstrapSensitivityTextField, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0), 30, 0));
    panel.add(firstRunSensitivityLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(firstRunSensitivityTextField, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0), 30, 0));
    panel.add(secondRunSensitivityLabel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(secondRunSensitivityTextField, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0), 30, 0));
    panel.add(thirdRunSensitivityLabel, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(thirdRunSensitivityTextField, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 0), 30, 0));
    panel.add(bootstrapLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));
    panel.add(firstRunLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));
    panel.add(bootstrapIterationNumberLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(bootstrapIterationNumberTextField, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 30, 0));
    panel.add(secondRunLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));
    panel.add(firstRunIterationNumberLabel, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(firstRunIterationNumberTextField, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 30, 0));
    panel.add(secondRunIterationNumberLabel, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(secondRunIterationNumberTextField, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 30, 0));
    panel.add(thirdRunLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));
    panel.add(thirdRunIterationNumberLabel, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    panel.add(thirdRunIterationNumberTextField, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 30, 0));
    panel.add(sensitivityThresholdLabel, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(20, 10, 10, 0), 0, 0));
    panel.add(sensitivityThresholdTextField, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(20, 5, 10, 0), 30, 0));
    add(panel);
  }
  public void init(TAlgorithm algo) {
    TLocalAlignmentGridAlgorithm alg = (TLocalAlignmentGridAlgorithm)algo;
    firstRunSensitivityTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.FIRST_RUN_SENSITIVITY).toString());
    firstRunIterationNumberTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.FIRST_RUN_ITERATIONS).toString());
    bootstrapSensitivityTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.BOOTSTRAP_SENSITIVITY).toString());
    bootstrapIterationNumberTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.BOOTSTRAP_ITERATIONS).toString());
    secondRunSensitivityTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_SENSITIVITY).toString());
    secondRunIterationNumberTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_ITERATIONS).toString());
    thirdRunSensitivityTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.THIRD_RUN_SENSITIVITY).toString());
    thirdRunIterationNumberTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.THIRD_RUN_ITERATIONS).toString());
    sensitivityThresholdTextField.setText(alg.getParameter(TLocalAlignmentGridAlgorithm.SENSITIVITY_THRESHOLD).toString());
    gridBlockAlignmentAlgorithmControlPanel.init(alg.getBlockAlgorithm());
  }
  public int getBootstrapSensitivity() {
    return Integer.parseInt(bootstrapSensitivityTextField.getText());
  }
  public int getFirstRunSensitivity() {
    return Integer.parseInt(firstRunSensitivityTextField.getText());
  }
  public int getSecondRunSensitivity() {
    return Integer.parseInt(secondRunSensitivityTextField.getText());
  }
  public int getThirdRunSensitivity() {
    return Integer.parseInt(thirdRunSensitivityTextField.getText());
  }
  public int getBootstrapIterations() {
    return Integer.parseInt(bootstrapIterationNumberTextField.getText());
  }
  public int getFirstRunIterations() {
    return Integer.parseInt(firstRunIterationNumberTextField.getText());
  }
  public int getSecondRunIterations() {
    return Integer.parseInt(secondRunIterationNumberTextField.getText());
  }
  public int getThirdRunIterations() {
    return Integer.parseInt(thirdRunIterationNumberTextField.getText());
  }
  public double getSensitivityThreshold() {
      return Double.parseDouble(sensitivityThresholdTextField.getText());
  }
  public TGridAlignmentAlgorithmPanel getGridAlignmentAlgorithmControlPanel() {
    return (TGridAlignmentAlgorithmPanel)gridBlockAlignmentAlgorithmControlPanel;
  }
  public static TLocalAlignmentGridAlgorithmPanel getInstance(TLocalAlignmentGridAlgorithm algo) {
    if (instance == null)
      instance = new TLocalAlignmentGridAlgorithmPanel(algo);
    else
      instance.jbInit();
    instance.init(algo);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
