package agscan.algo.localalignment;

import agscan.AGScan;
import agscan.algo.factory.TSpotDetectionAlgorithmFactory;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;

public class TGridAlignmentAlgorithm extends TGridBlockAlignmentAlgorithm {
  public static final int T = 0;
  public static final int C = 1;
  public static final int SENSITIVITY = 2;
  public static final int SPOT_DETECTION_ALGORITHM = 3;

  public static final int RESULT = 0;

  public static final String NAME = "Alignement local de grille";

  private double[][] spotSpeed;
  private double[][] spotPosition;
  private Object[] spots;

  private static double t = 0.1;//0.1
  private static double c = 0.7;//0.7
  private static String spotDetectionAlgorithmName = TFourProfilesSpotDetectionAlgorithm.NAME;
  private static Object[] locSpotDetectionAlgorithmParameters = { 
  		//new Integer(40), new Integer(10), new Integer(30)
//  	 il faut tenir comptes des valeurs par defaut pour initialiser les champs...
		// ici on met les valeurs par defaut de la detection de spots  4 profils
		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone")),
		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone")),
		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold"))		
  };

  private static double lastT = t;
  private static double lastC = c;
  private static String lastSpotDetectionAlgorithmName = spotDetectionAlgorithmName;
  private static Object[] lastLocSpotDetectionAlgorithmParameters;

  static {
    lastLocSpotDetectionAlgorithmParameters = new Object[locSpotDetectionAlgorithmParameters.length];
    for (int i = 0; i < locSpotDetectionAlgorithmParameters.length; i++)
      lastLocSpotDetectionAlgorithmParameters[i] = locSpotDetectionAlgorithmParameters[i];
  }

  public TGridAlignmentAlgorithm() {
    super();
    parameters = new Object[4];
    results = new Object[1];
    initWithDefaults();
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    setParameter(SPOT_DETECTION_ALGORITHM, factory.createAlgorithm(spotDetectionAlgorithmName, true));
    controlPanel = TGridAlignmentAlgorithmPanel.getInstance(this);
    initWithLast();
  }
  public String getName() {
    return NAME;
  }
  public void init() {
    setParameter(T, new Double(((TGridAlignmentAlgorithmPanel)controlPanel).getT()));
    setParameter(C, new Double(((TGridAlignmentAlgorithmPanel)controlPanel).getC()));
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(((TGridAlignmentAlgorithmPanel)controlPanel).getSpotDetectionAlgorithmName(), false);
    sdAlgo.init();
    setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
  }
  public void initWithDefaults() {
    setParameter(T, new Double(t));
    setParameter(C, new Double(c));
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(spotDetectionAlgorithmName, true);
    sdAlgo.init(locSpotDetectionAlgorithmParameters);
    setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
  }
  public void initSpotsData() {
    TGridBlock gb = (TGridBlock)workingData[GRID_BLOCK];
    if (gb != null) {
      spots = gb.getSpots().toArray();
      spotSpeed = new double[2][spots.length];
      spotPosition = new double[2][spots.length];
      TSpot s;
      for (int i = 0; i < spots.length; i++) {
        s = (TSpot) spots[i];
        spotSpeed[0][i] = 0;
        spotSpeed[1][i] = 0;
        spotPosition[0][i] = s.getXCenter();
        spotPosition[1][i] = s.getYCenter();
      }
    }
  }
  public void init(Object[] params) {
    setParameter(T, params[0]);
    setParameter(C, params[1]);
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(params[2], true);
    sdAlgo.init((Object[])params[3]);
    setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
  }
  // ajout integration 14/09/05 
  public String getStringParams() {
    String s = "";
    s = getParameter(T).toString() + "|" + getParameter(C).toString() + "|" +
        ((TSpotDetectionAlgorithm)getParameter(SPOT_DETECTION_ALGORITHM)).getStringParams();
    return s;
  }

  public void setLasts() {
    lastT = ((Double)parameters[T]).doubleValue();
    lastC = ((Double)parameters[C]).doubleValue();
    lastSpotDetectionAlgorithmName = ((TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM]).getName();
    lastLocSpotDetectionAlgorithmParameters = ((TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM]).getParameters();
  }
  public void initWithLast() {
    parameters[T] = new Double(lastT);
    parameters[C] = new Double(lastC);
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(lastSpotDetectionAlgorithmName, true);
    sdAlgo.init(lastLocSpotDetectionAlgorithmParameters);
    parameters[SPOT_DETECTION_ALGORITHM] = sdAlgo;
  }
  public void setSensitivity(int sens) {
    setParameter(SENSITIVITY, new Double(sens));
  }
  public void execute(boolean thread) {
    TSpot s = null;
    double DX, DY;
    double[] dbl;

    double t = ((Double)parameters[T]).doubleValue();
    double c = ((Double)parameters[C]).doubleValue();

    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM];
    sdAlgo.initData(workingData[IMAGE_DATA], workingData[XRES], workingData[YRES], workingData[IMAGE_WIDTH_IN_PIXELS],
                        workingData[IMAGE_HEIGHT_IN_PIXELS], workingData[HISTOGRAM_MIN], workingData[HISTOGRAM_MAX]);
    //sdAlgo.init();
    for (int i = 0; i < spots.length; i++) {
      dbl = nextSpeed(i);
      spotSpeed[0][i] = dbl[0];
      spotSpeed[1][i] = dbl[1];
    }
    double dx, dy;
    DX = DY = 0.0D;
    double angle, XX, YY;
    for (int i = 0; i < spots.length; i++) {
      s = (TSpot)spots[i];
      dx = t * spotSpeed[0][i];
      dy = t * spotSpeed[1][i];
      DX += dx;
      DY += dy;
      XX = dx * ((Double)workingData[XRES]).doubleValue(); //Math.cos(angle) * (dx + dy * Math.tan(angle));
      YY = dy * ((Double)workingData[YRES]).doubleValue(); //dy / Math.cos(angle) - XX * Math.tan(angle);
      s.addPosition(XX, YY);
      spotPosition[0][i] += XX;
      spotPosition[1][i] += YY;
    }
    TGridBlock gridBlock = (TGridBlock)workingData[GRID_BLOCK];
    angle = gridBlock.getAngleTotal();
    XX = Math.cos(angle) * (DX / spots.length + DY / spots.length * Math.tan(angle));
    YY = DY / spots.length / Math.cos(angle) - XX * Math.tan(angle);
    XX *= ((Double)workingData[XRES]).doubleValue();
    YY *= ((Double)workingData[YRES]).doubleValue();
    for (int i = 0; i < spots.length; i++) {
      s = (TSpot)spots[i];
      s.addPosition(-XX, -YY);
    }
    gridBlock.addPosition(XX, YY);
    results[RESULT] = new Double((DX * DX + DY * DY) / (double)spots.length);
  }
  public TSpotDetectionAlgorithm getSpotDetectionAlgorithm() {
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM];
    return sdAlgo;
  }
  private double[] nextSpeed(int i) {
    double speedX = spotSpeed[0][i];
    double speedY = spotSpeed[1][i];
    double posX = spotPosition[0][i];
    double posY = spotPosition[1][i];
    double xr = ((Double)workingData[XRES]).doubleValue();
    double yr = ((Double)workingData[YRES]).doubleValue();
    int posXPixel = (int)(posX / xr);
    int posYPixel = (int)(posY / yr);
    int w = ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue();
    int[] imageTab = (int[])workingData[IMAGE_DATA];
    double sumX = 0.0D, sumY = 0.0D;
    double r;
    TSpot spot, leftN, rightN, topN, bottomN;
    spot = (TSpot)spots[i];
    TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM];
    if (!((Boolean)workingData[ALL_GOOD]).booleanValue()) {
      sdAlgo.setWorkingData(TSpotDetectionAlgorithm.SPOTS, spot);
      sdAlgo.execute(true);
    }
    else
      spot.setQuality(0);
    int spotQuality = spot.getQuality();
    int iLeft = 0, iRight = 0, iTop = 0, iBottom = 0, k = 0;

    int iL, iR;
    while (true) {
      k++;
      iL = (int)posXPixel - k + (int)posYPixel * w;
      iR = (int)posXPixel + k + (int)posYPixel * w;
      if ((iL >= 0) && (iL < imageTab.length))
        iLeft += ~imageTab[iL] & 0xFFFF;
      else
        iLeft += 65535;
      if ((iR >= 0) && (iR < imageTab.length))
        iRight += ~imageTab[iR] & 0xFFFF;
      else
        iRight += 65535;
      if ((iLeft != iRight) || (k > 40)) break;
    }
    iLeft /= k;
    iRight /= k;

    int iT, iB;
    k = 0;
    while (true) {
      k++;
      iT = (int)posXPixel + (int)(posYPixel - k) * w;
      iB = (int)posXPixel + (int)(posYPixel + k) * w;
      if ((iT >= 0) && (iT < imageTab.length))
        iTop += ~imageTab[iT] & 0xFFFF;
      else
        iTop += 65535;
      if ((iB >= 0) && (iB < imageTab.length))
        iBottom += ~imageTab[iB] & 0xFFFF;
      else
        iBottom += 65535;
      if ((iTop != iBottom) || (k > 40)) break;
    }
    iTop /= k;
    iBottom /= k;
    TGridModel gridModel = (TGridModel)workingData[GRID_MODEL];
    TGridBlock gridBlock = (TGridBlock)workingData[GRID_BLOCK];
    leftN = spot.getGlobalLeft();
    rightN = spot.getGlobalRight();
    topN = spot.getGlobalTop();
    bottomN = spot.getGlobalBottom();
    double[] dbl = new double[2];
    double dx, dy;
    if (leftN != null) {
      dbl[0] = leftN.getXCenter();
      dbl[1] = leftN.getYCenter();
    }
    else
      dbl = gridModel.getWestAnchor(spot, gridBlock);
    dx = (posX - dbl[0]) / xr;
    dy = (posY - dbl[1]) / yr;
    r = dx * dx + dy * dy;
    sumX += (spot.getWidth() * spot.getWidth() / xr / xr / r - 1.0D) * dx;
    sumY += (spot.getHeight() * spot.getHeight() / yr / yr / r - 1.0D) * dy;
    if (rightN != null) {
      dbl[0] = rightN.getXCenter();
      dbl[1] = rightN.getYCenter();
    }
    else
      dbl = gridModel.getEastAnchor(spot, gridBlock);
    dx = (posX - dbl[0]) / xr;
    dy = (posY - dbl[1]) / yr;
    r = dx * dx + dy * dy;
    sumX += (spot.getWidth() * spot.getWidth() / xr / xr / r - 1.0D) * dx;
    sumY += (spot.getHeight() * spot.getHeight() / yr / yr / r - 1.0D) * dy;
    if (topN != null) {
      dbl[0] = topN.getXCenter();
      dbl[1] = topN.getYCenter();
    }
    else
      dbl = gridModel.getNorthAnchor(spot, gridBlock);
    dx = (posX - dbl[0]) / xr;
    dy = (posY - dbl[1]) / yr;
    r = dx * dx + dy * dy;
    sumX += (spot.getWidth() * spot.getWidth() / xr / xr / r - 1.0D) * dx;
    sumY += (spot.getHeight() * spot.getHeight() / yr / yr / r - 1.0D) * dy;
    if (bottomN != null) {
      dbl[0] = bottomN.getXCenter();
      dbl[1] = bottomN.getYCenter();
    }
    else
      dbl = gridModel.getSouthAnchor(spot, gridBlock);
    dx = (posX - dbl[0]) / xr;
    dy = (posY - dbl[1]) / yr;
    r = dx * dx + dy * dy;
    sumX += (spot.getWidth() * spot.getWidth() / xr / xr / r - 1.0D) * dx;
    sumY += (spot.getHeight() * spot.getHeight() / yr / yr / r - 1.0D) * dy;
    double t = ((Double)parameters[T]).doubleValue();
    double c = ((Double)parameters[C]).doubleValue();
    double alpha = ((Integer)parameters[SENSITIVITY]).doubleValue() / 1000.0D;
    double[] ret = { speedX + t * (-c * speedX - spotQuality * alpha * (double)(iRight - iLeft) / 2.0D + sumX),
        speedY + t * (-c * speedY - spotQuality * alpha * (double)(iBottom - iTop) / 2.0D + sumY) };
    return ret;
  }
  public double getDefaultT() {
    return t;
  }
  public double getDefaultC() {
    return c;
  }
  public String getDefaultSpotDetectionAlgorithmName() {
    return spotDetectionAlgorithmName;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
