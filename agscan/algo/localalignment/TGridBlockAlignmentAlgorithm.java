package agscan.algo.localalignment;

import agscan.algo.TAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;

public abstract class TGridBlockAlignmentAlgorithm extends TAlgorithm {
  public static final int XRES = 0;
  public static final int YRES = 1;
  public static final int IMAGE_DATA = 2;
  public static final int IMAGE_WIDTH_IN_PIXELS = 3;
  public static final int IMAGE_HEIGHT_IN_PIXELS = 4;
  public static final int GRID_MODEL = 5;
  public static final int ALL_GOOD = 6;
  public static final int GRID_BLOCK = 7;
  public static final int HISTOGRAM_MIN = 8;
  public static final int HISTOGRAM_MAX = 9;

  public TGridBlockAlignmentAlgorithm() {
    super();
    workingData = new Object[10];
  }
  public void initData(double xRes, double yRes, int[] imageData, int imageWidthInPixels, int imageHeightInPixels,
                       TGridModel gridModel, boolean allGood, TGridBlock gridBlock, int histoMin, int histoMax) {
    workingData[XRES] = new Double(xRes);
    workingData[YRES] = new Double(yRes);
    workingData[IMAGE_DATA] = imageData;
    workingData[IMAGE_WIDTH_IN_PIXELS] = new Integer(imageWidthInPixels);
    workingData[IMAGE_HEIGHT_IN_PIXELS] = new Integer(imageHeightInPixels);
    workingData[GRID_MODEL] = gridModel;
    workingData[ALL_GOOD] = new Boolean(allGood);
    workingData[GRID_BLOCK] = gridBlock;
    workingData[HISTOGRAM_MIN] = new Integer(histoMin);
    workingData[HISTOGRAM_MAX] = new Integer(histoMax);
  }
  public void initData(Object xRes, Object yRes, Object imageData, Object imageWidthInPixels, Object imageHeightInPixels,
                       Object gridModel, Object allGood, Object gridBlock, Object histoMin, Object histoMax) {
    workingData[XRES] = xRes;
    workingData[YRES] = yRes;
    workingData[IMAGE_DATA] = imageData;
    workingData[IMAGE_WIDTH_IN_PIXELS] = imageWidthInPixels;
    workingData[IMAGE_HEIGHT_IN_PIXELS] = imageHeightInPixels;
    workingData[GRID_MODEL] = gridModel;
    workingData[ALL_GOOD] = allGood;
    workingData[GRID_BLOCK] = gridBlock;
    workingData[HISTOGRAM_MIN] = histoMin;
    workingData[HISTOGRAM_MAX] = histoMax;
  }
  public abstract void init();
  public abstract void initSpotsData();
  public abstract TSpotDetectionAlgorithm getSpotDetectionAlgorithm();
  public abstract String getName();
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
