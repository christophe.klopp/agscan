package agscan.algo.localalignment;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;
import agscan.algo.factory.TSpotDetectionAlgorithmFactory;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TTwoProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithm;

public class TLevel1BlockAlignmentAlgorithmPanel extends TAlgorithmControlPanel implements ActionListener {
  private JTextField tTextField, cTextField;
  private JComboBox spotDetectionAlgorithmComboBox = new JComboBox();
  private static TLevel1BlockAlignmentAlgorithmPanel instance = null;
  private TAlgorithmControlPanel spotDetectionControlPanel;

  protected TLevel1BlockAlignmentAlgorithmPanel(TLevel1BlockAlignmentAlgorithm algo) {
    super();
    tTextField = new JTextField(String.valueOf(algo.getDefaultT()));
    cTextField = new JTextField(String.valueOf(algo.getDefaultC()));

    spotDetectionControlPanel = algo.getSpotDetectionAlgorithm().getControlPanel();
    spotDetectionAlgorithmComboBox.addItem(TTwoProfilesSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.addItem(TFourProfilesSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.addItem(TMultiChannelSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.setSelectedItem(algo.getDefaultSpotDetectionAlgorithmName());
    jbInit();
    spotDetectionAlgorithmComboBox.addActionListener(this);
  }
  public void actionPerformed(ActionEvent event) {
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm algo = (TSpotDetectionAlgorithm)factory.createAlgorithm(spotDetectionAlgorithmComboBox.getSelectedItem(), true);
    algo.initWithLast();
    spotDetectionControlPanel = algo.getControlPanel();
    jbInit();
    doLayout();
    algo.getControlPanel().doLayout();
    repaint();
  }
  private void jbInit() {
    removeAll();
    Border border1 = BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140));
    TitledBorder titledBorder1 = new TitledBorder(border1, Messages.getString("TLevel1BlockAlignmentAlgorithmPanel.0"), //$NON-NLS-1$
                                     TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
                                     new java.awt.Font("Dialog", 1, 11), Color.black); //$NON-NLS-1$
    setLayout(new GridBagLayout());
    setBorder(titledBorder1);

    JLabel tLabel = new JLabel("t :"); //$NON-NLS-1$
    JLabel cLabel = new JLabel(Messages.getString("TLevel1BlockAlignmentAlgorithmPanel.3")); //$NON-NLS-1$
    JLabel spotDetectionAlgorithmLabel = new JLabel(Messages.getString("TLevel1BlockAlignmentAlgorithmPanel.4")); //$NON-NLS-1$

    add(tLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 10, 0), 0, 0));
    add(tTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 10, 10), 50, 0));
    add(cLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 20, 10, 0), 0, 0));
    add(cTextField, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 10, 10), 50, 0));
    add(spotDetectionAlgorithmComboBox, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 10), 0, 0));
    add(spotDetectionAlgorithmLabel, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(2, 10, 10, 0), 0, 0));
    add(spotDetectionControlPanel, new GridBagConstraints(0, 2, 4, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 10, 0), 0, 0));
  }
  public double getT() {
    double ret = 0;
    try {
      ret = Double.parseDouble(tTextField.getText());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return ret;
  }
  public double getC() {
    double ret = 0;
    try {
      ret = Double.parseDouble(cTextField.getText());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return ret;
  }
  public String getSpotDetectionAlgorithmName() {
    return (String)spotDetectionAlgorithmComboBox.getSelectedItem();
  }
  public TAlgorithmControlPanel getSpotDetectionAlgorithmControlPanel() {
    return spotDetectionControlPanel;
  }
  public void init(TAlgorithm algo) {
    TLevel1BlockAlignmentAlgorithm alg = (TLevel1BlockAlignmentAlgorithm)algo;
    tTextField.setText(alg.getParameter(TLevel1BlockAlignmentAlgorithm.T).toString());
    cTextField.setText(alg.getParameter(TLevel1BlockAlignmentAlgorithm.C).toString());
    spotDetectionAlgorithmComboBox.setSelectedItem(alg.getSpotDetectionAlgorithm().getName());
    spotDetectionControlPanel = alg.getSpotDetectionAlgorithm().getControlPanel();
    spotDetectionControlPanel.init(alg.getSpotDetectionAlgorithm());
  }
  public static TLevel1BlockAlignmentAlgorithmPanel getInstance(TLevel1BlockAlignmentAlgorithm algo) {
    if (instance == null)
      instance = new TLevel1BlockAlignmentAlgorithmPanel(algo);
    else
      instance.jbInit();
    instance.init(algo);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
