package agscan.algo.localalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.AGScan;
import agscan.Messages;
import agscan.algo.factory.TGridBlockAlignmentAlgorithmFactory;
import agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithmPanel;

public class TLocalAlignmentBlockTemplateAlgorithm extends TLocalAlignmentAlgorithm {

	public static final int MAX_AREA_SIZE = 0;// borne max de l'aire acceptée dans l'analyseur
	public static final int MIN_AREA_SIZE = 1;//borne min de l'aire acceptée dans l'analyseur
	public static final int KIND_KERNEL = 2;//type de kernel choisi par l'utilisateur
	public static final int CSS = 3;// activation de la correction spot par spot
	public static final int ADAPT_DIAM = 4;//activation de la correction du diametre
	public static final int SEUIL_CIRCU = 5;//seuil de circularité maximale choisie pour le traitement
	
	
	public double startProgress, progressWidth;
	private static int MaxAreaSize = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.MaxAreaSize");
	private static int MinAreaSize = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.MinAreaSize");
	private static int KindKernel = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.KindKernel");
	private static int Css = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.CSS");
	private static int AdaptDiam = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.adaptDiam");
	private static int SeuilCircu = AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.seuilCircu");
	
	
	private static int lastMaxAreaSize,lastMinAreaSize,lastKindKernel, lastCss, lastAdaptDiam,lastSeuilCircu;
	
	
	static {
		lastMaxAreaSize = MaxAreaSize;
		lastMinAreaSize = MinAreaSize;
		lastKindKernel = KindKernel;
		lastCss = CSS;
		lastAdaptDiam = AdaptDiam;
		lastSeuilCircu = SeuilCircu;
	}  
	
	public static final String NAME = "Alignement local par Template Matching"; //$NON-NLS-1$

	public TLocalAlignmentBlockTemplateAlgorithm() {
		super();
	    parameters = new Object[6];
	   // initWithLast();
	   // controlPanel = TLocalAlignmentBlockTemplateAlgorithmPanel.getInstance(this, init);
	 
		initWithLast();
		controlPanel = TLocalAlignmentBlockTemplateAlgorithmPanel.getInstance(this);
		startProgress = 0;
		progressWidth = 100;
	}


	public String getName() {
		return NAME;
	}

	public void init() {
		setParameter(MAX_AREA_SIZE, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMaxAreaSize()));
		setParameter(MIN_AREA_SIZE, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMinAreaSize()));
		setParameter(KIND_KERNEL, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getKindKernel()));
		setParameter(CSS, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getCSS()));
		setParameter(ADAPT_DIAM, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getAdaptDiam()));
		setParameter(SEUIL_CIRCU, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getSeuilCircu()));
	
	
	}

	public void initWithDefaults() {
		setParameter(MAX_AREA_SIZE, new Integer(MaxAreaSize));
		setParameter(MIN_AREA_SIZE, new Integer(MinAreaSize));
		setParameter(KIND_KERNEL, new Integer(KindKernel));
		setParameter(CSS, new Integer(Css));
		setParameter(ADAPT_DIAM, new Integer(AdaptDiam));
		setParameter(SEUIL_CIRCU, new Integer(SeuilCircu));
	}

	public void init(Object[] params) {
		setParameter(MAX_AREA_SIZE, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMaxAreaSize()));
		setParameter(MIN_AREA_SIZE, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMinAreaSize()));
		setParameter(KIND_KERNEL, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getKindKernel()));
		setParameter(CSS, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getCSS()));
		setParameter(ADAPT_DIAM, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getAdaptDiam()));
		setParameter(SEUIL_CIRCU, new Integer(((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getSeuilCircu()));
	}


	//ajout integration 13/09/05
	public String getStringParams() {
		String s = "";
		s = NAME;
		return s;
	}
	public void setLasts() {
		lastMaxAreaSize = ((Integer)parameters[MAX_AREA_SIZE]).intValue();
		lastMinAreaSize = ((Integer)parameters[MIN_AREA_SIZE]).intValue();
		lastKindKernel = ((Integer)parameters[KIND_KERNEL]).intValue();
		lastCss = ((Integer)parameters[CSS]).intValue();
		lastAdaptDiam = ((Integer)parameters[ADAPT_DIAM]).intValue();
		lastSeuilCircu = ((Integer)parameters[SEUIL_CIRCU]).intValue();
	}

	public void initWithLast() {

		setParameter(MAX_AREA_SIZE, new Integer(lastMaxAreaSize));
		setParameter(MIN_AREA_SIZE, new Integer(lastMinAreaSize));
		setParameter(KIND_KERNEL, new Integer(lastKindKernel));
		setParameter(CSS, new Integer(lastCss));
		setParameter(ADAPT_DIAM, new Integer(lastAdaptDiam));
		setParameter(SEUIL_CIRCU, new Integer(lastSeuilCircu));
	}

	public void saveInProperties(){
		// remi add 17/08/05
		// on sauve dans les properties si different
		// cette methode est appele quand on choisi "apply"
		int panelValue;
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMaxAreaSize();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.MaxAreaSize")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.MaxAreaSize",panelValue); 	
	  	}
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getMinAreaSize();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.MinAreaSize")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.MinAreaSize",panelValue); 	
	  	}
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getKindKernel();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.KindKernel")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.KindKernel",panelValue); 	
	  	}
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getCSS();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.CSS")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.CSS",panelValue); 	
	  	}
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getAdaptDiam();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.adaptDiam")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.adaptDiam",panelValue); 	
	  	}
	 	panelValue = ((TLocalAlignmentBlockTemplateAlgorithmPanel)controlPanel).getSeuilCircu();
		if (panelValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockTemplateAlgorithm.seuilCircu")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockTemplateAlgorithm.seuilCircu",panelValue); 	
	  	}
	}

	/*
	 * 
	 */
	public void execute(boolean thread) {
		worker = new TLocalAlignmentBlockWorker(this);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			//worker.finished();
		}
	}
	public void execute(boolean thread, int priority) {
		worker = new TLocalAlignmentBlockTemplateWorker(this);
		setPriority(priority);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}

	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TLocalAlignmentBlockAlgorithm.1"), //$NON-NLS-1$
				Messages.getString("TLocalAlignmentBlockAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (rep == JOptionPane.YES_OPTION) {
			worker.stop();
		}
	}

	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			worker.setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			worker.setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			worker.setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}


	@Override
	public TGridBlockAlignmentAlgorithm getBlockAlgorithm() {
		// TODO Auto-generated method stub
		return null;
	}


}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
