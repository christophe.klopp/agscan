package agscan.algo.localalignment;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;
import agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithm;

public class TLocalAlignmentBlockTemplateAlgorithmPanel extends TAlgorithmControlPanel implements ChangeListener,ActionListener {
	private static TLocalAlignmentBlockTemplateAlgorithmPanel instance = null;
	private JTextField showGridCenterCrossTextField, showSpotCicularityTextField, kernelsizeTextField, pixelMinSpotSizeTextField, pixelMaxSpotSizeTextField;
	//JCheckBox gridCheckCheckBox = new JCheckBox();// ajout integration 13/09/05

	private JTabbedPane jtp;
	private JLabel lb1,lb2,lb3;
	private JSlider sl4_1, sl4_2;
	private JTextField tf4_1,tf4_2;
	private JRadioButton con,gauss, circ ;
	
	private JCheckBox ck2_1,ck2_2;
	private JLabel lb4;
	private JSlider sl2_1;
	private JTextField tf2_1;
	
	
	
	protected TLocalAlignmentBlockTemplateAlgorithmPanel(TLocalAlignmentBlockTemplateAlgorithm algo) {
		super();
		showGridCenterCrossTextField = new JTextField();
		showSpotCicularityTextField = new JTextField();
		kernelsizeTextField = new JTextField();
		pixelMinSpotSizeTextField = new JTextField();
		pixelMaxSpotSizeTextField = new JTextField();
		
		jtp = new JTabbedPane(JTabbedPane.TOP);
		jtp.addTab(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.12"),onglet4());
		jtp.addTab(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.13"),onglet2());
		
		
		jbInit();
	}
	private void jbInit() {
		removeAll();
		this.add(jtp);
	}
	
	public void init(TAlgorithm algo) {
		TLocalAlignmentBlockTemplateAlgorithm alg = (TLocalAlignmentBlockTemplateAlgorithm)algo;
		tf4_1.setText(alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.MIN_AREA_SIZE).toString());
		tf4_2.setText(alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.MAX_AREA_SIZE).toString());
		sl4_1.setValue((Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.MIN_AREA_SIZE));
		sl4_2.setValue((Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.MAX_AREA_SIZE));
		
		int kind = (Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.KIND_KERNEL);
		switch(kind){
			case 1 : gauss.setSelected(true);
					 break;
			case 2 : circ.setSelected(true);
					 break;
			case 3 : con.setSelected(true);
					 break;
			default : circ.setSelected(true);
			 		  break;
		}
		
		tf2_1.setText(alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.SEUIL_CIRCU).toString());
		sl2_1.setValue((Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.SEUIL_CIRCU));
		if((Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.CSS) == 1){
			ck2_1.setSelected(true);
			
		}
		else{
			ck2_1.setSelected(false);
			ck2_2.setEnabled(false);
			lb4.setEnabled(false);
			tf2_1.setEnabled(false);
			sl2_1.setEnabled(false);
		}
		if((Integer)alg.getParameter(TLocalAlignmentBlockTemplateAlgorithm.ADAPT_DIAM) == 1){
			ck2_2.setSelected(true);
		}
		else{
			ck2_2.setSelected(false);
		}
		
		
	}

	
	
	

	//placement des spots
	public JPanel onglet2(){
		JPanel res = new JPanel();
		res.removeAll();
		Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.5")); //$NON-NLS-1$
		setBorder(titledBorder1);
		
		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints constraint = new GridBagConstraints();
		res.setLayout(gb);
		
		buildConstraints(constraint, 0, 0, 3, 1, 40, 33);
		ck2_1 = new JCheckBox(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.14"));
		ck2_1.addActionListener(this);
		gb.setConstraints(ck2_1, constraint);
		ck2_1.setSelected(true);
		res.add(ck2_1);
	
		
		
		buildConstraints(constraint, 0, 1, 3, 1, 0, 33);
		constraint.anchor = GridBagConstraints.WEST;
		ck2_2 = new JCheckBox(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.15"));
		ck2_2.addActionListener(this);
		gb.setConstraints(ck2_2, constraint);
		ck2_2.setSelected(true);
		res.add(ck2_2);
		
		constraint.anchor = GridBagConstraints.CENTER;
		
		buildConstraints(constraint, 0, 2, 1, 1, 0, 33);
		lb4 = new JLabel(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.16"));
		gb.setConstraints(lb4, constraint);
		res.add(lb4);
		
		buildConstraints(constraint, 1, 2, 1, 1, 20, 0);
		tf2_1 = etiquette(false);
		gb.setConstraints(tf2_1, constraint);
		res.add(tf2_1);
		
		buildConstraints(constraint, 2, 2, 1, 1, 40, 0);
		sl2_1 = curseur(0,100,90.0D,10,5);
		gb.setConstraints(sl2_1, constraint);
		res.add(sl2_1);
		
		return res;
	}
	
	//paramètre du traitement d'image
	public JPanel onglet4(){
		JPanel res = new JPanel();
		res.removeAll();
		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints constraint = new GridBagConstraints();
		res.setLayout(gb);
		
		Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.6")); //$NON-NLS-1$
		res.setBorder(titledBorder1);
		
		
		//label
		buildConstraints(constraint, 0, 0, 1, 1, 40, 20);
		lb1 = new JLabel(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.6"));
		gb.setConstraints(lb1, constraint);
		res.add(lb1);
		
		buildConstraints(constraint, 0, 1, 1, 1, 0, 20);
		lb2 = new JLabel(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.7"));
		gb.setConstraints(lb2, constraint);
		res.add(lb2);
		
		buildConstraints(constraint, 0, 2, 3, 1, 0, 20);
		lb3 = new JLabel(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.8"));
		gb.setConstraints(lb3, constraint);
		res.add(lb3);
		
		//etiquette
		buildConstraints(constraint, 1, 0, 1, 1, 20, 0);
		tf4_1 = etiquette(false);
		gb.setConstraints(tf4_1, constraint);
		res.add(tf4_1);
		
		buildConstraints(constraint, 1, 1, 1, 1, 0, 0);
		tf4_2 = etiquette(false);
		gb.setConstraints(tf4_2, constraint);
		res.add(tf4_2);
		
		
		//curseur
		buildConstraints(constraint, 2, 0, 1, 1, 40, 0);
		sl4_1 =curseur(0, 200,50.0D,10,5);
		gb.setConstraints(sl4_1, constraint);
		res.add(sl4_1);
		
		buildConstraints(constraint, 2, 1, 1, 1, 0, 0);
		sl4_2 =curseur(0, 200,150.0D,10,5);
		gb.setConstraints(sl4_2, constraint);
		res.add(sl4_2);
		
		//bouton radio
		constraint.anchor = GridBagConstraints.WEST;
		
		
		buildConstraints(constraint, 0, 3, 3, 1, 0, 15);
		gauss = new JRadioButton(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.9"));
		gb.setConstraints(gauss, constraint);
		res.add(gauss);
		
		buildConstraints(constraint, 0, 4, 3, 1, 0, 0);
		circ = new JRadioButton(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.10"));
		gb.setConstraints(circ, constraint);
		res.add(circ);
		
		buildConstraints(constraint, 0, 5, 3, 1, 0, 0);
		con = new JRadioButton(Messages.getString("TLocalAlignmentBlockTemplateAlgorithmPanel.11"));
		gb.setConstraints(con, constraint);
		res.add(con);
		
		constraint.anchor = GridBagConstraints.WEST;
		
		ButtonGroup gr = new ButtonGroup();
		gr.add(gauss);
		gr.add(circ);
		gr.add(con);
		circ.setSelected(true);
		repaint();
		return res;
	}
	
	public void stateChanged(ChangeEvent event){
		Object source = event.getSource();
		if(source == sl4_1){
			int newValue = sl4_1.getValue();
			tf4_1.setText(""+newValue);
			
		}
		if(source == sl4_2){
			int newValue = sl4_2.getValue();
			tf4_2.setText(""+newValue);
			
		}
		
		if(source == sl2_1){
			int newValue = sl2_1.getValue();
			tf2_1.setText(""+newValue);
			
		}

		repaint();
	}
	
	public void actionPerformed(ActionEvent ae){
		Object source = ae.getSource();
		if(source == ck2_1){
			if(!ck2_1.isSelected()){
				ck2_2.setEnabled(false);
				lb4.setEnabled(false);
				tf2_1.setEnabled(false);
				sl2_1.setEnabled(false);
				
			}
			else{
				ck2_2.setEnabled(true);
				lb4.setEnabled(true);
				tf2_1.setEnabled(true);
				sl2_1.setEnabled(true);
			}
				
		}
		if(source == ck2_2){
			System.out.println("deuxieme bouton");
		}
		jbInit();
		doLayout();
		repaint();
			
	}
	
	public void buildConstraints(GridBagConstraints gbc, int gx, int gy, int gw, int gh, int wx,int wy){
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
	}
	
	//cree un curseur numerique
	private JSlider curseur(int deb, int fin,double init,int maj,int min){
		JSlider curs = new JSlider(SwingConstants.HORIZONTAL,deb,fin,(int)init);
		curs.setMajorTickSpacing(maj);
		curs.setMinorTickSpacing(min);
		curs.setPaintLabels(false);
		curs.setPaintTicks(true);
		
		curs.addChangeListener(this);
		this.repaint();
		return curs;
	}
	
	
	//crée une etiquette
	private JTextField etiquette (boolean b){
		JTextField res = new JTextField(4);
		res.setHorizontalAlignment(SwingConstants.CENTER);
		res.setEditable(b);
		return res;
	}
	
	
	public int getMaxAreaSize(){
		int ret = -1;
		try {
			ret = Integer.parseInt(tf4_2.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	public int getMinAreaSize(){
		int ret = -1;
		try {
			ret = Integer.parseInt(tf4_1.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	//1 -> gaussian kernel
	//2 -> circular kernel
	//3 -> conic kernel
	
	public int getKindKernel(){
		int ret = -1;
		if(gauss.isSelected())
			ret = 1;
		else{
			if(circ.isSelected())
				ret = 2;
			else{
				if(con.isSelected())
					ret = 3;
			}
		}
		
		
		return ret;
	}
	
	
	public int getSeuilCircu(){
		int ret = -1;
		try {
			ret = Integer.parseInt(tf2_1.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	//0-> inactivate
	//1-> activate
	public int getCSS(){
		int ret = 0;
		try {
			if(ck2_1.isSelected())
				ret = 1;
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	
	public int getAdaptDiam(){
		int ret = 0;
		try {
			if(ck2_2.isSelected())
				ret = 1;
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	
	public static TLocalAlignmentBlockTemplateAlgorithmPanel getInstance(TLocalAlignmentBlockTemplateAlgorithm algo) {
		//   if (instance == null)
			instance = new TLocalAlignmentBlockTemplateAlgorithmPanel(algo);
		//   else
			//   instance.jbInit();
		instance.init(algo);
		return instance;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
