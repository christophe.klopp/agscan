package agscan.algo.localalignment;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TUndoLocalAlignmentBlock;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;

public class TLocalAlignmentBlockWorker extends SwingWorker {
  private TLocalAlignmentBlockAlgorithm laAlgo;
  private TUndoLocalAlignmentBlock ulab = null;

  public TLocalAlignmentBlockWorker(TLocalAlignmentAlgorithm laAlgo) {
    this.laAlgo = (TLocalAlignmentBlockAlgorithm)laAlgo;
  }
  public Object construct(boolean thread) {
    TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
    TAlignment alignment = (TAlignment)aliView.getReference();
    aliView.getTablePanel().setSelectable(false);
    if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning())))
      ulab = new TUndoLocalAlignmentBlock(aliView.getReference());
    alignment.setAlgoRunning(true);
    if (thread) alignment.setAlgorithm(laAlgo);
    TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().removeInteractors();
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    TGridModel gridModel = (TGridModel)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL);
    Vector selection = gridModel.getSelectionModel().getBlockSelection();
    TGridBlock gb;
    Enumeration lev1Enum;
    boolean gridCheck = ((Boolean)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.GRID_CHECK)).booleanValue();
    Vector v;
    laAlgo.getBlockAlgorithm().initData(laAlgo.getWorkingData(TLocalAlignmentAlgorithm.XRES), laAlgo.getWorkingData(TLocalAlignmentAlgorithm.YRES),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_DATA), laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL), new Boolean(false), null,
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.HISTOGRAM_MIN),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.HISTOGRAM_MAX));
    v = new Vector();
    for (Enumeration enume = selection.elements(); enume.hasMoreElements(); ) {
      gb = (TGridBlock)enume.nextElement();
      if (gb.getFirstElement() instanceof TSpot)
        v.addElement(gb);
      else
        v.addAll(gb.getLevel1Elements());
    }
    lev1Enum = v.elements();
    int k = 0;
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer((int)laAlgo.getStartProgress())));
    TGridBlock tgb;
    int progressRange = laAlgo.getEndProgress() - laAlgo.getStartProgress();
    int pc1 = 0, pc2 = 0;
    while (lev1Enum.hasMoreElements() && !STOP) {
      tgb = (TGridBlock)lev1Enum.nextElement();
      applyProto(tgb);
      k++;
      pc1 = (int)(laAlgo.getStartProgress() + k * progressRange / v.size());
      if (pc1 != pc2) {
        TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
        pc2 = pc1;
      }
    }
    int nb_cols = gridModel.getConfig().getLev2NbCols();// * gridModel.getConfig().getLev3NbCols();//  level3 removed - 20005/10/28
    int nb_rows = gridModel.getConfig().getLev2NbRows();// * gridModel.getConfig().getLev3NbRows();//  level3 removed - 20005/10/28
    int nb_cols_lev1 = gridModel.getConfig().getLev1NbCols();
    int nb_rows_lev1 = gridModel.getConfig().getLev1NbRows();
    TGridBlock[][] blocks = new TGridBlock[nb_rows][nb_cols];
    if (gridCheck && gridModel.getSelectionModel().isSelected(gridModel.getRootBlock())) {
      for (int c = 0; c < nb_cols; c++) {
        for (int r = 0; r < nb_rows; r++) {
          blocks[r][c] = gridModel.getLevel1Element(r * nb_rows_lev1, c * nb_cols_lev1);
        }
      }
      double yMoy, xMoy, diff;
      TEvent e;
      if (nb_cols >= nb_rows) {
        for (int r = 0; r < nb_rows; r++) {
          yMoy = getYPos(blocks, gridModel, r, 0.6D);
          diff = blocks[r][0].getYNoRotation() - yMoy;
          if (Math.abs(diff) >= (0.6D * gridModel.getConfig().getSpotsHeight())) {
            if (diff > 0) {
              e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][0],
                             new Double(0), new Double(-gridModel.getConfig().getSpotsHeight()));
              TEventHandler.handleMessage(e);
            }
            else {
              e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][0],
                             new Double(0), new Double(gridModel.getConfig().getSpotsHeight()));
              TEventHandler.handleMessage(e);
            }
          }
          for (int c = 1; c < nb_cols; c++) {
            diff = blocks[r][c - 1].getYNoRotation() - blocks[r][c].getYNoRotation();
            if (Math.abs(diff) >= (0.6D * gridModel.getConfig().getSpotsHeight())) {
              if (diff > 0) {
                e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][c],
                               new Double(0), new Double(gridModel.getConfig().getSpotsHeight()));
                TEventHandler.handleMessage(e);
              }
              else {
                e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][c],
                               new Double(0), new Double(-gridModel.getConfig().getSpotsHeight()));
                TEventHandler.handleMessage(e);
              }
              applyProto(blocks[r][c]);
            }
          }
        }
      }
      else {
        for (int c = 0; c < nb_cols; c++) {
          xMoy = getXPos(blocks, gridModel, c, 0.6D);
          diff = blocks[0][c].getXNoRotation() - xMoy;
          if (Math.abs(diff) >= (0.6D * gridModel.getConfig().getSpotsWidth())) {
            if (diff > 0) {
              e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[0][c],
                             new Double(-gridModel.getConfig().getSpotsWidth()), new Double(0));
              TEventHandler.handleMessage(e);
            }
            else {
              e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[0][c],
                             new Double(gridModel.getConfig().getSpotsWidth()), new Double(0));
              TEventHandler.handleMessage(e);
            }
          }
          for (int r = 1; r < nb_rows; r++) {
            diff = blocks[r - 1][c].getXNoRotation() - blocks[r][c].getXNoRotation();
            if (Math.abs(diff) >= (0.6D * gridModel.getConfig().getSpotsWidth())) {
              if (diff > 0) {
                e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][c],
                               new Double(gridModel.getConfig().getSpotsWidth()), new Double(0));
                TEventHandler.handleMessage(e);
              }
              else {
                e = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE_BLOCK, null, blocks[r][c],
                               new Double(-gridModel.getConfig().getSpotsWidth()), new Double(0));
                TEventHandler.handleMessage(e);
              }
              applyProto(blocks[r][c]);
            }
          }
        }
      }
    }
    return null;
  }
  private void applyProto(TGridBlock tgb) {
    int bootstrapSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
    int bootstrapIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
    int firstRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
    int firstRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
    int secondRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
    int secondRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
    int thirdRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
    int thirdRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
    double sensitivityThreshold = ((Double)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
    double moveSum = 0;
    TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);

    laAlgo.getBlockAlgorithm().setWorkingData(TGridBlockAlignmentAlgorithm.GRID_BLOCK, tgb);
    laAlgo.getBlockAlgorithm().setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(true));
    laAlgo.getBlockAlgorithm().setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(bootstrapSensitivity));
    laAlgo.getBlockAlgorithm().initSpotsData();
    for (int i = 0; i < bootstrapIterations; i++) {
      laAlgo.getBlockAlgorithm().execute(true);
    }
    aliView.getGraphicPanel().refresh();
    laAlgo.getBlockAlgorithm().setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    laAlgo.getBlockAlgorithm().setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(firstRunSensitivity));
    laAlgo.getBlockAlgorithm().initSpotsData();
    for (int i = 0; i < firstRunIterations && !STOP; i++) {
      laAlgo.getBlockAlgorithm().execute(true);
      moveSum = ((Double)laAlgo.getBlockAlgorithm().getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
      if (((Double)laAlgo.getBlockAlgorithm().getSpotDetectionAlgorithm().getResult(0)).doubleValue() >= 0.1D) break;
    }
    aliView.getGraphicPanel().refresh();
    laAlgo.getBlockAlgorithm().setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    laAlgo.getBlockAlgorithm().setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(secondRunSensitivity));
    laAlgo.getBlockAlgorithm().initSpotsData();
    for (int i = 0; i < secondRunIterations && !STOP; i++) {
      laAlgo.getBlockAlgorithm().execute(true);
      moveSum = ((Double)laAlgo.getBlockAlgorithm().getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
    }
    aliView.getGraphicPanel().refresh();
    laAlgo.getBlockAlgorithm().setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    laAlgo.getBlockAlgorithm().setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(thirdRunSensitivity));
    laAlgo.getBlockAlgorithm().initSpotsData();
    for (int i = 0; i < thirdRunIterations && !STOP; i++) {
      laAlgo.getBlockAlgorithm().execute(true);
      moveSum = ((Double)laAlgo.getBlockAlgorithm().getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
    }
    aliView.getGraphicPanel().refresh();
  }
  private double getYPos(TGridBlock[][] blocks, TGridModel gridModel, int row, double pcThr) {
    double thr = pcThr * gridModel.getConfig().getSpotsHeight();
    int nb_cols = gridModel.getConfig().getLev2NbCols();// * gridModel.getConfig().getLev3NbCols();//  level3 removed - 20005/10/28
    TreeSet ts = new TreeSet();
    for (int c = 0; c < nb_cols; c++) ts.add(new Double(blocks[row][c].getYNoRotation()));
    Iterator it = ts.iterator();
    double cH, stdH, h1, h2;
    cH = h1 = stdH = ((Double)it.next()).doubleValue();
    int n = 0, n2 = 1;
    while (it.hasNext()) {
      h2 = ((Double)it.next()).doubleValue();
      if (Math.abs(h2 - h1) > thr) {
        if (n2 > n) {
          stdH = cH / n2;
          n = n2;
        }
        else {
          n2 = 1;
          cH = h2;
        }
      }
      else {
        cH += h2;
        n2++;
      }
      h1 = h2;
    }
    if (n2 > n) stdH = cH / n2;
    return stdH;
  }
  private double getXPos(TGridBlock[][] blocks, TGridModel gridModel, int col, double pcThr) {
    double thr = pcThr * gridModel.getConfig().getSpotsWidth();
    int nb_rows = gridModel.getConfig().getLev2NbRows();// * gridModel.getConfig().getLev3NbRows();//  level3 removed - 20005/10/28
    TreeSet ts = new TreeSet();
    for (int r = 0; r < nb_rows; r++) ts.add(new Double(blocks[r][col].getXNoRotation()));
    Iterator it = ts.iterator();
    double cW, stdW, w1, w2;
    cW = w1 = stdW = ((Double)it.next()).doubleValue();
    int n = 0, n2 = 1;
    while (it.hasNext()) {
      w2 = ((Double)it.next()).doubleValue();
      if (Math.abs(w2 - w1) > thr) {
        if (n2 > n) {
          stdW = cW / n2;
          n = n2;
        }
        else {
          n2 = 1;
          cW = w2;
        }
      }
      else {
        cW += w2;
        n2++;
      }
      w1 = w2;
    }
    if (n2 > n) stdW = cW / n2;
    return stdW;
  }
  public void finished() {
    TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
    TAlignment alignment = (TAlignment)aliView.getReference();
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
    Vector v = (Vector)TEventHandler.handleMessage(ev)[0];
    TSpot spot;
    for (Enumeration enu = v.elements(); enu.hasMoreElements(); ) {
      spot = (TSpot)enu.nextElement();
      spot.setFitAlgorithm(null);
      spot.setSynchro(false, aliView.getTablePanel());
    }
    if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
      ulab.addFinalPosition();
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment, ulab);
    }
    TEventHandler.handleMessage(ev);
    alignment.setAlgoRunning(false);
    alignment.setAlgorithm(null);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    aliView.getTablePanel().setSelectable(true);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
