package agscan.algo.localalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.AGScan;
import agscan.algo.factory.TGridBlockAlignmentAlgorithmFactory;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;

public class TLocalAlignmentGridAlgorithm extends TLocalAlignmentAlgorithm {
	public static final int BOOTSTRAP_SENSITIVITY = 0;
	public static final int BOOTSTRAP_ITERATIONS = 1;
	public static final int FIRST_RUN_SENSITIVITY = 2;
	public static final int FIRST_RUN_ITERATIONS = 3;
	public static final int SECOND_RUN_SENSITIVITY = 4;
	public static final int SECOND_RUN_ITERATIONS = 5;
	public static final int THIRD_RUN_SENSITIVITY = 6;
	public static final int THIRD_RUN_ITERATIONS = 7;
	public static final int SENSITIVITY_THRESHOLD = 8;
	public static final int GRID_BLOCK_ALIGNMENT_ALGORITHM = 9;
	
	//modif 16/08/05 cf DefaultProperties.java
	/*
	 private static int bootstrapSensitivity = 30;
	 private static int bootstrapIterations = 5;
	 private static int firstRunSensitivity = 30;
	 private static int firstRunIterations = 200;
	 private static int secondRunSensitivity = 20;
	 private static int secondRunIterations =200;
	 private static int thirdRunSensitivity = 10;
	 private static int thirdRunIterations = 100;
	 private static double sensitivityThreshold =0.0000001;
	 private static double locT = 0.1;
	 private static double locC = 0.7;
	 */
	
	// mofifs integration 14/09/05 private==>public 
	public static int bootstrapSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.bootstrapSensitivity");
	public static int bootstrapIterations = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.bootstrapIterations");
	public static int firstRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.firstRunSensitivity");
	public static int firstRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.firstRunIterations");
	public static int secondRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.secondRunSensitivity");
	public static int secondRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.secondRunIterations");
	public static int thirdRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.thirdRunSensitivity");
	public static int thirdRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.thirdRunIterations");
	public static double sensitivityThreshold = AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.sensitivityThreshold");
	public static double locT = AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.locT");
	public static double locC = AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.locC");
	
	
	public static String locSpotDetectionAlgorithmName = TFourProfilesSpotDetectionAlgorithm.NAME;
	public static Object[] locSpotDetectionAlgorithmParameters = { 
			//new Integer(40), new Integer(10), new Integer(30) 
			// il faut tenir comptes des valeurs par defaut pour initialiser les champs...
			// ici on met les valeurs par defaut de la detection de spots  4 profils
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold"))		
	};
	
	public static int lastBootstrapSensitivity = bootstrapSensitivity;
	public static int lastBootstrapIterations = bootstrapIterations;
	public static int lastFirstRunSensitivity = firstRunSensitivity;
	public static int lastFirstRunIterations = firstRunIterations;
	public static int lastSecondRunSensitivity = secondRunSensitivity;
	public static int lastSecondRunIterations = secondRunIterations;
	public static int lastThirdRunSensitivity = thirdRunSensitivity;
	public static int lastThirdRunIterations = thirdRunIterations;
	public static double lastSensitivityThreshold = sensitivityThreshold;
	public static double lastLocT = locT;
	public static double lastLocC = locC;
	public static String lastLocSpotDetectionAlgorithmName = locSpotDetectionAlgorithmName;
	public static Object[] lastLocSpotDetectionAlgorithmParameters;
	
	public static final String NAME = "Alignement local de la grille entire"; //$NON-NLS-1$
	
	static {
		lastLocSpotDetectionAlgorithmParameters = new Object[locSpotDetectionAlgorithmParameters.length];
		for (int i = 0; i < locSpotDetectionAlgorithmParameters.length; i++)
			lastLocSpotDetectionAlgorithmParameters[i] = locSpotDetectionAlgorithmParameters[i];
	}
	
	public TLocalAlignmentGridAlgorithm() {
		super();
		parameters = new Object[10];
		TGridBlockAlignmentAlgorithmFactory factory = new TGridBlockAlignmentAlgorithmFactory();
		setParameter(GRID_BLOCK_ALIGNMENT_ALGORITHM, factory.createAlgorithm(TGridAlignmentAlgorithm.NAME));
		initWithLast();
		controlPanel = TLocalAlignmentGridAlgorithmPanel.getInstance(this);
		setImagesMin(1);
		setImagesMax(1);
	}
	public String getName() {
		return NAME;
	}
	public void init() {
		setParameter(BOOTSTRAP_SENSITIVITY, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getBootstrapSensitivity()));
		setParameter(BOOTSTRAP_ITERATIONS, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getBootstrapIterations()));
		setParameter(FIRST_RUN_SENSITIVITY, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getFirstRunSensitivity()));
		setParameter(FIRST_RUN_ITERATIONS, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getFirstRunIterations()));
		setParameter(SECOND_RUN_SENSITIVITY, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getSecondRunSensitivity()));
		setParameter(SECOND_RUN_ITERATIONS, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getSecondRunIterations()));
		setParameter(THIRD_RUN_SENSITIVITY, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getThirdRunSensitivity()));
		setParameter(THIRD_RUN_ITERATIONS, new Integer(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getThirdRunIterations()));
		setParameter(SENSITIVITY_THRESHOLD, new Double(((TLocalAlignmentGridAlgorithmPanel)controlPanel).getSensitivityThreshold()));
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		blockAlgorithm.init();
	}
	public void init(Object[] params) {
		setParameter(BOOTSTRAP_SENSITIVITY, params[0]);
		setParameter(BOOTSTRAP_ITERATIONS, params[1]);
		setParameter(FIRST_RUN_SENSITIVITY, params[2]);
		setParameter(FIRST_RUN_ITERATIONS, params[3]);
		setParameter(SECOND_RUN_SENSITIVITY, params[4]);
		setParameter(SECOND_RUN_ITERATIONS, params[5]);
		setParameter(THIRD_RUN_SENSITIVITY, params[6]);
		setParameter(THIRD_RUN_ITERATIONS, params[7]);
		setParameter(SENSITIVITY_THRESHOLD, params[8]);
		Object[] params2 = new Object[4];
		params2[0] = ((Object[])params[9])[0];
		params2[1] = ((Object[])params[9])[1];
		params2[2] = ((Object[])params[9])[2];
		params2[3] = ((Object[])params[9])[3];
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		blockAlgorithm.init(params2);
	    setLasts();//ajout integration 13/09/05
	}
	//ajout integration 13/09/05
	  public String getStringParams() {
	    String s = "";
	    s = getParameter(BOOTSTRAP_SENSITIVITY).toString() + "|" + getParameter(BOOTSTRAP_ITERATIONS).toString() + "|" +
	        getParameter(FIRST_RUN_SENSITIVITY).toString() + "|" + getParameter(FIRST_RUN_ITERATIONS).toString() + "|" +
	        getParameter(SECOND_RUN_SENSITIVITY).toString() + "|" + getParameter(SECOND_RUN_ITERATIONS).toString() + "|" +
	        getParameter(THIRD_RUN_SENSITIVITY).toString() + "|" + getParameter(THIRD_RUN_ITERATIONS).toString() + "|" +
	        getParameter(SENSITIVITY_THRESHOLD).toString() + "|" + ((TGridAlignmentAlgorithm)getParameter(GRID_BLOCK_ALIGNMENT_ALGORITHM)).getStringParams();
	    return s;
	  }
	public void setLasts() {
		lastBootstrapSensitivity = ((Integer)parameters[BOOTSTRAP_SENSITIVITY]).intValue();
		lastBootstrapIterations = ((Integer)parameters[BOOTSTRAP_ITERATIONS]).intValue();
		lastFirstRunSensitivity = ((Integer)parameters[FIRST_RUN_SENSITIVITY]).intValue();
		lastFirstRunIterations = ((Integer)parameters[FIRST_RUN_ITERATIONS]).intValue();
		lastSecondRunSensitivity = ((Integer)parameters[SECOND_RUN_SENSITIVITY]).intValue();
		lastSecondRunIterations = ((Integer)parameters[SECOND_RUN_ITERATIONS]).intValue();
		lastThirdRunSensitivity = ((Integer)parameters[THIRD_RUN_SENSITIVITY]).intValue();
		lastThirdRunIterations = ((Integer)parameters[THIRD_RUN_ITERATIONS]).intValue();
		lastSensitivityThreshold = ((Double)parameters[SENSITIVITY_THRESHOLD]).doubleValue();
		
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		lastLocT = ((Double)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.T)).doubleValue();
		lastLocC = ((Double)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.C)).doubleValue();
		
		TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
		lastLocSpotDetectionAlgorithmName = spotDetectionAlgorithm.getName();
		for (int i = 0; i < spotDetectionAlgorithm.getParameters().length; i++)
			lastLocSpotDetectionAlgorithmParameters[i] = spotDetectionAlgorithm.getParameter(i);
	}
	public void initWithLast() {
		setParameter(BOOTSTRAP_SENSITIVITY, new Integer(bootstrapSensitivity));
		setParameter(BOOTSTRAP_ITERATIONS, new Integer(bootstrapIterations));
		setParameter(FIRST_RUN_SENSITIVITY, new Integer(firstRunSensitivity));
		setParameter(FIRST_RUN_ITERATIONS, new Integer(firstRunIterations));
		setParameter(SECOND_RUN_SENSITIVITY, new Integer(secondRunSensitivity));
		setParameter(SECOND_RUN_ITERATIONS, new Integer(secondRunIterations));
		setParameter(THIRD_RUN_SENSITIVITY, new Integer(thirdRunSensitivity));
		setParameter(THIRD_RUN_ITERATIONS, new Integer(thirdRunIterations));
		setParameter(SENSITIVITY_THRESHOLD, new Double(sensitivityThreshold));
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		Object[] params = new Object[4];
		params[0] = new Double(lastLocT);
		params[1] = new Double(lastLocC);
		params[2] = lastLocSpotDetectionAlgorithmName;
		params[3] = lastLocSpotDetectionAlgorithmParameters;
		blockAlgorithm.init(params);
	}
	public void initWithDefaults() {
		setParameter(BOOTSTRAP_SENSITIVITY, new Integer(bootstrapSensitivity));
		setParameter(BOOTSTRAP_ITERATIONS, new Integer(bootstrapIterations));
		setParameter(FIRST_RUN_SENSITIVITY, new Integer(firstRunSensitivity));
		setParameter(FIRST_RUN_ITERATIONS, new Integer(firstRunIterations));
		setParameter(SECOND_RUN_SENSITIVITY, new Integer(secondRunSensitivity));
		setParameter(SECOND_RUN_ITERATIONS, new Integer(secondRunIterations));
		setParameter(THIRD_RUN_SENSITIVITY, new Integer(thirdRunSensitivity));
		setParameter(THIRD_RUN_ITERATIONS, new Integer(thirdRunIterations));
		setParameter(SENSITIVITY_THRESHOLD, new Double(sensitivityThreshold));
		Object[] params = new Object[4];
		params[0] = new Double(locT);
		params[1] = new Double(locC);
		params[2] = locSpotDetectionAlgorithmName;
		params[3] = locSpotDetectionAlgorithmParameters;
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		blockAlgorithm.init(params);
	}
	
	public void saveInProperties(){
		// remi add 17/08/05
		// on sauve dans les properties si different
		// cette methode est appele quand on choisi "apply"
		int panelIntValue;
		double panelDoubleValue;
		
		// bootstrapSensitivity
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
	
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.bootstrapSensitivity")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.bootstrapSensitivity",panelIntValue); 	
		}
		// bootstrapIterations
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.bootstrapIterations")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.bootstrapIterations",panelIntValue); 	
		}
		// firstRunSensitivity 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.firstRunSensitivity")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.firstRunSensitivity",panelIntValue); 	
		}
		// firstRunIterations 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.firstRunIterations")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.firstRunIterations",panelIntValue); 	
		}
		// secondRunSensitivity 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.secondRunSensitivity")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.secondRunSensitivity",panelIntValue); 	
		}
		// secondRunIterations 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.secondRunIterations")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.secondRunIterations",panelIntValue); 	
		}
		// thirdRunSensitivity 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.thirdRunSensitivity")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.thirdRunSensitivity",panelIntValue); 	
		}
		// thirdRunIterations
		panelIntValue = ((Integer)getParameter(TLocalAlignmentGridAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentGridAlgorithm.thirdRunIterations")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.thirdRunIterations",panelIntValue); 	
		}
		// sensitivityThreshold 
		panelDoubleValue = ((Double)getParameter(TLocalAlignmentGridAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.sensitivityThreshold")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.sensitivityThreshold",panelDoubleValue); 	
		}
		
		TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
		// locT 
		panelDoubleValue = ((Double)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.T)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.locT")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.locT",panelDoubleValue); 	
		}
		// locC
		panelDoubleValue = ((Double)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.C)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentGridAlgorithm.locC")){
			AGScan.prop.setProperty("TLocalAlignmentGridAlgorithm.locC",panelDoubleValue); 	
		}
		TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)blockAlgorithm.getParameter(TGridAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
		// pour maxSearchZone & maxComputeZone & qualityThreshold : appel interne...
		spotDetectionAlgorithm.saveInProperties();
		
	}
	
	
	public void execute(boolean thread) {
		worker = new TLocalAlignmentGridWorker(this);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			//worker.finished();
		}
	}
	public void execute(boolean thread, int priority) {
		worker = new TLocalAlignmentGridWorker(this);
		worker.setPriority(priority);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			//worker.finished();
		}
	}
	public TGridBlockAlignmentAlgorithm getBlockAlgorithm() {
		return (TGridBlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
	}
	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TLocalAlignmentGridAlgorithm.1"), //$NON-NLS-1$
				Messages.getString("TLocalAlignmentGridAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (rep == JOptionPane.YES_OPTION) {
			worker.stop();
		}
	}
	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			worker.setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			worker.setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			worker.setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}
	public int getDefaultBootstrapSensitivity() {
		return bootstrapSensitivity;
	}
	public int getDefaultFirstRunSensitivity() {
		return firstRunSensitivity;
	}
	public int getDefaultSecondRunSensitivity() {
		return secondRunSensitivity;
	}
	public int getDefaultThirdRunSensitivity() {
		return thirdRunSensitivity;
	}
	public int getDefaultBootstrapIterations() {
		return bootstrapIterations;
	}
	public int getDefaultFirstRunIterations() {
		return firstRunIterations;
	}
	public int getDefaultSecondRunIterations() {
		return secondRunIterations;
	}
	public int getDefaultThirdRunIterations() {
		return thirdRunIterations;
	}
	public double getDefaultSensitivityThreshold() {
		return sensitivityThreshold;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
