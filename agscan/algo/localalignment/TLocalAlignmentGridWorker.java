package agscan.algo.localalignment;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.memento.TUndoLocalAlignmentGrid;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridModel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;

public class TLocalAlignmentGridWorker extends SwingWorker {
  private TLocalAlignmentGridAlgorithm laAlgo;
  private TUndoLocalAlignmentGrid ugp = null;

  public TLocalAlignmentGridWorker(TLocalAlignmentAlgorithm laAlgo) {
    this.laAlgo = (TLocalAlignmentGridAlgorithm)laAlgo;
  }
  public Object construct(boolean thread) {
    ugp = new TUndoLocalAlignmentGrid();
    TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
    aliView.getTablePanel().setSelectable(false);
    ((TAlignment)aliView.getReference()).setAlgoRunning(true);
    if (thread) ((TAlignment)aliView.getReference()).setAlgorithm(laAlgo);
    TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, aliView.getReference(), aliView.getReference());
    TEventHandler.handleMessage(ev);
    if (thread) aliView.getGraphicPanel().removeInteractors();
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, aliView.getReference());
    TEventHandler.handleMessage(ev);
    TGridModel gridModel = (TGridModel)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL);
    TGridBlock gb;
    double moveSum = 0;
    int bootstrapSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
    int bootstrapIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
    int firstRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
    int firstRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
    int secondRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
    int secondRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
    int thirdRunSensitivity = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
    int thirdRunIterations = ((Integer)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
    double sensitivityThreshold = ((Double)laAlgo.getParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
    TGridAlignmentAlgorithm blockAlgorithm = (TGridAlignmentAlgorithm)laAlgo.getBlockAlgorithm();
    blockAlgorithm.initData(laAlgo.getWorkingData(TLocalAlignmentAlgorithm.XRES), laAlgo.getWorkingData(TLocalAlignmentAlgorithm.YRES),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_DATA), laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL), new Boolean(false), null,
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.HISTOGRAM_MIN),
                            laAlgo.getWorkingData(TLocalAlignmentAlgorithm.HISTOGRAM_MAX));
    gb = gridModel.getRootBlock();
    blockAlgorithm.setWorkingData(TGridBlockAlignmentAlgorithm.GRID_BLOCK, gb);
    blockAlgorithm.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(true));
    blockAlgorithm.setParameter(TLevel1BlockAlignmentAlgorithm.SENSITIVITY, new Integer(bootstrapSensitivity));
    blockAlgorithm.initSpotsData();
    int k = bootstrapIterations + firstRunIterations + secondRunIterations + thirdRunIterations;
    int n = 0;
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(0));
    TEventHandler.handleMessage(ev);
    for (int i = 0; i < bootstrapIterations && !STOP; i++) {
      blockAlgorithm.execute(true);
      n++;
      ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
      TEventHandler.handleMessage(ev);
    }
    aliView.getGraphicPanel().refresh();
    blockAlgorithm.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    blockAlgorithm.setParameter(TLevel1BlockAlignmentAlgorithm.SENSITIVITY, new Integer(firstRunSensitivity));
    for (int i = 0; i < firstRunIterations && !STOP; i++) {
      blockAlgorithm.execute(true);
      n++;
      ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
      TEventHandler.handleMessage(ev);
      moveSum = ((Double)blockAlgorithm.getResult(TLevel1BlockAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
    }
    n = bootstrapIterations + firstRunIterations;
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().refresh();
    blockAlgorithm.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    blockAlgorithm.setParameter(TLevel1BlockAlignmentAlgorithm.SENSITIVITY, new Integer(secondRunSensitivity));
    for (int i = 0; i < secondRunIterations && !STOP; i++) {
      blockAlgorithm.execute(true);
      n++;
      ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
      TEventHandler.handleMessage(ev);
      moveSum = ((Double)blockAlgorithm.getResult(TLevel1BlockAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
    }
    n = bootstrapIterations + firstRunIterations + secondRunIterations;
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().refresh();
    blockAlgorithm.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
    blockAlgorithm.setParameter(TLevel1BlockAlignmentAlgorithm.SENSITIVITY, new Integer(thirdRunSensitivity));
    for (int i = 0; i < thirdRunIterations && !STOP; i++) {
      blockAlgorithm.execute(true);
      n++;
      ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(n * 100 / k));
      TEventHandler.handleMessage(ev);
      moveSum = ((Double)blockAlgorithm.getResult(TLevel1BlockAlignmentAlgorithm.RESULT)).doubleValue();
      if (moveSum < sensitivityThreshold) break;
    }
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(), new Integer(100));
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().refresh();
    return null;
  }
  public void finished() {
    ugp.addFinalPosition();
    TAlignmentView aliView = (TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW);
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO,
                           ((TAlignmentView)laAlgo.getWorkingData(TLocalAlignmentAlgorithm.ALIGNMENT_VIEW)).getReference(), ugp);
    TEventHandler.handleMessage(ev);
    ((TAlignment)aliView.getReference()).setAlgoRunning(false);
    ((TAlignment)aliView.getReference()).setAlgorithm(null);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, aliView.getReference());
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, aliView.getReference());
    TEventHandler.handleMessage(ev);
    aliView.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    aliView.getTablePanel().setSelectable(true);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
