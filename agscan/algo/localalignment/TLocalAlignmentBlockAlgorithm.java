package agscan.algo.localalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.AGScan;
import agscan.algo.factory.TGridBlockAlignmentAlgorithmFactory;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;

public class TLocalAlignmentBlockAlgorithm extends TLocalAlignmentAlgorithm {
  public static final int BOOTSTRAP_SENSITIVITY = 0;
  public static final int BOOTSTRAP_ITERATIONS = 1;
  public static final int FIRST_RUN_SENSITIVITY = 2;
  public static final int FIRST_RUN_ITERATIONS = 3;
  public static final int SECOND_RUN_SENSITIVITY = 4;
  public static final int SECOND_RUN_ITERATIONS = 5;
  public static final int THIRD_RUN_SENSITIVITY = 6;
  public static final int THIRD_RUN_ITERATIONS = 7;
  public static final int SENSITIVITY_THRESHOLD = 8;
  public static final int GRID_BLOCK_ALIGNMENT_ALGORITHM = 9;
  public static final int GRID_CHECK = 10;//ajout integration 13/09/05
//modif 16/08/05 cf DefaultProperties.java
  /*
  private static int bootstrapSensitivity = 30;
  private static int bootstrapIterations = 5;
  private static int firstRunSensitivity = 30;
  private static int firstRunIterations = 200;
  private static int secondRunSensitivity = 20;
  private static int secondRunIterations = 200;
  private static int thirdRunSensitivity = 10;
  private static int thirdRunIterations = 100;
  private static double sensitivityThreshold = 0.0000001;
  private static double locT = 0.1;
  private static double locC = 0.7;
  */
  
//modification du 13/09/05 integration  -tous les private sont devenus public pour le TjobBuilder
  public static int bootstrapSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.bootstrapSensitivity");
  public static int bootstrapIterations = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.bootstrapIterations");
  public static int firstRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.firstRunSensitivity");
  public static int firstRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.firstRunIterations");
  public static int secondRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.secondRunSensitivity");
  public static int secondRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.secondRunIterations");
  public static int thirdRunSensitivity = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.thirdRunSensitivity");
  public static int thirdRunIterations = AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.thirdRunIterations");
  public static double sensitivityThreshold = AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.sensitivityThreshold");
  public static boolean gridCheck = true;//ajout integration 13/09/05
  public static double locT = AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.locT");
  public static double locC = AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.locC");
  
  public static String locSpotDetectionAlgorithmName = TFourProfilesSpotDetectionAlgorithm.NAME;
  public static Object[] locSpotDetectionAlgorithmParameters = { 
  		//new Integer(40), new Integer(10), new Integer(30) 
  		// il faut tenir comptes des valeurs par defaut pour initialiser les champs...
  		// ici on met les valeurs par defaut de la detection de spots  4 profils
  		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone")),
  		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone")),
  		new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold"))
  };

  public static int lastBootstrapSensitivity = bootstrapSensitivity;
  public static int lastBootstrapIterations = bootstrapIterations;
  public static int lastFirstRunSensitivity = firstRunSensitivity;
  public static int lastFirstRunIterations = firstRunIterations;
  public static int lastSecondRunSensitivity = secondRunSensitivity;
  public static int lastSecondRunIterations = secondRunIterations;
  public static int lastThirdRunSensitivity = thirdRunSensitivity;
  public static int lastThirdRunIterations = thirdRunIterations;
  public static double lastSensitivityThreshold = sensitivityThreshold;
  public static boolean lastGridCheck = gridCheck;// ajout integration 13/09/05
  public static double lastLocT = locT;
  public static double lastLocC = locC;
  public static String lastLocSpotDetectionAlgorithmName = locSpotDetectionAlgorithmName;
  public static Object[] lastLocSpotDetectionAlgorithmParameters;
  public double startProgress, progressWidth;

  public static final String NAME = "Alignement local bloc par bloc"; //$NON-NLS-1$

  static {
    lastLocSpotDetectionAlgorithmParameters = new Object[locSpotDetectionAlgorithmParameters.length];
    for (int i = 0; i < locSpotDetectionAlgorithmParameters.length; i++)
      lastLocSpotDetectionAlgorithmParameters[i] = locSpotDetectionAlgorithmParameters[i];
  }
  public TLocalAlignmentBlockAlgorithm() {
    super();
    parameters = new Object[11];
    TGridBlockAlignmentAlgorithmFactory factory = new TGridBlockAlignmentAlgorithmFactory();
    setParameter(GRID_BLOCK_ALIGNMENT_ALGORITHM, factory.createAlgorithm(TLevel1BlockAlignmentAlgorithm.NAME));
    initWithLast();
    controlPanel = TLocalAlignmentBlockAlgorithmPanel.getInstance(this);
    startProgress = 0;
    progressWidth = 100;
	setImagesMin(1);
	setImagesMax(1);
  }
  public String getName() {
    return NAME;
  }
  public void init() {
    setParameter(BOOTSTRAP_SENSITIVITY, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getBootstrapSensitivity()));
    setParameter(BOOTSTRAP_ITERATIONS, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getBootstrapIterations()));
    setParameter(FIRST_RUN_SENSITIVITY, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getFirstRunSensitivity()));
    setParameter(FIRST_RUN_ITERATIONS, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getFirstRunIterations()));
    setParameter(SECOND_RUN_SENSITIVITY, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getSecondRunSensitivity()));
    setParameter(SECOND_RUN_ITERATIONS, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getSecondRunIterations()));
    setParameter(THIRD_RUN_SENSITIVITY, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getThirdRunSensitivity()));
    setParameter(THIRD_RUN_ITERATIONS, new Integer(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getThirdRunIterations()));
    setParameter(SENSITIVITY_THRESHOLD, new Double(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getSensitivityThreshold()));
    setParameter(GRID_CHECK, new Boolean(((TLocalAlignmentBlockAlgorithmPanel)controlPanel).getGridCheck()));
    TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
    blockAlgorithm.init();
    setLasts();
  }
  public void initWithDefaults() {
    setParameter(BOOTSTRAP_SENSITIVITY, new Integer(bootstrapSensitivity));
    setParameter(BOOTSTRAP_ITERATIONS, new Integer(bootstrapIterations));
    setParameter(FIRST_RUN_SENSITIVITY, new Integer(firstRunSensitivity));
    setParameter(FIRST_RUN_ITERATIONS, new Integer(firstRunIterations));
    setParameter(SECOND_RUN_SENSITIVITY, new Integer(secondRunSensitivity));
    setParameter(SECOND_RUN_ITERATIONS, new Integer(secondRunIterations));
    setParameter(THIRD_RUN_SENSITIVITY, new Integer(thirdRunSensitivity));
    setParameter(THIRD_RUN_ITERATIONS, new Integer(thirdRunIterations));
    setParameter(SENSITIVITY_THRESHOLD, new Double(sensitivityThreshold));
    Object[] params = new Object[4];
    params[0] = new Double(locT);
    params[1] = new Double(locC);
    params[2] = locSpotDetectionAlgorithmName;
    params[3] = locSpotDetectionAlgorithmParameters;
    TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
    blockAlgorithm.init(params);
  }
  public void init(Object[] params) {
    setParameter(BOOTSTRAP_SENSITIVITY, params[0]);
    setParameter(BOOTSTRAP_ITERATIONS, params[1]);
    setParameter(FIRST_RUN_SENSITIVITY, params[2]);
    setParameter(FIRST_RUN_ITERATIONS, params[3]);
    setParameter(SECOND_RUN_SENSITIVITY, params[4]);
    setParameter(SECOND_RUN_ITERATIONS, params[5]);
    setParameter(THIRD_RUN_SENSITIVITY, params[6]);
    setParameter(THIRD_RUN_ITERATIONS, params[7]);
    setParameter(SENSITIVITY_THRESHOLD, params[8]);
    setParameter(GRID_CHECK, params[9]);
    TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
    Object[] params2 = new Object[4];
    params2[0] = ((Object[])params[10])[0];
    params2[1] = ((Object[])params[10])[1];
    params2[2] = ((Object[])params[10])[2];
    params2[3] = ((Object[])params[10])[3];
    blockAlgorithm.init(params2);
  }
  
  //ajout integration 13/09/05
  public String getStringParams() {
    String s = "";
    s = getParameter(BOOTSTRAP_SENSITIVITY).toString() + "|" + getParameter(BOOTSTRAP_ITERATIONS).toString() + "|" +
        getParameter(FIRST_RUN_SENSITIVITY).toString() + "|" + getParameter(FIRST_RUN_ITERATIONS).toString() + "|" +
        getParameter(SECOND_RUN_SENSITIVITY).toString() + "|" + getParameter(SECOND_RUN_ITERATIONS).toString() + "|" +
        getParameter(THIRD_RUN_SENSITIVITY).toString() + "|" + getParameter(THIRD_RUN_ITERATIONS).toString() + "|" +
        getParameter(SENSITIVITY_THRESHOLD).toString() + "|" + getParameter(GRID_CHECK).toString() + "|" +
        ((TLevel1BlockAlignmentAlgorithm)getParameter(GRID_BLOCK_ALIGNMENT_ALGORITHM)).getStringParams();
    return s;
  }
  public void setLasts() {
    lastBootstrapSensitivity = ((Integer)parameters[BOOTSTRAP_SENSITIVITY]).intValue();
    lastBootstrapIterations = ((Integer)parameters[BOOTSTRAP_ITERATIONS]).intValue();
    lastFirstRunSensitivity = ((Integer)parameters[FIRST_RUN_SENSITIVITY]).intValue();
    lastFirstRunIterations = ((Integer)parameters[FIRST_RUN_ITERATIONS]).intValue();
    lastSecondRunSensitivity = ((Integer)parameters[SECOND_RUN_SENSITIVITY]).intValue();
    lastSecondRunIterations = ((Integer)parameters[SECOND_RUN_ITERATIONS]).intValue();
    lastThirdRunSensitivity = ((Integer)parameters[THIRD_RUN_SENSITIVITY]).intValue();
    lastThirdRunIterations = ((Integer)parameters[THIRD_RUN_ITERATIONS]).intValue();
    lastSensitivityThreshold = ((Double)parameters[SENSITIVITY_THRESHOLD]).doubleValue();
    lastGridCheck = ((Boolean)parameters[GRID_CHECK]).booleanValue();

    TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
    lastLocT = ((Double)blockAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.T)).doubleValue();
    lastLocC = ((Double)blockAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.C)).doubleValue();

    TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)blockAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
    lastLocSpotDetectionAlgorithmName = spotDetectionAlgorithm.getName();
    for (int i = 0; i < spotDetectionAlgorithm.getParameters().length; i++) {
      lastLocSpotDetectionAlgorithmParameters[i] = spotDetectionAlgorithm.getParameters()[i];
    }
  }
  public void initWithLast() {
    parameters[BOOTSTRAP_SENSITIVITY] = new Integer(lastBootstrapSensitivity);
    parameters[BOOTSTRAP_ITERATIONS] = new Integer(lastBootstrapIterations);
    parameters[FIRST_RUN_SENSITIVITY] = new Integer(lastFirstRunSensitivity);
    parameters[FIRST_RUN_ITERATIONS] = new Integer(lastFirstRunIterations);
    parameters[SECOND_RUN_SENSITIVITY] = new Integer(lastSecondRunSensitivity);
    parameters[SECOND_RUN_ITERATIONS] = new Integer(lastSecondRunIterations);
    parameters[THIRD_RUN_SENSITIVITY] = new Integer(lastThirdRunSensitivity);
    parameters[THIRD_RUN_ITERATIONS] = new Integer(lastThirdRunIterations);
    parameters[SENSITIVITY_THRESHOLD] = new Double(lastSensitivityThreshold);
    parameters[GRID_CHECK] = new Boolean(lastGridCheck);
    TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
    Object[] params = new Object[4];
    params[0] = new Double(lastLocT);
    params[1] = new Double(lastLocC);
    params[2] = lastLocSpotDetectionAlgorithmName;
    params[3] = lastLocSpotDetectionAlgorithmParameters;
    blockAlgorithm.init(params);
  }
  
  public void saveInProperties(){
	  // remi add 17/08/05
	  	// on sauve dans les properties si different
	  	// cette methode est appele quand on choisi "apply"
	  	int panelIntValue;
	  	double panelDoubleValue;
	  	  	
	  //	TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
	  	// bootstrapSensitivity
	  	panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
	 
	 
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.bootstrapSensitivity")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.bootstrapSensitivity",panelIntValue); 	
	  	}
	  	// bootstrapIterations
	  	panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.bootstrapIterations")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.bootstrapIterations",panelIntValue); 	
	  	}
	  	// firstRunSensitivity 
	  	panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.firstRunSensitivity")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.firstRunSensitivity",panelIntValue); 	
	  	}
	  	// firstRunIterations 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.firstRunIterations")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.firstRunIterations",panelIntValue); 	
	  	}
	  	// secondRunSensitivity 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.secondRunSensitivity")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.secondRunSensitivity",panelIntValue); 	
	  	}
	 	// secondRunIterations 
		panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.secondRunIterations")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.secondRunIterations",panelIntValue); 	
	  	}
	  	// thirdRunSensitivity 
	  	panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.thirdRunSensitivity")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.thirdRunSensitivity",panelIntValue); 	
	  	}
	  	// thirdRunIterations
	  	panelIntValue = ((Integer)getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
	  	if (panelIntValue != AGScan.prop.getIntProperty("TLocalAlignmentBlockAlgorithm.thirdRunIterations")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.thirdRunIterations",panelIntValue); 	
	  	}
	  	// sensitivityThreshold 
		panelDoubleValue = ((Double)getParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
	  	if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.sensitivityThreshold")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.sensitivityThreshold",panelDoubleValue); 	
	  	}
	  	
	  	TLevel1BlockAlignmentAlgorithm gridBlockAlignmentAlgorithm = (TLevel1BlockAlignmentAlgorithm)getParameter(TLocalAlignmentBlockAlgorithm.GRID_BLOCK_ALIGNMENT_ALGORITHM);
	  	// locT 
	  	panelDoubleValue = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.T)).doubleValue();
	  	if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.locT")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.locT",panelDoubleValue); 	
	  	}
	  	// locC
		panelDoubleValue = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.C)).doubleValue();
	  	if (panelDoubleValue != AGScan.prop.getDoubleProperty("TLocalAlignmentBlockAlgorithm.locC")){
	  		AGScan.prop.setProperty("TLocalAlignmentBlockAlgorithm.locC",panelDoubleValue); 	
	  	}
	  	
	  	TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
	  	// pour maxSearchZone & maxComputeZone & qualityThreshold : appel interne...
	  	spotDetectionAlgorithm.saveInProperties();
	  	 			
	  }
  
  /*
   * ces 3 methodes ont etes supprimes le 13/09/05 - integration
  public void setProgressionValues(double sp, double pw) {
    startProgress = sp;
    progressWidth = pw;
  }
  public double getStartProgress() {
    return startProgress;
  }
  public double getProgressionWidth() {
    return progressWidth;
  }
  */
  public void execute(boolean thread) {
    worker = new TLocalAlignmentBlockWorker(this);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      //worker.finished();
    }
  }
  public void execute(boolean thread, int priority) {
    worker = new TLocalAlignmentBlockWorker(this);
    setPriority(priority);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }
  public TGridBlockAlignmentAlgorithm getBlockAlgorithm() {
    return (TGridBlockAlignmentAlgorithm)parameters[GRID_BLOCK_ALIGNMENT_ALGORITHM];
  }
  public void actionPerformed(ActionEvent event) {
    int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TLocalAlignmentBlockAlgorithm.1"), //$NON-NLS-1$
                                            Messages.getString("TLocalAlignmentBlockAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
    if (rep == JOptionPane.YES_OPTION) {
      worker.stop();
    }
  }
  public void stateChanged(ChangeEvent e) {
    int val = ((JSlider)e.getSource()).getValue();
    switch (val) {
      case 0 :
        worker.setPriority(Thread.MIN_PRIORITY);
        break;
      case 1 :
        worker.setPriority(Thread.NORM_PRIORITY);
        break;
      case 2 :
        worker.setPriority(Thread.MAX_PRIORITY);
        break;
    }
  }
  public int getDefaultBootstrapSensitivity() {
    return bootstrapSensitivity;
  }
  public int getDefaultFirstRunSensitivity() {
    return firstRunSensitivity;
  }
  public int getDefaultSecondRunSensitivity() {
    return secondRunSensitivity;
  }
  public int getDefaultThirdRunSensitivity() {
    return thirdRunSensitivity;
  }
  public int getDefaultBootstrapIterations() {
    return bootstrapIterations;
  }
  public int getDefaultFirstRunIterations() {
    return firstRunIterations;
  }
  public int getDefaultSecondRunIterations() {
    return secondRunIterations;
  }
  public int getDefaultThirdRunIterations() {
    return thirdRunIterations;
  }
  public double getDefaultSensitivityThreshold() {
      return sensitivityThreshold;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
