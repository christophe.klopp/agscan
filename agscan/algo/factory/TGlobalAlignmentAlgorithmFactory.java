package agscan.algo.factory;

import agscan.algo.TAlgorithm;
import agscan.algo.globalalignment.TEdgesFinderGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TTemplateMatchingGlobalAlignmentAlgorithm;

public class TGlobalAlignmentAlgorithmFactory implements TAlgorithmFactory {
  public TGlobalAlignmentAlgorithmFactory() {
  }
  public TAlgorithm createAlgorithm(Object param) {
    String algoName = (String)param;
    if (algoName.equals(TEdgesFinderGlobalAlignmentAlgorithm.NAME)) {
      return new TEdgesFinderGlobalAlignmentAlgorithm();
    }
    else if (algoName.equals(TSnifferGlobalAlignmentAlgorithm.NAME)) {
      return new TSnifferGlobalAlignmentAlgorithm();
    }
    else if (algoName.equals(TTemplateMatchingGlobalAlignmentAlgorithm.NAME)) {
        return new TTemplateMatchingGlobalAlignmentAlgorithm();
      }
    return null;
  }
  public static int getID(Object param) {
    String algoName = (String)param;
    if (algoName.equals(TEdgesFinderGlobalAlignmentAlgorithm.NAME)) {
      return 1;
    }
    else if (algoName.equals(TSnifferGlobalAlignmentAlgorithm.NAME)) {
      return 2;
    }
    else if (algoName.equals(TTemplateMatchingGlobalAlignmentAlgorithm.NAME)) {
        return 3;
      }
    return 0;
  }
  public static String getAlgoName(int id) {
    if (id == 1) {
      return TEdgesFinderGlobalAlignmentAlgorithm.NAME;
    }
    else if (id == 2) {
      return TSnifferGlobalAlignmentAlgorithm.NAME;
    }
    else if (id == 3) {
        return TTemplateMatchingGlobalAlignmentAlgorithm.NAME;
      }
    return "";
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
