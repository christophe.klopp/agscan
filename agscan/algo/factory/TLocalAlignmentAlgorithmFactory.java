package agscan.algo.factory;

import agscan.algo.TAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;

public class TLocalAlignmentAlgorithmFactory implements TAlgorithmFactory {
  public TLocalAlignmentAlgorithmFactory() {
  }
  public TAlgorithm createAlgorithm(Object param) {
    String algoName = (String)param;
    if (algoName.equals(TLocalAlignmentBlockAlgorithm.NAME)) {
      return new TLocalAlignmentBlockAlgorithm();
    }
    else if (algoName.equals(TLocalAlignmentGridAlgorithm.NAME)) {
      return new TLocalAlignmentGridAlgorithm();
    }
    else if (algoName.equals(TLocalAlignmentBlockTemplateAlgorithm.NAME)) {
        return new TLocalAlignmentBlockTemplateAlgorithm();
    }
    return null;
  }
  public static int getID(Object param) {
    String algoName = (String)param;
    if (algoName.equals(TLocalAlignmentBlockTemplateAlgorithm.NAME))
      return 0;
    else if (algoName.equals(TLocalAlignmentGridAlgorithm.NAME)) {
      return 1;
    }
    else if (algoName.equals(TLocalAlignmentBlockAlgorithm.NAME)) {
      return 2;
    }
    return -1;
  }
  public static String getAlgoName(int id) {
    if (id == 0) {
      return TLocalAlignmentBlockTemplateAlgorithm.NAME;
    }
	else if (id == 1) {
      return TLocalAlignmentGridAlgorithm.NAME;
    }
    else if (id == 2) {
      return TLocalAlignmentBlockAlgorithm.NAME;
    }
    return "";
  }

}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
