package agscan.algo.globalalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.AGScan;

public class TTemplateMatchingGlobalAlignmentAlgorithm extends TGlobalAlignmentAlgorithm {
	public static final int SEARCH_ANGLE = 0;
	public static final int STD_DEV_H = 1; // nombre d'ecart type max accepté pour rassembler 2 spots ensemble
	public static final int STD_DEV_V = 2; // nombre d'ecart type min accepté pour rassembler 2 spots ensemble
	public static final int PERCENT_COL = 3;//pourcentage du nombre de spot par colonne  pour que l'agrandissement horizontale soit validé
	public static final int PERCENT_ROW = 4;//pourcentage du nombre de spot par ligne  pour que l'agrandissement vertticale soit validé
	public static final int MAX_RATIO_H = 5;// limite max du rapport moyenne/ecart-type pour le traitement horizontal
	public static final int MAX_RATIO_V = 6;// limite max du rapport moyenne/ecart-type pour le traitement vertical
	public static final int MAX_AREA_SIZE = 7;// borne max de l'aire acceptée dans l'analyseur
	public static final int MIN_AREA_SIZE = 8;// borne min de l'aire acceptée dans l'analyseur
	public static final int KIND_KERNEL = 9;//type de kernel choisi par l'utilisateur
	public static final int ACTI_SIZE = 10;//activation de la correction de la taille de la grille
	

	public double startProgress, progressWidth;
	private static int searchAngle = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.searchAngle");
	private static int stddevh = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevh");
	private static int stddevv = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevv");
	private static int percentcol = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentcol");
	private static int percentrow = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentrow");
	private static int maxratioh = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratioh");
	private static int maxratiov = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratiov");
	private static int MaxAreaSize = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MaxAreaSize");
	private static int MinAreaSize = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MinAreaSize");
	private static int KindKernel = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.KindKernel");
	private static int ActiSize  = AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.ActiSize");
	
	public static int lastSearchAngle,lastMaxAreaSize,lastMinAreaSize,lastKindKernel /*lastGridCenterCross, lastSpotCicularity, lastKernelSize, lastMinSpotSize, lastMaxSpotSize*/;
	public static int laststddevh, laststddevv,lastpercentcol,lastpercentrow,lastmaxratioh,lastmaxratiov,lastActiSize;
	
	static {
		lastSearchAngle = searchAngle;
		laststddevh = stddevh;
		laststddevv = stddevv;
		lastpercentcol = percentcol;
		lastpercentrow = percentrow;
		lastmaxratioh = maxratioh;
		lastmaxratiov = maxratiov;
		lastMaxAreaSize = MaxAreaSize;
		lastMinAreaSize = MinAreaSize;
		lastKindKernel = KindKernel;
		lastActiSize = ActiSize;
	}  
	
	public static final String NAME = "Gloabal Alignement by Template Matching"; //$NON-NLS-1$

	public TTemplateMatchingGlobalAlignmentAlgorithm() {
		super();
		parameters = new Object[11];
		
		//results = new Object[1];
		initWithLast();
		controlPanel = TTemplateMatchingGlobalAlignmentAlgorithmPanel.getInstance(this);
		startProgress = 0;
		progressWidth = 100;
	}
	public static String getName() {
		return NAME;
	}
	public String getStringParams() {
		String s = "";
		s = NAME;
		return s;
	}
	public void init() {
		setParameter(SEARCH_ANGLE, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle()));
		setParameter(STD_DEV_H, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getStandardDev_Hor()));
		setParameter(STD_DEV_V, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getStandardDev_Vert()));
		setParameter(PERCENT_COL, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getPercent_Col()));
		setParameter(PERCENT_ROW, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getPercent_Row()));
		setParameter(MAX_RATIO_H, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getRatioMax_Hor()));
		setParameter(MAX_RATIO_V, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getRatioMax_Vert()));
		setParameter(MAX_AREA_SIZE, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getMaxAreaSize()));
		setParameter(MIN_AREA_SIZE, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getMinAreaSize()));
		setParameter(KIND_KERNEL, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getKindKernel()));
		setParameter(ACTI_SIZE, new Integer(((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getActSizeCorr()));
	}
	
	public void initWithDefaults() {
		
		setParameter(SEARCH_ANGLE, new Integer(searchAngle));
		setParameter(STD_DEV_H, new Integer(stddevh));
		setParameter(STD_DEV_V, new Integer(stddevv));
		setParameter(PERCENT_COL, new Integer(percentcol));
		setParameter(PERCENT_ROW, new Integer(percentrow));
		setParameter(MAX_RATIO_H, new Integer(maxratioh));
		setParameter(MAX_RATIO_V, new Integer(maxratiov));
		setParameter(MAX_AREA_SIZE, new Integer(MaxAreaSize));
		setParameter(MIN_AREA_SIZE, new Integer(MinAreaSize));
		setParameter(KIND_KERNEL, new Integer(KindKernel));
		setParameter(ACTI_SIZE, new Integer(ActiSize));
		
	}
	
	public void init(Object[] params) {
		setParameter(SEARCH_ANGLE, params[0]);
		setParameter(STD_DEV_H, params[1]);
		setParameter(STD_DEV_V, params[2]);
		setParameter(PERCENT_COL, params[3]);
		setParameter(PERCENT_ROW, params[4]);
		setParameter(MAX_RATIO_H, params[5]);
		setParameter(MAX_RATIO_V, params[6]);
		setParameter(MAX_AREA_SIZE, params[7]);
		setParameter(MIN_AREA_SIZE, params[8]);
		setParameter(KIND_KERNEL, params[9]);
		setParameter(ACTI_SIZE, params[10]);

	}
	
	public void setLasts() {
		lastSearchAngle = ((Integer)parameters[SEARCH_ANGLE]).intValue();
		laststddevh = ((Integer)parameters[STD_DEV_H]).intValue();
		laststddevv = ((Integer)parameters[STD_DEV_V]).intValue();
		lastpercentcol = ((Integer)parameters[PERCENT_COL]).intValue();
		lastpercentrow = ((Integer)parameters[PERCENT_ROW]).intValue();
		lastmaxratioh = ((Integer)parameters[MAX_RATIO_H]).intValue();
		lastmaxratiov = ((Integer)parameters[MAX_RATIO_V]).intValue();
		lastMaxAreaSize = ((Integer)parameters[MAX_AREA_SIZE]).intValue();
		lastMinAreaSize = ((Integer)parameters[MIN_AREA_SIZE]).intValue();
		lastKindKernel = ((Integer)parameters[KIND_KERNEL]).intValue();
		lastActiSize = ((Integer)parameters[ACTI_SIZE]).intValue();
	}
	
	public void initWithLast() {
		setParameter(SEARCH_ANGLE, new Integer(lastSearchAngle));
		setParameter(STD_DEV_H, new Integer(laststddevh));
		setParameter(STD_DEV_V, new Integer(laststddevv));
		setParameter(PERCENT_COL, new Integer(lastpercentcol));
		setParameter(PERCENT_ROW, new Integer(lastpercentrow));
		setParameter(MAX_RATIO_H, new Integer(lastmaxratioh));
		setParameter(MAX_RATIO_V, new Integer(lastmaxratiov));
		setParameter(MAX_AREA_SIZE, new Integer(lastMaxAreaSize));
		setParameter(MIN_AREA_SIZE, new Integer(lastMinAreaSize));
		setParameter(KIND_KERNEL, new Integer(lastKindKernel));
		setParameter(ACTI_SIZE, new Integer(lastActiSize));
		
	}
	
	/*
	 * method added 2005/08/17
	 * if values are changed, we save them into the properties file
	 * @see agscan.algo.globalalignment.TGlobalAlignmentAlgorithm#saveInProperties()
	 */
	public void saveInProperties(){
		int panelValue;
	  	// qualityThreshold
	  	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle();
	  	// searchAngle
	  	if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.searchAngle")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.searchAngle",panelValue); 	
	  	}
		// stdDevH
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getStandardDev_Hor();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevh")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevh",panelValue); 	
	  	}
		// stdDevV
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getStandardDev_Vert();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevv")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.stddevv",panelValue); 	
	  	}
		// percentColparameters
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getPercent_Col();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentcol")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentcol",panelValue); 	
	  	}
		// percentRow
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getPercent_Col();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentrow")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.percentrow",panelValue); 	
	  	}
		// MaxRatioH
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getRatioMax_Hor();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratioh")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratioh",panelValue); 	
	  	}
		// MaxRatioV
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getRatioMax_Vert();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratiov")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.maxratiov",panelValue); 	
	  	}
		
		panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getMaxAreaSize();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MaxAreaSize")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MaxAreaSize",panelValue); 	
	  	}
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getMinAreaSize();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MinAreaSize")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.MinAreaSize",panelValue); 	
	  	}
	 	panelValue = ((TTemplateMatchingGlobalAlignmentAlgorithmPanel)controlPanel).getKindKernel();
		if (panelValue != AGScan.prop.getIntProperty("TTemplateMatchingGlobalAlignmentAlgorithm.KindKernel")){
	  		AGScan.prop.setProperty("TTemplateMatchingGlobalAlignmentAlgorithm.KindKernel",panelValue); 
		}

	}

	
	public void execute(boolean thread) {
		worker = new TTemplateMatchingGlobalAlignmentWorker(this);
		worker.setPriority(Thread.MIN_PRIORITY);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}
	
	public void execute(boolean thread, int priority) {
		worker = new TTemplateMatchingGlobalAlignmentWorker(this);
		worker.setPriority(priority);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}
	
	
	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TTemplateMatchingGlobalAlignmentAlgorithm.1"), //$NON-NLS-1$
				Messages.getString("TTemplateMatchingGlobalAlignmentAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (rep == JOptionPane.YES_OPTION) {
			worker.stop();
		}
	}
	
	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			worker.setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			worker.setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			worker.setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}
	
	public int getDefaultSearchAngle() {
		return searchAngle;
	}

	
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
