package agscan.algo.globalalignment;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm;

public class TTemplateMatchingGlobalAlignmentAlgorithmPanel extends TAlgorithmControlPanel implements ActionListener, ChangeListener  {
	private static TTemplateMatchingGlobalAlignmentAlgorithmPanel instance = null;
	private TAlgorithmControlPanel spotDetectionControlPanel;
	private TTemplateMatchingGlobalAlignmentAlgorithm alg;
	private JTextField showSearchAngleTextField/*, showGridCenterCrossTextField, showSpotCicularityTextField, kernelsizeTextField, pixelMinSpotSizeTextField, pixelMaxSpotSizeTextField*/;
	private JTabbedPane jtp;
	private JLabel lb1,lb2,lb3,lb4,lb5,lb6,lb7,lb8,lb9,lb10,lb11;
	private JSlider sl1h_1, sl1h_2,sl1v_1,sl1v_2,sl4_1,sl4_2;
	private JTextField e_sl1h_1, e_sl1h_2,e_sl1v_1,e_sl1v_2,r_h,r_v,tf4_1,tf4_2;
	private JRadioButton gauss,con,circ;
	private JCheckBox sizeCorrAct;
	
	protected TTemplateMatchingGlobalAlignmentAlgorithmPanel(TTemplateMatchingGlobalAlignmentAlgorithm algo) {
		
		//this.setSize(700,700);
		//this.setLocation(200,200);

		
		e_sl1h_1 = etiquette(false);
		e_sl1h_2 = etiquette(false);
		r_h = etiquette(true);
		e_sl1v_1 = etiquette(false);
		e_sl1v_2 = etiquette(false);
		r_v = etiquette(true);
		
		
		
		jtp = new JTabbedPane(JTabbedPane.TOP);
		jtp.addTab(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.20"),onglet1());
		jtp.addTab(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.8"),onglet2());
		
		jbInit();
		alg = algo;
		
	}
	private void jbInit() {	
		this.removeAll();
		this.add(jtp);
	}
	

//	paramètre du traitement d'image
	public JPanel onglet1(){
		JPanel res = new JPanel();
		res.removeAll();
		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints constraint = new GridBagConstraints();
		res.setLayout(gb);
		
		Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.6")); //$NON-NLS-1$
		res.setBorder(titledBorder1);
		
		
		//label
		buildConstraints(constraint, 0, 0, 1, 1, 40, 20);
		lb9 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.14"));
		gb.setConstraints(lb9, constraint);
		res.add(lb9);
		
		buildConstraints(constraint, 0, 1, 1, 1, 0, 20);
		lb10 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.15"));
		gb.setConstraints(lb10, constraint);
		res.add(lb10);
		
		buildConstraints(constraint, 0, 2, 1, 1, 0, 20);
		JLabel showSearchAngleLabel = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.0")); //$NON-NLS-1$
		gb.setConstraints(showSearchAngleLabel, constraint);
		res.add(showSearchAngleLabel);
		
		buildConstraints(constraint, 0, 2, 1, 1, 0, 20);
		
		
		buildConstraints(constraint, 0, 3, 3, 1, 0, 10);
		lb11 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.16"));
		gb.setConstraints(lb11, constraint);
		res.add(lb11);
		
		
		//etiquette
		buildConstraints(constraint, 1, 0, 1, 1, 20, 0);
		tf4_1 = etiquette(false);
		gb.setConstraints(tf4_1, constraint);
		res.add(tf4_1);
		
		buildConstraints(constraint, 1, 1, 1, 1, 0, 0);
		tf4_2 = etiquette(false);
		gb.setConstraints(tf4_2, constraint);
		res.add(tf4_2);
		
		buildConstraints(constraint, 1, 2, 1, 1, 0, 20);
		showSearchAngleTextField = new JTextField(4);
		gb.setConstraints(showSearchAngleTextField, constraint);
		res.add(showSearchAngleTextField);
		
		//curseur
		buildConstraints(constraint, 2, 0, 1, 1, 40, 0);
		sl4_1 =curseur(0, 200,50.0D,10,5);
		gb.setConstraints(sl4_1, constraint);
		res.add(sl4_1);
		
		buildConstraints(constraint, 2, 1, 1, 1, 0, 0);
		sl4_2 =curseur(0, 200,150.0D,10,5);
		gb.setConstraints(sl4_2, constraint);
		res.add(sl4_2);
		
		//bouton radio
		constraint.anchor = GridBagConstraints.WEST;
		
		
		buildConstraints(constraint, 0, 4, 3, 1, 0, 10);
		gauss = new JRadioButton(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.17"));
		gb.setConstraints(gauss, constraint);
		res.add(gauss);
		
		buildConstraints(constraint, 0, 5, 3, 1, 0, 10);
		circ = new JRadioButton(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.18"));
		gb.setConstraints(circ, constraint);
		res.add(circ);
		
		buildConstraints(constraint, 0, 6, 3, 1, 0, 10);
		con = new JRadioButton(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.19"));
		gb.setConstraints(con, constraint);
		res.add(con);
		
		constraint.anchor = GridBagConstraints.WEST;
		
		ButtonGroup gr = new ButtonGroup();
		gr.add(gauss);
		gr.add(circ);
		gr.add(con);
		circ.setSelected(true);
		repaint();
		return res;
	}
	
	//onglet contenant les param du resize
	public JPanel onglet2(){
		JPanel pane = new JPanel();
		pane.removeAll();
		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints constraint = new GridBagConstraints();
		pane.setLayout(gb);
		
		Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.6")); //$NON-NLS-1$
		pane.setBorder(titledBorder1);
		
		buildConstraints(constraint, 0, 0, 3, 1, 40, 20);
		sizeCorrAct = new JCheckBox(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.21"),false);
		sizeCorrAct.addActionListener(this);
		gb.setConstraints(sizeCorrAct, constraint);
		pane.add(sizeCorrAct);
		
		
		
		//label
		buildConstraints(constraint, 0, 1, 3, 1, 40, 10);
		lb1 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.9"));
		gb.setConstraints(lb1, constraint);
		pane.add(lb1);
		
		buildConstraints(constraint, 0, 2, 1, 1, 0, 10);
		lb2 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.10"));
		gb.setConstraints(lb2, constraint);
		pane.add(lb2);
		
		buildConstraints(constraint, 0, 3, 1, 1, 0, 10);
		lb3 =  new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.11"));
		gb.setConstraints(lb3, constraint);
		pane.add(lb3);

		buildConstraints(constraint, 0,4, 2, 1, 0, 10);
		lb4 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.12"));
		gb.setConstraints(lb4, constraint);
		pane.add(lb4);
		
		buildConstraints(constraint, 0,5, 3, 1, 0, 10);
		lb5 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.13"));
		gb.setConstraints(lb5, constraint);
		pane.add(lb5);
		
		buildConstraints(constraint, 0, 6, 1, 1, 0, 10);
		lb6 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.10"));
		gb.setConstraints(lb6, constraint);
		pane.add(lb6);
		
		buildConstraints(constraint, 0, 7, 1, 1, 0, 10);
		lb7 =  new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.11"));
		gb.setConstraints(lb7, constraint);
		pane.add(lb7);

		buildConstraints(constraint, 0, 8, 2, 1, 0, 10);
		lb8 = new JLabel(Messages.getString("TGlobalAlignmentTemplateMatchingAlgorithmPanel.12"));
		gb.setConstraints(lb8, constraint);
		pane.add(lb8);
		
		//zone de texte
		
		buildConstraints(constraint, 1, 2, 1, 1, 20, 0);
		gb.setConstraints(e_sl1h_1, constraint);
		pane.add(e_sl1h_1);
		
		buildConstraints(constraint, 1, 3, 1, 1, 20, 0);
		gb.setConstraints(e_sl1h_2, constraint);
		pane.add(e_sl1h_2);
		
		buildConstraints(constraint, 1, 6, 1, 1, 20, 0);
		gb.setConstraints(e_sl1v_1, constraint);
		pane.add(e_sl1v_1);


		buildConstraints(constraint, 1, 7, 1, 1, 20, 0);
		gb.setConstraints(e_sl1v_2, constraint);
		pane.add(e_sl1v_2);
		
		buildConstraints(constraint, 2, 4, 1, 1, 40, 0);
		gb.setConstraints(r_h, constraint);
		pane.add(r_h);


		buildConstraints(constraint, 2, 8, 1, 1, 0, 0);
		gb.setConstraints(r_v, constraint);
		pane.add(r_v);
		
		//slider
		sl1h_1 = curseur(0,200,0.0D,20,5);
		sl1h_2 = curseur(0,100,0.4*100,50,10);
		sl1v_1 = curseur(0,200,0.0D,20,5);
		sl1v_2 = curseur(0,100,0.4*100,50,10);
		
		buildConstraints(constraint, 2, 2, 1, 1, 0, 0);
		gb.setConstraints(sl1h_1, constraint);
		pane.add(sl1h_1);
		
		buildConstraints(constraint, 2, 3, 1, 1, 0, 0);
		gb.setConstraints(sl1h_2, constraint);
		pane.add(sl1h_2);
		
		buildConstraints(constraint, 2, 6, 1, 1, 0, 0);
		gb.setConstraints(sl1v_1, constraint);
		pane.add(sl1v_1);

		buildConstraints(constraint, 2, 7, 1, 1, 0, 0);
		gb.setConstraints(sl1v_2, constraint);
		pane.add(sl1v_2);
		
		//constraint.fill = GridBagConstraints.BOTH;
		
			e_sl1h_1.setEnabled(false);
			e_sl1h_2.setEnabled(false);
			r_h.setEnabled(false);
			e_sl1v_1.setEnabled(false);
			e_sl1v_2.setEnabled(false);
			r_v.setEnabled(false);
			sl1h_1.setEnabled(false);
			sl1h_2.setEnabled(false);
			sl1v_1.setEnabled(false);
			sl1v_2.setEnabled(false);
			lb1.setEnabled(false);
			lb2.setEnabled(false);
			lb3.setEnabled(false);
			lb4.setEnabled(false);
			lb5.setEnabled(false);
			lb6.setEnabled(false);
			lb7.setEnabled(false);
			lb8.setEnabled(false);
		
		
		
		
		return pane;
	}
	
	
	public void buildConstraints(GridBagConstraints gbc, int gx, int gy, int gw, int gh, int wx,int wy){
		gbc.gridx = gx;
		gbc.gridy = gy;
		gbc.gridwidth = gw;
		gbc.gridheight = gh;
		gbc.weightx = wx;
		gbc.weighty = wy;
	}
	
	//cree un curseur numerique
	private JSlider curseur(int deb, int fin,double init,int maj,int min){
		JSlider curs = new JSlider(SwingConstants.HORIZONTAL,deb,fin,(int)init);
		curs.setMajorTickSpacing(maj);
		curs.setMinorTickSpacing(min);
		curs.setPaintLabels(false);
		curs.setPaintTicks(true);
		
		curs.addChangeListener(this);
		return curs;
	}
	
	
	//crée une etiquette
	private JTextField etiquette (boolean b){
		JTextField res = new JTextField(4);
		res.setHorizontalAlignment(SwingConstants.CENTER);
		res.setEditable(b);
		return res;
	}
	
	public void actionPerformed(ActionEvent event) {
		showSearchAngleTextField = new JTextField();
		
		
		Object source = event.getSource();
		if(source == sizeCorrAct){
			if(sizeCorrAct.isSelected()){
				e_sl1h_1.setEnabled(true);
				e_sl1h_2.setEnabled(true);
				r_h.setEnabled(true);
				e_sl1v_1.setEnabled(true);
				e_sl1v_2.setEnabled(true);
				r_v.setEnabled(true);
				sl1h_1.setEnabled(true);
				sl1h_2.setEnabled(true);
				sl1v_1.setEnabled(true);
				sl1v_2.setEnabled(true);
				lb1.setEnabled(true);
				lb2.setEnabled(true);
				lb3.setEnabled(true);
				lb4.setEnabled(true);
				lb5.setEnabled(true);
				lb6.setEnabled(true);
				lb7.setEnabled(true);
				lb8.setEnabled(true);
			}
			else{
				e_sl1h_1.setEnabled(false);
				e_sl1h_2.setEnabled(false);
				r_h.setEnabled(false);
				e_sl1v_1.setEnabled(false);
				e_sl1v_2.setEnabled(false);
				r_v.setEnabled(false);
				sl1h_1.setEnabled(false);
				sl1h_2.setEnabled(false);
				sl1v_1.setEnabled(false);
				sl1v_2.setEnabled(false);
				lb1.setEnabled(false);
				lb2.setEnabled(false);
				lb3.setEnabled(false);
				lb4.setEnabled(false);
				lb5.setEnabled(false);
				lb6.setEnabled(false);
				lb7.setEnabled(false);
				lb8.setEnabled(false);
			}
			
			
			
		}
		
		
		jbInit(); /*   jbInit();*/
		//init(alg);
		doLayout();
		repaint();
	}
	 
	public void stateChanged(ChangeEvent event){
			Object source = event.getSource();
			if(source == sl1h_1){
				int newValue = sl1h_1.getValue();
				e_sl1h_1.setText(""+newValue);
				
			}
			if(source == sl1v_1){
				int newValue = sl1v_1.getValue();
				e_sl1v_1.setText(""+newValue);
				
			}
			if(source == sl1h_2){
				int newValue = sl1h_2.getValue();
				e_sl1h_2.setText(""+newValue);
				
			}
			if(source == sl1v_2){
				int newValue = sl1v_2.getValue();
				e_sl1v_2.setText(""+newValue);
				
			}
			if(source == sl4_1){
				int newValue = sl4_1.getValue();
				tf4_1.setText(""+newValue);
				
			}
			if(source == sl4_2){
				int newValue = sl4_2.getValue();
				tf4_2.setText(""+newValue);
				
			}
			repaint();
	}
	 
	public void init(TAlgorithm algo) {
		alg = (TTemplateMatchingGlobalAlignmentAlgorithm)algo;
		TTemplateMatchingGlobalAlignmentAlgorithm alg = (TTemplateMatchingGlobalAlignmentAlgorithm)algo;
		showSearchAngleTextField.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.SEARCH_ANGLE).toString());
		e_sl1h_1.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.STD_DEV_H).toString());
		e_sl1v_1.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.STD_DEV_V).toString());
		e_sl1h_2.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.PERCENT_COL).toString());
		e_sl1v_2.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.PERCENT_ROW).toString());
		r_h.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MAX_RATIO_H).toString());
		r_v.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MAX_RATIO_V).toString());
		
		
		sl1h_1.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.STD_DEV_H));
		sl1h_2.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.STD_DEV_V));
		sl1v_1.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.PERCENT_COL));
		sl1v_2.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.PERCENT_ROW));
	
		tf4_1.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MIN_AREA_SIZE).toString());
		tf4_2.setText(alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MAX_AREA_SIZE).toString());
		sl4_1.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MIN_AREA_SIZE));
		sl4_2.setValue((Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.MAX_AREA_SIZE));
		
		int kind = (Integer)alg.getParameter(TTemplateMatchingGlobalAlignmentAlgorithm.KIND_KERNEL);
		switch(kind){
			case 1 : gauss.setSelected(true);
					 break;
			case 2 : circ.setSelected(true);
					 break;
			case 3 : con.setSelected(true);
					 break;
			default : circ.setSelected(true);
			 		  break;
		}
	
	}

	public int getSearchAngle() {
		int ret = -1;
		try {
			System.out.println("la chaine qui me rend fou "+showSearchAngleTextField.getText());
			ret = Integer.parseInt(showSearchAngleTextField.getText());
		}
		catch (Exception ex) {
			System.out.println("on est ici");
			ex.printStackTrace();
		}
		return ret;
	}

	public int getStandardDev_Hor(){
		int ret = -1;
		try {
			ret = Integer.parseInt(e_sl1h_1.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public int getStandardDev_Vert(){
		int ret = -1;
		try {
			ret = Integer.parseInt(e_sl1v_1.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public int getRatioMax_Vert(){
		int ret = -1;
		try {
			ret = Integer.parseInt(r_v.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public int getRatioMax_Hor(){
		int ret = -1;
		try {
			ret = Integer.parseInt(r_h.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	
	public int getPercent_Col(){
		int ret = -1;
		try {
			ret = Integer.parseInt(e_sl1h_2.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public int getPercent_Row(){
		int ret = -1;
		try {
			ret = Integer.parseInt(e_sl1v_2.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public int getMaxAreaSize(){
		int ret = -1;
		try {
			ret = Integer.parseInt(tf4_2.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	public int getMinAreaSize(){
		int ret = -1;
		try {
			ret = Integer.parseInt(tf4_1.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		return ret;
	}
	
	//1 -> gaussian kernel
	//2 -> circular kernel
	//3 -> conic kernel
	
	public int getKindKernel(){
		int ret = -1;
		if(gauss.isSelected())
			ret = 1;
		else{
			if(circ.isSelected())
				ret = 2;
			else{
				if(con.isSelected())
					ret = 3;
			}
		}
		
		
		return ret;
	}
	
	//1->Activate
	//0->inactivate
	public int getActSizeCorr(){
		int ret = -1;
		if(sizeCorrAct.isSelected())
			ret = 1;
		else
			ret = 0;
		
		return ret;
	}
	
	
	
	public static TTemplateMatchingGlobalAlignmentAlgorithmPanel getInstance(TTemplateMatchingGlobalAlignmentAlgorithm algo) {
		if (instance == null)
			instance = new TTemplateMatchingGlobalAlignmentAlgorithmPanel(algo);
		else
			instance.jbInit();
		instance.init(algo);
		return instance;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
