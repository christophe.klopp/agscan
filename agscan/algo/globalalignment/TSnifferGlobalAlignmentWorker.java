package agscan.algo.globalalignment;

import java.awt.Color;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.localalignment.TLevel1BlockAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.data.TDataManager;
import agscan.data.controler.TAlignmentControler;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TUndoSnifferGlobalAlignment;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TAlignmentView;

/*
 * Doc remi 23/09/05
 *  How this algorithm works:
 * 
 * 				 3<==== 2====>4
 *				 5<====||====>6
 *				 7<====||====>8
 *			   	 9<====||====>10
 *			 21======== 1===========>22
 *				18<====||====>19
 *				16<====||====>17
 *				14<====||====>15
 * 				12<====11====>13
 * 
 * 1/ first block found by the sniffer : parcoursUDLR
 * 2-10/ top part of the image, above the first block: parcoursULR
 * 11-19/bottom part, behind the first block: parcoursDLR
 * 21/left of the block: parcoursL
 * 22/right of the block: parcoursR
 * 
 *
 */

public class TSnifferGlobalAlignmentWorker extends SwingWorker {
	private TSnifferGlobalAlignmentAlgorithm gaAlgo;
	private TUndoSnifferGlobalAlignment uefga;
	private TAlignmentModel alignmentModel;
	private int BLOCK_SIZE, BSM1S2, BSBSM1S2;
	private Color[] color = new Color[5];
	private double progressIncrement = 0;
	private int progressRange;
	
	public TSnifferGlobalAlignmentWorker(TGlobalAlignmentAlgorithm gaAlgo) {
		this.gaAlgo = (TSnifferGlobalAlignmentAlgorithm)gaAlgo;
		color[0] = Color.cyan;
		color[1] = Color.yellow;
		color[2] = Color.magenta;
		color[3] = Color.green;
		color[4] = Color.pink;
	}
	
	/**
	 * constructs the algorithm of sniffer global alignment. 
	 * @param  thread parameter not used here (comes from the herited class SwingWorker)
	 * @see SwingWorker.construct
	 */
	public Object construct(boolean thread) {
		String pathSeparator = System.getProperty("file.separator");
		double gHeight = 0;
		double gWidth = 0;
		double x0 = 0;
		double y0 = 0;
		pc_done = 0;
		alignmentModel = (TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL);
		TAlignment alignment = (TAlignment)alignmentModel.getReference();
		if (alignment.isInBatch())
			if (alignment.getBatch().showCurrentAlignment())
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.REFRESH_VIEW, alignment.getBatch()));
		alignment.getView().getTablePanel().setSelectable(false);
		TGrid grid = (TGrid)alignmentModel.getGridModel().getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		progressRange = gaAlgo.getEndProgress() - gaAlgo.getStartProgress();
		// Selection of the entire grid
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL, alignment));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
		
		if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning())))
			uefga = new TUndoSnifferGlobalAlignment();
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		alignment.setAlgoRunning(true);
		alignment.setAlgorithm(gaAlgo);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
				new Integer((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress())));
		aliView.getGraphicPanel().removeInteractors();
		TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null));
		
		int[] imageData = (int[])gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_DATA);
		int[] contrastedImageData = gaAlgo.makeImageSeuil();
		double l = 65535.0D - (double)((Integer)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.CONTRAST_PERCENT)).intValue() *
		(255 - ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN)).intValue()) * 257 / 100.0D;
		int w = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue();
		int h = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS)).intValue();
		int[] rastContrast, plotProf, plotDiff, plotProfInit;
		int dec, kmin = 0, step;
		rastContrast = new int[w * h];
		if (w >= h) {
			for (int i = 0; i < w; i++)
				for (int j = 0; j < h; j++)
					if ((~imageData[i + j * w] & 0xFFFF) >= l)
						rastContrast[i + j * w] = 65535;
					else
						rastContrast[i + j * w] = (~imageData[i + j * w] & 0xFFFF);
			plotProf = new int[w];
			plotDiff = new int[w];
			dec = (int)(Math.tan(((Integer)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE)).doubleValue() * Math.PI / 180.0D) * w);
			step = (int)(Math.tan(((Double)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE_INCREMENT)).doubleValue() * Math.PI / 180.0D) * w);
			if (step == 0) step = 1;
			kmin = gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					rastContrast, plotProf, plotDiff, w, h, -dec, dec, step, false);
			
			if (STOP) return null;
			
			kmin = gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					rastContrast, plotProf, plotDiff, w, h, kmin - 10, kmin + 10, 1, true);
			
			if (STOP) return null;
			
			plotProfInit = new int[w];
			gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					contrastedImageData, plotProfInit, plotDiff, w, h, kmin, kmin, 10, true);
			
			if (STOP) return null;
			
			gaAlgo.makeWindowMoy(plotProfInit, 40);
		}
		else {
			for (int i = 0; i < w; i++)
				for (int j = 0; j < h; j++)
					if ((~imageData[i + j * w] & 0xFFFF) >= l)
						rastContrast[(i + 1) * h - j - 1] = 65535;
					else
						rastContrast[(i + 1) * h - j - 1] = (~imageData[i + j * w] & 0xFFFF);
			plotProf = new int[h];
			plotDiff = new int[h];
			dec = (int)(Math.tan(((Integer)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE)).doubleValue() * Math.PI / 180.0D) * h);
			step = (int)(Math.tan(((Double)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE_INCREMENT)).doubleValue() * Math.PI / 180.0D) * h);
			if (step == 0) step = 1;
			kmin = gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					rastContrast, plotProf, plotDiff, h, w, -dec, dec, step, false);
			
			if (STOP) return null;
			
			kmin = gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					rastContrast, plotProf, plotDiff, h, w, kmin - 10, kmin + 10, 1, true);
			
			if (STOP) return null;
			
			plotProfInit = new int[w];
			plotDiff = new int[w];
			kmin = kmin * w / h;
			gaAlgo.makeHorizontalProfile(alignment.getPath() + pathSeparator + alignment.getName(),
					contrastedImageData, plotProfInit, plotDiff, w, h, kmin, kmin, 10, true);
			
			if (STOP) return null;
			
			gaAlgo.makeWindowMoy(plotProfInit, 40);
			
		}
		pc_done = 1000 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
				new Integer((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress())));
		
		double A = (double)kmin / (double)(w - 1);
		double angle = Math.atan(A);
		System.err.println("ANGLE = " + angle);
		// the angle is calculated
		BLOCK_SIZE = ((Integer)gaAlgo.getParameter(TSnifferGlobalAlignmentAlgorithm.SNIFFER_SIZE)).intValue();
		BSM1S2 = (BLOCK_SIZE - 1) / 2;
		BSBSM1S2 = (BLOCK_SIZE * BLOCK_SIZE - 1) / 2;
		
		if (alignment.isInBatch())
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TBatchControler.REFRESH_VIEW, alignment.getBatch()));
		
		
		TGridConfig gridConfig = new TGridConfig();//  = the configuration class of the little grid ( = the block) 
		gridConfig.setNbLevels(1);// that's a one level grid of course 
		gridConfig.setLevel1Params(BLOCK_SIZE, BLOCK_SIZE, grid.getGridModel().getConfig().getSpotsWidth(), grid.getGridModel().getConfig().getSpotsHeight(),
				grid.getGridModel().getConfig().getSpotsDiam(), TGridConfig.TOP_LEFT, TGridConfig.ID_NUMBER);
		TGrid newGrid = new TGrid(gridConfig);// little grid creation
		Enumeration enume = newGrid.getGridModel().getSpots().elements();
		TSpot spot;
		// update of the grid network
		int nbSpots = 0;
		while (enume.hasMoreElements()) {
			nbSpots++;
			spot = (TSpot)enume.nextElement();
			spot.setLeft(newGrid.getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent()));
			spot.setRight(newGrid.getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent()));
			spot.setTop(newGrid.getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent()));
			spot.setBottom(newGrid.getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent()));
			spot.setGlobalLeft(newGrid.getGridModel().getLeftSpot(spot, newGrid.getGridModel().getRootBlock()));
			spot.setGlobalRight(newGrid.getGridModel().getRightSpot(spot, newGrid.getGridModel().getRootBlock()));
			spot.setGlobalTop(newGrid.getGridModel().getTopSpot(spot, newGrid.getGridModel().getRootBlock()));
			spot.setGlobalBottom(newGrid.getGridModel().getBottomSpot(spot, newGrid.getGridModel().getRootBlock()));
		}
		//System.out.println("nbSpots=" +nbSpots);
		int dloc = aliView.getDividerLocation();
		alignmentModel.setGridModel(newGrid.getGridModel());
		aliView.setGridModel(newGrid.getGridModel());
		((TAlignmentControler)((TAlignment)alignmentModel.getReference()).getControler()).setGridControler(newGrid.getGridControler());
		
		aliView.setDividerLocation(dloc);
		
		newGrid.getGridControler().getGridSelectionControler().processEvent(
				new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, aliView.getReference()));// send a message to the DATA_MANAGER in order to select all the grid
		((TAlignmentControler)alignmentModel.getReference().getControler()).getGridPositionControler().processEvent(
				new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, aliView.getReference(), new Double(angle)));// send a message to the DATA_MANAGER to apply the angle to the grid
		//  we get the block algorithm of the global alignment ( =  the local alignment local of the global alignment)
		TLocalAlignmentBlockAlgorithm blockAlgorithm = gaAlgo.getLocalAlignmentBlockAlgorithm();
		blockAlgorithm.initWithDefaults();
		blockAlgorithm.initData(gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES),
				imageData, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS),
				newGrid.getGridModel(), aliView, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MAX));
		blockAlgorithm.init();
		//TODO NOTE that parameters for the little grid = snifffer block are defined here, cannot be choosed?
		blockAlgorithm.setParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS, new Integer(200));
		blockAlgorithm.setParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS, new Integer(200));
		blockAlgorithm.setParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS, new Integer(100));
		//  sensitivity threshold : here a little value in order to accept only a very beautiful spot...
		blockAlgorithm.setParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD, new Double(0.000000001));
		blockAlgorithm.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
				(int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress());
		if (alignment.isInBatch())
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TBatchControler.REFRESH_VIEW, alignment.getBatch()));//findInitSpot move the little grid to the 40% of the image (W and L)
			
		if (findInitSpot(blockAlgorithm, 0.4, 0.4) == 1) {//if a good spot is found = the little grid is placed
			blockAlgorithm.init();
			//the  compteUDLR function counts the number of positions of the little grid that will be used
			//in order to estimate the time needed to the generation of the network 
			int nbit = compteUDLR(angle, w, h, (int) ( ( (TGridModel) newGrid.getModel()).getRootBlock().getX() / xRes),
					(int) ( ( (TGridModel) newGrid.getModel()).getRootBlock().getY() / yRes));
			progressIncrement = (100.0D - pc_done) / (double) nbit;
			//the UpDownLeftRight function manages the route and the network of the little grid
			//spotTree IS the network
			TSpotTree spotTree = parcoursUDLR(blockAlgorithm);
			if (!STOP) {
				//at the end of the journey, links between spots are only horizontal.
				//That's why we need to generate vertical links
				spotTree.makeVerticalLinks(newGrid.getGridModel().getConfig().getSpotsHeight());
				spotTree.setLinesAndColumns();//give a number of row and column to each spot
				// the findEdges method gives the numbers of row and columnsat the left and right for every limit (bordures)
				//edge contains the 4 sides
				int[] edges = findEdges(spotTree, grid);
				Vector v = null;
				int nq = 0;
				angle = 0;
				for (int q = edges[2]; q <= edges[3]; q += 5) {
					//example if edge[2]=-20 and edge[3]=200, 221 rows are count
					// the for takes every 5 rows and compute the angle for each to mean them
					v = spotTree.getLine(q);
					if (v != null) {
						angle += getAngle(v);
						nq++;
					}
				}
				if (nq > 0) angle /= (double) nq;//mean
				System.err.println("ANGLE2 = " + angle);//angle2 is more precise 
				
				// part below consists of test suppressed since the 2005/12/13
				//TODO remove it when it seems ok
				/*
				 // here TESTS => addition of little crosses around of the found grid
				  v = spotTree.getColumn(edges[0]);
				  int X1, Y1, X2, Y2;
				  for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				  TSpotTree st = (TSpotTree) en.nextElement();
				  X1 = (int) (st.getX() / xRes) - 2;
				  Y1 = (int) (st.getY() / yRes);
				  X2 = (int) (st.getX() / xRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X2, Y1));
				  X1 = (int) (st.getX() / xRes);
				  Y1 = (int) (st.getY() / yRes) - 2;
				  Y2 = (int) (st.getY() / yRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X1, Y2));
				  }
				  v = spotTree.getColumn(edges[1]);
				  for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				  TSpotTree st = (TSpotTree) en.nextElement();
				  X1 = (int) (st.getX() / xRes) - 2;
				  Y1 = (int) (st.getY() / yRes);
				  X2 = (int) (st.getX() / xRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X2, Y1));
				  X1 = (int) (st.getX() / xRes);
				  Y1 = (int) (st.getY() / yRes) - 2;
				  Y2 = (int) (st.getY() / yRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X1, Y2));
				  }
				  
				  v = spotTree.getLine(edges[3]);
				  for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				  TSpotTree st = (TSpotTree) en.nextElement();
				  X1 = (int) (st.getX() / xRes) - 2;
				  Y1 = (int) (st.getY() / yRes);
				  X2 = (int) (st.getX() / xRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X2, Y1));
				  X1 = (int) (st.getX() / xRes);
				  Y1 = (int) (st.getY() / yRes) - 2;
				  Y2 = (int) (st.getY() / yRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X1, Y2));
				  }
				  
				  v = spotTree.getLine(edges[2]);
				  for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				  TSpotTree st = (TSpotTree) en.nextElement();
				  X1 = (int) (st.getX() / xRes) - 2;
				  Y1 = (int) (st.getY() / yRes);
				  X2 = (int) (st.getX() / xRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X2, Y1));
				  X1 = (int) (st.getX() / xRes);
				  Y1 = (int) (st.getY() / yRes) - 2;
				  Y2 = (int) (st.getY() / yRes) + 2;
				  ( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(
				  new TImageGraphicPanel.HotLine(Color.red, X1, Y1, X1, Y2));
				  }
				  //end of tests
				   */
				
				
				v = spotTree.getLine(edges[2]);//takes the first row
				// spots are getted and sorted in X croissant order
				Object[] vt = v.toArray();
				Arrays.sort(vt, new Comparator() {
					public int compare(Object o1, Object o2) {
						TSpotTree t1 = (TSpotTree) o1;
						TSpotTree t2 = (TSpotTree) o2;
						if (t1.getX() > t2.getX())
							return 1;
						else if (t1.getX() < t2.getX())
							return -1;
						return 0;
					}
					
					public boolean equals(Object obj) {
						return (obj == this);
					}
				});
				double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
				//in this loop, th e algo take each column and coordinates of the first and the last columns
				// detected by findedge (they are top left and right cornersof the found grid
				// aim of the loop:find the two corners of the network
				for (int i = 0; i < vt.length; i++) {
					if ( ( (TSpotTree) vt[i]).getColumn() == edges[1]) {
						x1 = ( (TSpotTree) vt[i]).getX();
						y1 = ( (TSpotTree) vt[i]).getY();
					}
					if ( ( (TSpotTree) vt[i]).getColumn() == edges[0]) {
						x2 = ( (TSpotTree) vt[i]).getX();
						y2 = ( (TSpotTree) vt[i]).getY();
					}
				}
				x0 = x2;
				y0 = y2;
				
				//calculation of the distance between the 2 selected top corners (= length of the estimated grid)
				gWidth = Math.abs(x1 - x2) / Math.cos(angle) + grid.getGridModel().getConfig().getSpotsWidth();
				
				
				//the width of grid is decreased with the interblocks values//  modification2005/05/17
				//largeur = largeur - (nb intercolonnes * nb de spot d'epaisseur de l'intercolonne* epaisseur d'un spot)
				//width = width - (intercolumn nb * nb of spots in the intercolumn* spot width) 
				gWidth = gWidth-(grid.getGridModel().getConfig().getLev2NbCols()-1)*grid.getGridModel().getConfig().getLev2XSpacing();//*grid.getGridModel().getConfig().getSpotsWidth();//
				( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(new TImageGraphicPanel.HotLine(
						Color.cyan, (int) (x0 / xRes), (int) (y0 / yRes), (int) ( (x0 + gWidth) / xRes), (int) (y1 / yRes)));
				( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(new TImageGraphicPanel.HotLine(
						Color.yellow, (int) (x0 / xRes), (int) (y0 / yRes),
						(int) ( (x0 + gWidth - grid.getGridModel().getConfig().getSpotsWidth()) / xRes), (int) (y1 / yRes)));
				
				
				//IDEM with the height of the grid...
				v = spotTree.getColumn(edges[0]);
				vt = v.toArray();
				Arrays.sort(vt, new Comparator() {
					public int compare(Object o1, Object o2) {
						TSpotTree t1 = (TSpotTree) o1;
						TSpotTree t2 = (TSpotTree) o2;
						if (t1.getY() > t2.getY())
							return 1;
						else if (t1.getY() < t2.getY())
							return -1;
						return 0;
					}
					
					public boolean equals(Object obj) {
						return (obj == this);
					}
				});
				for (int i = 0; i < vt.length; i++) {
					if ( ( (TSpotTree) vt[i]).getRow() == edges[3]) {
						x1 = ( (TSpotTree) vt[i]).getX();
						y1 = ( (TSpotTree) vt[i]).getY();
					}
					if ( ( (TSpotTree) vt[i]).getRow() == edges[2]) {
						x2 = ( (TSpotTree) vt[i]).getX();
						y2 = ( (TSpotTree) vt[i]).getY();
					}
				}
				gHeight = Math.abs(y1 - y2) / Math.cos(angle) + grid.getGridModel().getConfig().getSpotsHeight();
				// the height of the grid is decreased with interblocks values// added 2005/05/17
				// hauteur = hauteur - (nb interlignes * espace de l'interligne en nb de blocs * epaisseur d'un bloc)
				// height = height - (interrow nb* size of the interrow (in spots)*spot height 
				gHeight = gHeight-(grid.getGridModel().getConfig().getLev2NbRows()-1)*grid.getGridModel().getConfig().getLev2YSpacing();//17/05/05
				( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).addHotLine(new TImageGraphicPanel.HotLine(
						Color.cyan, (int) (x1 / xRes), (int) (y1 / yRes), (int) (x2 / xRes), (int) (y2 / yRes)));
			}
			
			int zoom = aliView.getGraphicPanel().getZoom();
			alignmentModel.setGridModel(grid.getGridModel());
			aliView.setGridModel(grid.getGridModel());
			( (TAlignmentControler)alignment.getControler()).setGridControler(grid.getGridControler());
			aliView.setDividerLocation(dloc);
			if (!STOP) {
				// the algorithm gives the height and the width found
				// the findedge function is used to find borders of the grid for the network,
				// it's also the function that must manage interblocks values in order to return height and width values
				// that consider these parameters ( = a bigger grid)
				// Indeed, findedge must find a number of columns and rows. Theses numbers are given
				// It's why they must count interblocks values
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_WIDTH,
						aliView.getReference(), new Double(gWidth)));
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_HEIGHT,
						aliView.getReference(), new Double(gHeight)));
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_ANGLE, aliView.getReference(),
						new Double(angle)));
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_POSITION, aliView.getReference(),
						new Double(x0), new Double(y0)));
			}
			// Here the grid is placed
			aliView.getGraphicPanel().setZoom(zoom);
			( (TAlignmentGraphicPanel) aliView.getGraphicPanel()).clearHotSpots();
			if (alignment.isInBatch())
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
						TBatchControler.REFRESH_VIEW, alignment.getBatch()));
			}
		return null;
	}
	private boolean isWarning(TGrid grid, TSpotTree spotTree, int[] edges) {
		boolean warn = false;
		int minCol = (int)(0.05D * (double)grid.getGridModel().getRootBlock().getNbColumnsInSpots());
		int minRow = (int)(0.05D * (double)grid.getGridModel().getRootBlock().getNbRowsInSpots());
		if ((edges[0] > spotTree.getLeftColumn()) && (spotTree.getColumnNbSpots(edges[0] - 1) > minCol)) warn = true;
		if ((edges[1] < spotTree.getRightColumn()) && (spotTree.getColumnNbSpots(edges[1] + 1) > minCol)) warn = true;
		if ((edges[2] > spotTree.getUpRow()) && (spotTree.getRowNbSpots(edges[2] - 1) > minRow)) warn = true;
		if ((edges[3] < spotTree.getDownRow()) && (spotTree.getRowNbSpots(edges[3] + 1) > minRow)) warn = true;
		return warn;
	}
	public void finished() {
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		TAlignment alignment = (TAlignment)aliView.getReference();
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);
		
		
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		aliView.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
		Boolean zoom = (Boolean)TEventHandler.handleMessage(event)[0];
		event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
		TEventHandler.handleMessage(event);
		if (!STOP) {
			Vector v = (Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_ALL_SPOTS, alignment))[0];
			TSpot spot;
			for (Enumeration enu = v.elements(); enu.hasMoreElements(); ) {
				spot = (TSpot) enu.nextElement();
				spot.setQuality(-1);
				spot.setFitAlgorithm(null);
				spot.setSynchro(false, grid.getView().getTablePanel());
			}
			if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
				uefga.addFinalPosition();
				ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment, uefga);
				TEventHandler.handleMessage(ev);
			}
		}
		else
			grid.getGridControler().undo(uefga);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		alignmentModel.getReference().getView().getTablePanel().setSelectable(true);
	}
	private double getAngle(Vector v) {
		TSpotTree st;
		double sx2 = 0, sy = 0, sx = 0, sxy = 0;
		int n = 0;
		for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
			st = (TSpotTree)en.nextElement();
			/*if (st.getQuality() == 1)*/ {
				sx2 += st.getX() * st.getX();
				sy += st.getY();
				sx += st.getX();
				sxy += st.getX() * st.getY();
				n++;
			}
		}
		return (n * sxy - sx * sy) / (n * sx2 - sx * sx);
	}
	
	// findinitspot allows to align the little grid ( = the sniffer block for the first time)
	// by searching in its central zone a beutiful spot
	private double findInitSpot(TLocalAlignmentBlockAlgorithm algo, double xMult, double yMult) {
		
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		double width = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).doubleValue() * xRes;
		double height = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS)).doubleValue() * yRes;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, null,
				new Double(width * xMult - grid.getGridModel().getRootBlock().getX()),
				new Double(height * yMult - grid.getGridModel().getRootBlock().getY())));
		double qq = 0;
		double dx = grid.getGridModel().getConfig().getSpotsWidth();    
		double dy = grid.getGridModel().getConfig().getSpotsHeight();
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali,
				new Double(-dx * BSM1S2), new Double(-dy * BSM1S2)));
		double trans_x = 0, trans_y = 0;//added 2005/09/13
		while (qq == 0) {
			for (int i = 0; i < 20; i++) {
				algo.execute(false);
				algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
				qq = ((TSpot)((TGridModel)algo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL)).getSpot(BSBSM1S2)).getQuality();
				//System.out.println("quality spot ="+qq);
				if (qq == 1.0D) break;//of quality is OK we pass
				// else an other position is tried around to this one
				if ( ( (TSpot) ( (TGridModel) algo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL)).getSpot(BSBSM1S2 - BLOCK_SIZE)).getQuality() == 1.0D) {
					trans_y -= dy;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double(0), new Double( -dy)));
				}
				else if ( ( (TSpot) ( (TGridModel) algo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL)).getSpot(BSBSM1S2 - 1)).getQuality() == 1.0D) {
					trans_x -= dx;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double( -dx), new Double(0)));
				}
				else if ( ( (TSpot) ( (TGridModel) algo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL)).getSpot(BSBSM1S2 + 1)).getQuality() == 1.0D) {
					trans_x += dx;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double(dx), new Double(0)));
				}
				else if ( ( (TSpot) ( (TGridModel) algo.getWorkingData(TLocalAlignmentAlgorithm.GRID_MODEL)).getSpot(BSBSM1S2 + BLOCK_SIZE)).getQuality() == 1.0D) {
					trans_y += dy;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double(0), new Double(dy)));
				}
				else if (xMult <= 0.5D) {
					trans_x += dx;
					trans_y += dy;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double(dx), new Double(dy)));
				}
				else {
					trans_x -= dx;
					trans_y -= dy;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double( -dx), new Double( -dy)));
				}
			}
			if (qq == 0) {
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali, new Double(trans_x), new Double(trans_y)));
				if (width > height) {
					if ((xMult * 1.2) > 1) break;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali,
							new Double(width * xMult * 1.2 - grid.getGridModel().getRootBlock().getX()),
							new Double(height * yMult - grid.getGridModel().getRootBlock().getY())));
				}
				else {
					if ((yMult * 1.2) > 1) break;
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, ali,
							new Double(width * xMult - grid.getGridModel().getRootBlock().getX()),
							new Double(height * yMult * 1.2 - grid.getGridModel().getRootBlock().getY())));
				}
				trans_x = trans_y = 0;
			}
		}
		double yAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
		double xAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
		grid.getGridModel().getRootBlock().reset();
		double yBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
		double xBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali, new Double(xAfter - xBef), new Double(yAfter - yBef)));
		algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
		return qq;
	}
	
	//with the network,nbCols and nbRows=> defines borders of the network
	private int[] findEdges(TSpotTree spotTree,TGrid grid) {
		// for the interblocks consideration,we just nedd to modify in "construct" numbers to find by adding
		// the good number of interblocks = nb of blocks -1
		// WARNING = interblocks values must be multiple of spot width to work with the algorithm! 	
		
		//2005/05/17 grisd info are get
		int nbCols=grid.getGridModel().getRootBlock().getNbColumnsInSpots();// the number of columns 
		int nbRows=grid.getGridModel().getRootBlock().getNbRowsInSpots();// the number of rows
		double colSpacing = grid.getGridModel().getConfig().getLev2XSpacing();//inter-column space
		double rowSpacing = grid.getGridModel().getConfig().getLev2YSpacing();// inter-row space
		
		int[] ret = new int[6];
		ret[0] = spotTree.getLeftColumn();//the column the most on the left where there is a valid spot (red)
		ret[1] = spotTree.getRightColumn();
		ret[2] = spotTree.getUpRow();
		ret[3] = spotTree.getDownRow();
		ret[4] = ret[1] - ret[0] + 1;// difference = nb of columns found
		//this difference is compared with the expected number given in parameter
		ret[5] = ret[3] - ret[2] + 1;//number of rows found
		int n1, n2;
		
		// 17/05/05 taking account of interblocks
		int nbInterCol = 0;
		int nbInterRow = 0;
		//if the inter-column space is different to 0, there is (nb of blocks -1) inter-columns
		if (colSpacing>0) 
			nbInterCol= (grid.getGridModel().getConfig().getLev2NbCols()-1);
		// idem for rows
		if (rowSpacing>0) 
			nbInterRow= (grid.getGridModel().getConfig().getLev2NbRows()-1);
		// inter-columns and inter-rows are added to the number of columns and rows to find
		nbCols+= nbInterCol;
		nbRows+= nbInterRow;
		
		while (ret[4] > nbCols) {
			// extrem columns that have the least of spots are removed until having the good number of columns 
			n1 = spotTree.getColumnNbSpots(ret[0]); // System.out.println("n1="+n1);
			n2 = spotTree.getColumnNbSpots(ret[1]); // System.out.println("n2="+n2);
			if (n1 < n2)
				ret[0]++;
			else
				ret[1]--;
			ret[4]--;
		}
		if (ret[4] < nbCols) {
			//case of missing columns: empty columns are added to the right
			ret[1] += (nbCols - ret[4]);
			ret[4] = nbCols;
		}
		
		// same treatment for rows
		while (ret[5] > nbRows) {
			n1 = spotTree.getRowNbSpots(ret[2]);
			n2 = spotTree.getRowNbSpots(ret[3]);
			if (n1 < n2)
				ret[2]++;
			else
				ret[3]--;
			ret[5]--;
		}
		if (ret[5] < nbRows) {
			ret[3] += (nbRows - ret[5]);
			ret[5] = nbRows;
		}
		
		return ret;
	}
	private int compteUDLR(double angle, int width, int height, int x, int y) {
		int n = 0;
		n += compteULR(angle, width, height, x, y);
		n += compteDLR(angle, width, height, x, y);
		n += compteL(angle, width, height, x, y);
		n += compteR(angle, width, height, x, y);
		return n;
	}
	
	
	// sniffer way = Up->Down->Left->Right
	// this method displays crosses from the first position of the sniffer 
	// and launch other methods ULR; DLR puis L et R...
	private TSpotTree parcoursUDLR(TLocalAlignmentBlockAlgorithm algo) {
		algo.getBlockAlgorithm().setParameter(TLevel1BlockAlignmentAlgorithm.FIXED_LINE, new Integer(0));
		algo.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
				(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
		algo.getBlockAlgorithm().setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
				(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
		algo.execute(false);
		pc_done += progressIncrement;
		TSpotTree st, tmp;
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		st = makeTree(grid, BSM1S2, BSM1S2);
		Vector v = st.getSpotTrees(BSM1S2);
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		//  halfSpotWidth & halfSpotHeight added 2005/09/23: in order to draw crosses in the middle of the spot during the alignment process
		double halfSpotWidth = grid.getGridModel().getConfig().getSpotsWidth()/2;
		double halfSpotHeight = grid.getGridModel().getConfig().getSpotsHeight()/2;
		int x1, y1, x2, y2;
		//display of little crosses of the sniffer block in the area where he success to init
		for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
			tmp = (TSpotTree)en.nextElement();
			if (tmp.getQuality() == 1.0D) {
				// crosses are 4 pixels H&W for the first block, 2 pixels for others
				x1 = (int)((tmp.getX()+halfSpotWidth) / xRes) - 3;
				y1 = (int)((tmp.getY()+halfSpotHeight) / yRes);
				x2 = (int)((tmp.getX()+halfSpotWidth) / xRes) + 3;
				((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
						
						new TImageGraphicPanel.HotLine(color[3], x1, y1, x2, y1));
				x1 = (int)((tmp.getX()+halfSpotWidth)/ xRes); 
				y1 = (int)((tmp.getY()+halfSpotHeight )/ yRes) - 3;
				y2 = (int)((tmp.getY()+halfSpotHeight )/ yRes) + 3;
				((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
						new TImageGraphicPanel.HotLine(color[3], x1, y1, x1, y2));
				
			}
		}
		
		double x0 = grid.getGridModel().getRootBlock().getX();
		double y0 = grid.getGridModel().getRootBlock().getY();
		
		TSpotTree st1, st2;
		tmp = parcoursULR(algo, (int)(((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue() * xRes));
		
		for (int k = 0; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getLeft();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getUp();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getLeft();
			//System.out.println("coucou BSM1S2="+BSM1S2);
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getDown();
			st1.setUp(st2);
			st2.setDown(st1);
		}
		for (int k = 1; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getRight();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getUp();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getRight();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getDown();
			st1.setUp(st2);
			st2.setDown(st1);
		}
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
				new Double(x0 - grid.getGridModel().getRootBlock().getX()),
				new Double(y0 - grid.getGridModel().getRootBlock().getY())));
		tmp = parcoursDLR(algo, 0, 0, (int)(((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS)).intValue() * yRes),
				(int)(((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue() * xRes));
		
		for (int k = 0; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getLeft();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getDown();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getLeft();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getUp();
			st1.setDown(st2);
			st2.setUp(st1);
		}
		for (int k = 1; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getRight();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getDown();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getRight();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getUp();
			st1.setDown(st2);
			st2.setUp(st1);
		}
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
				new Double(x0 - grid.getGridModel().getRootBlock().getX()),
				new Double(y0 - grid.getGridModel().getRootBlock().getY())));
		tmp = parcoursL(algo);
		
		for (int k = 0; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getUp();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getUp();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
			st1.setLeft(st2);
			st2.setRight(st1);
		}
		for (int k = 1; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getDown();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getDown();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
			st1.setLeft(st2);
			st2.setRight(st1);
		}
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
				new Double(x0 - grid.getGridModel().getRootBlock().getX()),
				new Double(y0 - grid.getGridModel().getRootBlock().getY())));
		tmp = parcoursR(algo, (int)(((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue() * xRes));
		
		for (int k = 0; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getUp();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getUp();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
			st1.setRight(st2);
			st2.setLeft(st1);
		}
		for (int k = 1; k <= BSM1S2; k++) {
			st1 = st;
			for (int i = 0; i < k; i++) st1 = st1.getDown();
			for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
			st2 = tmp;
			for (int i = 0; i < k; i++) st2 = st2.getDown();
			for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
			st1.setRight(st2);
			st2.setLeft(st1);
		}
		return st;
	}
	private int compteULR(double angle, int width, int height, int x, int y) {
		int n = 1;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		double yr = ((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getImageModel().getPixelHeight();
		y -= ((BLOCK_SIZE - 1) * spotHeight / yr * Math.cos(angle));
		x -= ((BLOCK_SIZE - 1) * spotHeight / yr * Math.sin(angle));
		if (y >= 0)
			n += compteULR(angle, width, height, x, y);
		n += compteL(angle, width, height, x, y);
		n += compteR(angle, width, height, x, y);
		return n;
	}
	
	// sniffer goes to the top...
	private TSpotTree parcoursULR(TLocalAlignmentBlockAlgorithm algo, int width) {
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		TSpotTree _spotTree = null, tmp;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		//  halfSpotWidth & halfSpotHeight added 2005/09/23: in order to draw crosses in the middle of the spot during the alignment process
		double halfSpotWidth = spotWidth/2;
		double halfSpotHeight = spotHeight/2;
		if (grid.getGridModel().getRootBlock().getY() >= 0) {
			TSpot spot = grid.getGridModel().getSpot(BSM1S2);
			double x = spot.getXCenter();
			double y = spot.getYCenter();
			double dx = x - grid.getGridModel().getSpot((int)((double)(BLOCK_SIZE - 1) * ((double)BLOCK_SIZE + 1.0D / 2.0D))).getXCenter();
			double dy = y - grid.getGridModel().getSpot((int)((double)(BLOCK_SIZE - 1) * ((double)BLOCK_SIZE + 1.0D / 2.0D))).getYCenter();
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(dx), new Double(dy)));
			double y0 = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			double x0 = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double xBef = x0;
			double yBef = y0;
			algo.getBlockAlgorithm().setParameter(TLevel1BlockAlignmentAlgorithm.FIXED_LINE, new Integer(TLevel1BlockAlignmentAlgorithm.BOTTOM));
			algo.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D + gaAlgo.getStartProgress()));
			algo.getBlockAlgorithm().setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
			algo.execute(false);
			pc_done += progressIncrement;
			double xAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			if ((Math.abs(yBef - yAfter) > (0.3D * spotHeight)) || (Math.abs(xBef - xAfter) > (0.3D * spotWidth))) {
				grid.getGridModel().getRootBlock().reset();
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
			}
			else {
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
				grid.getGridModel().getRootBlock().reset();
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
						new Double(xAfter - xBef), new Double(yAfter - yBef)));
				algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
			}
			Vector v = _spotTree.getSpotTrees(BSM1S2);
			int x1, y1, x2, y2;
			//drawing of the lillte crosses in the center of spots instead of top left corner of spot before 2005/09/23
			for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				tmp = (TSpotTree)en.nextElement();
				// if spot quality is 1, a cross is drawn
				if (tmp.getQuality() == 1.0D) {
					x1 = (int)((tmp.getX()+halfSpotWidth) / xRes) - 2;
					y1 = (int)((tmp.getY()+halfSpotHeight) / yRes);
					x2 = (int)((tmp.getX()+halfSpotWidth) / xRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[2], x1, y1, x2, y1));
					x1 = (int)((tmp.getX()+halfSpotWidth)/ xRes); 
					y1 = (int)((tmp.getY()+halfSpotHeight )/ yRes) - 2;
					y2 = (int)((tmp.getY()+halfSpotHeight )/ yRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[2], x1, y1, x1, y2));
				}
			}
			TSpotTree st;
			if (!STOP)
				st = parcoursULR(algo, width);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getLeft();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getUp();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getLeft();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getDown();
					st1.setUp(st2);
					st2.setDown(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getRight();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getUp();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getRight();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getDown();
					st1.setUp(st2);
					st2.setDown(st1);
				}
			}
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(x0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX()),
					new Double(y0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY())));
			if (!STOP)
				st = parcoursL(algo);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getUp();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getUp();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getDown();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getDown();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
			}
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(x0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX()),
					new Double(y0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY())));
			if (!STOP)
				st = parcoursR(algo, width);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getUp();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getUp();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getDown();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getDown();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
			}
		}
		return _spotTree;
	}
	private int compteDLR(double angle, int width, int height, int x, int y) {
		int n = 1;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		double yr = ((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getImageModel().getPixelHeight();
		y += ((BLOCK_SIZE - 1) * spotHeight / yr * Math.cos(angle));
		x += ((BLOCK_SIZE - 1) * spotHeight / yr * Math.sin(angle));
		if (y <= (height - ((BLOCK_SIZE - 1) * spotHeight / yr * Math.cos(angle))))
			n += compteDLR(angle, width, height, x, y);
		n += compteL(angle, width, height, x, y);
		n += compteR(angle, width, height, x, y);
		return n;
	}
	
	// sniffer goes to the bottom
	private TSpotTree parcoursDLR(TLocalAlignmentBlockAlgorithm algo, int colValue, int linValue, int height, int width) {
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		TSpotTree _spotTree = null, tmp;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		//  halfSpotWidth & halfSpotHeight added 2005/09/23: in order to draw crosses in the middle of the spot during the alignment process
		double halfSpotWidth = spotWidth/2;
		double halfSpotHeight = spotHeight/2;
		if (grid.getGridModel().getRootBlock().getY() <= (height - grid.getGridModel().getHeight())) {
			TSpot spot = grid.getGridModel().getSpot((int)((double)(BLOCK_SIZE - 1) * ((double)BLOCK_SIZE + 1.0D / 2.0D)));
			double x = spot.getXCenter();
			double y = spot.getYCenter();
			double dx = x - grid.getGridModel().getSpot(BSM1S2).getXCenter();
			double dy = y - grid.getGridModel().getSpot(BSM1S2).getYCenter();
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(dx), new Double(dy)));
			double y0 = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			double x0 = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double xBef = x0;
			double yBef = y0;
			algo.getBlockAlgorithm().setParameter(TLevel1BlockAlignmentAlgorithm.FIXED_LINE, new Integer(TLevel1BlockAlignmentAlgorithm.TOP));
			algo.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D + gaAlgo.getStartProgress()));
			algo.getBlockAlgorithm().setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
			algo.execute(false);
			pc_done += progressIncrement;
			double xAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			if ((Math.abs(yBef - yAfter) > (0.3D * spotWidth)) || (Math.abs(xBef - xAfter) > (0.3D * spotHeight))) {
				grid.getGridModel().getRootBlock().reset();
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
			}
			else {
				grid.getGridModel().getRootBlock().reset();
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
						new Double(xAfter - xBef), new Double(yAfter - yBef)));
				algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
			}
			Vector v = _spotTree.getSpotTrees(BSM1S2);
			int x1, y1, x2, y2;
			for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
				tmp = (TSpotTree)en.nextElement();
				if (tmp.getQuality() == 1.0D) {
					x1 = (int)((tmp.getX()+halfSpotWidth) / xRes) - 2;
					y1 = (int)((tmp.getY()+halfSpotHeight) / yRes);
					x2 = (int)((tmp.getX()+halfSpotWidth) / xRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							
							new TImageGraphicPanel.HotLine(color[2], x1, y1, x2, y1));
					x1 = (int)((tmp.getX()+halfSpotWidth)/ xRes); 
					y1 = (int)((tmp.getY()+halfSpotHeight )/ yRes) - 2;
					y2 = (int)((tmp.getY()+halfSpotHeight )/ yRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[2], x1, y1, x1, y2));
				}
			}
			TSpotTree st;
			if (!STOP)
				st = parcoursDLR(algo, colValue, linValue, height, width);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getLeft();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getDown();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getLeft();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getUp();
					st1.setDown(st2);
					st2.setUp(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getRight();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getDown();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getRight();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getUp();
					st1.setDown(st2);
					st2.setUp(st1);
				}
			}
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(x0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX()),
					new Double(y0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY())));
			if (!STOP)
				st = parcoursL(algo);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getUp();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getUp();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getDown();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getDown();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
			}
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(x0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX()),
					new Double(y0 - ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY())));
			if (!STOP)
				st = parcoursR(algo, width);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getUp();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getUp();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (int i = 0; i < k; i++) st1 = st1.getDown();
					for (int i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (int i = 0; i < k; i++) st2 = st2.getDown();
					for (int i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
			}
		}
		return _spotTree;
	}
	private int compteL(double angle, int width, int height, int x, int y) {
		int n = 1;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double xr = ((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getImageModel().getPixelWidth();
		y += ((BLOCK_SIZE - 1) * spotWidth / xr * Math.sin(angle));
		x -= ((BLOCK_SIZE - 1) * spotWidth / xr * Math.cos(angle));
		if (x >= 0) {
			n += compteL(angle, width, height, x, y);
		}
		return n;
	}
	//sniffer goes to the left
	private TSpotTree parcoursL(TLocalAlignmentBlockAlgorithm algo) {
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		TSpotTree _spotTree = null, tmp;
		TGrid grid = (TGrid)ali.getGridModel().getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		//  halfSpotWidth & halfSpotHeight added 2005/09/23: in order to draw crosses in the middle of the spot during the alignment process
		double halfSpotWidth = spotWidth/2;
		double halfSpotHeight = spotHeight/2;
		if (grid.getGridModel().getRootBlock().getX() >= 0) {
			TSpot spot = grid.getGridModel().getSpot(BSM1S2 * BLOCK_SIZE);
			double x = spot.getXCenter();
			double y = spot.getYCenter();
			double dx = x - grid.getGridModel().getSpot((BSM1S2 + 1) * BLOCK_SIZE - 1).getXCenter();
			double dy = y - grid.getGridModel().getSpot((BSM1S2 + 1) * BLOCK_SIZE - 1).getYCenter();
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(dx), new Double(dy)));
			double xBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			algo.getBlockAlgorithm().setParameter(TLevel1BlockAlignmentAlgorithm.FIXED_LINE, new Integer(TLevel1BlockAlignmentAlgorithm.RIGHT));
			algo.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D + gaAlgo.getStartProgress()));
			algo.getBlockAlgorithm().setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
			algo.execute(false);
			pc_done += progressIncrement;
			double xAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			if ((Math.abs(xBef - xAfter) > (0.3D * spotWidth)) || (Math.abs(yBef - yAfter) > (0.3D * spotHeight))) {
				grid.getGridModel().getRootBlock().reset();
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
			}
			else {
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
				grid.getGridModel().getRootBlock().reset();
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
						new Double(xAfter - xBef), new Double(yAfter - yBef)));
				algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
			}
			Vector v = _spotTree.getSpotTrees(BSM1S2);
			int i = 0, x1, y1, x2, y2;
			for (Enumeration en = v.elements(); en.hasMoreElements(); i++) {
				tmp = (TSpotTree)en.nextElement();
				if (tmp.getQuality() == 1.0D) {
					x1 = (int)((tmp.getX()+halfSpotWidth) / xRes) - 2;
					y1 = (int)((tmp.getY()+halfSpotHeight) / yRes);
					x2 = (int)((tmp.getX()+halfSpotWidth) / xRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[0], x1, y1, x2, y1));
					x1 = (int)((tmp.getX()+halfSpotWidth)/ xRes); 
					y1 = (int)((tmp.getY()+halfSpotHeight )/ yRes) - 2;
					y2 = (int)((tmp.getY()+halfSpotHeight )/ yRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[0], x1, y1, x1, y2));
				}
			}
			
			TSpotTree st;
			if (!STOP)
				st = parcoursL(algo);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (i = 0; i < k; i++) st1 = st1.getUp();
					for (i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (i = 0; i < k; i++) st2 = st2.getUp();
					for (i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (i = 0; i < k; i++) st1 = st1.getDown();
					for (i = 0; i < BSM1S2; i++) st1 = st1.getLeft();
					st2 = st;
					for (i = 0; i < k; i++) st2 = st2.getDown();
					for (i = 0; i < BSM1S2 - 1; i++) st2 = st2.getRight();
					st1.setLeft(st2);
					st2.setRight(st1);
				}
			}
		}
		return _spotTree;
	}
	private int compteR(double angle, int width, int height, int x, int y) {
		int n = 1;
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double xr = ((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getImageModel().getPixelWidth();
		y -= ((BLOCK_SIZE - 1) * spotWidth / xr * Math.sin(angle));
		x += ((BLOCK_SIZE - 1) * spotWidth / xr * Math.cos(angle));
		if (x <= (width - ((BLOCK_SIZE - 1) * spotWidth / xr * Math.cos(angle)))) {
			n += compteR(angle, width, height, x, y);
		}
		return n;
	}
	//sniffer goes to the right
	private TSpotTree parcoursR(TLocalAlignmentBlockAlgorithm algo, int width) {
		TAlignment ali = (TAlignment)((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference();
		TSpotTree _spotTree = null, tmp;
		TGrid grid = (TGrid)ali.getGridModel().getReference();
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		double spotWidth = grid.getGridModel().getConfig().getSpotsWidth();
		double spotHeight = grid.getGridModel().getConfig().getSpotsHeight();
		//  halfSpotWidth & halfSpotHeight added 2005/09/23: in order to draw crosses in the middle of the spot during the alignment process
		double halfSpotWidth = spotWidth/2;
		double halfSpotHeight = spotHeight/2;
		if (grid.getGridModel().getRootBlock().getX() <= (width - grid.getGridModel().getWidth())) {
			TSpot spot = grid.getGridModel().getSpot((BSM1S2 + 1) * BLOCK_SIZE - 1);
			double x = spot.getXCenter();
			double y = spot.getYCenter();
			double dx = x - grid.getGridModel().getSpot(BSM1S2 * BLOCK_SIZE).getXCenter();
			double dy = y - grid.getGridModel().getSpot(BSM1S2 * BLOCK_SIZE).getYCenter();
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
					new Double(dx), new Double(dy)));
			
			double xBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yBef = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			algo.getBlockAlgorithm().setParameter(TLevel1BlockAlignmentAlgorithm.FIXED_LINE, new Integer(TLevel1BlockAlignmentAlgorithm.LEFT));
			algo.setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D + gaAlgo.getStartProgress()));
			algo.getBlockAlgorithm().setProgressRange((int)(pc_done * progressRange / 100.0D) + gaAlgo.getStartProgress(),
					(int)((pc_done + progressIncrement) * progressRange / 100.0D) + gaAlgo.getStartProgress());
			pc_done += progressIncrement;
			algo.execute(false);
			double xAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getX();
			double yAfter = ((TGridBlock)grid.getGridModel().getRootBlock().getLevel1Elements().firstElement()).getY();
			if ((Math.abs(xBef - xAfter) > (0.3D * spotWidth)) || (Math.abs(yBef - yAfter) > (0.3D * spotHeight))) {
				grid.getGridModel().getRootBlock().reset();
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
			}
			else {
				_spotTree = makeTree(grid, BSM1S2, BSM1S2);
				grid.getGridModel().getRootBlock().reset();
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, ali,
						new Double(xAfter - xBef), new Double(yAfter - yBef)));
				algo.getBlockAlgorithm().getSpotDetectionAlgorithm().execute(false);
			}
			Vector v = _spotTree.getSpotTrees(BSM1S2);
			int i = 0, x1, y1, x2, y2;
			for (Enumeration en = v.elements(); en.hasMoreElements(); i++) {
				tmp = (TSpotTree)en.nextElement();
				if (tmp.getQuality() == 1.0D) {
					x1 = (int)((tmp.getX()+halfSpotWidth) / xRes) - 2;
					y1 = (int)((tmp.getY()+halfSpotHeight) / yRes);
					x2 = (int)((tmp.getX()+halfSpotWidth) / xRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[0], x1, y1, x2, y1));
					x1 = (int)((tmp.getX()+halfSpotWidth)/ xRes); 
					y1 = (int)((tmp.getY()+halfSpotHeight )/ yRes) - 2;
					y2 = (int)((tmp.getY()+halfSpotHeight )/ yRes) + 2;
					((TAlignmentGraphicPanel)ali.getView().getGraphicPanel()).addHotLine(
							new TImageGraphicPanel.HotLine(color[0], x1, y1, x1, y2));
				}
			}
			TSpotTree st;
			if (!STOP)
				st = parcoursR(algo, width);
			else
				st = null;
			if (st != null) {
				TSpotTree st1, st2;
				for (int k = 0; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (i = 0; i < k; i++) st1 = st1.getUp();
					for (i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (i = 0; i < k; i++) st2 = st2.getUp();
					for (i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
				for (int k = 1; k <= BSM1S2; k++) {
					st1 = _spotTree;
					for (i = 0; i < k; i++) st1 = st1.getDown();
					for (i = 0; i < BSM1S2; i++) st1 = st1.getRight();
					st2 = st;
					for (i = 0; i < k; i++) st2 = st2.getDown();
					for (i = 0; i < BSM1S2 - 1; i++) st2 = st2.getLeft();
					st1.setRight(st2);
					st2.setLeft(st1);
				}
			}
		}
		return _spotTree;
	}
	
	private TSpotTree makeTree(TGrid grid, int line, int column) {
		TSpotTree[][] tree;
		int nl = grid.getGridModel().getRootBlock().getNbRowsInSpots();
		int nc = grid.getGridModel().getRootBlock().getNbColumnsInSpots();
		tree = new TSpotTree[nl][nc];
		
		for (int i = 0; i < nl; i++)
			for (int j = 0; j < nc; j++) {
				tree[i][j] = new TSpotTree(grid.getGridModel().getSpot(i * nc + j).getX(),
						grid.getGridModel().getSpot(i * nc + j).getY(), j - column, i - line);
				tree[i][j].setQuality(grid.getGridModel().getSpot(i * nc + j).getQuality());
			}
		for (int i = 0; i < nc; i++)
			for (int j = 0; j < (nl - 1); j++) {
				tree[j][i].setDown(tree[j + 1][i]);
				tree[j + 1][i].setUp(tree[j][i]);
			}
		for (int i = 0; i < nl; i++)
			for (int j = 0; j < (nc - 1); j++) {
				tree[i][j].setRight(tree[i][j + 1]);
				tree[i][j + 1].setLeft(tree[i][j]);
			}
		return tree[line][column];
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
