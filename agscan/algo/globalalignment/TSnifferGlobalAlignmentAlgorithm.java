package agscan.algo.globalalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.AGScan;
import agscan.algo.factory.TLocalAlignmentAlgorithmFactory;
import agscan.algo.localalignment.TLevel1BlockAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;

public class TSnifferGlobalAlignmentAlgorithm extends TGlobalAlignmentAlgorithm {
	public static final int SEARCH_ANGLE = 0;
	public static final int CONTRAST_PERCENT = 1;
	public static final int SEARCH_ANGLE_INCREMENT = 2;
	public static final int SNIFFER_SIZE = 3;
	public static final int LOCAL_ALIGNMENT_ALGORITHM = 4;
	//TODO externalize or not?
	public static final String NAME = "Renifleur"; 
	//	modif 2005/08/16 cf DefaultProperties.java
	public static int searchAngle = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.searchAngle");
	public static int contrastPercent = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.contrastPercent");
	public static double searchAngleIncrement = AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.searchAngleIncrement");
	public static int snifferSize = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.snifferSize");
	public static int locBootstrapSensitivity = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity");
	public static int locBootstrapIterations = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapIterations");
	public static int locFirstRunSensitivity = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunSensitivity");
	public static int locFirstRunIterations = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunIterations");
	public static int locSecondRunSensitivity =  AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunSensitivity");
	public static int locSecondRunIterations = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunIterations");
	public static int locThirdRunSensitivity =  AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunSensitivity");
	public static int locThirdRunIterations = AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunIterations");
	public static double locSensitivityThreshold =  AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locSensitivityThreshold");
	public static double locT =  AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locT");
	public static double locC =  AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locC");
	
	public static boolean locGridCheck = false; 
	public static String locSpotDetectionAlgorithmName = TFourProfilesSpotDetectionAlgorithm.NAME;
	public static Object[] locSpotDetectionAlgorithmParameters = {
			//			 we must use default values to initialize these parameters
			// we use "4 profils spot detection" default values
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold"))
	};
	
	public static int lastSearchAngle = searchAngle;
	public static int lastContrastPercent = contrastPercent;
	public static double lastSearchAngleIncrement = searchAngleIncrement;
	public static int lastSnifferSize = snifferSize;
	public static int lastLocBootstrapSensitivity = locBootstrapSensitivity;
	public static int lastLocBootstrapIterations = locBootstrapIterations;
	public static int lastLocFirstRunSensitivity = locFirstRunSensitivity;
	public static int lastLocFirstRunIterations = locFirstRunIterations;
	public static int lastLocSecondRunSensitivity = locSecondRunSensitivity;
	public static int lastLocSecondRunIterations = locSecondRunIterations;
	public static int lastLocThirdRunSensitivity = locThirdRunSensitivity;
	public static int lastLocThirdRunIterations = locThirdRunIterations;
	public static double lastLocSensitivityThreshold = locSensitivityThreshold;
	public static boolean lastLocGridCheck = locGridCheck;
	public static double lastLocT = locT;
	public static double lastLocC = locC;
	public static String lastLocSpotDetectionAlgorithmName = locSpotDetectionAlgorithmName;
	public static Object[] lastLocSpotDetectionAlgorithmParameters;
	
	static {
		lastLocSpotDetectionAlgorithmParameters = new Object[locSpotDetectionAlgorithmParameters.length];
		for (int i = 0; i < locSpotDetectionAlgorithmParameters.length; i++)
			lastLocSpotDetectionAlgorithmParameters[i] = locSpotDetectionAlgorithmParameters[i];
	}
	
	public TSnifferGlobalAlignmentAlgorithm() {
		super();
		parameters = new Object[5];
		TLocalAlignmentAlgorithmFactory factory = new TLocalAlignmentAlgorithmFactory();
		setParameter(LOCAL_ALIGNMENT_ALGORITHM, factory.createAlgorithm(TLocalAlignmentBlockAlgorithm.NAME));
		initWithLast();
		controlPanel = TSnifferGlobalAlignmentAlgorithmPanel.getInstance(this);
		setImagesMin(1);
		setImagesMax(1);
	}
	public String getName() {
		return NAME;
	}
	public void init() {
		setParameter(SEARCH_ANGLE, new Integer(((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle()));
		setParameter(CONTRAST_PERCENT, new Integer(((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getContrastPercent()));
		setParameter(SEARCH_ANGLE_INCREMENT, new Double(((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngleIncrement()));
		setParameter(SNIFFER_SIZE, new Integer(((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSnifferSize()));
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		blockAlignmentAlgorithm.init();
	}
	public void initWithDefaults() {
		setParameter(SEARCH_ANGLE, new Integer(searchAngle));
		setParameter(CONTRAST_PERCENT, new Integer(contrastPercent));
		setParameter(SEARCH_ANGLE_INCREMENT, new Double(searchAngleIncrement));
		setParameter(SNIFFER_SIZE, new Integer(snifferSize));
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		Object[] params = new Object[11];
		params[0] = new Integer(locBootstrapSensitivity);
		params[1] = new Integer(locBootstrapIterations);
		params[2] = new Integer(locFirstRunSensitivity);
		params[3] = new Integer(locFirstRunIterations);
		params[4] = new Integer(locSecondRunSensitivity);
		params[5] = new Integer(locSecondRunIterations);
		params[6] = new Integer(locThirdRunSensitivity);
		params[7] = new Integer(locThirdRunIterations);
		params[8] = new Double(locSensitivityThreshold);
		params[9] = new Boolean(locGridCheck);
		Object[] params2 = new Object[4];
		params2[0] = new Double(locT);
		params2[1] = new Double(locC);
		params2[2] = locSpotDetectionAlgorithmName;
		params2[3] = locSpotDetectionAlgorithmParameters;
		params[10] = params2;
		blockAlignmentAlgorithm.init(params);
	}
	public void init(Object[] params) {
		setParameter(SEARCH_ANGLE, params[0]);
		setParameter(CONTRAST_PERCENT, params[1]);
		setParameter(SEARCH_ANGLE_INCREMENT, params[2]);
		setParameter(SNIFFER_SIZE, params[3]);
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		Object[] params2 = new Object[11];
		params2[0] = ((Object[])params[4])[0];
		params2[1] = ((Object[])params[4])[1];
		params2[2] = ((Object[])params[4])[2];
		params2[3] = ((Object[])params[4])[3];
		params2[4] = ((Object[])params[4])[4];
		params2[5] = ((Object[])params[4])[5];
		params2[6] = ((Object[])params[4])[6];
		params2[7] = ((Object[])params[4])[7];
		params2[8] = ((Object[])params[4])[8];
		params2[9] = ((Object[])params[4])[9];
		Object[] params3 = new Object[4];
		params3[0] = ((Object[])((Object[])params[4])[10])[0];
		params3[1] = ((Object[])((Object[])params[4])[10])[1];
		params3[2] = ((Object[])((Object[])params[4])[10])[2];
		params3[3] = ((Object[])((Object[])params[4])[10])[3];
		params2[10] = params3;
		blockAlignmentAlgorithm.init(params2);
		setLasts();
	}
	public String getStringParams() {
		String s = "";
		s = getParameter(SEARCH_ANGLE).toString() + "|" + getParameter(CONTRAST_PERCENT).toString() + "|" + getParameter(SEARCH_ANGLE_INCREMENT).toString() +
		"|" + getParameter(SNIFFER_SIZE).toString() + "|" + ((TLocalAlignmentBlockAlgorithm)getParameter(LOCAL_ALIGNMENT_ALGORITHM)).getStringParams();
		return s;
	}	
	/*
	 * method added 2005/08/17
	 * if values are changed, we save them into the properties file
	 * method called when "apply"
	 */
	public void saveInProperties(){
		int panelIntValue;
		double panelDoubleValue;
		
		// searchAngle
		panelIntValue = ((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle();
		// if the panel value is different that the property value => change the property value
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.searchAngle")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.searchAngle",panelIntValue); 	
		}
		// contrastPercent
		panelIntValue = ((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getContrastPercent();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.contrastPercent")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.contrastPercent",panelIntValue); 	
		}
		// searchAngleIncrement
		panelDoubleValue = ((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngleIncrement();
		
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.searchAngleIncrement")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.searchAngleIncrement",panelDoubleValue); 	
		}
		// snifferSize
		panelIntValue = ((TSnifferGlobalAlignmentAlgorithmPanel)controlPanel).getSnifferSize();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.snifferSize")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.snifferSize",panelIntValue); 	
		}
		
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		// locBootstrapSensitivity
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
		//System.out.println("locBootstrapSensitivity saisie = "+panelIntValue);
		//System.out.println("locBootstrapSensitivity du prop= "+AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity"));
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity",panelIntValue); 	
		}
		// locBootstrapIterations
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapIterations")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locBootstrapIterations",panelIntValue); 	
		}
		// locFirstRunSensitivity 
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunSensitivity")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunSensitivity",panelIntValue); 	
		}
		// locFirstRunIterations 
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunIterations")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locFirstRunIterations",panelIntValue); 	
		}
		// locSecondRunSensitivity 
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunSensitivity")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunSensitivity",panelIntValue); 	
		}
		// locSecondRunIterations 
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunIterations")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locSecondRunIterations",panelIntValue); 	
		}
		// locThirdRunSensitivity 
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunSensitivity")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunSensitivity",panelIntValue); 	
		}
		// locThirdRunIterations
		panelIntValue = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
		if (panelIntValue != AGScan.prop.getIntProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunIterations")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locThirdRunIterations",panelIntValue); 	
		}
		// locSensitivityThreshold 
		panelDoubleValue = ((Double)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locSensitivityThreshold")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locSensitivityThreshold",panelDoubleValue); 	
		}
		
		TLevel1BlockAlignmentAlgorithm gridBlockAlignmentAlgorithm = (TLevel1BlockAlignmentAlgorithm)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.GRID_BLOCK_ALIGNMENT_ALGORITHM);
		// locT 
		panelDoubleValue = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.T)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locT")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locT",panelDoubleValue); 	
		}
		// locC
		panelDoubleValue = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.C)).doubleValue();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TSnifferGlobalAlignmentAlgorithm.locC")){
			AGScan.prop.setProperty("TSnifferGlobalAlignmentAlgorithm.locC",panelDoubleValue); 	
		}
		
		TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
		// for maxSearchZone & maxComputeZone & qualityThreshold : internal call
		spotDetectionAlgorithm.saveInProperties();
		
	}
	
	public void setLasts() {
		lastContrastPercent = ((Integer)parameters[CONTRAST_PERCENT]).intValue();
		lastSearchAngle = ((Integer)parameters[SEARCH_ANGLE]).intValue();
		lastSearchAngleIncrement = ((Double)parameters[SEARCH_ANGLE_INCREMENT]).doubleValue();
		lastSnifferSize = ((Integer)parameters[SNIFFER_SIZE]).intValue();
		
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		lastLocBootstrapSensitivity = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_SENSITIVITY)).intValue();
		lastLocBootstrapIterations = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.BOOTSTRAP_ITERATIONS)).intValue();
		lastLocFirstRunSensitivity = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_SENSITIVITY)).intValue();
		lastLocFirstRunIterations = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.FIRST_RUN_ITERATIONS)).intValue();
		lastLocSecondRunSensitivity = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_SENSITIVITY)).intValue();
		lastLocSecondRunIterations = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SECOND_RUN_ITERATIONS)).intValue();
		lastLocThirdRunSensitivity = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_SENSITIVITY)).intValue();
		lastLocThirdRunIterations = ((Integer)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.THIRD_RUN_ITERATIONS)).intValue();
		lastLocSensitivityThreshold = ((Double)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.SENSITIVITY_THRESHOLD)).doubleValue();
		lastLocGridCheck = ((Boolean)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.GRID_CHECK)).booleanValue();
		TLevel1BlockAlignmentAlgorithm gridBlockAlignmentAlgorithm = (TLevel1BlockAlignmentAlgorithm)blockAlignmentAlgorithm.getParameter(TLocalAlignmentBlockAlgorithm.GRID_BLOCK_ALIGNMENT_ALGORITHM);
		lastLocT = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.T)).doubleValue();
		lastLocC = ((Double)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.C)).doubleValue();
		
		TSpotDetectionAlgorithm spotDetectionAlgorithm = (TSpotDetectionAlgorithm)gridBlockAlignmentAlgorithm.getParameter(TLevel1BlockAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
		lastLocSpotDetectionAlgorithmName = spotDetectionAlgorithm.getName();
		for (int i = 0; i < spotDetectionAlgorithm.getParameters().length; i++)
			lastLocSpotDetectionAlgorithmParameters[i] = spotDetectionAlgorithm.getParameter(i);
	}
	public void initWithLast() {
		parameters[CONTRAST_PERCENT] = new Integer(lastContrastPercent);
		parameters[SEARCH_ANGLE] = new Integer(lastSearchAngle);
		parameters[SEARCH_ANGLE_INCREMENT] = new Double(lastSearchAngleIncrement);
		parameters[SNIFFER_SIZE] = new Integer(lastSnifferSize);
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		Object[] params = new Object[11];
		params[0] = new Integer(lastLocBootstrapSensitivity);
		params[1] = new Integer(lastLocBootstrapIterations);
		params[2] = new Integer(lastLocFirstRunSensitivity);
		params[3] = new Integer(lastLocFirstRunIterations);
		params[4] = new Integer(lastLocSecondRunSensitivity);
		params[5] = new Integer(lastLocSecondRunIterations);
		params[6] = new Integer(lastLocThirdRunSensitivity);
		params[7] = new Integer(lastLocThirdRunIterations);
		params[8] = new Double(lastLocSensitivityThreshold);
		params[9] = new Boolean(lastLocGridCheck);
		Object[] params2 = new Object[4];
		params2[0] = new Double(lastLocT);
		params2[1] = new Double(lastLocC);
		params2[2] = lastLocSpotDetectionAlgorithmName;
		params2[3] = lastLocSpotDetectionAlgorithmParameters;
		params[10] = params2;
		blockAlignmentAlgorithm.init(params);
	}
	public void execute(boolean thread) {
		execute(thread, Thread.MIN_PRIORITY);
	}
	
	/** 2 possibility to call the execution of the algorithm:
	 * - start launch into an independant thread 
	 * - construct/finish launch into the same processus
	 * it is useful in order to use the algorithm into an other, forcing it to 
	 * wait the end of the sub-algorithm
	 */
	public void execute(boolean thread, int priority) {
		TLocalAlignmentBlockAlgorithm blockAlignmentAlgorithm = (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
		blockAlignmentAlgorithm.setProgressRange(startProgress, endProgress);
		worker = new TSnifferGlobalAlignmentWorker(this);
		worker.setPriority(priority);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}
	public TLocalAlignmentBlockAlgorithm getLocalAlignmentBlockAlgorithm() {
		return (TLocalAlignmentBlockAlgorithm)parameters[LOCAL_ALIGNMENT_ALGORITHM];
	}
	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TSnifferGlobalAlignmentAlgorithm.1"), //$NON-NLS-1$
				Messages.getString("TSnifferGlobalAlignmentAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (rep == JOptionPane.YES_OPTION) {
			worker.stop();
		}
	}
	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			worker.setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			worker.setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			worker.setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}
	public int getDefaultSnifferSize() {
		return snifferSize;
	}
	public int getDefaultSearchAngle() {
		return searchAngle;
	}
	public int getDefaultContrastPercent() {
		return contrastPercent;
	}
	public double getDefaultSearchAngleIncrement() {
		return searchAngleIncrement;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
