package agscan.algo.globalalignment;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;

public class TSnifferGlobalAlignmentAlgorithmPanel extends TAlgorithmControlPanel {
  private JSpinner searchAngleSpinner, contrastPercentSpinner, searchAngleIncrementSpinner, snifferSizeSpinner;
  public static TSnifferGlobalAlignmentAlgorithmPanel instance = null;
  private TAlgorithmControlPanel localAlignmentBlockAlgorithmControlPanel;

  protected TSnifferGlobalAlignmentAlgorithmPanel(TSnifferGlobalAlignmentAlgorithm algo) {
    localAlignmentBlockAlgorithmControlPanel = algo.getLocalAlignmentBlockAlgorithm().getControlPanel();
    jbInit();
  }
  private void jbInit() {
    removeAll();
    JLabel searchAngleLabel = new JLabel(Messages.getString("TSnifferGlobalAlignmentAlgorithmPanel.0")); //$NON-NLS-1$
    JLabel contrastPercentLabel = new JLabel(Messages.getString("TSnifferGlobalAlignmentAlgorithmPanel.1")); //$NON-NLS-1$
    JLabel searchAngleIncrementLabel = new JLabel(Messages.getString("TSnifferGlobalAlignmentAlgorithmPanel.2")); //$NON-NLS-1$
    JLabel snifferSizeLabel = new JLabel(Messages.getString("TSnifferGlobalAlignmentAlgorithmPanel.3")); //$NON-NLS-1$
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEtchedBorder());

    snifferSizeSpinner = new JSpinner(new SpinnerNumberModel(5, 5, 25, 2));
    snifferSizeSpinner.setPreferredSize(new Dimension(50, snifferSizeSpinner.getMinimumSize().height));
    searchAngleSpinner = new JSpinner(new SpinnerNumberModel(2, 2, 30, 1));
    searchAngleSpinner.setPreferredSize(new Dimension(50, searchAngleSpinner.getMinimumSize().height));
    contrastPercentSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));
    contrastPercentSpinner.setPreferredSize(new Dimension(50, contrastPercentSpinner.getMinimumSize().height));
    searchAngleIncrementSpinner = new JSpinner(new SpinnerNumberModel(0.01D, 0.01D, 0.5D, 0.01D));
    searchAngleIncrementSpinner.setPreferredSize(new Dimension(50, searchAngleIncrementSpinner.getMinimumSize().height));

    add(localAlignmentBlockAlgorithmControlPanel, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));
    add(searchAngleLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(searchAngleSpinner, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(searchAngleIncrementLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(searchAngleIncrementSpinner, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(contrastPercentLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(contrastPercentSpinner, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(snifferSizeLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0), 0, 0));
    add(snifferSizeSpinner, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 10), 0, 0));
  }
  public void init(TAlgorithm algo) {
    TSnifferGlobalAlignmentAlgorithm alg = (TSnifferGlobalAlignmentAlgorithm)algo;
    localAlignmentBlockAlgorithmControlPanel = alg.getLocalAlignmentBlockAlgorithm().getControlPanel();
    snifferSizeSpinner.setValue((Integer)alg.getParameter(TSnifferGlobalAlignmentAlgorithm.SNIFFER_SIZE));
    searchAngleSpinner.setValue((Integer)alg.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE));
    contrastPercentSpinner.setValue((Integer)alg.getParameter(TSnifferGlobalAlignmentAlgorithm.CONTRAST_PERCENT));
    searchAngleIncrementSpinner.setValue((Double)alg.getParameter(TSnifferGlobalAlignmentAlgorithm.SEARCH_ANGLE_INCREMENT));
    localAlignmentBlockAlgorithmControlPanel.init(alg.getLocalAlignmentBlockAlgorithm());
  }
  public TAlgorithmControlPanel getLocalAlignmentBlockAlgorithmControlPanel() {
    return localAlignmentBlockAlgorithmControlPanel;
  }
  public int getSnifferSize() {
    return ((Integer)snifferSizeSpinner.getValue()).intValue();
  }
  public int getSearchAngle() {
    return ((Integer)searchAngleSpinner.getValue()).intValue();
  }
  public int getContrastPercent() {
    return ((Integer)contrastPercentSpinner.getValue()).intValue();
  }
  public double getSearchAngleIncrement() {  	
  //	System.out.println("AngleIncrementSpinner= "+((Double)searchAngleIncrementSpinner.getValue()).doubleValue());
    return ((Double)searchAngleIncrementSpinner.getValue()).doubleValue();
  }
  public static TSnifferGlobalAlignmentAlgorithmPanel getInstance(TSnifferGlobalAlignmentAlgorithm algo) {
    if (instance == null)
      instance = new TSnifferGlobalAlignmentAlgorithmPanel(algo);
    else
      instance.jbInit();
    instance.init(algo);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2005 INRA - SIGENAE TEAM
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
