package agscan.algo.globalalignment;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Vector;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.factory.TGridBlockAlignmentAlgorithmFactory;
import agscan.algo.localalignment.TLevel1BlockAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.data.TDataManager;
import agscan.data.controler.TAlignmentControler;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TUndoEdgesFinderGlobalAlignment;
import agscan.data.controler.memento.TUndoGridPosition;
import agscan.data.controler.memento.TUndoGridResizing;
import agscan.data.controler.memento.TUndoLocalAlignmentBlock;
import agscan.data.controler.memento.TUndoLocalAlignmentGrid;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TLevel1SelectionModeAction;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TAlignmentView;

public class TEdgesFinderGlobalAlignmentWorker extends SwingWorker {
	private TEdgesFinderGlobalAlignmentAlgorithm gaAlgo;
	private TUndoEdgesFinderGlobalAlignment uefga;
	private TAlignmentModel alignmentModel;
	
	public TEdgesFinderGlobalAlignmentWorker(TGlobalAlignmentAlgorithm gaAlgo) {
		this.gaAlgo = (TEdgesFinderGlobalAlignmentAlgorithm)gaAlgo;
	}
	public Object construct(boolean thread) {
		pc_done = 0;
		alignmentModel = (TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL);
		alignmentModel.getReference().getView().getTablePanel().setSelectable(false);
		TGrid grid = (TGrid)alignmentModel.getGridModel().getReference();
		
		// Selection of the whole grid
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL,
				alignmentModel.getReference()));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL,
				alignmentModel.getReference()));
		
		uefga = new TUndoEdgesFinderGlobalAlignment();
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		((TAlignment)aliView.getReference()).setAlgoRunning(true);
		((TAlignment)aliView.getReference()).setAlgorithm(gaAlgo);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, aliView.getReference()));
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
				new Integer((int)pc_done)));
		aliView.getGraphicPanel().removeInteractors();
		TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null));
		
		int[] imageData = (int[])gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_DATA);
		int[] contrastedImageData = gaAlgo.makeImageSeuil();
		double l = 65535.0D - (double)((Integer)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.CONTRAST_PERCENT)).intValue() *
		(255 - ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN)).intValue()) * 256 / 100.0D;
		int w = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue();
		int h = ((Integer)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS)).intValue();
		int[] rastContrast = new int[w * h];
		
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++)
				if ((~imageData[i + j * w] & 0xFFFF) >= l)
					rastContrast[i + j * w] = 65535;
				else
					rastContrast[i + j * w] = (~imageData[i + j * w] & 0xFFFF);
		int[] plotProf = new int[w];
		int[] plotDiff = new int[w];
		int dec = (int)(Math.tan(((Integer)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ANGLE)).doubleValue() *
				Math.PI / 180.0D) * w);
		int step = (int)(Math.tan(((Double)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ANGLE_INCREMENT)).doubleValue() * Math.PI / 180.0D) * w);
		if (step == 0) step = 1;
		
		int kmin = gaAlgo.makeHorizontalProfile(alignmentModel.getReference().getPath() + alignmentModel.getReference().getName(),
				rastContrast, plotProf, plotDiff, w, h, -dec, dec, step, false);
		
		if (STOP) return null;
		
		kmin = gaAlgo.makeHorizontalProfile(alignmentModel.getReference().getPath() + alignmentModel.getReference().getName(),
				rastContrast, plotProf, plotDiff, w, h, kmin - 10, kmin + 10, 1, true);
		
		if (STOP) return null;
		
		int[] plotProfInit = new int[w];
		gaAlgo.makeHorizontalProfile(alignmentModel.getReference().getPath() + alignmentModel.getReference().getName(),
				contrastedImageData, plotProfInit, plotDiff, w, h, kmin, kmin, 10, true);
		
		if (STOP) return null;
		
		gaAlgo.makeWindowMoy(plotProfInit, 40);
		
		pc_done += 0.5;
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
				new Integer((int)pc_done)));
		
		double A = (double)kmin / (double)(w - 1);
		double angle = Math.atan(A);
		
		int[] vertProf = new int[h];
		int[] vertDiff = new int[h];
		gaAlgo.makeVerticalProfile(contrastedImageData, vertProf, vertDiff, w, h, kmin);
		
		gaAlgo.makeWindowMoy(vertProf, 40);
		
		int max = 0, min = 65536;
		for (double x = w * 0.25; x < w * 0.75; x += 1) {
			if (max < plotProfInit[(int)x]) max = plotProfInit[(int)x];
			if (min > plotProfInit[(int)x]) min = plotProfInit[(int)x];
		}
		int seuil = (int)(2 * max - min);
		if (seuil > 65535) seuil = max + (65536 - max) / 3;
		
		
		//j1 and j2 calculation : grid limits on the horizontal axis
		//j1 and j2 are used by plotProfInit in order to not miss colums on the board 
		int j1 = 0;
		int j2 = w - 1;
		int k;
		for (k = (int)(w / 2); k >= 0; k--)
			if (plotProfInit[k] > seuil) {
				j1 = k;
				break;
			}
		for (k = (int)(w / 2); k < w; k++)
			if (plotProfInit[k] > seuil) {
				j2 = k;
				break;
			}
			// Next, with values found, we search into pltoDiff the little hollow in direction of the grid
		if (j1 < 0) j1 = 0;
		if (j2 > (w - 1)) j2 = w-1;
		for (k = j1; k <= j2; k++)
			if ((plotDiff[k] < 0) && (plotDiff[k + 1] > 0)) {
				j1 = k + 1;
				break;
			}
		for (k = j2; k >= j1; k--)
			if ((plotDiff[k] < 0) && (plotDiff[k + 1] > 0)) {
				j2 = k + 1;
				break;
			}
			
		max = 0;
		min = 65536;
		for (double x = h * 0.25; x < h * 0.75; x += 1) {
			if (max < vertProf[(int)x]) max = vertProf[(int)x];
			if (min > vertProf[(int)x]) min = vertProf[(int)x];
		}
		seuil = (int)(2 * max - min);
		if (seuil > 65535) seuil = max + (65536 - max) / 3;
		
		//we search k1 and k2, the vertical limits on the vertical axis
		// process is the same that for j1 and j2.
		int k1 = 0;
		int k2 = h - 1;
		int milieu = (int)(h / 2);
		for (k = milieu; k >= 0; k--)
			if (vertProf[k] > seuil) {
				k1 = k;
				break;
			}
		for (k = milieu; k < h; k++)
			if (vertProf[k] > seuil) {
				k2 = k;
				break;
			}
		if (k1 < 0) k1 = 0;
		if (k2 > (h - 1)) k2 = h-1;
		
		for (k = k1; k <= k2; k++)
			if ((vertDiff[k] < 0) && (vertDiff[k + 1] > 0)) {
				k1 = k + 1;
				break;
			}
		for (k = k2; k >= k1; k--)
			if ((vertDiff[k] < 0) && (vertDiff[k + 1] > 0)) {
				k2 = k + 1;
				break;
			}
			
			// top left and bottom right corners are found using the calculated intersection of 
			// (j1,j2) and (k1,k2) straights
		double Ap = - 1.0D / A;
		double B = (double)k1 / Math.cos(angle);
		double Bp = (double)j1 / Math.sin(angle);
		int xDebGrille = (int)((double)j1 - A * (double)k1);
		int yDebGrille = (int)((double)k1 + A * (double)j1);
		xDebGrille = (int)((Bp - B) / (A - Ap));
		yDebGrille = (int)(A * xDebGrille + B);
		
		// angle and position are fixed
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_ANGLE,
				alignmentModel.getReference(), new Double(angle)));
		double xRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_POSITION,
				alignmentModel.getReference(), new Double(xDebGrille * xRes), new Double(yDebGrille * yRes)));
		pc_done += 0.5;
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
				new Integer((int)pc_done)));
		
		if (STOP) return null;
		
		// unselect all
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL,
				alignmentModel.getReference()));
		
		// the top left level1 block is selected
		TGridBlock bl = (TGridBlock)grid.getGridModel().getLevel1Element(0, 0);
		int xbl = (int)(bl.getX() / xRes) + 100;
		int ybl = (int)(bl.getY() / yRes) + 100;
		Point p = new Point(xbl, ybl);
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.TOGGLE_SELECT_POINT,
				alignmentModel.getReference(), p, new Integer(TLevel1SelectionModeAction.getID()),
				Boolean.FALSE));
		TUndoLocalAlignmentBlock ulab = new TUndoLocalAlignmentBlock(alignmentModel.getReference());
		double pc = 0;
		int nb_la;
		TGridBlockAlignmentAlgorithmFactory fact = new TGridBlockAlignmentAlgorithmFactory();
		TLevel1BlockAlignmentAlgorithm blockAlgorithm = (TLevel1BlockAlignmentAlgorithm)fact.createAlgorithm(TLevel1BlockAlignmentAlgorithm.NAME);
		blockAlgorithm.initWithDefaults();
		blockAlgorithm.initData(gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES),
				imageData, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
				((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel(),
				new Boolean(false), bl, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MAX));
		blockAlgorithm.init();
		for (nb_la = 1; (nb_la <= 20) && !STOP; nb_la++) {
			gaAlgo.align(blockAlgorithm, 10, 100, 0, 0, 40);
			blockAlgorithm.getSpotDetectionAlgorithm().setWorkingData(TSpotDetectionAlgorithm.SPOTS, bl.getSpots());
			blockAlgorithm.getSpotDetectionAlgorithm().execute(false);
			pc = ((Double)blockAlgorithm.getSpotDetectionAlgorithm().getResult(TSpotDetectionAlgorithm.RESULT_PC_GOOD)).doubleValue();
			if (pc >= 0.3D) break;
		}
		
		// we get this block coordinates after local alignment
		double x = bl.getX();
		double y = bl.getY();
		
		// we cancel cancel alignmant on this level1 block
		grid.getGridControler().undo(ulab);
		
		if (pc < 0.3D) {
			bl = (TGridBlock)grid.getGridModel().getLevel1Element(grid.getGridModel().getConfig().getLev1NbCols(),
					grid.getGridModel().getConfig().getLev1NbRows());
			xbl = (int)(bl.getX() / xRes) + 100;
			ybl = (int)(bl.getY() / yRes) + 100;
			p = new Point(xbl, ybl);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.TOGGLE_SELECT_POINT,
					alignmentModel.getReference(), p, new Integer(TLevel1SelectionModeAction.getID()),//TMenuManager.LEVEL1_SELECTION_MODE avant
					Boolean.FALSE));
			ulab = new TUndoLocalAlignmentBlock(alignmentModel.getReference());
			pc = 0;
			blockAlgorithm.initData(gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES),
					imageData, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
					gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
					((TAlignmentModel)gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel(),
					new Boolean(false), bl, gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN),
					gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MAX));
			for (nb_la = 1; (nb_la <= 20) && !STOP; nb_la++) {
				gaAlgo.align(blockAlgorithm, 10, 100, 0, 0, 40);
				blockAlgorithm.getSpotDetectionAlgorithm().setWorkingData(TSpotDetectionAlgorithm.SPOTS, bl.getSpots());
				blockAlgorithm.getSpotDetectionAlgorithm().execute(false);
				pc = ((Double)blockAlgorithm.getSpotDetectionAlgorithm().getResult(TSpotDetectionAlgorithm.RESULT_PC_GOOD)).doubleValue();
				if (pc >= 0.3D) break;
			}
			x = bl.getX();
			y = bl.getY();
			grid.getGridControler().undo(ulab);
		}
		
		if (STOP) return null;
		
		// all the grid is selected
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL,
				alignmentModel.getReference()));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL,
				alignmentModel.getReference()));
		
		// we move the grid to the level1 block position
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_POSITION,
				alignmentModel.getReference(), new Double(x), new Double(y)));
		
		// pixel number for the variation of the grid size(+/-)
		double zrb = ((Double)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ZONE)).doubleValue() *
		grid.getGridModel().getRootBlock().getSpotWidth() / xRes;
		
		// variation size incrementation
		int zrbi = ((Integer)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ZONE_INCREMENT)).intValue();
		
		// detection spot algorithme preparation
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SPOT_DETECTION_ALGORITHM);
		sdAlgo.initData(gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_DATA), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MAX));
		
		TUndoLocalAlignmentGrid ulag = new TUndoLocalAlignmentGrid(alignmentModel.getReference());
		TLocalAlignmentGridAlgorithm lagAlgo = new TLocalAlignmentGridAlgorithm();
		lagAlgo.initData(gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.XRES), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.YRES),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_DATA), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_WIDTH_IN_PIXELS),
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_HEIGHT_IN_PIXELS), grid.getGridModel(), aliView,
				gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MIN), gaAlgo.getWorkingData(TGlobalAlignmentAlgorithm.HISTOGRAM_MAX));
		lagAlgo.initWithDefaults();
		
		lagAlgo.setParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_ITERATIONS, new Integer(100));
		lagAlgo.setParameter(TLocalAlignmentGridAlgorithm.SECOND_RUN_ITERATIONS, new Integer(0));
		lagAlgo.setParameter(TLocalAlignmentGridAlgorithm.THIRD_RUN_ITERATIONS, new Integer(0));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.LOCAL_ALIGNMENT,
				alignmentModel.getReference(), lagAlgo, new Boolean(false)));
		((TAlignment)aliView.getReference()).setAlgoRunning(true);
		((TAlignment)aliView.getReference()).setAlgorithm(gaAlgo);
		ulag.addFinalPosition();
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY,
				alignmentModel.getReference(), sdAlgo));
		
		Vector los = bl.getSpots();
		int nbgs, nbgsmax = 0, imax = 0;
		Enumeration enume = los.elements();
		for (int i = 0; i < grid.getGridModel().getConfig().getLev1NbRows(); i++) {
			nbgs = 0;
			for (int j = 0; j < grid.getGridModel().getConfig().getLev1NbCols(); j++)
				if (((TSpot)enume.nextElement()).getQuality() == 1.0D) nbgs++;
			if (nbgsmax < nbgs) {
				nbgsmax = nbgs;
				imax = i;
			}
		}
		
		double angle2 = gaAlgo.getAngle(grid.getGridModel().getLineOfSpots(imax));
		if (!Double.isNaN(angle2)) angle = angle2;
		grid.getGridControler().undo(ulag);
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.SET_ANGLE,
				alignmentModel.getReference(), new Double(angle)));
		
		// grid state is memorized
		TUndoGridResizing ugr = new TUndoGridResizing(alignmentModel.getReference());
		
		// grid is dimentioned to its smallest size
		resizeGrid(TGridPositionControler.RESIZE_EAST, -(int)zrb);
		resizeGrid(TGridPositionControler.RESIZE_SOUTH, -(int)zrb);
		
		((TAlignment)aliView.getReference()).setAlgoRunning(true);
		((TAlignment)aliView.getReference()).setAlgorithm(gaAlgo);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, aliView.getReference()));
		
		sdAlgo = gaAlgo.getSpotDetectionAlgorithm();
		// every combination of sizes are tried in order to find the one that take the most of spots
		double pcgood, pcgoodmax = 0, dwmax = -zrb, dhmax = -zrb, inc;
		inc = 75.0D / (2.0D * zrb / zrbi + 1.0D) / (2.0D * zrb / zrbi + 1.0D);
		for (double dw = -zrb; (dw <= zrb) && !STOP; dw += zrbi) {
			for (double dh = -zrb; dh <= zrb; dh += zrbi) {
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY,
						alignmentModel.getReference(), sdAlgo));
				pcgood = ((Double)sdAlgo.getResult(TSpotDetectionAlgorithm.RESULT_PC_GOOD)).doubleValue();
				if (pcgood > pcgoodmax) {
					pcgoodmax = pcgood;
					dwmax = dw;
					dhmax = dh;
				}
				resizeGrid(TGridPositionControler.RESIZE_SOUTH, zrbi);
				pc_done += inc;
				TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS,
						aliView.getReference(), new Integer((int)pc_done)));
			}
			resizeGrid(TGridPositionControler.RESIZE_SOUTH, (int)(-2 * zrb - zrbi));
			resizeGrid(TGridPositionControler.RESIZE_EAST, zrbi);
		}
		
		// the grid is dimensioned to its initial size
		ugr.addFinalState(alignmentModel.getReference());
		grid.getGridControler().undo(ugr);
		
		if (STOP) return null;
		
		// the grid is dimensioned with its calculated values
		resizeGrid(TGridPositionControler.RESIZE_SOUTH, (int)dhmax);
		resizeGrid(TGridPositionControler.RESIZE_EAST, (int)dwmax);
		
		TUndoGridPosition ugp = new TUndoGridPosition(alignmentModel.getReference());
		
		int decal = ((Integer)gaAlgo.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.EDGES_SEARCH_ZONE)).intValue();
		translateGrid(-decal, 1.0, 1.0);
		pcgoodmax = 0;
		int dxmax = 0, dymax = 0;
		inc = 10.0D / (2.0D * decal + 1.0D) / (2.0D * decal + 1.0D);
		for (int dx = -decal; (dx <= decal) && !STOP; dx++) {
			for (int dy = -decal; dy <= decal; dy++) {
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY,
						alignmentModel.getReference(), sdAlgo));
				pcgood = ((Double)sdAlgo.getResult(TSpotDetectionAlgorithm.RESULT_PC_GOOD)).doubleValue();
				if (pcgood > pcgoodmax) {
					pcgoodmax = pcgood;
					dxmax = dx;
					dymax = dy;
				}
				translateGrid(1, 0.0, 1.0);
				pc_done += inc;
				TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS,
						aliView.getReference(), new Integer((int)pc_done)));
			}
			translateGrid(-2 * decal - 1, 0.0, 1.0);
			translateGrid(1, 1.0, 0.0);
		}
		
		if (STOP) return null;
		
		ugp.addFinalPosition(alignmentModel.getReference());
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO,
				alignmentModel.getReference(), ugp));
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.UNDO, alignmentModel.getReference());
		TEventHandler.handleMessage(ev);
		
		translateGrid(dxmax, 1.0, 0.0);
		translateGrid(dymax, 0.0, 1.0);
		
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY,
				alignmentModel.getReference(), sdAlgo));
		
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS,
				aliView.getReference(), new Integer(100)));
		return null;
	}
	
	private void resizeGrid(int message, int d) {
		double pixelWidth = alignmentModel.getPixelWidth();
		double pixelHeight = alignmentModel.getPixelHeight();
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, message, alignmentModel.getReference(),
				new Double(d), new Double(pixelWidth), new Double(pixelHeight)));
	}
	
	private void translateGrid(int n, double xTranslate, double yTranslate) {
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, alignmentModel.getReference());
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		double spotWidth = alignmentModel.getGridModel().getRootBlock().getSpotWidth();
		double d = spotWidth * n;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE,
				alignmentModel.getReference(), new Double(d * xTranslate), new Double(d * yTranslate)));
	}
	public void finished() {
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		TGrid grid = (TGrid)((TAlignmentModel)gaAlgo.getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getGridModel().getReference();
		((TAlignment)aliView.getReference()).setAlgoRunning(false);
		((TAlignment)aliView.getReference()).setAlgorithm(null);
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, aliView.getReference());
		TEventHandler.handleMessage(ev);
		aliView.getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		if (!STOP) {
			uefga.addFinalPosition();
			ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignmentModel.getReference(), uefga);
			TEventHandler.handleMessage(ev);
		}
		else {
			grid.getGridControler().undo(uefga);
		}
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, aliView.getReference());
		TEventHandler.handleMessage(ev);
		alignmentModel.getReference().getView().getTablePanel().setSelectable(true);
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
