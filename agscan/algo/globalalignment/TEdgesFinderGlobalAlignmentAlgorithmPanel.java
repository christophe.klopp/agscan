package agscan.algo.globalalignment;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;
import agscan.algo.factory.TSpotDetectionAlgorithmFactory;
import agscan.algo.spotdetection.TMultiChannelSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TTwoProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;

public class TEdgesFinderGlobalAlignmentAlgorithmPanel extends TAlgorithmControlPanel implements ActionListener {
  private static TEdgesFinderGlobalAlignmentAlgorithmPanel instance = null;
  private JSpinner sizeSearchZoneSpinner, searchAngleSpinner, sizeSearchZoneIncrementSpinner, edgesSearchZoneSpinner,
      contrastPercentSpinner, searchAngleIncrementSpinner;
  private JComboBox spotDetectionAlgorithmComboBox = new JComboBox();
  private TAlgorithmControlPanel spotDetectionControlPanel;
  private TEdgesFinderGlobalAlignmentAlgorithm alg;

  protected TEdgesFinderGlobalAlignmentAlgorithmPanel(TEdgesFinderGlobalAlignmentAlgorithm algo) {
    spotDetectionControlPanel = algo.getSpotDetectionAlgorithm().getControlPanel();
    spotDetectionAlgorithmComboBox.addItem(TTwoProfilesSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.addItem(TFourProfilesSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.addItem(TMultiChannelSpotDetectionAlgorithm.NAME);
    spotDetectionAlgorithmComboBox.addActionListener(this);
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEtchedBorder());
    sizeSearchZoneSpinner = new JSpinner(new SpinnerNumberModel(0.5, 0.5, 5.0, 0.5));
    sizeSearchZoneSpinner.setPreferredSize(new Dimension(50, sizeSearchZoneSpinner.getMinimumSize().height));
    searchAngleSpinner = new JSpinner(new SpinnerNumberModel(2, 2, 30, 1));
    searchAngleSpinner.setPreferredSize(new Dimension(50, searchAngleSpinner.getMinimumSize().height));
    sizeSearchZoneIncrementSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 5, 1));
    sizeSearchZoneIncrementSpinner.setPreferredSize(new Dimension(50, sizeSearchZoneIncrementSpinner.getMinimumSize().height));
    edgesSearchZoneSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
    edgesSearchZoneSpinner.setPreferredSize(new Dimension(50, edgesSearchZoneSpinner.getMinimumSize().height));
    contrastPercentSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));
    contrastPercentSpinner.setPreferredSize(new Dimension(50, contrastPercentSpinner.getMinimumSize().height));
    searchAngleIncrementSpinner = new JSpinner(new SpinnerNumberModel(0.01, 0.01, 0.5, 0.01));
    searchAngleIncrementSpinner.setPreferredSize(new Dimension(50, searchAngleIncrementSpinner.getMinimumSize().height));

    jbInit();
    alg = algo;
  }
  private void jbInit() {
    removeAll();
    JLabel spotDetectionAlgorithmLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.0")); //$NON-NLS-1$
    JLabel searchAngleLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.1")); //$NON-NLS-1$
    JLabel sizeSearchZoneLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.2")); //$NON-NLS-1$
    JLabel sizeSearchZoneIncrementLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.3")); //$NON-NLS-1$
    JLabel edgesSearchZoneLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.4")); //$NON-NLS-1$
    JLabel contrastPercentLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.5")); //$NON-NLS-1$
    JLabel searchAngleIncrementLabel = new JLabel(Messages.getString("TEdgesFinderGlobalAlignmentAlgorithmPanel.6")); //$NON-NLS-1$

    add(spotDetectionAlgorithmLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    add(spotDetectionAlgorithmComboBox, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 0, 0));
    add(spotDetectionControlPanel, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
    add(searchAngleLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(searchAngleSpinner, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(searchAngleIncrementLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(searchAngleIncrementSpinner, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(contrastPercentLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(contrastPercentSpinner, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(sizeSearchZoneLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(sizeSearchZoneSpinner, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(sizeSearchZoneIncrementLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    add(sizeSearchZoneIncrementSpinner, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    add(edgesSearchZoneLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0), 0, 0));
    add(edgesSearchZoneSpinner, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 10), 0, 0));
  }
  public void actionPerformed(ActionEvent event) {
    TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
    TSpotDetectionAlgorithm algo = (TSpotDetectionAlgorithm)factory.createAlgorithm(spotDetectionAlgorithmComboBox.getSelectedItem(), true);
    algo.initWithLast();
    spotDetectionControlPanel = algo.getControlPanel();
    jbInit();
    //init(alg);
    doLayout();
    algo.getControlPanel().doLayout();
    repaint();
  }
  public void init(TAlgorithm algo) {
    alg = (TEdgesFinderGlobalAlignmentAlgorithm)algo;
    spotDetectionControlPanel = alg.getSpotDetectionAlgorithm().getControlPanel();
    spotDetectionControlPanel.init(alg.getSpotDetectionAlgorithm());
    add(spotDetectionControlPanel, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
    sizeSearchZoneSpinner.setValue((Double)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ZONE));
    sizeSearchZoneIncrementSpinner.setValue((Integer)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ZONE_INCREMENT));
    edgesSearchZoneSpinner.setValue((Integer)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.EDGES_SEARCH_ZONE));
    searchAngleSpinner.setValue((Integer)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ANGLE));
    contrastPercentSpinner.setValue((Integer)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.CONTRAST_PERCENT));
    searchAngleIncrementSpinner.setValue((Double)alg.getParameter(TEdgesFinderGlobalAlignmentAlgorithm.SEARCH_ANGLE_INCREMENT));
    spotDetectionAlgorithmComboBox.setSelectedItem(alg.getSpotDetectionAlgorithm().getName());
  }
  public double getSizeSearchZone() {
    return ((Double)sizeSearchZoneSpinner.getValue()).doubleValue();
  }
  public int getSizeSearchZoneIncrement() {
    return ((Integer)sizeSearchZoneIncrementSpinner.getValue()).intValue();
  }
  public int getEdgesSearchZone() {
    return ((Integer)edgesSearchZoneSpinner.getValue()).intValue();
  }
  public int getSearchAngle() {
    return ((Integer)searchAngleSpinner.getValue()).intValue();
  }
  public int getContrastPercent() {
    return ((Integer)contrastPercentSpinner.getValue()).intValue();
  }
  public double getSearchAngleIncrement() {
    return ((Double)searchAngleIncrementSpinner.getValue()).doubleValue();
  }
  public String getSpotDetectionAlgorithmName() {
    return (String)spotDetectionAlgorithmComboBox.getSelectedItem();
  }
  public TAlgorithmControlPanel getSpotDetectionAlgorithmControlPanel() {
    return spotDetectionControlPanel;
  }
  public static TEdgesFinderGlobalAlignmentAlgorithmPanel getInstance(TEdgesFinderGlobalAlignmentAlgorithm algo) {
    if (instance == null)
      instance = new TEdgesFinderGlobalAlignmentAlgorithmPanel(algo);
    else
      instance.jbInit();
    instance.init(algo);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2005 INRA - SIGENAE TEAM
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
