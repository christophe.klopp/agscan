package agscan.algo.globalalignment;



import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.measure.ResultsTable;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Polygon;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JOptionPane;
import plugins.ImageTools;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TUndoSnifferGlobalAlignment;
import agscan.data.controler.memento.TUndoTemplateMatchingGlobalAlignment;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.grid.TGrid;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;

public class TTemplateMatchingGlobalAlignmentWorker extends SwingWorker {
	private TTemplateMatchingGlobalAlignmentAlgorithm gaAlgo;

	private TUndoTemplateMatchingGlobalAlignment uefga;

	private TAlignmentModel alignmentModel;

	private Color[] color = new Color[5];

	private int progressRange;
	
	private static double pixelH; //height of a pixel in µm
	private static double pixelW; //width of a pixel in µm
	private static int nb_spot;//number of spots found
	private static TAlignment alignment;//current alignment
	
	public TTemplateMatchingGlobalAlignmentWorker(
			TGlobalAlignmentAlgorithm gaAlgo) {
		this.gaAlgo = (TTemplateMatchingGlobalAlignmentAlgorithm) gaAlgo;
		color[0] = Color.cyan;
		color[1] = Color.yellow;
		color[2] = Color.magenta;
		color[3] = Color.green;
		color[4] = Color.pink;
	}

	public Object construct(boolean thread) {
		alignmentModel = (TAlignmentModel) gaAlgo
				.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL);
		alignment = (TAlignment) alignmentModel.getReference();

		alignment.getView().getTablePanel().setSelectable(false);
		TGrid grid = (TGrid) alignmentModel.getGridModel().getReference();
		double diameter = grid.getGridModel().getSpot(0).getInitDiameter();
		double xRes = ((Double) gaAlgo
				.getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double) gaAlgo
				.getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		progressRange = gaAlgo.getEndProgress() - gaAlgo.getStartProgress();
		// Selection of the entire grid
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TGridSelectionControler.UNSELECT_ALL, alignment));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TGridSelectionControler.SELECT_ALL, alignment));

		TAlignmentView aliView = (TAlignmentView) ((TAlignmentModel) gaAlgo
				.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL))
				.getReference().getView();
		alignment.setAlgoRunning(true);
		alignment.setAlgorithm(gaAlgo);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR,
				TStatusBar.UPDATE, alignment));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));
		aliView.getGraphicPanel().removeInteractors();
		TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER,
				TMenuManager.UPDATE, null));

		pixelH = alignment.getGridModel().getPixelHeight();
		pixelW = alignment.getGridModel().getPixelWidth();

		
		
		
		/*
		 * The next part : - rotates the image (using the angle) - searches the
		 * spots using a template - gets the lists of coordinates of the spot
		 * centers - searches the best position for the grid meaning the
		 * position in which most of the spots are in the grid blocs - fits the
		 * grid to the image
		 */

		/*
		 * The template matching process reduces the image from the kernel size
		 * In order to have the right coordinates at the end for the spots we
		 * are first going to build a large image
		 */
		TImageModel imodel = alignment.getImage().getImageModel();
		TGridModel gmodel = alignment.getGridModel();
		int nbImages = imodel.getNumberOfChannels();
		int pixelWidth = (int) imodel.getPixelWidth(); // width of one pixel in
														// micrometers( for
														// example 25)
		int pixelHeight = (int) imodel.getPixelHeight(); // height of one
															// pixel in
															// micrometers( for
															// example 25)

		// generate a kernel
		double spotSurface = (gmodel.getSpot(1).getWidth() / pixelWidth)
				* (gmodel.getSpot(1).getHeight() / pixelHeight);
		float[][] kernel ;
		int spotDiameter = (int) ((gmodel.getConfig().getSpotsDiam() / pixelWidth));
		int spotSize = (int) ((gmodel.getConfig().getSpotsWidth() / pixelWidth));
		System.out.println("global spotDiameter "+spotDiameter);
		System.out.println("global spotSize "+spotSize);
		
		//case if size is odd
		if(spotSize % 2 == 0)
			spotSize++;
		
		//selection of the kind ok kernel
		int kindKern = (Integer) gaAlgo.getParameter(gaAlgo.KIND_KERNEL);
		switch(kindKern){
			case 1 :{
				kernel = alignment.calcGaussAGScanKernel(spotDiameter,spotSize);
				System.out.println("kernel gaussien choisi ");
				break;
			}
			case 2 :{
				kernel = alignment.calcCircAGScanKernel(spotDiameter,spotSize);
				System.out.println("kernel circulaire choisi");
				break;
			}
			case 3 :{
				kernel = alignment.calcConicAGScanKernel(spotDiameter,spotSize);
				System.out.println("kernel conique choisi ");
				
				
				break;
			}
			default : {
				kernel = alignment.calcCircAGScanKernel(spotDiameter,spotSize);
				break;
			}
			
		
		}


		int imageWidth = (int) (imodel.getImageWidth() / imodel.getPixelWidth());
		int imageHeight = (int) (imodel.getImageHeight() / imodel
				.getPixelWidth());
		int[] imageMax = imodel.getMaxImageData();
		int halfKernelSize = (int) (kernel.length / 2);

		 ImageProcessor maxProc = new FloatProcessor(imageWidth, imageHeight,
		 imageMax);
		 maxProc.invert();
		 ImagePlus maximp = new ImagePlus("Image max", maxProc);
		// FileSaver fs = new FileSaver(maximp);
		// fs.saveAsTiff("/tmp/toto.tif");
		// maximp.show();

		double background = getBackground(imageWidth, imageMax);

		System.out.println("imageMax.length " + imageMax.length
				+ " imageWidth " + imageWidth + " imageHeight " + imageHeight
				+ " kernel.length " + kernel.length);
		float[] spotData = new float[(imageWidth + kernel.length)
				* (imageHeight + kernel.length)];
		// float[][] origimg = new
		// float[imageWidth+kernel.length][imageHeight+kernel.length];

		for (int j = 0; j < (imageWidth + kernel.length); j++) {
			for (int k = 0; k < (imageHeight + kernel.length); k++) {
				if (j <= halfKernelSize
						|| j >= (imageWidth + kernel.length - halfKernelSize - 1)
						|| k <= halfKernelSize
						|| k >= (imageHeight + kernel.length - halfKernelSize - 1)) {
					// origimg[j][k] = 32768;
					spotData[k * (imageWidth + kernel.length) + j] = (float) background;
				} else {
					// origimg[j][k] = imageMax[(k-halfKernelSize)*imageWidth+ j
					// - halfKernelSize -1];
					spotData[k * (imageWidth + kernel.length) + j] = imageMax[(k - halfKernelSize)
							* imageWidth + j - halfKernelSize - 1];
					// spotData[k*(imageWidth+kernel.length)+ j] = 0;
				}
			}
		}

		pc_done = 500 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		FloatProcessor ipSpot = new FloatProcessor(imageWidth + kernel.length,
				imageHeight + kernel.length, spotData, null);
		float[][] origimg = new float[ipSpot.getWidth()][ipSpot.getHeight()];

		for (int j = 0; j < ipSpot.getWidth(); j++) {
			for (int k = 0; k < ipSpot.getHeight(); k++) {
				origimg[j][k] = ipSpot.getPixelValue(j, k);
			}
		}
		/*
		 * Getting the correlation image
		 */
		pc_done = 1000 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		float[][] corrimg = null;
		corrimg = new float[imageWidth + 1][imageHeight + 1];
		
		corrimg = ImageTools.statsCorrelation(origimg, kernel);
		System.out.println("Correlation Image Hight " + corrimg.length
				+ " imageWidth " + corrimg[0].length + " kernel.length "
				+ kernel.length);
		float[] outData = new float[corrimg.length * corrimg[0].length];
		for (int k = 0; k < corrimg.length; k++) {
			for (int p = 0; p < corrimg[0].length; p++) {
				outData[p * corrimg.length + k] = (corrimg[k][p] * 32167) + 32167;
			}
		}
		// System.out.println(" corrimg "+corrimg.length+" "+corrimg[0].length);
		pc_done = 1500 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		ImageProcessor outSpot = new FloatProcessor(corrimg.length,
				corrimg[0].length, outData, null);
		ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);

		// outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();
		 //outimpSpot.show();

		/*
		 * Angle search
		 */

		ij.process.ImageProcessor outipSpot2 = outimpSpot.getProcessor()
				.duplicate();
		ImagePlus outimpSpot2 = new ImagePlus("extended Image", outipSpot2);

		double newAngle = getAngle(alignment, -1
				* (Integer) gaAlgo.getParameter(gaAlgo.SEARCH_ANGLE),
				(Integer) gaAlgo.getParameter(gaAlgo.SEARCH_ANGLE), 1,
				outimpSpot2);
		// System.out.println("Second angle calculation 1 "+newAngle);

		newAngle = getAngle(alignment, newAngle - 1.1, newAngle + 1.1, 0.1,
				outimpSpot2);
		// System.out.println("Second angle calculation 2 "+newAngle);

		newAngle = getAngle(alignment, newAngle - 0.11, newAngle + 0.11, 0.01,
				outimpSpot2);
		System.out.println("Calculated angle = " + newAngle);

		outSpot.setInterpolate(true);
		outSpot.rotate(newAngle);
		//angle_rad = newAngle / 360 * 2 * Math.PI;
		pc_done = 6500 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		outimpSpot = new ImagePlus("correlation rotated Image", outSpot);
		System.out.println("rotated image length " + outSpot.getHeight()
				+ " imageWidth " + outSpot.getWidth() + " kernel.length "
				+ kernel.length);



		/*
		 * Searching the spot coordinates
		 */
		outSpot.invertLut(); // invert the correlation image
		outSpot.autoThreshold();
		
		ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();

		
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		double partSize = (spotDiameter/(2))*(spotDiameter/(2))*Math.PI*2.0D;
		ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,8192+255, myrt, partSize*b_min, partSize*b_max);
		//ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,8192+64+1, myrt, 0, Double.POSITIVE_INFINITY);
		mypart.analyze(outimpSpot);

		pc_done = 6000 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		/*
		 * Storing the result point of the template Matching operation
		 */

		double[] resultPointsX = new double[myrt.getCounter() + 1];
		double[] resultPointsY = new double[myrt.getCounter() + 1];
		double[] resultPointsXRotated = new double[myrt.getCounter() + 1];
		double[] resultPointsYRotated = new double[myrt.getCounter() + 1];
		double[] resultPointsCircularity = new double[myrt.getCounter() + 1];
		//int x1, y1, x2, y2;

		System.out.println(" Number of spots found = " + myrt.getCounter());
		//nb_spot = myrt.getCounter();
		for (int o = 0; o < myrt.getCounter(); o++) {
			// System.out.print(o+" : X =
			// "+myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[o]);
			// System.out.println(" Y =
			// "+myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[o]);
			resultPointsX[o] = myrt
					.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o];
			resultPointsY[o] = myrt
					.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o];
			resultPointsCircularity[o] = myrt
					.getColumn(ij.measure.ResultsTable.CIRCULARITY)[o];

			resultPointsXRotated[o] = imageWidth
					/ 2
					+ (Math.cos(-newAngle / 360 * 2 * Math.PI) * (resultPointsX[o] - imageWidth / 2))
					- (Math.sin(-newAngle / 360 * 2 * Math.PI) * (resultPointsY[o] - imageHeight / 2));
			resultPointsYRotated[o] = imageHeight
					/ 2
					+ (Math.sin(-newAngle / 360 * 2 * Math.PI) * (resultPointsX[o] - imageWidth / 2))
					+ (Math.cos(-newAngle / 360 * 2 * Math.PI) * (resultPointsY[o] - imageHeight / 2));

			// if (resultPointsX[o]< 450){
			// System.out.print(" X = "+resultPointsX[o]+"
			// "+resultPointsXRotated[o]);
			// System.out.println(" Y = "+resultPointsY[o]+"
			// "+resultPointsYRotated[o]);
			// }

			/*
			 * Presenting the spot centers on the original image
			 */
			
//			if ((resultPointsCircularity[o] > (float) ((Integer) gaAlgo
//					.getParameter(gaAlgo.SPOT_CIRCULARITY)) / 100)
//					&& ((Integer) gaAlgo.getParameter(gaAlgo.GRID_CENTER_CROSS) == 1)) {
//				x1 = (int) (resultPointsXRotated[o]) - 2;
//				y1 = (int) (resultPointsYRotated[o]);
//				x2 = (int) (resultPointsXRotated[o]) + 2;
//				((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
//						.addHotLine(new TImageGraphicPanel.HotLine(color[2],
//								x1, y1, x2, y1));
//				x1 = (int) (resultPointsXRotated[o]);
//				y1 = (int) (resultPointsYRotated[o]) - 2;
//				y2 = (int) (resultPointsYRotated[o]) + 2;
//				((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
//						.addHotLine(new TImageGraphicPanel.HotLine(color[2],
//								x1, y1, x1, y2));
//			}
		}

		/*
		 * Searching the location of the grid - creating the grid projection -
		 * seaching the possible cases in X and Y
		 */

		pc_done = 8000 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer((int) (pc_done
						* progressRange / 100.0D)
						+ gaAlgo.getStartProgress())));

		if (alignment.isInBatch())
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TBatchControler.REFRESH_VIEW, alignment.getBatch()));

		double[] startXGrid = new double[gmodel.getConfig().getLev2NbCols()];
		double[] endXGrid = new double[gmodel.getConfig().getLev2NbCols()];
		double[] startYGrid = new double[gmodel.getConfig().getLev2NbRows()];
		double[] endYGrid = new double[gmodel.getConfig().getLev2NbRows()];

		int sbin =2;
		
		for (int i = 0; i < startXGrid.length; i++) {
			startXGrid[i] = (int) (i
					* (gmodel.getConfig().getSpotsWidth()
							* gmodel.getConfig().getLev1NbCols() + gmodel
							.getConfig().getLev2XSpacing()) / pixelWidth);
			endXGrid[i] = (int) (startXGrid[i] + sbin + gmodel.getConfig()
					.getSpotsWidth()
					* (gmodel.getConfig().getLev1NbCols()-1) / pixelWidth);
			 //System.out.println("X Grid coords = "+startXGrid[i]+"			 "+endXGrid[i]);
		}

		for (int i = 0; i < startYGrid.length; i++) {
			startYGrid[i] = (int) (i
					* (gmodel.getConfig().getSpotsHeight()
							* gmodel.getConfig().getLev1NbRows() + gmodel
							.getConfig().getLev2YSpacing()) / pixelHeight);
			endYGrid[i] = (int) (startYGrid[i] + sbin + gmodel.getConfig()
					.getSpotsHeight()
					* (gmodel.getConfig().getLev1NbRows()-1) / pixelHeight) ;
			//System.out.println("Y Grid coords = "+startYGrid[i]+"  "+endYGrid[i]);
		}

		/*
		 * Checking the number of points i the grid in each position
		 */

		double bestXposition = 0;
		int bestXscore = 0;
		int count;

		for (int i = 0; i < imageWidth - endXGrid[startXGrid.length - 1]; i++) {
			count = 0;
			for (int o = 0; o < myrt.getCounter(); o++) {
				for (int j = 0; j < startXGrid.length; j++) {
					// if
					// (((resultPointsX[o]+(gmodel.getConfig().getSpotsDiam()/pixelWidth)/2)>startXGrid[j]+i)
					// && ((resultPointsX[o]
					// -(gmodel.getConfig().getSpotsDiam()/pixelWidth)/2)<endXGrid[j]+i)){
					if ((resultPointsX[o] > startXGrid[j] + i)
							&& (resultPointsX[o] < endXGrid[j] + i)) {
						count++;
					}
					// System.out.println("i,o,j"+i+" "+o+" "+j);
					// System.out.println(resultPointsX[o]+" "+startXGrid[j]+"
					// "+resultPointsX[o]+" "+endXGrid[j]);
				}
			}
			if (count > bestXscore) {
				bestXposition = i;
				bestXscore = count;
			}
		}

		double bestYposition = 0;
		int bestYscore = 0;

		for (int i = 0; i < imageHeight - endYGrid[startYGrid.length - 1]; i++) {
			count = 0;
			for (int o = 0; o < myrt.getCounter(); o++) {
				for (int j = 0; j < startYGrid.length; j++) {
					// if
					// (((resultPointsY[o]+(gmodel.getConfig().getSpotsDiam()/pixelHeight)/2)>startYGrid[j]+i)
					// &&
					// ((resultPointsY[o]-(gmodel.getConfig().getSpotsDiam()/pixelHeight)/2)<endYGrid[j]+i)){
					if ((resultPointsY[o] > startYGrid[j] + i)
							&& (resultPointsY[o] < endYGrid[j] + i)) {
						count++;
					}
				}
			}
			if (count > bestYscore) {
				bestYposition = i;
				bestYscore = count;
			}
		}
		
		//calcul de la meilleure position en prenant en compte le centre de rotation
		//de la grille qui est le centre du premier spot
		
		//bestXposition = bestXposition /*- (gmodel.getSpot(1).getWidth()*/ / (2*pixelWidth)/*)*/;
		//bestYposition = bestYposition /*+ (gmodel.getSpot(1).getHeight()*/ / (2*pixelHeight)/*)*/;
		
		System.out.println("bestXposition = " + bestXposition + " "
				+ bestXscore);
		System.out.println("bestYposition = " + bestYposition + " "
				+ bestYscore);

		double newBestXposition = imageWidth
				/ 2
				+ (Math.cos(-newAngle / 360 * 2 * Math.PI) * (bestXposition - imageWidth / 2))
				- (Math.sin(-newAngle / 360 * 2 * Math.PI) * (bestYposition - imageHeight / 2));

		double newBestYposition = imageHeight
				/ 2
				+ (Math.sin(-newAngle / 360 * 2 * Math.PI) * (bestXposition - imageWidth / 2))
				+ (Math.cos(-newAngle / 360 * 2 * Math.PI) * (bestYposition - imageHeight / 2));

		System.out.println("angle = " + (newAngle / 360 * 2 * Math.PI));
		System.out.println("new bestXposition = " + newBestXposition);
		System.out.println("new bestYposition = " + newBestYposition);

		// bestXposition = bestXposition +
		// (gmodel.getConfig().getSpotsWidth()/pixelWidth)/2;
		// bestYposition = bestYposition -
		// (gmodel.getConfig().getSpotsHeight()/pixelHeight)/2;
		
		double deca_X = newBestXposition - bestXposition;
		double deca_Y = newBestYposition - bestYposition;
		
		System.out.println("deca_X global "+deca_X);
		System.out.println("deca_Y global "+deca_Y);
		
		alignment.getGridModel().getConfig().setDeca_X(deca_X);
		alignment.getGridModel().getConfig().setDeca_Y(deca_Y);
		
		
		bestXposition = newBestXposition
				- (gmodel.getConfig().getSpotsDiam() / pixelWidth) / 2;
		bestYposition = newBestYposition
				- (gmodel.getConfig().getSpotsDiam() / pixelHeight) / 2;

		//bestXposition = newBestXposition;
		//bestYposition = newBestYposition;
		
		
		
		
		//if ((Integer) gaAlgo.getParameter(gaAlgo.GRID_CENTER_CROSS) != 1) {
			// TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
			// TGridPositionControler.TRANSLATE, null,
			// new Double(bestXposition*pixelWidth-diameter/2),new
			// Double(bestYposition*pixelHeight+diameter/2));
			
			
			//-------/
			TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
					TGridPositionControler.TRANSLATE, null, new Double(
							bestXposition * pixelWidth), new Double(
							bestYposition * pixelHeight));
			//---------/
			// new
			// Double(bestXposition-(gmodel.getConfig().getSpotsWidth()/pixelWidth)/2),new
			// Double(bestYposition-(gmodel.getConfig().getSpotsWidth()/pixelHeight)/2));
			//---------/
			TEventHandler.handleMessage(ev);
			
			//--------
			// TEvent ev3 = new TEvent(TEventHandler.MAIN_PANE,
			// TMainPane.REPAINT_VIEW, null);
			// TEventHandler.handleMessage(ev3);
			// double radian_angle = angle*(Math.PI / 180.0D);
			double radian_angle = Math.PI * (-newAngle) / 180.0D;
			TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER,
					TGridPositionControler.ROTATE, null, new Double(
							radian_angle));
			TEventHandler.handleMessage(ev2);
			
			
			
			TEvent ev1 = new TEvent(TEventHandler.MAIN_PANE,
					TMainPane.REPAINT_VIEW, null);
			TEventHandler.handleMessage(ev1);
		//}
		pc_done = 10000 / progressRange;
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
				TStatusBar.ALGO_PROGRESS, alignment, new Integer(
						(int) (pc_done * progressRange / 100.0D)
								+ gaAlgo.getStartProgress())));
		
		if (alignment.isInBatch())
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TBatchControler.REFRESH_VIEW, alignment.getBatch()));

//		//test Alexis
		outimpSpot.setTitle(outimpSpot.getTitle()+" global");
		//outimpSpot.show();
		double angle_radian = -alignment.getGridModel().getAngle();
		System.out.println(" angle "+(angle_radian * 180.0D/Math.PI));
		
		//rotation de la grille
		TEvent ev12 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.ROTATE, null, new Double(
						angle_radian));
		TEventHandler.handleMessage(ev12);

		
		
		//les elements de translation ont été calculé lors de la première translation de la grille
		TEvent ev6 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.TRANSLATE, null,-(deca_X*pixelW), -(deca_Y*pixelH));
		TEventHandler.handleMessage(ev6);

		//traitement
		
		SizeCorrection(alignment,outimpSpot);
		//fin traitement


		//on remet la grille a l'orientation d'origine
		TEvent ev3 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.ROTATE, null, new Double(
						-angle_radian));
		TEventHandler.handleMessage(ev3);

		//translation inverse
		TEvent ev7 = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.TRANSLATE, null,(deca_X*pixelW), (deca_Y*pixelH));
		TEventHandler.handleMessage(ev7);
//		
		return null;
	}
	
	
	
	private double getBackground(int imageWidth, int[] imageMax) {
		// background of the first 2 lines
		double bachground = 0;
		for (int j = 0; j < imageWidth * 2; j++) {
			bachground = bachground + imageMax[j];
		}
		return bachground / (imageWidth * 2);
	}

	public void finished() {
		TAlignmentView aliView = (TAlignmentView) ((TAlignmentModel) gaAlgo
				.getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL))
				.getReference().getView();
		((TAlignment) aliView.getReference()).setAlgoRunning(false);
		((TAlignment) aliView.getReference()).setAlgorithm(null);
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE,
				aliView.getReference());
		TEventHandler.handleMessage(ev);
		aliView.getGraphicPanel().setCurrentInteractor(
				TGraphicInteractor.ALIGNMENT);
		if (!STOP) {
			// uefga.addFinalPosition();
			ev = new TEvent(TEventHandler.DATA_MANAGER,
					TGridControler.ADD_ACTION_MEMENTO, alignmentModel
							.getReference(), uefga);
			TEventHandler.handleMessage(ev);
		} else {
			// grid.getGridCpublicontroler().undo(uefga);
		}
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE,
				aliView.getReference());
		TEventHandler.handleMessage(ev);
		alignmentModel.getReference().getView().getTablePanel().setSelectable(
				true);
	}

	public  double getAngle(TAlignment alignment, double start, double stop,
			double pace, ImagePlus imp2) {
		double maxStdDev = 0;
		double angle = 0.0;
		int nbloop = (int) ((stop - start) / pace) + 1;
		IJ.log("start stop pace = " + start + " " + stop + " " + pace + " \n");
		double[] stdDev = new double[nbloop + 1];
		IJ.log("nb loops = " + nbloop);
		ij.process.ImageProcessor ip3 = imp2.getProcessor().duplicate()
				.convertToByte(true);
		// ip3.autoThreshold();
		ip3.setInterpolate(true);
		ip3.threshold(200);
		ip3.invertLut();
		ip3.rotate(start - pace);
		ij.ImagePlus imp3 = new ImagePlus("Image used to retrive the angle"
				+ (start - pace), ip3);
		// imp3.show();
		for (int k = 0; k < nbloop; k++) {
			/*
			 * Progress bar update
			 */
			pc_done = pc_done + 1;
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,
					TStatusBar.ALGO_PROGRESS, alignment, new Integer(
							(int) (pc_done * progressRange / 100.0D)
									+ gaAlgo.getStartProgress())));

			ip3.setInterpolate(true);
			ip3.rotate(pace);
			imp3 = new ImagePlus("Image used to retrive the angle" + k, ip3);
			imp3.setRoi(1, 1, imp3.getWidth(), imp3.getHeight());
			// imp3.show();
			ij.gui.ProfilePlot myplot = new ij.gui.ProfilePlot(imp3);
			// myplot.createWindow();
			double[] mytable = myplot.getProfile();

			double total = 0;
			for (int l = 1; l < imp3.getWidth() - 1; l++) {
				total = total + mytable[l];
			}
			total = total / (imp3.getWidth() - 1);
			double variance = 0;
			for (int l = 1; l < imp3.getWidth() - 1; l++) {
				variance = variance + (total - mytable[l])
						* (total - mytable[l]);
			}
			// System.out.println("n = "+k +" Std Dev = "+variance+" \n");
			stdDev[k] = variance;
		}
		
		//int idemincator = 0; // checking if several angles give the same standard deviation  
		for (int k = 0; k < nbloop; k++) {
			//System.out.println("k = "+k +" Std Dev = "+stdDev[k]);
			// "+minStdDev);
			if (maxStdDev < stdDev[k]) {
				maxStdDev = stdDev[k];
				angle = start + k * pace;
				//idemincator = 1;
			//}else if (maxStdDev == stdDev[k]){
			//	idemincator++;
			//	angle = angle + start + k * pace;
			//	System.out.println("k = "+idemincator+" sum of angles = "+angle+" \n");
			}
		}
		//if (idemincator > 1){
		//	angle = angle/idemincator;
		//}
		// IJ.log("Calculated angle: "+angle);
		// angle = angle*(360/60);
		return angle;
	}
	
	
	

	
	
	

	//--------------------Outil de verification visuelle sur l'image ou AGSCAN
	
	/**
	 * @author ajulin
	 * @param ip : current image Processor
	 * @param c : color for draw
	 * 
	 * Draw the current grid on an ImagePlus
	 */
	private static void drawIJGrid(ImageProcessor ip, Color c){
		int nb_bloc_row = alignment.getGridModel().getConfig().getLev2NbCols();
		int nb_bloc_col =  alignment.getGridModel().getConfig().getLev2NbRows();
		
		int nb_spot_row = alignment.getGridModel().getConfig().getLev1NbCols();
		int nb_spot_col =  alignment.getGridModel().getConfig().getLev1NbRows();
		
		int nb_spot_bloc = nb_spot_row * nb_spot_col;
		
		Vector v = alignment.getGridModel().getSpots();
		TSpot sp1 = (TSpot)v.get(0);
		
		TSpot sp2 = (TSpot)v.get((nb_bloc_row-1) * nb_spot_bloc + nb_spot_row - 1); 
		
		TSpot sp3 = (TSpot)v.get(nb_bloc_row * nb_spot_bloc * (nb_bloc_col-1)- nb_spot_row + nb_spot_bloc );
		
		TSpot sp4 = (TSpot)v.get(v.size()-1);
		 ip.setColor(c);
		
		Polygon p = new Polygon();
		p.addPoint((int)(sp1.getXCenter()/pixelW), (int)(sp1.getYCenter()/pixelH));
		p.addPoint((int)(sp2.getXCenter()/pixelW), (int)(sp2.getYCenter()/pixelH));
		p.addPoint((int)(sp4.getXCenter()/pixelW), (int)(sp4.getYCenter()/pixelH));
		p.addPoint((int)(sp3.getXCenter()/pixelW), (int)(sp3.getYCenter()/pixelH));
		
		ip.drawPolygon(p);
		
		
	}

	/**
	 * @author ajulin
	 * @param ip : current image Processor
	 * @param xcenter : X coordonate in pixel
	 * @param ycenter : Y coordonate in pixel
	 * @param c : color for draw
	 * 
	 * Draw a cross on the current grid at the position( xcenter, ycenter )on an ImagePlus
	 */
	private static void drawIJ_Cross(ImageProcessor ip, int xcenter, int ycenter, Color c){
		ip.setColor(c);
		ip.drawLine(xcenter-2,ycenter , xcenter+2, ycenter);
		ip.drawLine(xcenter,ycenter-2 , xcenter, ycenter+2);
		
	}
	/**
	 * @author ajulin
	 * @param c : color for draw
	 * 
	 * Draw a grid on AGScan view
	 */
	private static void plotGrid(Color c){
		
		
		int nb_bloc_row = alignment.getGridModel().getConfig().getLev2NbCols();
		int nb_bloc_col =  alignment.getGridModel().getConfig().getLev2NbRows();
		
		int nb_spot_row = alignment.getGridModel().getConfig().getLev1NbCols();
		int nb_spot_col =  alignment.getGridModel().getConfig().getLev1NbRows();
		
		int nb_spot_bloc = nb_spot_row * nb_spot_col;
		
		Vector v = alignment.getGridModel().getSpots();
		TSpot sp1 = (TSpot)v.get(0);
		
		TSpot sp2 = (TSpot)v.get((nb_bloc_row-1) * nb_spot_bloc + nb_spot_row - 1); 
		
		TSpot sp3 = (TSpot)v.get(nb_bloc_row * nb_spot_bloc * (nb_bloc_col-1)- nb_spot_row + nb_spot_bloc );
		
		TSpot sp4 = (TSpot)v.get(v.size()-1);
		
		
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(sp1.getXCenter()/pixelW), (int)(sp1.getYCenter()/pixelH),
				(int)(sp2.getXCenter()/pixelW), (int)(sp2.getYCenter()/pixelH)));
		
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(sp1.getXCenter()/pixelW), (int)(sp1.getYCenter()/pixelH),
				(int)(sp3.getXCenter()/pixelW), (int)(sp3.getYCenter()/pixelH)));
		
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(sp4.getXCenter()/pixelW), (int)(sp4.getYCenter()/pixelH),
				(int)(sp2.getXCenter()/pixelW), (int)(sp2.getYCenter()/pixelH)));
		
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(c,
				(int)(sp3.getXCenter()/pixelW), (int)(sp3.getYCenter()/pixelH),
				(int)(sp4.getXCenter()/pixelW), (int)(sp4.getYCenter()/pixelH)));
		
		
		
		
	}
	
	/**
	 * @author ajulin
	 * @param c : color for draw
	 * 
	 * Draw a grid's center on AGScan view
	 */
	private static void plotCentreGrid(Color c){
		Vector v = alignment.getGridModel().getSpots();
		TSpot sp1 = (TSpot)v.get(0);
		TSpot sp2 = (TSpot)v.get(v.size()-1);
		
		//calcul du milieu de la grille apres la rotation
		double x_mil_or = (sp2.getXCenter() - sp1.getXCenter())/2.0D + sp1.getXCenter();
		double y_mil_or = (sp2.getYCenter() - sp1.getYCenter())/2.0D + sp1.getYCenter();
		plot_cross((int)(x_mil_or/pixelW), (int)(y_mil_or/pixelH), c);
		
	}
	
	/**
	 * @author ajulin
	 * @param x coordonate in pixel
	 * @param y coordonate in pixel
	 * @param color color for draw
	 * 
	 * Draw a cross a the (x,y) position
	 */
	private static void plot_cross(int x, int y, Color color){
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x-2, y, x+2, y));
		
		((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
		.addHotLine(new TImageGraphicPanel.HotLine(color,
				x, y-2, x, y+2));
		
	}
	
	/**
	 * @author ajulin
	 * @param sp
	 * @param color
	 * 
	 * draw center of spot sp on AGScan
	 */
	private static void plot_cross(TSpot sp, Color color){
			
			int x=(int)(sp.getXCenter()/pixelW);
			int y=(int)(sp.getYCenter()/pixelH);
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(color,
					x-2, y, x+2, y));
			
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(color,
					x, y-2, x, y+2));
			
		}
	
	/**
	 * 
	 * @param cMa
	 * @param cMo
	 * @param colorcMa
	 * @param colorcMo
	 */
	private static void plot_AllHorizontalLine(Vector cMa,Vector cMo, Color colorcMa, Color colorcMo){
		
		
		for(int i = 0 ; i < cMo.size() ; i++){
			double y2 = (Double)cMo.get(i);
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(colorcMo,
					2, (int)y2, alignment.getImage().getImageModel().getElementWidth()-2, (int)y2));

			
			
			
			double y1 = (Double)cMa.get(i);
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(colorcMa,
					2, (int)y1, alignment.getImage().getImageModel().getElementWidth()-2, (int)y1));
			

		}
		
	}
	
	/**
	 * 
	 * @param cMa
	 * @param cMo
	 * @param colorcMa
	 * @param colorcMo
	 */
	private static void plot_AllVerticalLine(Vector cMa,Vector cMo, Color colorcMa, Color colorcMo){
		
		
		for(int i = 0 ;i < cMo.size() ; i++){
		
			
			double x1 = (Double)cMa.get(i);	
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(colorcMa,
					(int)x1,2, (int)x1, alignment.getImage().getImageModel().getElementHeight()-2));

			double x2 = (Double)cMo.get(i);
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(colorcMo,
					(int)x2,2, (int)x2, alignment.getImage().getImageModel().getElementHeight()-2));

		}
		
	}

	/**
	 * 
	 * @param x1
	 * @param c
	 */
	private static void plot_VerticalLine(int x1, Color c){
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(c,
					x1,2, x1, alignment.getImage().getImageModel().getElementHeight()-2));
		
	}
	
	/**
	 * 
	 * @param y1
	 * @param c
	 */
	private static void plot_HorizontalLine(int y1, Color c){
		
			((TAlignmentGraphicPanel) alignment.getView().getGraphicPanel())
			.addHotLine(new TImageGraphicPanel.HotLine(c,
					2, (int)y1, alignment.getImage().getImageModel().getElementWidth()-2, (int)y1));

		
	}
	
	/**
	 * @author ajulin
	 * @param c
	 * 
	 * Draw all elements of a grid on AGScan view
	 */
	private static void plot_grid(Color c){
		Vector test = alignment.getGridModel().getSpots();
		for(int i = 0 ; i < test.size() ; i++){
			TSpot sp = (TSpot)test.get(i);
			plot_cross(sp,c);
		}
		
	}
	
	//--------------------------Fin outil de verification
	
	
//	---------------------------Traitement de vecteur tri, moyenne ecart type 
	
	/**
	 * @author ajulin
	 * @param v vector sort by block
	 * @param nbSpotCol number of spot on 1 lines
	 * @return vector sort by line
	 */
	private static Vector sortVector (Vector v,int nbSpotCol){
		Vector buff= new Vector();
		Vector res = new Vector();
		
		//copie du vecteur
		for( int i = 0 ; i< v.size(); i++){
			buff.add(v.get(i));
		}
		
		if(v.size()!=1){
			//on trie l'ensemble des spot de la grille selon les Y 
			Collections.sort( buff, new Comparator (){
				public int compare(Object arg0, Object arg1) {
					int res;
					TSpot sp1 = (TSpot) arg0;
					TSpot sp2 = (TSpot) arg1;
					
					res = ((int)(sp1.getYCenter()) - ((int)sp2.getYCenter()));
					return res;
					
				}
			});
			
			//on recupere une sous liste contenant tt les spot de la ligne de la grille pour les trier selon les X
			List  l;
			for(int j = 0 ; j<(buff.size()/nbSpotCol);j++){
				l = buff.subList(j*nbSpotCol, (j+1)*nbSpotCol);
				Collections.sort(l, new Comparator (){
					public int compare(Object arg0, Object arg1) {
						TSpot sp1 = (TSpot) arg0;
						TSpot sp2 = (TSpot) arg1;
						
						return ((int)(sp1.getYCenter()) - (int) (sp2.getYCenter()));
						
					}
				});
				//on les range dans un vecteur
				for(int o = 0 ; o < l.size() ; o++)
					res.add(l.get(o));
				
				
			}//fin du for			
		}
		else
			res.add((TSpot)buff.firstElement());
		return res;
		
	}

	/**
	 * @author ajulin
	 * @param v vector to sort
	 * @param index
	 * @return
	 */
	public static double maj_Moy(Vector v, int index){
		double moy;
		double nb = 1.0D;
		double sum = 0.0D;
		if( index == -1){
			nb = v.size();
			for(int i = 0 ; i < v.size() ; i++)
				sum  = sum + (Double)v.get(i);
		}
		else{
			nb = index;
			for(int i = 0 ; i <index ; i++)
				sum  = sum + (Double)v.get(i);
		}
			
		moy = sum / nb;
		
		return moy;
	}
	
	/**
	 * @author ajulin
	 * @param v 
	 * @return mean of vector v
	 * 
	 */
	private static double moyenne(Vector v){
		double moy = 0.0D;
		double sum = 0.0D;
		for(int i = 0 ; i < v.size() ; i++){
			double curr = (Double)v.get(i);
			sum = sum + curr;
			
		}
		moy = sum / (double)v.size();
		
		
		return moy;
	}
	
	
	/**
	 * @author ajulin 
	 * @param value
	 * @param dec precision
	 * @return calculate an round of value
	 */
	public static double round(double value, int dec) {
		double mult = Math.pow (10.0, (double)dec);
		return Math.round(value * mult) / mult;
	}

	/**
	 * @author ajulin
	 * @param v
	 * @return standard deviation of v
	 * 
	 */
	private static double ecartType(Vector v){
		double ecart = 0.0D;
		double sum = 0.0D;
		double moyenne = moyenne(v);
		
		for(int i= 0 ; i < v.size() ; i++){
			double curr = (Double)v.get(i);
			sum = sum + (curr - moyenne)*(curr - moyenne);
		}
		ecart = Math.sqrt(sum / (double)v.size());
		
		return ecart;
	}
	
	
	//---------------------------fin Traitement de vecteur tri, moyenne ecart type
	
	//---------------------------Outils de traitement et recuperation des données de l'image
	
	//	res[0] centre moyen sur l'axe des Y => centre moyen des lignes
	//res[1] centre moyen sur l'axe des X => centre moyen des colonnes
	/**
	 * @author ajulin
	 * @param gm current alignement's TGridModel 
	 * @param tri 
	 * @return An array of vector which contains "Means center" of lines and rows for
	 * grid elements in pixel
	 */
	public static Vector [] centreMoyen(TGridModel gm, boolean tri){
		
		Vector [] res = new Vector[2];

		
		double centreY;
		double centreX;
		Vector cercle2;
		Vector Yc = new Vector();
		Vector Xc = new Vector();
		Vector centre_moyen = new Vector();
		Vector centre_moyen2 = new Vector();
		TGridConfig gc = gm.getConfig();
		
		//on recupere la grille pour connaitre les lignes de centre des colonnes

		int nb_spot_col = gc.getLev1NbRows() * gc.getLev2NbRows();
		int nb_spot_lig = gc.getLev1NbCols() * gc.getLev2NbCols();
		double diam_spot = ((double)gc.getSpotsDiam()/25.0D);
		Vector cercle = gm.getSpots();
		
				
		if(tri){
			cercle2 = sortVector(cercle,2*gc.getLev1NbRows());
		}
		else
			cercle2 = cercle;
		
	
		for(int i = 0 ; i< cercle2.size() ; i++){
			TSpot sp = (TSpot)cercle2.get(i);
			Xc.add((double)(sp.getXCenter()/25.0D));
			Yc.add((double)(sp.getYCenter()/25.0D));
		}
		
		
		for(int i = 0 ; i < nb_spot_col ; i++){
			centreY = 0;
			for (int j = 0 ; j < nb_spot_lig ; j++){
				centreY = centreY + (Double)(Yc.get(i * nb_spot_lig + j));
			}
			
			centre_moyen.add((double)(centreY/(double)nb_spot_lig)); 
		}
		
		
		for(int i = 0 ; i < nb_spot_lig ; i++){
			centreX = 0;
			
			for (int j = 0 ; j < nb_spot_col ; j++){
				centreX = centreX + (Double)(Xc.get(i + nb_spot_lig * j));
			}
			centre_moyen2.add((double)(centreX/(double)nb_spot_col)); 
		}
			
		
		res[0] = centre_moyen;
		res[1] = centre_moyen2;
		
		return res;
	}
	
	//res[0] => coord en X des centres de masse
	//res[1] => coord en Y des centres de masse
	/**
	 * @author ajulin
	 * @param im Image to analyse with particles 
	 * @return An array in 2D of double which contains "Mass center" of lines and rows for
	 * image elements in pixel
	 */
	public double [][] centreMasse(ImagePlus im){
		
		
		TGridConfig gc = alignment.getGridModel().getConfig();
		
		ImageProcessor ip2;
		//on cherche a recuperer les centres des spots de l'images
		//L'image est trait�e pour l'alignement =>un simple seuillage suffit
		ImageProcessor ip = im.getProcessor();
		ip.autoThreshold();
		
		//analyse de l'image seuille�
		//conversion en une image 8 bits
		ImagePlus im2;
		if(im.getBitDepth() !=8 ){
			ip2=ip.convertToByte(false);
			im2 = new ImagePlus ("resultat",(ImageProcessor)ip2);
		}
		else{
			im2 = new ImagePlus ("resultat",ip);
		}
		//im2.show();
		
		
		
		ResultsTable rt = new ResultsTable();
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		System.out.println("part size "+partSize);
		//ParticleAnalyzer mypart = new ParticleAnalyzer(0,8192+255, rt, partSize*0.6D, partSize*1.4D);
		ParticleAnalyzer mypart = new ParticleAnalyzer(8+32+64+256,64, rt, partSize * b_min, partSize * b_max);
		mypart.analyze(im2);

		double [][] res = new double[2][rt.getCounter()];



		
		for (int o = 0; o < rt.getCounter(); o++) {
			res[0][o] = (double)(rt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o]);
			res[1][o] = (double)(rt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o]);

		}

		System.out.println("nb_particule ds centre masse= "+rt.getCounter());

		return res;
	}
	
	
	
	
	
	/**
	 * @author ajulin
	 * @param gm
	 * @param cdm Mass center coordonnates give by image's analysis
	 * @return  a vector which contains spots inside grid and inside a block
	 */
	public static Vector[] score1(TGridModel gm,double cdm [][]){
		
		Vector resX = new Vector();
		Vector resY = new Vector();
		
		
		
		TGridConfig gc = gm.getConfig();
		Vector spots = gm.getSpots();
		Vector blocs = new Vector();

		
		int nb_spot_bloc = gc.getLev1NbCols() * gc.getLev1NbRows();
		int nb_bloc = gc.getLev2NbCols() * gc.getLev2NbRows();
		double eisc = (double)(gc.getSpotsWidth()/25.0D);
		double eisl = (double)(gc.getSpotsHeight()/25.0D);
		
		for(int i = 0 ; i < nb_bloc ; i++){
			TSpot sp1 = (TSpot)spots.get(i * nb_spot_bloc);
			TSpot sp2 = (TSpot)spots.get((i+1) * nb_spot_bloc - 1);
			
			blok b = new blok(sp1 , sp2,eisc,eisl);
			blocs.add(b);
			
		}

		boolean find;
		int j;
		for(int i = 0 ; i < cdm[0].length ; i++){
			find = false;
			j = 0;
			while((!find) && (j != blocs.size())){
				if(((blok)blocs.get(j)).appartient(cdm[0][i], cdm[1][i],false))
						find = true;
				j++;
			}
			if(find){
				resX.add(cdm[0][i]);
				resY.add(cdm[1][i]);
			}

		}
		
		Vector  res [] = new Vector[3];
		res[0] = resX;
		res[1] = resY;
		
		Vector taille = new Vector();
		taille.add(cdm[0].length);
		res[2] = taille;
		
		
		return res;
		
	}

	
	/**
	 * @author ajulin
	 * @param gm
	 * @param im
	 * @return 
	 */
	public int inGrid(TGridModel gm, ImagePlus im){
		int nb_part = 0;
		
		
		Vector v = gm.getSpots();
		TGridConfig gc = gm.getConfig();
		TSpot deb = (TSpot)v.get(0);
		TSpot fin = (TSpot)v.get(v.size()-1);
		//extraction de l'image		
		int x1 = (int)((deb.getXCenter() - gc.getSpotsWidth()/2.0D)/pixelW);
		int y1 = (int)((deb.getYCenter() - gc.getSpotsHeight()/2.0D)/pixelH);
		
		int x2 = (int)((fin.getXCenter() + gc.getSpotsWidth()/2.0D)/pixelW);
		int y2 = (int)((fin.getYCenter() + gc.getSpotsHeight()/2.0D)/pixelH);
		
		ImagePlus res;
		im.setRoi(x1, y1, x2 - x1,y2 - y1);
		ImageProcessor ip1 =im.getProcessor();
		ImageProcessor ip2 = ip1.crop();
		res = new ImagePlus("Intra Grid",ip2);
		im.killRoi();
		//res.show();
		
//		analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		//on travaille sur l'image de correlation donc on prend pour une limite d'aire plus grande que pour l'image max
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt,partSize * b_min,partSize * b_max);
		analyse.analyze(res);
		nb_part = rt.getCounter();
		
		
		
		return nb_part;
	}
	
	/**
	 * @author ajulin
	 * @param gm
	 * @param im
	 * @return number of particles found in 2 last columns of the grid
	 */
	public int analyse2LastCol(TGridModel gm, ImagePlus im){
		int nb_part = 0;
		
		Vector v = gm.getSpots();
		TGridConfig gc = gm.getConfig();
		TSpot deb = (TSpot)v.get(0);
		TSpot fin = (TSpot)v.get(v.size()-1);
		//extraction de l'image		
		int x1 = (int)((deb.getXCenter() - gc.getSpotsWidth()/2.0D)/pixelW);
		int y1 = (int)((deb.getYCenter() - gc.getSpotsHeight()/2.0D)/pixelH);
		
		int x2 = (int)((fin.getXCenter() + gc.getSpotsWidth()/2.0D)/pixelW);
		int y2 = (int)((fin.getYCenter() + gc.getSpotsHeight()/2.0D)/pixelH);
		
		ImagePlus res;
		im.setRoi((int)(x2 - 2 *gc.getSpotsWidth()/pixelW) , y1, (int)(2 *gc.getSpotsWidth()/pixelW),y2 - y1);
		ImageProcessor ip1 =im.getProcessor();
		ImageProcessor ip2 = ip1.crop();
		res = new ImagePlus("2LastCol",ip2);
		//im.show();
		//res.show();
		
//		analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt,partSize * b_min, partSize * b_max);
		analyse.analyze(res);
		nb_part = rt.getCounter();
		//System.out.println("nbre de part " + nb_part);
		
		return nb_part;
	}
	
	/**
	 * @author ajulin
	 * @param gm
	 * @param im
	 * @return number of particles found in 2 last columns of the grid
	 */
	public int analyse2LastRow(TGridModel gm, ImagePlus im){
		int nb_part = 0;
		
		Vector v = gm.getSpots();
		TGridConfig gc = gm.getConfig();
		TSpot deb = (TSpot)v.get(0);
		TSpot fin = (TSpot)v.get(v.size()-1);
//extraction de l'image		
		int x1 = (int)((deb.getXCenter() - gc.getSpotsWidth()/2.0D)/pixelW);
		int y1 = (int)((deb.getYCenter() - gc.getSpotsHeight()/2.0D)/pixelH);
		
		int x2 = (int)((fin.getXCenter() + gc.getSpotsWidth()/2.0D)/pixelW);
		int y2 = (int)((fin.getYCenter() + gc.getSpotsHeight()/2.0D)/pixelH);
		
		ImagePlus res;
		im.setRoi(x1 ,(int)(y2 - 2 *gc.getSpotsHeight()/pixelH), x2 - x1,(int)(2 *gc.getSpotsHeight()/pixelH));
		ImageProcessor ip1 =im.getProcessor();
		ImageProcessor ip2 = ip1.crop();
		res = new ImagePlus("2LastRow",ip2);
//		im.show();
//		res.show();
		
//		analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, partSize * b_min, partSize * b_max);
		analyse.analyze(res);
		nb_part = rt.getCounter();
		//System.out.println("nbre de part " + nb_part);
		
		
		return nb_part;
	}
	
	//	---------------------------fin Outils de traitement 
	
	
	//----------------------------fonctions intervenant dans la correction
	/**
	 * @author ajulin
	 * @param im image to analyse
	 * 
	 * align the grid on the first spot found
	 */
	public void first_Align(ImagePlus im){
		
		
		//extraction de l'image
		TGridConfig gc = alignment.getGridModel().getConfig();
		Vector v_b = alignment.getGridModel().getLevel1Elements();
		TGridBlock gb = (TGridBlock)v_b.get(0);
		
		
		TSpot sp1 = (TSpot)gb.getFirstElement();
		TSpot sp2 = (TSpot)gb.getLastElement();
		double spotH = gc.getSpotsHeight()/pixelH;
		double spotW = gc.getSpotsWidth()/pixelW;
		int x1 = (int)(sp1.getXCenter()/pixelW - spotW/2.0D);
		int y1 = (int)(sp1.getYCenter()/pixelH - spotH/2.0D);
		int x2 = (int)(sp2.getXCenter()/pixelW + spotW/2.0D);
		int y2 = (int)(sp2.getYCenter()/pixelH + spotH/2.0D);
		
		//extraire la partie d'image qui correspond a ce bloc
		im.setRoi(x1,y1,x2-x1,y2-y1);
		ImageProcessor ip = im.getProcessor();
		ImageProcessor ip2 = ip.crop();
		ImagePlus im2 = new ImagePlus("bloc 0", ip2);
		//im2.show();
		im.killRoi();
//		analyse de l'image obtenue
		ResultsTable rt = new ResultsTable();
		double b_max = (double)((Integer)gaAlgo.getParameter(gaAlgo.MAX_AREA_SIZE))/100.0D;
		double b_min = (double)((Integer)gaAlgo.getParameter(gaAlgo.MIN_AREA_SIZE))/100.0D;
		double partSize = (gc.getSpotsDiam()/(pixelW*2))*(gc.getSpotsDiam()/(pixelW*2))*Math.PI*2.0D;
		ParticleAnalyzer analyse = new ParticleAnalyzer(8+32+64+256,64, rt, partSize * b_min, partSize * b_max);
		analyse.analyze(im2);

		double [][] coord = new double[2][rt.getCounter()];

		//System.out.println("Spots");
		for (int o = 0; o < rt.getCounter(); o++) {
			coord[0][o] = (double)(rt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[o]);
			coord[1][o] = (double)(rt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[o]);
			//System.out.println(coord[0][o]);
		}
		
		
		
		double m = Double.POSITIVE_INFINITY;
		int indice = -1;
		for(int i = 0 ; i < 0.5 * coord[0].length ; i++){
			double d = Math.sqrt(coord[0][i]*coord[0][i] + coord[1][i]*coord[1][i]);
			if(d < m){
				indice = i;
				m = d;
			}
			
		}
		int first_X = (int)(coord[0][indice]);
		int first_Y = (int)(coord[1][indice]);
		
		
		//on cherche le spot le plus proche
		Vector cMo []  = centreMoyen_2(gc,gb);
		
		Vector x_grid = cMo[0];
		Vector y_grid = cMo[1];
		
		
		int col = -1;
		int row = -1;
		
		int min = Integer.MAX_VALUE;
		
		//on cherche la colonne de la grille dont le spot est le plus proche
		for(int i =0 ; i < x_grid.size() ; i++){
			int col_grid = (Integer)x_grid.get(i);
			if(Math.abs(first_X - col_grid) < min){
				col = i;
				min = Math.abs(first_X - col_grid);
			}
			
		}
		min = Integer.MAX_VALUE;
		
		//on cherche la ligne de la grille dont le spot est le plus proche
		for(int i =0 ; i < y_grid.size() ; i++){
			int row_grid = (Integer)y_grid.get(i);
			
			if(Math.abs(first_Y - row_grid) < min){
				row = i;
				min = Math.abs(first_Y - row_grid);
			}
			
		}
		im.repaintWindow();
		
		//on realise la translation
		TEvent e = new TEvent(TEventHandler.DATA_MANAGER,
				TGridPositionControler.TRANSLATE, null,
				(first_X - (Integer)x_grid.get(col))* pixelW	
				, (first_Y - (Integer)y_grid.get(row))* pixelH);
		TEventHandler.handleMessage(e);

	}
	
	/**
	 * @author ajulin
	 * @param gc
	 * @param gb
	 * @return coordonates of all the lines and columns in the image of the current block
	 */
	public static Vector [] centreMoyen_2(TGridConfig gc, TGridBlock gb){
		Vector res [] = new Vector[2];
		Vector centre_X = new Vector();
		Vector centre_Y = new Vector();

		
		//plot_cross(alignment, (int)(gb.getX()/pixelW), (int)(gb.getY()/pixelH), Color.red);
		
		Vector spots = gb.getElements();
		for(int i = 0 ; i < gc.getLev1NbCols() ; i++){
			TSpot curr = (TSpot)spots.get(i);
			int abs = (int)(Math.rint(curr.getXCenter()/pixelW - gb.getX()/pixelW));
			centre_X.add(abs);
		}
		
		for(int i = 0 ; i < spots.size() ; i = i + gc.getLev1NbCols()){
			TSpot curr = (TSpot)spots.get(i);
			int ord = (int)(Math.rint(curr.getYCenter()/pixelH - gb.getY()/pixelH));
			centre_Y.add(ord);
		}
		
		res [0] = centre_X;
		res [1] = centre_Y;

		return res;
		
	}
	

	/**
	 * @author ajulin
	 * @param tab coordonates of lines and columns of the current grid
	 * @param gc
	 * @param tab2  means coordonates of lines and columns of the image after analyse
	 * 
	 * @return coefficient for horizontal resize
	 */
	public double Horizontal_size(TAlignment ali,Vector [] tab,TGridConfig gc,TImageModel imo,double tab2 [][]){
		double decalage = 0.0D;

		double pixelWidth = (double) imo.getPixelWidth();
		//on recupere les centres des colonnes de la grille
		Vector centreMoyen_X = tab[1];

		
		//on recupere les coordonnees des centres de masse de l'image segmentee
		Vector centreMasse_X = new Vector();
		for(int i = 0 ; i < tab2[1].length ; i++)
			centreMasse_X.add(tab2[0][i]);


		//on regroupe les centres de masses par colonnes
		Vector col = new Vector();
		Vector moy = new Vector();
		double born = 0.5 * gc.getSpotsWidth()/pixelWidth;
		boolean find;
		int indice;
		double curr_X;
		double moy_X;
		for(int i = 0 ; i < centreMasse_X.size() ; i++){
			find = false;
			indice = 0;
			
			while ((!find) && (indice != col.size())){
				moy_X = (Double)moy.get(indice);
				curr_X = (Double)centreMasse_X.get(i);
				if((curr_X <= moy_X + born) && (curr_X >= moy_X  - born))
					find = true;
				else
					indice++;
			}
			
			//on rajoute au vecteur indexé par indice
			if(find){
				Vector v = (Vector)col.get(indice);
				v.add((Double)(centreMasse_X.get(i)));
				col.setElementAt(v, indice);
				//on met a jour la moyenne => optimisation => utiliser la moyenne precedemment calculée
				double m = maj_Moy(v, -1);
				moy.setElementAt(m,indice);
				
			}
			//on cree un nv vecteur
			else{
				Vector v = new Vector();
				v.add((Double)centreMasse_X.get(i));
				col.add(v);
				//on rajoute cette valeur ds le vecteur de moyenne
				moy.add((Double)centreMasse_X.get(i));
			}
			
			
				
				
			}

		
		//on trie le vecteur col les X croissant 
		Collections.sort( col, new Comparator (){
			public int compare(Object arg0, Object arg1) {
				int res;
				Vector v1 = (Vector) arg0;
				Vector v2 = (Vector) arg1;
				
				double p1 = (Double)v1.get(0);
				double p2 = (Double)v2.get(0);

				res = ((int)p1 - (int)p2);
				return res;

			}
		});
		
		Collections.sort( moy, new Comparator (){
			public int compare(Object arg0, Object arg1) {
				int res;
				double p1 = (Double) arg0;
				double p2 = (Double) arg1;

				res = ((int)p1 - (int)p2);
				return res;

			}
		});
		

		boolean valide = true;
		double rapport = 0.0D;
		Vector decalage1 = new Vector();
		Vector decalage2 = new Vector();
		
		if(valide){
			decalage1.clear();
			for(int i = 0 ; i < centreMoyen_X.size() ; i++)
				decalage1.add((Double)centreMoyen_X.get(i) - (Double)moy.get(i));


			for(int i = 0 ; i < decalage1.size()-1 ; i++)
				decalage2.add((Double)decalage1.get(i+1)- (Double)decalage1.get(i));
			
			//test sur d'autre méthode pour améliorer le calcul du resize
//			//test segmentation min/max
//			double max = Double.NEGATIVE_INFINITY;
//			double min = Double.POSITIVE_INFINITY;
//
//			
//			min = moyenne(decalage2) - ecartType(decalage2);
//			max = moyenne(decalage2) + ecartType(decalage2);
//			
//			int nb_class = 9;
//			double amplitude = max - min;
//			double pas = amplitude/(double)nb_class;
//			
//			Vector born_max = new Vector(nb_class);
//			Vector born_min = new Vector(nb_class);
//			
//			
//			for(int i = 0 ; i < nb_class ; i++){
//				born_min.add(min + i*pas);
//				born_max.add(min + (i+1) * pas);
//					
//			}
//			
//			Vector vote_min_max = new Vector(nb_class);
//			for(int i = 0 ; i< nb_class ; i++)
//				vote_min_max.add(0);
//			
//			
//			for(int i = 0 ; i < decalage2.size() ; i++){
//				double curr = (Double)decalage2.get(i);
//				int cpt = 0;
//				boolean trouve= false;
//				
//				while((!trouve) && (cpt < nb_class)){
//					double b_min = (Double)born_min.get(cpt);
//					double b_max = (Double)born_max.get(cpt);
//					
//					if((curr >= b_min) && (curr < b_max)){
//						vote_min_max.setElementAt(((Integer)vote_min_max.get(cpt))+1, cpt);
//						trouve = true;
//					
//					}
//					cpt++;
//					
//					
//				}
//				
//				
//			}
			
			
			//decalage valide
			Vector vote = new Vector();
			Vector deca = new Vector();
			double  moy_curr = 0.0D;

			//recuperation du seuil défini dans les paramètres
			int seuil1 = (Integer)gaAlgo.getParameter(gaAlgo.STD_DEV_H);
			double multiplicateur = (double)seuil1/100.0D;
			
			double  ecart= ecartType(decalage2);
			rapport = ecart / moyenne(decalage2);
			
			double borne = multiplicateur * ecart;
			
			
			//on regroupe les decalages et on les moyennes
			for(int i = 0 ; i < decalage2.size() ; i++){
				find = false;
				int cpt = 0;
				double d  = (Double)decalage2.get(i);

				
				while(cpt != deca.size() && (! find)){
					moy_curr = (Double)deca.get(cpt);

					
					
					if((d >= moy_curr - borne  ) && ((d <= moy_curr + borne))){
						find = true;
					}
					else	
						cpt++;
				}
				if(find){
					int v = (Integer)vote.get(cpt);
					double nv_moy =  (d + moy_curr * v)/(v + 1);
					deca.setElementAt(nv_moy, cpt);
					v ++;
					vote.setElementAt(v,cpt);
				}
				else{
					deca.add(d);
					vote.add(new Integer(1));
				}

			}

			
			//on recherche le decalage qui a obtenu le maximum de vote
			int max_vote = 0;
			indice = -1;
			for(int i = 0 ; i < vote.size() ; i++){

				if((Integer)vote.get(i)>max_vote){
					max_vote = (Integer)vote.get(i);
					indice = i;
				}

			}
			//on valide ou non le decalage
			int seuil2 = (Integer)gaAlgo.getParameter(gaAlgo.PERCENT_COL);
			double d_seuil2 = ((double)seuil2/100.0D);
			if(max_vote > d_seuil2*(gc.getLev1NbCols()*gc.getLev2NbCols()-1))
				decalage = (Double)deca.get(indice);
			else
				decalage = 0.0D;
			
			System.out.println("resize horizontal trouvé "+decalage);
		}
		else{
			System.out.println("aucun decalage trouvé");
			decalage = Double.POSITIVE_INFINITY;
		}
		System.out.println("rapport = "+rapport);
		//verication du rapport ecart-type/moyenne => si >borne alors grande dispersion
		//pas de correction de decalage
		int seuil3 = (Integer)gaAlgo.getParameter(gaAlgo.MAX_RATIO_H);
		if(Math.abs(rapport) >(double)seuil3){
			decalage = 0.0D;
		}
		
		return decalage;
	}
	
	
	/**
	 * @author ajulin
	 * @param tab coordonates of lines and columns of the current grid
	 * @param gc
	 * @param tab2  means coordonates of lines and columns of the image after analyse
	 * 
	 * @return coefficient for vetical resize
	 */
	public double Vertical_size(TAlignment ali,Vector [] tab,TGridConfig gc,TImageModel imo,double tab2 [][]){
		double decalage = 0.0D;

		double pixelHeight = (double) imo.getPixelHeight();
		//on recupere les centres moyens en X
		Vector centreMoyen_Y = tab[0];

		//on recupere les coordonnees des centres de masse de l'image segmentee
		Vector centreMasse_Y = new Vector();
		for(int i = 0 ; i < tab2[0].length ; i++)
			centreMasse_Y.add(tab2[1][i]);


		//on regroupe les centres de masses par colonnes
		Vector col = new Vector();
		Vector moy = new Vector();
		double born = 0.5 * gc.getSpotsHeight()/pixelHeight;
		boolean find;
		int indice;
		double curr_Y;
		double moy_Y;
		for(int i = 0 ; i < centreMasse_Y.size() ; i++){
			find = false;
			indice = 0;
			
			while ((!find) && (indice != col.size())){
				moy_Y = (Double)moy.get(indice);
				curr_Y = (Double)centreMasse_Y.get(i);
				if((curr_Y <= moy_Y + born) && (curr_Y >= moy_Y  - born))
					find = true;
				else
					indice++;
			}
			
			//on rajoute au vecteur indexé par indice
			if(find){
				Vector v = (Vector)col.get(indice);
				v.add((Double)(centreMasse_Y.get(i)));
				col.setElementAt(v, indice);
				//on met a jour la moyenne => optimisation => utiliser la moyenne precedemment calculée
				double m = maj_Moy(v, -1);
				moy.setElementAt(m,indice);
				
			}
			//on cree un nv vecteur
			else{
				Vector v = new Vector();
				v.add((Double)centreMasse_Y.get(i));
				col.add(v);
				//on rajoute cette valeur ds le vecteur de moyenne
				moy.add((Double)centreMasse_Y.get(i));
			}
			
			
			
			
		}
		
		//on trie le vecteur col les Y croissants 
		Collections.sort( col, new Comparator (){
			public int compare(Object arg0, Object arg1) {
				int res;
				Vector v1 = (Vector) arg0;
				Vector v2 = (Vector) arg1;
				
				double p1 = (Double)v1.get(0);
				double p2 = (Double)v2.get(0);

				res = ((int)p1 - (int)p2);
				return res;

			}
		});
		
		Collections.sort( moy, new Comparator (){
			public int compare(Object arg0, Object arg1) {
				int res;
				double p1 = (Double) arg0;
				double p2 = (Double) arg1;

				res = ((int)p1 - (int)p2);
				return res;

			}
		});
		
		boolean valide = true;
		Vector decalage1 = new Vector();
		Vector decalage2 = new Vector();
		double rapport = 0.0D;
		
		if(valide){
			decalage1.clear();
			for(int i = 0 ; i < centreMoyen_Y.size() ; i++)
				decalage1.add((Double)centreMoyen_Y.get(i) - (Double)moy.get(i));
			
				
			for(int i = 0 ; i < decalage1.size()-1 ; i++)
				decalage2.add((Double)decalage1.get(i+1) - (Double)decalage1.get(i));
			
			//decalage valide
			Vector vote = new Vector();
			Vector deca = new Vector();
			double  moy_curr = 0.0D;
			
			
			//int seuil1 = (Integer)seuil.get(2);
			int seuil1 = (Integer)gaAlgo.getParameter(gaAlgo.STD_DEV_V);
			double multiplicateur = (double)seuil1/100.0D;
			double ecart = ecartType(decalage2);
			
			double borne = multiplicateur * ecart;
			rapport = ecart / moyenne(decalage2);
			
			
			for(int i = 0 ; i < decalage2.size() ; i++){
				find = false;
				int cpt = 0;
				double d  = (Double)decalage2.get(i);

				while(cpt != deca.size() && (! find)){
					moy_curr = (Double)deca.get(cpt);

					
					if((d >= moy_curr - borne) && ((d <= moy_curr + borne)))
						find = true;
					else	
						cpt++;
				}
				if(find){
					int v = (Integer)vote.get(cpt);
					double nv_moy =  (d + moy_curr * v)/(v + 1);
					deca.setElementAt(nv_moy, cpt);
					v ++;
					vote.setElementAt(v,cpt);
				}
				else{
					deca.add(d);
					vote.add(new Integer(1));
				}

			}

			int max_vote = 0;
			indice = -1;
			for(int i = 0 ; i < vote.size() ; i++){
				
				if((Integer)vote.get(i)>max_vote){
					max_vote = (Integer)vote.get(i);
					indice = i;
				}
				
			}
			
			//int seuil2 = (Integer)seuil.get(3);
			int seuil2 = (Integer)gaAlgo.getParameter(gaAlgo.PERCENT_ROW);
			double d_seuil2 = ((double)seuil2/100.0D);
			if(max_vote > d_seuil2*(gc.getLev1NbRows()*gc.getLev2NbRows()-1))
				decalage = (Double)deca.get(indice);
			else
				decalage = 0.0D;
			
	
			System.out.println("resize vertical trouvé "+decalage);
		}
			
		else
			decalage = Double.POSITIVE_INFINITY;
		
		//verication du rapport ecart-type/moyenne => si >borne alors grande dispersion
		//pas de correction de decalage
		int seuil3 = (Integer)gaAlgo.getParameter(gaAlgo.MAX_RATIO_V);
		if(Math.abs(rapport) >(double)seuil3){
			decalage = 0.0D;
		}
		
		
		
		return decalage;
	}
	
	
	/**
	 * @author ajulin
	 * @param alignement
	 * @param im
	 */
	public boolean SizeCorrection(TAlignment alignement, ImagePlus im){
		
		boolean res = true;
		
		//stockage des carac pre-correction
		int before[] = verif_Correction(alignement, im);
		System.out.println("nb spot hors grille = "+before[0]);
		System.out.println("nb spot dans les 2 dernieres col = "+before[1]);
		System.out.println("nb spot dans les 2 dernieres lignes = "+before[2]);
		
		//recuperation des données
		double cMa[][] = centreMasse(im);
		Vector cMo[] = centreMoyen(alignement.getGridModel(), true);
		TGridConfig gc = alignement.getGridModel().getConfig();
		TImageModel imo = alignement.getImage().getImageModel();
		int nb_col = gc.getLev1NbCols()*gc.getLev2NbCols();
		int nb_row = gc.getLev1NbRows()*gc.getLev2NbRows();
		

		
		
		//first_Align(cMo,alignement,im);
		first_Align(im);
		
		int acti = (Integer)gaAlgo.getParameter(gaAlgo.ACTI_SIZE);
		
		
		if(acti == 1){
			System.out.println("correction de la taille activée");
			Vector cMo2[] = centreMoyen(alignement.getGridModel(), true);
			double pixelWidth = imo.getPixelWidth();
			double pixelHeight = imo.getPixelHeight();
			double s_h =-Horizontal_size(alignement,cMo2, gc,imo, cMa);
			double s_v =-Vertical_size(alignement,cMo2, gc,imo, cMa);
			if(s_h != Double.POSITIVE_INFINITY){
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_EAST, alignmentModel.getReference(),
						s_h*nb_col, pixelWidth, pixelHeight));
				res = false;
			}
			if(s_v != Double.POSITIVE_INFINITY){
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_SOUTH, alignmentModel.getReference(),
						s_v*nb_row, pixelWidth, pixelHeight));
				res = false;
			}


			//Vector cMo1[] = centreMoyen(alignement.getGridModel(), true);
			//first_Align(cMo1,alignement,cMa, im);

			//stockage des carac post-correction
			int after[] = verif_Correction(alignement, im);
			System.out.println("nb spot hors grille = "+after[0]);
			System.out.println("nb spot dans les 2 dernieres col = "+after[1]);
			System.out.println("nb spot dans les 2 dernieres lignes = "+after[2]);


			if(after[1] + after[2] < before[1] + before[2]){
				//grille trop grande
				System.out.println("Grille apres resize trop grande");
				if(after[1] < before[1])
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_EAST, alignmentModel.getReference(),
							-s_h*nb_col, pixelWidth, pixelHeight));

				if(after[2] < before[2])
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_SOUTH, alignmentModel.getReference(),
							-s_v*nb_row, pixelWidth, pixelHeight));

			}
			else{

				if(after[0] > before[0]){
					System.out.println("Grille apres resize trop petite");
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_SOUTH, alignmentModel.getReference(),
							-s_v*nb_row, pixelWidth, pixelHeight));
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER,TGridPositionControler.RESIZE_EAST, alignmentModel.getReference(),
							-s_h*nb_col, pixelWidth, pixelHeight));
				}
			}
		}
		
		return res;
		
	}
	
	
	
	public int[] verif_Correction(TAlignment alignement, ImagePlus im){
		int res [] = new int[3];
		TGridModel gm = alignement.getGridModel();
		int inG = inGrid(gm,im);
		System.out.println("nb spot in grid = "+inG);
		res[0] = nb_spot - inG;
		res[1] = analyse2LastCol(gm, im);
		res[2] = analyse2LastRow(gm, im);
		return res;
	}
	
}


class blok{
	/**
	 * @author ajulin
	 * 
	 *    (x1,y1)          (x2,y2)
	 *      *---------------*
	 * 		|               |
	 *      |               |
	 *      |      Block    |
	 *      |               |
	 *      |               |
	 *      *---------------*       
	 *    (x3,y3)          (x4,y4)  
	 */
	

	//coin sup gauche
	double x1;
	double y1;
	
	//coin sup droit
	double x2;
	double y2;
	
	//coin inf gauche
	double x3;
	double y3;
	
	//coin inf droit
	double x4;
	double y4;
	
	//espace inter spot
	double eisc;
	double eisl;
	
	/**
	 * @author ajulin
	 * @param sp1 upper and left spot of block
	 * @param sp2 down and rigth spot of block
	 * @param c inter spot space between 2 columns
	 * @param l inter spot space between 2 lines
	 */
	blok(TSpot sp1 , TSpot sp2, double c , double l){
		x1 = (double)(sp1.getXCenter()/25.0D);
		y1 = (double)(sp1.getYCenter()/25.0D);
		
		x4 = (double)(sp2.getXCenter()/25.0D);
		y4 = (double)(sp2.getYCenter()/25.0D);
		
		x2 = x4;
		y2 = y1;
		
		x3 = x1;
		y3 = y4;
		
		
		eisc = c;
		eisl = l;
	}
	
	
	/**
	 * @author ajulin
	 * @param a x1
	 * @param b y1
	 * @param e x4
	 * @param d y4
	 * @param c inter spot space between 2 columns
	 * @param l inter spot space between 2 lines
	 */
	blok(double a ,double b , double e , double d , double c, double l){
		x1 = a;
		y1 = b;
		x4 = e;
		y4 = d;
		
		x2 = x1;
		y2 = y4;
		
		x3 = x4;
		y3 = y1;
		
		
		eisc = c;
		eisl = l;
	}

	public double getX1() {
		return x1;
	}

	public double getX2() {
		return x2;
	}

	public double getY1() {
		return y1;
	}

	public double getY2() {
		return y2;
	}
	
	
	/**
	 * @author ajulin
	 * @param x pixel's coordinate on x-axis
	 * @param y pixel's coordinate on y-axis
	 * @param vide for two methods (score2 & score3)
	 * @return
	 */
	public boolean appartient(double x , double y,boolean vide){
		if(!vide)
			return ((x < x4+eisc/2) && (x >x1-eisc/2) && (y < y4+eisl/2) && (y > y1-eisl/2));
		else
			return ((x < x4) && (x >x1) && (y < y4) && (y > y1));
	}
	public String toString(){
		
		return "[(x1 = "+x1+" ; "+"y1 = "+y1+" ) : (x4 = "+x4+" ; "+"y4 = "+y4+" )]";
	}

	public double getEisc() {
		return eisc;
	}

	public double getEisl() {
		return eisl;
	}

	public double getX3() {
		return x3;
	}

	public double getX4() {
		return x4;
	}

	public double getY3() {
		return y3;
	}

	public double getY4() {
		return y4;
	}
	
	
}
/*******************************************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 * 
 * Copyright (c) 2007 INRA - SIGENAE TEAM
 * 
 * 
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * provided that the above copyright notice(s) and this permission notice appear
 * in all copies of the Software and that both the above copyright notice(s) and
 * this permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE
 * LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other
 * dealings in this Software without prior written authorization of the
 * copyright holder.
 ******************************************************************************/
