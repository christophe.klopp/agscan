package agscan.algo.globalalignment;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

public class TSpotTree {
	private static int upRow, downRow, leftColumn, rightColumn;
	private double x, y, quality;
	private int column, row;
	private TSpotTree left, right, up, down;
	private boolean dirty;
	
	public TSpotTree(double _x, double _y, int _column, int _row) {
		x = _x;
		y = _y;
		column = _column;
		row = _row;
		quality = 1.0D;
		left = right = up = down = null;
		dirty = false;
	}
	public void setQuality(double _quality) {
		quality = _quality;
	}
	public void setLeft(TSpotTree _spotTree) {
		left = _spotTree;
	}
	public void setRight(TSpotTree _spotTree) {
		right = _spotTree;
	}
	public void setUp(TSpotTree _spotTree) {
		up = _spotTree;
	}
	public void setDown(TSpotTree _spotTree) {
		down = _spotTree;
	}
	public void clean() {
		dirty = false;
	}
	public void cleanAll() {
		dirty = false;
		if ((left != null) && (!left.isClean())) left.cleanAll();
		if ((right != null) && (!right.isClean())) right.cleanAll();
		if ((up != null) && (!up.isClean())) up.cleanAll();
		if ((down != null) && (!down.isClean())) down.cleanAll();
	}
	public boolean isClean() {
		return !dirty;
	}
	public int getRow() {
		return row;
	}
	public int getColumn() {
		return column;
	}
	public TSpotTree getLeft() {
		return left;
	}
	public TSpotTree getRight() {
		return right;
	}
	public TSpotTree getUp() {
		return up;
	}
	public TSpotTree getDown() {
		return down;
	}
	public void afficher(boolean showSons) {
		dirty = true;
		if (showSons) {
			if ((left != null) && (left.isClean())) left.afficher(showSons);
			if ((right != null) && (right.isClean())) right.afficher(showSons);
		}
	}
	public void makeVerticalLinks(double is) {
		Vector v = new Vector();
		int i;
		TSpotTree start = left.left;
		TSpotTree tmp, ts1, ts2;
		
		tmp = start;
		while (tmp != null) {
			v.add(tmp);
			tmp = tmp.up;
		}
		tmp = start.down;
		while (tmp != null) {
			v.add(tmp);
			tmp = tmp.down;
		}
		Object[] list = v.toArray();
		Arrays.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				double y1 = ((TSpotTree)o1).y;
				double y2 = ((TSpotTree)o2).y;
				if (y1 < y2)
					return -1;
				else if (y1 > y2)
					return 1;
				return 0;
			}
			public boolean equals(Object obj) {
				double y1 = ((TSpotTree)obj).y;
				return (y == y1);
			}
		});
		
		Object[] list2 = new Object[list.length];
		for (i = 0; i < list.length; i++) list2[i] = list[i];
		boolean ok = true;
		while (ok) {
			for (i = 0; i < (list.length - 1); i++) {
				ts1 = (TSpotTree)list[i];
				ts2 = (TSpotTree)list[i + 1];
				if (ts1 != null) ts1.setDown(ts2);
				if (ts2 != null) ts2.setUp(ts1);
			}
			ok = false;
			for (i = 0; i < list.length; i++) {
				if (list[i] != null) list[i] = ((TSpotTree)list[i]).getLeft();
				ok = ok | (list[i] != null);
			}
		}
		ok = true;
		while (ok) {
			for (i = 0; i < (list2.length - 1); i++) {
				if (list2[i] != null) ((TSpotTree)list2[i]).setDown((TSpotTree)list2[i + 1]);
				if (list2[i + 1] != null) ((TSpotTree)list2[i + 1]).setUp((TSpotTree)list2[i]);
			}
			ok = false;
			for (i = 0; i < list2.length; i++) {
				if (list2[i] != null) list2[i] = ((TSpotTree)list2[i]).getRight();
				ok = ok | (list2[i] != null);
			}
		}
	}
	public void setLinesAndColumns() {
		TSpotTree tmp = this, tmp2;
		int col = 0, lin = 0;
		leftColumn = rightColumn = upRow = downRow = 0;
		
		while (tmp != null) {
			tmp2 = tmp;
			lin = 0;
			while (tmp2 != null) {
				if (tmp2.quality != 0) {
					if (upRow > lin) upRow = lin;
					if (downRow < lin) downRow = lin;
					if (leftColumn > col) leftColumn = col;
					if (rightColumn < col) rightColumn = col;
				}
				tmp2.column = col;
				tmp2.row = lin--;
				tmp2 = tmp2.up;
			}
			tmp2 = tmp.down;
			lin = 1;
			while (tmp2 != null) {
				if (tmp2.quality != 0) {
					if (upRow > lin) upRow = lin;
					if (downRow < lin) downRow = lin;
					if (leftColumn > col) leftColumn = col;
					if (rightColumn < col) rightColumn = col;
				}
				tmp2.column = col;
				tmp2.row = lin++;
				tmp2 = tmp2.down;
			}
			col--;
			tmp = tmp.left;
		}
		tmp = this.right;
		col = 1;
		while (tmp != null) {
			tmp2 = tmp;
			lin = 0;
			while (tmp2 != null) {
				if (tmp2.quality != 0) {
					if (upRow > lin) upRow = lin;
					if (downRow < lin) downRow = lin;
					if (leftColumn > col) leftColumn = col;
					if (rightColumn < col) rightColumn = col;
				}
				tmp2.column = col;
				tmp2.row = lin--;
				tmp2 = tmp2.up;
			}
			tmp2 = tmp.down;
			lin = 1;
			while (tmp2 != null) {
				if (tmp2.quality != 0) {
					if (upRow > lin) upRow = lin;
					if (downRow < lin) downRow = lin;
					if (leftColumn > col) leftColumn = col;
					if (rightColumn < col) rightColumn = col;
				}
				tmp2.column = col;
				tmp2.row = lin++;
				tmp2 = tmp2.down;
			}
			col++;
			tmp = tmp.right;
		}
	}
	public Vector getLine(int l) {
		Vector v = null;
		TSpotTree start = this, tmp;
		while (l < start.getRow()) {
			start = start.up;
			if (start == null) break;
		}
		if (start != null)
			while (l > start.getRow()) {
				start = start.down;
				if (start == null) break;
			}
		if (start != null) {
			v = new Vector();
			tmp = start;
			while (tmp != null) {
				v.add(tmp);
				tmp = tmp.left;
			}
			tmp = start.right;
			while (tmp != null) {
				v.add(tmp);
				tmp = tmp.right;
			}
		}
		return v;
	}
	public Vector getColumn(int c) {
		Vector v = null;
		TSpotTree start = this, tmp;
		while (c < start.getColumn()) {
			start = start.left;
			if (start == null) break;
		}
		if (start != null)
			while (c > start.getColumn()) {
				start = start.right;
				if (start == null) break;
			}
		if (start != null) {
			v = new Vector();
			tmp = start;
			while (tmp != null) {
				v.add(tmp);
				tmp = tmp.up;
			}
			tmp = start.down;
			while (tmp != null) {
				v.add(tmp);
				tmp = tmp.down;
			}
		}
		return v;
	}
	public int getUpRow() {
		return upRow;
	}
	public int getDownRow() {
		return downRow;
	}
	public int getLeftColumn() {
		return leftColumn;
	}
	public int getRightColumn() {
		return rightColumn;
	}
	public double getQuality() {
		return quality;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public Vector getSpotTrees(int d) {
		Vector v = new Vector();
		TSpotTree start = this, tmp;
		for (int i = 0; i < d; i++) start = start.left.up;
		for (int c = 0; c < (2 * d + 1); c++) {
			tmp = start;
			for (int r = 0; r < (2 * d + 1 ); r++) {
				v.add(tmp);
				tmp = tmp.down;
			}
			start = start.right;
		}
		return v;
	}
	public int getColumnNbSpots(int c) {
		Vector v = getColumn(c);
		TSpotTree tst;
		int n = 0;
		for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
			tst = (TSpotTree)en.nextElement();
			if (tst.getQuality() == 1.0D) n++;
		}
		return n;
	}
	public int getRowNbSpots(int r) {
		Vector v = getLine(r);
		TSpotTree tst;
		int n = 0;
		for (Enumeration en = v.elements(); en.hasMoreElements(); ) {
			tst = (TSpotTree)en.nextElement();
			if (tst.getQuality() == 1.0D) n++;
		}
		return n;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
