package agscan.algo.globalalignment;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.AGScan;
import agscan.algo.factory.TSpotDetectionAlgorithmFactory;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;

public class TEdgesFinderGlobalAlignmentAlgorithm extends TGlobalAlignmentAlgorithm {
	public static final int SEARCH_ZONE = 0;
	public static final int SEARCH_ZONE_INCREMENT = 1;
	public static final int EDGES_SEARCH_ZONE = 2;
	public static final int SEARCH_ANGLE = 3;
	public static final int CONTRAST_PERCENT = 4;
	public static final int SEARCH_ANGLE_INCREMENT = 5;
	public static final int SPOT_DETECTION_ALGORITHM = 6;
	public static final int RESULT = 0;
	
	public static final String NAME = "Edges finder"; //$NON-NLS-1$
	
	//modif 2005/08/16 cf DefaultProperties.java
	public static double sizeSearchZone = AGScan.prop.getDoubleProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZone");
	public static int sizeSearchZoneIncrement = AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZoneIncrement");
	public static int edgesSearchZone = AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.edgesSearchZone");
	public static int searchAngle = AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngle");
	public static int contrastPercent = AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.contrastPercent");
	public static double searchAngleIncrement = AGScan.prop.getDoubleProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngleIncrement");
	public static String spotDetectionAlgorithmName = TFourProfilesSpotDetectionAlgorithm.NAME;
	
	public static Object[] locSpotDetectionAlgorithmParameters = {
			// we must use default values to initialize these parameters
			// we use "4 profils spot detection" default values
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxSearchZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.maxComputeZone")),
			new Integer(AGScan.prop.getIntProperty("TFourProfilesSpotDetectionAlgorithm.qualityThreshold"))
	};
	
	public static double lastSizeSearchZone = sizeSearchZone;
	public static int lastSizeSearchZoneIncrement = sizeSearchZoneIncrement;
	public static int lastEdgesSearchZone = edgesSearchZone;
	public static int lastSearchAngle = searchAngle;
	public static int lastContrastPercent = contrastPercent;
	public static double lastSearchAngleIncrement = searchAngleIncrement;
	public static String lastSpotDetectionAlgorithmName = spotDetectionAlgorithmName;
	public static Object[] lastLocSpotDetectionAlgorithmParameters;
	
	static {
		lastLocSpotDetectionAlgorithmParameters = new Object[locSpotDetectionAlgorithmParameters.length];
		for (int i = 0; i < locSpotDetectionAlgorithmParameters.length; i++)
			lastLocSpotDetectionAlgorithmParameters[i] = locSpotDetectionAlgorithmParameters[i];
	}
	
	public TEdgesFinderGlobalAlignmentAlgorithm() {
		super();
		parameters = new Object[7];
		results = new Object[1];
		initWithLast();
		controlPanel = TEdgesFinderGlobalAlignmentAlgorithmPanel.getInstance(this);
	}
	public static String getName() {
		return NAME;
	}
	public String getStringParams() {
		String s = "";
		s = getParameter(SEARCH_ZONE).toString() + "|" + getParameter(SEARCH_ZONE_INCREMENT).toString() + "|" + getParameter(EDGES_SEARCH_ZONE).toString() +
		"|" + getParameter(SEARCH_ANGLE).toString() + "|" + getParameter(CONTRAST_PERCENT).toString() + "|" +
		getParameter(SEARCH_ANGLE_INCREMENT).toString() + "|" + ((TSpotDetectionAlgorithm)getParameter(SPOT_DETECTION_ALGORITHM)).getStringParams();
		return s;
	}
	public void init() {
		setParameter(SEARCH_ZONE, new Double(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSizeSearchZone()));
		setParameter(SEARCH_ZONE_INCREMENT, new Integer(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSizeSearchZoneIncrement()));
		setParameter(EDGES_SEARCH_ZONE, new Integer(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getEdgesSearchZone()));
		setParameter(SEARCH_ANGLE, new Integer(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle()));
		setParameter(CONTRAST_PERCENT, new Integer(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getContrastPercent()));
		setParameter(SEARCH_ANGLE_INCREMENT, new Double(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngleIncrement()));
		TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSpotDetectionAlgorithmName(), false);
		sdAlgo.init();
		setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
	}
	public void initWithDefaults() {
		setParameter(SEARCH_ZONE, new Double(sizeSearchZone));
		setParameter(SEARCH_ZONE_INCREMENT, new Integer(sizeSearchZoneIncrement));
		setParameter(EDGES_SEARCH_ZONE, new Integer(edgesSearchZone));
		setParameter(SEARCH_ANGLE, new Integer(searchAngle));
		setParameter(CONTRAST_PERCENT, new Integer(contrastPercent));
		setParameter(SEARCH_ANGLE_INCREMENT, new Double(searchAngleIncrement));
		TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(spotDetectionAlgorithmName, true);
		sdAlgo.init(locSpotDetectionAlgorithmParameters);
		setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
	}
	public void init(Object[] params) {
		setParameter(SEARCH_ZONE, params[0]);
		setParameter(SEARCH_ZONE_INCREMENT, params[1]);
		setParameter(EDGES_SEARCH_ZONE, params[2]);
		setParameter(SEARCH_ANGLE, params[3]);
		setParameter(CONTRAST_PERCENT, params[4]);
		setParameter(SEARCH_ANGLE_INCREMENT, params[5]);
		TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(params[6], true);
		sdAlgo.init((Object[])params[7]);
		setParameter(SPOT_DETECTION_ALGORITHM, sdAlgo);
		setLasts();//added 2005/09/13
	}
	public void setLasts() {
		lastSizeSearchZone = ((Double)parameters[SEARCH_ZONE]).doubleValue();
		lastSizeSearchZoneIncrement = ((Integer)parameters[SEARCH_ZONE_INCREMENT]).intValue();
		lastEdgesSearchZone = ((Integer)parameters[EDGES_SEARCH_ZONE]).intValue();
		lastSearchAngle = ((Integer)parameters[SEARCH_ANGLE]).intValue();
		lastSearchAngleIncrement = ((Double)parameters[SEARCH_ANGLE_INCREMENT]).doubleValue();
		lastContrastPercent = ((Integer)parameters[CONTRAST_PERCENT]).intValue();
		lastSpotDetectionAlgorithmName = ((TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM]).getName();
		lastLocSpotDetectionAlgorithmParameters = ((TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM]).getParameters();
	}
	public void initWithLast() {
		parameters[SEARCH_ZONE] = new Double(lastSizeSearchZone);
		parameters[SEARCH_ZONE_INCREMENT] = new Integer(lastSizeSearchZoneIncrement);
		parameters[EDGES_SEARCH_ZONE] = new Integer(lastEdgesSearchZone);
		parameters[SEARCH_ANGLE] = new Integer(lastSearchAngle);
		parameters[SEARCH_ANGLE_INCREMENT] = new Double(lastSearchAngleIncrement);
		parameters[CONTRAST_PERCENT] = new Integer(lastContrastPercent);
		TSpotDetectionAlgorithmFactory factory = new TSpotDetectionAlgorithmFactory();
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)factory.createAlgorithm(lastSpotDetectionAlgorithmName, true);
		sdAlgo.init(lastLocSpotDetectionAlgorithmParameters);
		parameters[SPOT_DETECTION_ALGORITHM] = sdAlgo;
	}
	
	/*
	 * method added 2005/08/17
	 * if values are changed, we save them into the properties file
	 * @see agscan.algo.globalalignment.TGlobalAlignmentAlgorithm#saveInProperties()
	 */
	public void saveInProperties(){
		int panelIntValue;
		double panelDoubleValue;
		// sizeSearchZone
		panelDoubleValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSizeSearchZone();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZone")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZone",panelDoubleValue); 	
		}
		//sizeSearchZoneIncrement
		panelIntValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSizeSearchZoneIncrement();
		if (panelIntValue != AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZoneIncrement")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.sizeSearchZoneIncrement",panelIntValue); 	
		}
		//edgesSearchZone
		panelIntValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getEdgesSearchZone();
		if (panelIntValue != AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.edgesSearchZone")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.edgesSearchZone",panelIntValue); 	
		}
		//searchAngle 
		panelIntValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngle();
		if (panelIntValue != AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngle")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngle",panelIntValue); 	
		}
		//contrastPercent
		panelIntValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getContrastPercent();
		if (panelIntValue != AGScan.prop.getIntProperty("TEdgesFinderGlobalAlignmentAlgorithm.contrastPercent")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.contrastPercent",panelIntValue); 	
		}
		//searchAngleIncrement
		panelDoubleValue = ((TEdgesFinderGlobalAlignmentAlgorithmPanel)controlPanel).getSearchAngleIncrement();
		if (panelDoubleValue != AGScan.prop.getDoubleProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngleIncrement")){
			AGScan.prop.setProperty("TEdgesFinderGlobalAlignmentAlgorithm.searchAngleIncrement",panelDoubleValue); 	
		}
		// for maxSearchZone & maxComputeZone & qualityThreshold : intern call...
		getSpotDetectionAlgorithm().saveInProperties();
		
	}
	
	public void execute(boolean thread) {
		worker = new TEdgesFinderGlobalAlignmentWorker(this);
		worker.setPriority(Thread.MIN_PRIORITY);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}
	
	public void execute(boolean thread, int priority) {
		worker = new TEdgesFinderGlobalAlignmentWorker(this);
		worker.setPriority(priority);
		if (thread)
			worker.start();
		else {
			worker.construct(false);
			worker.finished();
		}
	}
	
	public TSpotDetectionAlgorithm getSpotDetectionAlgorithm() {
		TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)parameters[SPOT_DETECTION_ALGORITHM];
		return sdAlgo;
	}
	
	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TEdgesFinderGlobalAlignmentAlgorithm.1"), //$NON-NLS-1$
				Messages.getString("TEdgesFinderGlobalAlignmentAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (rep == JOptionPane.YES_OPTION) {
			worker.stop();
		}
	}
	
	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			worker.setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			worker.setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			worker.setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}
	
	public double getDefaultSizeSearchZone() {
		return sizeSearchZone;
	}
	
	public int getDefaultSizeSearchZoneIncrement() {
		return sizeSearchZoneIncrement;
	}
	
	public int getDefaultEdgesSearchZone() {
		return edgesSearchZone;
	}
	
	public int getDefaultSearchAngle() {
		return searchAngle;
	}
	
	public String getDefaultSpotDetectionAlgorithmName() {
		return spotDetectionAlgorithmName;
	}
	
	public int getDefaultContrastPercent() {
		return contrastPercent;
	}
	
	public double getDefaultSearchAngleIncrement() {
		return searchAngleIncrement;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
