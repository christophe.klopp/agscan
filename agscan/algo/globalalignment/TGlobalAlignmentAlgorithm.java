package agscan.algo.globalalignment;

import ij.ImagePlus;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Vector;

import javax.swing.JComponent;

import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.TAlgorithmListener;
import agscan.algo.localalignment.TGridAlignmentAlgorithm;
import agscan.algo.localalignment.TGridBlockAlignmentAlgorithm;
import agscan.algo.localalignment.TLevel1BlockAlignmentAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;

public abstract class TGlobalAlignmentAlgorithm extends TAlgorithmListener {
	public static class TImageView extends JComponent {
		//PlanarImage image; //retire le 25/08/05
		ImagePlus image;
		/* public TImageView(PlanarImage pi) {
		 image = pi;
		 }
		 */
		public TImageView(ImagePlus pi) {
			image = pi;
		}
		
		public void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D)g;
			//   g2d.drawRenderedImage(image, new AffineTransform()); //retir le 25/08/05
			g2d.drawImage(image.getProcessor().createImage(), new AffineTransform(),null); //remplace...
			setPreferredSize(new Dimension((int)(image.getWidth()), (int)(image.getHeight())));
			setSize(new Dimension((int)(image.getWidth()), (int)(image.getHeight())));
		}
	}
	public static final int IMAGE_WIDTH_IN_PIXELS = 0;
	public static final int IMAGE_HEIGHT_IN_PIXELS = 1;
	public static final int IMAGE_DATA = 2;
	public static final int HISTOGRAM_MIN = 3;
	public static final int HISTOGRAM_MAX = 4;
	public static final int XRES = 5;
	public static final int YRES = 6;
	public static final int ALIGNMENT_MODEL = 7;
	
	protected SwingWorker worker;
	protected int imagesMin;//number min of images that this plugin needs to perform quantification
	protected int imagesMax;//number max of images that this plugin can accept to perform quantification
		
		  /**
			 * For example a alignment for a fluorescent experiment needs a minimum of 2 images
			 * @return the number min of images that this algo needs to perform alignment
			 * @since 2005/10/19
			 */
			public int getImagesMin() {
				return imagesMin;
			}
			
			/**
			 * 
			 * @param imagesMin the number min of images that this algo needs to perform alignment
			 * @since 2005/10/19
			 */
			public void setImagesMin(int imagesMin) {
				this.imagesMin = imagesMin;
			}
			
			/**
			 * For example a alignment for a radio experiment needs a maximum of 1 image
			 * @return number max of images that this algo can accept to perform alignment
			 * @since 2005/10/19
			 */
			public int getImagesMax() {
				return imagesMax;		
			}
			
			/**
			 * 
			 * @param imagesMax  number max of images that this algo can accept to perform alignment
			 * @since 2005/10/19
			 */
			public void setImagesMax(int imagesMax) {
				this.imagesMax = imagesMax;
			}	
	
	public TGlobalAlignmentAlgorithm() {
		//2005/09/13 modified (before no param to the constructor)
		super(Messages.getString("TGlobalAlignmentAlgorithm.0"));
		workingData = new Object[8];
	}
	public void initData(double xRes, double yRes, int[] imageData, int imageWidthInPixels, int imageHeightInPixels,
			TAlignmentModel alignmentModel, int histoMin, int histoMax) {
		workingData[XRES] = new Double(xRes);
		workingData[YRES] = new Double(yRes);
		workingData[IMAGE_DATA] = imageData;
		workingData[IMAGE_WIDTH_IN_PIXELS] = new Integer(imageWidthInPixels);
		workingData[IMAGE_HEIGHT_IN_PIXELS] = new Integer(imageHeightInPixels);
		workingData[ALIGNMENT_MODEL] = alignmentModel;
		workingData[HISTOGRAM_MIN] = new Integer(histoMin);
		workingData[HISTOGRAM_MAX] = new Integer(histoMax);
	}
	public void setPriority(int priority) {
		worker.setPriority(priority);
	}
	public abstract String getStringParams();
	public int[] makeImageSeuil() {
		double xRes = ((Double)getWorkingData(TGlobalAlignmentAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)getWorkingData(TGlobalAlignmentAlgorithm.YRES)).doubleValue();
		TAlignmentModel alignmentModel = (TAlignmentModel)getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL);
		
		xRes = alignmentModel.getImageModel().getPixelWidth();
		yRes = alignmentModel.getImageModel().getPixelHeight();
		int nbColSpots = alignmentModel.getGridModel().getRootBlock().getNbColumnsInSpots();
		int nbRowSpots = alignmentModel.getGridModel().getRootBlock().getNbRowsInSpots();
		double spotWidth = alignmentModel.getGridModel().getConfig().getSpotsWidth();
		double spotHeight = alignmentModel.getGridModel().getConfig().getSpotsHeight();
		int[] imageData = (int[])getWorkingData(TGlobalAlignmentAlgorithm.IMAGE_DATA);
		int histoSeuil = imageData.length - (int)(nbColSpots * spotWidth / xRes * nbRowSpots * spotHeight / yRes);
		int[] histo = ((TImage)alignmentModel.getImageModel().getReference()).getHistogramData();
		int[] contrastedImageData = new int[imageData.length];
		int seuil;
		for (seuil = 255; seuil >= 0; seuil--) {
			histoSeuil -= histo[seuil];
			if (histoSeuil <= 0) break;
		}
		seuil *= 256;
		double A = 65535.0D / (double)seuil;
		
		for (int i = 0; i < imageData.length; i++)
			if ((~imageData[i] & 0xFFFF) > seuil)
				contrastedImageData[i] = 65535;
			else
				contrastedImageData[i] = (int)(A * (double)(~imageData[i] & 0xFFFF));
		return contrastedImageData;
	}
	
	public void makeVerticalProfile(int[] imageTab, int[] vertProf, int[] vertDiff, int w, int h, int k) {
		double A, B, C;
		int yd, nb;
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		A = (double)k / (w - 1);
		C = -A;
		for (int y = 0; y < h; y++) {
			B = y - A * C * y;
			vertProf[(int)y] = 0;
			nb = 0;
			for (int x = 0; x < w; x++) {
				yd = (int)(A * x + B);
				if ((yd >= 0) && (yd < h)) {
					vertProf[(int)y] += ~imageTab[(int)x + yd * w] & 0xFFFF;
					nb++;
				}
			}
			if (nb > 0) vertProf[(int)y] /= nb;
		}
		for (int j = 0; j < h - 1; j++) vertDiff[j] = vertProf[j + 1] - vertProf[j];
		try {
			BufferedWriter fos = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("profile.vprf")));
			for (int u = 0; u < vertProf.length; u++) {
				fos.write(u + " " + vertProf[u]);
				fos.newLine();
			}
			fos.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		worker.setPcDone(worker.getPcDone() + 1);
		
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS,
				aliView.getReference(), new Integer((int)worker.getPcDone()), new Integer(4)));
	}
	
	
	
	/** Calcul de la projection d'une image au format paysage (w > h) sur differents
	 *  axes calcules en fonction des parametres deb et fin. La valeur de retour
	 *  correspond a l'axe sur lequel la projection de l'image donne le moins de pics.
	 *  imageTab : tableau contenant les donnees de l'image
	 *  plotProf : profil presentant le moins de pics
	 *  plotDiff : differences 2 a 2 successives des elements de plotProf
	 *  w, h : largeur et hauteur de l'image en pixels
	 *  deb, fin : axes de projection, correspondants aux droites passant par
	 *             le point (0,0) et les points (w-1, deb) et (w-1, fin)
	 *  step : le pas de parcours des axes
	 */
	public int makeHorizontalProfile(String filename, int[] imageTab, int[] plotProf, int[] plotDiff,
			double w, double h, int deb, int fin, int step, boolean affine) {
		double A, B, C, D, x, y;
		int nb, xd, i, j, nbMin, nbMinMin = 1000000, iMin = -1;
		int[][] testProf = new int[fin - deb + 1][(int)w];
		BufferedWriter fos = null;
		int[] nbMinTab = new int[(fin - deb) / step + 1];
		double pb_step;
		TAlignmentView aliView = (TAlignmentView)((TAlignmentModel)getWorkingData(
				TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL)).getReference().getView();
		
		if (fin == deb)
			pb_step = 0;
		else if (affine)
			pb_step = 2.0D / (double)((fin - deb) / step);
		else
			pb_step = 6.0D / (double)((fin - deb) / step);
		try {
			// browsing of every axis between "deb" and "fin" with a step of "step" pixels
			for (i = deb; (i <= fin) && !worker.getSTOP(); i += step) {
				// axis equation : y = Ax + B
				A = (double)i / (w - 1);
				B = 0;
				
				// orthogonal axis equation: x = C y + D
				C = -A;
				D = 0;
				for (x = 0; x < w; x ++) {
					// For each x, we calculate the point on the axis
					y = A * x;
					
					// the D value for the orthogonal axis is deduced
					D = x - C * y;
					
					testProf[i - deb][(int)(x)] = 0;
					nb = 0;
					for (y = 0; y < h; y += 2) {
						// for each y, we calculate the corresponding point on the orthogonal axis
						xd = (int)(C * y + D);
						
						// If the point is in the image,its value will be considered into the projection on the x axis
						if ((xd >= 0) && (xd < w)) {
							testProf[i - deb][(int)(x)] += (~imageTab[xd + (int)(y * w)] & 0xFFFF);
							nb++;
						}
						else {
							testProf[i - deb][(int)(x)] += 65535;
							nb++;
						}
					}
					// To finish, the projection value on x is the mean value of all the projection points 					
					if (nb > 0) testProf[i - deb][(int)(x)] /= nb;
				}
				if (plotDiff != null) {
					// We compute plotDiff, doing successive differences of testProf elements
					for (j = 0; j < (w - 1); j++) plotDiff[j] = testProf[i - deb][j + 1] - testProf[i - deb][j];
					nbMin = 0;
					
					// We compute thanks to plotDiff the number of   the number of hollows throughout  testProf
					for (j = 0; j < (w - 2); j++)
						if ((plotDiff[j] < 0) && (plotDiff[j + 1] > 0)) nbMin++;
						
						//  Ones preserves the plotProf projection given the minimum of hollow 
						//  and the corresponding orientation is given
					if (nbMinMin > nbMin) {
						nbMinMin = nbMin;
						iMin = i;
					}
					nbMinTab[(i-deb)/step] = nbMin;
				}
				else
					iMin = i;
				worker.setPcDone(worker.getPcDone() + pb_step);
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
						new Integer((int)worker.getPcDone()), new Integer(4)));
			}
			
			if (worker.getSTOP()) return 0;
			
			makeWindowMoy(nbMinTab, 5);
			iMin = 0;
			nbMin = nbMinTab[0];
			for (i = 5; i < nbMinTab.length - 5; i++)
				if (nbMinTab[i] < nbMin) {
					nbMin = nbMinTab[i];
					iMin = i;
				}
			int iMin2 = iMin;
			for (i = iMin + 1; i < nbMinTab.length - 5; i++)
				if (nbMinTab[i] == nbMin)
					iMin2 = i;
				else
					break;
			iMin = (int)((iMin + iMin2) / 2 * step + deb);
			for (i = 0; i < plotProf.length; i++) plotProf[i] = testProf[iMin - deb][i];
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		if (affine)
			worker.setPcDone(8);
		else
			worker.setPcDone(6);
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, 
				aliView.getReference(), new Integer((int)worker.getPcDone()), new Integer(4)));
		return iMin;
	}
	/** 
	 * Lisse les valeurs d'un tableau en moyennant sur une fenetre de 2 * w points de large 
	 * */
	protected void makeWindowMoy(int[] tab, int w) {
		double sum;
		int i, k;
		for (k = w; k < (tab.length - w); k++) {
			sum = 0;
			for (i = k - w; i < k + w; i++) sum += tab[i];
			sum /= (2 * w);
			tab[k] = (int)sum;
		}
	}
	protected double getAngle(Vector v) {
		double sx2 = 0, sy = 0, sx = 0, sxy = 0;
		int n = 0;
		TSpot sp;
		for (int i = 0; i < (v.size() * 2 / 5); i++) {
			sp = (TSpot)v.elementAt(i);
			if (sp.getQuality() == 1.0D) {
				sx2 += sp.getXCenter() * sp.getXCenter();
				sy += sp.getYCenter();
				sx += sp.getXCenter();
				sxy += sp.getXCenter() * sp.getYCenter();
				n++;
			}
		}
		return (n * sxy - sx * sy) / (n * sx2 - sx * sx);
	}
	
	protected void align(TLevel1BlockAlignmentAlgorithm l1baAlgo, int bootstrapNb, int firstRunNb, int secondRunNb, int thirdRunNb,
			int qThreshold) {
		double moveSum;
		TAlignmentModel alignmentModel = (TAlignmentModel)getWorkingData(TGlobalAlignmentAlgorithm.ALIGNMENT_MODEL);
		TAlignmentView aliView = (TAlignmentView)alignmentModel.getReference().getView();
		l1baAlgo.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(true));
		l1baAlgo.setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(20));
		l1baAlgo.getSpotDetectionAlgorithm().setParameter(TFourProfilesSpotDetectionAlgorithm.QUALITY_THRESHOLD, new Integer(qThreshold));
		l1baAlgo.initSpotsData();
		for (int i = 0; i < bootstrapNb; i++) {
			l1baAlgo.execute(false);
		}
		aliView.getGraphicPanel().refresh();
		if (worker.getPcDone() < 15) {
			worker.setPcDone(worker.getPcDone() + 1.25);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
					new Integer( (int) worker.getPcDone()), new Integer(4)));
		}
		l1baAlgo.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
		l1baAlgo.setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(30));
		for (int i = 0; i < firstRunNb && !worker.getSTOP(); i++) {
			l1baAlgo.execute(false);
			moveSum = ((Double)l1baAlgo.getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
			if (moveSum < 0.00001) break;
		}
		aliView.getGraphicPanel().refresh();
		if (worker.getPcDone() < 15) {
			worker.setPcDone(worker.getPcDone() + 1.25);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
					new Integer((int)worker.getPcDone()), new Integer(4)));
		}
		l1baAlgo.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
		l1baAlgo.setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(20));
		for (int i = 0; i < secondRunNb && !worker.getSTOP(); i++) {
			l1baAlgo.execute(false);
			moveSum = ((Double)l1baAlgo.getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
			if (moveSum < 0.00001) break;
		}
		aliView.getGraphicPanel().refresh();
		if (worker.getPcDone() < 15) {
			worker.setPcDone(worker.getPcDone() + 1.25);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
					new Integer((int)worker.getPcDone()), new Integer(4)));
		}
		l1baAlgo.setWorkingData(TGridBlockAlignmentAlgorithm.ALL_GOOD, new Boolean(false));
		l1baAlgo.setParameter(TGridAlignmentAlgorithm.SENSITIVITY, new Integer(10));
		for (int i = 0; i < thirdRunNb && !worker.getSTOP(); i++) {
			l1baAlgo.execute(false);
			moveSum = ((Double)l1baAlgo.getResult(TGridAlignmentAlgorithm.RESULT)).doubleValue();
			if (moveSum < 0.00001) break;
		}
		aliView.getGraphicPanel().refresh();
		if (worker.getPcDone() < 15) {
			worker.setPcDone(worker.getPcDone() + 1.25);
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, aliView.getReference(),
					new Integer((int)worker.getPcDone()), new Integer(4)));
		}
	}
	
	public abstract void saveInProperties();// added 2005/08/17
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
