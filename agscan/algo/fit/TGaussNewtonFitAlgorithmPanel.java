package agscan.algo.fit;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;

public class TGaussNewtonFitAlgorithmPanel extends TAlgorithmControlPanel {
	private JSpinner nbIterationsSpinner;
	private static TGaussNewtonFitAlgorithmPanel instance = null;
	
	public TGaussNewtonFitAlgorithmPanel(TGaussNewtonFitAlgorithm algo) {
		super();
		nbIterationsSpinner = new JSpinner(new SpinnerNumberModel(new Integer(algo.getDefaultNbIterations()), new Integer(2),
				new Integer(100), new Integer(1)));
		jbInit();
	}
	public void init(TAlgorithm algo) {
		TGaussNewtonFitAlgorithm alg = (TGaussNewtonFitAlgorithm)algo;
		nbIterationsSpinner.setValue(alg.getParameter(TGaussNewtonFitAlgorithm.NB_ITERATIONS));
	}
	private void jbInit() {
		removeAll();
		JLabel nbIterationsLabel = new JLabel(Messages.getString("TGaussNewtonFitAlgorithmPanel.0")); //$NON-NLS-1$
		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new GridBagLayout());
		add(nbIterationsLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 10, 0), 0, 0));
		add(nbIterationsSpinner, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 10, 10), 20, 0));
	}
	public int getNbIterations() {
		return ((Integer)nbIterationsSpinner.getValue()).intValue();
	}
	public static TGaussNewtonFitAlgorithmPanel getInstance(TGaussNewtonFitAlgorithm algo) {
		if (instance == null)
			instance = new TGaussNewtonFitAlgorithmPanel(algo);
		else
			instance.jbInit();
		instance.init(algo);
		return instance;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
