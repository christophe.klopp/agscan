package agscan.algo.fit;

import ij.ImageStack;
import ij.process.ImageProcessor;

import java.util.Vector;

import agscan.algo.TAlgorithm;
import agscan.data.controler.TImageControler;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;

public abstract class TFitAlgorithm extends TAlgorithm {
	public static final int XRES = 0;
	public static final int YRES = 1;
	public static final int ALIGNMENT_MODEL = 2;
	public static final int IMAGE_WIDTH_IN_PIXELS = 3;
	public static final int IMAGE_HEIGHT_IN_PIXELS = 4;
	
	protected TAlignmentModel alignmentModel;//added 2005/09/16
	protected Vector dataVector;// added 2005/10/13: contains tabs of imageDataPSL
	protected double[] imageDataPSL;//added 2005/09/16 // modified 2005/09/21: double instead of int 
	
	public TFitAlgorithm() {
		super();
		workingData = new Object[5];
	}
	
	/*
	 * initData
	 * modified 2005/09/21: PSL data are available here
	 * modified 2005/10/13: several channels modification (usage of a stack of images)
	 * 
	 */
	public void initData(TAlignmentModel aligntModel) {
		dataVector = new Vector();
		workingData[ALIGNMENT_MODEL] = aligntModel;
		workingData[XRES] = new Double(aligntModel.getPixelWidth());
		workingData[YRES] = new Double(aligntModel.getPixelHeight());
		workingData[IMAGE_WIDTH_IN_PIXELS] = new Integer(aligntModel.getElementWidth());
		workingData[IMAGE_HEIGHT_IN_PIXELS] = new Integer(aligntModel.getElementHeight());
		alignmentModel = (TAlignmentModel)workingData[ALIGNMENT_MODEL];//moved 2005/09/16
		
		//added 2005/09/21: we get data directly in PSL unit (data are converted here if unit image is QL)
		// because fit algorithms works with PSL data.
		// Note that in BZScan, for a pixel the QL and the PSL values were stocked in the same int but in the light low-order bit 
		// and the high-order bit. In AGScan we doesnt stock these two different values. 
		// That's why we convert and stock only unit values used here in order to easily use them.
		
		//TODO all channels of the stack are prepared but can we use this algofor other thing that radio?
		ImageStack images =  alignmentModel.getImageModel().getImages();  	//2005/10/14
		ImageProcessor currentProc = null;//2005/10/14:we work on the ImageProcessor 
		if (alignmentModel.getImageModel().getUnit().equals("QL")){
			for (int index=1;index<=images.getSize();index++){
				currentProc = images.getProcessor(index);//2005/10/14
				int[] imageDataQL =ImagePlusTools.getImageData(currentProc,ImagePlusTools.DEPTH_16_BITS);//modif 2005/10/14
				//TODO see how many and where are conversion method because they are several(TImageControler, TImageModelFactory...)
				double latitude = ImagePlusTools.getLatitude();
				// QL=>PSL conversion
				imageDataPSL = TImageControler.ql2pslData(imageDataQL, latitude);
				dataVector.add(imageDataPSL);
			}
			
		}
		else {//PSL image: decimal data
			//TODO voir si c'est normal de se poser ici la question qu'une image soit en PSL ou QL sous entendu les data sont toujours en QL ou plutot au format 16 bits + generalement et c'est tout
			//TODO on peut alors considerer le passage en PSL comme une conversion pre-quantif = preparation aux quantifs
			for (int index=1;index<=images.getSize();index++){//2005/10/13
				currentProc = images.getProcessor(index);  	//2005/10/14
				int[] temp = ImagePlusTools.getImageData(currentProc,ImagePlusTools.DEPTH_16_BITS);//2005/10/14
				imageDataPSL = new double[temp.length];
				//creation of the same tab but in double
				//TODO is it useful? think about conversions, int and double!!!
				for (int i=0;i< imageDataPSL.length;i++)    {
					imageDataPSL[i]=temp[i];
					dataVector.add(imageDataPSL);//2005/10/13
				}
			}
		}
	}
	public void computeDiameter(TSpot spot, double maxDiam) {
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double diam;
		
		if (spot.getFitAlgorithm() == null)
			computeFit(spot);
		else if (!spot.getFitAlgorithm().getName().equals(getName()))
			computeFit(spot);
		if (((Double)spot.getFitParameter("B")).doubleValue() != 0)
			diam = ((Double)spot.getFitParameter("A")).doubleValue() / ((Double)spot.getFitParameter("B")).doubleValue() * 1.5D;
		else
			diam = 0;
		// System.out.println("A="+((Double)spot.getFitParameter("A")).doubleValue());
		// System.out.println("B="+((Double)spot.getFitParameter("B")).doubleValue());
		// System.out.println("diam="+diam);
		diam = Math.max(diam, 0.0D);
		diam = Math.sqrt(diam);
		diam *= 2.0D * xRes;
		if (diam <= 0) diam = 0;
		if (spot.getQuality() == 0) {
			diam = Math.min(diam, spot.getSpotWidth());
			diam = Math.min(diam, spot.getSpotHeight());
		}
		if (diam > maxDiam) diam = maxDiam;
		diam = (int)(diam * 100000) / 100000.0D;
		spot.setComputedDiameter(diam);
	}
	
	public abstract String getName();
	public abstract double[] getFitData(TSpot spot);//, int indexImage);//modif 2005/10/13
	public abstract void computeFit(TSpot spot);
	public abstract String getParameters(TSpot spot);
	public abstract void setParameters(TSpot spot, String params);
	public double getFitCorrection(TSpot spot) {
		return 0;
	}
	public double getQCouronne(TSpot spot) {
		return 0;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 * 
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
