package agscan.algo.fit;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import Jama.Matrix;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
//compatible only with PSL data since 2005/09/21
public class TGaussNewtonFitAlgorithm extends TFitAlgorithm {
	public static final int NB_ITERATIONS = 0;
	public static final String NAME = "Fit Gauss-Newton";
	private static int nbIterations = 20;
	private static int lastNbIterations = nbIterations;
	
	private double cii = 0;
	public WritableRaster diffRaster, thRaster;
	
	public TGaussNewtonFitAlgorithm() {
		super();
		parameters = new Object[1];
		initWithDefaults();
		controlPanel = TGaussNewtonFitAlgorithmPanel.getInstance(this);
		initWithLast();
	}
	public void init(Object[] params) {
		setParameter(NB_ITERATIONS, params[0]);
	}
	public void setLasts() {
		lastNbIterations = ((Integer)parameters[NB_ITERATIONS]).intValue();
	}
	public void initWithLast() {
		parameters[NB_ITERATIONS] = new Integer(lastNbIterations);
	}
	public void initWithDefaults() {
		setParameter(NB_ITERATIONS, new Integer(nbIterations));
	}
	public void init() {
		setParameter(NB_ITERATIONS, new Double(((TGaussNewtonFitAlgorithmPanel)controlPanel).getNbIterations()));
	}
	public void execute(boolean thread) {
		computeCii();
		computeFit();
		makeThRaster();
	}
	public void execute(TSpot spot) {
		if (cii == 0) computeCii();
		computeFit(spot);
	}
	public String getName() {
		return NAME;
	}
	public int getDefaultNbIterations() {
		return nbIterations;
	}
	
	/*
	 * ComputeCii is a function that compute a parameter for the fit formula like a background signal.
	 *  the cii parameter is used for initialize the C fit parameter
	 * this method only use 2 spots of the grid: the first and the last
	 * it seems that the cii value is the mean value of the intensity of 2 areas of 5 pixels into these 2 spots  
	 */
	//TODO or NOT? this parameter must be computed for each image of the vector
	private void computeCii() {
		TSpot spot;
		int xCenter, yCenter;
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		TGridModel gridModel = ((TAlignment)alignmentModel.getReference()).getGridModel();
		int imageWidth = ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue();
		int imageHeight = ((Integer)workingData[IMAGE_HEIGHT_IN_PIXELS]).intValue();
		int nb = 0;
		spot = (TSpot)gridModel.getSpots().firstElement();
		xCenter = (int)((spot.getXCenter() - spot.getSpotWidth()) / xRes);
		yCenter = (int)((spot.getYCenter() - spot.getSpotHeight()) / yRes);
		for ( int ii = 0; ii < Math.min( 5, xCenter ); ii++ )
			for (int jj = 0; jj < Math.min( 5, yCenter ); jj++ ) {
				cii += imageDataPSL[(yCenter - jj) * imageWidth + xCenter - ii];//modif 2005/09/21
				nb++;
			}
		spot = (TSpot)gridModel.getSpots().lastElement();
		xCenter = (int)((spot.getXCenter() + spot.getSpotWidth()) / xRes);
		yCenter = (int)((spot.getYCenter() + spot.getSpotHeight()) / yRes);
		for ( int ii = 0; ii < Math.min(5, imageWidth - xCenter); ii++)
			for (int jj = 0; jj < Math.min(5, imageHeight - yCenter); jj++) {
				//modif remi 2005/09/21
				/*if (unit.equals("PSL"))
				 //cii += ((imageData[(yCenter + jj) * imageWidth + xCenter + ii] >> 16) & 0xFFFF);//remi modif 20/09/05
				  cii += TImageModelFactory.ql2psl(((imageData[(yCenter + jj) * imageWidth + xCenter + ii])), 65536,lat);//remi modi 20/09/05
				  else
				  //cii += ((imageData[(yCenter - jj) * imageWidth + xCenter - ii] >> 16) & 0xFFFF) * Math.pow(10.0D, lat / 2.0D) / 65536.0D;//remi modif 20/09/05
				   cii += TImageModelFactory.ql2psl(((imageData[(yCenter - jj) * imageWidth + xCenter - ii])), 65536,lat) * Math.pow(10.0D, lat / 2.0D) / 65536.0D;//remi modif 20/09/05
				   */
				//TODO probalby a little pb into bzscan: verify how formula above works because: why have we two cases: yCenter + jj et yCenter -jj
				// in the if/else? If it is just a conversion, why is the calcul different?
				cii += imageDataPSL[(yCenter + jj) * imageWidth + xCenter + ii];//modif 2005/09/21
				nb++;
			}
		cii /= nb;
	}
	public void computeFit(TSpot spot) {
		Vector pixels;
		int xCenter, yCenter;
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		pixels = spot.getPoints2D(xRes, yRes);
		xCenter = (int)(spot.getXCenter() / xRes);
		yCenter = (int)(spot.getYCenter() / yRes);
		int nb = 0;
		double Ainit, Binit, Cinit;
		double Xi, A, B, c, psl;
		Ainit = Binit = A = B = 0;
		Cinit = c = cii;
		int imageWidth = ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue();
		Enumeration enume;
		Point2D.Double point;
		double interSpot = alignmentModel.getGridModel().getConfig().getSpotsWidth();
		
		psl = imageDataPSL[yCenter * imageWidth + xCenter];//added 2005/09/21
		if (psl <= c) {
			Ainit = A = 65535;
			Binit = B = 0;
		}
		else {
			Ainit = A = Math.pow(psl - c, -2.0D/3.0D);
			enume = pixels.elements();
			nb = 0;
			while (enume.hasMoreElements()) {
				point = (Point2D.Double)enume.nextElement();
				Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
				if (Xi > Math.pow(0.5D * interSpot / xRes, 2.0D)) {
					psl =imageDataPSL[(int)point.getY() * imageWidth + (int)point.getX()];//modif 2005/09/21
					if (((psl - c) < Math.pow(A, -3.0D/2.0D)) && ((psl - c) > 0))
						B += (Math.pow(psl - c, -2.0D/3.0D) - A) / Xi;
					else
						B += 1.0D / Xi;
					nb++;
				}
			}
			B /= nb;
			Binit = B;
		}
		double[] inc = null;
		int k;
		for (k = 0; k < 50; k++) {
			inc = gaussNewton(A, B, c, spot, imageDataPSL, xRes, yRes, 65536, imageWidth, interSpot);
			if ((Math.abs(inc[0] / A) + Math.abs(inc[1] / B) + Math.abs(inc[2] / c)) < 0.01D) break;
			A += inc[0];
			B += inc[1];
			c += inc[2];
			if (Double.isNaN(A) || Double.isNaN(B) || Double.isNaN(c)) {
				A = Ainit;
				B = Binit;
				c = Cinit;
				break;
			}
		}
		spot.addFitParameter("NAME", getName());
		spot.addFitParameter("A", new Double(A));//System.out.println("A="+A);
		spot.addFitParameter("B", new Double(B));//System.out.println("B="+B);
		spot.addFitParameter("C", new Double(c));//System.out.println("C="+c);
		spot.setFitAlgorithm(this);
	}
	public void computeFit() {
		TSpot spot;
		int xCenter, yCenter;
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		int imageWidth = ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue();
		int nb = 0;
		Object[] spots = alignmentModel.getGridModel().getSelectionModel().getRecursivelySelectedSpots().toArray();
		double Xi, A, B, c, psl;
		Vector pixels;
		Enumeration enume;
		Point2D.Double point;
		double Ainit, Binit, Cinit;
		double interSpot = alignmentModel.getGridModel().getConfig().getSpotsWidth();
		nb = 0;
		for (int i = 0; i < spots.length; i++) {
			spot = (TSpot)spots[i];
			pixels = spot.getPoints2D(xRes, yRes);
			xCenter = (int)(spot.getXCenter() / xRes);
			yCenter = (int)(spot.getYCenter() / yRes);
			nb = 0;
			Ainit = Binit = A = B = 0;
			Cinit = c = cii;
			psl = imageDataPSL[yCenter * imageWidth + xCenter];//modif remi 21/09/05
			if (psl <= c) {
				Ainit = A = 65535;
				Binit = B = 0;
			}
			else {
				Ainit = A = Math.pow(psl - c, -2.0D/3.0D);
				enume = pixels.elements();
				nb = 0;
				while (enume.hasMoreElements()) {
					point = (Point2D.Double)enume.nextElement();
					Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
					if (Xi > Math.pow(0.5D * interSpot / xRes, 2.0D)) {
						psl = imageDataPSL[(int)point.getY() * imageWidth + (int)point.getX()];//modif remi 21/09/05
						if (((psl - c) < Math.pow(A, -3.0D/2.0D)) && ((psl - c) > 0))
							B += (Math.pow(psl - c, -2.0D/3.0D) - A) / Xi;
						else
							B += 1.0D / Xi;
						nb++;
					}
				}
				B /= nb;
				Binit = B;
			}
			double[] inc = null;
			int k;
			for (k = 0; k < 50; k++) {
				inc = gaussNewton(A, B, c, spot, imageDataPSL, xRes, yRes, 65536, imageWidth, interSpot);
				if ((Math.abs(inc[0] / A) + Math.abs(inc[1] / B) + Math.abs(inc[2] / c)) < 0.01D) break;
				A += inc[0];
				B += inc[1];
				c += inc[2];
				if (Double.isNaN(A) || Double.isNaN(B) || Double.isNaN(c)) {
					A = Ainit;
					B = Binit;
					c = Cinit;
					break;
				}
			}
			spot.addFitParameter("NAME", getName());
			spot.addFitParameter("A", new Double(A));
			spot.addFitParameter("B", new Double(B));
			spot.addFitParameter("C", new Double(c));
			spot.setFitAlgorithm(this);
		}
	}
	
	//TODO understand what this function does in order to remove it...if not useful
	// each call ot the thread uses this function
	public void makeThRaster() {
		Object[] spots = alignmentModel.getGridModel().getSelectionModel().getRecursivelySelectedSpots().toArray();
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		TSpot spot;
		Enumeration enume;
		Point2D.Double point;
		double Yi, fitQL;
		int imageWidth = ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue();
		int imageHeight = ((Integer)workingData[IMAGE_HEIGHT_IN_PIXELS]).intValue();
		TImageModel imageModel = alignmentModel.getImageModel();
		double lat = ImagePlusTools.getLatitude();
		short[] thData = new short[imageWidth * imageHeight];
		double[] fitData;
		byte[] diffData = new byte[imageWidth * imageHeight];
		int bbb;
		
		computeCii();
		short fond = (short)(65535.0D - 65536.0D * (Math.log(cii) / Math.log(10) / lat + 0.5D));
		Arrays.fill(thData, fond);
		for (int i = 0; i < spots.length; i++) {
			spot = (TSpot)spots[i];
			fitData = spot.getFitData();
			enume = spot.getPoints2D(xRes, yRes).elements();
			int n = 0;
			while (enume.hasMoreElements()) {
				point = (Point2D.Double)enume.nextElement();
				bbb = (int)point.getX()+ (int)point.getY() * imageWidth;
				try {
					Yi = (imageDataPSL[bbb] /*& 0xFFFF*/);
				}
				catch(ArrayIndexOutOfBoundsException ex) {
					Yi = 0;
				}
				fitQL = 65535.0D * (Math.log(fitData[n++]) / Math.log(10) / lat + 0.5D);
				if (fitQL > 65535) fitQL = 65535;
				//fitQL = fitData[n++];
				if (thData[bbb] == fond)
					thData[bbb] = (short)(~(int)fitQL & 0xFFFF);
				else
					thData[bbb] = (short)Math.max((int)thData[bbb], ~((int)fitQL & 0xFFFF));
				diffData[bbb] = (byte)(Math.abs(fitQL - Yi) / 256);
			}
		}
		diffRaster = Raster.createPackedRaster(new DataBufferByte(diffData, imageWidth * imageHeight),
				imageWidth, imageHeight, 8, new Point(0, 0));
		thRaster = Raster.createPackedRaster(new DataBufferUShort(thData, imageWidth * imageHeight),
				imageWidth, imageHeight, 16, new Point(0, 0));
	}
	
	
	public double[] getFitData(TSpot spot){
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		Vector pixels = spot.getPoints2D(xRes, yRes);
		double[] fitData = new double[pixels.size()];
		int xCenter = (int)(spot.getXCenter() / xRes);
		int yCenter = (int)(spot.getYCenter() / yRes);
		Point2D.Double point;
		double Xi, fitPSL;
		int n = 0;
		for (Enumeration enu = pixels.elements(); enu.hasMoreElements(); ) {
			point = (Point2D.Double)enu.nextElement();
			Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) +
			((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
			fitPSL = 1.0D / Math.pow((((Double)spot.getFitParameter("A")).doubleValue() +
					((Double)spot.getFitParameter("B")).doubleValue() * Xi), 1.5D) +
					((Double)spot.getFitParameter("C")).doubleValue();
			if (fitPSL < 0) fitPSL = 0;
			fitData[n++] = fitPSL;
		}
		return fitData;
	}
	
	//warning: image tab values are PSL pixels values
	// this tab is a double tab because PSL values are decimals 
	// before an int tab was used containing all values into  the light low-order bit and the high-order bit.
	//TODO rename "image" into "dataPSL"
	private double[] gaussNewton(double a, double b, double c, TSpot spot, /*int[]*/double[] image, double xRes, double yRes, int nMax, int width, double is) {
		double[] result = new double[3];
		int nb = 0, i = 0, xCenter, yCenter;
		Point2D.Double point;
		double Ik, Xk2, FI;
		Vector pixels = spot.getPoints2D(xRes, yRes);
		xCenter = (int)(spot.getXCenter() / xRes);
		yCenter = (int)(spot.getYCenter() / yRes);
		double DF1 = 0, DF2 = 0, DF3 = 0, DF4 = 0, DF5 = 0, DFI1 = 0, DFI2 = 0, DFI3 = 0;
		Matrix DF = new Matrix(3, 3);
		Matrix DFI = new Matrix(1, 3);
		for (Enumeration enume = pixels.elements(); enume.hasMoreElements(); i++) {
			point = (Point2D.Double)enume.nextElement();
			Ik = image[(int)point.getX() + (int)point.getY() * width];//modif remi 21/09/05
			Xk2 = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) +
			((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
			FI = Ik - (Math.pow((a + b * Xk2), -1.5D) + c);
			DF1 += Math.pow((a + b * Xk2), -5.0D) * 2.25D;
			DF2 += Xk2 * Math.pow((a + b * Xk2), -5.0D) * 2.25D;
			DF3 += Math.pow((a + b * Xk2), -2.5D) * 1.5D;
			DF4 += Xk2 * Xk2 * Math.pow((a + b * Xk2), -5.0D) * 2.25D;
			DF5 += Xk2 * Math.pow((a + b * Xk2), -2.5D) * 1.5D;
			DFI1 += Math.pow((a + b * Xk2), -2.5D) * 1.5D * FI;
			DFI2 += Xk2 * Math.pow((a + b * Xk2), -2.5D) * 1.5D * FI;
			DFI3 += FI;
			nb++;
		}
		DF3 = -DF3;
		DF5 = -DF5;
		DFI1 = -DFI1;
		DFI2 = -DFI2;
		DF.set(0, 0, DF1);
		DF.set(0, 1, DF2);
		DF.set(0, 2, DF3);
		DF.set(1, 0, DF2);
		DF.set(1, 1, DF4);
		DF.set(1, 2, DF5);
		DF.set(2, 0, DF3);
		DF.set(2, 1, DF5);
		DF.set(2, 2, (double)nb);
		DFI.set(0, 0, DFI1);
		DFI.set(0, 1, DFI2);
		DFI.set(0, 2, DFI3);
		
		try {
			Matrix m = DF.solveTranspose(DFI);
			result[0] = m.get(0, 0);
			result[1] = m.get(1, 0);
			result[2] = m.get(2, 0);
		}
		catch (Exception ex) {
			result[0] = Double.NaN;
			result[1] = Double.NaN;
			result[2] = Double.NaN;
		}
		return result;
	}
	public String getParameters(TSpot spot) {
		double a = ((Double)spot.getFitParameter("A")).doubleValue();
		double b = ((Double)spot.getFitParameter("B")).doubleValue();
		double c = ((Double)spot.getFitParameter("C")).doubleValue();
		return String.valueOf(a) + " " + String.valueOf(b) + " " + String.valueOf(c);
	}
	public void setParameters(TSpot spot, String params) {
		StringTokenizer strtok = new StringTokenizer(params);
		spot.addFitParameter("NAME", getName());
		spot.addFitParameter("A", Double.valueOf(strtok.nextToken()));
		spot.addFitParameter("B", Double.valueOf(strtok.nextToken()));
		spot.addFitParameter("C", Double.valueOf(strtok.nextToken()));
	}
	public double getFitCorrection(TSpot spot) {
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		return ((Double)spot.getFitParameter("C")).doubleValue() * Math.PI / xRes / yRes;
	}
	public double getQCouronne(TSpot spot) {
		if (spot.getFitData() == null) computeFit(spot);
		double[] fitData = spot.getFitData();
		double q_couronne = 0, Xi;
		double xRes = ((Double)workingData[XRES]).doubleValue();
		double yRes = ((Double)workingData[YRES]).doubleValue();
		Enumeration enu_pixels = spot.getPoints2D(xRes, yRes).elements();
		Point2D.Double point;
		int n = 0;
		double interSpot = ((TAlignmentModel)workingData[ALIGNMENT_MODEL]).getGridModel().getConfig().getSpotsWidth();
		while (enu_pixels.hasMoreElements()) {
			point = (Point2D.Double)enu_pixels.nextElement();
			Xi = (double)((int)point.getX() - spot.getXCenter() / xRes) * ((int)point.getX() - spot.getXCenter() / xRes) +
			((int)point.getY() - spot.getYCenter() / yRes) * ((int)point.getY() - spot.getYCenter() / yRes);
			if ((Xi <= Math.pow((spot.getComputedDiameter() / 2.0D + interSpot) / xRes, 2.0D)) &&
					(Xi >= Math.pow((spot.getComputedDiameter() / 2.0D - interSpot) / xRes, 2.0D)) && (fitData[n] > 0))
				q_couronne += fitData[n] - ((Double)spot.getFitParameter("C")).doubleValue();
			n++;
		}
		return q_couronne / 4.0D / interSpot;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
