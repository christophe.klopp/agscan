package agscan.algo.spotdetection;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;

public class TMultiChannelSpotDetectionAlgorithmPanel extends TAlgorithmControlPanel {
  //private JCheckBox showGridCenterCrossCheckBox, showSpotCenterCrossCheckBox; 
  private JTextField showGridCenterCrossTextField, showSpotCicularityTextField, kernelsizeTextField, pixelMinSpotSizeTextField, pixelMaxSpotSizeTextField;
  private static TMultiChannelSpotDetectionAlgorithmPanel instance = null;

  protected TMultiChannelSpotDetectionAlgorithmPanel() {
    super();
    showGridCenterCrossTextField = new JTextField();
    showSpotCicularityTextField = new JTextField();
    kernelsizeTextField = new JTextField();
    pixelMinSpotSizeTextField = new JTextField();
    pixelMaxSpotSizeTextField = new JTextField();
    jbInit();
  }
  private void jbInit() {
    removeAll();
    setLayout(new GridBagLayout());
    JLabel showGridCenterCrossLabel = new JLabel(Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.0")); //$NON-NLS-1$
    JLabel showSpotCircularityLabel = new JLabel(Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.1")); //$NON-NLS-1$
    JLabel kernelsizeTextLabel = new JLabel(Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.2")); //$NON-NLS-1$
    JLabel pixelMinSpotSizeLabel = new JLabel(Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.3")); //$NON-NLS-1$
    JLabel pixelMaxSpotSizeLabel = new JLabel(Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.4")); //$NON-NLS-1$
    Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TMultiChannelSpotDetectionAlgorithmPanel.5")); //$NON-NLS-1$
    setBorder(titledBorder1);
    add(showGridCenterCrossLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 5, 0), 0, 0));
    add(showSpotCircularityLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 5, 0), 0, 0));
    add(kernelsizeTextLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 10, 0), 0, 0));
    add(pixelMinSpotSizeLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 10, 0), 0, 0));
    add(pixelMaxSpotSizeLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 10, 0), 0, 0));
   add(showGridCenterCrossTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
   add(showSpotCicularityTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
           ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
   add(kernelsizeTextField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
           ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
   add(pixelMinSpotSizeTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
           ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
   add(pixelMaxSpotSizeTextField, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
           ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
  }
  public void init(TAlgorithm algo) {
    TMultiChannelSpotDetectionAlgorithm alg = (TMultiChannelSpotDetectionAlgorithm)algo;
    showGridCenterCrossTextField.setText(alg.getParameter(TMultiChannelSpotDetectionAlgorithm.GRID_CENTER_CROSS).toString());
    showSpotCicularityTextField.setText(alg.getParameter(TMultiChannelSpotDetectionAlgorithm.SPOT_CIRCULARITY).toString());
    kernelsizeTextField.setText(alg.getParameter(TMultiChannelSpotDetectionAlgorithm.KERNEL_SIZE).toString());
    pixelMinSpotSizeTextField.setText(alg.getParameter(TMultiChannelSpotDetectionAlgorithm.MIN_SPOT_SIZE).toString());
    pixelMaxSpotSizeTextField.setText(alg.getParameter(TMultiChannelSpotDetectionAlgorithm.MAX_SPOT_SIZE).toString());
  }
  
  public int getGridCenterCross() {
    int ret = -1;
    try {
      ret = Integer.parseInt(showGridCenterCrossTextField.getText());
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return ret;
  }
  
  public int getSpotCircularity() {
	    int ret = -1;
	    try {
	      ret = Integer.parseInt(showSpotCicularityTextField.getText());
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return ret;
	  }
  
  public int getKernelSize() {
	    int ret = -1;
	    try {
	      ret = Integer.parseInt(kernelsizeTextField.getText());
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return ret;
	  }
  
  public int getMinSpotSize() {
	    int ret = -1;
	    try {
	      ret = Integer.parseInt(pixelMinSpotSizeTextField.getText());
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return ret;
	  }
  public int getMaxSpotSize() {
	    int ret = -1;
	    try {
	      ret = Integer.parseInt(pixelMaxSpotSizeTextField.getText());
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return ret;
	  } 
  
  
  public static TMultiChannelSpotDetectionAlgorithmPanel getInstance(TMultiChannelSpotDetectionAlgorithm algo, boolean init) {
    if (instance == null)
      instance = new TMultiChannelSpotDetectionAlgorithmPanel();
    else
      instance.jbInit();
    if (init) instance.init(algo);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2007 INRA SIGENAE
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
