package agscan.algo.spotdetection;

import ij.*;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Point;
import java.util.Enumeration;
import java.util.Vector;

import plugins.ImageTools;

import agscan.Messages;
import agscan.AGScan;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

public class TMultiChannelSpotDetectionAlgorithm extends TSpotDetectionAlgorithm {
  public static final int GRID_CENTER_CROSS = 0;
  public static final int SPOT_CIRCULARITY = 1;
  public static final int KERNEL_SIZE = 2;
  public static final int MIN_SPOT_SIZE = 3;
  public static final int MAX_SPOT_SIZE = 4;
  private int progressRange;
  
  public static final String NAME = Messages.getString("TMultiChannelSpotDetectionAlgorithm.0"); //$NON-NLS-1$

  //private static int maxSearchZone = 40;//replace 16/08/05
  //private static int maxComputeZone = 10;//replace 16/08/05
  //private static int qualityThreshold = 30;//replace 16/08/05
  private static int gridCenterCross = AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.gridCenterCross");
  private static int spotCircularity = AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.spotCircularity");
  private static int kernelSize = AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.kernelSize");
  private static int minSpotSize = AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.minSpotSize");
  private static int maxSpotSize = AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.maxSpotSize");
  
  private static int lastGridCenterCross, lastSpotCicularity, lastKernelSize, lastMinSpotSize, lastMaxSpotSize;

  static {
    lastGridCenterCross = gridCenterCross;
    lastSpotCicularity = spotCircularity;
    lastKernelSize = kernelSize;
    lastMinSpotSize = minSpotSize;
    lastMaxSpotSize = maxSpotSize;
  }

  public TMultiChannelSpotDetectionAlgorithm(boolean init) {
    super();
    parameters = new Object[5];
    initWithLast();
    controlPanel = TMultiChannelSpotDetectionAlgorithmPanel.getInstance(this, init);
  }
  public void init() {
    setParameter(GRID_CENTER_CROSS, new Integer(((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getGridCenterCross()));
    setParameter(SPOT_CIRCULARITY, new Integer(((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getSpotCircularity()));
    setParameter(KERNEL_SIZE, new Integer(((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getKernelSize()));
    setParameter(MIN_SPOT_SIZE, new Integer(((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getMinSpotSize()));
    setParameter(MAX_SPOT_SIZE, new Integer(((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getMaxSpotSize()));
    }
  public String getStringParams() {
    String s = "";
    s = NAME + "|" + getParameter(GRID_CENTER_CROSS).toString() + "|" + getParameter(SPOT_CIRCULARITY).toString() + "|" +
        getParameter(KERNEL_SIZE).toString()+ "|" + getParameter(MIN_SPOT_SIZE).toString()+ "|" + getParameter(MAX_SPOT_SIZE).toString();
    return s;
  }
  public void initWithDefaults() {
    setParameter(GRID_CENTER_CROSS, new Integer(gridCenterCross));
    setParameter(SPOT_CIRCULARITY, new Integer(spotCircularity));
    setParameter(KERNEL_SIZE, new Integer(kernelSize));
    setParameter(MIN_SPOT_SIZE, new Integer(minSpotSize));
    setParameter(MAX_SPOT_SIZE, new Integer(maxSpotSize));
  }
  public void initWithLast() {
    setParameter(GRID_CENTER_CROSS, new Integer(gridCenterCross));
    setParameter(SPOT_CIRCULARITY, new Integer(spotCircularity));
    setParameter(KERNEL_SIZE, new Integer(kernelSize));
    setParameter(MIN_SPOT_SIZE, new Integer(minSpotSize));
    setParameter(MAX_SPOT_SIZE, new Integer(maxSpotSize));
  }
  public void setLasts() {
	  lastGridCenterCross = ((Integer)parameters[GRID_CENTER_CROSS]).intValue();
	  lastSpotCicularity = ((Integer)parameters[SPOT_CIRCULARITY]).intValue();
	  lastKernelSize = ((Integer)parameters[KERNEL_SIZE]).intValue();
	  lastMinSpotSize = ((Integer)parameters[MIN_SPOT_SIZE]).intValue();
	  lastMaxSpotSize = ((Integer)parameters[MAX_SPOT_SIZE]).intValue();
  }
  public void init(Object[] params) {
    setParameter(GRID_CENTER_CROSS, params[0]);
    setParameter(SPOT_CIRCULARITY, params[1]);
    setParameter(KERNEL_SIZE, params[2]);
    setParameter(MIN_SPOT_SIZE, params[3]);
    setParameter(MAX_SPOT_SIZE, params[4]);
  }
  
  public void saveInProperties(){
  // remi add 17/08/05
  	// on sauve dans les properties si different
  	// cette methode est appele quand on choisi "apply"
  	int panelValue;
  	
  	// qualityThreshold
  	panelValue = ((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getGridCenterCross();
  	// if the panel value is different that the property value => change the property value
  	if (panelValue != AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.gridCenterCross")){
  		AGScan.prop.setProperty("TMultiChannelSpotDetectionAlgorithm.gridCenterCross",panelValue); 	
  	}
  	// maxSearchZone
  	panelValue = ((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getSpotCircularity();
	if (panelValue != AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.spotCircularity")){
  		AGScan.prop.setProperty("TMultiChannelSpotDetectionAlgorithm.spotCircularity",panelValue); 	
  	}
	// maxComputeZone
  	panelValue = ((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getKernelSize();
	if (panelValue != AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.kernelSize")){
  		AGScan.prop.setProperty("TMultiChannelSpotDetectionAlgorithm.kernelSize",panelValue); 	
  	}	
  	panelValue = ((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getMinSpotSize();
	if (panelValue != AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.minSpotSize")){
  		AGScan.prop.setProperty("TMultiChannelSpotDetectionAlgorithm.minSpotSize",panelValue); 	
  	}	
 	panelValue = ((TMultiChannelSpotDetectionAlgorithmPanel)controlPanel).getMaxSpotSize();
	if (panelValue != AGScan.prop.getIntProperty("TMultiChannelSpotDetectionAlgorithm.maxSpotSize")){
  		AGScan.prop.setProperty("TMultiChannelSpotDetectionAlgorithm.maxSpotSize",panelValue); 	
  	}	
  }
  
  public String getName() {
    return NAME;
  }
  public int getDefaultGridCenterCross() {
    return gridCenterCross	;
  }
  public int getDefaultSpotCircularity() {
    return spotCircularity;
  }
  public int getDefaultKernelSize() {
    return kernelSize;
  }
  public int getDefaultMinSpotSize() {
	    return minSpotSize;
	  }
  public int getDefaultMaxSpotSize() {
	    return maxSpotSize;
	  }
  public void execute(boolean thread) {
    Object spots = workingData[SPOTS];
    TSpot spot;
    int nb = 0;
    

    
    // getting number of images in the stack 
    TAlignment alignment = getCurrentAlignment();
	TImageModel imodel = alignment.getImage().getImageModel();
	TGridModel gmodel = alignment.getGridModel();
    int nbImages = imodel.getNumberOfChannels();
    
    /*
     * giving information on the progress of the algorithm
     */
    TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    
    //System.out.println("NB Images "+nbImages);
    int pixelWidth = (int) imodel.getPixelWidth(); //width of one pixel in micrometers( for example 25)
	int pixelHeight = (int) imodel.getPixelHeight(); //height of one pixel in micrometers( for example 25)
	
    // getting Max image data 
	int imageWidth = (int)(imodel.getImageWidth()/imodel.getPixelWidth());
	int imageHeight = (int)(imodel.getImageHeight()/imodel.getPixelWidth());
    int[] imageMax = imodel.getMaxImageData();
    //LookUpTable lut = imodel.getMaxImage().createLut();
        
    /*
     *  kernel calculation : if more than 1 image in the stack the circular kernel if not gaussian kernel
     */
    //System.out.println("sport size "+(gmodel.getSpot(1).getWidth()/pixelWidth)+" * "+(gmodel.getSpot(1).getHeight()/pixelHeight));
	float[][] kernel;
	int kernelsize = (int)((gmodel.getSpot(1).getWidth()/pixelWidth));
	//System.out.println("kernel size before "+kernelsize);
	if (nbImages == 1){
		kernel = ImageTools.gaussianKernel(kernelsize, kernelsize);
	}else{
		kernel = ImageTools.calcCircKernel(kernelsize/(Integer)parameters[KERNEL_SIZE]);
	}
	
	kernelsize = kernel.length;
	//System.out.println("kernel size after "+kernel.length+" :"+kernel[0].length);
	
    if (spots instanceof TSpot) {
      computeSpotQuality(kernelsize, kernel, imageMax, imageWidth, (TSpot)spots );
      results[RESULT_PC_GOOD] = new Double(((TSpot)spots).getQuality());
    }
    else if (spots instanceof Vector) {
    	//System.out.println("Its a vector");
    	int count = 0;
    	int nbspots = ( (Vector) spots).size();
      for (Enumeration enume = ( (Vector) spots).elements(); enume.hasMoreElements(); ) {
        /*
         * progress information
         */
    	  
    	//count++;
    	spot = (TSpot) enume.nextElement();
		//TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.UPDATE, null,
		//		new Integer(10))); //(int)(count*10000/nbspots)));

        computeSpotQuality(kernelsize, kernel, imageMax, imageWidth, spot);
        double[] coords = computeSpotCenter(kernelsize, kernel, imageMax, imageWidth, spot );
//        System.out.print("NB  = "+coords[0]);
//        System.out.print(" X "+coords[1]);
//        System.out.println(" Y "+coords[2]);  
        if ((Integer)parameters[GRID_CENTER_CROSS] ==  1){
        	if (coords[0] == 1){
        		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
        				new TImageGraphicPanel.HotLine(Color.pink, (int)((spot.getX()+spot.getWidth()/2)/pixelWidth-2), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight), (int)((spot.getX()+spot.getWidth()/2)/pixelWidth+2), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight)));
        		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
        				new TImageGraphicPanel.HotLine(Color.pink,(int)((spot.getX()+spot.getWidth()/2)/pixelWidth), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight-2), (int)((spot.getX()+spot.getWidth()/2)/pixelWidth), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight+2)));
        	}
        }
        if (spot.getQuality() == 1) nb++;
      }
      results[RESULT_PC_GOOD] = new Double((double)nb / (double)((Vector)spots).size());
    }
  }
  
  private void computeSpotQuality(int kernelsize, float[][] kernel, int[] imageMax, double imageWidth,  TSpot spot) {
	  
	/**
	 * Retieving the spot information  
	 */  

	TGridModel model = spot.getModel();
	int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
	int pixelHeight = (int) model.getPixelHeight();
	//int spotWidth = (int)((spot.getSpotWidth()+spot.getInitDiameter())/pixelWidth);
	//int spotHeight = (int)((spot.getSpotHeight()+spot.getInitDiameter())/pixelHeight);
	int spotWidth = (int)((spot.getSpotWidth()/pixelWidth)+kernelsize);
	int spotHeight = (int)((spot.getSpotHeight()/pixelHeight)+kernelsize);
	int xSpotPosition = (int)((spot.getX()/pixelWidth)-kernelsize/2);
	int ySpotPosition = (int)((spot.getY()/pixelHeight)-kernelsize/2);
	
	//System.out.println("xSpotPosition "+xSpotPosition+" ySpotPosition "+ySpotPosition);
	//System.out.println("spotWidth "+spotWidth+" spotHeight "+spotHeight);
	//System.out.println("kernel length "+(int)(kernelsize/2));
	//System.out.println("kernel info "+kernel.length+" - "+kernel[0].length);
	
	//float[] spotData = new float[spotWidth*spotHeight];
	float[][] origimg = new float[spotWidth][spotHeight];
	
	for (int j = 0; j < spotWidth; j++){
		for (int k = 0; k < spotHeight; k++){
			//spotData[k*spotWidth+ j] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];
			origimg[j][k] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];	
		}
	}
	
	//FloatProcessor ipSpot = new FloatProcessor(spotWidth, spotHeight, spotData, null);
	//ImagePlus impSpot = new ImagePlus("extracted Spot Image", ipSpot);
	
	//impSpot.show();
	
	/**
	 * Template matching  
	 */  	
	
	float[][] corrimg = null;
	corrimg = new float[spotWidth][spotHeight];
	//System.out.println("corrimg before "+corrimg.length+" - "+corrimg[0].length);
	corrimg = ImageTools.statsCorrelation(origimg, kernel);
	//ImagePlus imp = new ImagePlus();
	//ij.process.ImageProcessor ip2 = imp.getProcessor().createProcessor(corrimg.length,corrimg[0].length);
	
	//System.out.println("corrimg after "+ corrimg.length+" "+corrimg[0].length);
	float[] outData = new float[corrimg.length*corrimg[0].length];
	for (int k=0; k < corrimg.length; k++){
		for (int l=0; l < corrimg[0].length; l++){
			outData[l*corrimg.length+k] = (corrimg[k][l]*32167)+32167;
		}
	}
	ImageProcessor outSpot = new FloatProcessor(corrimg.length, corrimg[0].length, outData, null);
	ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);

	//outimpSpot.show();
	
	outSpot.invert();
	new ij.process.ImageConverter(outimpSpot).convertToGray8();
	outSpot = outimpSpot.getProcessor();

	/**
	 * Thresholding  
	 */  
	
	//System.out.println("SPOT_SIZE = "+(Integer)parameters[SPOT_SIZE]);
	
	//float toto = (Integer)parameters[SPOT_SIZE];
	//System.out.println("SPOT_SIZE = "+toto);
	//toto = toto/100;
	//System.out.println("SPOT_SIZE = "+(pixelWidth*pixelHeight*((float)((Integer)parameters[MIN_SPOT_SIZE])/100))+" "+(pixelWidth*pixelHeight*((float)((Integer)parameters[MAX_SPOT_SIZE])/100)));
	
	outSpot.autoThreshold();
	ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
	ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,8192+255, myrt, (pixelWidth*pixelHeight*((float)((Integer)parameters[MIN_SPOT_SIZE])/100)), (pixelWidth*pixelHeight*((float)((Integer)parameters[MAX_SPOT_SIZE])/100)));
	mypart.analyze(outimpSpot);
	String myhead = myrt.getColumnHeadings();

	//System.out.println("heading "+myhead);
	//System.out.println("exists = "+myrt.columnExists(ij.measure.ResultsTable.X_CENTROID));
	//xm = myrt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS);
	//ym = myrt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS);
	//System.out.println("X = "+myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[0]);
	//System.out.println("Y = "+myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0]);
	//if (myrt.getCounter() > 0){
		//System.out.println("CIRC = "+myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[0]);	
	//}
	//System.out.println("NB spots = "+myrt.getCounter());
	
    if (myrt.getCounter() == 1 && myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[0] > ((float)((Integer)parameters[SPOT_CIRCULARITY])/100))
      spot.setQuality(1);
    else
      spot.setQuality(0);

  }
  
  public double[] computeSpotCenter(int kernelsize, float[][] kernel, int[] imageMax, double imageWidth,  TSpot spot) {
	  
		/**
		 * Retieving the spot information  
		 */  
		
		TGridModel model = spot.getModel();
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight();
		//int spotWidth = (int)((spot.getSpotWidth()+spot.getInitDiameter())/pixelWidth);
		//int spotHeight = (int)((spot.getSpotHeight()+spot.getInitDiameter())/pixelHeight);
		int spotWidth = (int)((spot.getSpotWidth()/pixelWidth)+kernelsize);
		int spotHeight = (int)((spot.getSpotHeight()/pixelHeight)+kernelsize);
		int xSpotPosition = (int)((spot.getX()/pixelWidth)-kernelsize/2);
		int ySpotPosition = (int)((spot.getY()/pixelHeight)-kernelsize/2);
		
		//System.out.println("xSpotPosition "+xSpotPosition+" ySpotPosition "+ySpotPosition);
		//System.out.println("spotWidth "+spotWidth+" spotHeight "+spotHeight);
		//System.out.println("kernel length "+(int)(kernelsize/2));
		//System.out.println("kernel info "+kernel.length+" - "+kernel[0].length);
		
		//float[] spotData = new float[spotWidth*spotHeight];
		float[][] origimg = new float[spotWidth][spotHeight];
		for (int j = 0; j < spotWidth; j++){
			for (int k = 0; k < spotHeight; k++){
				//spotData[k*spotWidth+ j] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];
				origimg[j][k] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];	
			}
		}
		
		//FloatProcessor ipSpot = new FloatProcessor(spotWidth, spotHeight, spotData, null);
		//ImagePlus impSpot = new ImagePlus("extracted Spot Image", ipSpot);
		
		//impSpot.show();
		
		/**
		 * Template matching  
		 */  	
		
		float[][] corrimg = null;
		corrimg = new float[spotWidth][spotHeight];
		//System.out.println("corrimg before "+corrimg.length+" - "+corrimg[0].length);
		corrimg = ImageTools.statsCorrelation(origimg, kernel);
		//ImagePlus imp = new ImagePlus();
		//ij.process.ImageProcessor ip2 = imp.getProcessor().createProcessor(corrimg.length,corrimg[0].length);
		
		//System.out.println("corrimg after "+ corrimg.length+" "+corrimg[0].length);
		float[] outData = new float[corrimg.length*corrimg[0].length];
		for (int k=0; k < corrimg.length; k++){
			for (int l=0; l < corrimg[0].length; l++){
				outData[l*corrimg.length+k] = (corrimg[k][l]*32167)+32167;
			}
		}
		ImageProcessor outSpot = new FloatProcessor(corrimg.length, corrimg[0].length, outData, null);
		ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);

		//outimpSpot.show();
		
		outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();

		/**
		 * Thresholding  
		 */  
		
		outSpot.autoThreshold();
		ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
		ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,8192+255, myrt, (pixelWidth*pixelHeight/20), (pixelWidth*pixelHeight/1.8));
		mypart.analyze(outimpSpot);
		String myhead = myrt.getColumnHeadings();

		double[] coords = new double[3];
		coords[0] = myrt.getCounter();
		
	    if (myrt.getCounter() > 0){    	
	    	//System.out.println(" X = "+(spot.getSpotWidth()/pixelWidth));
	    	//System.out.println(" Y = "+(spot.getSpotHeight()/pixelHeight));
	    
	    	//System.out.println(" X = "+myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[0]);
	    	//System.out.println(" Y = "+myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0]);
	       coords[1] = (spot.getSpotWidth()/pixelWidth/2)- myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[0];
	       coords[2] = (spot.getSpotHeight()/pixelHeight/2) - myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0];
	    }else{
	       coords[1] = coords[2] = 0;
	    }
	    
	    return coords; 

	  }
  
  
  protected TAlignment getCurrentAlignment() {
		TAlignment alignment = null;
		TEvent event = null;
		event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
		
		TControlerImpl c = currentElement.getControler();
		
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else System.out.println("PROBLEM!!!!");
		return alignment;
	}	
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2007 INRA SIGENAE
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/