package agscan.algo.spotdetection;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import agscan.Messages;
import agscan.algo.TAlgorithm;
import agscan.algo.TAlgorithmControlPanel;

public class TFourProfilesSpotDetectionAlgorithmPanel extends TAlgorithmControlPanel {
	private JTextField maxSearchZoneTextField, maxComputeTextField, qualityThresholdTextField;
	private static TFourProfilesSpotDetectionAlgorithmPanel instance = null;
	
	protected TFourProfilesSpotDetectionAlgorithmPanel() {
		super();
		maxSearchZoneTextField = new JTextField();
		maxComputeTextField = new JTextField();
		qualityThresholdTextField = new JTextField();
		jbInit();
	}
	private void jbInit() {
		removeAll();
		setLayout(new GridBagLayout());
		JLabel maxSearchZoneLabel = new JLabel(Messages.getString("TFourProfilesSpotDetectionAlgorithmPanel.0")); //$NON-NLS-1$
		JLabel maxComputeLabel = new JLabel(Messages.getString("TFourProfilesSpotDetectionAlgorithmPanel.1")); //$NON-NLS-1$
		JLabel qualityThresholdLabel = new JLabel(Messages.getString("TFourProfilesSpotDetectionAlgorithmPanel.2")); //$NON-NLS-1$
		Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)), Messages.getString("TFourProfilesSpotDetectionAlgorithmPanel.3")); //$NON-NLS-1$
		setBorder(titledBorder1);
		add(maxSearchZoneLabel,      new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 5, 0), 0, 0));
		add(maxComputeLabel,      new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 5, 0), 0, 0));
		add(qualityThresholdLabel,      new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 10, 10, 0), 0, 0));
		add(maxSearchZoneTextField,         new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 5, 5, 10), 30, 0));
		add(maxComputeTextField,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 10), 30, 0));
		add(qualityThresholdTextField,    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 10, 10), 30, 0));
	}
	public void init(TAlgorithm algo) {
		TFourProfilesSpotDetectionAlgorithm alg = (TFourProfilesSpotDetectionAlgorithm)algo;
		maxSearchZoneTextField.setText(alg.getParameter(TFourProfilesSpotDetectionAlgorithm.MAX_SEARCH_ZONE).toString());
		maxComputeTextField.setText(alg.getParameter(TFourProfilesSpotDetectionAlgorithm.MAX_COMPUTE_ZONE).toString());
		qualityThresholdTextField.setText(alg.getParameter(TFourProfilesSpotDetectionAlgorithm.QUALITY_THRESHOLD).toString());
	}
	public int getMaxSearchZone() {
		int ret = -1;
		try {
			ret = Integer.parseInt(maxSearchZoneTextField.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
	public int getMaxZone() {
		int ret = -1;
		try {
			ret = Integer.parseInt(maxComputeTextField.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
	public int getQualityThreshold() {
		int ret = -1;
		try {
			ret = Integer.parseInt(qualityThresholdTextField.getText());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
	public static TFourProfilesSpotDetectionAlgorithmPanel getInstance(TFourProfilesSpotDetectionAlgorithm algo, boolean init) {
		if (instance == null)
			instance = new TFourProfilesSpotDetectionAlgorithmPanel();
		else
			instance.jbInit();
		if (init) instance.init(algo);
		return instance;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
