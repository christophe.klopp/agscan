package agscan.algo.spotdetection;

import java.awt.Point;
import java.util.Enumeration;
import java.util.Vector;

import agscan.Messages;
import agscan.AGScan;
import agscan.data.model.grid.TSpot;

public class TTwoProfilesSpotDetectionAlgorithm extends TSpotDetectionAlgorithm {
  public static final int MAX_SEARCH_ZONE = 0;
  public static final int MAX_COMPUTE_ZONE = 1;
  public static final int QUALITY_THRESHOLD = 2;

  public static final String NAME = Messages.getString("TTwoProfilesSpotDetectionAlgorithm.0"); //$NON-NLS-1$

  //private static int maxSearchZone = 40;//replace 16/08/05
  //private static int maxComputeZone = 10;//replace 16/08/05
  //private static int qualityThreshold = 30;//replace 16/08/05
  private static int maxSearchZone = AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.maxSearchZone");
  private static int maxComputeZone = AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.maxComputeZone");
  private static int qualityThreshold = AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.qualityThreshold");
  
  private static int lastMaxSearchZone, lastMaxComputeZone, lastQualityThreshold;

  static {
    lastMaxSearchZone = maxSearchZone;
    lastMaxComputeZone = maxComputeZone;
    lastQualityThreshold = qualityThreshold;
  }

  public TTwoProfilesSpotDetectionAlgorithm(boolean init) {
    super();
    parameters = new Object[3];
    initWithLast();
    controlPanel = TTwoProfilesSpotDetectionAlgorithmPanel.getInstance(this, init);
  }
  public void init() {
    setParameter(MAX_SEARCH_ZONE, new Integer(((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getMaxSearchZone()));
    setParameter(MAX_COMPUTE_ZONE, new Integer(((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getMaxZone()));
    setParameter(QUALITY_THRESHOLD, new Integer(((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getQualityThreshold()));
  }
  public String getStringParams() {
    String s = "";
    s = NAME + "|" + getParameter(MAX_SEARCH_ZONE).toString() + "|" + getParameter(MAX_COMPUTE_ZONE).toString() + "|" +
        getParameter(QUALITY_THRESHOLD).toString();
    return s;
  }
  public void initWithDefaults() {
    setParameter(MAX_SEARCH_ZONE, new Integer(maxSearchZone));
    setParameter(MAX_COMPUTE_ZONE, new Integer(maxComputeZone));
    setParameter(QUALITY_THRESHOLD, new Integer(qualityThreshold));
  }
  public void initWithLast() {
    setParameter(MAX_SEARCH_ZONE, new Integer(lastMaxSearchZone));
    setParameter(MAX_COMPUTE_ZONE, new Integer(lastMaxComputeZone));
    setParameter(QUALITY_THRESHOLD, new Integer(lastQualityThreshold));
  }
  public void setLasts() {
    lastMaxSearchZone = ((Integer)parameters[MAX_SEARCH_ZONE]).intValue();
    lastMaxComputeZone = ((Integer)parameters[MAX_COMPUTE_ZONE]).intValue();
    lastQualityThreshold = ((Integer)parameters[QUALITY_THRESHOLD]).intValue();
  }
  public void init(Object[] params) {
    setParameter(MAX_SEARCH_ZONE, params[0]);
    setParameter(MAX_COMPUTE_ZONE, params[1]);
    setParameter(QUALITY_THRESHOLD, params[2]);
  }
  
  public void saveInProperties(){
  // remi add 17/08/05
  	// on sauve dans les properties si different
  	// cette methode est appelee quand on choisi "apply"
  	int panelValue;
  	
  	// qualityThreshold
  	panelValue = ((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getQualityThreshold();
  	// if the panel value is different that the property value => change the property value
  	if (panelValue != AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.qualityThreshold")){
  		AGScan.prop.setProperty("TTwoProfilesSpotDetectionAlgorithm.qualityThreshold",panelValue); 	
  	}
  	// maxSearchZone
  	panelValue = ((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getMaxSearchZone();
	if (panelValue != AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.maxSearchZone")){
  		AGScan.prop.setProperty("TTwoProfilesSpotDetectionAlgorithm.maxSearchZone",panelValue); 	
  	}
	// maxComputeZone
  	panelValue = ((TTwoProfilesSpotDetectionAlgorithmPanel)controlPanel).getMaxZone();
	if (panelValue != AGScan.prop.getIntProperty("TTwoProfilesSpotDetectionAlgorithm.maxComputeZone")){
  		AGScan.prop.setProperty("TTwoProfilesSpotDetectionAlgorithm.maxComputeZone",panelValue); 	
  	}		
  }
  
  public String getName() {
    return NAME;
  }
  public int getDefaultMaxSearchZone() {
    return maxSearchZone;
  }
  public int getDefaultMaxComputeZone() {
    return maxComputeZone;
  }
  public int getDefaultQualityThreshold() {
    return qualityThreshold;
  }
  public void execute(boolean thread) {
    Object spots = workingData[SPOTS];
    TSpot spot;
    int nb = 0;
    if (spots instanceof TSpot) {
      computeSpotQuality( (TSpot) spots);
      results[RESULT_PC_GOOD] = new Double(((TSpot)spots).getQuality());
    }
    else if (spots instanceof Vector) {
      for (Enumeration enume = ( (Vector) spots).elements(); enume.hasMoreElements(); ) {
        spot = (TSpot) enume.nextElement();
        computeSpotQuality(spot);
        if (spot.getQuality() == 1) nb++;
      }
      results[RESULT_PC_GOOD] = new Double((double)nb / (double)((Vector)spots).size());
    }
  }
  private void computeSpotQuality(TSpot spot) {
    int[][] points = spot.getPoints(((Double)workingData[XRES]).doubleValue(),
                                    ((Double)workingData[YRES]).doubleValue());
    double[] dm3 = getDiffMoy(points[0][0], points[1][0], points[0][2], points[1][2]);
    double[] dm4 = getDiffMoy(points[0][1], points[1][1], points[0][3], points[1][3]);
    double[] dm1 = getDiffMoy((points[0][0] + points[0][1]) / 2, (points[1][0] + points[1][1]) / 2, (points[0][2] + points[0][3]) / 2, (points[1][2] + points[1][3]) / 2);
    double[] dm2 = getDiffMoy((points[0][1] + points[0][2]) / 2, (points[1][1] + points[1][2]) / 2, (points[0][0] + points[0][3]) / 2, (points[1][0] + points[1][3]) / 2);
    int histoMin = ((Integer)workingData[HISTOGRAM_MIN_USED]).intValue() * 257;
    int histoMax = ((Integer)workingData[HISTOGRAM_MAX_USED]).intValue() * 257;
    double cutoff = (histoMax - histoMin) * ((Integer)parameters[QUALITY_THRESHOLD]).intValue() / 1000;
    double ampl1 = dm1[2] - dm1[1];
    double ampl2 = dm2[2] - dm2[1];
    double ampl = Math.max(ampl1, ampl2);
    if ((dm1[0] > cutoff) &&
        (dm2[0] > cutoff) &&
        (ampl >= ((double)(histoMax - histoMin) * 0.1D)) &&
        (dm1[1] <= ((double)(histoMax - histoMin) * 0.4D)))
      spot.setQuality(1);
    else
      spot.setQuality(0);

  }
  private double[] getDiffMoy(int x1, int y1, int x2, int y2) {
    int[][] points = getPoints(x1, y1, x2, y2);
    int nbp = points[2][0];
    int[] profil = new int[nbp];
    int k = 0;
    double l = (double)((Integer)parameters[MAX_SEARCH_ZONE]).intValue() / 100.0D;
    int kmin = (int)((double)nbp * (1.0D - l) / 2.0D);
    int kmax = nbp - kmin;
    int maxIntensity = Integer.MAX_VALUE, minIntensity = Integer.MIN_VALUE, pointMax = 0, pointMin = Integer.MAX_VALUE;
    double moyTot = 0.0D, moyMax = 0.0D;
    double[] ret = new double[3];
    ret[0] = 0;
    ret[1] = 0;
    ret[2] = 0;
    int[] imageData = (int[])workingData[IMAGE_DATA];
    Point point;
    for (int i = 0; i < nbp; i++) {
      point = new Point(points[0][k], points[1][k]);
      if ((point.x < 0) || (point.x >= ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue()) ||
          (point.y < 0) || (point.y >= ((Integer)workingData[IMAGE_HEIGHT_IN_PIXELS]).intValue())) return ret;
      profil[k] = (~imageData[point.x + point.y * ((Integer)workingData[IMAGE_WIDTH_IN_PIXELS]).intValue()]) & 0xFFFF;
      moyTot += profil[k];
      if ((k >= kmin) && (k <= kmax))
        if (profil[k] < maxIntensity) {
          pointMax = k;
          maxIntensity = profil[k];
        }
        if (profil[k] > minIntensity) {
          pointMin = k;
          minIntensity = profil[k];
        }
      k++;
    }
    moyTot /= (double)nbp;
    int window = (int)(((Integer)parameters[MAX_COMPUTE_ZONE]).doubleValue() / 100.0D * (double)nbp);
    int n = 0;
    if (window > 0) {
      for (k = -window; k <= window; k++)
        if (((k + pointMax) >= 0) && ((k + pointMax) < profil.length)) {
          moyMax += profil[k + pointMax];
          n++;
        }
      moyMax /= (double)n;
    }
    ret[0] = moyTot - moyMax;
    ret[1] = maxIntensity;
    ret[2] = minIntensity;
    return ret;
  }
  private int[][] getPoints(int x1, int y1, int x2, int y2) {
    int[] samp = new int[1];
    int dx = (int)Math.abs(x2 - x1) + 1;
    int dy = (int)Math.abs(y2 - y1) + 1;
    int xmin = (int)Math.min(x1, x2);
    int xmax = (int)Math.max(x1, x2);
    int ymin = (int)Math.min(y1, y2);
    int ymax = (int)Math.max(y1, y2);
    int xdeb, ydeb;
    int k, x, y, n;
    double increment = 0.0D;
    int[][] points = new int[3][2 * Math.max(dx, dy)];
    if (dx >= dy) {
      increment = (double)dy / (double)dx;
      if (xmin == x1)
        ydeb = (int)y1;
      else
        ydeb = (int)y2;

      if (ydeb == ymax) increment = -increment;
      n = 0;
      for (k = xmin; k <= xmax; k++) {
        points[0][n] = k;
        points[1][n++] = (int)((double)(k - xmin) * increment) + ydeb;
      }
      points[2][0] = n;
    }
    else {
      increment = (double)dx / (double)dy;
      if (ymin == y1)
        xdeb = (int)x1;
      else
        xdeb = (int)x2;
      if (xdeb == xmax) increment = -increment;
      n = 0;
      for (k = ymin; k <= ymax; k++) {
        points[0][n] = (int)((double)(k - ymin) * increment) + xdeb;
        points[1][n++] = k;
      }
      points[2][0] = n;
    }
    return points;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
