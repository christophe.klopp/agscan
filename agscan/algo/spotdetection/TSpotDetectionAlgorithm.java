package agscan.algo.spotdetection;

import agscan.algo.TAlgorithm;

public abstract class TSpotDetectionAlgorithm extends TAlgorithm {
  public static final int IMAGE_DATA = 0;
  public static final int XRES = 1;
  public static final int YRES = 2;
  public static final int IMAGE_WIDTH_IN_PIXELS = 3;
  public static final int IMAGE_HEIGHT_IN_PIXELS = 4;
  public static final int HISTOGRAM_MIN_USED = 5;
  public static final int HISTOGRAM_MAX_USED = 6;
  public final static int SPOTS = 7;

  public static final int RESULT_PC_GOOD = 0;

  protected TSpotDetectionAlgorithm() {
    super();
    workingData = new Object[8];
    results = new Object[1];
  }
  public abstract String getStringParams();
  public void initData(int[] imageData, double xRes, double yRes, int imageWidthInPixels,
                       int imageHeightInPixels, int histogramMin, int histogramMax) {
    workingData[IMAGE_DATA] = imageData;
    workingData[XRES] = new Double(xRes);
    workingData[YRES] = new Double(yRes);
    workingData[IMAGE_WIDTH_IN_PIXELS] = new Integer(imageWidthInPixels);
    workingData[IMAGE_HEIGHT_IN_PIXELS] = new Integer(imageHeightInPixels);
    workingData[HISTOGRAM_MIN_USED] = new Integer(histogramMin);
    workingData[HISTOGRAM_MAX_USED] = new Integer(histogramMax);
  }
  public void initData(Object imageData, Object xRes, Object yRes, Object imageWidthInPixels,
                       Object imageHeightInPixels, Object histogramMin, Object histogramMax) {
    workingData[IMAGE_DATA] = imageData;
    workingData[XRES] = xRes;
    workingData[YRES] = yRes;
    workingData[IMAGE_WIDTH_IN_PIXELS] = imageWidthInPixels;
    workingData[IMAGE_HEIGHT_IN_PIXELS] = imageHeightInPixels;
    workingData[HISTOGRAM_MIN_USED] = histogramMin;
    workingData[HISTOGRAM_MAX_USED] = histogramMax;
  }
  public abstract void init();
  public abstract void initWithDefaults();
  public abstract void saveInProperties();
  public abstract String getName();
  public Object[] getParameters() {
    return parameters;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
