package agscan.algo;

import agscan.SwingWorker;

public abstract class TAlgorithm {
  protected TAlgorithmControlPanel controlPanel;
  protected Object[] parameters;
  protected Object[] workingData;
  protected Object[] results;
  protected SwingWorker worker;
  protected int startProgress, endProgress;

  public TAlgorithm() {
    super();
    startProgress = 0;
    endProgress = 100;
  }
  public TAlgorithmControlPanel getControlPanel() {
    return controlPanel;
  }
  public void setWorkingData(int i, Object data) {
    workingData[i] = data;
  }
  public void setParameter(int i, Object parameter) {
    parameters[i] = parameter;
  }
  public Object getResult(int i) {
    return results[i];
  }
  public void setResults(Object[] r) {
    results = r;
  }
  public Object getWorkingData(int i) {
    return workingData[i];
  }
  public Object getParameter(int i) {
    return parameters[i];
  }
  public abstract void execute(boolean thread);
  public void execute(boolean thread, int priority) {
    execute(thread);
  }
  public SwingWorker getWorker() {
    return worker;
  }
  public void setProgressRange(int st, int en) {
    startProgress = st;
    endProgress = en;
  }
  public int getStartProgress() {
    return startProgress;
  }
  public int getEndProgress() {
    return endProgress;
  }
  public abstract void init();
  public abstract void initWithDefaults();
  public abstract void initWithLast();
  public abstract void setLasts();
  public abstract void init(Object[] params);
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
