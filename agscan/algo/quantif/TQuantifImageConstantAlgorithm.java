package agscan.algo.quantif;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.TAlgorithmListener;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.model.TAlignmentModel;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TQuantifImageConstantAlgorithm extends TAlgorithmListener {
  public static final String NAME = "Quantification Image / Diametre Constant";
  public static final String COLUMN_NAME = "Qtf. Image/Const.";
  public static final int ALIGNMENT_MODEL = 0;
  public static final int XRES = 1;
  public static final int YRES = 2;
  public static final int IMAGE_WIDTH_IN_PIXELS = 3;
  public static final int IMAGE_HEIGHT_IN_PIXELS = 4;
  public static final int SPOTS = 5;

  public TQuantifImageConstantAlgorithm(TAlignmentModel alignmentModel) {
  	super(NAME + Messages.getString("TQuantifImageConstantAlgorithm.2"));//  externalized since 2006/03/20
    workingData = new Object[6];
    workingData[ALIGNMENT_MODEL] = alignmentModel;
    workingData[XRES] = new Double(alignmentModel.getPixelWidth());
    workingData[YRES] = new Double(alignmentModel.getPixelHeight());
    workingData[IMAGE_WIDTH_IN_PIXELS] = new Integer(alignmentModel.getElementWidth());
    workingData[IMAGE_HEIGHT_IN_PIXELS] = new Integer(alignmentModel.getElementHeight());
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignmentModel.getReference());
    workingData[SPOTS] = TEventHandler.handleMessage(event)[0];
    parameters = null;
    initWithDefaults();
    controlPanel = null;
    initWithLast();
  }

  public void setPriority(int priority) {
    worker.setPriority(priority);
  }
  public void execute(boolean thread) {
    worker = new TQuantifImageConstantWorker(this);
    worker.setPriority(Thread.MIN_PRIORITY);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }
  public void execute(boolean thread, int priority) {
    worker = new TQuantifImageConstantWorker(this);
    worker.setPriority(priority);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }
  /**
   * init
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init() {
  }

  /**
   * init
   *
   * @param params Object[]
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init(Object[] params) {
  }

  /**
   * initWithDefaults
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithDefaults() {
  }

  /**
   * initWithLast
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithLast() {
  }

  /**
   * setLasts
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void setLasts() {
  }
  public void actionPerformed(ActionEvent event) {
//  externalized since 2006/03/20
    int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TQuantifImageConstantAlgorithm.0"),
    		Messages.getString("TQuantifImageConstantAlgorithm.1"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    if (rep == JOptionPane.YES_OPTION) {
      worker.stop();
    }
  }
  public void stateChanged(ChangeEvent e) {
    int val = ((JSlider)e.getSource()).getValue();
    switch (val) {
      case 0 :
        worker.setPriority(Thread.MIN_PRIORITY);
        break;
      case 1 :
        worker.setPriority(Thread.NORM_PRIORITY);
        break;
      case 2 :
        worker.setPriority(Thread.MAX_PRIORITY);
        break;
    }
  }
}

/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
