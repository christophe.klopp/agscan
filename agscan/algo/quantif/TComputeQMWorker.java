package agscan.algo.quantif;

import java.awt.Color;
import java.awt.datatransfer.StringSelection;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableColumnModel;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.TImageControler;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TComputeQMWorker extends SwingWorker {
  private TComputeQMAlgorithm cqmAlgo;
  private Object[] results;

  public TComputeQMWorker(TComputeQMAlgorithm a) {
    cqmAlgo = a;
    results = new Object[1];
  }
  public Object construct(boolean thread) {
    TAlignmentModel alignmentModel = (TAlignmentModel)cqmAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment)alignmentModel.getReference();
    int pc_done = 0;
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, alignment);
    int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    TFitAlgorithm fitAlgo = null;
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData((TAlignmentModel)alignment.getModel());
    alignment.setAlgoRunning(true);
    alignment.setAlgorithm(cqmAlgo);
    int progressRange = cqmAlgo.getEndProgress() - cqmAlgo.getStartProgress();
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
                                           new Integer((int)(pc_done * progressRange / 100.0D) + cqmAlgo.getStartProgress())));
    alignment.getView().getTablePanel().setSelectable(false);
    alignment.getView().getGraphicPanel().removeInteractors();
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
    Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];
    double[] res = new double[spots.size()];
    int p = 0, xCenter = 0, yCenter = 0;
    TSpot spot;
    double xRes = alignmentModel.getPixelWidth(), yRes = alignmentModel.getPixelHeight(), Xi, Yi;
    String unit = alignmentModel.getImageModel().getUnit();
    double lat = ImagePlusTools.getLatitude();
    Enumeration enu_pixels;
    double[] fitData;
    Point2D.Double point;
    int k = 0;
    double qm_den, qm_num;
    double maxDiam = Math.min(alignment.getGridModel().getConfig().getSpotsWidth(), alignment.getGridModel().getConfig().getSpotsHeight());
    TImageModel imageModel = alignmentModel.getImageModel();
    //int[] imageData = imageModel.getImageData();//modif 22/09/05

    //  ajout remi 22/09/05
    double[] imageDataPSL = null; 
    if (imageModel.getUnit().equals("QL")){
  		int [] imageDataQL = ImagePlusTools.getImageData(imageModel.getInitialImage(),16);
  		double latitude = ImagePlusTools.getLatitude();
  		//conversion QL=>PSL
  		imageDataPSL = TImageControler.ql2pslData(imageDataQL, latitude);       	
  	}
  	else {//image PSL, données decimales
   		int[] temp = ImagePlusTools.getImageData(imageModel.getInitialImage(),16);//21/09/05 imageData est recuperé en int mais on travaille avec en double...
  		imageDataPSL = new double[temp.length];
  	  	//creation du meme tableau mais en double
  	  	for (int i=0;i< imageDataPSL.length;i++)    {
  	  		imageDataPSL[i]=temp[i];
    	}		
  	}
    //fin ajout
    
    
    
    
    int bbb;
    int imageWidth = alignmentModel.getElementWidth();

    int pc1 = 0, pc2 = 0;
    for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
      spot = (TSpot)enu_spots.nextElement();
      p++;
      pc_done = (p * 100) / spots.size();
      pc1 = (int)(pc_done * progressRange / 100.0D) + cqmAlgo.getStartProgress();
      if (pc1 != pc2) {
        TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
        pc2 = pc1;
      }
      if (spot.getFitAlgorithm() == null)
        fitAlgo.computeFit(spot);
      else if (!spot.getFitAlgorithm().getName().equals(fitAlgo.getName()))
        fitAlgo.computeFit(spot);
      fitAlgo.computeDiameter(spot, maxDiam);
      xCenter = (int)(spot.getXCenter() / xRes);
      yCenter = (int)(spot.getYCenter() / yRes);
      enu_pixels = spot.getPoints2D(xRes, yRes).elements();
      fitData = spot.getFitData();
      if (fitData == null) {
        fitAlgo.computeFit(spot);
        fitData = spot.getFitData();
      }
      int n = 0;
      qm_den = qm_num = 0;
      while (enu_pixels.hasMoreElements()) {
        point = (Point2D.Double)enu_pixels.nextElement();
        bbb = (int)point.getX()+ (int)point.getY() * imageWidth;
        Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
        if (Xi < Math.pow(spot.getComputedDiameter() / 2.0D / xRes, 2.0D)) {
        	try {
        		//modif 22/09/05
        		/*     	
        		 if (unit.equals("PSL"))
        		 Yi = ((imageData[bbb] >> 16) & 0xFFFF);
        		 else
        		 Yi = ((imageData[bbb] >> 16) & 0xFFFF) * Math.pow(10.0D, lat / 2.0D) / 65536.0D;
        		 */
        		Yi = imageDataPSL[bbb];//22/09/00005 
        	}
        	catch(ArrayIndexOutOfBoundsException ex) {
        		Yi = 0;
        	}
        	qm_num += Math.abs(Yi - fitData[n]);
        	qm_den += (Yi + fitData[n]);
        }
        n++;
      }
      res[k] = (int)((1 - qm_num / qm_den) * 100000) / 100000.0D;
      k++;
    }
    results[0] = new Vector();
    if (!STOP) {
      for (int i = 0; i < spots.size(); i++)
        ( (Vector) results[0]).addElement(new Double(res[i]));
      cqmAlgo.setResults(results);
    }
    return null;
  }
  public void finished() {
    TAlignmentModel alignmentModel = (TAlignmentModel) cqmAlgo.getWorkingData(TComputeQMAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment) alignmentModel.getReference();
    TEvent ev;
    if (!STOP) {
      Vector res = (Vector) cqmAlgo.getResult(0);
      Vector spots = (Vector) cqmAlgo.getWorkingData(TComputeQMAlgorithm.SPOTS);

      DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
      TColumn col = null;
      Enumeration enume = columns.getColumns();
      boolean nameExist = false;
      boolean typeOK = false;
      while (enume.hasMoreElements()) {
        col = (TColumn) enume.nextElement();
        if (col.getSpotKey().equals(TComputeQMAlgorithm.COLUMN_NAME)) {
          nameExist = true;
          if ( (col.getType() == TColumn.TYPE_INTEGER) || (col.getType() == TColumn.TYPE_REAL) || (col.getType() == TColumn.TYPE_TEXT))
            typeOK = true;
        }
        if (nameExist && typeOK) {
          break;
        }
        else
          col = null;
      }

      boolean afc = false, iswv = false;

      if (col == null) {
        if (nameExist) {
          JOptionPane.showMessageDialog(null, "<html><P>Une colonne portant le nom <font color=blue><b>" + TComputeQMAlgorithm.COLUMN_NAME +
                                        "</b></font> existe d�j� et son type n'est pas compatible avec un QM !</P></html>", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        else {
          Vector params = new Vector();
          params.addElement(TComputeQMAlgorithm.COLUMN_NAME);
          params.addElement(TComputeQMAlgorithm.COLUMN_NAME);
          params.addElement(new Double( -1));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          params.addElement(new Double(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_REAL), params);
          col = (TColumn) TEventHandler.handleMessage(ev)[0];
          col.setBooleanBetweenColor(false);
          col.setBooleanInfColor(false);
          col.setBooleanSupColor(false);
          col.setEditable(false);
          col.setSynchrone(true);
          afc = true;
          iswv = false;
        }
      }
      else {
        afc = false;
        iswv = true;
        col.setSynchrone(true);
      }
      if (col != null) {
        try {
          for (int i = 0; i < res.size(); i++)
            ( (TSpot) spots.elementAt(i)).addParameter(TComputeQMAlgorithm.COLUMN_NAME, res.elementAt(i), true);
          if ( (!alignment.isInBatch()) || ( (alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
            int[] ix = new int[2];
            TColumnState[] cs = new TColumnState[2];
            StringSelection[] ss = new StringSelection[2];
            ix[0] = col.getModelIndex();
            cs[0] = col.createSnapshot();
            StringBuffer s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[0] = new StringSelection(s.toString());
            int[] sel = new int[spots.size()];
            for (int k = 0; k < spots.size(); k++) sel[k] = ( (TSpot) spots.elementAt(k)).getIndex();
            ix[1] = col.getModelIndex();
            cs[1] = col.createSnapshot();
            s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[1] = new StringSelection(s.toString());
            ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment,
                            new TGridAction(false, false, false, false, false, false, afc, iswv, ix, cs, ss, null, sel));
            TEventHandler.handleMessage(ev);
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    alignment.setAlgoRunning(false);
    alignment.setAlgorithm(null);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    alignment.getView().getTablePanel().setSelectable(true);
  }
}
