package agscan.algo.quantif;

import java.awt.Color;
import java.awt.datatransfer.StringSelection;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableColumnModel;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TImageControler;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Societe : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TQuantifImageComputedWorker extends SwingWorker {
  private TQuantifImageComputedAlgorithm qicAlgo;
  private Object[] results;

  public TQuantifImageComputedWorker(TQuantifImageComputedAlgorithm qicAlgo) {
    this.qicAlgo = qicAlgo;
    results = new Object[1];
  }

  public Object construct(boolean thread) {
    TAlignmentModel alignmentModel = (TAlignmentModel)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment)alignmentModel.getReference();
    Vector spots = (Vector)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.SPOTS);
    TSpot spot;
    int pc_done = 0;
    alignment.setAlgoRunning(true);
    alignment.setAlgorithm(qicAlgo);
    int progressRange = qicAlgo.getEndProgress() - qicAlgo.getStartProgress();
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
                                           new Integer((int)(pc_done * progressRange / 100.0D) + qicAlgo.getStartProgress())));
    alignment.getView().getTablePanel().setSelectable(false);
    if (thread) alignment.setAlgorithm(qicAlgo);
    alignment.getView().getGraphicPanel().removeInteractors();
    TEvent ev;
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment.getView().getReference());
    TEventHandler.handleMessage(ev);

    double maxDiam = Math.min(alignment.getGridModel().getConfig().getSpotsWidth(), alignment.getGridModel().getConfig().getSpotsHeight());
    double Xi, Yi;
    Enumeration enu_spots, enu_pixels;
    double xRes = ((Double)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.XRES)).doubleValue();
    double yRes = ((Double)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.YRES)).doubleValue();
    Point2D.Double point;
    int xCenter, yCenter;
    double lat = ImagePlusTools.getLatitude();
    String unit = alignmentModel.getImageModel().getUnit();
    //int[] imageData = alignmentModel.getImageModel().getImageData();//modif remi 22/09/05
    //  debut ajout remi 22/09/05
    double[] imageDataPSL = null; 
    if (alignmentModel.getImageModel().getUnit().equals("QL")){
    	int [] imageDataQL = ImagePlusTools.getImageData(alignmentModel.getImageModel().getInitialImage(),16);
    	double latitude = ImagePlusTools.getLatitude();
    	//conversion QL=>PSL
    	imageDataPSL = TImageControler.ql2pslData(imageDataQL, latitude);       	
    }
    else {//image PSL, donnees decimales
    	int[] temp = ImagePlusTools.getImageData(alignmentModel.getImageModel().getInitialImage(),16);//21/09/05 imageData est recupere en int mais on travaille avec en double...
    	imageDataPSL = new double[temp.length];
    	//creation du meme tableau mais en double
    	for (int i=0;i< imageDataPSL.length;i++)    {
    		imageDataPSL[i]=temp[i];
    	}		
    }
    //fin ajoutremi 22/09/05
    int imageWidth = ((Integer)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue();
    double[] res = new double[spots.size()];
    results = new Object[1];
    results[0] = new Vector();
    for (int i = 0; i < spots.size(); i++) res[i] = 0;
    int k = 0;
    int p = 0;
    TFitAlgorithm fitAlgo = null;
    TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
    int fitAlg = ((Integer)TEventHandler.handleMessage(event)[0]).intValue();
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData(alignmentModel);
    int pc1 = 0, pc2 = 0;
    for (enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
      p++;
      pc_done = (p * 100) / spots.size();
      pc1 = (int)(pc_done * progressRange / 100.0D) + qicAlgo.getStartProgress();
      if (pc1 != pc2) {
        TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
        pc2 = pc1;
      }
      spot = (TSpot)enu_spots.nextElement();
      if (spot.getFitAlgorithm() == null)
        fitAlgo.computeFit(spot);
      else if (!spot.getFitAlgorithm().getName().equals(fitAlgo.getName()))
        fitAlgo.computeFit(spot);
      fitAlgo.computeDiameter(spot, maxDiam);
      xCenter = (int)(spot.getXCenter() / xRes);
      yCenter = (int)(spot.getYCenter() / yRes);
      enu_pixels = spot.getPoints2D(xRes, yRes).elements();
      while (enu_pixels.hasMoreElements()) {
        point = (Point2D.Double)enu_pixels.nextElement();
        Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
        try {
//        	modif remi 22/09/05 changement du tableau de data
        	/*
          if (unit.equals("PSL"))
            Yi = ((imageData[(int)point.getY() * imageWidth + (int)point.getX()] >> 16) & 0xFFFF);
          else
            Yi = ((imageData[(int)point.getY() * imageWidth + (int)point.getX()] >> 16) & 0xFFFF) * Math.pow(10.0D, lat / 2.0D) / 65536.0D;
            */
            Yi = imageDataPSL[(int)point.getY() * imageWidth + (int)point.getX()];
        }
        catch (Exception ex) {
          Yi = 0;
        }
        if (Xi < Math.pow(spot.getComputedDiameter() / 2.0D / xRes, 2.0D)) res[k] += Yi;
      }
      k++;
    }
    if (!STOP) {
      for (int i = 0; i < spots.size(); i++) ( (Vector) results[0]).addElement(new Integer( (int) res[i]));
      qicAlgo.setResults(results);
    }
    return null;
  }
  public void finished() {
    TAlignmentModel alignmentModel = (TAlignmentModel) qicAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment) alignmentModel.getReference();
    TEvent ev;

    if (!STOP) {
      Vector res = (Vector) qicAlgo.getResult(0);
      Vector spots = (Vector) qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.SPOTS);

      DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
      TColumn col = null;
      Enumeration enume = columns.getColumns();
      boolean nameExist = false;
      boolean typeOK = false;
      while (enume.hasMoreElements()) {
        col = (TColumn) enume.nextElement();
        if (col.getSpotKey().equals(TQuantifImageComputedAlgorithm.COLUMN_NAME)) {
          nameExist = true;
          if ((col.getType() == TColumn.TYPE_INTEGER) || (col.getType() == TColumn.TYPE_REAL) || (col.getType() == TColumn.TYPE_TEXT))
            typeOK = true;
        }
        if (nameExist && typeOK) {
          break;
        }
        else
          col = null;
      }

      boolean afc = false, iswv = false;

      if (col == null) {
        if (nameExist) {
          JOptionPane.showMessageDialog(null, "<html><P>Une colonne portant le nom <font color=blue><b>" + TQuantifImageComputedAlgorithm.COLUMN_NAME +
                                        "</b></font> existe deja et son type n'est pas compatible avec une quantification !</P></html>", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        else {
          Vector params = new Vector();
          params.addElement(TQuantifImageComputedAlgorithm.COLUMN_NAME);
          params.addElement(TQuantifImageComputedAlgorithm.COLUMN_NAME);
          params.addElement(new Integer( -1));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Integer(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Integer(0));
          params.addElement(new Integer(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Integer(0));
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_INTEGER), params);
          col = (TColumn) TEventHandler.handleMessage(ev)[0];
          col.setBooleanBetweenColor(false);
          col.setBooleanInfColor(false);
          col.setBooleanSupColor(false);
          col.setEditable(false);
          col.setSynchrone(true);
          afc = true;
          iswv = false;
        }
      }
      else {
        afc = false;
        iswv = true;
        col.setSynchrone(true);
      }
      if (col != null) {
        try {
          for (int i = 0; i < res.size(); i++)
            ( (TSpot) spots.elementAt(i)).addParameter(TQuantifImageComputedAlgorithm.COLUMN_NAME, res.elementAt(i), true);
          if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
            int[] ix = new int[2];
            TColumnState[] cs = new TColumnState[2];
            StringSelection[] ss = new StringSelection[2];
            ix[0] = col.getModelIndex();
            cs[0] = col.createSnapshot();
            StringBuffer s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[0] = new StringSelection(s.toString());

            int[] sel = new int[spots.size()];
            for (int k = 0; k < spots.size(); k++) sel[k] = ( (TSpot) spots.elementAt(k)).getIndex();
            ix[1] = col.getModelIndex();
            cs[1] = col.createSnapshot();
            s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[1] = new StringSelection(s.toString());
            ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment,
                            new TGridAction(false, false, false, false, false, false, afc, iswv, ix, cs, ss, null, sel));
            TEventHandler.handleMessage(ev);
            TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment));
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    alignment.setAlgoRunning(false);
    alignment.setAlgorithm(null);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    alignment.getView().getTablePanel().setSelectable(true);
  }
}
