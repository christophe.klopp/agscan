package agscan.algo.quantif;

import java.awt.Color;
import java.awt.datatransfer.StringSelection;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableColumnModel;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TComputeOvershiningCorrectionWorker extends SwingWorker {
  private TComputeOvershiningCorrectionAlgorithm cocAlgo;
  private Object[] results;

  public TComputeOvershiningCorrectionWorker(TComputeOvershiningCorrectionAlgorithm a) {
    cocAlgo = a;
    results = new Object[1];
  }
  public Object construct(boolean thread) {
    TAlignmentModel alignmentModel = (TAlignmentModel)cocAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment)alignmentModel.getReference();
    int pc_done = 0;
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, alignment);
    int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    TFitAlgorithm fitAlgo = null;
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData((TAlignmentModel)alignment.getModel());
    alignment.setAlgoRunning(true);
    alignment.setAlgorithm(cocAlgo);
    int progressRange = cocAlgo.getEndProgress() - cocAlgo.getStartProgress();
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
                                           new Integer((int)(pc_done * progressRange / 100.0D) + cocAlgo.getStartProgress())));
    alignment.getView().getTablePanel().setSelectable(false);
    alignment.getView().getGraphicPanel().removeInteractors();
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
    Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];
    double[] res = new double[spots.size()];
    double xRes = alignmentModel.getPixelWidth(), yRes = alignmentModel.getPixelHeight(), Xi, Yi;
    int p = 0;
    TSpot spot;
    int k = 0;
    TImageModel imageModel = alignmentModel.getImageModel();
   // int[] imageData = imageModel.getImageData();//modif 11/10/05
    //ligne suivante equivalente a imageModel.getInitialImage(),16)
    int[] imageData =ImagePlusTools.getImageData(imageModel.getChannelImage(1),ImagePlusTools.DEPTH_16_BITS);//0 avant mais 1 en fait!
    int xCenter, yCenter, bbb;
    int imageWidth = alignmentModel.getElementWidth();
    int pc1 = 0, pc2 = 0;
    for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
      spot = (TSpot)enu_spots.nextElement();
      xCenter = (int)(spot.getXCenter() / xRes);
      yCenter = (int)(spot.getYCenter() / yRes);
      bbb = xCenter + yCenter * imageWidth;
      p++;
      pc_done = (p * 100) / spots.size();
      pc1 = (int)(pc_done * progressRange / 100.0D) + cocAlgo.getStartProgress();
      if (pc1 != pc2) {
        TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
        pc2 = pc1;
      }
      if ((imageData[bbb] & 0xFFFF) >= (int)(0.9D * 65536.0D))
        res[k] = ((int) (spot.getFitAlgorithm().getQCouronne(spot) * 100000)) / 100000.0D;
      else
        res[k] = 0;
      k++;
    }
    results[0] = new Vector();
    if (!STOP) {
      for (int i = 0; i < spots.size(); i++)
        ( (Vector) results[0]).addElement(new Double(res[i]));
      cocAlgo.setResults(results);
    }
    return null;
  }
  public void finished() {
    TAlignmentModel alignmentModel = (TAlignmentModel) cocAlgo.getWorkingData(TComputeQMAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment) alignmentModel.getReference();
    TEvent ev;
    if (!STOP) {
      Vector res = (Vector) cocAlgo.getResult(0);
      Vector spots = (Vector) cocAlgo.getWorkingData(TComputeQMAlgorithm.SPOTS);

      DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
      TColumn col = null;
      Enumeration enume = columns.getColumns();
      boolean nameExist = false;
      boolean typeOK = false;
      while (enume.hasMoreElements()) {
        col = (TColumn) enume.nextElement();
        if (col.getSpotKey().equals(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME)) {
          nameExist = true;
          if ( (col.getType() == TColumn.TYPE_INTEGER) || (col.getType() == TColumn.TYPE_REAL) || (col.getType() == TColumn.TYPE_TEXT))
            typeOK = true;
        }
        if (nameExist && typeOK) {
          break;
        }
        else
          col = null;
      }

      boolean afc = false, iswv = false;

      if (col == null) {
        if (nameExist) {
          JOptionPane.showMessageDialog(null, "<html><P>Une colonne portant le nom <font color=blue><b>" + TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME +
                                        "</b></font> existe d�j� et son type n'est pas compatible avec un QM !</P></html>", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        else {
          Vector params = new Vector();
          params.addElement(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
          params.addElement(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
          params.addElement(new Double(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          params.addElement(new Double(0));
          params.addElement(new Boolean(false));
          params.addElement(Color.black);
          params.addElement(new Double(0));
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_REAL), params);
          col = (TColumn) TEventHandler.handleMessage(ev)[0];
          col.setBooleanBetweenColor(false);
          col.setBooleanInfColor(false);
          col.setBooleanSupColor(false);
          col.setEditable(false);
          col.setSynchrone(true);
          afc = true;
          iswv = false;
        }
      }
      else {
        afc = false;
        iswv = true;
        col.setSynchrone(true);
      }
      if (col != null) {
        try {
          TSpot spot, spotL, spotR, spotB, spotT;
          Object param;
          double over;
          for (int i = 0; i < res.size(); i++) {
            spot = (TSpot)spots.elementAt(i);
            over = ((Double)res.elementAt(i)).doubleValue();
            spotL = spot.getGlobalLeft();
            if (spotL != null) {
              param = spotL.getParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
              if (param != null)
                spotL.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double((int)((((Double)param).doubleValue() + over) * 100000) / 100000.0D), true);
              else
                spotL.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double(over) , true);
            }
            spotR = spot.getGlobalRight();
            if (spotR != null) {
              param = spotR.getParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
              if (param != null)
                spotR.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double((int)((((Double)param).doubleValue() + over) * 100000) / 100000.0D), true);
              else
                spotR.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double(over) , true);
            }
            spotB = spot.getGlobalBottom();
            if (spotB != null) {
              param = spotB.getParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
              if (param != null)
                spotB.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double((int)((((Double)param).doubleValue() + over) * 100000) / 100000.0D), true);
              else
                spotB.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double(over) , true);
            }
            spotT = spot.getGlobalTop();
            if (spotT != null) {
              param = spotT.getParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME);
              if (param != null)
                spotT.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double((int)((((Double)param).doubleValue() + over) * 100000) / 100000.0D), true);
              else
                spotT.addParameter(TComputeOvershiningCorrectionAlgorithm.COLUMN_NAME, new Double(over) , true);
            }
          }
          if ( (!alignment.isInBatch()) || ( (alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
            int[] ix = new int[2];
            TColumnState[] cs = new TColumnState[2];
            StringSelection[] ss = new StringSelection[2];
            ix[0] = col.getModelIndex();
            cs[0] = col.createSnapshot();
            StringBuffer s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[0] = new StringSelection(s.toString());
            int[] sel = new int[spots.size()];
            for (int k = 0; k < spots.size(); k++) sel[k] = ( (TSpot) spots.elementAt(k)).getIndex();
            ix[1] = col.getModelIndex();
            cs[1] = col.createSnapshot();
            s = new StringBuffer(spots.size() * 10);
            for (int i = 0; i < spots.size(); i++)
              s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
            ss[1] = new StringSelection(s.toString());
            ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment,
                            new TGridAction(false, false, false, false, false, false, afc, iswv, ix, cs, ss, null, sel));
            TEventHandler.handleMessage(ev);
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    alignment.setAlgoRunning(false);
    alignment.setAlgorithm(null);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    alignment.getView().getTablePanel().setSelectable(true);
  }

}
