package agscan.algo.quantif;

import java.awt.Color;
import java.awt.datatransfer.StringSelection;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableColumnModel;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TQuantifFitConstantWorker extends SwingWorker {
	private TQuantifFitConstantAlgorithm qfcAlgo;
	private Object[] results;
	
	
	public TQuantifFitConstantWorker(TQuantifFitConstantAlgorithm qfcAlgo) {
		this.qfcAlgo = qfcAlgo;
		results = new Object[1];
	}
	public Object construct(boolean thread) {
		//	java.util.Date time = new java.util.Date();
	//	System.out.println("time1="+time.getHours()+"_"+time.getMinutes()+"_"+time.getSeconds());
				TAlignmentModel alignmentModel = (TAlignmentModel)qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
		TAlignment alignment = (TAlignment)alignmentModel.getReference();
		/*
		 * 2005/10/13 test
		results = new Object[1];
		results[0] = new Vector();//test (remonté ici)
		//test remi 
		Vector images =  alignmentModel.getImageModel().getInitialImages();//test
		for (int index=0;index<images.size();index++){//test
			SImagePlus currentImage = (SImagePlus)images.get(index);//test			
		*/	
			
		int pc_done = 0;
		alignment.setAlgoRunning(true);
		alignment.setAlgorithm(qfcAlgo);
		int progressRange = qfcAlgo.getEndProgress() - qfcAlgo.getStartProgress();
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
		TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
				new Integer((int)(pc_done * progressRange / 100.0D) + qfcAlgo.getStartProgress())));
		alignment.getView().getTablePanel().setSelectable(false);
		if (thread) alignment.setAlgorithm(qfcAlgo);
		alignment.getView().getGraphicPanel().removeInteractors();
		TEvent ev;
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
		Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
		ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		Vector spots = (Vector)qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.SPOTS);
		TSpot spot;
		double Xi;
		Enumeration enu_spots, enu_pixels;
		double xRes = ((Double)qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.XRES)).doubleValue();
		double yRes = ((Double)qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.YRES)).doubleValue();
		Point2D.Double point;
		int xCenter, yCenter;
		double[] res = new double[spots.size()];
		results[0] = new Vector();
		for (int i = 0; i < spots.size(); i++) res[i] = 0;
		int k = 0, n;
		TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
		int fitAlg = ((Integer)TEventHandler.handleMessage(event)[0]).intValue();
		TFitAlgorithm fitAlgo = null;
		if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
			fitAlgo = new TGaussNewtonFitAlgorithm();
		else if (fitAlg == TMenuManager.FIT_NORMAL)
			fitAlgo = new TNormalFitAlgorithm();
		fitAlgo.initData(alignmentModel);
		double[] fitData;
		int p = 0;
		int pc1 = 0, pc2 = 0;
	//	System.out.println("spots.size="+spots.size());
		for (enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
			p++;
			pc_done = (p * 100) / spots.size();
			pc1 = (int)(pc_done * progressRange / 100.0D) + qfcAlgo.getStartProgress();
			if (pc1 != pc2) {
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
				pc2 = pc1;
			}
			
			spot = (TSpot)enu_spots.nextElement();
			xCenter = (int)(spot.getXCenter() / xRes);
			yCenter = (int)(spot.getYCenter() / yRes);
			enu_pixels = spot.getPoints2D(xRes, yRes).elements();
			fitData = spot.getFitData();
			if (fitData == null) {
				fitAlgo.computeFit(spot);
				fitData = spot.getFitData();
			}
			n = 0;
			res[k] = 0;
		//	System.out.println(p+"===");
			while (enu_pixels.hasMoreElements()) {
				point = (Point2D.Double)enu_pixels.nextElement();
				Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
				if (Xi < Math.pow(spot.getUserDiameter() / 2.0D / xRes, 2.0D)) res[k] += fitData[n];
				n++;
			}
		//	System.out.println(p+"======");
		//	System.out.println();
			k++;
		}
		if (!STOP) {
				for (int i = 0; i < spots.size(); i++) ( (Vector) results[0]).addElement(new Integer( (int) res[i]));
			qfcAlgo.setResults(results);
		}
		return null;
	}
	public void finished() {
		//	java.util.Date time2 = new java.util.Date();
	//	System.out.println("time2="+time2.getHours()+"_"+time2.getMinutes()+"_"+time2.getSeconds());
		TAlignmentModel alignmentModel = (TAlignmentModel) qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
		TAlignment alignment = (TAlignment) alignmentModel.getReference();
		TEvent ev;
		if (!STOP) {
			Vector res = (Vector) qfcAlgo.getResult(0);
			Vector spots = (Vector) qfcAlgo.getWorkingData(TQuantifFitConstantAlgorithm.SPOTS);
			
			DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
			TColumn col = null;
			Enumeration enume = columns.getColumns();
			boolean nameExist = false;
			boolean typeOK = false;
			while (enume.hasMoreElements()) {
				col = (TColumn) enume.nextElement();
				if (col.getSpotKey().equals(TQuantifFitConstantAlgorithm.COLUMN_NAME)) {
					nameExist = true;
					if ( (col.getType() == TColumn.TYPE_INTEGER) || (col.getType() == TColumn.TYPE_REAL) || (col.getType() == TColumn.TYPE_TEXT))
						typeOK = true;
				}
				if (nameExist && typeOK) {
					break;
				}
				else
					col = null;
			}
			
			boolean afc = false, iswv = false;
			
			if (col == null) {
				if (nameExist) {
					JOptionPane.showMessageDialog(null, "<html><P>Une colonne portant le nom <font color=blue><b>" + TQuantifFitConstantAlgorithm.COLUMN_NAME +
							"</b></font> existe deja et son type n'est pas compatible avec une quantification !</P></html>", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
				else {
					Vector params = new Vector();
					params.addElement(TQuantifFitConstantAlgorithm.COLUMN_NAME);
					params.addElement(TQuantifFitConstantAlgorithm.COLUMN_NAME);
					params.addElement(new Integer( -1));
					params.addElement(new Boolean(false));
					params.addElement(Color.black);
					params.addElement(new Integer(0));
					params.addElement(new Boolean(false));
					params.addElement(Color.black);
					params.addElement(new Integer(0));
					params.addElement(new Integer(0));
					params.addElement(new Boolean(false));
					params.addElement(Color.black);
					params.addElement(new Integer(0));
					ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_INTEGER), params);
					col = (TColumn) TEventHandler.handleMessage(ev)[0];
					col.setBooleanBetweenColor(false);
					col.setBooleanInfColor(false);
					col.setBooleanSupColor(false);
					col.setEditable(false);
					col.setSynchrone(true);
					afc = true;
					iswv = false;
				}
			}
			else {
				afc = false;
				iswv = true;
				col.setSynchrone(true);
			}
			if (col != null) {
				try {
					for (int i = 0; i < res.size(); i++)
						( (TSpot) spots.elementAt(i)).addParameter(TQuantifFitConstantAlgorithm.COLUMN_NAME, res.elementAt(i), true);
					if ( (!alignment.isInBatch()) || ( (alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
						int[] ix = new int[2];
						TColumnState[] cs = new TColumnState[2];
						StringSelection[] ss = new StringSelection[2];
						ix[0] = col.getModelIndex();
						cs[0] = col.createSnapshot();
						StringBuffer s = new StringBuffer(spots.size() * 10);
						for (int i = 0; i < spots.size(); i++)
							s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
						ss[0] = new StringSelection(s.toString());
						
						int[] sel = new int[spots.size()];
						for (int k = 0; k < spots.size(); k++) sel[k] = ( (TSpot) spots.elementAt(k)).getIndex();
						ix[1] = col.getModelIndex();
						cs[1] = col.createSnapshot();
						s = new StringBuffer(spots.size() * 10);
						for (int i = 0; i < spots.size(); i++)
							s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
						ss[1] = new StringSelection(s.toString());
						ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment,
								new TGridAction(false, false, false, false, false, false, afc, iswv, ix, cs, ss, null, sel));
						TEventHandler.handleMessage(ev);
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
		Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
		ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
		TEventHandler.handleMessage(ev);
		alignment.getView().getTablePanel().setSelectable(true);
	}
}
