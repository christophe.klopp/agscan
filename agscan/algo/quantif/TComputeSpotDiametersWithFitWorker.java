package agscan.algo.quantif;

import java.util.Enumeration;
import java.util.Vector;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Societe : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TComputeSpotDiametersWithFitWorker extends SwingWorker {
  private TComputeSpotDiametersWithFitAlgorithm csdwfAlgo;

  public TComputeSpotDiametersWithFitWorker(TComputeSpotDiametersWithFitAlgorithm a) {
    csdwfAlgo = a;
  }
  public Object construct(boolean thread) {
  	
    TAlignmentModel alignmentModel = (TAlignmentModel)csdwfAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment)alignmentModel.getReference();
 
    
    int pc_done = 0;
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, alignment);
    int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    TFitAlgorithm fitAlgo = null;
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData(alignmentModel);
    alignment.setAlgoRunning(true);
    alignment.setAlgorithm(csdwfAlgo);
    int progressRange = csdwfAlgo.getEndProgress() - csdwfAlgo.getStartProgress();
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
                                           new Integer((int)(pc_done * progressRange / 100.0D) + csdwfAlgo.getStartProgress())));
    alignment.getView().getTablePanel().setSelectable(false);
    alignment.getView().getGraphicPanel().removeInteractors();
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);

    ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
    Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];
    double maxDiam = Math.min(alignment.getGridModel().getConfig().getSpotsWidth(), alignment.getGridModel().getConfig().getSpotsHeight());
    int p = 0;
    TSpot spot;
    boolean us = ((Boolean)csdwfAlgo.getWorkingData(TComputeSpotDiametersWithFitAlgorithm.UPDATE_SHAPE)).booleanValue();
    int pc1 = 0, pc2 = 0;
    for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
      spot = (TSpot)enu_spots.nextElement();
      p++;
      if (!spot.isSynchro(TSpot.PARAMETER_COMPUTED_DIAMETER)) {
        pc_done = (p * 100) / spots.size();
        pc1 = (int)(pc_done * progressRange / 100.0D) + csdwfAlgo.getStartProgress();
        if (pc1 != pc2) {
          TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
          pc2 = pc1;
        }
        if (spot.getFitAlgorithm() == null)
          fitAlgo.computeFit(spot);
        else if (!spot.getFitAlgorithm().getName().equals(fitAlgo.getName()))
          fitAlgo.computeFit(spot);
        fitAlgo.computeDiameter(spot, maxDiam);
      }
      if (us) spot.updateDiameterWithComputed();
    }
    return null;
  }
  public void finished() {
    TAlignmentModel alignmentModel = (TAlignmentModel)csdwfAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
    TAlignment alignment = (TAlignment)alignmentModel.getReference();

    alignment.setAlgoRunning(false);
    alignment.setAlgorithm(null);
    TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
    TEventHandler.handleMessage(ev);
    alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
    TEventHandler.handleMessage(ev);
    alignment.getView().getTablePanel().setSelectable(true);
  }
}
