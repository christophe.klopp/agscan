package agscan.algo.quantif;

import ij.ImageStack;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.datatransfer.StringSelection;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableColumnModel;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TImageControler;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TQuantifImageConstantWorker extends SwingWorker {
	private TQuantifImageConstantAlgorithm qicAlgo;
	private Object[] results;
	
	public TQuantifImageConstantWorker(TQuantifImageConstantAlgorithm qicAlgo) {
		this.qicAlgo = qicAlgo;
		results = new Object[1];
	}
	public Object construct(boolean thread) {
		TAlignmentModel alignmentModel = (TAlignmentModel)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.ALIGNMENT_MODEL);
		TAlignment alignment = (TAlignment)alignmentModel.getReference();
		Vector spots = (Vector)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.SPOTS);//spots de la selection
		TSpot spot;
		results = new Object[1];
		results[0] = new Vector();//test (remonté ici)
		//test remi 
		//Vector images =  alignmentModel.getImageModel().getInitialImages();//test
		ImageStack images =  alignmentModel.getImageModel().getImages();//test 2005/10/14
		for (int index=1;index<=images.getSize();index++){//test
			//ImagePlus currentImage = (ImagePlus)images.get(index);//test												
			ImageProcessor currentProc = images.getProcessor(index);//test 2005/10/14 travaille sur ImageProc au lieu de ImagePlus
			int pc_done = 0;
			alignment.setAlgoRunning(true);
			alignment.setAlgorithm(qicAlgo);
			int progressRange = qicAlgo.getEndProgress() - qicAlgo.getStartProgress();
			TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
			TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
					new Integer((int)(pc_done * progressRange / 100.0D) + qicAlgo.getStartProgress())));
			alignment.getView().getTablePanel().setSelectable(false);
			if (thread) alignment.setAlgorithm(qicAlgo);
			alignment.getView().getGraphicPanel().removeInteractors();
			TEvent ev;
			ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
			Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
			ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment.getView().getReference());
			TEventHandler.handleMessage(ev);
			double Xi, Yi;
			Enumeration enu_spots, enu_pixels;
			double xRes = ((Double)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.XRES)).doubleValue();
			double yRes = ((Double)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.YRES)).doubleValue();
			Point2D.Double point;
			int xCenter, yCenter;
			//double lat = alignmentModel.getImageModel().getInitialImage().getLatitude();//test
			double lat = ImagePlusTools.getLatitude();
			String unit = alignmentModel.getImageModel().getUnit();
			//int[] imageData = alignmentModel.getImageModel().getImageData();//modif remi 22/09/05
			//  debut ajout remi 22/09/05
			double[] imageDataPSL = null; 
			if (alignmentModel.getImageModel().getUnit().equals("QL")){
				//	int [] imageDataQL = alignmentModel.getImageModel().getInitialImage().getImageData(16);
				//int [] imageDataQL = ImagePlusTools.getImageData(currentImage,16);//test
				int [] imageDataQL = ImagePlusTools.getImageData(currentProc,16);//test 2005/10/14
				//double latitude = alignmentModel.getImageModel().getInitialImage().getLatitude();
				//conversion QL=>PSL
				imageDataPSL = TImageControler.ql2pslData(imageDataQL, lat);       	
			}
			else {//image PSL, données decimales
				//int[] temp = alignmentModel.getImageModel().getInitialImage().getImageData(16);//21/09/05 imageData est recuperé en int mais on travaille avec en double...
				//int[] temp = ImagePlusTools.getImageData(currentImage,16);//test
				int[] temp = ImagePlusTools.getImageData(currentProc,16);//test 2005/10/14
				imageDataPSL = new double[temp.length];
				//creation du meme tableau mais en double
				for (int i=0;i< imageDataPSL.length;i++)    {
					imageDataPSL[i]=temp[i];
				}		
			}
			//fin ajout remi 22/09/05
			
			int imageWidth = ((Integer)qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.IMAGE_WIDTH_IN_PIXELS)).intValue();
			double[] res = new double[spots.size()];//test
			//double[] res = new double[(spots.size()*index)+spots.size()];//test ajout du *index+spots.size()
			//results = new Object[1];//test
			//results[0] = new Vector();//test ici modif
			//for (int i = 0; i < spots.size(); i++) res[i] = 0;//test
			for (int i = 0; i < res.length; i++) res[i] = 0;//test
			int k = 0;
			int p = 0;
			int pc1 = 0, pc2 = 0;
			for (enu_spots = spots.elements(); enu_spots.hasMoreElements() && !STOP; ) {
				p++;
				pc_done = (p * 100) / spots.size();
				pc1 = (int)(pc_done * progressRange / 100.0D) + qicAlgo.getStartProgress();
				if (pc1 != pc2) {
					TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(pc1)));
					pc2 = pc1;
				}
				spot = (TSpot)enu_spots.nextElement();
				xCenter = (int)(spot.getXCenter() / xRes);
				yCenter = (int)(spot.getYCenter() / yRes);
				enu_pixels = spot.getPoints2D(xRes, yRes).elements();
				while (enu_pixels.hasMoreElements()) {
					point = (Point2D.Double)enu_pixels.nextElement();
					// Il fait pythagore avec le rayon = hypotenuse
					// x et y sont les 2 autres cotés
					//Xi est donc la somme des carrés des 2 cotés
					// et en bas on compare avec r²
					// remi comment 05/10/05
					Xi = (double)((int)point.getX() - xCenter) * ((int)point.getX() - xCenter) + ((int)point.getY() - yCenter) * ((int)point.getY() - yCenter);
					try {
						//modif remi 22/09/05 changement du tableau de data
						/*
						 if (unit.equals("PSL"))
						 Yi = ((imageData[(int)point.getY() * imageWidth + (int)point.getX()] >> 16) & 0xFFFF);
						 else
						 Yi = ((imageData[(int)point.getY() * imageWidth + (int)point.getX()] >> 16) & 0xFFFF) * Math.pow(10.0D, lat / 2.0D) / 65536.0D;
						 */
						Yi = imageDataPSL[(int)point.getY() * imageWidth + (int)point.getX()];
					}
					catch (Exception ex) {
						Yi = 0;
					}
					//si Xi²< r² // remi comment 05/10/05
					if (Xi < Math.pow(spot.getUserDiameter() / 2.0D / xRes, 2.0D)) res[k] += Yi;
				}
				k++;
			}
			if (!STOP) {
				for (int i = 0; i < spots.size(); i++) {
					//System.out.println("i= "+i+" et res= "+res[i]);//test
					( (Vector) results[0]).addElement(new Integer( (int) res[i]));
				}
				qicAlgo.setResults(results);
			}
		}//fin test
		return null;
	}
	public void finished() {
		TAlignmentModel alignmentModel = (TAlignmentModel) qicAlgo.getWorkingData(TQuantifFitConstantAlgorithm.ALIGNMENT_MODEL);
		TAlignment alignment = (TAlignment) alignmentModel.getReference();
		TEvent ev;
		
		//test
		// creer pour chaque image sa colonne, la 1ere c'est la normale, les autrs on indexe...
		String nameColumn = TQuantifImageConstantAlgorithm.COLUMN_NAME;
		//		test remi 
		//remplacement Vector => Stack et du coup modif du travail sur ImagePlus => ImageProcessor
	//	Vector images =  alignmentModel.getImageModel().getInitialImages();//test
		ImageStack images =  alignmentModel.getImageModel().getImages();//test
		for (int index=0;index<images.getSize();index++){//test			
			if (index!=0) nameColumn = TQuantifImageConstantAlgorithm.COLUMN_NAME+"("+(index+1)+")";//test		
			if (!STOP) {
				Vector spots = (Vector) qicAlgo.getWorkingData(TQuantifImageConstantAlgorithm.SPOTS);
				
				//Vector res = (Vector) qicAlgo.getResult(0);//test
				//2005/10/13 : avec n images,res a comme taille n*nb de spots, donc il faut prendre la partie courante du res
				// correspondant à l'image courante
				Vector resAll = (Vector) qicAlgo.getResult(0);//test
				List res = resAll.subList(index*spots.size(),((index+1)*spots.size()));//test
								
				// recuperation du model des colonnes deja ds la table
				DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
				TColumn col = null;
				Enumeration enume = columns.getColumns();//l'enumeration contient toutes les colonnes de la table
				boolean nameExist = false;
				boolean typeOK = false;
				while (enume.hasMoreElements()) {
					col = (TColumn) enume.nextElement();
					//recherche de la colonne du model correpondant a cet algo, ex ici  COLUMN_NAME = "Qtf. Image/Const."
					//				if (col.getSpotKey().equals(TQuantifImageConstantAlgorithm.COLUMN_NAME)) {//test
					if (col.getSpotKey().equals(nameColumn)) {
						System.out.println("column:"+col.getSpotKey());
						nameExist = true;
						if ((col.getType() == TColumn.TYPE_INTEGER) || (col.getType() == TColumn.TYPE_REAL) || (col.getType() == TColumn.TYPE_TEXT))
							typeOK = true;
					}
					if (nameExist && typeOK) {
						break;// des que la bonne colonne est trouvée (celle concernée par 
						// notre algo:  COLUMN_NAME = "Qtf. Image/Const." ici, on peut passer a la suite:
						//col contient cette colonne
					}
					else
						col = null;// la colonne n'est pas encore dans la table
				}
				
				boolean afc = false, iswv = false;
				// si col = null c'est que la colonne n'est pas encore dans le model....on peut la créer
				if (col == null) {
					if (nameExist) {
						JOptionPane.showMessageDialog(null, "<html><P>Une colonne portant le nom <font color=blue><b>" + TQuantifImageConstantAlgorithm.COLUMN_NAME +
								"</b></font> existe d�j� et son type n'est pas compatible avec une quantification !</P></html>", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
					// ajout de la colonne
					else {
						Vector params = new Vector();
						//params.addElement(TQuantifImageConstantAlgorithm.COLUMN_NAME);//test
						//params.addElement(TQuantifImageConstantAlgorithm.COLUMN_NAME);//test
						params.addElement(nameColumn);//test
						params.addElement(nameColumn);//test
						params.addElement(new Integer( -1));
						params.addElement(new Boolean(false));
						params.addElement(Color.black);
						params.addElement(new Integer(0));
						params.addElement(new Boolean(false));
						params.addElement(Color.black);
						params.addElement(new Integer(0));
						params.addElement(new Integer(0));
						params.addElement(new Boolean(false));
						params.addElement(Color.black);
						params.addElement(new Integer(0));
						ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, alignment, new Integer(TColumn.TYPE_INTEGER), params);
						col = (TColumn) TEventHandler.handleMessage(ev)[0];
						col.setBooleanBetweenColor(false);
						col.setBooleanInfColor(false);
						col.setBooleanSupColor(false);
						col.setEditable(false);
						col.setSynchrone(true);
						afc = true;
						iswv = false;
					}
				}
				//else si la colonne est deja dans la table, il faut juste la mettre a jour
				else {
					afc = false;
					iswv = true;
					col.setSynchrone(true);
				}
				
				if (col != null) {//cad si la colonne existait deja ou si elle vient d'etre créée
					try {
						// pour chaque spot on met sa valeur de quantif calculée (-1 par défaut dans chaque case 
						// à la création de la colonne dans la table)
						for (int i = 0; i < res.size(); i++)
							//( (TSpot) spots.elementAt(i)).addParameter(TQuantifImageConstantAlgorithm.COLUMN_NAME, res.elementAt(i), true);//test
							( (TSpot) spots.elementAt(i)).addParameter(nameColumn, /*res.elementAt(i)*/res.get(i), true);//test
						// la suite sert (peut etre) pour faire un undo/redo de la quantif (= mise à jour de la table) 
						// mais rien ne se passe avec un undo apres quantif...
						//TODO comprendre pourquoi...
						if ((!alignment.isInBatch()) || ((alignment.isInBatch()) && (!alignment.getBatch().isBatchRunning()))) {
							int[] ix = new int[2];					
							TColumnState[] cs = new TColumnState[2];
							StringSelection[] ss = new StringSelection[2];
							ix[0] = col.getModelIndex();
							cs[0] = col.createSnapshot();
							StringBuffer s = new StringBuffer(spots.size() * 10);
							//toutes les valeurs calculées sont concatenées ( certaienemnt pour se souvenir)
							for (int i = 0; i < spots.size(); i++)
								s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
							ss[0] = new StringSelection(s.toString());
							int[] sel = new int[spots.size()];
							for (int k = 0; k < spots.size(); k++) sel[k] = ( (TSpot) spots.elementAt(k)).getIndex();
							ix[1] = col.getModelIndex();
							cs[1] = col.createSnapshot();
							s = new StringBuffer(spots.size() * 10);
							for (int i = 0; i < spots.size(); i++)
								s.append( ( (TSpot) spots.elementAt(i)).getParameterString(col.getSpotKey()) + "\n");
							ss[1] = new StringSelection(s.toString());
							ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, alignment,
									new TGridAction(false, false, false, false, false, false, afc, iswv, ix, cs, ss, null, sel));
							TEventHandler.handleMessage(ev);
							TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment));
						}
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}	
			alignment.setAlgoRunning(false);
			alignment.setAlgorithm(null);
			ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, alignment);
			TEventHandler.handleMessage(ev);
			alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
			ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
			Boolean zoom = (Boolean)TEventHandler.handleMessage(ev)[0];
			ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, zoom);
			TEventHandler.handleMessage(ev);
			alignment.getView().getTablePanel().setSelectable(true);
		}
		
		
	}
	
	
}
