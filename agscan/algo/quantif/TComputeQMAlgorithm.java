package agscan.algo.quantif;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.TAlgorithmListener;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.model.TAlignmentModel;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Soci�t� : </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TComputeQMAlgorithm extends TAlgorithmListener {
  public static final String NAME = "Calcul du QM";
  public static final String COLUMN_NAME = "QM";
  public static final int ALIGNMENT_MODEL = 0;
  public static final int XRES = 1;
  public static final int SPOTS = 2;

  public TComputeQMAlgorithm(TAlignmentModel alignmentModel) {
    super(NAME + Messages.getString("TComputeQMAlgorithm.0"));//  externalized since 2006/03/20
    workingData = new Object[3];
    workingData[ALIGNMENT_MODEL] = alignmentModel;
    workingData[XRES] = new Double(alignmentModel.getPixelWidth());
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignmentModel.getReference());
    workingData[SPOTS] = TEventHandler.handleMessage(event)[0];
    parameters = null;
    initWithDefaults();
    controlPanel = null;
    initWithLast();
  }
  /**
   * execute
   *
   * @param thread boolean
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void setPriority(int priority) {
    worker.setPriority(priority);
  }
  public void execute(boolean thread) {
    worker = new TComputeQMWorker(this);
    worker.setPriority(Thread.MIN_PRIORITY);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }
  public void execute(boolean thread, int priority) {
    worker = new TComputeQMWorker(this);
    worker.setPriority(priority);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }

  /**
   * init
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init() {
  }

  /**
   * init
   *
   * @param params Object[]
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init(Object[] params) {
  }

  /**
   * initWithDefaults
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithDefaults() {
  }

  /**
   * initWithLast
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithLast() {
  }

  /**
   * setLasts
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void setLasts() {
  }

  public void actionPerformed(ActionEvent event) {
//  externalized since 2006/03/20  
    int rep = JOptionPane.showConfirmDialog(null,Messages.getString("TComputeQMAlgorithm.1") ,
    		Messages.getString("TComputeQMAlgorithm.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    if (rep == JOptionPane.YES_OPTION) {
      worker.stop();
    }
  }
  public void stateChanged(ChangeEvent e) {
    int val = ((JSlider)e.getSource()).getValue();
    switch (val) {
      case 0 :
        worker.setPriority(Thread.MIN_PRIORITY);
        break;
      case 1 :
        worker.setPriority(Thread.NORM_PRIORITY);
        break;
      case 2 :
        worker.setPriority(Thread.MAX_PRIORITY);
        break;
    }
  }
}
