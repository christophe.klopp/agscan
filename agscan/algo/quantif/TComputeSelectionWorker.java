package agscan.algo.quantif;

import java.util.Enumeration;
import java.util.Vector;

import agscan.SwingWorker;
import agscan.algo.TAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.data.model.TAlignmentModel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TComputeSelectionWorker extends SwingWorker {
  private TComputeSelectionAlgorithm csAlgo;

  public TComputeSelectionWorker(TComputeSelectionAlgorithm a) {
    csAlgo = a;
  }
  public Object construct(boolean thread) {
    Vector calculs = (Vector)csAlgo.getWorkingData(TComputeSelectionAlgorithm.CALCULS);
    Enumeration enu = calculs.elements();
    int calc;
    TAlignmentModel alignmentModel = ((TAlignmentModel)csAlgo.getWorkingData(TComputeSelectionAlgorithm.ALIGNMENT_MODEL));
    TAlgorithm algo;
    while (enu.hasMoreElements()) {
      calc = ((Integer)enu.nextElement()).intValue();
      algo = null;
      switch (calc) {
        case TAlignmentControler.COMPUTE_FIT_CORRECTION:
          algo = new TComputeFitCorrectionAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_OVERSHINING_CORRECTION:
          algo = new TComputeOvershiningCorrectionAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_QM:
          algo = new TComputeQMAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_QUANTIF_FIT_COMP:
          algo = new TQuantifFitComputedAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_QUANTIF_FIT_CONST:
          algo = new TQuantifFitConstantAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_QUANTIF_IMAGE_COMP:
          algo = new TQuantifImageComputedAlgorithm(alignmentModel);
          algo.execute(false);
          break;
        case TAlignmentControler.COMPUTE_QUANTIF_IMAGE_CONST:
          algo = new TQuantifImageConstantAlgorithm(alignmentModel);
          algo.execute(false);
          break;
      }
      if (algo != null)
        if (algo.getWorker().getSTOP() == true)
          break;
    }
    return null;
  }
}
