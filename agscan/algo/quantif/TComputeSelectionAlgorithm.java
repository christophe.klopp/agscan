package agscan.algo.quantif;

import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import agscan.Messages;
import agscan.algo.TAlgorithmListener;
import agscan.data.model.TAlignmentModel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TComputeSelectionAlgorithm extends TAlgorithmListener {
  public static final int ALIGNMENT_MODEL = 0;
  public static final int CALCULS = 1;

  public TComputeSelectionAlgorithm(TAlignmentModel alignmentModel, Vector c) {
  	super("");
  
    workingData = new Object[2];
    workingData[ALIGNMENT_MODEL] = alignmentModel;
    workingData[CALCULS] = c;
  }
  /**
   * execute
   *
   * @param thread boolean
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void setPriority(int priority) {
    worker.setPriority(priority);
  }
  public void execute(boolean thread) {
    worker = new TComputeSelectionWorker(this);
    worker.setPriority(Thread.MIN_PRIORITY);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }
  public void execute(boolean thread, int priority) {
    worker = new TComputeSelectionWorker(this);
    worker.setPriority(priority);
    if (thread)
      worker.start();
    else {
      worker.construct(false);
      worker.finished();
    }
  }

  /**
   * init
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init() {
  }

  /**
   * init
   *
   * @param params Object[]
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void init(Object[] params) {
  }

  /**
   * initWithDefaults
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithDefaults() {
  }

  /**
   * initWithLast
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void initWithLast() {
  }

  /**
   * setLasts
   *
   * @todo Implement this image.algo.TAlgorithm method
   */
  public void setLasts() {
  }

  public void actionPerformed(ActionEvent event) {
  	// externalized since 2006/03/20
    int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TComputeSelectionAlgorithm.0"),
    		Messages.getString("TComputeSelectionAlgorithm.1"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    if (rep == JOptionPane.YES_OPTION) {
      worker.stop();
    }
  }
  public void stateChanged(ChangeEvent e) {
    int val = ((JSlider)e.getSource()).getValue();
    switch (val) {
      case 0 :
        worker.setPriority(Thread.MIN_PRIORITY);
        break;
      case 1 :
        worker.setPriority(Thread.NORM_PRIORITY);
        break;
      case 2 :
        worker.setPriority(Thread.MAX_PRIORITY);
        break;
    }
  }
}
