/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.columnmenubuilder;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;

import agscan.Messages;
import agscan.data.model.grid.table.TColumn;
import agscan.menu.TMenuManager;
import agscan.menu.action.TColorizeColumnAction;
import agscan.menu.action.TCopyAction;
import agscan.menu.action.THideColumnAction;
import agscan.menu.action.TInitColumnWithDefaultValueAction;
import agscan.menu.action.TInitColumnWithFileAction;
import agscan.menu.action.TInitColumnWithOtherValueAction;
import agscan.menu.action.TModifyColumnAction;
import agscan.menu.action.TPasteAction;
import agscan.menu.action.TRemoveColumnAction;
import agscan.menu.action.TSortColumnAscendingAction;
import agscan.menu.action.TSortColumnDescendingAction;

public class TProbaColumnMenuBuilder extends TColumnMenuBuilder {
  private static TProbaColumnMenuBuilder instance;
  protected TProbaColumnMenuBuilder() {
    super();
  }
  public void createPopupMenu(TColumn col, TButtonGroup sortBg, boolean sorted, boolean ascending, TButtonGroup colBg,
                              boolean colorized, boolean paste) {
    popupMenu = new JPopupMenu(Messages.getString("TProbaColumnMenuBuilder.0") + col.toString()); //$NON-NLS-1$
    JMenu menuInitialize = new JMenu(Messages.getString("TProbaColumnMenuBuilder.1")); //$NON-NLS-1$
    JMenu menuSort = new JMenu(Messages.getString("TProbaColumnMenuBuilder.2")); //$NON-NLS-1$
    //menuInitialize.add(TMenuManager.getAction(TMenuManager.INIT_WITH_DEFAULT_VALUE));//modif REMI
    menuInitialize.add(TMenuManager.getAction(TInitColumnWithDefaultValueAction.getID()));
    //menuInitialize.add(TMenuManager.getAction(TMenuManager.INIT_WITH_FILE));//modif REMI
    menuInitialize.add(TMenuManager.getAction(TInitColumnWithFileAction.getID()));
    //menuInitialize.add(TMenuManager.getAction(TMenuManager.INIT_WITH_OTHER_VALUE));//modif REMI
    menuInitialize.add(TMenuManager.getAction(TInitColumnWithOtherValueAction.getID()));
    JCheckBoxMenuItem cmi, cmi1, cmi2;
//    cmi1 = new JCheckBoxMenuItem(TMenuManager.getAction(TMenuManager.SORT_ASCENDING));//modif REMI
    cmi1 = new JCheckBoxMenuItem(TMenuManager.getAction(TSortColumnAscendingAction.getID()));
    menuSort.add(cmi1);
    //cmi2 = new JCheckBoxMenuItem(TMenuManager.getAction(TMenuManager.SORT_DESCENDING));//modif REMI
    cmi2 = new JCheckBoxMenuItem(TMenuManager.getAction(TSortColumnDescendingAction.getID()));
    menuSort.add(cmi2);
    sortBg.add(cmi1);
    sortBg.add(cmi2);
    if (sorted) {
      cmi1.setSelected(ascending);
      cmi2.setSelected(!ascending);
    }
    if (col.isEditable()) popupMenu.add(menuInitialize);
    popupMenu.add(menuSort);
    //cmi = new JCheckBoxMenuItem(TMenuManager.getAction(TMenuManager.COLORIZE));//modif REMI
    cmi = new JCheckBoxMenuItem(TMenuManager.getAction(TColorizeColumnAction.getID()));
    colBg.add(cmi);
    popupMenu.add(cmi);
    cmi.setSelected(colorized);
   // cmi = new JCheckBoxMenuItem(TMenuManager.getAction(TMenuManager.HIDE_COLUMN));//modif REMI
    cmi = new JCheckBoxMenuItem(TMenuManager.getAction(THideColumnAction.getID()));
    popupMenu.add(cmi);
   // popupMenu.add(TMenuManager.getAction(TMenuManager.MODIFY_COLUMN));//modif REMI
    popupMenu.add(TMenuManager.getAction(TModifyColumnAction.getID()));
    //if (col.isRemovable()) popupMenu.add(TMenuManager.getAction(TMenuManager.REMOVE_COLUMN));//modif REMI
    if (col.isRemovable()) popupMenu.add(TMenuManager.getAction(TRemoveColumnAction.getID()));
    popupMenu.addSeparator();
    //popupMenu.add(TMenuManager.getAction(TMenuManager.COPY));//modif REMI
    popupMenu.add(TMenuManager.getAction(TCopyAction.getID()));
   // if (col.isEditable()) popupMenu.add(TMenuManager.getAction(TMenuManager.PASTE));//modif REMI
    if (col.isEditable()) popupMenu.add(TMenuManager.getAction(TPasteAction.getID()));
    //TMenuManager.getAction(TMenuManager.PASTE).setEnabled(paste);//modif REMI
    TMenuManager.getAction(TPasteAction.getID()).setEnabled(paste);
  }
  public static TProbaColumnMenuBuilder getInstance() {
    if (instance == null)
      instance = new TProbaColumnMenuBuilder();
    return instance;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
