/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.columnmenubuilder;

import java.util.Enumeration;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TShowColumnAction;

public class TIndexColumnMenuBuilder extends TColumnMenuBuilder {
  private static TIndexColumnMenuBuilder instance;
  public TIndexColumnMenuBuilder() {
    super();
  }
  public void createPopupMenu(TColumn col, TButtonGroup sortBg, boolean sorted, boolean ascending, TButtonGroup colBg,
                              boolean colorized, boolean paste) {
    popupMenu = new JPopupMenu(Messages.getString("TIndexColumnMenuBuilder.0") + col.toString()); //$NON-NLS-1$
    JMenu menuInitialize = new JMenu(Messages.getString("TIndexColumnMenuBuilder.1")); //$NON-NLS-1$
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
    Enumeration enume = ((TGriddedElement)element).getGridModel().getConfig().getColumns().getColumns();
    TColumn column;
    JCheckBoxMenuItem cmi;
    enume.nextElement();
    while (enume.hasMoreElements()) {
      column = (TColumn)enume.nextElement();
      cmi = new JCheckBoxMenuItem(TMenuManager.getAction(TShowColumnAction.getID()));//TMenuManager.SHOW_COLUMN avant
      cmi.setText(column.toString());
      cmi.setSelected(column.isVisible());
      popupMenu.add(cmi);
    }
  }
  public static TIndexColumnMenuBuilder getInstance() {
    if (instance == null)
      instance = new TIndexColumnMenuBuilder();
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
