/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.columnmenubuilder;

import java.util.Enumeration;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;

import agscan.Messages;
import agscan.data.model.grid.table.TColumn;
import agscan.data.model.grid.table.TIntegerListColumn;
import agscan.menu.TMenuManager;
import agscan.menu.action.TCopyAction;
import agscan.menu.action.THideColumnAction;
import agscan.menu.action.TInitColumnWithDefaultValueAction;
import agscan.menu.action.TInitColumnWithFileAction;
import agscan.menu.action.TInitColumnWithListValueAction;
import agscan.menu.action.TModifyColumnAction;
import agscan.menu.action.TPasteAction;
import agscan.menu.action.TRemoveColumnAction;

public class TIntegerListColumnMenuBuilder extends TColumnMenuBuilder {
  private static TIntegerListColumnMenuBuilder instance;
  private JMenu menuOtherValue;
  protected TIntegerListColumnMenuBuilder() {
    super();
  }
  public void createPopupMenu(TColumn col, TButtonGroup sortBg, boolean sorted, boolean ascending, TButtonGroup colBg,
                              boolean colorized, boolean paste) {
    popupMenu = new JPopupMenu(Messages.getString("TIntegerListColumnMenuBuilder.0") + col.toString()); //$NON-NLS-1$
    JMenu menuInitialize = new JMenu(Messages.getString("TIntegerListColumnMenuBuilder.1")); //$NON-NLS-1$
    menuInitialize.add(TMenuManager.getAction(TInitColumnWithDefaultValueAction.getID()));//TMenuManager.INIT_WITH_DEFAULT_VALUE avant
    menuInitialize.add(TMenuManager.getAction(TInitColumnWithFileAction.getID()));//TMenuManager.INIT_WITH_FILE avant
    menuOtherValue = new JMenu(Messages.getString("TIntegerListColumnMenuBuilder.2")); //$NON-NLS-1$
    Integer i;
    TInitColumnWithListValueAction action;
    for (Enumeration enume = ((TIntegerListColumn)col).getValues().elements(); enume.hasMoreElements(); ) {
      i = (Integer)enume.nextElement();
      action = new TInitColumnWithListValueAction(i.toString(), null);
      menuOtherValue.add(action);
    }
    menuInitialize.add(menuOtherValue);
    if (col.isEditable()) popupMenu.add(menuInitialize);
    JCheckBoxMenuItem cmi = new JCheckBoxMenuItem(TMenuManager.getAction(THideColumnAction.getID()));//TMenuManager.HIDE_COLUMN avant
    popupMenu.add(cmi);
    popupMenu.add(TMenuManager.getAction(TModifyColumnAction.getID()));//TMenuManager.MODIFY_COLUMN avant
    if (col.isRemovable()) popupMenu.add(TMenuManager.getAction(TRemoveColumnAction.getID()));//TMenuManager.REMOVE_COLUMN avant
    popupMenu.addSeparator();
    popupMenu.add(TMenuManager.getAction(TCopyAction.getID()));//TMenuManager.COPY avant
    if (col.isEditable()) popupMenu.add(TMenuManager.getAction(TPasteAction.getID()));//TMenuManager.PASTE avant
    TMenuManager.getAction(TPasteAction.getID()).setEnabled(paste);//TMenuManager.PASTE avant
  }
  public static TIntegerListColumnMenuBuilder getInstance() {
    if (instance == null)
      instance = new TIntegerListColumnMenuBuilder();
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
