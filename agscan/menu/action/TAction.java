/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import agscan.TEventHandler;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;

public abstract class TAction extends AbstractAction {

// les TYPES de donnees qui definissent sur lesquelles les actions s'appliquent (utilisés pour dégriser les menus concernés)
public static int INACTIVE_TYPE = 0;
public static int IMAGE_TYPE = 1;// modif REMI
public static int GRID_TYPE = 2;// modif REMI
public static int ALIGNMENT_TYPE = 4;// modif REMI
public static int BATCH_TYPE = 8;
public static int ACTIVE_TYPE = 16;//action toujours active (modif REMI)
  protected JToggleButton toggleButton;
  private int ActiveType;//les types de l'action
  public static int id0;
  //TODO remarque les id des actions differents de celles des plugins? 
  //TODO pourquoi ne pas remonter id et getID directement dans TAction?
  
  
  public TAction(String title, ImageIcon icon) {
    super(title, icon);
    toggleButton = null;
    ActiveType = INACTIVE_TYPE;
    TEvent event = new TEvent(TEventHandler.MENU_MANAGER,TMenuManager.NEXT_ID,null);// modif REMI
    id0=((Integer)TEventHandler.handleMessage(event)[0]).intValue();
   // System.out.println("*** TAction: "+title+"==> id0="+id0);
    
     }
  
  
  // JToggleButton => bouton qui reste enfoncé style le zoom
  public void setToggleButton(JToggleButton tb) {
    toggleButton = tb;
  }
  public void setToggleButtonSelected(boolean b) {
    if (toggleButton != null) toggleButton.setSelected(b);
  }
  
   public int getActiveType(){
  	return ActiveType;
  }
  
  public void setActiveType(int val){
  	ActiveType = val;
  }
  
  protected void incrementID(){
  	id0++;
  }
  
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
