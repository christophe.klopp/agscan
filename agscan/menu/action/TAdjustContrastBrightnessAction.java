package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.Messages;
import agscan.dialog.TcontrastAdjuster;


public class TAdjustContrastBrightnessAction extends TAction {
	public static int id = -1;
	private String selectedMode = "b&c";//b&c (Brightness/Contrast), balance (Color Balance) or wl (Window/Level) 
	public TAdjustContrastBrightnessAction(ImageIcon icon) {
		super(Messages.getString("TAdjustContrastBrightnessAction.0"), icon); //$NON-NLS-1$
		id=id0;
		setActiveType(IMAGE_TYPE+ALIGNMENT_TYPE);
	}
	public static int getID(){
		return id;
	}
	
	public void actionPerformed(ActionEvent e) {
		//TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		//TDataElement element = (TDataElement) TEventHandler.handleMessage(event)[0];
		//event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.CONTRAST_AND_BRIGHTNESS, element);
		//TEventHandler.handleMessage(event);
		//ContrastAdjuster c = new ContrastAdjuster();
		//TODO il serait peut etre plus astucieux de passer en param l'element pour ne pas le demander dans le plugin ij modifié (ca ferait
		// une modif de moins, on le change dans l'entete et voila...non?)
		TcontrastAdjuster c = new TcontrastAdjuster();
	    //c.run("balance");// l'argument balance donne l'acces aux couleusr dans la dialog
		// 18/08/05: on peut donner en argument au run "Balance 
		//c.run("toto");
		c.run(selectedMode);
		//En fait la classe ContrastAdjuster de ij gère 3 fenetres de parametres et 
		// selon l'argument passsé en parametres, c'est la fenetre adequat qui s'ouvre
		// run sans argument, c'est B&C qui apparait = Brightness/Contrast commands
		// run("wl"), c'est wl qui apparait = Window/level commands
		// run("balance"), c'est Color qui apparait = Color Balance commands
		//appelle la dialog avec l'option brightness //$NON-NLS-1$
		//TODO prendre en consideration le type d'image pour l'appel de balance 		
		//  fin test remi
	}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
