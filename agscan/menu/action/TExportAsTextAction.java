package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TAlignmentControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 *
 * <p>Description : </p>
 *
 * <p>Copyright : Copyright (c) 2002</p>
 *
 * <p>Societe : </p>
 *
 * Modification : 2005/10/26:  modification==> setActiveType(TAction.ALIGNMENT_TYPE)
 * @version 2005/10/26
 * @version 1.0
 */
public class TExportAsTextAction extends TAction {
	public static int id = -1;
  class TextFileFilter extends FileFilter {
    private String desc;
    public TextFileFilter(String s) {
      super();
      desc = s;
    }
    public String getDescription() {
      return desc;
    }
    public boolean accept(File file) {
      return (file.getAbsolutePath().substring(file.getAbsolutePath().length() - 4).equalsIgnoreCase(".txt"));
    }
  }
  public TExportAsTextAction() {
    super(Messages.getString("TExportAsTextAction.0"),//externalized since 2006/02/16 
    		new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/text01.gif")));
    id=id0;
    setActiveType(TAction.ALIGNMENT_TYPE);//modified - 2005/10/26
  }
  public static int getID(){
  	return id;
  }
  public void actionPerformed(ActionEvent e) {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
    JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("exportsPath"));//modified 2005/11/29 
  //2006/03/08 - .txt chooser filter is removed 
    //if (element instanceof TAlignment) {
      //chooser.addChoosableFileFilter(new TextFileFilter("Text file (*.txt)"));
    // }
    
    // 2006/03/08 modification - The  suggested name in the chooser is the .zaf alignment name with a ".txt" extension instead of ".zaf".
    String name = element.getName();
    name = name.substring(0,name.length()-4)+".txt";
    	
    
    //chooser.setSelectedFile(new File(element.getName()));TEST
    chooser.setSelectedFile(new File(name));//TEST
    int returnVal = chooser.showSaveDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String desc = chooser.getFileFilter().getDescription();
      File file = chooser.getSelectedFile();
      String filename = file.getAbsolutePath();
      event = null;
      if (desc.equals("Text file (*.txt)") && !filename.substring(filename.length() - 4).equalsIgnoreCase(".txt"))
        filename += ".txt";
      event = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.EXPORT_AS_TEXT, element, new File(filename));
      TEventHandler.handleMessage(event);
      AGScan.prop.setProperty("exportsPath",chooser.getCurrentDirectory().getAbsolutePath());//added 2005/11/29
    }
  }
}
