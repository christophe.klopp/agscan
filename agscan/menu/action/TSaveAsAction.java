package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Enumeration;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.element.image.TImage;
import agscan.data.model.TImageModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.dialog.TFileImageChooser;
import agscan.event.TEvent;
import agscan.ioxml.TAlignmentFileChooser;
import agscan.ioxml.TBatchFileChooser;
import agscan.plugins.TPluginManager;
import agscan.data.element.grid.TGrid;

public class TSaveAsAction extends TAction {
	public static int id = -1;// modif REMI
	public TSaveAsAction(String title, ImageIcon icon) {
		super(title, icon);
		id=id0;
		setActiveType(ALIGNMENT_TYPE+IMAGE_TYPE+GRID_TYPE+BATCH_TYPE);//BATCH_TYPE ADDED 2005/11/29
	}
	public static int getID(){
		return id;
	}
	
	public void actionPerformed(ActionEvent e) {
		int nbOfSave = 1;//added 2005/11/02 - count of elements to save (1 by default for per example a grid but n if n channels images are open!)  
		TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
		JFileChooser chooser = null;
		String fileNameToSave = element.getName();//added 2005/11/02 - name of current element to save (warnin gseveral names for n channels)
		
		if (element instanceof TAlignment){
			System.out.println("alignement a sauver!");
			chooser = new TAlignmentFileChooser();
			chooser.addChoosableFileFilter(new ImageFilter("zaf", Messages.getString("TSaveAsAction.15"))); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else if (element instanceof TImage) {
			System.out.println("image(s) a sauver!");
			TImageModel tImageModel = (TImageModel) element.getModel();//for count the number of channels
			nbOfSave =  tImageModel.getNumberOfChannels();
			chooser = new TFileImageChooser();
		}
		else if (element instanceof TGrid) {
			System.out.println("grille a sauver!");
			chooser = new JFileChooser(AGScan.prop.getProperty("gridsPath"));
			chooser.addChoosableFileFilter(new ImageFilter("grd", Messages.getString("TSaveAsAction.13"))); //$NON-NLS-1$ //$NON-NLS-2$
		}
		//2005/11/29 batch case added
		else if (element instanceof TBatch){
			System.out.println("batch a sauver!");
			chooser = new TBatchFileChooser();
			//chooser = new JFileChooser(AGScan.prop.getProperty("batchPath"));//batch case
			chooser.addChoosableFileFilter(new ImageFilter("bzb", "Batchs"));
		}
					
		// here we repeat for each file to save the save procedure
		System.out.println("NB of SAVES="+nbOfSave);
		if (nbOfSave>1) {// = case of several images, we inform the user of the number of saves to do
			JOptionPane.showMessageDialog(null,Messages.getString("TSaveAsAction.38")+nbOfSave+Messages.getString("TSaveAsAction.39"), Messages.getString("TSaveAsAction.40"),JOptionPane.INFORMATION_MESSAGE);//externalized since 2006/02/20 
		}
		for (int index=0;index<nbOfSave;index++){
			//chooser.setSelectedFile(new File(element.getName()));
			
			if  (element instanceof TImage){
				chooser.setDialogTitle("Save of image number "+(index+1));			
				TImageModel tImageModel = (TImageModel) element.getModel();//for count the number of channels
				fileNameToSave =  (tImageModel.getChannelImage(index+1)).getTitle();//recup of the name
			}
			chooser.setSelectedFile(new File(fileNameToSave));
			int returnVal = chooser.showSaveDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String desc = chooser.getFileFilter().getDescription();
				System.out.println("la description est : "+desc);
				File newFile = chooser.getSelectedFile();
				event = null;
				
				// recuperation du type d'image si image choisie      
				// on recupere tous les types d'extensions possibles choisis via le chooser
				// the exts array contains all extensions supported = tif + plugins extensions 
				String[] exts = new String[TPluginManager.getExtensionsIntoArray().length + 1];
				exts[0] = "tif"; //$NON-NLS-1$
				int i = 1;// pour commencer a 1 puisque l'extension 0 est le tif
				// on regarde si un type d'image a enregistrer a ete choisi     
				boolean ok = false;// sera a true si l'extension choisie vient d'un plugin
				String ext = null;// l'extension choisie
				for (Enumeration enume = TPluginManager.extensions.keys();
				enume.hasMoreElements();) {
					String current = (String) enume.nextElement();
					System.out.println("current =" + current); //$NON-NLS-1$
					exts[i] = current;
					System.out.println(desc);
					System.out.println(current.toUpperCase() + " images (*." + current + ")");
					if (desc.equals(current.toUpperCase() + " images (*." + current + ")")){
						ok = true;
						ext = current;
						// on sait l'extension qu'a choisit l'utilisateur
						// il faudra en sauvant l'ajouter...
						String filename = newFile.getAbsolutePath();
						
					}
					i++;
				}
				

				
				
				if (desc.equals("TIFF images (*." + exts[0] + ")")){// if TIF format chosen
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_IMAGE, null, newFile,"tif");
				}
				else if (ok == true) // a particular format chosen
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_IMAGE, null, newFile,ext);
				else if (desc.equals(Messages.getString("TSaveAsAction.22"))) //$NON-NLS-1$ //grid case
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS, null, newFile);
				else if (desc.equals(Messages.getString("TSaveAsAction.23"))) //$NON-NLS-1$ // alignment case
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS, null, newFile, new Boolean(((TAlignmentFileChooser)chooser).isImageCheckBoxSelected()));
				//2005/11/29 - Here is missing the batch case. It's why the 211105 AGScan version considers .bzb extension as an image format
				else if (desc.equals("Batchs (.bzb)")) //batch case
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS, null, newFile,((TBatchModelNImages)element.getModel()).getSaveImages());
				else { // cas generique du style "all images" choisi...il faut retrouver l'extension ecrite
					String filename = newFile.getAbsolutePath();
					int ix = filename.lastIndexOf((int)'.');
					if (ix > 0) {
						String extension = filename.substring(ix+1);
						System.out.println("extens="+extension);    
						i = 1;
						for (Enumeration enume = TPluginManager.extensions.keys();enume.hasMoreElements();) {
							String current = (String) enume.nextElement();
							System.out.println("current ext="+current);
							exts[i] = current;
							if (extension.equalsIgnoreCase(current)){
								System.out.println("ok = true!");
								ok = true;
								ext = current;
							}
							i++;
						}
						
						/*
						 if (extension.equalsIgnoreCase(".inf")) //$NON-NLS-1$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_FUJI, null, file);
						 else if (extension.equalsIgnoreCase(".pcb")) //$NON-NLS-1$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_TINA, null, file);
						 else if (extension.equalsIgnoreCase(".jpg")) //$NON-NLS-1$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_JPEG, null, file);
						 else if (extension.equalsIgnoreCase(".tif") || extension.equalsIgnoreCase(".tiff")) //$NON-NLS-1$ //$NON-NLS-2$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_TIFF, null, file);
						 else if (extension.equalsIgnoreCase(".png")) //$NON-NLS-1$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_PNG, null, file);
						 else if (extension.equalsIgnoreCase(".bmp")) //$NON-NLS-1$
						 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_BMP, null, file);
						 */
						if (extension.equalsIgnoreCase("tif")) //$NON-NLS-1$
							event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_IMAGE, null, newFile,"tif",new Integer(index+1));//add of a param 2005/11/02
						else  if (ok ==true ) //$NON-NLS-1$
							event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS_IMAGE, null, newFile,ext,new Integer(index+1));//add of a param 2005/11/02
						else if (extension.equalsIgnoreCase("grd")) //$NON-NLS-1$
							event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS, null, newFile);
						else if (extension.equalsIgnoreCase("zaf")) //$NON-NLS-1$
							event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE_AS, null, newFile, new Boolean(((TAlignmentFileChooser)chooser).isImageCheckBoxSelected()));
						else
							JOptionPane.showMessageDialog(null, Messages.getString("TSaveAsAction.33") + extension + //$NON-NLS-1$
									Messages.getString("TSaveAsAction.34"), Messages.getString("TSaveAsAction.35"), //$NON-NLS-1$ //$NON-NLS-2$
									JOptionPane.WARNING_MESSAGE);
					}
					
					else
						JOptionPane.showMessageDialog(null, Messages.getString("TSaveAsAction.36"), Messages.getString("TSaveAsAction.37"), //$NON-NLS-1$ //$NON-NLS-2$
								JOptionPane.WARNING_MESSAGE);
				}
				if (event != null) TEventHandler.handleMessage(event);
				
				TEvent e1 = null;
				e1 = new TEvent(TEventHandler.DATA_MANAGER,
						TDataManager.GET_CURRENT_DATA, null); //REMI
				
				TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(e1)[0]; //REMI
				
				currentElement.setName(newFile.getName());
				
				System.out.println("nom sauvegarde "+currentElement.getName());
				
			    TEvent event2 = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME,newFile.getName());
			    TEventHandler.handleMessage(event2);
				
			}//fin de la boucle sur tous les elements a sauver
		}
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
