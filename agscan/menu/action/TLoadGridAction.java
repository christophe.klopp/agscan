package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;

public class TLoadGridAction extends TAction {
	public static int id = -1;// modif REMI
 // private File currentDirectory = null;//removed 2005/11/29
  public TLoadGridAction() {
    super(Messages.getString("TLoadGridAction.0"), 
    		new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/gridGreen.gif")));
    id=id0;
   // setActiveType(IMAGE_TYPE+GRID_TYPE+ALIGNMENT_TYPE);modified 2005/10/28
    setActiveType(IMAGE_TYPE);
    //plante pour l'instant sur l'alignement
    // il faudrait qu'on puisse charger une autre grille sur un ali (permettre ce changement)
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
    System.err.println("Charger une grille ..."); //$NON-NLS-1$
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
    //System.err.println(element.getPath());
    if (element.getPath() == "") { //$NON-NLS-1$
      JOptionPane.showMessageDialog(null, Messages.getString("TLoadGridAction.1"), //$NON-NLS-1$
                                    Messages.getString("TLoadGridAction.4"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
    }
    else {
    	JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("gridsPath")); //modified 2005/11/29
    //  if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);// removed 2005/11/29
      ImageFilter ff = new ImageFilter("grd", Messages.getString("TLoadGridAction.6")); //$NON-NLS-1$ //$NON-NLS-2$
      chooser.addChoosableFileFilter(ff);
      chooser.addChoosableFileFilter(new ImageFilter("xml", Messages.getString("TLoadGridAction.8"))); //$NON-NLS-1$ //$NON-NLS-2$
      chooser.setFileFilter(ff);
      int returnVal = chooser.showOpenDialog(null);
    //  currentDirectory = chooser.getCurrentDirectory();//removed 2005/11/29
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = chooser.getSelectedFile();
        event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.LOAD_GRID, null, file, element);
        TEventHandler.handleMessage(event);
        AGScan.prop.setProperty("gridsPath",chooser.getCurrentDirectory().getAbsolutePath());//added 2005/11/29
      }
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
