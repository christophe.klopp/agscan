/*
 * Created on 19 aout 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.menu.action;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.Messages;
import agscan.dialog.TParametersDialog;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// action en réponse à la selection d'un menu
public class TParametersAction extends TAction {
	public static int id = -1; // modif REMI
	
	public TParametersAction(ImageIcon icon) {
		super(Messages.getString("TParametersAction.0"), icon); //MENU TEST REMI  //$NON-NLS-1$
		id = id0;
		setActiveType(ACTIVE_TYPE);
	}
	
	public static int getID() {
		return id;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		//TODO appeler la dialog avec le manager des dialog? 	
		TParametersDialog tParametersDialog = new TParametersDialog();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		tParametersDialog.setLocation((screenSize.width - tParametersDialog.getWidth()) / 2, (screenSize.height - tParametersDialog.getHeight()) / 2);
		tParametersDialog.setVisible(true);
		
	}
}
