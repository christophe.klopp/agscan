package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.event.TEvent;

public class TCloseAllAction extends TAction {
	public static int id = -1;// modif REMI
  public TCloseAllAction(ImageIcon icon) {
    super(Messages.getString("TCloseAllAction.0"), icon); //$NON-NLS-1$
    id=id0;
    
       setActiveType(IMAGE_TYPE+GRID_TYPE+ALIGNMENT_TYPE);//actif tt le tps
  }

  public static int getID(){
  	return id;
  }
  public void actionPerformed(ActionEvent e) {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.CLOSE_ALL, null);
    TEventHandler.handleMessage(event);
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
