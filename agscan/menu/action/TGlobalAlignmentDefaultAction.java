package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TTemplateMatchingGlobalAlignmentAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.TPluginManager;

public class TGlobalAlignmentDefaultAction extends TAction {
	public static int id = -1;// modif REMI
	public final static int G_SNIFFER = 0;
	public final static int G_TEMPMATCH = 1;
	public TGlobalAlignmentDefaultAction(ImageIcon icon) {
    super(Messages.getString("TGlobalAlignmentDefaultAction.0"), icon); //$NON-NLS-1$
    id=id0;
    setActiveType(ALIGNMENT_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
    int algo = AGScan.prop.getIntProperty("TShorcutParameters.ga");
    System.out.println("Numero de l alignement: "+algo);
    int nbchan = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");
    TGlobalAlignmentAlgorithm gaAlgo=null;
    switch(algo)
    {
    	case G_SNIFFER : 	if (nbchan == 1)
    						{
	    					 	gaAlgo = new TSnifferGlobalAlignmentAlgorithm();
								// TSnifferGlobalAlignmentAlgorithm gaAlgo = new TSnifferGlobalAlignmentAlgorithm();
						        gaAlgo.initWithDefaults();
						        TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.GLOBAL_ALIGNMENT, null, gaAlgo);
						        TEventHandler.handleMessage(event);
    						}
    						else
    						{
    							TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
    							TEventHandler.handleMessage(ev);
    							return;
    						}
    						break;
    	case G_TEMPMATCH :  if (nbchan == 1 || nbchan == 2)
    						{
    							gaAlgo = new TTemplateMatchingGlobalAlignmentAlgorithm(); 
    							// TSnifferGlobalAlignmentAlgorithm gaAlgo = new TSnifferGlobalAlignmentAlgorithm();
    						    gaAlgo.initWithDefaults();
    						    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.GLOBAL_ALIGNMENT, null, gaAlgo);
    						    TEventHandler.handleMessage(event);
    						}
					    	else
							{
								TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
								TEventHandler.handleMessage(ev);
								return;
							}
    						break;
    						
    	default :			if (algo>=100)
    						{
    							int iplugin = Integer.parseInt(Integer.toString(algo).substring(1,3));
    							System.out.println("Indice du plugin choisi: "+iplugin);
    							SAlignPlugin plug = (SAlignPlugin)TPluginManager.plugs[iplugin];
    							if(nbchan>=plug.getImagesMin()&&nbchan<=plug.getImagesMax())plug.run();
    							else
    							{
    								TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
    								TEventHandler.handleMessage(ev);
    								return;
    							}
    						}
					    	
    		
    }
	
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
