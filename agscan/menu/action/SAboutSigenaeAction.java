/*
 * Created on Oct 27, 2005
 * 
 * This class is the action associated with the presentation of SIGENAE into About menu
 * 	
 * @version 2005/10/27
 * @author rcathelin
 * 
 */

package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import agscan.FenetrePrincipale;
import agscan.Messages;

public class SAboutSigenaeAction extends TAction {
	public static int id = -1;
	public SAboutSigenaeAction() {
    super(Messages.getString("SAboutSigenaeAction.0"),
    		new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/sigenae.gif"))); 
    id=id0;
    setActiveType(ACTIVE_TYPE);//actif tt le tps
  }
  
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
    JOptionPane.showMessageDialog(null, Messages.getString("SAboutSigenaeAction.1"), Messages.getString("SAboutSigenaeAction.2"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
  }
}
