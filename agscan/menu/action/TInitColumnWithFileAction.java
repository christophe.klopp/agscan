/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridControler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;

public class TInitColumnWithFileAction extends TAction {
	public static int id = -1;// modif REMI
  public TInitColumnWithFileAction(ImageIcon icon) {
    super(Messages.getString("TInitColumnWithFileAction.0"), icon); //$NON-NLS-1$
    id=id0;
    setActiveType(ALIGNMENT_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
  	JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("columnsPath"));//modified 2005/11/29
    chooser.addChoosableFileFilter(new ImageFilter("txt", Messages.getString("TInitColumnWithFileAction.2"))); //$NON-NLS-1$ //$NON-NLS-2$
    String[] exts = {"txt"}; //$NON-NLS-1$
    int returnVal = chooser.showOpenDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION) {    	
      File file = chooser.getSelectedFile();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
      TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
      int i = ((TGriddedElement)element).getGridModel().getSelectedColumn();
      TColumn column = (TColumn)((TGriddedElement)element).getGridModel().getConfig().getColumns().getColumn(i);
      event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.INIT_COLUMN_WITH_FILE, null, column, file);
      TEventHandler.handleMessage(event);
      AGScan.prop.setProperty("columnsPath",chooser.getCurrentDirectory().getAbsolutePath());//added 2005/11/29
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
