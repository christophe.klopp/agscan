/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.data.controler.TImageControler;
import agscan.event.TEvent;

public class TFlipHorizontalAction extends TAction {
	public static int id = -1;// modif REMI
  private int value;
  public TFlipHorizontalAction(String title, ImageIcon icon) {
    super(title, icon);
    id=id0;
    setActiveType(IMAGE_TYPE);
    value = 0;
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
    TEvent event = null;
    switch(value) {
      case 0 :
        event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.HORIZONTAL_FLIP, null,"true");
        break;
      case 1 :
        event = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.VERTICAL_FLIP, null,"true");
        break;
    }
    if (event != null) {
      TEventHandler.handleMessage(event);
      //pour recuperer l'image courante:
      //event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
      //TDataElement currentElement = (TDataElement)TEventHandler.handleMessage(event)[0];
      //event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, currentElement, null);
      //TEventHandler.handleMessage(event);
    }
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
