package agscan.menu.action;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import agscan.AGScan;
import agscan.Messages;
import agscan.dialog.TColorParametersDialog;
import agscan.dialog.TParametersDialog;
import agscan.dialog.TProcessChooserDialog;
import agscan.plugins.TPlugin;


public class TProcessChooserAction extends TAction{

		public static int id = -1;

		public TProcessChooserAction(ImageIcon icon)
		{
			super(Messages.getString("TProcessChooserAction.0"),icon); 
			id = id0;
			setActiveType(ACTIVE_TYPE);
		}
		
		public static int getID() {
			return id;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			TProcessChooserDialog tProcChooserDialog = new TProcessChooserDialog(NAME);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			tProcChooserDialog.setLocation((screenSize.width - tProcChooserDialog.getWidth()) / 2, (screenSize.height - tProcChooserDialog.getHeight()) / 2);
			tProcChooserDialog.setVisible(true);
		}
}

		