/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.util.Enumeration;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;

public class TShowColumnAction extends TAction {
	public static int id = -1;// modif REMI
  public TShowColumnAction(ImageIcon icon) {
    super(Messages.getString("TShowColumnAction.0"), icon); //$NON-NLS-1$
    id=id0;
    setActiveType(ALIGNMENT_TYPE+GRID_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
    JCheckBoxMenuItem source = (JCheckBoxMenuItem)e.getSource();
    String columnName = source.getText();
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(event)[0];
    Enumeration columns = ((TGriddedElement)element).getGridModel().getConfig().getColumns().getColumns();
    TColumn column;
    while (columns.hasMoreElements()) {
      column = (TColumn)columns.nextElement();
      if (column.toString().equals(columnName)) {
        if (((JMenuItem)e.getSource()).isSelected())
          event = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.SHOW_COLUMN, null, column);
        else {
          event = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.HIDE_COLUMN, null, column);
          ((TGriddedElement)element).getGridModel().selectColumn(0);
        }
        TEventHandler.handleMessage(event);
        column.setVisible(((JMenuItem)e.getSource()).isSelected());
        break;
      }
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
