/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import ij.ImageStack;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TImageControler;
import agscan.data.element.TDataElement;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.view.graphic.interactor.TRoiInteractor;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.factory.TImageModelFactory;
import agscan.menu.TMenuManager;

public class TCropAction extends TAction {
	public static int id = -1;
	private Rectangle cropRect;
	
  public TCropAction(String title, ImageIcon icon) {
    super(title, icon);
    id=id0;
    setActiveType(IMAGE_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
  	cropRect = TRoiInteractor.getInstance().getROI();// pour recuperer le ROI 
	TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
	TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
	double zoom = element.getView().getGraphicPanel().getZoom();
	if (zoom < 0) zoom = 1.0D / -zoom;
	////  int x = (int)((double)event.getX() / zoom);
	////  int y = (int)((double)event.getY() / zoom);
	////  if ((event.getClickCount() == 2) && (x > (cropRect.x + 3)) && (x < (cropRect.x + cropRect.width - 3)) && (y > (cropRect.y + 3)) && (y < (cropRect.y + cropRect.height - 3))) {
	if (cropRect!=null){//remi
		TImageModel tim = (TImageModel)element.getModel();
		String fileName = ((TImage)element).getName();
	
		ImageStack cropStack = TRoiInteractor.getInstance().getRoiStack(element);
		
		//creation du model de la nouvelle image
		TImageModelFactory timf = TImageModelFactory.getImageModelFactory(tim.getFileFormat());
		tim = timf.createImageModel(cropStack, ImagePlusTools.getImageData(tim.getInitialImage(),16), (int)tim.getPixelWidth(), (int)tim.getPixelHeight(),
				tim.getElementWidth(), tim.getImageBitDepth(), tim.getUnit(), tim.getIPType(),
				ImagePlusTools.getLatitude(), tim.getFileFormat(),cropRect);
		TImage image = null;
			
		if (tim != null) {
			// creation of the cropped image
			//2005/10/26 - modification of the cropped image name
			int i = fileName.lastIndexOf((int)'.');//-1 if no extension
			String cropName = null;//name of the crop image
			if (i==-1){
				cropName = fileName + "_CROP";
			}
			else{
				cropName = fileName.substring(0,i)+"_CROP"+fileName.substring(i,fileName.length());
				//System.err.println("apres crop"+ cropName);
			}
			image = new TImage(cropName, tim);
			
			tim.setReference(image);
			image.setPath("");
			image.getView().setZoom(element.getView().getGraphicPanel().getZoom());
			image.getModel().addModif(1);
			ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, image);
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, image.getView());
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.MIN_AND_MAX, null, new Double(((TImage)element).getImageView().getImageGraphicPanel().getMin()), new Double(((TImage)element).getImageView().getImageGraphicPanel().getMax()));
			TEventHandler.handleMessage(ev);//TODO voir si utile
			ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ROI_MODE, new Boolean(false));
			TEventHandler.handleMessage(ev);
			ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TRoiAction.getID()), new Boolean(false));//TMenuManager.CROP avant
			TEventHandler.handleMessage(ev);
			element.getView().getGraphicPanel().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
	}
	else {
		//selected area is empty!
		 TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
                TDialogManager.ERROR_DIALOG, "No selection!");
        TEventHandler.handleMessage(event);			
	}
	
   }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
