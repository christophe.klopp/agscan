package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.event.TEvent;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TOpenBatchAction extends TAction {
	public static int id = -1;
//  private File currentDirectory = null;//removed 2005/11/29
  public TOpenBatchAction(ImageIcon icon) {
    super("Batch", icon);
    id=id0;
    setActiveType(ACTIVE_TYPE);
  }
  public static int getID(){
  	return id;
  }
  public void actionPerformed(ActionEvent e) {
    System.err.println("Open batch !");
    JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("batchPath"));// modified 2005/11/29
   // if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);/removed 2005/11/29
    ImageFilter ff = new ImageFilter("bzb", "Batchs (*.bzb)");
    chooser.addChoosableFileFilter(ff);
    String[] exts = {"bzb"};
    chooser.setFileFilter(ff);
    int returnVal = chooser.showOpenDialog(null);
    //currentDirectory = chooser.getCurrentDirectory();//removed 2005/11/29
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = chooser.getSelectedFile();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_BATCH, null, file);
      TEventHandler.handleMessage(event);
      AGScan.prop.setProperty("batchPath",chooser.getCurrentDirectory().getAbsolutePath());//added 2005/11/29
    }
  }
}
