/*
 * Created on 4 mai 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.plugins.TPlugin;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// action en reponse a la selection d'un menu
public class TMenuTestRemiAction extends TPlugin {
    public static int id = -1; // modif REMI

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public TMenuTestRemiAction(ImageIcon icon) {
        super("Menu Test REMI!", icon); //MENU TEST REMI 
        id = id0;
        setActiveType(ACTIVE_TYPE);
    }

    public static int getID() {
        return id;
    }

    // l'actionPerformed d'une action va envoyer un message (= creation d'un TEvent) a la classe communiquante concerne (parmi les 5 possibles)
    // dans notre cas du test on envoie un message au dialogManager
    public void actionPerformed(ActionEvent e) {
        TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
                TDialogManager.MESSAGE_DIALOG, "texte approprie a ce menu...");
        TEventHandler.handleMessage(event);
    }
}
