package agscan.menu.action;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.table.TColumn;
import agscan.dialog.TColorParametersDialog;
import agscan.dialog.TShorcutParametersDialog;
import agscan.event.TEvent;

public class TShorcutParametersAction extends TAction {
	public static int id = -1;// modif REMI
	  public TShorcutParametersAction(ImageIcon icon) {
	    super(Messages.getString("TShowColumnAction.0"), icon); //$NON-NLS-1$
	    id=id0;
	    setActiveType(ACTIVE_TYPE);
	  }
	  public static int getID(){
	  	return id;
	  }
	  
	  public void actionPerformed(ActionEvent e) {
		  TShorcutParametersDialog tScParametersDialog = new TShorcutParametersDialog();
		  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		  tScParametersDialog.setLocation((screenSize.width - tScParametersDialog.getWidth()) / 2, (screenSize.height - tScParametersDialog.getHeight()) / 2);
		  tScParametersDialog.setVisible(true);
	  }
}
