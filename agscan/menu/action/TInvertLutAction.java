/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author REMI
 * @version 1.0
 */
package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TImageControler;
import agscan.data.element.TDataElement;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;


public class TInvertLutAction extends TAction {
    public static int id = -1;
    private int value;

    public TInvertLutAction(String title, ImageIcon icon) {
        super(title, icon);
        id = id0;
        setActiveType(IMAGE_TYPE);
    }

    public static int getID() {
        return id;
    }

    public void actionPerformed(ActionEvent e) {
        // 2 philosophies: faire l'invertLUT
        TEvent event = null;
        event = new TEvent(TEventHandler.DATA_MANAGER,
                TImageControler.INVERT_LUT, null);
        TEventHandler.handleMessage(event);
        event = new TEvent(TEventHandler.DATA_MANAGER,
                TDataManager.GET_CURRENT_DATA, null);

        TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0];
        event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE,
                currentElement, null);
        TEventHandler.handleMessage(event);
    }
}
