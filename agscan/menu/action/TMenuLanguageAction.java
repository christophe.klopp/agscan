/*
 * Created on 11 aout 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.menu.action;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.Messages;
import agscan.dialog.TChooseLanguageDialog;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// action en réponse à la selection d'un menu
public class TMenuLanguageAction extends TAction {
	public static int id = -1; // modif REMI
	
	public TMenuLanguageAction(ImageIcon icon) {
		super(Messages.getString("TMenuLanguageAction.0"), icon); //MENU TEST REMI 
		id = id0;
		setActiveType(ACTIVE_TYPE);
	}
	
	public static int getID() {
		return id;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		//TODO appeler la dialog avec le manager des dilog? 	
		TChooseLanguageDialog tChooseLanguageDialog = new TChooseLanguageDialog();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		tChooseLanguageDialog.setLocation((screenSize.width - tChooseLanguageDialog.getWidth()) / 2, (screenSize.height - tChooseLanguageDialog.getHeight()) / 2);
		tChooseLanguageDialog.setVisible(true);
		
	}
}
