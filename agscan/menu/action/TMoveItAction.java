/*
 * Created on 4 mai 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.event.TEvent;
import agscan.plugins.TPlugin;


/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// action en reponse a la selection d'un menu
public class TMoveItAction extends TAction {
	public static int id = -1; // modif REMI
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public TMoveItAction(ImageIcon icon) {
		super("Move It!", icon); //MENU TEST REMI 
		id = id0;
		setActiveType(ALIGNMENT_TYPE);
	}
	
	public static int getID() {
		return id;
	}
	
	// l'actionPerformed d'une action va envoyer un message (= creation d'un TEvent) a la classe communiquante concerné (parmi les 5 possibles)
	// dans notre cas du test on envoie un message au dialogManager
	public void actionPerformed(ActionEvent e) {
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, null);
		TEventHandler.handleMessage(ev);
		 ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, null,
				new Double(100),new Double(100));
		TEventHandler.handleMessage(ev);
	}
}
