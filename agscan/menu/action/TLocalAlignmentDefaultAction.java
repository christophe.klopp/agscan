package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.factory.TLocalAlignmentAlgorithmFactory;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;

public class TLocalAlignmentDefaultAction extends TAction {
	public static int id = -1;// modif REMI
	public final static int L_BLOC = 0;
	public final static int L_GRID = 1;
	public final static int L_TEMPMATCH = 2;
  public TLocalAlignmentDefaultAction(ImageIcon icon) {
    super(Messages.getString("TLocalAlignmentDefaultAction.0"), icon); //$NON-NLS-1$
    id=id0;
    setActiveType(ALIGNMENT_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
	
	TLocalAlignmentAlgorithmFactory factory = new TLocalAlignmentAlgorithmFactory();
	  
	int algo = AGScan.prop.getIntProperty("TShorcutParameters.la");
	int nbchan = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");
	TLocalAlignmentAlgorithm laba=null;
	switch(algo)
	{
	    	case L_BLOC : 	if(nbchan == 1)
	    					laba = (TLocalAlignmentBlockAlgorithm)factory.createAlgorithm(TLocalAlignmentBlockAlgorithm.NAME);
	    					else
	    					{
	    						TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
	    						TEventHandler.handleMessage(ev);
	    						return;
	    					}
	    					break;
	    	case L_GRID : 	if (nbchan == 1)
	    					laba = (TLocalAlignmentGridAlgorithm)factory.createAlgorithm(TLocalAlignmentGridAlgorithm.NAME);
	    					else
	    					{
	    						TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
	    						TEventHandler.handleMessage(ev);
	    						return;
	    					}
	    					break;
	    	case L_TEMPMATCH : 	if (nbchan == 1 || nbchan ==2)
	    						laba = (TLocalAlignmentBlockTemplateAlgorithm)factory.createAlgorithm(TLocalAlignmentBlockTemplateAlgorithm.NAME);
						    	else
								{
									TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
									TEventHandler.handleMessage(ev);
									return;
								}
	}			
	
    //TLocalAlignmentBlockAlgorithm laba = (TLocalAlignmentBlockAlgorithm)factory.createAlgorithm(TLocalAlignmentBlockAlgorithm.NAME);
    laba.initWithDefaults();
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.LOCAL_ALIGNMENT, null, laba, new Boolean(true));
    TEventHandler.handleMessage(event);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
