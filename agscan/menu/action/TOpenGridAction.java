/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.event.TEvent;

public class TOpenGridAction extends TAction {
	public static int id = -1;// modif REMI
 // private File currentDirectory = null;// removed 2005/11/29

  public TOpenGridAction(ImageIcon icon) {
    super(Messages.getString("TOpenGridAction.0"), icon); //$NON-NLS-1$
    id=id0;
    setActiveType(ACTIVE_TYPE);
  }
  public static int getID(){
  	return id;
  }
  
  public void actionPerformed(ActionEvent e) {
     JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("gridsPath")); //modified 2005/11/29
    //if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);//removed 2005/11/29
    ImageFilter ff = new ImageFilter("grd", Messages.getString("TOpenGridAction.2")); //$NON-NLS-1$ //$NON-NLS-2$
    chooser.addChoosableFileFilter(ff);
    chooser.addChoosableFileFilter(new ImageFilter("xml", Messages.getString("TOpenGridAction.1"))); //$NON-NLS-1$ //$NON-NLS-2$
    String[] exts = {"grd", "xml"}; //$NON-NLS-1$ //$NON-NLS-2$
    chooser.setFileFilter(ff);
    int returnVal = chooser.showOpenDialog(null);
    //currentDirectory = chooser.getCurrentDirectory();//removed 2005/11/29
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = chooser.getSelectedFile();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_GRID, null, file);
      TEventHandler.handleMessage(event);
      AGScan.prop.setProperty("gridsPath",chooser.getCurrentDirectory().getAbsolutePath());//added 2005/11/29
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
