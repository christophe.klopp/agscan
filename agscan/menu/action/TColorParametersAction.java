package agscan.menu.action;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import agscan.AGScan;
import agscan.Messages;
import agscan.dialog.TColorParametersDialog;
import agscan.dialog.TParametersDialog;
import agscan.plugins.TPlugin;


public class TColorParametersAction  extends TAction{

		public static int id = -1;

		public TColorParametersAction(ImageIcon icon)
		{
			super(Messages.getString("TColorParametersDialog.18"),icon); 
			id = id0;
			setActiveType(ACTIVE_TYPE);
		}
		
		public static int getID() {
			return id;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			TColorParametersDialog tColParametersDialog = new TColorParametersDialog();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			tColParametersDialog.setLocation((screenSize.width - tColParametersDialog.getWidth()) / 2, (screenSize.height - tColParametersDialog.getHeight()) / 2);
			tColParametersDialog.setVisible(true);
		}
}

		