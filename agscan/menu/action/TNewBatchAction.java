package agscan.menu.action;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.event.TEvent;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TNewBatchAction extends TAction {
	public static int id = -1;
  public TNewBatchAction() {
    super(Messages.getString("TMenuManager.31"), 
    		new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/gear.gif")));//TODO rename TMenuManager.31...
    id=id0;
    setActiveType(ACTIVE_TYPE);
  }
  public static int getID(){
  	return id;
  }
  public void actionPerformed(ActionEvent e) {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.NEW_BATCH, null);
    TEventHandler.handleMessage(event);

/*    TGrid grid = new TGrid(new TGridConfig());
    TBatch batch = new TBatch(grid);
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, batch);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, batch.getView());
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.NEW_GRID, grid);
    TEventHandler.handleMessage(event);*/
  }
}
