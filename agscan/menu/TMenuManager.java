package agscan.menu;



import ij.ImagePlus;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.data.element.image.TImage;
import agscan.data.view.graphic.interactor.TRoiInteractor;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.menu.action.SAboutSigenaeAction;
import agscan.menu.action.TAboutBZScanAction;
import agscan.menu.action.TAction;
import agscan.menu.action.TAdjustColorBalanceAction;
import agscan.menu.action.TAdjustContrastBrightnessAction;
import agscan.menu.action.TAdjustWindowLevelAction;
import agscan.menu.action.TCloseAllAction;
import agscan.menu.action.TCloseFileAction;
import agscan.menu.action.TColorParametersAction;
import agscan.menu.action.TColorizeColumnAction;
import agscan.menu.action.TComputeDiameterAction;
import agscan.menu.action.TComputeFitCorrectionAction;
import agscan.menu.action.TComputeOvershiningCorrectionAction;
import agscan.menu.action.TComputeQMAction;
import agscan.menu.action.TComputeQuantifFitConstAction;
import agscan.menu.action.TComputeQuantifFitVarAction;
import agscan.menu.action.TComputeQuantifImageConstAction;
import agscan.menu.action.TComputeQuantifImageVarAction;
import agscan.menu.action.TComputeSelectionAction;
import agscan.menu.action.TComputeSpotQualityDefaultAction;
import agscan.menu.action.TComputeSpotQualityFourProfilesAction;
import agscan.menu.action.TComputeSpotQualityTwoProfilesAction;
import agscan.menu.action.TComputeSpotQualityMultiChannelAction;
import agscan.menu.action.TCopyAction;
import agscan.menu.action.TCropAction;
import agscan.menu.action.TExportAsTextAction;
import agscan.menu.action.TFenetresAction;
import agscan.menu.action.TFlipHorizontalAction;
import agscan.menu.action.TFlipVerticalAction;
import agscan.menu.action.TGlobalAlignmentDefaultAction;
import agscan.menu.action.TGlobalAlignmentEdgesFinderAction;
import agscan.menu.action.TGlobalAlignmentTemplateMatchingAction;
import agscan.menu.action.TGlobalAlignmentSnifferAction;
import agscan.menu.action.TGridModifyStructureAction;
import agscan.menu.action.TGridMovableAction;
import agscan.menu.action.TGridPositioningAction;
import agscan.menu.action.TGridSelectionModeAction;
import agscan.menu.action.THideColumnAction;
import agscan.menu.action.TInitBooleanColumnWith0Action;
import agscan.menu.action.TInitBooleanColumnWith1Action;
import agscan.menu.action.TInitColumnWithDefaultValueAction;
import agscan.menu.action.TInitColumnWithFileAction;
import agscan.menu.action.TInitColumnWithOtherValueAction;
import agscan.menu.action.TInvertLutAction;
import agscan.menu.action.TLevel1SelectionModeAction;
import agscan.menu.action.TLevel2SelectionModeAction;
import agscan.menu.action.TLoadGridAction;
import agscan.menu.action.TLocalAlignmentBlocAction;
import agscan.menu.action.TLocalAlignmentDefaultAction;
import agscan.menu.action.TLocalAlignmentGridAction;
import agscan.menu.action.TLocalAlignmentBlocTemplateAction;
import agscan.menu.action.TMenuLanguageAction;
import agscan.menu.action.TMenuTestRemiAction;
import agscan.menu.action.TModifyColumnAction;
import agscan.menu.action.TMoveItAction;
import agscan.menu.action.TNewBatchAction;
import agscan.menu.action.TNewGridAction;
import agscan.menu.action.TOngletsAction;
import agscan.menu.action.TOpenAlignmentAction;
import agscan.menu.action.TOpenBatchAction;
import agscan.menu.action.TOpenFileAction;
import agscan.menu.action.TOpenGridAction;
import agscan.menu.action.TParametersAction;
import agscan.menu.action.TPasteAction;
import agscan.menu.action.TProcessChooserAction;
import agscan.menu.action.TProprietesAction;
import agscan.menu.action.TQuitAction;
import agscan.menu.action.TRedoAction;
import agscan.menu.action.TRemoveColumnAction;
import agscan.menu.action.TRoiAction;
import agscan.menu.action.TRotateAction_180;
import agscan.menu.action.TRotateAction_m90;
import agscan.menu.action.TRotateAction_p90;
import agscan.menu.action.TRulesAction;
import agscan.menu.action.TSaveAction;
import agscan.menu.action.TSaveAsAction;
import agscan.menu.action.TShorcutParametersAction;
import agscan.menu.action.TShowColumnAction;
import agscan.menu.action.TShowComputedDiametersAction;
import agscan.menu.action.TSortColumnAscendingAction;
import agscan.menu.action.TSortColumnDescendingAction;
import agscan.menu.action.TSpotResizingModeAction;
import agscan.menu.action.TSpotSelectionModeAction;
import agscan.menu.action.TStatusBarAction;
import agscan.menu.action.TUndoAction;
import agscan.menu.action.TUnselectAllSpotsAction;
import agscan.menu.action.TYellowCrossAction;
import agscan.menu.action.TZoomAction;
import agscan.menu.action.TZoomDialogAction;
import agscan.plugins.TPlugin;
import agscan.plugins.TPluginManager;

/**
 * 
 * @author rcathelin Organisation of Menus: The menuBar contains:
 *         menuEdit-menuView-menuImage-menuGrid-menuAlign-analyzeMenu-batchMenu-toolsMenu-menuApropos
 *         renaming of menus:
 *         menuEdit-menuView-menuImage-menuGrid-menuAlign-menuQuantif-menuBatch-menuTools-menuAbout
 * 
 */

public class TMenuManager implements ActionListener, TBZMessageListener {

	// actions du logiciels pour pouvoir donner des numeros d'id supplementaires
	// indices de TAction mis en commentaire car dynamique desormais avec getID
	// cf TAction

	public static final int IS_SIMPLIFIED_SPOTS = 52;

	public static final int IS_SPOT_RESIZING_MODE = 53;

	public static final int IS_GRID_MOVABLE = 55;

	public static final int FIT_NORMAL = 67;// remis pour integration 12/09/05

	public static final int FIT_GAUSS_NEWTON = 68;// remis pour integration
													// 12/09/05

	public static final int GET_POPUP = 1;

	public static final int SET_AVAILABLE = 2;

	public static final int SET_SELECTED = 3;

	public static final int IS_ZOOM = 4;

	public static final int IS_ROI = 5;

	public static final int IS_RULES = 6;

	public static final int FIRE_ACTION = 7;

	public static final int UPDATE = 8;

	public static final int GET_SELECTION_MODE = 9;

	public static final int GET_FIT_ALGORITHM = 10;

	public static final int NEXT_ID = 11; // action traite par TMenuManager :
											// cette action trouve l'id d'action
											// suivante dispo

	public static final int IS_COMPUTED_DIAMETERS = 12;// ajout integration
														// 14/09/05

	// private static TAction[] actions;//modif REMI PASSAGE de tableau a VECTOR
	public static Vector actions;// modif REMI


	private JMenuBar menuBar;

	private JPopupMenu popupMenu;

	private JToolBar toolBar;

	private JCheckBoxMenuItem zoom, roi, rules, statusBar, yellowCross,
			spotResizingMode, gridMovable, fitNormal, fitGaussNewton,
			showComputedDiameters;

	// sortAscending, sortDescending, colorizeColumn, hideColumn vires et
	// showComputedDiameters ajoute - integration 14/09/05
	private JToggleButton zoomButton, ycButton, spotResizingButton,
			gridMovableButton, gridSelectionButton, level1SelectionButton,
			level2SelectionButton, spotSelectionButton, roiButton;

	private JRadioButtonMenuItem onglets, fenetres,/* pleinecran, */gridMode,
			spotMode, level1Mode, level2Mode;

	public static TPluginManager pluginManager;

	public void initActions(FenetrePrincipale fp) {
		JMenuItem mi;
		// actions = new TAction[79];//MENU TEST REMI => adapter le nb d'actions
		// (=incrementer quand on en ajoute une)

		actions.addElement(new TOpenFileAction(new ImageIcon(fp.getClass()
				.getResource("ressources/image.gif")),false)); //$NON-NLS-1$
		actions.addElement(new TOpenGridAction(new ImageIcon(fp.getClass()
				.getResource("ressources/gridBlue.gif")))); //$NON-NLS-1$
		actions.addElement(new TOpenAlignmentAction(new ImageIcon(fp.getClass()
				.getResource("ressources/openFile.gif")))); //$NON-NLS-1$
		actions.addElement(new TCloseFileAction(new ImageIcon(fp.getClass()
				.getResource("ressources/file_cls.gif")))); //$NON-NLS-1$
		actions.addElement(new TZoomAction(new ImageIcon(fp.getClass()
				.getResource("ressources/zoom.gif")))); //$NON-NLS-1$
		actions.addElement(new TAboutBZScanAction(new ImageIcon(fp.getClass()
				.getResource("ressources/help.gif")))); //$NON-NLS-1$
		actions.addElement(new SAboutSigenaeAction()); // added 2005/10/27
		actions.addElement(new TQuitAction());
		actions.addElement(new TCloseAllAction(new ImageIcon(fp.getClass()
				.getResource("ressources/file_cls_all.gif")))); //$NON-NLS-1$
		actions.addElement(new TOngletsAction(null));
		actions.addElement(new TFenetresAction(null));
		// actions.addElement( new TPleinEcranAction(null));//removed since
		// 2005/10/27
		actions.addElement(new TAdjustContrastBrightnessAction(null));
		actions.addElement(new TAdjustWindowLevelAction(null));
		actions.addElement(new TAdjustColorBalanceAction(null));
		actions.addElement(new TProprietesAction(null));
		actions.addElement(new TZoomDialogAction(null));
		actions.addElement(new TRotateAction_p90(Messages
				.getString("TMenuManager.8"), null)); //$NON-NLS-1$
		actions.addElement(new TRotateAction_m90(Messages
				.getString("TMenuManager.9"), null)); //$NON-NLS-1$
		actions.addElement(new TRotateAction_180(Messages
				.getString("TMenuManager.10"), null)); //$NON-NLS-1$
		actions.addElement(new TFlipHorizontalAction(Messages
				.getString("TMenuManager.11"), null)); //$NON-NLS-1$
		actions.addElement(new TFlipVerticalAction(Messages
				.getString("TMenuManager.12"), null)); //$NON-NLS-1$
		actions.addElement(new TCropAction(Messages
				.getString("TMenuManager.13"), null));
		actions.addElement(new TRoiAction("ROI Selection", new ImageIcon(fp
				.getClass().getResource("ressources/selection.gif"))));
		actions.addElement(new TRulesAction(Messages
				.getString("TMenuManager.14"), null)); //$NON-NLS-1$
		actions
				.addElement(new TSaveAction(
						Messages.getString("TMenuManager.15"), new ImageIcon(fp.getClass().getResource("ressources/save.gif")))); //$NON-NLS-1$ //$NON-NLS-2$
		actions
				.addElement(new TSaveAsAction(
						Messages.getString("TMenuManager.17"), new ImageIcon(fp.getClass().getResource("ressources/saveas.gif")))); //$NON-NLS-1$ //$NON-NLS-2$
		actions
				.addElement(new TUndoAction(
						Messages.getString("TMenuManager.19"), new ImageIcon(fp.getClass().getResource("ressources/undo_edit.gif")))); //$NON-NLS-1$ //$NON-NLS-2$
		actions
				.addElement(new TRedoAction(
						Messages.getString("TMenuManager.21"), new ImageIcon(fp.getClass().getResource("ressources/redo_edit.gif")))); //$NON-NLS-1$ //$NON-NLS-2$
		actions.addElement(new TStatusBarAction(null));
		actions.addElement(new TNewGridAction(new ImageIcon(fp.getClass()
				.getResource("ressources/new_grid.gif")))); //$NON-NLS-1$
		actions.addElement(new TGridSelectionModeAction(null));
		actions.addElement(new TSpotSelectionModeAction(null));
		actions.addElement(new TLevel1SelectionModeAction(null));
		actions.addElement(new TLevel2SelectionModeAction(null));
		actions.addElement(new TInitColumnWithDefaultValueAction(null));
		actions.addElement(new TInitColumnWithFileAction(null));
		actions.addElement(new TInitColumnWithOtherValueAction(null));
		actions.addElement(new TInitBooleanColumnWith0Action(null));
		actions.addElement(new TInitBooleanColumnWith1Action(null));
		actions.addElement(new TSortColumnAscendingAction(null));
		actions.addElement(new TSortColumnDescendingAction(null));
		actions.addElement(new TColorizeColumnAction(null));
		actions.addElement(new THideColumnAction(null));
		actions.addElement(new TRemoveColumnAction(null));
		actions.addElement(new TShowColumnAction(null));
		actions.addElement(new TCopyAction(null));
		actions.addElement(new TPasteAction(null));
		actions.addElement(new TGridModifyStructureAction(null));
		// actions.addElement( new TGridAddColumnsAction(null));//removed since
		// 2005/10/27
		// actions.addElement(new TImportColumnsAction(null));//removed since
		// 2005/10/27
		actions.addElement(new TModifyColumnAction(null));
		actions.addElement(new TUnselectAllSpotsAction(null));
		actions.addElement(new TYellowCrossAction(null));
		actions.addElement(new TSpotResizingModeAction());
		actions.addElement(new TGridMovableAction(null));
		actions.addElement(new TLoadGridAction());
		actions.addElement(new TGridPositioningAction(null));

		actions.addElement(new TComputeSpotQualityDefaultAction(new ImageIcon(
				fp.getClass().getResource("ressources/spot_detection.gif")))); //$NON-NLS-1$
		actions.addElement(new TComputeSpotQualityTwoProfilesAction(null));
		actions.addElement(new TComputeSpotQualityFourProfilesAction(null));
		actions.addElement(new TComputeSpotQualityMultiChannelAction(null));
		actions.addElement(new TGlobalAlignmentDefaultAction(new ImageIcon(fp
				.getClass().getResource("ressources/global_alignment.gif")))); //$NON-NLS-1$
		actions.addElement(new TGlobalAlignmentTemplateMatchingAction(null));
		actions.addElement(new TGlobalAlignmentEdgesFinderAction(null));
		actions.addElement(new TGlobalAlignmentSnifferAction(null));
		actions.addElement(new TLocalAlignmentDefaultAction(new ImageIcon(fp
				.getClass().getResource("ressources/local_alignment.gif")))); //$NON-NLS-1$
		actions.addElement(new TLocalAlignmentGridAction(null));
		actions.addElement(new TLocalAlignmentBlocAction(null));
		actions.addElement(new TLocalAlignmentBlocTemplateAction(null));
		actions.addElement(new TComputeSelectionAction());
		actions.addElement(new TComputeQuantifImageConstAction(null));
		actions.addElement(new TComputeQuantifImageVarAction(null));

		actions.addElement(new TComputeQuantifFitConstAction(null));
		actions.addElement(new TComputeQuantifFitVarAction(null));
		actions.addElement(new TComputeDiameterAction(null));
		actions.addElement(new TComputeOvershiningCorrectionAction(null));
		actions.addElement(new TComputeFitCorrectionAction(null));
		actions.addElement(new TComputeQMAction(null));
		actions.addElement(new TMenuTestRemiAction(null));// MENU TEST REMI
		actions.addElement(new TInvertLutAction(Messages
				.getString("TMenuManager.27"), null));// INVERT REMI
														// //$NON-NLS-1$
		actions.addElement(new TMenuLanguageAction(null));
		actions.addElement(new TParametersAction(null));
		actions.addElement(new TColorParametersAction(null));
		actions.addElement(new TShowComputedDiametersAction(null));

		actions.addElement(new TExportAsTextAction());
		actions.addElement(new TNewBatchAction());// icon linked since
													// 2005/10/27
		actions.addElement(new TOpenBatchAction(new ImageIcon(
				FenetrePrincipale.application.getClass().getResource(
						"ressources/openFile.gif"))));
		actions.addElement(new TMoveItAction(null));
		actions.addElement(new TProcessChooserAction(null));// test CK
		actions.addElement(new TShorcutParametersAction(new ImageIcon(fp
				.getClass().getResource("ressources/probmrk_tsk.gif"))));;

		menuBar = new JMenuBar();
		JMenu menuFile = new JMenu(Messages.getString("TMenuManager.28")); //$NON-NLS-1$
		JMenu menuOpen = new JMenu(Messages.getString("TMenuManager.29")); //$NON-NLS-1$

		// menuOpen.add(actions[OUVRIR_IMAGE]);// tous remplace a cause de
		// l'utilisation d'un vecteur TEST REMI
		menuOpen.add((TAction) actions.elementAt(TOpenFileAction.getID()));
		// menuOpen.add(actions[OUVRIR_GRILLE]);
		menuOpen.add((TAction) actions.get(TOpenGridAction.getID()));
		// menuOpen.add(actions[OPEN_ALIGNMENT]);
		menuOpen.add((TAction) actions.get(TOpenAlignmentAction.getID()));

		// menuOpen.add(actions[OPEN_BATCH]);//ajout integration 14/09/05
		menuOpen.add((TAction) actions.get(TOpenBatchAction.getID()));// modif
																		// post-integration
																		// 14/09/05

		menuFile.add(menuOpen);
		JMenu menuNew = new JMenu(Messages.getString("TMenuManager.30")); //$NON-NLS-1$

		// menuNew.add(actions[NEW_GRID]);
		menuNew.add((TAction) actions.get(TNewGridAction.getID()));
		// menuNew.add(actions[NEW_BATCH]);//ajout integration 14/09/05
		menuNew.add((TAction) actions.get(TNewBatchAction.getID()));// modif
																	// post-integration
																	// 14/09/05
		// mi = new JMenuItem(Messages.getString("TMenuManager.31"));
		// mi.setEnabled(false); menuNew.add(mi); //vire e l'integration
		// 14/09/05


		menuFile.add(menuNew);
		menuFile.addSeparator();

		// menuFile.add(actions[SAVE]);
		menuFile.add((TAction) actions.get(TSaveAction.getID()));
		// menuFile.add(actions[SAVE_AS]);
		menuFile.add((TAction) actions.get(TSaveAsAction.getID()));
		menuFile.addSeparator();

		// menuFile.add(actions[PROPRIETES]);
		// 2005/10/27 - remove of the 2 next lines => properties menu is already
		// in "Display" menu
		// menuFile.add((TAction)actions.get(TProprietesAction.getID()));//deplace
		// dans le menu affichage
		// menuFile.addSeparator();
		// menuFile.add(actions[FERMER]);
		menuFile.add((TAction) actions.get(TCloseFileAction.getID()));
		// menuFile.add(actions[FERMER_TOUT]);
		menuFile.add((TAction) actions.get(TCloseAllAction.getID()));

		menuFile.addSeparator();
		// menuFile.add(actions[QUITTER]);
		menuFile.add((TAction) actions.get(TQuitAction.getID()));
		menuBar.add(menuFile);

		JMenu menuEdit = new JMenu(Messages.getString("TMenuManager.35")); //$NON-NLS-1$
		// menuEdit.add(actions[UNDO]);
		menuEdit.add((TAction) actions.get(TUndoAction.getID()));
		// menuEdit.add(actions[REDO]);
		menuEdit.add((TAction) actions.get(TRedoAction.getID()));
		menuEdit.addSeparator();
		JMenu selectionModeMenu = new JMenu(Messages
				.getString("TMenuManager.36")); //$NON-NLS-1$
		ButtonGroup bg = new ButtonGroup();
		// gridMode = new JRadioButtonMenuItem(actions[GRID_SELECTION_MODE]);
		gridMode = new JRadioButtonMenuItem((TAction) actions
				.get(TGridSelectionModeAction.getID()));
		gridMode.setSelected(true);
		bg.add(gridMode);
		// actions[GRID_SELECTION_MODE].putValue("menuitem", gridMode);
		((TAction) actions.get(TGridSelectionModeAction.getID())).putValue(
				"menuitem", gridMode); //$NON-NLS-1$
		selectionModeMenu.add(gridMode);

		// spotMode = new JRadioButtonMenuItem(actions[SPOT_SELECTION_MODE]);
		spotMode = new JRadioButtonMenuItem((TAction) actions
				.get(TSpotSelectionModeAction.getID()));
		spotMode.setSelected(false);
		bg.add(spotMode);
		// actions[SPOT_SELECTION_MODE].putValue("menuitem", spotMode);
		((TAction) actions.get(TSpotSelectionModeAction.getID())).putValue(
				"menuitem", spotMode); //$NON-NLS-1$

		selectionModeMenu.add(spotMode);
		// level1Mode = new
		// JRadioButtonMenuItem(actions[LEVEL1_SELECTION_MODE]);
		level1Mode = new JRadioButtonMenuItem((TAction) actions
				.get(TLevel1SelectionModeAction.getID()));

		level1Mode.setSelected(false);
		bg.add(level1Mode);
		// actions[LEVEL1_SELECTION_MODE].putValue("menuitem", level1Mode);
		((TAction) actions.get(TLevel1SelectionModeAction.getID())).putValue(
				"menuitem", level1Mode); //$NON-NLS-1$
		selectionModeMenu.add(level1Mode);
		// level2Mode = new
		// JRadioButtonMenuItem(actions[LEVEL2_SELECTION_MODE]);
		// removed since 2005/10/28
		/*
		 * level2Mode = new
		 * JRadioButtonMenuItem((TAction)actions.get(TLevel2SelectionModeAction.getID()));
		 * level2Mode.setSelected(false); bg.add(level2Mode);
		 * //actions[LEVEL2_SELECTION_MODE].putValue("menuitem", level2Mode);
		 * ((TAction)actions.get(TLevel2SelectionModeAction.getID())).putValue("menuitem",
		 * level2Mode); //$NON-NLS-1$ selectionModeMenu.add(level2Mode);
		 */
		menuEdit.add(selectionModeMenu);
		// menuEdit.add(actions[UNSELECT_ALL]);
		menuEdit.add((TAction) actions.get(TUnselectAllSpotsAction.getID()));
		menuEdit.addSeparator();
		menuEdit.add((TAction) actions.get(TMenuLanguageAction.getID()));// moved
																			// here
																			// since
																			// 2005/10/27
		menuEdit.add((TAction) actions.get(TParametersAction.getID()));// 19/08/08
																		// -
																		// moved
																		// here
																		// since
																		// 2005/10/28
		menuEdit.add((TAction) actions.get(TColorParametersAction.getID()));// 28/03/07
		menuBar.add(menuEdit);// MENU EDIT ADDED

		JMenu menuView = new JMenu(Messages.getString("TMenuManager.41")); //$NON-NLS-1$
		bg = new ButtonGroup();
		// onglets = new JRadioButtonMenuItem(actions[ONGLETS]);
		onglets = new JRadioButtonMenuItem((TAction) actions.get(TOngletsAction
				.getID()));
		onglets.setSelected(true);
		bg.add(onglets);
		// actions[ONGLETS].putValue("menuitem", onglets);
		((TAction) actions.get(TOngletsAction.getID())).putValue(
				"menuitem", onglets); //$NON-NLS-1$
		menuView.add(onglets);
		// fenetres = new JRadioButtonMenuItem(actions[FENETRES]);
		fenetres = new JRadioButtonMenuItem((TAction) actions
				.get(TFenetresAction.getID()));
		fenetres.setSelected(false);
		bg.add(fenetres);
		// actions[FENETRES].putValue("menuitem", fenetres);
		((TAction) actions.get(TFenetresAction.getID())).putValue(
				"menuitem", fenetres); //$NON-NLS-1$
		menuView.add(fenetres);
		// pleinecran = new JRadioButtonMenuItem(actions[PLEIN_ECRAN]);
		// pleinecran = new
		// JRadioButtonMenuItem((TAction)actions.get(TPleinEcranAction.getID()));//removed
		// since 2005/10/27
		// pleinecran.setSelected(false);//removed since 2005/10/27
		// bg.add(pleinecran);//removed since 2005/10/27
		// actions[PLEIN_ECRAN].putValue("menuitem", pleinecran);
		// ((TAction)actions.get(TPleinEcranAction.getID())).putValue("menuitem",
		// pleinecran); //removed since 2005/10/27
		// actions[PLEIN_ECRAN].setEnabled(false);
		// ((TAction)actions.get(TPleinEcranAction.getID())).putValue("menuitem",
		// pleinecran);//removed since 2005/10/27
		// menuView.add(pleinecran);//removed since 2005/10/27
		menuView.addSeparator();
		// zoom = new JCheckBoxMenuItem(actions[ZOOM]);
		zoom = new JCheckBoxMenuItem((TAction) actions.get(TZoomAction.getID()));
		// actions[ZOOM].putValue("menuitem", zoom);
		((TAction) actions.get(TZoomAction.getID())).putValue("menuitem", zoom); //$NON-NLS-1$
		menuView.add(zoom);
		// JMenuItem zoomDialog = new JMenuItem(actions[ZOOM_DIALOG]);
		JMenuItem zoomDialog = new JMenuItem((TAction) actions
				.get(TZoomDialogAction.getID()));
		// actions[ZOOM_DIALOG].putValue("menuitem", zoomDialog);
		((TAction) actions.get(TZoomDialogAction.getID())).putValue(
				"menuitem", zoomDialog); //$NON-NLS-1$
		menuView.add(zoomDialog);
		menuView.addSeparator();
		// rules = new JCheckBoxMenuItem(actions[RULES]);
		rules = new JCheckBoxMenuItem((TAction) actions.get(TRulesAction
				.getID()));
		// actions[RULES].putValue("menuitem", rules);
		((TAction) actions.get(TRulesAction.getID())).putValue(
				"menuitem", rules); //$NON-NLS-1$
		menuView.add(rules);
		// statusBar = new JCheckBoxMenuItem(actions[STATUS_BAR]);
		statusBar = new JCheckBoxMenuItem((TAction) actions
				.get(TStatusBarAction.getID()));
		// actions[STATUS_BAR].putValue("menuitem", statusBar);
		((TAction) actions.get(TStatusBarAction.getID())).putValue(
				"menuitem", statusBar); //$NON-NLS-1$
		menuView.add(statusBar);
		menuView.addSeparator();

		/* Ajouter ici "Configuration" */
		// mi = new JMenuItem(Messages.getString("TMenuManager.50"));
		// mi.setEnabled(false); menuView.add(mi);
		menuView.add((TAction) actions.get(TProprietesAction.getID()));
		// menuView.addSeparator();

		menuBar.add(menuView);

		JMenu menuImage = new JMenu(Messages.getString("TMenuManager.51")); //$NON-NLS-1$

		/* Ajouter ici "Inverser" */
		// mi = new JMenuItem("Inverser"); mi.setEnabled(false);
		// menuImage.add(mi);
		menuImage.add((TAction) actions.get(TInvertLutAction.getID()));// REMI
																		// 22/07/05
		/** *********************** */

		// menuImage.add(actions[CONTRASTE_ET_LUMINOSITE]);

		// menuImage.add((TAction)actions.get(TContrasteEtLuminositeAction.getID()));//remi
		// 18/08/05
		// test
		JMenu menuContrast = new JMenu(Messages.getString("TMenuManager.0")); //$NON-NLS-1$
		menuContrast.add((TAction) actions.get(TAdjustContrastBrightnessAction
				.getID()));
		menuContrast.add((TAction) actions
				.get(TAdjustWindowLevelAction.getID()));
		menuContrast.add((TAction) actions.get(TAdjustColorBalanceAction
				.getID()));
		menuImage.add(menuContrast);

		// fin test

		JMenu menuRotate = new JMenu(Messages.getString("TMenuManager.52")); //$NON-NLS-1$
		// menuRotate.add(actions[ROTATE_PLUS_90]);
		menuRotate.add((TAction) actions.get(TRotateAction_p90.getID()));
		// menuRotate.add(actions[ROTATE_MOINS_90]);
		menuRotate.add((TAction) actions.get(TRotateAction_m90.getID()));
		// menuRotate.add(actions[ROTATE_180]);
		menuRotate.add((TAction) actions.get(TRotateAction_180.getID()));
		menuImage.add(menuRotate);

		JMenu menuFlip = new JMenu(Messages.getString("TMenuManager.53")); //$NON-NLS-1$
		// menuFlip.add(actions[FLIP_HORIZONTAL]);
		menuFlip.add((TAction) actions.get(TFlipHorizontalAction.getID()));
		// menuFlip.add(actions[FLIP_VERTICAL]);
		menuFlip.add((TAction) actions.get(TFlipVerticalAction.getID()));
		menuImage.add(menuFlip);
		// crop = new JCheckBoxMenuItem(actions[CROP]);
		roi = new JCheckBoxMenuItem((TAction) actions.get(TRoiAction.getID()));
		// actions[CROP].putValue("menuitem", crop);
		((TAction) actions.get(TRoiAction.getID())).putValue("menuitem", roi); //$NON-NLS-1$
		menuImage.add(roi);
		menuImage.add((TAction) actions.get(TCropAction.getID()));
		menuBar.add(menuImage);

		JMenu menuGrid = new JMenu(Messages.getString("TMenuManager.55")); //$NON-NLS-1$
		menuGrid.add((TAction) actions.get(TNewGridAction.getID()));// add here
																	// (2nd
																	// instance)
																	// since
																	// 2005/10/27
		menuGrid.add((TAction) actions.get(TLoadGridAction.getID()));// move
																		// here
																		// since
																		// 2005/10/27
		// menuGrid.add((TAction)actions.get(TOpenGridAction.getID()));//add
		// here (2nd instance) since 2005/10/28
		// menuGrid.add(actions[MODIFY_GRID_STRUCTURE]);
		menuGrid.add((TAction) actions.get(TGridModifyStructureAction.getID()));
		// menuGrid.add(actions[ADD_COLUMNS]);
		// menuGrid.add((TAction)actions.get(TGridAddColumnsAction.getID()));//menu
		// removed since 2005/10/27
		// menuGrid.add(actions[IMPORT_COLUMNS]);
		// menuGrid.add((TAction)actions.get(TImportColumnsAction.getID()));//menu
		// removed since 2005/10/27
		menuGrid.addSeparator();
		// yellowCross = new JCheckBoxMenuItem(actions[YELLOW_CROSS]);
		yellowCross = new JCheckBoxMenuItem((TAction) actions
				.get(TYellowCrossAction.getID()));
		// actions[YELLOW_CROSS].putValue("menuitem", yellowCross);
		((TAction) actions.get(TYellowCrossAction.getID())).putValue(
				"menuitem", yellowCross); //$NON-NLS-1$
		menuGrid.add(yellowCross);
		// spotResizingMode = new
		// JCheckBoxMenuItem(actions[SPOT_RESIZING_MODE]);
		spotResizingMode = new JCheckBoxMenuItem((TAction) actions
				.get(TSpotResizingModeAction.getID()));
		// actions[SPOT_RESIZING_MODE].putValue("menuitem", spotResizingMode);
		((TAction) actions.get(TSpotResizingModeAction.getID())).putValue(
				"menuitem", spotResizingMode); //$NON-NLS-1$
		menuGrid.add(spotResizingMode);
		// gridMovable = new JCheckBoxMenuItem(actions[GRID_MOVABLE]);
		gridMovable = new JCheckBoxMenuItem((TAction) actions
				.get(TGridMovableAction.getID()));
		// actions[GRID_MOVABLE].putValue("menuitem", gridMovable);
		((TAction) actions.get(TGridMovableAction.getID())).putValue(
				"menuitem", gridMovable); //$NON-NLS-1$
		menuGrid.add(gridMovable);
		menuBar.add(menuGrid);

		JMenu menuAlign = new JMenu(Messages.getString("TMenuManager.59")); //$NON-NLS-1$
		// menuAlign.add(actions[LOAD_GRID]);

		// menuAlign.add(actions[GRID_POSITIONING]);
		menuAlign.add((TAction) actions.get(TGridPositioningAction.getID()));
		menuAlign.addSeparator();

		JMenu spotQualityAlgorithmMenu = new JMenu(Messages
				.getString("TMenuManager.60")); //$NON-NLS-1$
		// spotQualityAlgorithmMenu.add(actions[SPOT_QUALITY_DEFAULT]);
		spotQualityAlgorithmMenu.add((TAction) actions
				.get(TComputeSpotQualityDefaultAction.getID()));
		// spotQualityAlgorithmMenu.add(actions[SPOT_QUALITY_TWO_PROFILES]);
		spotQualityAlgorithmMenu.add((TAction) actions
				.get(TComputeSpotQualityTwoProfilesAction.getID()));
		spotQualityAlgorithmMenu.add((TAction) actions
				.get(TComputeSpotQualityMultiChannelAction.getID()));
		// mi = new JMenuItem(actions[SPOT_QUALITY_FOUR_PROFILES]);
		mi = new JMenuItem((TAction) actions
				.get(TComputeSpotQualityFourProfilesAction.getID()));

		mi.setFont(new Font("tahoma", Font.BOLD, 11)); //$NON-NLS-1$
		spotQualityAlgorithmMenu.add(mi);
		menuAlign.add(spotQualityAlgorithmMenu);

		JMenu globalAlignmentMenu = new JMenu(Messages
				.getString("TMenuManager.62")); //$NON-NLS-1$
		// globalAlignmentMenu.add(actions[GLOBAL_ALIGNMENT_DEFAULT]);
		globalAlignmentMenu.add((TAction) actions
				.get(TGlobalAlignmentDefaultAction.getID()));
		// globalAlignmentMenu.add(actions[GLOBAL_ALIGNMENT_EDGES_FINDER]);
		globalAlignmentMenu.add((TAction) actions
				.get(TGlobalAlignmentEdgesFinderAction.getID()));
		globalAlignmentMenu.add((TAction) actions
				.get(TGlobalAlignmentTemplateMatchingAction.getID()));
		// mi = new JMenuItem(actions[GLOBAL_ALIGNMENT_SNIFFER]);
		mi = new JMenuItem((TAction) actions.get(TGlobalAlignmentSnifferAction
				.getID()));

		mi.setFont(new Font("tahoma", Font.BOLD, 11)); //$NON-NLS-1$
		globalAlignmentMenu.add(mi);
		menuAlign.add(globalAlignmentMenu);

		// 2006/03/09
		mi = new JMenuItem((TAction) actions.get(TMoveItAction.getID()));// CK
		// menuAlign.add(mi);//CK
		// 2006/03/09

		/*

		 * //TEST REMI try{
		 *  // PluginLoader pluginLoader = new
		 * PluginLoader("./plugins/image/plugins/globalAlign"); PluginLoader
		 * pluginLoader = new PluginLoader("./plugins/");
		 *  // Chargement de tous les plugins places dans les URLs donnes
		 * ci-dessus. pluginLoader.loadPlugins(); Plugin[] plugs =
		 * pluginLoader.getPluginInstances(); //System.out.println("taille du
		 * tableau recupere = " +plugs.length);
		 * 
		 *  // L'actionListener qui va ecouter les entrees du menu des plugins
		 * 
		 * 
		 * //ActionListener listener = new ActionListener() { //public void
		 * actionPerformed(ActionEvent e) { // Met l'instance de plugin associee
		 * a l'entree du menu comme // dessinateur courant. //if
		 * (e.getSource()==plugs[i].getName()) //plug =
		 * (PluginAlign)(e.getSource()); //System.out.println("plugin courant = " +
		 * plug.getName() // + " obtenu depuis " + e.getSource()); // } //};
		 * 
		 * JMenuItem menuItem;
		 * 
		 * //JMenuItem jmi; // ActionListener listener;// = new
		 * ActionListener(); for (int i=0; i< plugs.length;i++){ menuItem = new
		 * JMenuItem(plugs[i].getName()); //globalAlignmentMenu.add(new
		 * JMenuItem(plugs[i].getName())); // mettre un listener qui soit aussi
		 * dynamique...et associe au menu (TAction?)
		 * menuItem.addActionListener(this); globalAlignmentMenu.add(menuItem); }
		 *  } catch (MalformedURLException malex){ System.out.println("bouh la
		 * malformed URL"+ malex.getMessage()); }
		 * 
		 * //FIN TEST REMI
		 */

		JMenu localAlignmentMenu = new JMenu(Messages
				.getString("TMenuManager.64")); //$NON-NLS-1$
		// localAlignmentMenu.add(actions[LOCAL_ALIGNMENT_DEFAULT]);
		localAlignmentMenu.add((TAction) actions
				.get(TLocalAlignmentDefaultAction.getID()));
		// localAlignmentMenu.add(actions[LOCAL_ALIGNMENT_GRID]);
		localAlignmentMenu.add((TAction) actions.get(TLocalAlignmentGridAction
				.getID()));
		localAlignmentMenu.add((TAction) actions
				.get(TLocalAlignmentBlocTemplateAction.getID()));
		// mi = new JMenuItem(actions[LOCAL_ALIGNMENT_BLOC]);
		mi = new JMenuItem((TAction) actions.get(TLocalAlignmentBlocAction
				.getID()));

		mi.setFont(new Font("tahoma", Font.BOLD, 11)); //$NON-NLS-1$
		localAlignmentMenu.add(mi);
		menuAlign.add(localAlignmentMenu);

		// 2005/10/27 - fitMenu removed because only FitGaussNewton available"
		/*
		 * JMenu fitMenu = new JMenu(Messages.getString("TMenuManager.66"));
		 * //$NON-NLS-1$
		 *  // Ajouter ici "Algorithme par defaut", "Fit 1", "Fit 2" fitNormal =
		 * new JCheckBoxMenuItem("Normal"); //$NON-NLS-1$
		 * fitNormal.setEnabled(false); //actions[ZOOM].putValue("menuitem",
		 * fitNormal); fitMenu.add(fitNormal); fitGaussNewton = new
		 * JCheckBoxMenuItem(Messages.getString("TMenuManager.68"));
		 * //$NON-NLS-1$ fitGaussNewton.setSelected(true);
		 * //actions[ZOOM].putValue("menuitem", fitGaussNewton);
		 * fitMenu.add(fitGaussNewton); bg = new ButtonGroup();
		 * bg.add(fitNormal); bg.add(fitGaussNewton);
		 * 
		 * menuAlign.add(fitMenu);
		 */

		// menuAlign.addSeparator();
		/* Ajouter ici "Reinitialiser la position des spots" */
		// mi = new JMenuItem(Messages.getString("TMenuManager.70"));
		// mi.setEnabled(false); menuAlign.add(mi); removed because non
		// implemented (2005/10/27)
		/** ************************************************** */

		menuBar.add(menuAlign);

		/*
		 * Ajouter ici "Quantifier", "Afficher l'image fit", "Afficher l'image
		 * difference", "Afficher les diametres calcules"
		 */

		JMenu menuQuantif = new JMenu(Messages.getString("TMenuManager.71")); //$NON-NLS-1$
		JMenu computeMenu = new JMenu(Messages.getString("TMenuManager.72")); //$NON-NLS-1$

		// mi = new JMenuItem(actions[COMPUTE_SELECTION]);//avant
		mi = new JMenuItem((TAction) actions.get(TComputeSelectionAction
				.getID()));// apres
		computeMenu.add(mi);


		// ajout et transformations post-integration du 14/09/05 des actions
		// liees aux quantifs

		computeMenu.addSeparator();
		// mi = new JMenuItem(actions[COMPUTE_QUANTIF_IMAGE_CONST]);
		mi = new JMenuItem((TAction) actions
				.get(TComputeQuantifImageConstAction.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_QUANTIF_IMAGE_VAR]);
		mi = new JMenuItem((TAction) actions.get(TComputeQuantifImageVarAction
				.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_QUANTIF_FIT_CONST]);
		mi = new JMenuItem((TAction) actions.get(TComputeQuantifFitConstAction
				.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_QUANTIF_FIT_VAR]);
		mi = new JMenuItem((TAction) actions.get(TComputeQuantifFitVarAction
				.getID()));// apres
		computeMenu.add(mi);
		computeMenu.addSeparator();
		// mi = new JMenuItem(actions[COMPUTE_DIAMETER]);
		mi = new JMenuItem((TAction) actions
				.get(TComputeDiameterAction.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_OVERSHINING_CORRECTION]);
		mi = new JMenuItem((TAction) actions
				.get(TComputeOvershiningCorrectionAction.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_FIT_CORRECTION]);
		mi = new JMenuItem((TAction) actions.get(TComputeFitCorrectionAction
				.getID()));// apres
		computeMenu.add(mi);
		// mi = new JMenuItem(actions[COMPUTE_QM]);
		mi = new JMenuItem((TAction) actions.get(TComputeQMAction.getID()));// apres
		computeMenu.add(mi);

		menuQuantif.add(computeMenu);

		// Export menu moved here since 2005/10/27
		JMenu menuExport = new JMenu(Messages.getString("TMenuManager.32")); //$NON-NLS-1$

		// mi = new JMenuItem(Messages.getString("TMenuManager.33"));
		// mi.setEnabled(false); menuExport.add(mi);//vire e l'integration
		// 14/09/05
		// menuExport.add(actions[EXPORT_AS_TEXT]);//ajout integration 14/09/05
		menuExport.add((TAction) actions.get(TExportAsTextAction.getID()));// modif
																			// post-integration
																			// 14/09/05


		/* Ajouter ici "Fichier MAGE-ML" */

		// 2005/10/26 - menu supprime tant que rien d'implemente (avant grise,
		// maintenant supprime)
		// mi = new JMenuItem(Messages.getString("TMenuManager.34"));
		// mi.setEnabled(false); menuExport.add(mi);

		menuQuantif.add(menuExport);
		menuQuantif.addSeparator();

		// mi = new JMenuItem(actions[SHOW_FIT_IMAGE]);
		// mi = new
		// JMenuItem((TAction)actions.get(TShowFitImageAction.getID()));//apres//2005/10/27
		// - menu removed from application
		// analyzeMenu.add(mi);//2005/10/27 - menu removed from application
		// mi = new JMenuItem(actions[SHOW_DIFF_IMAGE]);
		// mi = new
		// JMenuItem((TAction)actions.get(TShowDiffImageAction.getID()));//apres
		// analyzeMenu.add(mi);//2005/10/27 - menu removed from application
		// showComputedDiameters = new
		// JCheckBoxMenuItem(actions[SHOW_COMPUTED_DIAMETERS]);
		showComputedDiameters = new JCheckBoxMenuItem((TAction) actions
				.get(TShowComputedDiametersAction.getID()));// APRES
		// actions[SHOW_COMPUTED_DIAMETERS].putValue("menuitem",
		// showComputedDiameters);
		((TAction) actions.get(TShowComputedDiametersAction.getID())).putValue(
				"menuitem", showComputedDiameters);// APRES
		showComputedDiameters.setSelected(false);
		menuQuantif.add(showComputedDiameters);

		// en dessous 3 lignes retirees
		/***********************************************************************
		 * mi = new JMenuItem(Messages.getString("TMenuManager.73"));
		 * mi.setEnabled(false); analyzeMenu.add(mi); //$NON-NLS-1$ mi = new
		 * JMenuItem(Messages.getString("TMenuManager.74"));
		 * mi.setEnabled(false); analyzeMenu.add(mi); //$NON-NLS-1$ mi = new
		 * JMenuItem(Messages.getString("TMenuManager.75"));
		 * mi.setEnabled(false); analyzeMenu.add(mi); //$NON-NLS-1$
		 */
		/*********************************************************************** */


		menuBar.add(menuQuantif);

		// new menu : batch menu (since 2005/10/26)
		JMenu menuBatch = new JMenu(Messages.getString("TMenuManager.56")); //$NON-NLS-1$
		mi = new JMenuItem((TAction) actions.get(TNewBatchAction.getID()));
		menuBatch.add(mi);
		menuBar.add(menuBatch);

		JMenu menuTools = new JMenu(Messages.getString("TMenuManager.76")); //$NON-NLS-1$


		/* Ajouter ici "Vue 3D", "Plot", "Matrice de plots" */
		// 2005/11/29 non-active menus "Vue 3D", "Plot", "Matrice de plots"
		// removed...
		// mi = new JMenuItem(Messages.getString("TMenuManager.77"));
		// mi.setEnabled(false); menuTools.add(mi); //$NON-NLS-1$
		// mi = new JMenuItem(Messages.getString("TMenuManager.78"));
		// mi.setEnabled(false); menuTools.add(mi); //$NON-NLS-1$
		// mi = new JMenuItem(Messages.getString("TMenuManager.79"));
		// mi.setEnabled(false); menuTools.add(mi); //$NON-NLS-1$
		// mi = new
		// JMenuItem(actions[IDENTIFIANT_DE_L_ACTION_DU_MENU]);menuTools.add(mi);//TEST
		// REMI MENU
		// mi = new
		// JMenuItem((TAction)actions.get(TMenuTestRemiAction.getID()));menuTools.add(mi);//menu
		// test retire, commente.
		/** ************************************************* */


		menuBar.add(menuTools);
		JMenu menuAbout = new JMenu(Messages.getString("TMenuManager.80")); //$NON-NLS-1$

		/* Ajouter ici "TAGC" */
		// mi = new JMenuItem(Messages.getString("TMenuManager.81"));
		// mi.setEnabled(false); menuApropos.add(mi); //$NON-NLS-1$
		menuAbout.add((TAction) actions.get(SAboutSigenaeAction.getID()));

		// menuAbout.add(actions[APROPOS_BZSCAN]);
		menuAbout.add((TAction) actions.get(TAboutBZScanAction.getID()));
		menuBar.add(menuAbout);


		/* Definition de la toolbar */


		toolBar = new JToolBar();

		// toolbar remplacee avec tiptooltext le 26/08/05

		toolBar.add((TAction) actions.get(TOpenFileAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.4")); //$NON-NLS-1$
		toolBar.add((TAction) actions.get(TLoadGridAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.115"));// externalized
																		// since
																		// 2006/02/20
		toolBar.add((TAction) actions.get(TNewBatchAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.113"));// added
																		// 2005/10/27

		toolBar.addSeparator();
		toolBar.addSeparator();
		// toolBar.add(actions[SAVE]).setToolTipText("Sauvegarder");
		toolBar.add((TAction) actions.get(TSaveAction.getID())).setToolTipText(
				Messages.getString("TMenuManager.1")); //$NON-NLS-1$
		// toolBar.add((TAction)actions.get(TSaveAction.getID()));
		toolBar.add((TAction) actions.get(TSaveAsAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.2")); //$NON-NLS-1$
		// toolBar.add((TAction)actions.get(TSaveAsAction.getID()));
		toolBar.add((TAction) actions.get(TCloseFileAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.3")); //$NON-NLS-1$
		// toolBar.add((TAction)actions.get(TCloseFileAction.getID()));
		toolBar.addSeparator();
		toolBar.addSeparator();

		roiButton = new JToggleButton((TAction) actions.get(TRoiAction.getID()));
		roiButton.setText("");
		roiButton.setToolTipText("ROI Selection");
		roiButton.setMaximumSize(new Dimension(30, 30));
		roiButton.setMinimumSize(new Dimension(30, 30));
		toolBar.add(roiButton);

		// zoomButton = new JToggleButton(actions[ZOOM]);
		zoomButton = new JToggleButton((TAction) actions.get(TZoomAction
				.getID()));
		zoomButton.setText(Messages.getString("TMenuManager.82")); // menu vide
																	// pour le
																	// bouton
		zoomButton.setToolTipText(Messages.getString("TMenuManager.83")); //$NON-NLS-1$
		zoomButton.setMaximumSize(new Dimension(30, 30));
		zoomButton.setMinimumSize(new Dimension(30, 30));
		toolBar.add(zoomButton);
		// ycButton = new JToggleButton(actions[YELLOW_CROSS]);
		/*
		 * //button removed 2005/10/28 ycButton = new
		 * JToggleButton((TAction)actions.get(TYellowCrossAction.getID()));
		 * ycButton.setText(Messages.getString("TMenuManager.84"));
		 * //$NON-NLS-1$
		 * ycButton.setToolTipText(Messages.getString("TMenuManager.85"));
		 * //$NON-NLS-1$ ycButton.setIcon(new
		 * ImageIcon(fp.getClass().getResource("ressources/simp_spot.gif")));
		 * //$NON-NLS-1$ toolBar.add(ycButton);
		 */
		// button removed 2005/10/28
		// spotResizingButton = new JToggleButton(actions[SPOT_RESIZING_MODE]);
		/*
		 * spotResizingButton = new
		 * JToggleButton((TAction)actions.get(TSpotResizingModeAction.getID()));
		 * spotResizingButton.setText(Messages.getString("TMenuManager.87"));
		 * //$NON-NLS-1$ spotResizingButton.setIcon(new
		 * ImageIcon(fp.getClass().getResource("ressources/spot_resize.gif")));
		 * //$NON-NLS-1$
		 * spotResizingButton.setToolTipText(Messages.getString("TMenuManager.89"));
		 * //$NON-NLS-1$ toolBar.add(spotResizingButton);
		 */
		// gridMovableButton = new JToggleButton(actions[GRID_MOVABLE]);
		gridMovableButton = new JToggleButton((TAction) actions
				.get(TGridMovableAction.getID()));
		gridMovableButton.setText(Messages.getString("TMenuManager.90")); //$NON-NLS-1$
		gridMovableButton.setIcon(new ImageIcon(fp.getClass().getResource(
				"ressources/grid_movable.gif"))); //$NON-NLS-1$
		gridMovableButton.setToolTipText(Messages.getString("TMenuManager.92")); //$NON-NLS-1$
		toolBar.add(gridMovableButton);
		toolBar.addSeparator();
		toolBar.addSeparator();
		// gridSelectionButton = new
		// JToggleButton(actions[GRID_SELECTION_MODE]);
		gridSelectionButton = new JToggleButton((TAction) actions
				.get(TGridSelectionModeAction.getID()));
		// level2SelectionButton = new
		// JToggleButton(actions[LEVEL2_SELECTION_MODE]);
		// level2SelectionButton = new
		// JToggleButton((TAction)actions.get(TLevel2SelectionModeAction.getID()));
		// level1SelectionButton = new
		// JToggleButton(actions[LEVEL1_SELECTION_MODE]);
		level1SelectionButton = new JToggleButton((TAction) actions
				.get(TLevel1SelectionModeAction.getID()));
		// spotSelectionButton = new
		// JToggleButton(actions[SPOT_SELECTION_MODE]);
		spotSelectionButton = new JToggleButton((TAction) actions
				.get(TSpotSelectionModeAction.getID()));
		gridSelectionButton.setIcon(new ImageIcon(fp.getClass().getResource(
				"ressources/grid_select.gif"))); //$NON-NLS-1$
		gridSelectionButton.setText(Messages.getString("TMenuManager.94")); //$NON-NLS-1$
		gridSelectionButton.setToolTipText(Messages
				.getString("TMenuManager.95")); //$NON-NLS-1$
		gridSelectionButton.setSelected(true);
		toolBar.add(gridSelectionButton);
		/*
		 * removed since 2005/10/28 level2SelectionButton.setIcon(new
		 * ImageIcon(fp.getClass().getResource("ressources/level2_select.gif")));
		 * //$NON-NLS-1$
		 * level2SelectionButton.setText(Messages.getString("TMenuManager.97"));
		 * //$NON-NLS-1$
		 * level2SelectionButton.setToolTipText(Messages.getString("TMenuManager.98"));
		 * //$NON-NLS-1$ level2SelectionButton.setSelected(false);
		 * toolBar.add(level2SelectionButton);
		 */
		level1SelectionButton.setIcon(new ImageIcon(fp.getClass().getResource(
				"ressources/level1_select.gif"))); //$NON-NLS-1$
		level1SelectionButton.setText(Messages.getString("TMenuManager.100")); //$NON-NLS-1$
		level1SelectionButton.setToolTipText(Messages
				.getString("TMenuManager.101")); //$NON-NLS-1$
		level1SelectionButton.setSelected(false);
		toolBar.add(level1SelectionButton);
		spotSelectionButton.setIcon(new ImageIcon(fp.getClass().getResource(
				"ressources/spot_select.gif"))); //$NON-NLS-1$
		spotSelectionButton.setText(Messages.getString("TMenuManager.103")); //$NON-NLS-1$
		spotSelectionButton.setToolTipText(Messages
				.getString("TMenuManager.104")); //$NON-NLS-1$
		spotSelectionButton.setSelected(false);
		toolBar.add(spotSelectionButton);
		toolBar.addSeparator();
		toolBar.addSeparator();
		
		toolBar.add(
				(TAction) actions.get(TShorcutParametersAction.getID()))
				.setToolTipText(Messages.getString("Configurer les raccourcis"));
		toolBar.addSeparator();
		
		
		toolBar.add(
				(TAction) actions.get(TGlobalAlignmentDefaultAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.106")); //$NON-NLS-1$
		// toolBar.add(actions[LOCAL_ALIGNMENT_DEFAULT]).setToolTipText("Alignement
		// local");
		toolBar
				.add(
						(TAction) actions.get(TLocalAlignmentDefaultAction
								.getID())).setToolTipText(
						Messages.getString("TMenuManager.107")); //$NON-NLS-1$
		toolBar.addSeparator();
		toolBar.addSeparator();

		// toolBar.add(actions[SPOT_QUALITY_DEFAULT]).setToolTipText("Detection
		// des spots");
		toolBar
				.add(
						(TAction) actions.get(TComputeSpotQualityDefaultAction
								.getID())).setToolTipText(
						Messages.getString("TMenuManager.105")); //$NON-NLS-1$
		toolBar.add((TAction) actions.get(TComputeSelectionAction.getID()))
				.setToolTipText(Messages.getString("TMenuManager.114"));// externalized
																		// since
																		// 2006/02/20

		toolBar.addSeparator();
		toolBar.addSeparator();
		// toolBar.add(toolBar.add(actions[APROPOS_BZSCAN]).setToolTipText("Aide");
		// toolBar.add((TAction)actions.get(TAboutBZScanAction.getID())).setToolTipText(Messages.getString("TMenuManager.108"));
		// //TODO remettre avec une vraie aide...
		// toolBar.add(toolBar.add(actions[UNDO]).setToolTipText("Undo");
		// toolBar.add((TAction)actions.get(TUndoAction.getID())).setToolTipText(Messages.getString("TMenuManager.109"));
		// //$NON-NLS-1$ //TODO remettre ou pas les undo ici?
		// toolBar.add(actions[REDO]).setToolTipText("Redo");
		// toolBar.add((TAction)actions.get(TRedoAction.getID())).setToolTipText(Messages.getString("TMenuManager.110"));
		// //$NON-NLS-1$//TODO remettre ou pas les undo ici?
		// toolBar.add(actions[QUITTER]).setToolTipText("Fermer l'application");
		toolBar.add((TAction) actions.get(TQuitAction.getID())).setToolTipText(
				Messages.getString("TMenuManager.111")); //$NON-NLS-1$

		setButtonListeners();

		// ((TAction)actions[ZOOM]).setToggleButton(zoomButton);
		((TAction) actions.get(TZoomAction.getID()))
				.setToggleButton(zoomButton);
		((TAction) actions.get(TRoiAction.getID())).setToggleButton(roiButton);
		// ((TAction)actions[YELLOW_CROSS]).setToggleButton(ycButton);
		// ((TAction)actions.get(TYellowCrossAction.getID())).setToggleButton(ycButton);//shorcut
		// removed 2005/10/28
		// ((TAction)actions[SPOT_RESIZING_MODE]).setToggleButton(spotResizingButton);
		// ((TAction)actions.get(TSpotResizingModeAction.getID())).setToggleButton(spotResizingButton);//shorcut
		// removed 2005/10/28
		// ((TAction)actions[GRID_MOVABLE]).setToggleButton(gridMovableButton);
		((TAction) actions.get(TGridMovableAction.getID()))
				.setToggleButton(gridMovableButton);
		// ((TAction)actions[GRID_SELECTION_MODE]).setToggleButton(gridSelectionButton);
		((TAction) actions.get(TGridSelectionModeAction.getID()))
				.setToggleButton(gridSelectionButton);
		// ((TAction)actions[LEVEL2_SELECTION_MODE]).setToggleButton(level2SelectionButton);
		((TAction) actions.get(TLevel2SelectionModeAction.getID()))
				.setToggleButton(level2SelectionButton);
		// ((TAction)actions[LEVEL1_SELECTION_MODE]).setToggleButton(level1SelectionButton);
		((TAction) actions.get(TLevel1SelectionModeAction.getID()))
				.setToggleButton(level1SelectionButton);
		// ((TAction)actions[SPOT_SELECTION_MODE]).setToggleButton(spotSelectionButton);
		((TAction) actions.get(TSpotSelectionModeAction.getID()))
				.setToggleButton(spotSelectionButton);

		popupMenu = new JPopupMenu(Messages.getString("TMenuManager.112")); //$NON-NLS-1$

		// popupMenu.add(actions[ONGLETS]);
		popupMenu.add((TAction) actions.get(TOngletsAction.getID()));
		// popupMenu.add(actions[FENETRES]);
		popupMenu.add((TAction) actions.get(TFenetresAction.getID()));

		// sortAscending = new JCheckBoxMenuItem(actions[SORT_ASCENDING]);
		// sortDescending = new JCheckBoxMenuItem(actions[SORT_DESCENDING]);
		// colorizeColumn = new JCheckBoxMenuItem(actions[COLORIZE]);
		// hideColumn = new JCheckBoxMenuItem(actions[HIDE_COLUMN]);
		/*
		 * modif commentee depuis integration 14/09/05 sortAscending = new
		 * JCheckBoxMenuItem((TAction)actions.get(TSortColumnAscendingAction.getID()));
		 * sortDescending = new
		 * JCheckBoxMenuItem((TAction)actions.get(TSortColumnDescendingAction.getID()));
		 * colorizeColumn = new
		 * JCheckBoxMenuItem((TAction)actions.get(TColorizeColumnAction.getID()));
		 * hideColumn = new
		 * JCheckBoxMenuItem((TAction)actions.get(THideColumnAction.getID()));
		 */

		// TEST REMI
		// LES MENUS A AJOUTER DEPUIS LES PLUGINS
		pluginManager = new TPluginManager();// init des plugins
		JMenuItem menuItem;
		String parentTypeName = null;

		// 2005/12/13 addition for each menu of a separator in order to separate
		// plugins from initial functionnalities
		// 2005/12/14 separator only for menu concerned by plugins addition
		// menuEdit.addSeparator();
		// menuView.addSeparator();
		menuImage.addSeparator();
		menuGrid.addSeparator();
		menuAlign.addSeparator();
		menuQuantif.addSeparator();
		// menuBatch.addSeparator();
		// menuTools.addSeparator();
		// menuAbout.addSeparator();

		for (int i = 0; i < TPluginManager.plugs.length; i++) {
			System.out.println("plugin name="
					+ TPluginManager.plugs[i].getType().getName());
			// 2005/12/13 - According to the type of plugin, plugins menus are
			// created at the right place
			// System.out.println("actions.size avant="+actions.size());
			// /actions.addElement(TPluginManager.plugs[i]);// POUR AJOUTER
			// L'ACTION ASSOCIE AU PLUGIN MAIS NE METS PAS A JOUR L'ID PROPRE AU
			// PLUGIN
			// /TPluginManager.plugs[i].setMenuId(actions.size());//test remi
			// verif du nouveau num
			// System.out.println("MENU ID
			// apres="+TPluginManager.plugs[i].getId());//test remi
			// System.out.println("actions.size apres="+actions.size());
			// si c'est un menuPlugin, on l'ajoute dans un menu, sinon non.
			if (TPluginManager.plugs[i].isMenuPlugin()) {
				parentTypeName = TPluginManager.plugs[i].getType()
						.getSuperclass().getName();

				// System.out.println("parent type="+parentTypeName);
				menuItem = new JMenuItem((TPlugin) TPluginManager.plugs[i]);
				menuItem.addActionListener(this);// gives to the menu the
													// actionPerformed of the
													// plugin...
				// it is not useful to create a field for determmine where the
				// plugin
				// menu is added. We use the parent plugin type
				// 2006/02/20 - all menus were menuQuantif!!!Modification of
				// this bad copy/paste!
				if (parentTypeName == "agscan.plugins.SFormatPlugin") {
					menuImage.add(menuItem);
				} else if (parentTypeName == "agscan.plugins.SQuantifPlugin") {
					menuQuantif.add(menuItem);
				} else if (parentTypeName == "agscan.plugins.SImagePlugin") {
					menuImage.add(menuItem);
				} else if (parentTypeName == "agscan.plugins.SGridPlugin") {
					menuGrid.add(menuItem);
				} else if (parentTypeName == "agscan.plugins.SAlignPlugin") {
					menuAlign.add(menuItem);
				} else
					// agscan.plugins.TPlugin
					menuTools.add(menuItem);// PAR DEFAUT DANS le toolsMenu
			}

			// toolsMenu.add(menuItem);// si dans aucun l'ajouter dans le menu
			// des outils
		}
		// test affichage
		// for (int i=0;i < actions.size();i++){
		// System.out.println("Action "+i+" =>
		// "+actions.get(i).getClass().toString()); //$NON-NLS-1$ //$NON-NLS-2$
		// }
		// fin test affichage
		// FIN TEST REMI
	}

	public TMenuManager() {
		actions = new Vector();
		init(null);// init est appele a chaque fois que l'on change de type
	}

	// REMI ===> inutile car on execute systematiquement le actionPerformed du
	// plugin
	public void actionPerformed(ActionEvent e) {
		// on peut faire une liste des noms des plugins a leur chargement et

		// checker l'evenement dans cette liste.On cree alors la classe du
		// plugin et on appelle sa fonction ...


		JMenuItem source = (JMenuItem) (e.getSource());
		System.out
				.println("redirection vers le actionPerformed du plugin:" + source.getText()); //$NON-NLS-1$
		for (int i = 0; i < TPluginManager.plugs.length; i++) {

			if (source.getText() == TPluginManager.plugs[i].getName()) {
				System.out
						.println("clic sur " + TPluginManager.plugs[i].getName()); //$NON-NLS-1$

				// on recupere le type de plugin pour savoir quoi executer
				// System.out.println("type="+plugs[i].getType().getName());
				if (TPluginManager.plugs[i].getType().getName() == "image.plugins.globalAlign.PluginGlobalAlign") { //$NON-NLS-1$
					// PluginGlobalAlignTest popo=
					// (PluginGlobalAlignTest)plugs[i];
					// popo.align();

				}
				if (TPluginManager.plugs[i].getType().getName() == "image.plugins.tools.PluginTool") { //$NON-NLS-1$
					System.out.println("NAN je ne ferais rien!"); //$NON-NLS-1$
					// PluginTool popo = (PluginTool)plugs[i];
					// popo.run();
				}
				if (TPluginManager.plugs[i].getType().getName() == "TTestPlugin") { //$NON-NLS-1$
					System.out.println("boudiou du TTestPlugin!"); //$NON-NLS-1$
					// PluginTool popo = (PluginTool)plugs[i];
					// popo.run();
				}
				if (TPluginManager.plugs[i].getType().getName() == "TInfReaderPlugin") { //$NON-NLS-1$
					// TESTS EN DUR
					System.out.println("alors ca fait koi?"); //$NON-NLS-1$
					java.lang.reflect.Method[] m = TPluginManager.plugs[i]
							.getType().getDeclaredMethods();// list of public
															// methods
					for (int j = 0; j < m.length; j++) {
						System.out.println(m[j].getName());
					}
					System.out.println("CONSULTATION DES METHODES"); //$NON-NLS-1$
					Class[] params = null;
					for (int k = 0; k < m.length; ++k) {
						System.out
								.print(Modifier.toString(m[k].getModifiers()));
						System.out.print(" "); //$NON-NLS-1$
						System.out.print(m[k].getReturnType().getName());
						System.out.print(" "); //$NON-NLS-1$
						System.out.print(m[k].getName());
						System.out.print("("); //$NON-NLS-1$
						params = m[k].getParameterTypes();
						for (int k2 = 0; k2 < params.length; ++k2) {
							System.out.print(params[k2].getName());
						}
						System.out.println(")"); //$NON-NLS-1$
					}
					System.out.println("FIN DE CONSULTATION DES METHODES"); //$NON-NLS-1$

					// on recupere direct celle au nom qui nous interesse
					Method m1 = null;
					try {
						m1 = TPluginManager.plugs[i].getType().getMethod(
								"getImagePlus", params);// test params non recup
														// : c'est les derniers!
														// //$NON-NLS-1$
					} catch (NoSuchMethodException ex) {
						System.out.println("method getImagePlus not found!"); //$NON-NLS-1$
						System.out.println(ex.toString());
					}
					Object[] o = new Object[1];
					o[0] = new String("/home/rcathelin/test/sample.inf"); //$NON-NLS-1$
					// params[0]=new
					// String("/home/rcathelin/test/sample.inf").getClass();
					try {
						System.out.println("nom=" + m1.getName()); //$NON-NLS-1$
						ImagePlus ip = (ImagePlus) m1
								.invoke(TPluginManager.plugs[i].getType()
										.newInstance(), o);
						ip.show();
						// ca marche ici on a bien l'imagePlus!!!!!
					} catch (Exception ex2) {
						System.out.println("error...!"); //$NON-NLS-1$
						System.out.println("error=" + ex2.toString()); //$NON-NLS-1$
						System.out
								.println("cause=" + ex2.getCause().toString()); //$NON-NLS-1$
					}

					// tInfReaderPlugin = (TInfReaderPlugin)plugs[i];
					// ImagePlus ip =
					// tInfReaderPlugin.getImagePlus("/home/rcathelin/test/sample.inf");
					// ip.show();
					System.out.println("ca marche?"); //$NON-NLS-1$
				}

			}
		}
		// if (source.getText() == "pluginAlignHello") {
		// System.out.println("clic sur pluginAlignHello");
		// }
	}

	// fin REMI

	private void setButtonListeners() {
		zoomButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				zoom.setSelected(((JToggleButton) event.getSource())
						.isSelected());
			}
		});
		zoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				zoomButton.setSelected(((JCheckBoxMenuItem) event.getSource())
						.isSelected());
			}
		});
		// synchronisation de l'enfoncement du menu et du bouton
		roiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				roi.setSelected(((JToggleButton) event.getSource())
						.isSelected());
				if (((JToggleButton) event.getSource()).isSelected() == false) {
					// si decocher on retire de l'affichage le rectangle trac

					TRoiInteractor.getInstance().reset();
					TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
							TDataManager.GET_CURRENT_DATA, null);
					TDataElement element = (TDataElement) TEventHandler
							.handleMessage(ev)[0];
					element.getView().getGraphicPanel().repaint();
					// element.getView().getGraphicPanel().setCursor(new
					// Cursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		roi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				roiButton.setSelected(((JCheckBoxMenuItem) event.getSource())
						.isSelected());
				if (((JCheckBoxMenuItem) event.getSource()).isSelected() == false) {
					// si decocher on retire de l'affichage le rectangle trac

					TRoiInteractor.getInstance().reset();
					TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
							TDataManager.GET_CURRENT_DATA, null);
					TDataElement element = (TDataElement) TEventHandler
							.handleMessage(ev)[0];
					element.getView().getGraphicPanel().repaint();
					// element.getView().getGraphicPanel().setCursor(new
					// Cursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		// 2 buttons removed - 2005/10/28
		/*
		 * ycButton.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent event) {
		 * yellowCross.setSelected(((JToggleButton)event.getSource()).isSelected()); }
		 * });
		 * 
		 * yellowCross.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent event) {
		 * ycButton.setSelected(((JCheckBoxMenuItem)event.getSource()).isSelected()); }
		 * });
		 * 
		 * spotResizingButton.addActionListener(new ActionListener() { public
		 * void actionPerformed(ActionEvent event) {
		 * spotResizingMode.setSelected(((JToggleButton)event.getSource()).isSelected()); }
		 * }); spotResizingMode.addActionListener(new ActionListener() { public
		 * void actionPerformed(ActionEvent event) {
		 * spotResizingButton.setSelected(((JCheckBoxMenuItem)event.getSource()).isSelected()); }
		 * });
		 */
		gridMovableButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridMovable.setSelected(((JToggleButton) event.getSource())
						.isSelected());
			}
		});
		gridMovable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridMovableButton.setSelected(((JCheckBoxMenuItem) event
						.getSource()).isSelected());
			}
		});
		gridSelectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridMode.setSelected(true);
				level1Mode.setSelected(false);
				// level2Mode.setSelected(false);//removed 2005/11/08
				spotMode.setSelected(false);
				gridSelectionButton.setSelected(true);
				level1SelectionButton.setSelected(false);
				// level2SelectionButton.setSelected(false);//removed 2005/11/08
				spotSelectionButton.setSelected(false);
			}
		});
		gridMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridSelectionButton.setSelected(((JRadioButtonMenuItem) event
						.getSource()).isSelected());
				level1SelectionButton.setSelected(false);
				level2SelectionButton.setSelected(false);
				spotSelectionButton.setSelected(false);
			}
		});
		/*
		 * associated with the level3 = removed since 2005/10/28
		 * level2SelectionButton.addActionListener(new ActionListener() { public
		 * void actionPerformed(ActionEvent event) {
		 * gridMode.setSelected(false); level1Mode.setSelected(false);
		 * level2Mode.setSelected(true); spotMode.setSelected(false);
		 * gridSelectionButton.setSelected(false);
		 * level1SelectionButton.setSelected(false);
		 * level2SelectionButton.setSelected(true);
		 * spotSelectionButton.setSelected(false); } });
		 * level2Mode.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent event) {
		 * level2SelectionButton.setSelected(((JRadioButtonMenuItem)event.getSource()).isSelected());
		 * gridSelectionButton.setSelected(false);
		 * level1SelectionButton.setSelected(false);
		 * spotSelectionButton.setSelected(false); } });
		 */
		level1SelectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridMode.setSelected(false);
				level1Mode.setSelected(true);
				// level2Mode.setSelected(false);//removed 2005/11/08
				spotMode.setSelected(false);
				gridSelectionButton.setSelected(false);
				level1SelectionButton.setSelected(true);
				// level2SelectionButton.setSelected(false);//removed 2005/11/08
				spotSelectionButton.setSelected(false);
			}
		});
		level1Mode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				level1SelectionButton.setSelected(((JRadioButtonMenuItem) event
						.getSource()).isSelected());
				gridSelectionButton.setSelected(false);
				level2SelectionButton.setSelected(false);
				spotSelectionButton.setSelected(false);
			}
		});
		spotSelectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				gridMode.setSelected(false);
				level1Mode.setSelected(false);
				// level2Mode.setSelected(false);//removed 2005/11/08
				spotMode.setSelected(true);
				gridSelectionButton.setSelected(false);
				level1SelectionButton.setSelected(false);
				// level2SelectionButton.setSelected(false);//removed 2005/11/08
				spotSelectionButton.setSelected(true);
			}
		});
		spotMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				spotSelectionButton.setSelected(((JRadioButtonMenuItem) event
						.getSource()).isSelected());
				gridSelectionButton.setSelected(false);
				level1SelectionButton.setSelected(false);
				level2SelectionButton.setSelected(false);
			}
		});
	}

	public void init(TDataElement element) {
		TAction tmp;
		if (element == null) {
			for (Enumeration e = actions.elements(); e.hasMoreElements();) {
				tmp = (TAction) e.nextElement();
				tmp.setEnabled(true);
				// System.out.println("actif="+tmp.getActiveType());
				if (tmp.getActiveType() != TAction.ACTIVE_TYPE)
					tmp.setEnabled(false);
			}

			// if (pleinecran !=null) pleinecran.setEnabled(false);//pleinecran
			// removed since 2005/10/27
			if (zoom != null) {
				zoom.setState(false);
				zoom.setEnabled(false);
			}
			if (rules != null)
				rules.setState(false);
		}
		// HERE element of menus are actived or not
		else {
			if (element instanceof TImage) {
				// pleinecran.setEnabled(true);//pleinecran removed since
				// 2005/10/27
				for (Enumeration e = actions.elements(); e.hasMoreElements();) {
					tmp = (TAction) e.nextElement();
					if ((tmp.getActiveType() & TAction.IMAGE_TYPE) == TAction.IMAGE_TYPE
							|| (tmp.getActiveType() == TAction.ACTIVE_TYPE))
						tmp.setEnabled(true);
					else
						tmp.setEnabled(false);
				}
			} else if (element instanceof TGrid) {
				for (Enumeration e = actions.elements(); e.hasMoreElements();) {
					tmp = (TAction) e.nextElement();
					if ((tmp.getActiveType() & TAction.GRID_TYPE) == TAction.GRID_TYPE
							|| (tmp.getActiveType() == TAction.ACTIVE_TYPE))
						tmp.setEnabled(true);
					else
						tmp.setEnabled(false);
				}
				// pleinecran.setEnabled(true);//pleinecran removed since
				// 2005/10/27
			}
			// TBatch - added 2005/11/08
			else if (element instanceof TBatch) {
				for (Enumeration e = actions.elements(); e.hasMoreElements();) {
					tmp = (TAction) e.nextElement();
					if ((tmp.getActiveType() & TAction.BATCH_TYPE) == TAction.BATCH_TYPE
							|| (tmp.getActiveType() == TAction.ACTIVE_TYPE))
						tmp.setEnabled(true);
					else
						tmp.setEnabled(false);
				}
			} else if (element instanceof TAlignment) {
				// the two following lines allows to disable the roi from the
				// current element
				roi.setState(false);// added 2006/01/04
				roi.setEnabled(false);// added 2006/01/04
				if (((TAlignment) element).isAlgoRunning()) {
					for (Enumeration e = actions.elements(); e
							.hasMoreElements();) {
						tmp = (TAction) e.nextElement();
						if ((tmp.getActiveType() & TAction.ALIGNMENT_TYPE) == TAction.ALIGNMENT_TYPE)
							tmp.setEnabled(false);
						else
							tmp.setEnabled(false);
					}

				}

				else {
					// pas d'algo qui tourne
					for (Enumeration e = actions.elements(); e
							.hasMoreElements();) {
						tmp = (TAction) e.nextElement();
						if ((tmp.getActiveType() & TAction.ALIGNMENT_TYPE) == TAction.ALIGNMENT_TYPE
								|| (tmp.getActiveType() == TAction.ACTIVE_TYPE))
							tmp.setEnabled(true);
						else
							tmp.setEnabled(false);
					}

					// pleinecran.setEnabled(true);//pleinecran removed since
					// 2005/10/27
				}
			}
		}
	}

	public JMenuBar getMenuBar() {
		return menuBar;
	}

	public JPopupMenu getPopupMenu() {
		return popupMenu;
	}

	public JToolBar getToolBar() {
		return toolBar;
	}

	public Object[] BZMessageResponse(TEvent event) {
		int action = event.getAction();
		Object[] ret = null;
		int item;
		switch (action) {
		case GET_POPUP:
			ret = new Object[1];
			ret[0] = getPopupMenu();
			break;
		case SET_AVAILABLE:
			item = ((Integer) event.getParam(0)).intValue();
			boolean available = ((Boolean) event.getParam(1)).booleanValue();
			((TAction) actions.get(item)).setEnabled(available);
			break;
		case SET_SELECTED:

			item = ((Integer) event.getParam(0)).intValue();
			boolean selected = ((Boolean) event.getParam(1)).booleanValue();
			// ((JMenuItem)((TAction)actions.get(item)).getValue("menuitem")).setSelected(selected);
			// //$NON-NLS-1$
			((TAction) actions.get(item)).setToggleButtonSelected(selected);
			break;
		case IS_ZOOM:
			ret = new Object[1];
			ret[0] = new Boolean(zoom.isSelected());
			break;
		case IS_SIMPLIFIED_SPOTS:
			ret = new Object[1];
			ret[0] = new Boolean(yellowCross.isSelected());
			break;
		case IS_SPOT_RESIZING_MODE:
			ret = new Object[1];
			ret[0] = new Boolean(spotResizingMode.isSelected());
			break;
		case IS_GRID_MOVABLE:
			ret = new Object[1];
			ret[0] = new Boolean(!gridMovable.isSelected());
			break;
		case IS_COMPUTED_DIAMETERS:// ajout integration 14/09/05
			ret = new Object[1];
			ret[0] = new Boolean(showComputedDiameters.isSelected());
			break;
		case IS_ROI:
			ret = new Object[1];
			ret[0] = new Boolean(roi.isSelected());
			break;
		case IS_RULES:
			ret = new Object[1];
			ret[0] = new Boolean(rules.isSelected());
			break;
		case FIRE_ACTION:
			((TAction) actions.get(((Integer) event.getParam(0)).intValue()))
					.actionPerformed(new ActionEvent(this,
							ActionEvent.ACTION_PERFORMED, "", 0)); //$NON-NLS-1$
			break;
		case UPDATE:
			TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,
					TDataManager.GET_CURRENT_DATA, null);
			TDataElement element = (TDataElement) TEventHandler
					.handleMessage(ev)[0];
			init(element);
			// init((TDataElement)event.getParam(0));
			break;
		case GET_SELECTION_MODE:
			ret = new Object[1];
			ret[0] = new Integer(-1);
			if (gridMode.isSelected())
				ret[0] = new Integer(TGridSelectionModeAction.getID());
			else if (spotMode.isSelected())
				ret[0] = new Integer(TSpotSelectionModeAction.getID());
			// ret[0] = new Integer(SPOT_SELECTION_MODE);
			else if (level1Mode.isSelected())
				ret[0] = new Integer(TLevel1SelectionModeAction.getID());
			// ret[0] = new Integer(LEVEL1_SELECTION_MODE);
			else if (level2Mode.isSelected())
				ret[0] = new Integer(TLevel2SelectionModeAction.getID());
			// ret[0] = new Integer(LEVEL2_SELECTION_MODE);
			break;
		case GET_FIT_ALGORITHM:
			ret = new Object[1];
			ret[0] = new Integer(-1);

			// 3 lines behind commented because we will not choose the fit since
			// 2005/10/27
			// it's why FIT_GAUSS_NEWTON is by default now: 2005/10/28
			// if (fitNormal.isSelected())
			// ret[0] = new Integer(FIT_NORMAL);
			// else if (fitGaussNewton.isSelected())
			ret[0] = new Integer(FIT_GAUSS_NEWTON);

			break;
		case NEXT_ID:
			ret = new Object[1];
			ret[0] = new Integer(actions.size());
		}
		return ret;
	}

	public static TAction getAction(int i) {
		return (TAction) actions.get(i);
	}

}
/*******************************************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 * 
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 * 
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * provided that the above copyright notice(s) and this permission notice appear
 * in all copies of the Software and that both the above copyright notice(s) and
 * this permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE
 * LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other
 * dealings in this Software without prior written authorization of the
 * copyright holder.
 ******************************************************************************/
