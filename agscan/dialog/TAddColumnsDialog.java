/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.DataFormatException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableColumnModel;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.data.element.grid.TGrid;

public class TAddColumnsDialog extends TDialog {
  JButton closeButton = new JButton();
  JPanel centerPanel = new JPanel();
  GridBagLayout gridBagLayout2 = new GridBagLayout();
  JTextField defaultValueTextField = new JTextField();
  JLabel nameLabel = new JLabel();
  JLabel valuesLabel = new JLabel();
  JLabel defaultValueLabel = new JLabel();
  JLabel typeLabel = new JLabel();
  JTextField nameTextField = new JTextField();
  GridBagLayout gridBagLayout4 = new GridBagLayout();
  JPanel columnParamsPanel = new JPanel();
  JTextField valuesTextField = new JTextField();
  JComboBox typeComboBox = new JComboBox();
  JButton supButton = new JButton();
  JTextField supTextField = new JTextField();
  JLabel betweenLabel = new JLabel();
  JCheckBox betweenCheckBox = new JCheckBox();
  JPanel colorPanel = new JPanel();
  JCheckBox infCheckBox = new JCheckBox();
  JCheckBox supCheckBox = new JCheckBox();
  JButton betweenButton = new JButton();
  JTextField between2TextField = new JTextField();
  JButton infButton = new JButton();
  JTextField infTextField = new JTextField();
  JTextField between1TextField = new JTextField();
  GridBagLayout gridBagLayout3 = new GridBagLayout();
  Border border1;
  TitledBorder titledBorder1;
  JPanel okClosePanel = new JPanel();
  JButton okButton = new JButton();

  private DefaultTableColumnModel columns;

  public TAddColumnsDialog(TDataElement element) throws HeadlessException {
    try {
      jbInit();
      initListeners();
      init(element);
      pack();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void init(TDataElement element) {
    if (!(element instanceof TGrid) && !(element instanceof TAlignment)) {
      nameTextField.setText(""); //$NON-NLS-1$
      nameTextField.setEditable(false);
      typeComboBox.setSelectedIndex(0);
      typeComboBox.setEnabled(false);
      defaultValueTextField.setText(""); //$NON-NLS-1$
      defaultValueTextField.setEditable(false);
      valuesTextField.setText(""); //$NON-NLS-1$
      valuesTextField.setEditable(false);
      supCheckBox.setSelected(false);
      supCheckBox.setEnabled(false);
      supButton.setEnabled(false);
      supButton.setBackground(Color.lightGray);
      betweenCheckBox.setSelected(false);
      betweenCheckBox.setEnabled(false);
      betweenButton.setEnabled(false);
      betweenButton.setBackground(Color.lightGray);
      infCheckBox.setSelected(false);
      infCheckBox.setEnabled(false);
      infButton.setEnabled(false);
      infButton.setBackground(Color.lightGray);
      okButton.setEnabled(false);
      columns = null;
    }
    else {
      nameTextField.setEditable(true);
      typeComboBox.setEnabled(true);
      typeComboBox.getItemListeners()[0].itemStateChanged(new ItemEvent(typeComboBox, 0, typeComboBox.getSelectedItem(), ItemEvent.SELECTED));
      okButton.setEnabled(true);
      columns = ((TGriddedElement)element).getGridModel().getConfig().getColumns();
    }
  }
  private void jbInit() throws Exception {
    border1 = BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140));
    titledBorder1 = new TitledBorder(border1,Messages.getString("TAddColumnsDialog.3")); //$NON-NLS-1$
    setTitle(Messages.getString("TAddColumnsDialog.4")); //$NON-NLS-1$
    setResizable(false);
    closeButton.setMargin(new Insets(0, 5, 0, 5));
    closeButton.setSelected(false);
    closeButton.setText(Messages.getString("TAddColumnsDialog.5")); //$NON-NLS-1$
    centerPanel.setLayout(gridBagLayout2);
    nameLabel.setText(Messages.getString("TAddColumnsDialog.6")); //$NON-NLS-1$
    valuesLabel.setText(Messages.getString("TAddColumnsDialog.7")); //$NON-NLS-1$
    defaultValueLabel.setText(Messages.getString("TAddColumnsDialog.8")); //$NON-NLS-1$
    typeLabel.setText(Messages.getString("TAddColumnsDialog.9")); //$NON-NLS-1$
    columnParamsPanel.setLayout(gridBagLayout4);
    columnParamsPanel.setBorder(BorderFactory.createEtchedBorder());
    supButton.setPreferredSize(new Dimension(35, 20));
    supButton.setMinimumSize(new Dimension(35, 20));
    supButton.setMaximumSize(new Dimension(35, 20));
    betweenLabel.setText(Messages.getString("TAddColumnsDialog.10")); //$NON-NLS-1$
    betweenCheckBox.setText(Messages.getString("TAddColumnsDialog.11")); //$NON-NLS-1$
    colorPanel.setLayout(gridBagLayout3);
    infCheckBox.setText(Messages.getString("TAddColumnsDialog.12")); //$NON-NLS-1$
    supCheckBox.setText(Messages.getString("TAddColumnsDialog.13")); //$NON-NLS-1$
    betweenButton.setMaximumSize(new Dimension(35, 20));
    betweenButton.setMinimumSize(new Dimension(35, 20));
    betweenButton.setPreferredSize(new Dimension(35, 20));
    infButton.setMaximumSize(new Dimension(35, 20));
    infButton.setMinimumSize(new Dimension(35, 20));
    infButton.setPreferredSize(new Dimension(35, 20));
    colorPanel.setBorder(titledBorder1);
    okButton.setPreferredSize(new Dimension(70, 21));
    okButton.setText(Messages.getString("TAddColumnsDialog.14")); //$NON-NLS-1$
    supTextField.setPreferredSize(new Dimension(50, 21));
    supTextField.setRequestFocusEnabled(true);
    between1TextField.setPreferredSize(new Dimension(50, 21));
    between2TextField.setPreferredSize(new Dimension(50, 21));
    infTextField.setPreferredSize(new Dimension(50, 21));
    this.getContentPane().add(centerPanel, BorderLayout.CENTER);
    columnParamsPanel.add(nameTextField,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(10, 5, 0, 10), 0, 0));
    columnParamsPanel.add(nameLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    columnParamsPanel.add(typeLabel,  new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    columnParamsPanel.add(typeComboBox,  new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 10), 0, 0));
    columnParamsPanel.add(defaultValueLabel,   new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    columnParamsPanel.add(defaultValueTextField,   new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 10), 0, 0));
    columnParamsPanel.add(valuesLabel,   new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0), 0, 0));
    columnParamsPanel.add(valuesTextField,   new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 10, 10), 0, 0));
    centerPanel.add(colorPanel,                     new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 10), 0, 0));
    colorPanel.add(supCheckBox,                new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(supTextField,               new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(betweenCheckBox,                new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(between1TextField,            new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(betweenLabel,           new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(between2TextField,          new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    colorPanel.add(supButton,                      new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 0, 5), 0, 0));
    colorPanel.add(betweenButton,           new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 0, 5), 0, 0));
    colorPanel.add(infCheckBox,          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    colorPanel.add(infTextField,    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    colorPanel.add(infButton,      new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 5, 5), 0, 0));
    centerPanel.add(columnParamsPanel,                    new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(10, 10, 10, 5), 0, 0));
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_INTEGER]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_REAL]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_TEXT]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_BOOLEAN]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_PROBA]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_INTEGER_LIST]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_REAL_LIST]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_TEXT_LIST]);
    typeComboBox.addItem(TColumn.columnType[TColumn.TYPE_URL]);
    centerPanel.add(okClosePanel,         new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 10, 0), 0, 0));
    okClosePanel.add(okButton, null);
    okClosePanel.add(closeButton, null);
  }
  private void initListeners() {
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        String name = nameTextField.getText();
        TEvent ev;
        if (name.equals("")) { //$NON-NLS-1$
          JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.16"), Messages.getString("TAddColumnsDialog.17"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
          return;
        }
        if (!existColumn(name)) {
          int type = typeComboBox.getSelectedIndex();
          Vector v = null;
          Object defaultValue = null;
          if ((type == TColumn.TYPE_INTEGER_LIST) || (type == TColumn.TYPE_TEXT_LIST) ||
              (type == TColumn.TYPE_REAL_LIST)) {
            try {
              v = TColumn.makeList(type, valuesTextField.getText());
            }
            catch (NumberFormatException ex) {
              JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.18"), Messages.getString("TAddColumnsDialog.19"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
              return;
            }
            if (v.size() < 2) {
              JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.20"), Messages.getString("TAddColumnsDialog.21"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
              return;
            }
          }
          if (defaultValueTextField.getText().equals("")) { //$NON-NLS-1$
            JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.23"), Messages.getString("TAddColumnsDialog.24"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
            return;
          }
          try {
            defaultValue = TColumn.makeValue(type, defaultValueTextField.getText(), v);
          }
          catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.25") + defaultValueTextField.getText() + //$NON-NLS-1$
                                          Messages.getString("TAddColumnsDialog.26"), Messages.getString("TAddColumnsDialog.27"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
            return;
          }
          catch (DataFormatException ex) {
            JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.28") + defaultValueTextField.getText() + //$NON-NLS-1$
                                          Messages.getString("TAddColumnsDialog.29"), Messages.getString("TAddColumnsDialog.30"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
            return;
          }
          Object supValue = null;
          if (supCheckBox.isSelected()) {
            try {
              supValue = TColumn.makeValue(type, supTextField.getText(), null);
            }
            catch (Exception ex) {
              JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.31") + supTextField.getText() + //$NON-NLS-1$
                                                  Messages.getString("TAddColumnsDialog.32"), Messages.getString("TAddColumnsDialog.33"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
              return;
            }
          }
          Object betweenValue1 = null;
          Object betweenValue2 = null;
          if (betweenCheckBox.isSelected()) {
            try {
              betweenValue1 = TColumn.makeValue(type, between1TextField.getText(), null);
              betweenValue2 = TColumn.makeValue(type, between2TextField.getText(), null);
              if (((Comparable)betweenValue1).compareTo((Comparable)betweenValue2) == 1) {
                JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.34") + supTextField.getText() + //$NON-NLS-1$
                                                    Messages.getString("TAddColumnsDialog.35"), Messages.getString("TAddColumnsDialog.36"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
                return;
              }
              if (supValue != null)
                if (((Comparable)supValue).compareTo((Comparable)betweenValue2) == -1) {
                  JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.37") + supTextField.getText() + //$NON-NLS-1$
                                                Messages.getString("TAddColumnsDialog.38"), Messages.getString("TAddColumnsDialog.39"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
                  return;
                }
            }
            catch (Exception ex) {
              JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.40"), Messages.getString("TAddColumnsDialog.41"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
              return;
            }
          }
          Object infValue = null;
          if (infCheckBox.isSelected()) {
            try {
              infValue = TColumn.makeValue(type, infTextField.getText(), null);
              if (betweenCheckBox.isSelected())
                if (((Comparable)infValue).compareTo((Comparable)betweenValue1) == 1) {
                  JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.42") + infTextField.getText() + //$NON-NLS-1$
                                                Messages.getString("TAddColumnsDialog.43"), Messages.getString("TAddColumnsDialog.44"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
                  return;
              }
            }
            catch (Exception ex) {
              JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.45") + supTextField.getText() + //$NON-NLS-1$
                                                  Messages.getString("TAddColumnsDialog.46"), Messages.getString("TAddColumnsDialog.47"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
              return;
            }
          }
          Vector params = new Vector();
          switch (type) {
            case TColumn.TYPE_INTEGER:
            case TColumn.TYPE_PROBA:
            case TColumn.TYPE_REAL:
              params.addElement(name);
              params.addElement(name);
              params.addElement(defaultValue);
              params.addElement(new Boolean(infCheckBox.isSelected()));
              params.addElement(infButton.getBackground());
              params.addElement(infValue);
              params.addElement(new Boolean(betweenCheckBox.isSelected()));
              params.addElement(betweenButton.getBackground());
              params.addElement(betweenValue1);
              params.addElement(betweenValue2);
              params.addElement(new Boolean(supCheckBox.isSelected()));
              params.addElement(supButton.getBackground());
              params.addElement(supValue);
              break;
            case TColumn.TYPE_TEXT:
            case TColumn.TYPE_BOOLEAN:
            case TColumn.TYPE_URL:
              params.addElement(name);
              params.addElement(name);
              params.addElement(defaultValue);
              break;
            case TColumn.TYPE_INTEGER_LIST:
            case TColumn.TYPE_REAL_LIST:
            case TColumn.TYPE_TEXT_LIST:
              params.addElement(name);
              params.addElement(name);
              params.addElement(defaultValue);
              params.addElement(v);
              break;
          }
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.ADD_COLUMN, null, new Integer(type), params);
          TColumn column = (TColumn)TEventHandler.handleMessage(ev)[0];
          column.setBooleanBetweenColor(betweenCheckBox.isSelected());
          column.setBooleanInfColor(infCheckBox.isSelected());
          column.setBooleanSupColor(supCheckBox.isSelected());
          ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
          ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, TEventHandler.handleMessage(ev)[0]);
          TEventHandler.handleMessage(ev);
        }
        else {
          JOptionPane.showMessageDialog(null, Messages.getString("TAddColumnsDialog.48") + name + //$NON-NLS-1$
                                              Messages.getString("TAddColumnsDialog.49"), Messages.getString("TAddColumnsDialog.50"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
    });
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        dispose();
      }
    });
    infCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        infTextField.setEditable(infCheckBox.isSelected());
        infButton.setEnabled(infCheckBox.isSelected());
      }
    });
    betweenCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        between1TextField.setEditable(betweenCheckBox.isSelected());
        between2TextField.setEditable(betweenCheckBox.isSelected());
        betweenButton.setEnabled(betweenCheckBox.isSelected());
      }
    });
    supCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        supTextField.setEditable(supCheckBox.isSelected());
        supButton.setEnabled(supCheckBox.isSelected());
      }
    });
    typeComboBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
          if ((event.getItem() == TColumn.columnType[TColumn.TYPE_INTEGER_LIST]) ||
              (event.getItem() == TColumn.columnType[TColumn.TYPE_REAL_LIST]) ||
              (event.getItem() == TColumn.columnType[TColumn.TYPE_TEXT_LIST])) {
            valuesTextField.setEditable(true);
          }
          else {
            valuesTextField.setEditable(false);
          }
          if ((event.getItem() == TColumn.columnType[TColumn.TYPE_INTEGER]) ||
              (event.getItem() == TColumn.columnType[TColumn.TYPE_REAL]) ||
              (event.getItem() == TColumn.columnType[TColumn.TYPE_PROBA])) {
            infCheckBox.setEnabled(true);
            infCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(infCheckBox));
            betweenCheckBox.setEnabled(true);
            betweenCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(betweenCheckBox));
            supCheckBox.setEnabled(true);
            supCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(supCheckBox));
          }
          else {
            infCheckBox.setSelected(false);
            infCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(infCheckBox));
            infCheckBox.setEnabled(false);
            betweenCheckBox.setSelected(false);
            betweenCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(betweenCheckBox));
            betweenCheckBox.setEnabled(false);
            supCheckBox.setSelected(false);
            supCheckBox.getChangeListeners()[0].stateChanged(new ChangeEvent(supCheckBox));
            supCheckBox.setEnabled(false);
          }
        }
      }
    });
    infButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        Color col = JColorChooser.showDialog(null, Messages.getString("TAddColumnsDialog.51"), infButton.getBackground()); //$NON-NLS-1$
        infButton.setBackground(col);
      }
    });
    betweenButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        Color col = JColorChooser.showDialog(null, Messages.getString("TAddColumnsDialog.52"), betweenButton.getBackground()); //$NON-NLS-1$
        betweenButton.setBackground(col);
      }
    });
    supButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        Color col = JColorChooser.showDialog(null, Messages.getString("TAddColumnsDialog.53"), supButton.getBackground()); //$NON-NLS-1$
        supButton.setBackground(col);
      }
    });
  }
  private boolean existColumn(String name) {
    boolean exist = false;
    Enumeration enume = columns.getColumns();
    while (enume.hasMoreElements())
      if (((TColumn)enume.nextElement()).toString().equals(name)) {
        exist = true;
        break;
      }
    return exist;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
