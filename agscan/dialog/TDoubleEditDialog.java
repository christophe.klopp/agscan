/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import agscan.Messages;

public class TDoubleEditDialog extends JDialog {
  JPanel centerPanel = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JLabel valueLabel = new JLabel();
  protected JSpinner valueSpinner = new JSpinner();
  JPanel southPanel = new JPanel();
  JButton okButton = new JButton();
  JButton cancelButton = new JButton();

  private Double result;

  public TDoubleEditDialog(Double d) throws HeadlessException {
    try {
      jbInit(d);
      initListeners();
      result = null;
      setModal(true);
      pack();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit(Double d) throws Exception {
    setTitle(Messages.getString("TDoubleEditDialog.0")); //$NON-NLS-1$
    setResizable(false);
    this.getContentPane().setLayout(borderLayout1);
    valueSpinner = new JSpinner(new TSpinnerDoubleModel(d.doubleValue(), -Double.MAX_VALUE, Double.MAX_VALUE, 1.0D, 100000000));
    valueSpinner.setEditor(new JSpinner.NumberEditor(valueSpinner, "########.########")); //$NON-NLS-1$
    valueSpinner.setMaximumSize(new Dimension(100, 25));
    valueSpinner.setMinimumSize(new Dimension(100, 25));
    valueSpinner.setPreferredSize(new Dimension(100, 25));
    centerPanel.setLayout(gridBagLayout1);
    valueLabel.setText(Messages.getString("TDoubleEditDialog.2")); //$NON-NLS-1$
    okButton.setPreferredSize(new Dimension(49, 21));
    okButton.setText(Messages.getString("TDoubleEditDialog.3")); //$NON-NLS-1$
    cancelButton.setPreferredSize(new Dimension(73, 21));
    cancelButton.setText(Messages.getString("TDoubleEditDialog.4")); //$NON-NLS-1$
    this.getContentPane().add(centerPanel,  BorderLayout.CENTER);
    centerPanel.add(valueLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    centerPanel.add(valueSpinner,    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    this.getContentPane().add(southPanel,  BorderLayout.SOUTH);
    southPanel.add(okButton, null);
    southPanel.add(cancelButton, null);
  }
  public Double getResult() { return result; }
  private void initListeners() {
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        result = (Double)valueSpinner.getValue();
        dispose();
      }
    });
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        dispose();
      }
    });
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
