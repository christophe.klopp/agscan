package agscan.dialog;

/**
 * Modification 2006/02/20: string externalization 
 * Modification 2006/03/08: fixed bug#201 of Mulcyber
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.controler.TAlignmentControler;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPluginManager;

public class TQuantifSelectionDialog extends TDialog {
	private static TDialog instance;
	
	public TQuantifSelectionDialog() {
		try {
			jbInit();
			setResizable(false);
			setTitle(Messages.getString("TQuantifSelectionDialog.0")); //$NON-NLS-1$
			initListeners();
			allCheckBox.setSelected(true);
			qtfFitConstCheckBox.setSelected(true);
			qtfFitVarCheckBox.setSelected(true);
			qtfImageConstCheckBox.setSelected(true);
			qtfImageVarCheckBox.setSelected(true);
			qmCheckBox.setSelected(true);
			fitCorrectionCheckBox.setSelected(true);
			overshiningCorrectionCheckBox.setSelected(true);
			pack();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public void init(TDataElement element) {
		
	}
	private void initListeners() {
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				Vector calculs = new Vector();
				//The order of the following lines impacts the order of the output result table
				// fixed bug#201 of Mulcyber: inversion of the fit and image lines - 2006/03/09
				if (!pluginCheckBox.isSelected())
				{
					if (qtfImageConstCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_QUANTIF_IMAGE_CONST));
					if (qtfImageVarCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_QUANTIF_IMAGE_COMP));
					if (qtfFitConstCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_QUANTIF_FIT_CONST));
					if (qtfFitVarCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_QUANTIF_FIT_COMP));
					if (qmCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_QM));
					if (fitCorrectionCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_FIT_CORRECTION));
					if (overshiningCorrectionCheckBox.isSelected()) calculs.addElement(new Integer(TAlignmentControler.COMPUTE_OVERSHINING_CORRECTION));
					if(calculs.size() !=0)
					{
						TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SELECTION, null, calculs);
						TEventHandler.handleMessage(ev);
						notifyClosing();
						dispose();
					}
					else
					{
						TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Veuillez choisir...");// externalized 2006/02/02  //$NON-NLS-1$
						TEventHandler.handleMessage(ev);
						return;
					}
				}
				else 
				{
					int iplugin = pluginSelected();
					int nbchan = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
					if(iplugin!=-1)
					{
						SQuantifPlugin plug = (SQuantifPlugin)TPluginManager.plugs[iplugin];
						if (nbchan>=plug.getImagesMin()&&nbchan<=plug.getImagesMax())
						{
								plug.run();
								notifyClosing();
								dispose();
						}
						else
						{
							TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Impossible, le nombre de canaux est incompatible avec l'alignement.");// externalized 2006/02/02  //$NON-NLS-1$
							TEventHandler.handleMessage(ev);
							return;
						}
					}
					else
					{
						TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Veuillez choisir...");// externalized 2006/02/02  //$NON-NLS-1$
						TEventHandler.handleMessage(ev);
						return;
					}
				}
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				notifyClosing();
				dispose();
			}
		});
		allCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				qtfFitConstCheckBox.setSelected(allCheckBox.isSelected());
				qtfFitVarCheckBox.setSelected(allCheckBox.isSelected());
				qtfImageConstCheckBox.setSelected(allCheckBox.isSelected());
				qtfImageVarCheckBox.setSelected(allCheckBox.isSelected());
				qmCheckBox.setSelected(allCheckBox.isSelected());
				fitCorrectionCheckBox.setSelected(allCheckBox.isSelected());
				overshiningCorrectionCheckBox.setSelected(allCheckBox.isSelected());
			}
		});
	}
	private void jbInit() throws Exception {
		Container con = new JPanel() {
			public Insets getInsets() {
				return new Insets(10, 10, 10, 10);
			}
		};
		this.setContentPane(con);
		// string externalization since 2006/02/20
		selectLabel.setText(Messages.getString("TQuantifSelectionDialog.1")); //$NON-NLS-1$
		this.getContentPane().setLayout(borderLayout1);
		okButton.setText(Messages.getString("TQuantifSelectionDialog.2")); //$NON-NLS-1$
		cancelButton.setText(Messages.getString("TQuantifSelectionDialog.3")); //$NON-NLS-1$
		southPanel.setLayout(flowLayout1);
		qtfImageConstCheckBox.setText(Messages.getString("TQuantifSelectionDialog.4")); //$NON-NLS-1$
		qtfImageVarCheckBox.setText(Messages.getString("TQuantifSelectionDialog.5")); //$NON-NLS-1$
		centerPanel.setLayout(gridBagLayout1);
		centerPanel.setBorder(null);
		jPanel1.setLayout(gridBagLayout2);
		jPanel1.setBorder(border3);
		allCheckBox.setText(Messages.getString("TQuantifSelectionDialog.6")); //$NON-NLS-1$
		qtfFitConstCheckBox.setText(Messages.getString("TQuantifSelectionDialog.7")); //$NON-NLS-1$
		qtfFitVarCheckBox.setText(Messages.getString("TQuantifSelectionDialog.8")); //$NON-NLS-1$
		qmCheckBox.setText(Messages.getString("TQuantifSelectionDialog.9")); //$NON-NLS-1$
		fitCorrectionCheckBox.setText(Messages.getString("TQuantifSelectionDialog.10")); //$NON-NLS-1$
		overshiningCorrectionCheckBox.setText(Messages.getString("TQuantifSelectionDialog.11")); //$NON-NLS-1$
		borderLayout1.setHgap(10);
		borderLayout1.setVgap(10);
		this.getContentPane().add(northPanel, java.awt.BorderLayout.NORTH);
		northPanel.add(selectLabel);
		this.getContentPane().add(southPanel, java.awt.BorderLayout.SOUTH);
		southPanel.add(okButton);
		southPanel.add(cancelButton);
		this.getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);
		centerPanel.add(jPanel1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		centerPanel.add(jPanel2, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		jPanel1.add(qtfImageConstCheckBox, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(qtfImageVarCheckBox, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(qtfFitConstCheckBox, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(qtfFitVarCheckBox, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(qmCheckBox, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(fitCorrectionCheckBox, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(overshiningCorrectionCheckBox, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 10), 0, 0));
		jPanel1.add(allCheckBox, new GridBagConstraints(0, 7, 3, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(15, 10, 5, 10), 0, 0));
	
		// plugins
		initPlugins();
		pluginCheckBox.setText(Messages.getString("TQuantifSelectionDialog.13")); //$NON-NLS-1$
		pluginCheckBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e)
			{
				for(int i=0;i<rbplugs.length;i++)
				{
					rbplugs[i].setEnabled(pluginCheckBox.isSelected());
				}
				qtfFitConstCheckBox.setEnabled(!pluginCheckBox.isSelected());
				qtfFitVarCheckBox.setEnabled(!pluginCheckBox.isSelected());
				qtfImageConstCheckBox.setEnabled(!pluginCheckBox.isSelected());
				qtfImageVarCheckBox.setEnabled(!pluginCheckBox.isSelected());
				qmCheckBox.setEnabled(!pluginCheckBox.isSelected());
				fitCorrectionCheckBox.setEnabled(!pluginCheckBox.isSelected());
				overshiningCorrectionCheckBox.setEnabled(!pluginCheckBox.isSelected());
				allCheckBox.setEnabled(!pluginCheckBox.isSelected());
			}
		});
		jPanel2.setLayout(new BoxLayout(jPanel2,BoxLayout.Y_AXIS));
		jPanel2.setBorder(border3);
		plugPan.setLayout(new BoxLayout(plugPan,BoxLayout.Y_AXIS));
		for(int i=0;i<rbplugs.length;i++)
		{
			
				plugPan.add(rbplugs[i]);
				bgplugs.add(rbplugs[i]);
		}
		jPanel2.add(pluginCheckBox);
		jPanel2.add(plugPan);
	}
	
	public void initPlugins()
	{
		for(int i=0;i<TPluginManager.plugs.length;i++)
		{
			if(TPluginManager.plugs[i] instanceof SQuantifPlugin && TPluginManager.plugs[i].isBatchPlugin())indexes.add(i);
		}
		int tot = indexes.size();
		rbplugs = new JRadioButton[tot];
		for (int i=0;i<tot;i++)
		{
			rbplugs[i] = new JRadioButton(TPluginManager.plugs[indexes.get(i)].getName());
			rbplugs[i].setEnabled(pluginCheckBox.isSelected());
		}
	}
	
	public int pluginSelected()
	{
		for(int i=0;i<rbplugs.length;i++)
		{
			if (rbplugs[i].isSelected())return indexes.get(i);
		}
		return -1;
	}
	
	public static TDialog getInstance(TDataElement element) {
		if (instance == null)
			instance = new TQuantifSelectionDialog();
		else
			instance.init(element);
		return instance;
	}
	
	JLabel selectLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	JPanel northPanel = new JPanel();
	JPanel southPanel = new JPanel();
	JPanel centerPanel = new JPanel();
	JButton okButton = new JButton();
	JButton cancelButton = new JButton();
	FlowLayout flowLayout1 = new FlowLayout();
	JCheckBox qtfImageConstCheckBox = new JCheckBox();
	JCheckBox qtfImageVarCheckBox = new JCheckBox();
	Border border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 100, 0, 0));
	Border border2 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
	JPanel jPanel1 = new JPanel();
	JPanel jPanel2 = new JPanel();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JCheckBox allCheckBox = new JCheckBox();
	GridBagLayout gridBagLayout2 = new GridBagLayout();
	Border border3 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 0, 5, 5));
	JCheckBox qtfFitConstCheckBox = new JCheckBox();
	JCheckBox qtfFitVarCheckBox = new JCheckBox();
	JCheckBox qmCheckBox = new JCheckBox();
	JCheckBox fitCorrectionCheckBox = new JCheckBox();
	JCheckBox overshiningCorrectionCheckBox = new JCheckBox();
	JCheckBox pluginCheckBox = new JCheckBox();
	JRadioButton[] rbplugs;
	JPanel plugPan = new JPanel();
	ButtonGroup bgplugs = new ButtonGroup();
	Vector<Integer> indexes = new Vector<Integer>();
}
