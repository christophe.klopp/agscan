/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * Modification : 2005/10/26:connexion of the dialog with current image  (save into project properties) 
 * @version 2005/10/26
 * @author rcathelin
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import agscan.Messages;
import agscan.AGScan;

public class TImageInfoDialog extends JDialog {
	class DoubleVerifier extends InputVerifier {
		
		public boolean verify(JComponent input) {
			boolean ok = true;
			try {
				Double.parseDouble(((JTextField)input).getText());
			}
			catch (NumberFormatException ex) {
				ok = false;
			}
			System.out.println("dans le verifier="+ok);
			return ok;
		}
	}
	
	private JButton cancelButton = new JButton(Messages.getString("TImageInfoDialog.0")); //$NON-NLS-1$
	private JButton okButton = new JButton(Messages.getString("TImageInfoDialog.1")); //$NON-NLS-1$
	private JTextField latitudeTextField = new JTextField(""); //$NON-NLS-1$
	private JTextField  pixelHeightTextField = new JTextField (""); //$NON-NLS-1$
	//private JTextField sensitivityTextField = new JTextField(""); viré le 26/08/05
	private JTextField pixelWidthTextField = new JTextField(""); //$NON-NLS-1$
	private JTextField ipTypeTextField = new JTextField(""); //$NON-NLS-1$
	private JComboBox unitComboBox = new JComboBox();
	private boolean cancelled;
	
	public TImageInfoDialog() {
		//jbInit(25, 25, "QL", "20x25", 5); //modif 2005/10/26
		jbInit(AGScan.prop.getDoubleProperty("TImageInfoDialog.pixelWidth"),
				AGScan.prop.getDoubleProperty("TImageInfoDialog.pixelHeight"),
				AGScan.prop.getProperty("TImageInfoDialog.unit"),
				AGScan.prop.getProperty("TImageInfoDialog.ipType"),
				AGScan.prop.getDoubleProperty("TImageInfoDialog.latitude")); 
		initListeners();
		cancelled = true;
		pack();
	}
	private void jbInit(double pW, double pH, String un, String ipt, double lat) {
		Border textFieldBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
				new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 5, 0, 5));
		setModal(true);
		setResizable(false);
		setTitle(Messages.getString("TImageInfoDialog.9")); //$NON-NLS-1$
		unitComboBox.addItem(Messages.getString("TImageInfoDialog.10")); //$NON-NLS-1$
		unitComboBox.addItem(Messages.getString("TImageInfoDialog.11")); //$NON-NLS-1$
		JPanel varPanel = new JPanel(new GridBagLayout());
		varPanel.setBorder(BorderFactory.createCompoundBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white,
				new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		latitudeTextField.setInputVerifier(new DoubleVerifier());
		pixelHeightTextField.setInputVerifier(new DoubleVerifier());
		JLabel latitudeLabel = new JLabel(Messages.getString("TImageInfoDialog.12")); //$NON-NLS-1$
		//JLabel sensitivityLabel = new JLabel(Messages.getString("TImageInfoDialog.13")); //$NON-NLS-1$
		JLabel bppLabel = new JLabel(Messages.getString("TImageInfoDialog.14")); //$NON-NLS-1$
		//   sensitivityTextField.setInputVerifier(new DoubleVerifier());
		JLabel pixelWidthLabel = new JLabel(Messages.getString("TImageInfoDialog.15")); //$NON-NLS-1$
		pixelWidthTextField.setInputVerifier(new DoubleVerifier());

		JLabel pixelHeightLabel = new JLabel(Messages.getString("TImageInfoDialog.16")); //$NON-NLS-1$
		JLabel unitLabel = new JLabel(Messages.getString("TImageInfoDialog.17")); //$NON-NLS-1$
		JLabel ipTypeLabel = new JLabel(Messages.getString("TImageInfoDialog.18")); //$NON-NLS-1$
		ipTypeTextField.setEditable(true);
		cancelButton.setPreferredSize(new Dimension(cancelButton.getMinimumSize().width, 21));
		cancelButton.setMargin(new Insets(2, 2, 2, 2));
		cancelButton.setFont(new java.awt.Font("Dialog", 1, 11)); //$NON-NLS-1$
		okButton.setPreferredSize(new Dimension(okButton.getMinimumSize().width, 21));
		okButton.setMargin(new Insets(2, 2, 2, 2));
		okButton.setFont(new Font("Dialog", 1, 11)); //$NON-NLS-1$
		JPanel southPanel = new JPanel();
		southPanel.add(okButton, null);
		southPanel.add(cancelButton, null);
		pixelWidthTextField.setText(String.valueOf(pW));
		pixelHeightTextField.setText(String.valueOf(pH));
		unitComboBox.setSelectedItem(un);
		latitudeTextField.setText(String.valueOf(lat));
		// sensitivityTextField.setText(String.valueOf(sens));
		ipTypeTextField.setText(ipt);
		JPanel centerPanel = new JPanel();
		centerPanel.add(varPanel, null);
		this.getContentPane().add(centerPanel, BorderLayout.CENTER);
		this.getContentPane().add(southPanel, BorderLayout.SOUTH);
		pixelWidthTextField.setEditable(true);
		pixelHeightTextField.setEditable(true);
		unitComboBox.setEnabled(true);
		latitudeTextField.setEditable(true);
		//sensitivityTextField.setEditable(true);
		ipTypeTextField.setEditable(true);
		varPanel.add(pixelWidthLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		varPanel.add(pixelWidthTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		varPanel.add(pixelHeightLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		varPanel.add(pixelHeightTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		varPanel.add(unitLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		varPanel.add(unitComboBox, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		varPanel.add(ipTypeLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		varPanel.add(ipTypeTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		varPanel.add(latitudeLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		varPanel.add(latitudeTextField, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
		// varPanel.add(sensitivityLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
		//                                                      GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		//varPanel.add(sensitivityTextField, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
		//                                                         GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
	}
	private void initListeners() {
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cancelled = true;
				dispose();
			}
		});
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				//try {
				//	Double.parseDouble(pixelHeightTextField.getText());
				//	Double.parseDouble(pixelWidthTextField.getText());
				//	Double.parseDouble(latitudeTextField.getText());
					//   Double.parseDouble(sensitivityTextField.getText());
					
					// 2005/10/26: images parameters are included intoi the parameters file 
					AGScan.prop.setProperty("TImageInfoDialog.pixelWidth",getPixelWidth());
					AGScan.prop.setProperty("TImageInfoDialog.pixelHeight",getPixelHeight());
					AGScan.prop.setProperty("TImageInfoDialog.unit",getUnit());
					AGScan.prop.setProperty("TImageInfoDialog.ipType",getIPType());
					AGScan.prop.setProperty("TImageInfoDialog.latitude",getLatitude()); 
					
					cancelled = false;
					dispose();
				//}
				//catch (NumberFormatException ex) {
				//}
			}
		});
	}
	public boolean isCancelled() {
		return cancelled;
	}
	
	//modif =>tous les getters en private 2005/10/26
	private double getPixelWidth() {
		return Double.parseDouble(pixelWidthTextField.getText());
	}
	private double getPixelHeight() {
		return Double.parseDouble(pixelHeightTextField.getText());
	}
	private double getLatitude() {
		return Double.parseDouble(latitudeTextField.getText());
	}
	/*public double getSensitivity() {
	 return Double.parseDouble(sensitivityTextField.getText());
	 }
	 */
	private String getUnit() {
		return (String)unitComboBox.getSelectedItem();
	}
	private String getIPType() {
		return ipTypeTextField.getText();
	}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
