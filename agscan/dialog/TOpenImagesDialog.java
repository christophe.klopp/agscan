/*
 * Created on june 2005
 * 
 * This class is the open images dialog
 * Modification : 2005/10/07: add of a 3rd file image/channel for fluorescence
 * Modification : 2005/10/26: add of a properties image button 
 * Modification : 2005/10/26: radiobuttons aspect modified in order to become more visible
 * Modification : 2005/10/26: title of this dialog added
 * Modification : 2006/02/02: externalization of missing strings
 * 
 * @version 2005/10/26
 * @author rcathelin
 * 
 */
package agscan.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.event.TEvent;

public class TOpenImagesDialog extends JDialog implements ActionListener {
	private File file1 = null;//	file1 = Cy5 image file (red)
	private File file2 = null;//	file1 = Cy3 image file (green)
	private File file3 = null;//	file3 is blue image for the 3rd RGB channel
	private boolean cancelled;
	private TFileImageChooser chooser = new TFileImageChooser();
	private File currentDirectory = null; //repertoire pour le chooser
	// DECLARATION DES COMPOSANTS
	private ButtonGroup groupe = new ButtonGroup();
	private JRadioButton jRadioButton_radio = new JRadioButton(Messages.getString("TOpenImagesDialog.0"), true); //$NON-NLS-1$
	private JRadioButton jRadioButton_fluo = new JRadioButton(Messages.getString("TOpenImagesDialog.1"), false); //$NON-NLS-1$
	
	private JPanel jPanel_radio = new JPanel();
	private JPanel jPanel_fluo = new JPanel();
	private JPanel jPanel_fluo1 = new JPanel();
	private JPanel jPanel_fluo2 = new JPanel();
	private JPanel jPanel_fluo3 = new JPanel();//06/10/05
	private JPanel jPanel_choice = new JPanel();
	private JTextField jTextField_radioName = new JTextField();
	private JTextField jTextField_fluoName1 = new JTextField();
	private JTextField jTextField_fluoName2 = new JTextField();
	private JTextField jTextField_fluoName3 = new JTextField();
	private JTextField jTextField_radioImage = new JTextField(Messages.getString("TOpenImagesDialog.2")); //$NON-NLS-1$
	private JTextField jTextField_fluoImage1 = new JTextField(Messages.getString("TOpenImagesDialog.4")); //$NON-NLS-1$
	private JTextField jTextField_fluoImage2 = new JTextField(Messages.getString("TOpenImagesDialog.3")); //$NON-NLS-1$
	private JTextField jTextField_fluoImage3 = new JTextField(Messages.getString("TOpenImagesDialog.13")); //$NON-NLS-1$
	private JButton jButton_radioBrowse = new JButton(Messages.getString("TOpenImagesDialog.5")); //$NON-NLS-1$
	private JButton jButton_fluo1Browse = new JButton(Messages.getString("TOpenImagesDialog.5")); //$NON-NLS-1$
	private JButton jButton_fluo2Browse = new JButton(Messages.getString("TOpenImagesDialog.5")); //$NON-NLS-1$
	private JButton jButton_fluo3Browse = new JButton(Messages.getString("TOpenImagesDialog.5")); //$NON-NLS-1$
	private JButton jButton_OK = new JButton(Messages.getString("TOpenImagesDialog.8")); //$NON-NLS-1$
	private JButton jButton_cancel = new JButton(Messages.getString("TOpenImagesDialog.9")); //$NON-NLS-1$
	private JButton jButton_props = new JButton(Messages.getString("TOpenImagesDialog.14")); // externalized 2006/02/02 
	
	
	public TOpenImagesDialog() {
		this.setTitle(Messages.getString("TOpenImagesDialog.15"));// externalized 2006/02/02 
		init();
		cancelled = true;
		pack();
	}
	private void init() {
		// CONSTRUCTION DES COMPOSANTS //
		
		jTextField_radioName.setPreferredSize(new Dimension(100, 27));
		//jTextField_radioName.setDisabledTextColor(Color.white);
		jTextField_radioName.setEditable(false);
		jTextField_radioName.setBackground(Color.white);
		
		jTextField_fluoName1.setPreferredSize(new Dimension(100, 27));
		//jTextField_fluoName1.setDisabledTextColor(Color.white);
		jTextField_fluoName1.setEditable(false);
		jTextField_fluoName1.setBackground(Color.white);
		
		jTextField_fluoName2.setPreferredSize(new Dimension(100, 27));
		//jTextField_fluoName2.setDisabledTextColor(Color.white);
		jTextField_fluoName2.setEditable(false);
		jTextField_fluoName2.setBackground(Color.white);
	
		jTextField_fluoName3.setPreferredSize(new Dimension(100, 27));		
		jTextField_fluoName3.setEditable(false);
		jTextField_fluoName3.setBackground(Color.lightGray);//idea => gray button = optional
	
		jTextField_radioImage.setEditable(false);
		jTextField_fluoImage1.setEditable(false);
		jTextField_fluoImage2.setEditable(false);
		jTextField_fluoImage3.setEditable(false);
		
		//2005/10/26 - add : modify color structure of button in order to know that they are clickable
		jRadioButton_radio.setBackground(Color.white);
		jRadioButton_radio.setForeground(Color.black);
		jRadioButton_fluo.setBackground(Color.white);
		jRadioButton_fluo.setForeground(Color.black);
		jRadioButton_radio.setContentAreaFilled(false);
		jRadioButton_fluo.setContentAreaFilled(false);
		
		groupe.add(jRadioButton_radio);
		groupe.add(jRadioButton_fluo); 	
		
		// AFFECTATION DES LISTENERS
		jRadioButton_fluo.addActionListener(this);
		jRadioButton_radio.addActionListener(this);  	
		jButton_radioBrowse.addActionListener(this);
		jButton_fluo1Browse.addActionListener(this);
		jButton_fluo2Browse.addActionListener(this);
		jButton_fluo3Browse.addActionListener(this);
		jButton_cancel.addActionListener(this);
		jButton_OK.addActionListener(this);
		jButton_props.addActionListener(this);
		
		//ASSEMBLAGE DES COMPOSANTS
		jPanel_radio.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jPanel_radio.add(jRadioButton_radio);
	//	jPanel_choice.add(jRadioButton_radio);
		jPanel_radio.add(jTextField_radioImage);
		jPanel_radio.add(jTextField_radioName);
		jPanel_radio.add(jButton_radioBrowse);
		
		jPanel_fluo.setLayout(new BoxLayout(jPanel_fluo, BoxLayout.Y_AXIS));
		jPanel_fluo1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jPanel_fluo1.add(jRadioButton_fluo);
		//jPanel_choice.add(jRadioButton_fluo);
		jPanel_fluo1.add(jTextField_fluoImage1);
		jPanel_fluo1.add(jTextField_fluoName1);
		jPanel_fluo1.add(jButton_fluo1Browse);
		jPanel_fluo2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jPanel_fluo2.add(jTextField_fluoImage2);
		jPanel_fluo2.add(jTextField_fluoName2);
		jPanel_fluo2.add(jButton_fluo2Browse);
		jPanel_fluo3.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jPanel_fluo3.add(jTextField_fluoImage3);
		jPanel_fluo3.add(jTextField_fluoName3);
		jPanel_fluo3.add(jButton_fluo3Browse);
		jPanel_fluo.add(jPanel_fluo1);
		jPanel_fluo.add(jPanel_fluo2);
		jPanel_fluo.add(jPanel_fluo3);
				
		jPanel_choice.add(jButton_OK);
		jPanel_choice.add(jButton_cancel);
		jPanel_choice.add(jButton_props);
		
		jPanel_radio.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TOpenImagesDialog.10"))); //$NON-NLS-1$
		jPanel_fluo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TOpenImagesDialog.11"))); //$NON-NLS-1$
		jPanel_choice.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TOpenImagesDialog.12"))); //$NON-NLS-1$
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.getContentPane().add(jPanel_radio);
		this.getContentPane().add(jPanel_fluo);
		this.getContentPane().add(jPanel_choice);
		
		// INITIALISATION DES CHOIX
		setEnabledwithFluo(false);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == jRadioButton_fluo) {
			System.out.println("clic sur fluo"); //$NON-NLS-1$
			setEnabledwithFluo(true);
			
		}
		if (e.getSource() == jRadioButton_radio) {
			System.out.println("clic sur radio"); //$NON-NLS-1$
			setEnabledwithFluo(false);
		}
		if (e.getSource() == jButton_radioBrowse){
			if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
			int returnVal = chooser.showOpenDialog(null);
			currentDirectory = chooser.getCurrentDirectory();
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file1 = chooser.getSelectedFile();
				file2 = null;
				file3 = null;
				jTextField_radioName.setText(file1.getPath());
			}
		}
		if (e.getSource() == jButton_fluo1Browse){
			if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
			int returnVal = chooser.showOpenDialog(null);
			currentDirectory = chooser.getCurrentDirectory();
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file1 = chooser.getSelectedFile();
				jTextField_fluoName1.setText(file1.getPath());
			}
		}
		if (e.getSource() == jButton_fluo2Browse){
			if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
			int returnVal = chooser.showOpenDialog(null);
			currentDirectory = chooser.getCurrentDirectory();
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file2 = chooser.getSelectedFile();
				jTextField_fluoName2.setText(file2.getPath());
			}
		}
		if (e.getSource() == jButton_fluo3Browse){
			if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
			int returnVal = chooser.showOpenDialog(null);
			currentDirectory = chooser.getCurrentDirectory();
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file3 = chooser.getSelectedFile();
				jTextField_fluoName3.setText(file3.getPath());
			}
		}
		if (e.getSource() == jButton_cancel){
			cancelled = true;
			dispose();
		}
		
		if (e.getSource() == jButton_props){
			TImageInfoDialog dialog = new TImageInfoDialog();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
			dialog.setVisible(true);
		}
			
		if (e.getSource() == jButton_OK){
			File file1 = getFile1();//Cy5 image file (red)
			File file2 = getFile2();//Cy3 image file (green)
			File file3 = getFile3();//Blue image file // 07/10/05
			//verifier si un ou deux fichier ( = si radio ou fluo)
			System.out.println("file1"+file1); 
			System.out.println("file2="+file2);
			System.out.println("file3="+file3);
			// on fait les if avec les tailles des jTextField plutot que les noms (null ou pas)...
			//car si une image dans radio et l'autre ou les 2 autres dans fluo ca marche: c'est la image1 du radioqui est prise!
			//je retire le fait que si juste une image on se retrouve en fluo
			//maintenant c'est 2 images 1 et 2 ou les 3 images
			if (jRadioButton_radio.isSelected() && jTextField_radioName.getText().length()!=0){
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, file1);
				TEventHandler.handleMessage(event);				
			}
			else if (jRadioButton_fluo.isSelected() &&
					jTextField_fluoName1.getText().length()!=0 &&
					jTextField_fluoName2.getText().length()!=0 &&
					jTextField_fluoName3.getText().length()==0){
				//fluo with 2 images Green and Red
				//	TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, file1);
				//		TEventHandler.handleMessage(event);
				//			event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, file2);
				//			TEventHandler.handleMessage(event);
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, file1, file2);
				TEventHandler.handleMessage(event);				
			}
			//fluo with 3 images ( Green/Red/Blue)
			else if (jRadioButton_fluo.isSelected() &&
					jTextField_fluoName1.getText().length()!=0 &&
					jTextField_fluoName2.getText().length()!=0 &&
					jTextField_fluoName3.getText().length()!=0){
				System.out.println("COUCOU= "+ jTextField_fluoName1.getText().length()+"  "+jTextField_fluoName2.getText().length()!=0+"  "+jTextField_fluoName3.getText().length());
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, file1, file2, file3);
				TEventHandler.handleMessage(event);		
			}
			// files are not given, ask user for it
			else{			
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,Messages.getString("TOpenImagesDialog.16"));// externalized 2006/02/02 
				TEventHandler.handleMessage(event);
				return;//permet de quitter cette methode =  sans fermer la fenetre
			}
			cancelled = false;
			dispose();
			
			
			
			AGScan.prop.setProperty("imagesPath",currentDirectory.getAbsolutePath());//added 2005/11/29
		}
		
		
	}
	
	//tous les composants du fluo sont mis enabled ou non
	private void setEnabledwithFluo(boolean bool){
		
		jPanel_fluo.setEnabled(bool);
		jTextField_fluoName1.setEnabled(bool);
		jButton_fluo1Browse.setEnabled(bool);
		jTextField_fluoName2.setEnabled(bool);
		jButton_fluo2Browse.setEnabled(bool);
		jTextField_fluoName3.setEnabled(bool);
		jButton_fluo3Browse.setEnabled(bool);
		jTextField_fluoImage1.setEnabled(bool);
		jTextField_fluoImage2.setEnabled(bool);
		jTextField_fluoImage3.setEnabled(bool);
		

		
		jPanel_radio.setEnabled(!bool);
		jTextField_radioName.setEnabled(!bool);
		jButton_radioBrowse.setEnabled(!bool);
		jTextField_radioImage.setEnabled(!bool);
		
		if (bool) {
			jTextField_radioName.setBackground(Color.gray);
			jTextField_fluoName1.setBackground(Color.white);
			jTextField_fluoName2.setBackground(Color.white);
			jTextField_fluoName3.setBackground(Color.lightGray);//always gray because optional,good idea?
		}
		else {
			jTextField_radioName.setBackground(Color.white);
			jTextField_fluoName1.setBackground(Color.gray);
			jTextField_fluoName2.setBackground(Color.gray);	
			jTextField_fluoName3.setBackground(Color.gray);
		}
		
	}

	public boolean isCancelled() {
		return cancelled;
	}
	
	public File getFile1(){
		return file1;
	}
	public File getFile2(){
		return file2;
	}
	public File getFile3(){
		return file3;
	}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
