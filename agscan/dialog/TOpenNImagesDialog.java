/*
 * Created on june 2005
 * 
 * This class is the open images dialog
 * Modification : 2005/10/07: add of a 3rd file image/channel for fluorescence
 * Modification : 2005/10/26: add of a properties image button 
 * Modification : 2005/10/26: radiobuttons aspect modified in order to become more visible
 * Modification : 2005/10/26: title of this dialog added
 * Modification : 2006/02/02: externalization of missing strings
 * 
 * @version 2005/10/26
 * @author rcathelin
 * 
 */
package agscan.dialog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TBatchControler;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TAction;
import agscan.menu.action.TProcessChooserAction;
import agscan.menu.action.TUndoAction;
import agscan.statusbar.TStatusBar;

public class TOpenNImagesDialog extends JDialog implements ActionListener 
{
	
	/**** DECLARATION DES ATTRIBUTS ****/
	// Vrai si la fenetre est ouverte dans le cadre d'un batch
	private boolean batch;
	// Memorise le nombre d'images a ouvrir (nombre de canaux)
	private int nbImages ;
	// Memorise le nom des couleurs de la configuration choisie
	String[] les_couleurs;
	// Memorise la liste des fichiers d'images a ouvrir
	private File[] N_file;
	// Permet la selection d'un fichier d'image dans l'arborescence
	private TFileImageChooser chooser;
	// Memorise le repertoire courant
	private File currentDirectory = null; 
	// Panel global pour la selection des images a ouvrir
	private JPanel jPanel_fluo;
	// Panel pour la validation (boutons Ok/Annuler)
	private JPanel jPanel_choice;
	// Panel pour le choix du nombre d'images a ouvrir
	private JPanel jPanel_nbcol;
	// Champ de texte specifiant le nombre d'images a ouvrir
	private JTextField tf_nb;
	// Bouton pour valider le nombre d'images a ouvrir (met a jour jPanel_choice)
	private JButton b_valnb;
	// Tableau de Panel pour la selection des images a ouvrir (1 Panel par image)
	private JPanel[] N_jPanel;
	// Tableau de Champ de texte specifiant les noms de couleur des images a ouvrir
	// Non Editable
	private JTextField[] N_jTextField_Name;
	// Tableau de Champ de texte specifiant les chemins d'acces des images a ouvrir
	// Editable
	private JTextField[] N_jTextField_Image;
	// Tableau de Bouton pour la selection des chemins d'acces des images ("Parcourir")
	private JButton[] N_jButton_Browse;
	// Bouton Ok
	private JButton jButton_OK; 
	// Bouton Annuler
	private JButton jButton_cancel; 
	// Bouton Proprietes qui ouvre la fenetre permettant
	// de modifier les proprietes
	private JButton jButton_props;  
	
	
	
	/**** CONSTRUCTEUR ****/
	public TOpenNImagesDialog(boolean isbatch) 
	{
		// si c est un batch, batch recoit VRAI
		batch = isbatch;
		// titre, init, et compactage de la fenetre 
		if(!batch)this.setTitle(Messages.getString("TOpenNImagesDialog.15")); //$NON-NLS-1$
		else this.setTitle(Messages.getString("TOpenNImagesDialog.17")); //$NON-NLS-1$
		init(batch);
		pack();
	}
	
	
	
	/**** INITIALISATIONS ****/
	private void init(boolean batch) {
		
		// on declare un nouveau panel pour le contenu de la fenetre
		this.setContentPane(new JPanel());
		
		// on recupere le nombre de couleurs du choix precedent
		nbImages = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
		System.out.println(nbImages);
		
		// on declare un tableau de chaines qui contiendra la liste des couleurs
		les_couleurs = new String[nbImages];
		
		// instanciation d'un nouveau selectionneur de fichier
		chooser = new TFileImageChooser();
		
		// instanciations de 3 nouveaux panels pour :
		// la selection des fichiers, les boutons ok/annuler/proprietes 
		// et le choix du nombre de couleurs
		jPanel_fluo = new JPanel();
		jPanel_choice = new JPanel();
		jPanel_nbcol = new JPanel();
		
		// instanciation d'un nouveau panel pour le choix du nombre de parametrage
		jPanel_nbcol = new JPanel();
		
		// choix d'une configuration FlowLayout
		jPanel_nbcol.setLayout(new FlowLayout());
		
		// on met une bordure sur le panel avec un titre 
		jPanel_nbcol.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TOpenNImagesDialog.19"))); //$NON-NLS-1$
		
		// instanciation d'un nouveau champ de texte pour le choix du nombre de parametrage
		tf_nb = new JTextField(""+AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"),3); //$NON-NLS-1$ //$NON-NLS-2$
		
		// on rend le champ editable si ce n est pas un batch
		tf_nb.setEditable(!batch);
		
		// on aligne le contenu du champ sur la droite car c'est un champ numerique
		tf_nb.setHorizontalAlignment(JTextField.RIGHT);
		
		// on ajoute le champ de texte au panel
		jPanel_nbcol.add(tf_nb);
		
		// instanciations des 3 boutons et mise sur ecoute
		b_valnb = new JButton(Messages.getString("TOpenNImagesDialog.20"));  //$NON-NLS-1$
		b_valnb.setEnabled(!batch);			// on desactive le bouton si c est un batch
		b_valnb.addActionListener(this);
		
		// on ajoute le bouton de validation au panel
		jPanel_nbcol.add(b_valnb);
		
		// construction des tableaux de composants
		N_jTextField_Image = new JTextField[nbImages];
		N_jTextField_Name = new JTextField[nbImages];
		N_jButton_Browse = new JButton[nbImages];
		N_jPanel = new JPanel[nbImages];
		N_file = new File[nbImages];
		
		// on remplit le tableau des couleurs
		les_couleurs = findColors();
		
		// construction des elements 1 par 1
		for(int i=0;i<nbImages;i++)
		{
			// fichier
			N_file[i]=null;
			
			// barre de texte chemin d'acces
			N_jTextField_Image[i] = new JTextField(les_couleurs[i]);
			N_jTextField_Image[i].setEditable(false);
			
			// label de numerotation
			N_jTextField_Name[i] = new JTextField();
			N_jTextField_Name[i].setPreferredSize(new Dimension(100, 27));
			N_jTextField_Name[i].setEditable(false);
			N_jTextField_Name[i].setBackground(Color.white);
			N_jTextField_Name[i].setEditable(false);
			
			// bouton "Parcourir"
			N_jButton_Browse[i] = new JButton(Messages.getString("TOpenNImagesDialog.4")); //$NON-NLS-1$
			N_jButton_Browse[i].addActionListener(this);
			
			// panel
			N_jPanel[i] = new JPanel();
			N_jPanel[i].setLayout(new FlowLayout(FlowLayout.RIGHT));
			
			// ajout des labels, barre de texte et boutons a chaque panel
			N_jPanel[i].add(N_jTextField_Image[i]);
			N_jPanel[i].add(N_jTextField_Name[i]);
			N_jPanel[i].add(N_jButton_Browse[i]);
			
			// ajout de chaque panel au panel (pere) de choix d'images
			jPanel_fluo.add(N_jPanel[i]);
		}
		
		// configuration du panel de choix des images
		jPanel_fluo.setLayout(new BoxLayout(jPanel_fluo, BoxLayout.Y_AXIS));
		
		// instanciations de 3 nouveaux boutons pour ok/annuler/proprietes
		jButton_OK = new JButton(Messages.getString("TOpenNImagesDialog.8"));  //$NON-NLS-1$
		jButton_cancel = new JButton(Messages.getString("TOpenNImagesDialog.9"));  //$NON-NLS-1$
		jButton_props = new JButton(Messages.getString("TOpenNImagesDialog.10"));   //$NON-NLS-1$
		
		// affectation des listeners sur les composants du panel validation
		jButton_cancel.addActionListener(this);
		jButton_OK.addActionListener(this);
		jButton_props.addActionListener(this);
		
		// ajout des composants au panel de validation
		jPanel_choice.add(jButton_OK);
		jPanel_choice.add(jButton_cancel);
		jPanel_choice.add(jButton_props);
		
		
		// bordures des differents panels
		jPanel_fluo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TOpenNImagesDialog.12"))); //$NON-NLS-1$
		jPanel_choice.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.ORANGE),Messages.getString("TOpenNImagesDialog.14"))); //$NON-NLS-1$
		
		// configuration du panel global et assemblage de tous les panels
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.getContentPane().add(jPanel_nbcol);
		this.getContentPane().add(jPanel_fluo);
		this.getContentPane().add(jPanel_choice);
	}
	
	/**** FONCTION GENERALE POUR TOUS LES LISTENERS ****/
	public void actionPerformed(ActionEvent e) 
	{
		// CK add multiple file selection for mono channel images 07/04/2008
		// if images are mono channel then it is possible to select multiple images 
		// two elements have to be filled N_file the table of files 
		// N_jTextField_Name[0] the string with all the names 
		if (nbImages == 1){
			if (e.getSource() == N_jButton_Browse[0]){
				System.out.println("if nb image = 1");
				if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
				int returnVal = chooser.showOpenDialog(null);
				currentDirectory = chooser.getCurrentDirectory();
				//File[] files = null ;
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					N_file = chooser.getSelectedFiles(); 
					String filePath = "";
					for (int i = 0; i < N_file.length; i++){
						//System.out.println("file "+i+" "+files[i].getPath());
						filePath = filePath+" "+N_file[i].getPath();
					}
					N_jTextField_Name[0].setText(filePath);
				}	
			}
		// for images with more than one channel
		}else{
			// on cherche quel bouton a ete clique
			for(int i=0;i<nbImages;i++)
			{
				// cas d'un des boutons "parcourir"
				if (e.getSource() == N_jButton_Browse[i])
				{

					// on permet l'ouverture des fichiers .inf et .tif
					// CK deletion of the 4 next redundant elements 
					//chooser.addChoosableFileFilter(new ImageFilter("inf", "Fuji images (*.inf)")); //$NON-NLS-1$ //$NON-NLS-2$
					//chooser.addChoosableFileFilter(new ImageFilter("tif", "TIFF images (*.tif)")); //$NON-NLS-1$ //$NON-NLS-2$
					//String[] exts = {"inf", "tif"}; //$NON-NLS-1$ //$NON-NLS-2$
					//chooser.setFileFilter(new ImageFilter(exts, "All images (*.inf *.tif )")); //$NON-NLS-1$


					// ouverture d'une boite de dialogue de selection de fichier
					if (currentDirectory != null) chooser.setCurrentDirectory(currentDirectory);
					int returnVal = chooser.showOpenDialog(null);
					currentDirectory = chooser.getCurrentDirectory();

					// si on valide l'ouverture du fichier 
					if (returnVal == JFileChooser.APPROVE_OPTION) {

						// le fichier est memorise
						N_file[i] = chooser.getSelectedFile();
						// le chemin d'acces est affiche dans la barre de texte
						N_jTextField_Name[i].setText(N_file[i].getPath());
					}
				}
			}
		}
		
		// cas du bouton "annuler"
		if (e.getSource() == jButton_cancel)
		{
			// on ferme la fenetre
			dispose();
		}
		
		// cas du bouton "proprietes"
		if (e.getSource() == jButton_props)
		{
			// on ouvre la boite de dialogue pour le choix des proprietes
			TImageInfoDialog dialog = new TImageInfoDialog();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
			dialog.setVisible(true);
		}
		
		// cas du bouton "valider"
		if (e.getSource() == b_valnb)
		{
			// on retient la valeur saisie dans le champ de texte 
			int nb = Integer.valueOf(tf_nb.getText()).intValue();
			
			// si le nombre de couleurs saisi est bien compris entre 1 et 7 et,
			// si celui ci est different de celui du fichier des parametres
			if(		nb>Integer.valueOf("0").intValue() //$NON-NLS-1$
				&&	nb<Integer.valueOf("8").intValue() //$NON-NLS-1$
				&&  nb!=AGScan.prop.getIntProperty("TColorParametersAction.nbcolors")) //$NON-NLS-1$
			{
				// on va chercher l indice de la configuration 
				// qui correspond au nombre de canaux saisi
				int res = getConf(nb);
				// si on trouve un entier >-1
				if (res != -1)
				{
					// on met a jour le fichier des parametres
					AGScan.prop.setProperty("TColorParametersAction.nbcolors",nb); //$NON-NLS-1$
					AGScan.prop.setProperty("TColorParametersAction.colors",AGScan.prop.getProperty("TColorParametersAction.colors"+(res+1))); //$NON-NLS-1$ //$NON-NLS-2$
					// on reconstruit la fenetre
					init(false);
					// raffraichissement
					this.repaint();
					// recompactage
					this.pack();
				}
				// si on trouve -1 : aucune configuration ne correspond
				else
				{			
					// on affiche un message d'erreur
					TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,Messages.getString("TOpenNImagesDialog.18")); //$NON-NLS-1$
					TEventHandler.handleMessage(event);
					return;//permet de quitter cette methode =  sans fermer la fenetre
				}
				
			}
		}
			
		
		// cas du bouton "ok"
		if (e.getSource() == jButton_OK)
		{

			// on verifie qu'on a bien choisi un fichier pour chaque image
			boolean openok = true;
			for(int i=0;i<nbImages;i++)
			{
				if (N_jTextField_Name[i].getText().length()==0 ) openok = false;	
			}
			
			// si c est le cas 
			if (openok)
			{
				// si on n'est pas dans le cas d'un batch
				if(!batch)
				{
					// on ouvre la ou les image(s)
					// si on a plus de 3 images, 
					// on construit l image RGB avec les 3 premieres
					AGScan.prop.setProperty("TColorParametersAction.nbcolors",nbImages); //$NON-NLS-1$
					TEvent event3 = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, N_file);
					TEventHandler.handleMessage(event3);

				}
				// si c'est un batch 
				else
				{
					// on va ajouter les images 
					// a la pile d'images a traiter
					TEvent events = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.ADD_JOBS, null, N_file,new Integer(nbImages));
					TEventHandler.handleMessage(events);
					TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
				}
					
				
				
			}
		
			// sinon on signale une erreur  
			else{			
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,Messages.getString("TOpenImagesDialog.16"));// externalized 2006/02/02  //$NON-NLS-1$
				TEventHandler.handleMessage(event);
				return;//permet de quitter cette methode =  sans fermer la fenetre
			}
			
			// on ferme la fenetre 
			dispose();
			
			// on retient le chemin d'acces i 
			// choisi pour selectionner les fichiers
			// il devient le chemin par defaut (modif. des proprietes)
			AGScan.prop.setProperty("imagesPath",currentDirectory.getAbsolutePath());//added 2005/11/29 //$NON-NLS-1$
		}
	}
	
	/**** Decompose la chaine de parametrage 
		et place chaque couleur dans le tableau de chaines les_couleurs ****/
	public static String[] findColors()
	{
		// on recupere le nombre de canaux voulu par l'utilisateur
		int k = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
		String[] colors = new String[k];
		
		//System.out.println("findColors : "+couleurs);
		int numconf = getConf(k)+1;
		System.out.println(numconf);
		String couleurs = AGScan.prop.getProperty("TColorParametersAction.colors"+numconf); //$NON-NLS-1$
		
		// on decompose la chaine : 
		// on recupere les groupes de caracteres separes par des virgules
		String toto = "^.:"; //$NON-NLS-1$
		for (int j = 0; j < k-1; j++){ toto = toto+"([^,]*),";} //$NON-NLS-1$
		toto = toto+"([^,]*)"; //$NON-NLS-1$
		Pattern p = Pattern.compile(toto);
		Matcher m = p.matcher(couleurs);
		
		// chaque element est une couleur que l'on place dans le tableau
		if( m.matches())
		for( int i = 0; i< m.groupCount(); i++)
		{
			//System.out.println(m.group(i+1));
			colors[i] = m.group(i+1);
		}
		return colors;
	}
	
	
	/**** Renvoie l'indice du parametrage couleur 
	 	qui correspond au nombre de canaux choisi ****/
	public static int getConf(int nb)
	{
		// si on ne trouve pas on renvoie -1
		int iconf = -1;
		
		// on recupere le nombre de parametrage
		int nbconf =  AGScan.prop.getIntProperty("TColorParametersAction.nbconfig"); //$NON-NLS-1$
		for(int i=0;i<nbconf;i++)
		{
			// pour chaque parametrage on recupere le premier caractere
			// qui correspond au nombre de canaux
			String conf = AGScan.prop.getProperty("TColorParametersAction.colors"+(i+1)); //$NON-NLS-1$
			System.out.println(conf);
			conf = conf.substring(0,1);
			
			// si ce nombre est egal a celui saisi par l utilisateur alors on le renvoie
			if (Integer.parseInt(conf)==nb)iconf = i;
		}
		return iconf;
	}
	

	/**** ACCESSEURS AUX FICHIERS ****/
	public File getFile1(){
		return N_file[0];
	}
	public File getFile2(){
		return N_file[1];
	}
	public File getFile3(){
		return N_file[2];
	}

}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
