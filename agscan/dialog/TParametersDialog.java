/**
 * Creation - August 18, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 * @author rcathelin 
 * @see agscan.utils.TableParametersModel
 */

package agscan.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import agscan.Messages;
import agscan.AGScan;
import agscan.utils.TableParametersModel;

public class TParametersDialog extends JDialog implements ActionListener {
	
	private TableParametersModel model;
	private JTable table ;
	
	// DECLARATION DES COMPOSANTS
	
	private JPanel jPanel_1 = new JPanel();
	private JScrollPane scrollpane;
	private JPanel jPanel_choice = new JPanel();
	
	private JButton jButton_OK = new JButton(Messages.getString("TParametersDialog.0"));  //$NON-NLS-1$
	private JButton jButton_cancel = new JButton(Messages.getString("TParametersDialog.1"));   //$NON-NLS-1$
	private JButton jButton_init = new JButton(Messages.getString("TParametersDialog.2"));   //$NON-NLS-1$
	
	public TParametersDialog() {
		this.setTitle(Messages.getString("TParametersDialog.3")); //$NON-NLS-1$
		init();
		pack();
		//this.setSize(800,600);
	}
	private void init() {
		
		// CONSTRUCTION DES COMPOSANTS //
		
		// création de la table
		model = new TableParametersModel();
		table = new JTable(model);	
		//adapter la taille des colonnes
		displayTableProperties();
		scrollpane = new JScrollPane(table);
		
		// AFFECTATION DES LISTENERS
		
		jButton_cancel.addActionListener(this);
		jButton_OK.addActionListener(this);
		jButton_init.addActionListener(this);
		
		//ASSEMBLAGE DES COMPOSANTS
		
		jPanel_1.add(scrollpane);
		
		jPanel_choice.add(jButton_OK);
		jPanel_choice.add(jButton_cancel);
		jPanel_choice.add(jButton_init);
		jPanel_choice.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TParametersDialog.4"))); //$NON-NLS-1$
		
		
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.getContentPane().add(jPanel_1);
		this.getContentPane().add(jPanel_choice);
		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == jButton_cancel){
			dispose();
		}
		if (e.getSource() == jButton_OK){			
			for (int i=0;i<table.getModel().getRowCount();i++){
				model.fireTableCellUpdated(i,1);//dire que la cellule a bougé, necessaire?
				// si la valeur de la table differe de celle de prop
				if (model.getValueAt(i,1)!=(AGScan.prop.get(table.getValueAt(i,0)))){
					if (model.getValueAt(i,1).toString().equalsIgnoreCase("")) { //$NON-NLS-1$
						//	si rien dans la case alors qu'avant il y avait une pté définie par l'user, on remet la valeur defaut...
						//System.out.println("en " +i+" different et vide!");
						AGScan.prop.setProperty((String)table.getValueAt(i,0),AGScan.defaultProp.getProperty((String)table.getValueAt(i,0)));
					}
					else{
						//	si qqchose dans la case different a la val enregistrée alors on change la valeur de cette propriété
						//System.out.println("en "+i+" different mais pas vide!");
						AGScan.prop.setProperty((String)table.getValueAt(i,0),(String)model.getValueAt(i,1));
					}
				}
			}
			
			dispose();
		}
		if (e.getSource() == jButton_init){
			// on ne peut pas faire un clear direct car il y a des infos comme la langue a garder
			// soit on sauve la langue, on efface le reste et on la remets
			// soit on efface toutes les valeurs dans prop qui ont des clés qu'a aussi DefaultProperties
			// (mieux, plus propre et plus souple...donc c'est ca qui est fait		
			for (Enumeration en = AGScan.defaultProp.keys() ; en.hasMoreElements() ;) {
				String defKey = (String)en.nextElement();
				if (AGScan.prop.containsKey(defKey))
					AGScan.prop.remove(defKey);
			}
			model.updateModel();
			table.repaint();
			displayTableProperties();			
		}		
	}
	
	// cette classe gere les options d'affichage à l'ecran de la table (largeur des colonnes...)
	private void displayTableProperties(){
		TableColumn column = null;
		for (int i = 0; i < 3; i++) {
			column = table.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(400); //parameters column is bigger
			} else {
				column.setPreferredWidth(80);
			}
		}
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setPreferredScrollableViewportSize(new Dimension(560,400));
	}
}



/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
