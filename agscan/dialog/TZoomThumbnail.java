package agscan.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;

public class TZoomThumbnail extends JPanel implements MouseListener, MouseMotionListener {
  public static final int VERTICAL = 1;
  public static final int HORIZONTAL = 2;

  private int width, height;
  private BufferedImage thumb;
  private Rectangle rect;
  private int x0 = -1, y0 = -1;
  private TZoomPanel parent;

  public TZoomThumbnail(TZoomPanel zp) {
    width = height = 0;
    thumb = null;
    parent = zp;
    rect = null;
    addMouseListener(this);
    addMouseMotionListener(this);
  }
  public void init(TDataElement element) {
    if (element != null) {
    		System.out.println("appel Snapshot 1");
      thumb = element.getView().getGraphicPanel().getSnapshot(300);
      if (thumb != null) {
        width = thumb.getWidth();
        height = thumb.getHeight();
        Rectangle visibleRect = element.getView().getGraphicScrollPane().getViewport().getViewRect();
        int x = visibleRect.x * thumb.getWidth() / (int) element.getView().getGraphicPanel().getPreferredSize().getWidth();
        int y = visibleRect.y * thumb.getHeight() / (int) element.getView().getGraphicPanel().getPreferredSize().getHeight();
        int w = visibleRect.width * thumb.getWidth() / (int) element.getView().getGraphicPanel().getPreferredSize().getWidth();
        int h = visibleRect.height * thumb.getHeight() / (int) element.getView().getGraphicPanel().getPreferredSize().getHeight();
        if (w > (thumb.getWidth() - 1)) w = thumb.getWidth() - 1;
        if (h > (thumb.getHeight() - 1)) h = thumb.getHeight() - 1;
        rect = new Rectangle(x, y, w, h);
      }
      else
        width = height = 300;
    }
    else {
      thumb = null;
      width = height = 300;
    }
    revalidate();
    repaint();
  }
  public void paintComponent(Graphics g) {
    if (thumb == null) {
      g.setColor(Color.red);
      g.drawRect(0, 0, width - 1, height - 1);
      g.drawLine(0, 0, width - 1, height - 1);
      g.drawLine(width - 1, 0, 0, height - 1);
    }
    else {
      g.drawImage(thumb, 0, 0, null);
      g.setColor(Color.magenta);
      if (rect != null) g.drawRect(rect.x, rect.y, rect.width, rect.height);
    }
  }

  public Dimension getPreferredSize() { return new Dimension(width, height); }

  public void mouseClicked(MouseEvent e) { }
  public void mouseEntered(MouseEvent e) { }
  public void mouseExited(MouseEvent e) { }
  public void mousePressed(MouseEvent e) {
    if (rect != null)
      if (rect.contains(e.getX(), e.getY())) {
        x0 = e.getX();
        y0 = e.getY();
      }
  }
  public void mouseReleased(MouseEvent e) {
    x0 = y0 = -1;
  }
  public void mouseDragged(MouseEvent e) {
    if ((rect != null) && (x0 != -1) && (y0 != -1)) {
      rect.x += (e.getX() - x0);
      rect.y += (e.getY() - y0);
      if (rect.x < 0) rect.x = 0;
      if (rect.y < 0) rect.y = 0;
      if (rect.x > (getThumbWidth() - rect.width - 1)) rect.x = getThumbWidth() - rect.width - 1;
      if (rect.y > (getThumbHeight() - rect.height - 1)) rect.y = getThumbHeight() - rect.height - 1;
      x0 = e.getX();
      y0 = e.getY();
      repaint();
      TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.VIEWPORT_POSITION, rect, new Dimension(thumb.getWidth(), thumb.getHeight()));
      TEventHandler.handleMessage(event);
    }
  }
  public void mouseMoved(MouseEvent e) { }
  public int getWidth() { return width; }
  public int getHeight() { return height; }
  public int getThumbWidth() {
    if (thumb != null) return thumb.getWidth();
    return 0;
  }
  public int getThumbHeight() {
    if (thumb != null) return thumb.getHeight();
    return 0;
  }
  public Rectangle getRect() {
    return rect;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
