package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import agscan.Messages;
import agscan.data.element.TDataElement;

public class TZoomDialog extends TDialog {
  private static TDialog instance;
  private TZoomPanel zoomPanel;
  private JButton closeButton;

  protected TZoomDialog(TDataElement element) {
    setTitle(Messages.getString("TZoomDialog.0")); //$NON-NLS-1$
    setResizable(false);
    zoomPanel = new TZoomPanel(element);
    closeButton = new JButton(Messages.getString("TZoomDialog.1")); //$NON-NLS-1$
    closeButton.setMinimumSize(new Dimension(60, 20));
    closeButton.setPreferredSize(new Dimension(60, 20));
    closeButton.setMargin(new Insets(2, 2, 2, 2));
    closeButton.setFont(new java.awt.Font("Dialog", 1, 11)); //$NON-NLS-1$
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        notifyClosing();
        dispose();
      }
    });
    JPanel southPanel = new JPanel();
    southPanel.add(closeButton, null);
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(zoomPanel, BorderLayout.CENTER);
    getContentPane().add(southPanel, BorderLayout.SOUTH);
    pack();
  }
  public void init(TDataElement element) {
    zoomPanel.init(element);
    pack();
  }
  public static TDialog getInstance(TDataElement element) {
    if (instance == null)
      instance = new TZoomDialog(element);
    else
      instance.init(element);
    return instance;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
