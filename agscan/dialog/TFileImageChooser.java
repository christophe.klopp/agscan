/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */
package agscan.dialog;

import java.util.Enumeration;

import javax.swing.JFileChooser;

import agscan.AGScan;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.plugins.TPluginManager;


// classe qui etend JFileChooser, juste le constructeur differe pour y mettre les extensions connues 
public class TFileImageChooser extends JFileChooser {
	private int nbImages ;
    public TFileImageChooser() {
        super(AGScan.prop.getProperty("imagesPath"));//modified 2005/11/29; now we call the parent class with a current directory

        //creation of filters for the chooser dialog
        addChoosableFileFilter(new ImageFilter("tif",
                Messages.getString("TFileImageChooser.1"))); //$NON-NLS-1$ //$NON-NLS-2$

        // the exts array contains all extensions supported = tif + plugins extensions 
        String[] exts = new String[TPluginManager.getExtensionsIntoArray().length +
            1];
        exts[0] = "tif"; //$NON-NLS-1$

        int i = 0;
        
        for (Enumeration enume = TPluginManager.extensions.keys();
        	enume.hasMoreElements();) {
        	i++;
        	String current = (String) enume.nextElement();
        	System.out.println(i+"current=" + current); //$NON-NLS-1$
        	exts[i] = current;
        	addChoosableFileFilter(new ImageFilter(current,
        			current.toUpperCase() + " images (*." + current + ")")); //$NON-NLS-1$ //$NON-NLS-2$
        }
        //setFileFilter(new ImageFilter(exts,
        //        Messages.getString("TFileImageChooser.6"))); //$NON-NLS-1$
        System.out.println("nb extensions = "+exts.length+" i= "+i);
        String[] exts2 = new String[i+1];
        for (int k = 0; k < i+1; k++){exts2[k]=exts[k];}
        setFileFilter(new ImageFilter(exts2, "images"));
        // CK add multiple file selection for mono channel images 07/04/2008
        nbImages = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
        if (nbImages == 1){
        	setMultiSelectionEnabled(true);
        }
        
        //setFileFilter(new ImageFilter(new String[] {"inf", "tif"}, "images"));
    }
}
