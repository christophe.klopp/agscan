/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import agscan.FenetrePrincipale;
import agscan.Messages;

public class TURLEditDialog extends JDialog {
  BorderLayout borderLayout1 = new BorderLayout();
  JPanel centerPanel = new JPanel();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JLabel libelleLabel = new JLabel();
  JTextField libelleTextField = new JTextField();
  JLabel linkLabel = new JLabel();
  JTextField linkTextField = new JTextField();
  JPanel jPanel1 = new JPanel();
  JButton okButton = new JButton();
  JButton cancelButton = new JButton();

  private Vector result;

  public TURLEditDialog(Vector v) throws HeadlessException {
    super(FenetrePrincipale.application);
    try {
      jbInit();
      setModal(true);
      init(v);
      initListeners();
      pack();
      result = null;
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void init(Vector v) {
    libelleTextField.setText(v.firstElement().toString());
    linkTextField.setText(v.lastElement().toString());
  }
  private void jbInit() throws Exception {
    setTitle(Messages.getString("TURLEditDialog.0")); //$NON-NLS-1$
    setResizable(false);
    this.getContentPane().setLayout(borderLayout1);
    centerPanel.setLayout(gridBagLayout1);
    libelleLabel.setText(Messages.getString("TURLEditDialog.1")); //$NON-NLS-1$
    libelleTextField.setText(Messages.getString("TURLEditDialog.2")); //$NON-NLS-1$
    linkLabel.setText(Messages.getString("TURLEditDialog.3")); //$NON-NLS-1$
    linkTextField.setText(Messages.getString("TURLEditDialog.4")); //$NON-NLS-1$
    okButton.setPreferredSize(new Dimension(49, 21));
    okButton.setText(Messages.getString("TURLEditDialog.5")); //$NON-NLS-1$
    cancelButton.setPreferredSize(new Dimension(73, 21));
    cancelButton.setText(Messages.getString("TURLEditDialog.6")); //$NON-NLS-1$
    this.getContentPane().add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(libelleLabel,      new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
    centerPanel.add(libelleTextField,        new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 100, 0));
    centerPanel.add(linkLabel,     new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    centerPanel.add(linkTextField,       new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 100, 0));
    this.getContentPane().add(jPanel1,  BorderLayout.SOUTH);
    jPanel1.add(okButton, null);
    jPanel1.add(cancelButton, null);
  }
  public Vector getResult() { return result; }
  private void initListeners() {
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        result = new Vector();
        result.addElement(libelleTextField.getText());
        result.addElement(linkTextField.getText());
        dispose();
      }
    });
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        dispose();
      }
    });
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
