package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import agscan.Messages;
import agscan.data.model.grid.TGridConfig;
import agscan.data.element.grid.TGrid;

public class TGridPropertiesPanel extends JPanel {
  private JTextField nbLevelsTextField = new JTextField();
  private JTextField nbSpotsTextField = new JTextField();
  private JTextField formatTextField = new JTextField();
  private JTextField sizeTextField = new JTextField();
  private JTextField spotTextField = new JTextField();
  private JTextField diameterTextField = new JTextField();

  public TGridPropertiesPanel() {
    setLayout(new BorderLayout());
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new GridBagLayout());
    JLabel nbLevelsLabel = new JLabel(Messages.getString("TGridPropertiesPanel.0")); //$NON-NLS-1$
    nbLevelsTextField.setBackground(SystemColor.inactiveCaptionText);
    nbLevelsTextField.setBorder(BorderFactory.createEtchedBorder());
    nbLevelsTextField.setEditable(false);
    JLabel nbSpotsLabel = new JLabel(Messages.getString("TGridPropertiesPanel.1")); //$NON-NLS-1$
    nbSpotsTextField.setBackground(SystemColor.inactiveCaptionText);
    nbSpotsTextField.setBorder(BorderFactory.createEtchedBorder());
    nbSpotsTextField.setEditable(false);
    JLabel formatLabel = new JLabel(Messages.getString("TGridPropertiesPanel.2")); //$NON-NLS-1$
    formatTextField.setBackground(SystemColor.inactiveCaptionText);
    formatTextField.setBorder(BorderFactory.createEtchedBorder());
    formatTextField.setEditable(false);
    JLabel sizeLabel = new JLabel(Messages.getString("TGridPropertiesPanel.3")); //$NON-NLS-1$
    sizeTextField.setBackground(SystemColor.inactiveCaptionText);
    sizeTextField.setBorder(BorderFactory.createEtchedBorder());
    sizeTextField.setEditable(false);
    JLabel spotLabel = new JLabel(Messages.getString("TGridPropertiesPanel.4")); //$NON-NLS-1$
    spotTextField.setBackground(SystemColor.inactiveCaptionText);
    spotTextField.setBorder(BorderFactory.createEtchedBorder());
    spotTextField.setEditable(false);
    JLabel diameterLabel = new JLabel(Messages.getString("TGridPropertiesPanel.5")); //$NON-NLS-1$
    diameterTextField.setBackground(SystemColor.inactiveCaptionText);
    diameterTextField.setBorder(BorderFactory.createEtchedBorder());
    diameterTextField.setEditable(false);
    add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(nbLevelsLabel,      new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
    centerPanel.add(nbSpotsLabel,    new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    centerPanel.add(nbSpotsTextField,     new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    centerPanel.add(formatLabel,      new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    centerPanel.add(formatTextField,    new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    centerPanel.add(nbLevelsTextField,    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 5, 0, 10), 0, 0));
    centerPanel.add(sizeLabel,    new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    centerPanel.add(sizeTextField,    new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    centerPanel.add(spotLabel,   new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    centerPanel.add(spotTextField,   new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 10), 0, 0));
    centerPanel.add(diameterLabel,  new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 10, 0), 0, 0));
    centerPanel.add(diameterTextField,   new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 10, 10), 0, 0));
  }
  public void init(TGrid grid) {
    if (grid == null) {
      nbLevelsTextField.setText(""); //$NON-NLS-1$
      nbSpotsTextField.setText(""); //$NON-NLS-1$
      formatTextField.setText(""); //$NON-NLS-1$
      sizeTextField.setText(""); //$NON-NLS-1$
      spotTextField.setText(""); //$NON-NLS-1$
      diameterTextField.setText(""); //$NON-NLS-1$
    }
    else {
      TGridConfig config = grid.getGridModel().getConfig();
      nbLevelsTextField.setText(" " + String.valueOf(config.getNbLevels())); //$NON-NLS-1$
      int nbCols = config.getLev1NbCols() * config.getLev2NbCols();// * config.getLev3NbCols();// level3 removed - 20005/10/28
      int nbRows = config.getLev1NbRows() * config.getLev2NbRows();// * config.getLev3NbRows();// level3 removed - 20005/10/28
      nbSpotsTextField.setText(" " + String.valueOf(nbRows) + " x " + String.valueOf(nbCols) + " = " + String.valueOf(nbCols * nbRows)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      String s = " (" + String.valueOf(config.getLev1NbRows()) + " x " + String.valueOf(config.getLev1NbCols()) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      if (config.getNbLevels() > 1)
        s += " (" + String.valueOf(config.getLev2NbRows()) + " x " + String.valueOf(config.getLev2NbCols()) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      //if (config.getNbLevels() > 2)  s += " (" + String.valueOf(config.getLev3NbRows()) + " x " + String.valueOf(config.getLev3NbCols()) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$// level3 removed - 20005/10/28
      formatTextField.setText(s);
      sizeTextField.setText(" " + String.valueOf(grid.getGridModel().getWidth()) + " x " + String.valueOf(grid.getGridModel().getHeight())); //$NON-NLS-1$ //$NON-NLS-2$
      spotTextField.setText(" " + String.valueOf(config.getSpotsWidth()) + " x " + String.valueOf(config.getSpotsHeight())); //$NON-NLS-1$ //$NON-NLS-2$
      diameterTextField.setText(" " + String.valueOf(config.getSpotsDiam())); //$NON-NLS-1$
    }
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
