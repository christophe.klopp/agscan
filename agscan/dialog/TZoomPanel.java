package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TRoiAction;
import agscan.menu.action.TZoomAction;

public class TZoomPanel extends JPanel {
  private JPanel controlPanel = new JPanel();
  private JPanel zoomCursorPanel = new JPanel();
  private JSlider zoomSlider = new JSlider(JSlider.HORIZONTAL, -18, 20, 1);
  private JLabel zoomLabel = new JLabel();
  private TZoomThumbnail zoomThumbnail;
  private JCheckBox enableMouseCheckBox;
  private TDataElement element;
  private ChangeListener zoomSliderChangeListener, enableMouseCheckBoxChangeListener;

  public TZoomPanel(TDataElement element) {
    try {
      jbInit();
      setListeners();
      init(element);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit() throws Exception {
    this.setBorder(BorderFactory.createEtchedBorder());
    zoomLabel.setPreferredSize(new Dimension(60, 21));
    zoomLabel.setHorizontalAlignment(SwingConstants.CENTER);
    zoomLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    zoomCursorPanel.add(zoomSlider, null);
    zoomCursorPanel.add(zoomLabel, null);
    enableMouseCheckBox = new JCheckBox(Messages.getString("TZoomPanel.0")); //$NON-NLS-1$
    zoomThumbnail = new TZoomThumbnail(this);
    controlPanel.setLayout(new GridBagLayout());
    controlPanel.add(zoomThumbnail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    controlPanel.add(zoomCursorPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    controlPanel.add(enableMouseCheckBox, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    setLayout(new BorderLayout());
    zoomLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black, 2), BorderFactory.createEmptyBorder(0,5,0,5)));
    zoomLabel.setText("1"); //$NON-NLS-1$
    zoomCursorPanel.setBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)));
    zoomSlider.setPreferredSize(new Dimension(128, 24));
    zoomSlider.setSnapToTicks(true);
    add(controlPanel, BorderLayout.CENTER);
  }
  public void init(TDataElement element) {
    this.element = element;
    if (element != null) {
      disableListeners();
      zoomSlider.setEnabled(true);
      enableMouseCheckBox.setEnabled(true);
      int z = element.getView().getGraphicPanel().getZoom();
      zoomSlider.setValue((z > 0) ? z : z + 2);
      zoomLabel.setText(String.valueOf(z));
      TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
      boolean iz = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      enableMouseCheckBox.setSelected(iz);
      enableListeners();
    }
    else {
      disableListeners();
      zoomSlider.setEnabled(false);
      enableMouseCheckBox.setEnabled(false);
      zoomSlider.setValue(1);
      enableMouseCheckBox.setSelected(false);
      enableListeners();
    }
    zoomThumbnail.init(element);
    revalidate();
    repaint();
  }
  private void setListeners() {
    zoomSliderChangeListener = new ChangeListener() {
      public void stateChanged(ChangeEvent ev) {
        int z = zoomSlider.getValue();
        zoomLabel.setText((z > 0) ? String.valueOf(z) : "1 / " + String.valueOf(2 - z)); //$NON-NLS-1$
        TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.CENTERED_ZOOM, new Integer((z > 0) ? z : z - 2));
        TEventHandler.handleMessage(event);
        zoomThumbnail.init(element);
        revalidate();
        repaint();
      }
    };
    enableMouseCheckBoxChangeListener = new ChangeListener() {
      public void stateChanged(ChangeEvent ev) {
        boolean b = enableMouseCheckBox.isSelected();
        //TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TMenuManager.ZOOM), new Boolean(b));
        TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TZoomAction.getID()), new Boolean(b));
        TEventHandler.handleMessage(event);
        event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ZOOM_MODE, new Boolean(b));
        TEventHandler.handleMessage(event);
        event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TRoiAction.getID()), new Boolean(false));
        TEventHandler.handleMessage(event);
      }
    };
  }
  public TZoomThumbnail getThumbnail() { return zoomThumbnail; }
  public void enableListeners() {
    zoomSlider.addChangeListener(zoomSliderChangeListener);
    enableMouseCheckBox.addChangeListener(enableMouseCheckBoxChangeListener);
  }
  public void disableListeners() {
    zoomSlider.removeChangeListener(zoomSliderChangeListener);
    enableMouseCheckBox.removeChangeListener(enableMouseCheckBoxChangeListener);
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
