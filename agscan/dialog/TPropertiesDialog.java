package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import agscan.Messages;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.element.grid.TGrid;

public class TPropertiesDialog extends TDialog {
  private JButton okButton = new JButton();
  private TImagePropertiesPanel imagePropertiesPanel;
  private TGridPropertiesPanel gridPropertiesPanel;
  private JPanel panel = new JPanel();
  private JPanel southPanel = new JPanel();
  private static TDialog instance;

  protected TPropertiesDialog() {
    super();
    imagePropertiesPanel = new TImagePropertiesPanel();
    gridPropertiesPanel = new TGridPropertiesPanel();
    init(null);
  }
  protected TPropertiesDialog(TDataElement element) {
    super();
    try {
      imagePropertiesPanel = new TImagePropertiesPanel();
      gridPropertiesPanel = new TGridPropertiesPanel();
      jbInit();
      init(element);
      initListeners();
      pack();  	
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  public void init(TDataElement element) {
    if (element == null) {
      imagePropertiesPanel.init(null);
      gridPropertiesPanel.init(null);
    }
    else if (element instanceof TImage) {
      setTitle(Messages.getString("TPropertiesDialog.0")); //$NON-NLS-1$
      imagePropertiesPanel.init((TImage)element);
      imagePropertiesPanel.setBorder(null);
      panel.removeAll();
      panel.add(imagePropertiesPanel, BorderLayout.CENTER);
      panel.add(southPanel, BorderLayout.SOUTH);
    }
    else if (element instanceof TGrid) {
      setTitle(Messages.getString("TPropertiesDialog.1")); //$NON-NLS-1$
      gridPropertiesPanel.init((TGrid)element);
      gridPropertiesPanel.setBorder(null);
      panel.removeAll();
      panel.add(gridPropertiesPanel, BorderLayout.CENTER);
      panel.add(southPanel, BorderLayout.SOUTH);
    }
    else if (element instanceof TAlignment) {
      setTitle(Messages.getString("TPropertiesDialog.2")); //$NON-NLS-1$
      imagePropertiesPanel.init(((TAlignment)element).getImage());
      imagePropertiesPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)) , Messages.getString("TPropertiesDialog.3"), //$NON-NLS-1$
          TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.PLAIN, 12), Color.black)); //$NON-NLS-1$
      gridPropertiesPanel.init((TGrid)((TAlignment)element).getGridModel().getReference());
      gridPropertiesPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)) , Messages.getString("TPropertiesDialog.5"), //$NON-NLS-1$
          TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.PLAIN, 12), Color.black)); //$NON-NLS-1$
      panel.removeAll();
      panel.add(imagePropertiesPanel, BorderLayout.NORTH);
      panel.add(gridPropertiesPanel, BorderLayout.CENTER);
      panel.add(southPanel, BorderLayout.SOUTH);
    }
    pack();
  }
  private void jbInit() throws Exception {
    setModal(true);
    setResizable(false);
    okButton.setPreferredSize(new Dimension(25, 20));
    okButton.setMargin(new Insets(2, 2, 2, 2));
    okButton.setFont(new Font("Dialog", 1, 11)); //$NON-NLS-1$
    okButton.setText(Messages.getString("TPropertiesDialog.8")); //$NON-NLS-1$

    panel.setLayout(new BorderLayout());
    this.getContentPane().add(panel, BorderLayout.CENTER);
    southPanel.add(okButton, null);
  }
  private void initListeners() {
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        notifyClosing();
        dispose();
      }
    });
  }
  public static TDialog getInstance(TDataElement element) {
    if (instance == null)
      instance = new TPropertiesDialog(element);
    else
      instance.init(element);
    return instance;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
