package agscan.dialog;
//2005/12/06: add of the case ERROR_MESSAGE
//TODO it should be well to choose the title of windows for cases ERROR_DIALOG and MESSAGE_DIALOG 


import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;

public class TDialogManager implements TBZMessageListener {
	//les identifiants d'action negatifs sont des actions propres à la classe
	public static final int UPDATE = -1;
	public static final int CLOSE = -2;
	public static final int HIDE_ALL = -3;
	public static final int SHOW_ALL = -4;
	public static final int CLOSE_ALL = -5;
	public static final int MESSAGE_DIALOG = -6;//message traite dans la classe.//MENU TEST REMI
	public static final int ERROR_DIALOG = -7;//added 2005/12/06
	// ces identifiants sont des identifiants de boites de dialogues differentes...
	public static final int CONTRAST_AND_BRIGHTNESS = 1;
	public static final int PROPERTIES = 2;
	public static final int ZOOM = 3;
	public static final int NEW_GRID = 4;
	public static final int ADD_COLUMNS = 5;
	public static final int GRID_POSITIONING = 6;
	public static final int QUANTIF_SELECTION = 7;//ajout integration 14/09/05
	
	
	private Vector openedDialogs;
	
	
	public TDialogManager() {
		openedDialogs = new Vector();
	}
	
	
	// cette methode gere les evenements:
	// si il est negatif (action propre) il fait le traitement ci-dessous
	// (sinon = default) = si il est positif, il affiche alors le dialog correspondant au numero
	public Object[] BZMessageResponse(TEvent event) {
		int action = event.getAction();
		TDialog dial;
		switch (action) {
		case UPDATE:
			TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
			TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
			//TDataElement element = (TDataElement)event.getParam(0);
			for (Enumeration enume = openedDialogs.elements(); enume.hasMoreElements(); ) {
				dial = (TDialog)enume.nextElement();
				if (dial != event.getParam(1)) dial.init(element);
			}
			break;
		case CLOSE:
			TDialog dialog = (TDialog)event.getParam(0);
			openedDialogs.remove(dialog);
			break;
		case CLOSE_ALL:
			for (Enumeration enume = openedDialogs.elements(); enume.hasMoreElements(); )
				((TDialog)enume.nextElement()).dispose();
			break;
		case HIDE_ALL :
			for (Enumeration enume = openedDialogs.elements(); enume.hasMoreElements(); )
				((TDialog)enume.nextElement()).setVisible(false);
			break;
		case SHOW_ALL :
			for (Enumeration enume = openedDialogs.elements(); enume.hasMoreElements(); )
				((TDialog)enume.nextElement()).setVisible(true);
			break;
		case MESSAGE_DIALOG:
			JOptionPane.showMessageDialog(null, (String)event.getParam(0),"MESSAGE",JOptionPane.INFORMATION_MESSAGE);
			break;
		case ERROR_DIALOG:
			JOptionPane.showMessageDialog(null, (String)event.getParam(0),"ERROR",JOptionPane.ERROR_MESSAGE);
			break;
		default:
			openDialog(action, (TDataElement)event.getParam(0));
		break;
		}
		return null;
	}
	
	// affichage d'une boite de dialogue non modale
	private void openDialog(int type, TDataElement element) {
		TDialog dialog;
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		switch (type) {
		/* 
		 // 18/08/05:suppression du code ci-dessous supposé inutilisé après avvoir supprimé les 2 classes:
		  // agscan.dialog.TcontrastAndBrightnessDialog.java
		   // et agscan.dialog.histogram.THistogramComponent.java
		    
		    case CONTRAST_AND_BRIGHTNESS:
		    dialog = TContrastAndBrightnessDialog.getInstance(element);
		    if (!openedDialogs.contains(dialog)) {
		    openedDialogs.addElement(dialog);
		    dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
		    dialog.setModal(false);
		    dialog.setVisible(true);
		    }
		    break;
		    */
		case PROPERTIES:
			dialog = TPropertiesDialog.getInstance(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(false);
				dialog.setVisible(true);
			}
			break;
		case ZOOM:
			dialog = TZoomDialog.getInstance(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(false);
				dialog.setVisible(true);
			}
			break;
		case NEW_GRID:
			dialog = TNewGridDialog.getInstance(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(false);
				dialog.setVisible(true);
			}
			break;
		case ADD_COLUMNS:
			dialog = new TAddColumnsDialog(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(false);
				dialog.setVisible(true);
			}
			break;
		case GRID_POSITIONING:
			dialog = TGridPositioningDialog.getInstance(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(false);
				dialog.setVisible(true);
			}
			break;
			//ajout integration 14/09/05
		case QUANTIF_SELECTION:
			dialog = TQuantifSelectionDialog.getInstance(element);
			if (!openedDialogs.contains(dialog)) {
				openedDialogs.addElement(dialog);
				dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
				dialog.setModal(true);
				dialog.setVisible(true);
			}
			break;
		}
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
