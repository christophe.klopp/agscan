package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.event.TEvent;

public class TColorParametersDialog extends JDialog implements ActionListener {
	
	// nombre de configurations voulues
	private int nbconfig;
	
	// indique si un champ de texte n'a pas ete rempli
	private boolean champvide;
	
	// composants de la fenetre
	private JPanel globpan;
	private JPanel colorpan;
	private JPanel[] Ncolorpan;
	private JTextField[] tf_color;
	private JPanel ctrlpan ;
	private JButton b_ok;  
	private JButton b_cancel;  
	private JPanel nbpan;
	private JButton b_valnb;  
	private JTextField tf_nb;
	
	public TColorParametersDialog()
	{
		// titre
		this.setTitle(Messages.getString("TColorParametersDialog.0")); //$NON-NLS-1$
		
		// initialisations
		init();
		
		// compactage de la fenetre
		pack();
	}
	
	private void init() 
	{	
		// recuperation de la valeur nbconfig dans le fichier paramatre
		nbconfig = AGScan.prop.getIntProperty("TColorParametersAction.nbconfig"); //$NON-NLS-1$
		// instanciation d'un nouveau panel pour le choix des couleurs
		colorpan = new JPanel();
		// on met une bordure sur le panel avec un titre 
		colorpan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TColorParametersDialog.1"))); //$NON-NLS-1$
		// configuration BoxLayout : les composants sont ajout�s verticalement
		colorpan.setLayout(new BoxLayout(colorpan,BoxLayout.Y_AXIS));
		// instanciations de 2 tableaux : 
		// ==> panels,champs de texte pour le choix des couleurs
		Ncolorpan = new JPanel[nbconfig];
		tf_color = new JTextField[nbconfig];
		
		// instanciations des composants dont le nombre est g�n�rique
		for(int i=0;i<nbconfig;i++)
		{
			// les panels choix des couleurs : 1 par parametrage ...
			Ncolorpan[i] = new JPanel();
			// on met une bordure sur le panel avec un titre 
			Ncolorpan[i].setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TColorParametersDialog.2")+(i+1))); //$NON-NLS-1$
			// ... avec une configuration FlowLayout
			// pour inserer label et champ de texte
			Ncolorpan[i].setLayout(new FlowLayout());
			// les champs de textes choix des couleurs : 1 par parametrage
			tf_color[i] = new JTextField(AGScan.prop.getProperty("TColorParametersAction.colors"+(i+1)),25); //$NON-NLS-1$
			// ajout des champs de texte aux panels (2e colonne)
			Ncolorpan[i].add(tf_color[i]);
			// ajout des panels au panel plus general de choix des couleurs (panel p�re)
			colorpan.add(Ncolorpan[i]);
			
		}
		// instanciations des 3 boutons et mise sur ecoute
		b_ok = new JButton(Messages.getString("TColorParametersDialog.4"));  //$NON-NLS-1$
		b_ok.addActionListener(this);
		b_cancel = new JButton(Messages.getString("TColorParametersDialog.5")); //$NON-NLS-1$
		b_cancel.addActionListener(this);
		b_valnb = new JButton(Messages.getString("TColorParametersDialog.6"));  //$NON-NLS-1$
		b_valnb.addActionListener(this);
		
		// instanciation d'un nouveau panel pour le choix du nombre de parametrage
		nbpan = new JPanel();
		// choix d'une configuration FlowLayout
		nbpan.setLayout(new FlowLayout());
		// on met une bordure sur le panel avec un titre 
		nbpan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TColorParametersDialog.7"))); //$NON-NLS-1$
		// instanciation d'un nouveau champ de texte pour le choix du nombre de parametrage
		tf_nb = new JTextField(""+AGScan.prop.getIntProperty(Messages.getString("TColorParametersDialog.9")),3); //$NON-NLS-1$ //$NON-NLS-2$
		// on rend le champ editable biensur
		tf_nb.setEditable(true);
		// on aligne le contenu du champ sur la droite car c'est un champ numerique
		tf_nb.setHorizontalAlignment(JTextField.RIGHT);
		// on ajoute le champ de texte au panel
		nbpan.add(tf_nb);
		// on ajoute le bouton de validation au panel
		nbpan.add(b_valnb);
		
		// instanciation d'un nouveau panel pour les boutons de validation ok/annuler
		ctrlpan = new JPanel();
		// on met une bordure sur le panel avec un titre 
		ctrlpan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.ORANGE),Messages.getString("TColorParametersDialog.10"))); //$NON-NLS-1$
		// choix d'une configuration FlowLayout 
		ctrlpan.setLayout(new FlowLayout());
		// on ajoute le bouton ok
		ctrlpan.add(b_ok);
		// on ajoute le bouton annuler
		ctrlpan.add(b_cancel);
		
		// instanciation d'un nouveau panel global
		globpan = new JPanel();
		// choix d'une configuration GridLayout : grille de 3 ligne et 1 seule colonne
		globpan.setLayout(new BorderLayout());
		// ajout du panel choix du nombre de parametrage
		globpan.add(nbpan,BorderLayout.NORTH);
		// ajout du panel choix des couleurs
		globpan.add(colorpan,BorderLayout.CENTER);
		// ajout du panel des boutons de validation
		globpan.add(ctrlpan,BorderLayout.SOUTH);
		
		// le panel global est associe a la fenetre
		this.setContentPane(globpan);
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		// si on clique sur ok
		if(e.getSource() == b_ok)
		{
			champvide = false;
		
			for(int i=0;i<nbconfig;i++)
			{
				// si les couleurs saisies sont differentes de celles que contient le fichier des parametres
				if (tf_color[i].getText() != AGScan.prop.getProperty(Messages.getString("TColorParametersDialog.11")+(i+1))) //$NON-NLS-1$
				{
					// on met a jour le fichier des parametres
					AGScan.prop.setProperty(Messages.getString("TColorParametersDialog.11")+(i+1),tf_color[i].getText()); 	 //$NON-NLS-1$
				}
				// si le nombre de parametrage saisi est different de celui que contient le fichier des parametres
				if (Integer.valueOf(tf_nb.getText()).intValue() != AGScan.prop.getIntProperty(Messages.getString("TColorParametersDialog.13"))) //$NON-NLS-1$
				{
					// on met a jour le fichier des parametres
					AGScan.prop.setProperty(Messages.getString("TColorParametersDialog.13"),tf_nb.getText()); 	 //$NON-NLS-1$
				}
				// si le champ n'est pas rempli !
				if(tf_color[i].getText()=="")champvide = true; //$NON-NLS-1$
				
			}
			// si tous les champs de texte ont bien �t� remplis
			if (!champvide) 
			{
				// on met a jour le fichier des parametres 
				// en mettant par d�faut le premier parametrage (T.ColorParametersAction.colors1)
				AGScan.prop.setProperty("TColorParametersAction.colors",AGScan.prop.getProperty("TColorParametersAction.colors1"));
				int nbcol = Integer.parseInt(AGScan.prop.getProperty("TColorParametersAction.colors1").substring(0,1)); 
				AGScan.prop.setProperty("TColorParametersAction.nbcolors",nbcol);
				
				// on peut quitter la fenetre
				dispose();
			}
			// sinon on signale une erreur  
			 	else{			
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,Messages.getString("TColorParametersDialog.8"));// externalized 2006/02/02  //$NON-NLS-1$
				TEventHandler.handleMessage(event);
				return;//permet de quitter cette methode =  sans fermer la fenetre
			}//$NON-NLS-1$*/
		}
		// si on clique sur annuler on quitte la fenetre sans rien faire
		else if (e.getSource() == b_cancel) dispose();
		// si on clique sur valider
		else if (e.getSource() == b_valnb)
		{
			int nb = Integer.valueOf(tf_nb.getText()).intValue();
			// si le nombre de parametrage saisi est bien compris entre 1 et 6 et,
			// si celui ci est different de celui du fichier des parametres
			if(		nb>Integer.valueOf(Messages.getString("TColorParametersDialog.16")).intValue() //$NON-NLS-1$
				&&	nb<Integer.valueOf(Messages.getString("TColorParametersDialog.17")).intValue() //$NON-NLS-1$
				&&  nb!=AGScan.prop.getIntProperty(Messages.getString("TColorParametersDialog.13"))) //$NON-NLS-1$
			{
				// on met a jour le fichier des parametres
				AGScan.prop.setProperty(Messages.getString("TColorParametersDialog.13"),nb); //$NON-NLS-1$
				// on reconstruit la fenetre
				init();
				// raffraichissement
				this.repaint();
				// recompactage
				this.pack();
			}
			//else throw ("Valeur incorrecte");
		}
	}
}
