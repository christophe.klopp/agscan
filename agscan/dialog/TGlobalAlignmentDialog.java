/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.event.TEvent;

public class TGlobalAlignmentDialog extends JDialog {
  private JButton applyButton, closeButton;
  private TGlobalAlignmentAlgorithm algoGlobalAlignment;

  public TGlobalAlignmentDialog(TGlobalAlignmentAlgorithm algo) throws HeadlessException {
    algoGlobalAlignment = algo;
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(algoGlobalAlignment.getControlPanel(), BorderLayout.CENTER);
    JPanel southPanel = new JPanel();
    applyButton = new JButton(Messages.getString("TGlobalAlignmentDialog.0")); //$NON-NLS-1$
    applyButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        algoGlobalAlignment.init();
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.GLOBAL_ALIGNMENT, null, algoGlobalAlignment);
        TEventHandler.handleMessage(ev);
        algoGlobalAlignment.setLasts();
        algoGlobalAlignment.saveInProperties();//add 17/08/05
        dispose();
      }
    });
    closeButton = new JButton(Messages.getString("TGlobalAlignmentDialog.1")); //$NON-NLS-1$
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        dispose();
      }
    });
    southPanel.add(applyButton);
    southPanel.add(closeButton);
    getContentPane().add(southPanel, BorderLayout.SOUTH);
    setResizable(false);
    setModal(true);
    setTitle(Messages.getString("TGlobalAlignmentDialog.2")); //$NON-NLS-1$
    getContentPane().doLayout();
    pack();
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
