/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import agscan.Messages;
import agscan.data.model.grid.table.TColumn;

public class TImportColumnsDialog extends JDialog {
  JPanel northPanel = new JPanel();
  JLabel northLabel = new JLabel();
  JPanel centerPanel = new JPanel() {
    public Insets getInsets() {
      return new Insets(10, 10, 10, 10);
    }
  };
  JPanel southPanel = new JPanel();
  JPanel columnsPanel = new JPanel();
  JCheckBox[] columnCheckBox;
  JComboBox[] columnComboBox;
  JButton importButton = new JButton();
  JButton cancelButton = new JButton();
  JCheckBox synchroCheckBox = new JCheckBox();
  private boolean canceled = true;

  public TImportColumnsDialog(Hashtable cn, boolean syn) throws HeadlessException {
    try {
      jbInit(cn, syn);
      initListeners();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  private void jbInit(Hashtable cn, boolean syn) throws Exception {
    setResizable(false);
    setTitle(Messages.getString("TImportColumnsDialog.0")); //$NON-NLS-1$
    Border border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
                       new Color(148, 145, 140)), BorderFactory.createEmptyBorder(10, 10, 10, 10));
    getContentPane().setLayout(new BorderLayout());
    northLabel.setText(Messages.getString("TImportColumnsDialog.1")); //$NON-NLS-1$
    centerPanel.setLayout(new GridBagLayout());
    centerPanel.setBorder(null);
    columnsPanel.setLayout(new GridBagLayout());
    columnCheckBox = new JCheckBox[cn.size()];
    columnComboBox = new JComboBox[cn.size()];
    Enumeration keys = cn.keys();
    for (int i = 0; i < cn.size(); i++) {
      columnCheckBox[i] = new JCheckBox((String)keys.nextElement());
      columnComboBox[i] = new JComboBox();
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_INTEGER]);
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_REAL]);
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_TEXT]);
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_BOOLEAN]);
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_PROBA]);
      columnComboBox[i].addItem(TColumn.columnType[TColumn.TYPE_URL]);
      if (i < (cn.size() - 1)) {
        columnsPanel.add(columnCheckBox[i], new GridBagConstraints(0, i, 1, 1, 0.0, 0.0
            , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 20), 0, 0));
        columnsPanel.add(new JLabel(Messages.getString("TImportColumnsDialog.2")), new GridBagConstraints(1, i, 1, 1, 0.0, 0.0 //$NON-NLS-1$
            , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 0, 10), 0, 0));
        columnsPanel.add(columnComboBox[i], new GridBagConstraints(2, i, 1, 1, 0.0, 0.0
            , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 0, 10), 0, 0));
      }
      else {
        columnsPanel.add(columnCheckBox[i], new GridBagConstraints(0, i, 1, 1, 0.0, 0.0
            , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 20), 0, 0));
        columnsPanel.add(new JLabel(Messages.getString("TImportColumnsDialog.3")), new GridBagConstraints(1, i, 1, 1, 0.0, 0.0 //$NON-NLS-1$
            , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 0, 10), 0, 0));
        columnsPanel.add(columnComboBox[i], new GridBagConstraints(2, i, 1, 1, 0.0, 0.0
            , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(10, 0, 10, 10), 0, 0));
      }
    }
    columnsPanel.setBorder(BorderFactory.createEtchedBorder());
    importButton.setPreferredSize(new Dimension(73, 21));
    importButton.setText(Messages.getString("TImportColumnsDialog.4")); //$NON-NLS-1$
    cancelButton.setPreferredSize(new Dimension(73, 21));
    cancelButton.setToolTipText(Messages.getString("TImportColumnsDialog.5")); //$NON-NLS-1$
    cancelButton.setText(Messages.getString("TImportColumnsDialog.6")); //$NON-NLS-1$
    synchroCheckBox.setText(Messages.getString("TImportColumnsDialog.7")); //$NON-NLS-1$
    getContentPane().add(northPanel, BorderLayout.NORTH);
    northPanel.add(northLabel, null);
    getContentPane().add(centerPanel, BorderLayout.CENTER);
    getContentPane().add(southPanel, BorderLayout.SOUTH);
    southPanel.add(importButton, null);
    centerPanel.add(columnsPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    if (syn)
      centerPanel.add(synchroCheckBox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
          ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 0, 10, 0), 0, 0));
    southPanel.add(cancelButton, null);
  }
  public void initListeners() {
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        dispose();
      }
    });
    importButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        canceled = false;
        dispose();
      }
    });
  }
  public boolean isCanceled() {
    return canceled;
  }
  public Object[] getSelectedColumns() {
    Vector v = new Vector();
    for (int i = 0; i < columnCheckBox.length; i++)
      if (columnCheckBox[i].isSelected())
        v.addElement(columnCheckBox[i].getText());
    return v.toArray();
  }
  public boolean isSynchro() {
    return synchroCheckBox.isSelected();
  }
  public Object[] getSelectedTypes() {
    Vector v = new Vector();
    for (int i = 0; i < columnCheckBox.length; i++)
      if (columnCheckBox[i].isSelected())
        v.addElement(new Integer(columnComboBox[i].getSelectedIndex()));
    return v.toArray();
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
