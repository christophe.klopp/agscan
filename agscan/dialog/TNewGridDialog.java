/**	
 * class TNewGridDialog.java
 * modified 2006/01/26: 
 * 	fixed bug#166: correction of display for inter-blocks values for re-opened grids
 * 	fixed bug#181: possible to define inter-row or inter-column space in micrometer.
 */

package agscan.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.text.DefaultFormatter;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridStructureControler;
import agscan.data.controler.TImageControler;
import agscan.data.element.TDataElement;
import agscan.data.model.grid.TGridConfig;
import agscan.event.TEvent;
import agscan.data.element.grid.TGrid;

public class TNewGridDialog extends TDialog {
	private JPanel mainPanel = new JPanel();
	private BorderLayout borderLayout1 = new BorderLayout();
	private JPanel centerPanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JButton closeButton = new JButton();
	private BoxLayout boxLayout21;
	private Border border1;
	private Border border3;
	private Border border4;
	private TitledBorder titledBorder5;
	private Border border5;
	private Border border6;
	private Border border7;
	private Border border8;
	private Border border9;
	private Border border10;
	private Border border11;
	private JTabbedPane jTabbedPane1 = new JTabbedPane();
	private JPanel level1LeftPanel = new JPanel();
	private JSpinner level1RowHeightSpinbox = new JSpinner(new SpinnerNumberModel(375.0D, 1.0D, 10000.0D, 5.0D));
	private JLabel level1NbRowsLabel = new JLabel();
	private JRadioButton level1SpotIDRCRadioButton = new JRadioButton();
	private JLabel level1StartElementLabel = new JLabel();
	private JLabel level1SpotIDLabel = new JLabel();
	private JSpinner level1NbColumnsSpinbox = new JSpinner(new SpinnerNumberModel(12, 1, 512, 1));
	private JSpinner level1SpotDiameterSpinbox = new JSpinner(new SpinnerNumberModel(250.0D, 1.0D, 10000.0D, 5.0D));
	private JPanel level1RightPanel = new JPanel();
	private JSpinner level1NbRowsSpinbox = new JSpinner(new SpinnerNumberModel(12, 1, 512, 1));
	private JPanel level1Panel = new JPanel();
	private BoxLayout boxLayout25;
	private JSpinner level1ColumnWidthSpinbox = new JSpinner(new SpinnerNumberModel(375.0D, 1.0D, 10000.0D, 5.0D));
	private JLabel levelRowHeightLabel = new JLabel();
	private JRadioButton level1SpotIDNumberRadioButton = new JRadioButton();
	private GridBagLayout gridBagLayout8 = new GridBagLayout();
	private GridBagLayout gridBagLayout9 = new GridBagLayout();
	private JComboBox level1StartElementComboBox = new JComboBox();
	private JLabel level1SpotDiameterLabel = new JLabel();
	private Component component4;
	private JLabel level2StartElementLabel = new JLabel();
	private JLabel level2SpotIDLabel = new JLabel();
	private JSpinner level2NbRowsSpinbox = new JSpinner(new SpinnerNumberModel(1, 1, 512, 1));
	private JSpinner level2RowSpacingSpinbox = new JSpinner(new SpinnerNumberModel(0.0D, 0.0D, 40000.0D, 1.0D));//06/01/26
	private JLabel level2RowSpacingLabel = new JLabel();
	private JLabel level2NbColumnsLabel = new JLabel();
	private JLabel level2ColumnSpacingLabel = new JLabel();
	private JComboBox level2StartElementComboBox = new JComboBox();
	private JRadioButton level2SpotIDNumberRadioButton = new JRadioButton();
	private JRadioButton level2SpotIDRCRadioButton = new JRadioButton();
	private JSpinner level2NbColumnsSpinbox = new JSpinner(new SpinnerNumberModel(1, 1, 512, 1));
	private JPanel level2LeftPanel = new JPanel();
	private BoxLayout boxLayout26;
	private GridBagLayout gridBagLayout10 = new GridBagLayout();
	private GridBagLayout gridBagLayout11 = new GridBagLayout();
	private JLabel level2NbRowsLabel = new JLabel();
	
	private JSpinner level2ColumnSpacingSpinbox = new JSpinner(new SpinnerNumberModel(0.0D, 0.0D,40000.0D, 1.0D));//06/01/26
	private JPanel level2RightPanel = new JPanel();
	private JPanel level2Panel = new JPanel();
	private Component component5;
	
	private JComboBox spotUnitChoiceComboBox= new JComboBox();//2006/01/26
	private JComboBox spotUnitChoice2ComboBox= new JComboBox();//2006/01/26
	private JLabel spotUnitChoiceLabel = new JLabel();//2006/01/26
	
	private JLabel level1NbColumnsLabel = new JLabel();
	private Border border12;
	private Border border13;
	private Border border14;
	private GridBagLayout gridBagLayout1 = new GridBagLayout();
	private Border border15;
	private Border border16;
	private Border border17;
	private JLabel level1ColumnWidthLabel = new JLabel();
	private Border border18;
	private TitledBorder titledBorder6;
	private JLabel nbLevelsLabel = new JLabel();
	private JSpinner nbLevelsSpinbox = new JSpinner(new SpinnerNumberModel(1, 1, 2, 1));//3==>2
	private JPanel nbLevelsPanel = new JPanel();
	private Border border2;
	
	private static TDialog instance;
	private ChangeListener level1ColumnWidthSpinboxChangeListener, level1RowHeightSpinboxChangeListener,
	nbLevelsSpinboxChangeListener, level1NbColumnsSpinboxChangeListener,
	level1NbRowsSpinboxChangeListener, level1SpotDiameterSpinboxChangeListener,
	level2NbColumnsSpinboxChangeListener, level2NbRowsSpinboxChangeListener,
	level2ColumnSpacingSpinboxChangeListener, level2RowSpacingSpinboxChangeListener;
	private ActionListener closeButtonActionListener, level1StartElementComboBoxActionListener,
	level2StartElementComboBoxActionListener,
	spotUnitChoiceComboBoxActionListener,
	spotUnitChoice2ComboBoxActionListener;//2006/01/26
	private ItemListener level1SpotIDNumberRadioButtonChangeListener, level2SpotIDNumberRadioButtonChangeListener;
	
	private MouseAdapter spinboxMouseListener;
	private KeyAdapter spinboxKeyListener;
	
	protected TNewGridDialog(TDataElement element) throws HeadlessException {
		super();
		try {
			jbInit();
			initListeners();
			init(element);
			pack();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public static TDialog getInstance(TDataElement element) {
		if (instance == null)
			instance = new TNewGridDialog(element);
		else
			instance.init(element);
		return instance;
	}
	public static TDialog getInstance() {
		return instance;
	}
	
	public void init(TDataElement element) {
		TGridConfig config;
		disableListeners();
		if (element instanceof TGrid) {
			setDialogEnabled(true);
			config = ((TGrid)element).getGridModel().getConfig();
			nbLevelsSpinbox.setValue(new Integer(config.getNbLevels()));
			jTabbedPane1.setEnabledAt(1, (config.getNbLevels() > 1));
			
			level1ColumnWidthSpinbox.setValue(new Double(config.getSpotsWidth()));
			level1NbColumnsSpinbox.setValue(new Integer(config.getLev1NbCols()));
			level1NbRowsSpinbox.setValue(new Integer(config.getLev1NbRows()));
			level1RowHeightSpinbox.setValue(new Double(config.getSpotsHeight()));
			level1SpotDiameterSpinbox.setValue(new Double(config.getSpotsDiam()));
			level1SpotIDNumberRadioButton.setSelected(config.getLev1ElementID() == TGridConfig.ID_NUMBER);
			level1SpotIDRCRadioButton.setSelected(config.getLev1ElementID() == TGridConfig.ID_ROW_COLUMN);
			level1StartElementComboBox.setSelectedItem(TGridConfig.getStartElementString(config.getLev1StartElement()));
			
			level2NbColumnsSpinbox.setValue(new Integer(config.getLev2NbCols()));
			level2NbRowsSpinbox.setValue(new Integer(config.getLev2NbRows()));
			
			//06/01/26 the following lines fix the bug of "0" in interbloc space when a grid is open.
			if (spotUnitChoiceComboBox.getSelectedItem().equals(new String("mcm")))
				level2ColumnSpacingSpinbox.setValue(new Double(config.getLev2XSpacing()));
			else level2ColumnSpacingSpinbox.setValue(new Double(micrometerToSpotInWidth(config.getLev2XSpacing())));
			if (spotUnitChoice2ComboBox.getSelectedItem().equals(new String("mcm")))
				level2RowSpacingSpinbox.setValue(new Double(config.getLev2YSpacing()));
			else level2RowSpacingSpinbox.setValue(new Double(micrometerToSpotInHeight(config.getLev2YSpacing())));
			
			// les 2 lignes qui suivent permettent que l'utilisateur ne saisisse pas n'importe quoi dans le spinner
			JFormattedTextField field=((JSpinner.DefaultEditor)level2ColumnSpacingSpinbox.getEditor()).getTextField();//added 18/05/05
			((DefaultFormatter)field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.//added 18/05/05
			//level2ColumnSpacingSpinbox.setValue(new Double(config.getLev2XSpacing()));//removed 2005/05/18
			JFormattedTextField field2=((JSpinner.DefaultEditor)level2RowSpacingSpinbox.getEditor()).getTextField();//added 05/05/18
			((DefaultFormatter)field2.getFormatter()).setAllowsInvalid(false);//disable invalid characters.//added  18/05/05
			//level2RowSpacingSpinbox.setValue(new Double(config.getLev2YSpacing()));//removed 2005/05/18
			level2SpotIDNumberRadioButton.setSelected(config.getLev2ElementID() == TGridConfig.ID_NUMBER);
			level2SpotIDRCRadioButton.setSelected(config.getLev2ElementID() == TGridConfig.ID_ROW_COLUMN);
			level2StartElementComboBox.setSelectedItem(TGridConfig.getStartElementString(config.getLev2StartElement()));
			//level3 removed - 20005/10/28
			
		}
		else {
			setDialogEnabled(false);
		}
		enableListeners();
	}
	private void jbInit() throws Exception {
		border2 = BorderFactory.createLoweredBevelBorder();
		setTitle(Messages.getString("TNewGridDialog.0")); //$NON-NLS-1$
		setResizable(false);
		boxLayout25 = new BoxLayout(level1Panel, BoxLayout.Y_AXIS);
		component4 = Box.createHorizontalStrut(8);
		boxLayout26 = new BoxLayout(level2Panel, BoxLayout.Y_AXIS);
		component5 = Box.createHorizontalStrut(8);
		titledBorder6 = new TitledBorder(border18,Messages.getString("TNewGridDialog.1")); //$NON-NLS-1$
		((JSpinner.NumberEditor)nbLevelsSpinbox.getEditor()).getTextField().setEditable(false);
		((JSpinner.NumberEditor)nbLevelsSpinbox.getEditor()).getTextField().setBackground(Color.white);
		border1 = BorderFactory.createEmptyBorder(5,5,5,5);
		border3 = BorderFactory.createCompoundBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border4 = new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(148, 145, 140));
		titledBorder5 = new TitledBorder(border4,Messages.getString("TNewGridDialog.2")); //$NON-NLS-1$
		border5 = BorderFactory.createCompoundBorder(titledBorder5,BorderFactory.createEmptyBorder(0,5,5,5));
		border6 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border7 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border8 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border9 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border10 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		border11 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),BorderFactory.createEmptyBorder(5,5,5,5));
		mainPanel.setLayout(borderLayout1);
		closeButton.setText(Messages.getString("TNewGridDialog.3")); //$NON-NLS-1$
		boxLayout21 = new BoxLayout(centerPanel, BoxLayout.Y_AXIS);
		centerPanel.setLayout(boxLayout21);
		centerPanel.setBorder(border1);
		level1LeftPanel.setLayout(gridBagLayout9);
		level1LeftPanel.setBorder(border6);
		level1RowHeightSpinbox.setFont(new java.awt.Font("Dialog", 0, 8)); //$NON-NLS-1$
		level1RowHeightSpinbox.setBorder(null);
		level1NbRowsLabel.setText(Messages.getString("TNewGridDialog.5")); //$NON-NLS-1$
		level1SpotIDRCRadioButton.setSelected(true);
		level1SpotIDRCRadioButton.setText(Messages.getString("TNewGridDialog.6")); //$NON-NLS-1$
		level1StartElementLabel.setText(Messages.getString("TNewGridDialog.7")); //$NON-NLS-1$
		level1SpotIDLabel.setText(Messages.getString("TNewGridDialog.8")); //$NON-NLS-1$
		level1NbColumnsSpinbox.setFont(new java.awt.Font("Dialog", 0, 8)); //$NON-NLS-1$
		level1NbColumnsSpinbox.setBorder(null);
		level1SpotDiameterSpinbox.setFont(new java.awt.Font("Dialog", 0, 8)); //$NON-NLS-1$
		level1SpotDiameterSpinbox.setBorder(null);
		level1RightPanel.setBorder(border7);
		level1RightPanel.setLayout(gridBagLayout8);
		level1NbRowsSpinbox.setFont(new java.awt.Font("Dialog", 0, 8)); //$NON-NLS-1$
		level1NbRowsSpinbox.setBorder(null);
		level1Panel.setLayout(boxLayout25);
		level1ColumnWidthSpinbox.setFont(new java.awt.Font("Dialog", 0, 8)); //$NON-NLS-1$
		level1ColumnWidthSpinbox.setBorder(null);
		levelRowHeightLabel.setText(Messages.getString("TNewGridDialog.13")); //$NON-NLS-1$
		level1SpotIDNumberRadioButton.setText(Messages.getString("TNewGridDialog.14")); //$NON-NLS-1$
		ButtonGroup bg = new ButtonGroup();
		nbLevelsLabel.setBorder(border2);
		nbLevelsLabel.setText(Messages.getString("TNewGridDialog.15")); //$NON-NLS-1$
		nbLevelsSpinbox.setBorder(null);
		nbLevelsSpinbox.setPreferredSize(new Dimension(40, 20));
		nbLevelsPanel.setBorder(border16);
		bg.add(level1SpotIDNumberRadioButton);
		bg.add(level1SpotIDRCRadioButton);
		level1SpotIDRCRadioButton.setSelected(true);
		level1SpotDiameterLabel.setText(Messages.getString("TNewGridDialog.16")); //$NON-NLS-1$
		level2StartElementLabel.setText(Messages.getString("TNewGridDialog.17")); //$NON-NLS-1$
		level2SpotIDLabel.setText(Messages.getString("TNewGridDialog.18")); //$NON-NLS-1$
		level2RowSpacingSpinbox.setBorder(null);
		level2NbRowsSpinbox.setBorder(null);
		level2RowSpacingLabel.setText(Messages.getString("TNewGridDialog.19")); //$NON-NLS-1$
		level2NbColumnsLabel.setText(Messages.getString("TNewGridDialog.20")); //$NON-NLS-1$
		level2ColumnSpacingLabel.setText(Messages.getString("TNewGridDialog.21")); //$NON-NLS-1$
		level2SpotIDNumberRadioButton.setText(Messages.getString("TNewGridDialog.22")); //$NON-NLS-1$
		bg = new ButtonGroup();
		bg.add(level2SpotIDNumberRadioButton);
		bg.add(level2SpotIDRCRadioButton);
		level2SpotIDRCRadioButton.setSelected(true);
		level2SpotIDRCRadioButton.setText(Messages.getString("TNewGridDialog.23")); //$NON-NLS-1$
		level2NbColumnsSpinbox.setBorder(null);
		level2LeftPanel.setLayout(gridBagLayout11);
		level2LeftPanel.setBorder(border8);
		level2NbRowsLabel.setText(Messages.getString("TNewGridDialog.24")); //$NON-NLS-1$
		level2ColumnSpacingSpinbox.setBorder(null);
		level2RightPanel.setBorder(border9);
		level2RightPanel.setLayout(gridBagLayout10);
		level2Panel.setLayout(boxLayout26);
		
		level1NbColumnsLabel.setText(Messages.getString("TNewGridDialog.33")); //$NON-NLS-1$
		level1Panel.setBorder(border12);
		level2Panel.setBorder(border13);
		//level3Panel.setBorder(border14);//level3 removed - 20005/10/28
		southPanel.setLayout(gridBagLayout1);
		level1ColumnWidthLabel.setText(Messages.getString("TNewGridDialog.34")); //$NON-NLS-1$
		bg = new ButtonGroup();
		getContentPane().add(mainPanel);
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		centerPanel.add(jTabbedPane1, null);
		jTabbedPane1.add(level1Panel,    Messages.getString("TNewGridDialog.35")); //$NON-NLS-1$
		level1Panel.add(level1LeftPanel, null);
		jTabbedPane1.add(level2Panel,   Messages.getString("TNewGridDialog.36")); //$NON-NLS-1$
		jTabbedPane1.setToolTipTextAt(0,Messages.getString("TNewGridDialog.37"));//externalized since 2006/02/20 
		jTabbedPane1.setToolTipTextAt(1,Messages.getString("TNewGridDialog.38"));//externalized since 2006/02/20  
		level2Panel.add(level2LeftPanel, null);
		level2LeftPanel.add(level2NbColumnsLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		southPanel.add(closeButton, new GridBagConstraints(0, 0, 1, 1, 0.05, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 20), 0, 0));
		mainPanel.add(nbLevelsPanel, BorderLayout.NORTH);
		nbLevelsPanel.add(nbLevelsLabel, null);
		nbLevelsPanel.add(nbLevelsSpinbox, null);
		
		level1LeftPanel.add(level1NbColumnsSpinbox,   new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));    
		level1LeftPanel.add(level1ColumnWidthSpinbox,   new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1NbRowsSpinbox,   new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1NbRowsLabel,   new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(levelRowHeightLabel,   new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1SpotDiameterSpinbox,   new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1SpotDiameterLabel,   new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1RowHeightSpinbox,   new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1NbColumnsLabel,     new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level1LeftPanel.add(level1ColumnWidthLabel,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		
		level1RightPanel.add(level1StartElementLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		level1RightPanel.add(level1SpotIDLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level1RightPanel.add(level1StartElementComboBox, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
		level1RightPanel.add(level1SpotIDRCRadioButton, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
		level1RightPanel.add(level1SpotIDNumberRadioButton, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		
		level1Panel.add(level1RightPanel, null);
		level1StartElementComboBox.addItem(TGridConfig.getStartElementString(1));
		level1StartElementComboBox.addItem(TGridConfig.getStartElementString(2));
		level1StartElementComboBox.addItem(TGridConfig.getStartElementString(3));
		level1StartElementComboBox.addItem(TGridConfig.getStartElementString(4));
		level2StartElementComboBox.addItem(TGridConfig.getStartElementString(1));
		level2StartElementComboBox.addItem(TGridConfig.getStartElementString(2));
		level2StartElementComboBox.addItem(TGridConfig.getStartElementString(3));
		level2StartElementComboBox.addItem(TGridConfig.getStartElementString(4));
		spotUnitChoiceComboBox.addItem(new String("Spots"));//2006/01/26
		spotUnitChoice2ComboBox.addItem(new String("Spots"));//2006/01/26
		spotUnitChoiceComboBox.addItem(new String("mcm"));//2006/01/26
		spotUnitChoice2ComboBox.addItem(new String("mcm"));//2006/01/26
		
		level2LeftPanel.add(level2NbColumnsSpinbox, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		level2LeftPanel.add(level2ColumnSpacingLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level2LeftPanel.add(level2NbRowsLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level2LeftPanel.add(level2NbRowsSpinbox, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		level2LeftPanel.add(spotUnitChoiceComboBox, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));//2006/01/26
		level2LeftPanel.add(spotUnitChoice2ComboBox, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));//2006/01/26
		level2LeftPanel.add(level2RowSpacingLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
		level2LeftPanel.add(level2RowSpacingSpinbox, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 0), 0, 0));
		level2LeftPanel.add(level2ColumnSpacingSpinbox, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		
		level2RightPanel.add(level2StartElementLabel,  new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level2RightPanel.add(level2StartElementComboBox,  new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
		level2RightPanel.add(level2SpotIDLabel,  new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		level2RightPanel.add(level2SpotIDRCRadioButton,  new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 0), 0, 0));
		level2RightPanel.add(level2SpotIDNumberRadioButton,  new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
		
		level2Panel.add(level2RightPanel, null);
		
		jTabbedPane1.setEnabledAt(1, false);
		
	}
	private void initListeners() {
		nbLevelsSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				disableListeners();
				int n = ( (Integer) nbLevelsSpinbox.getValue()).intValue();
				System.out.println("n="+n);
				
				if (n < 2) {
					level2ColumnSpacingSpinbox.setValue(new Double(0));
					level2RowSpacingSpinbox.setValue(new Double(0));
					level2NbColumnsSpinbox.setValue(new Integer(1));
					level2NbRowsSpinbox.setValue(new Integer(1));
					level2SpotIDNumberRadioButton.setSelected(false);
					level2SpotIDRCRadioButton.setSelected(true);
					level2StartElementComboBox.setSelectedItem(TGridConfig.getStartElementString(TGridConfig.TOP_LEFT));
				}
				enableListeners();
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.NB_LEVELS, null, nbLevelsSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		closeButtonActionListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				notifyClosing();
				dispose();
			}
		};
		//  modified 18/05/05
		// la largeur d'un spot modifie aussi la largeur totale de l'inter-colonne qui est proportionnelle
		// il faut donc aussi updater la valeur inter-colonne  
		level1ColumnWidthSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.SPOT_WIDTH, null, level1ColumnWidthSpinbox.getValue());
				TEventHandler.handleMessage(ev);
				// pour maj l'intercolonne en mm temps on ajoute le contenu de level2ColumnSpacingSpinboxChangeListener
				//TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_COLUMN_SPACING, null, level2ColumnSpacingSpinbox.getValue());
				double level2ColumnSpacing = ((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue()*((Double)(level1ColumnWidthSpinbox.getValue())).doubleValue();//17/05/05
				TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_COLUMN_SPACING, null,new Double(level2ColumnSpacing));
				TEventHandler.handleMessage(ev2);
				
			}
		};
		
		//  modified 18/05/05
		// la hauteur d'un spot modifie aussi la hauteur totale de l'inter-ligne qui est proportionnelle
		// il faut donc aussi updater la valeur inter-ligne  
		level1RowHeightSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.SPOT_HEIGHT, null, level1RowHeightSpinbox.getValue());
				TEventHandler.handleMessage(ev);
				// pour maj l'inter-ligne en mm temps on ajoute le contenu de level2RowSpacingSpinboxChangeListener
				double level2RowSpacing = ((Double)(level2RowSpacingSpinbox.getValue())).doubleValue()*((Double)(level1RowHeightSpinbox.getValue())).doubleValue();//17/05/05
				TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_ROW_SPACING, null, new Double(level2RowSpacing));
				TEventHandler.handleMessage(ev2);
			}
		};
		level1NbColumnsSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL1_NB_COLUMNS, null, level1NbColumnsSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		level1NbRowsSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL1_NB_ROWS, null, level1NbRowsSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		level1SpotDiameterSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL1_SPOT_DIAMETER, null, level1SpotDiameterSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		level1StartElementComboBoxActionListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL1_START, null, level1StartElementComboBox.getSelectedItem());
				TEventHandler.handleMessage(ev);
			}
		};
		level1SpotIDNumberRadioButtonChangeListener = new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL1_ID_NUMBER, null, new Boolean(level1SpotIDNumberRadioButton.isSelected()));
					TEventHandler.handleMessage(ev);
				}
			}
		};
		
		level2NbColumnsSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_NB_COLUMNS, null, level2NbColumnsSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		level2NbRowsSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_NB_ROWS, null, level2NbRowsSpinbox.getValue());
				TEventHandler.handleMessage(ev);
			}
		};
		// modifie le 18/05/05
		// l'inter-colonne recupere est en nb de "largeur de spots"
		// il faut multiplier cette valeur par la "largeur d'un spot" (valeur du 1er niveau)
		// pour avoir sa correspondance en micrometres
		level2ColumnSpacingSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {      
				double level2ColumnSpacing;	//level2ColumnSpacing is stored in micrometers....
				if (spotUnitChoiceComboBox.getSelectedItem().equals(new String("mcm"))){//no convcersion to do
					level2ColumnSpacing = ((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue();
				}//else spot case = conversion
				else level2ColumnSpacing = spotToMicrometerInWidth(((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue());
				System.out.println("dans le listener du spinner, level2ColumnSpacing="+level2ColumnSpacing);
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_COLUMN_SPACING, null,new Double(level2ColumnSpacing));
				TEventHandler.handleMessage(ev);
			}
		};
		
		//  modifie le 18/05/05
		// l'inter-ligne recupere est en nb de "hauteur de spots"
		// il faut multiplier cette valeur par la "hauteur d'un spot" (valeur du 1er niveau)
		// pour avoir sa correspondance en micrometres
		level2RowSpacingSpinboxChangeListener = new ChangeListener() {
			public void stateChanged(ChangeEvent event) {        
				double level2RowSpacing;//stored in micrometers....
				//      = ((Double)(level2RowSpacingSpinbox.getValue())).doubleValue()*((Double)(level1RowHeightSpinbox.getValue())).doubleValue();//17/05/05
				if (spotUnitChoice2ComboBox.getSelectedItem().equals(new String("mcm"))){//no convcersion to do
					level2RowSpacing = ((Double)(level2RowSpacingSpinbox.getValue())).doubleValue();
				}//else spot case = conversion
				else level2RowSpacing = spotToMicrometerInHeight(((Double)(level2RowSpacingSpinbox.getValue())).doubleValue());
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_ROW_SPACING, null, new Double(level2RowSpacing));
				TEventHandler.handleMessage(ev);
			}
		};
		level2StartElementComboBoxActionListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_START, null, level2StartElementComboBox.getSelectedItem());
				TEventHandler.handleMessage(ev);
			}
		};
		spotUnitChoiceComboBoxActionListener = new ActionListener() {//2006/01/26
			public void actionPerformed(ActionEvent event) {
				if (spotUnitChoiceComboBox.getSelectedItem().equals(new String("Spots"))){
					System.err.println("TEST SPOTS");
					//	level2ColumnSpacingSpinbox.removeChangeListener(level2ColumnSpacingSpinboxChangeListener);
					level2ColumnSpacingSpinbox.setValue(new Double(micrometerToSpotInWidth(((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue())));
					//	level2ColumnSpacingSpinbox.addChangeListener(level2ColumnSpacingSpinboxChangeListener);
					
				}
				else if (spotUnitChoiceComboBox.getSelectedItem().equals(new String("mcm"))){
					System.err.println("TEST MicroMeter");
					//2006/01/26
					//level2ColumnSpacing = ((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue();//2006/01/26
					// 		level2ColumnSpacingSpinbox.removeChangeListener(level2ColumnSpacingSpinboxChangeListener);
					level2ColumnSpacingSpinbox.setValue(new Double(spotToMicrometerInWidth(((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue())));
					// 		level2ColumnSpacingSpinbox.addChangeListener(level2ColumnSpacingSpinboxChangeListener);
					// level2NbColumnsSpinbox.setModel((new SpinnerNumberModel(1, 1, 48, 1)));//Micrometers spinner model
					//level2NbColumnsSpinbox.setModel((new SpinnerNumberModel(level2ColumnSpacing, 1, 100000, 1)));//SPOT spinner model
				}//2006/01/26
				
			}
		};
		spotUnitChoice2ComboBoxActionListener = new ActionListener() {//2006/01/26
			public void actionPerformed(ActionEvent event) {
				if (spotUnitChoice2ComboBox.getSelectedItem().equals(new String("Spots"))){
					System.err.println("TEST SPOTS2");
					//	level2ColumnSpacingSpinbox.removeChangeListener(level2ColumnSpacingSpinboxChangeListener);
					level2RowSpacingSpinbox.setValue(new Double(micrometerToSpotInHeight(((Double)(level2RowSpacingSpinbox.getValue())).doubleValue())));
					//	level2ColumnSpacingSpinbox.addChangeListener(level2ColumnSpacingSpinboxChangeListener);
					
				}
				else if (spotUnitChoice2ComboBox.getSelectedItem().equals(new String("mcm"))){
					System.err.println("TEST MicroMeter2");
					//2006/01/26
					//level2ColumnSpacing = ((Double)(level2ColumnSpacingSpinbox.getValue())).doubleValue();//2006/01/26
					// 		level2ColumnSpacingSpinbox.removeChangeListener(level2ColumnSpacingSpinboxChangeListener);
					level2RowSpacingSpinbox.setValue(new Double(spotToMicrometerInHeight(((Double)(level2RowSpacingSpinbox.getValue())).doubleValue())));
					// 		level2ColumnSpacingSpinbox.addChangeListener(level2ColumnSpacingSpinboxChangeListener);
					// level2NbColumnsSpinbox.setModel((new SpinnerNumberModel(1, 1, 48, 1)));//Micrometers spinner model
					//level2NbColumnsSpinbox.setModel((new SpinnerNumberModel(level2ColumnSpacing, 1, 100000, 1)));//SPOT spinner model
				}//2006/01/26
				
			}
		};
		
		level2SpotIDNumberRadioButtonChangeListener = new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_ID_NUMBER, null, new Boolean(level2SpotIDNumberRadioButton.isSelected()));
					TEventHandler.handleMessage(ev);
				}
			}
		};
		
		spinboxMouseListener = new MouseAdapter() {
			public void mouseReleased(MouseEvent event) {
				TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_MEMENTO, null);
				TEventHandler.handleMessage(ev);
			}
		};
		spinboxKeyListener = new KeyAdapter() {
			public void keyReleased(KeyEvent event) {
				if (event.getID() == KeyEvent.KEY_RELEASED) {
					if (event.getKeyCode() == 10) {
						TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.ADD_MEMENTO, null);
						TEventHandler.handleMessage(ev);
					}
				}
			}
		};
	}
	private void setDialogEnabled(boolean b) {
		jTabbedPane1.setEnabled(b);
		nbLevelsSpinbox.setEnabled(b);
		level1ColumnWidthSpinbox.setEnabled(b);
		level1NbColumnsSpinbox.setEnabled(b);
		level1NbRowsSpinbox.setEnabled(b);
		level1RowHeightSpinbox.setEnabled(b);
		level1SpotDiameterSpinbox.setEnabled(b);
		level1SpotIDNumberRadioButton.setEnabled(b);
		level1SpotIDRCRadioButton.setEnabled(b);
		level1StartElementComboBox.setEnabled(b);
		
		level2ColumnSpacingSpinbox.setEnabled(b);
		level2NbColumnsSpinbox.setEnabled(b);
		level2NbRowsSpinbox.setEnabled(b);
		level2RowSpacingSpinbox.setEnabled(b);
		level2SpotIDNumberRadioButton.setEnabled(b);
		level2SpotIDRCRadioButton.setEnabled(b);
		level2StartElementComboBox.setEnabled(b);
		
	}
	public void enableListeners() {
		level1ColumnWidthSpinbox.addChangeListener(level1ColumnWidthSpinboxChangeListener);
		Component[] components = level1ColumnWidthSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton)
				components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level1ColumnWidthSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level1RowHeightSpinbox.addChangeListener(level1RowHeightSpinboxChangeListener);
		components = level1RowHeightSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level1RowHeightSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		nbLevelsSpinbox.addChangeListener(nbLevelsSpinboxChangeListener);
		components = nbLevelsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)nbLevelsSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level1NbColumnsSpinbox.addChangeListener(level1NbColumnsSpinboxChangeListener);
		components = level1NbColumnsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level1NbColumnsSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level1NbRowsSpinbox.addChangeListener(level1NbRowsSpinboxChangeListener);
		components = level1NbRowsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level1NbRowsSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level1SpotDiameterSpinbox.addChangeListener(level1SpotDiameterSpinboxChangeListener);
		components = level1SpotDiameterSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level1SpotDiameterSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level1SpotIDNumberRadioButton.addItemListener(level1SpotIDNumberRadioButtonChangeListener);
		level2NbColumnsSpinbox.addChangeListener(level2NbColumnsSpinboxChangeListener);
		components = level2NbColumnsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level2NbColumnsSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level2NbRowsSpinbox.addChangeListener(level2NbRowsSpinboxChangeListener);
		components = level2NbRowsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level2NbRowsSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level2ColumnSpacingSpinbox.addChangeListener(level2ColumnSpacingSpinboxChangeListener);
		components = level2ColumnSpacingSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level2ColumnSpacingSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level2RowSpacingSpinbox.addChangeListener(level2RowSpacingSpinboxChangeListener);
		components = level2RowSpacingSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].addMouseListener(spinboxMouseListener);
		((NumberEditor)level2RowSpacingSpinbox.getEditor()).getTextField().addKeyListener(spinboxKeyListener);
		level2SpotIDNumberRadioButton.addItemListener(level2SpotIDNumberRadioButtonChangeListener);
		
		
		closeButton.addActionListener(closeButtonActionListener);
		level1StartElementComboBox.addActionListener(level1StartElementComboBoxActionListener);
		level2StartElementComboBox.addActionListener(level2StartElementComboBoxActionListener);
		spotUnitChoiceComboBox.addActionListener(spotUnitChoiceComboBoxActionListener);//2006/01/26
		spotUnitChoice2ComboBox.addActionListener(spotUnitChoice2ComboBoxActionListener);//2006/01/26
		
	}
	public void disableListeners() {
		level1ColumnWidthSpinbox.removeChangeListener(level1ColumnWidthSpinboxChangeListener);
		Component[] components = level1ColumnWidthSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level1ColumnWidthSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level1RowHeightSpinbox.removeChangeListener(level1RowHeightSpinboxChangeListener);
		components = level1RowHeightSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level1RowHeightSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		nbLevelsSpinbox.removeChangeListener(nbLevelsSpinboxChangeListener);
		components = nbLevelsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)nbLevelsSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level1NbColumnsSpinbox.removeChangeListener(level1NbColumnsSpinboxChangeListener);
		components = level1NbColumnsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level1NbColumnsSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level1NbRowsSpinbox.removeChangeListener(level1NbRowsSpinboxChangeListener);
		components = level1NbRowsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level1NbRowsSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level1SpotDiameterSpinbox.removeChangeListener(level1SpotDiameterSpinboxChangeListener);
		components = level1SpotDiameterSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level1SpotDiameterSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level1SpotIDNumberRadioButton.removeItemListener(level1SpotIDNumberRadioButtonChangeListener);
		level2NbColumnsSpinbox.removeChangeListener(level2NbColumnsSpinboxChangeListener);
		components = level2NbColumnsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level2NbColumnsSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level2NbRowsSpinbox.removeChangeListener(level2NbRowsSpinboxChangeListener);
		components = level2NbRowsSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level2NbRowsSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level2ColumnSpacingSpinbox.removeChangeListener(level2ColumnSpacingSpinboxChangeListener);
		components = level2ColumnSpacingSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level2ColumnSpacingSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level2RowSpacingSpinbox.removeChangeListener(level2RowSpacingSpinboxChangeListener);
		components = level2RowSpacingSpinbox.getComponents();
		for (int i = 0, n = components.length; i < n; i++)
			if (components[i] instanceof BasicArrowButton) components[i].removeMouseListener(spinboxMouseListener);
		((NumberEditor)level2RowSpacingSpinbox.getEditor()).getTextField().removeKeyListener(spinboxKeyListener);
		level2SpotIDNumberRadioButton.removeItemListener(level2SpotIDNumberRadioButtonChangeListener);
		
		closeButton.removeActionListener(closeButtonActionListener);
		level1StartElementComboBox.removeActionListener(level1StartElementComboBoxActionListener);
		level2StartElementComboBox.removeActionListener(level2StartElementComboBoxActionListener);
		spotUnitChoiceComboBox.removeActionListener(spotUnitChoiceComboBoxActionListener);//2006/01/26
		spotUnitChoice2ComboBox.removeActionListener(spotUnitChoice2ComboBoxActionListener);//2006/01/26
	}
	
	/**
	 * Convert a width from number of "spots" to micrometers 
	 * @param spotVal number of "width of spot"
	 * @return the number of micrometers
	 */
	double spotToMicrometerInWidth(double spotVal){//2006/01/26
		return spotVal*((Double)(level1ColumnWidthSpinbox.getValue())).doubleValue();
	}
	
	/**
	 * Convert a micrometers meseare into the corresponding number of "spot-width"  
	 * @param spotVal number of micrometers
	 * @return the number of "spot-width"
	 */
	double micrometerToSpotInWidth(double microVal){//2006/01/26
		return /*Math.floor*/(microVal/((Double)(level1ColumnWidthSpinbox.getValue())).doubleValue());
	}
	
	/**
	 * Convert a height from number of "spots" to micrometers 
	 * @param spotVal number of "height of spot"
	 * @return the number of micrometers
	 */
	double spotToMicrometerInHeight(double spotVal){//2006/01/26
		return spotVal*((Double)(level1RowHeightSpinbox.getValue())).doubleValue();
	}
	
	/**
	 * Convert a micrometers meseare into the corresponding number of "spot-height"  
	 * @param spotVal number of micrometers
	 * @return the number "spot-height"
	 */
	double micrometerToSpotInHeight(double microVal){//2006/01/26
		return /*Math.floor*/(microVal/((Double)(level1RowHeightSpinbox.getValue())).doubleValue());
	}
	
	
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2006 INRA - SIGENAE TEAM
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 * 
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
