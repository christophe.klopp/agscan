package agscan.dialog;

import javax.swing.SpinnerNumberModel;

public class TSpinnerDoubleModel extends SpinnerNumberModel {
  private double rap;
  public TSpinnerDoubleModel(double d1, double d2, double d3, double d4, double rap) {
    super(d1, d2, d3, d4);
    this.rap = rap;
  }
  private Number incrValue(int dir) {
    Number newValue;
    double v = ((Number)getValue()).doubleValue() + (getStepSize().doubleValue() * dir);
    v = Math.round(v * rap) / rap;
    newValue = new Double(v);
    if ((getMaximum() != null) && (getMaximum().compareTo(newValue) < 0)) return null;
    if ((getMinimum() != null) && (getMinimum().compareTo(newValue) > 0)) return null;
    return newValue;
  }
  public Object getNextValue() {
    return incrValue(1);
  }
  public Object getPreviousValue() {
    return incrValue(-1);
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
