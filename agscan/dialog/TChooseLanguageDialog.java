/*
 * Creation - August 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

package agscan.dialog;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.Locale;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import agscan.Messages;
import agscan.AGScan;
import agscan.TEventHandler;
import agscan.event.TEvent;

public class TChooseLanguageDialog extends JDialog implements ActionListener {
	
	// DECLARATION DES COMPOSANTS
	private JPanel jPanel_1 = new JPanel();
	private JList list;
	private JScrollPane listScroller;
	
	private JPanel jPanel_choice = new JPanel();
	private JButton jButton_OK = new JButton(Messages.getString("TChooseLanguageDialog.0"));  //$NON-NLS-1$
	private JButton jButton_cancel = new JButton(Messages.getString("TChooseLanguageDialog.1"));  //$NON-NLS-1$
	
	// language courant
	private static final String current_language = AGScan.prop.getProperty("language");// renvoie qqchose du type fr ou en ... //$NON-NLS-1$
	// current_language est mis en final pour que le menu ne change pas avant le relancement de l'appli
	private String current_language_name = new Locale(current_language).getDisplayName();// on veut le nom complet french, english...
	private String current_language_name_display = new Locale(current_language).getDisplayName(new Locale(current_language));//nom complet dans la langue courante!
	
	private String[] iso639tab;// en anglais
	private String[] iso639tab_translated;//en langue courante
	
	public TChooseLanguageDialog() {
		this.setTitle(Messages.getString("TChooseLanguageDialog.3")); //$NON-NLS-1$
		init();
		pack();
		this.setSize(250,200);
	}
	private void init() {
		
		// on recherche les fichiers properties existants selon les langues possibles
		File tempFile = null;
		String[] liste = Locale.getISOLanguages();// liste de tous les noms complets de locales reconnues par la JVM
		
		Vector possibilities =new Vector();// langues reconnues par AGScan
		// on recupere la liste de toutes les locales avec la liste des noms complets de locales ( remarque:
		// on ne demande pas directement a la JVM pour ne pas avoir de redondances = tous les fr differents...)
		Locale[] locales = new Locale[liste.length];
		iso639tab_translated = new String[liste.length];
		iso639tab = new String[liste.length];
		for (int j=0;j<liste.length;j++){
			locales[j] = new Locale(liste[j]);
			iso639tab_translated[j] = locales[j].getDisplayName(new Locale(current_language)); 
			iso639tab[j] = locales[j].getDisplayName();
		}
		for (int i=0;i<locales.length;i++){
			// si le fichier messages_xx_properties existe pour la langue xx, c'est que BZScan2 peut traduire
			//tempFile = new File("agscan"+File.separator+"messages_"+locales[i].getLanguage()+".properties"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			//2005/12/05 - pb the agscan directory doesn't exist by default ioutside of sources
			//we replace it by the directory "languages"
			///TODO remarque sous XP bug on dirait des langues....
			tempFile = new File("languages"+File.separator+"messages_"+locales[i].getLanguage()+".properties"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		//	System.out.println("temp file="+tempFile);//test 2005/12/05
			if (tempFile.exists()){
				System.out.println("path existant pour la locale="+tempFile.getAbsolutePath());//test 2005/12/05
				possibilities.add(locales[i].getDisplayName(new Locale(current_language)));		// pour afficher dans la langue actuellement utilisée		
			}			
		}
		String[] possibilities_array = new String[possibilities.size()];//tableau des possibilités (noms complets) de BZ2
		possibilities.copyInto(possibilities_array);
		Arrays.sort(possibilities_array);// tri du tableau
		
		
		// CONSTRUCTION DES COMPOSANTS //
		list = new JList(possibilities_array); //data has type Object[]
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(4);
		list.setSelectedValue(current_language_name_display,true);
		listScroller = new JScrollPane(list);
		
		// AFFECTATION DES LISTENERS
		
		jButton_cancel.addActionListener(this);
		jButton_OK.addActionListener(this);
		
		//ASSEMBLAGE DES COMPOSANTS
		jPanel_1.add(listScroller);
		jPanel_choice.add(jButton_OK);
		jPanel_choice.add(jButton_cancel);
		
		jPanel_1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TChooseLanguageDialog.7")));  //$NON-NLS-1$
		jPanel_choice.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TChooseLanguageDialog.8"))); //$NON-NLS-1$
		
		
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.getContentPane().add(jPanel_1);
		this.getContentPane().add(jPanel_choice);
		
				
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == jButton_cancel){
			dispose();
		}
		if (e.getSource() == jButton_OK){
			
			//TODO : il aurait été + leger de sauvegarder les locales dispo et de rechercher 
			// celle selectionnée parmi elles.
			// si la selection differe de la langue initiale, on modifie les propriétés
			System.out.println("OK=> "+current_language_name); //$NON-NLS-1$
			System.out.println("OK=> "+list.getSelectedValue()); //$NON-NLS-1$
			String selected = (String)list.getSelectedValue();//on récupere dans la langue courante
			// on le retrouve en anglais dans la liste iso
			String selected_en = null;
			for (int i=0;i<iso639tab_translated.length;i++){
				//System.out.println(iso639tab_translated[i] +" et selection=" +selected);
				//System.out.println("iso639tab="+iso639tab[i]);
						if (iso639tab_translated[i].equals(selected)) selected_en = iso639tab[i];
			}
			// if (!selected.equals((new Locale(current_language).getDisplayName(new Locale(current_language)))))// comparaison avec le nom complet dans la langue courante!
				
			if (!selected.equals(current_language_name)) {
				//String new_current_name = (String)list.getSelectedValue();
				String new_current =null;
				String[] liste = Locale.getISOLanguages();// liste de tous les noms complets de locales reconnues par la JVM
				String[] listeNames = new String[liste.length];// on recupere les noms complets
				for (int j=0;j<liste.length;j++){
					listeNames[j] =  new Locale(liste[j]).getDisplayName();
				}
				//galere pour repasser du nom complet au nom iso (2 lettres)
				for (int i=0;i<liste.length;i++){
					//System.out.println(listeNames[i]+ " et "+ new_current_name);
					//if (listeNames[i].equalsIgnoreCase(new_current_name)) new_current = liste[i];
					if (listeNames[i].equalsIgnoreCase(selected_en)) new_current = liste[i];
				}
				Locale newLoc = new Locale(new_current);	
				AGScan.prop.setProperty("language",newLoc.getLanguage()); //$NON-NLS-1$
				//prévenir que le changement de langue sera effectif au prochain demarrage
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
						TDialogManager.MESSAGE_DIALOG, Messages.getString("TChooseLanguageDialog.2")); //$NON-NLS-1$
				TEventHandler.handleMessage(event);		
				//test
				/*
				 System.out.println("new1"); //$NON-NLS-1$
				 for (Enumeration en = AGScan.prop.elements() ; en.hasMoreElements() ;) {
				 System.out.println(en.nextElement());
				 }
				 System.out.println("new2"); //$NON-NLS-1$
				 */
				// fin test
			}
			dispose();
			
		}
		
	}
	
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
