package agscan.dialog;
/* 2006/01/04 - #180 bug of mulcyber fixed: Modification of menu word comparison(radian, degrees...)
 * now, comparison is made with the current language word of the menu! 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.memento.TUndoGridPosition;
import agscan.data.controler.memento.TUndoGridResizing;
import agscan.data.controler.memento.TUndoGridRotation;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.element.alignment.TAlignment;
import agscan.event.TEvent;

public class TGridPositioningDialog extends TDialog {
  class TranslateListener implements ActionListener {
    private double xTranslate, yTranslate;
    public TranslateListener(double xTranslate, double yTranslate) {
      this.xTranslate = xTranslate;
      this.yTranslate = yTranslate;
    }
    public void actionPerformed(ActionEvent event) {
      double dx, dy;
      try {
        dx = dy = Double.parseDouble(translateValueTextField.getText());
        TUndoGridPosition ugp = new TUndoGridPosition();
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
        double pixelWidth = element.getModel().getPixelWidth();
        double pixelHeight = element.getModel().getPixelHeight();
        double spotWidth = ((TGriddedElement)element).getGridModel().getRootBlock().getSpotWidth();
        double spotHeight = ((TGriddedElement)element).getGridModel().getRootBlock().getSpotHeight();
        // 2006/01/04 - Modification for the #180 bug of mulcyber: comparison must be done with the current language words!
        //if (((String)unitTranslateComboBox.getSelectedItem()).equals("pixels")) {
        if (((String)unitTranslateComboBox.getSelectedItem()).equals(Messages.getString("TGridPositioningDialog.7"))) {//pixels
          dx *= pixelWidth;
          dy *= pixelHeight;
        }
        // 2006/01/04 - Modification for the #180 bug of mulcyber: comparison must be done with the current language words!
        //else if (((String)unitTranslateComboBox.getSelectedItem()).equals("spots")) {
        else if (((String)unitTranslateComboBox.getSelectedItem()).equals(Messages.getString("TGridPositioningDialog.8"))) {//spots
          dx *= spotWidth;
          dy *= spotHeight;
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.TRANSLATE, null,
                        new Double(dx * xTranslate), new Double(dy * yTranslate));
        TEventHandler.handleMessage(ev);
        ugp.addFinalPosition();
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugp);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
        TEventHandler.handleMessage(ev);
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
      }
    }
  }
  class RotateListener implements ActionListener {
    double rotateFactor;
    public RotateListener(boolean negative) {
      if (negative)
        rotateFactor = -1.0D;
      else
        rotateFactor = 1.0D;
    }
    public void actionPerformed(ActionEvent event) {
      double da;
      try {
        da = Double.parseDouble(rotateValueTextField.getText()) * rotateFactor;
        TUndoGridRotation ugo = new TUndoGridRotation();
        // 2006/01/04 - Modification for the #180 bug of mulcyber: comparison must be done with the current language words!
        //if (((String)unitRotateComboBox.getSelectedItem()).equals("degr�s")) da *= (Math.PI / 180.0D); 
        if (((String)unitRotateComboBox.getSelectedItem()).equals(Messages.getString("TGridPositioningDialog.9"))) da *= (Math.PI / 180.0D); 
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, null, new Double(Math.tan(da)));
        TEventHandler.handleMessage(ev);
        ugo.addFinalPosition();
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugo);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
        TEventHandler.handleMessage(ev);
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
      }
    }
  }
  class ResizeListener implements ActionListener {
    private int message;
    public ResizeListener(int message) {
      this.message = message;
    }
    public void actionPerformed(ActionEvent event) {
      double dx, dy;
      try {
        dx = dy = Double.parseDouble(resizeValueTextField.getText());
        TUndoGridResizing ugr = new TUndoGridResizing();
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
        double pixelWidth = element.getModel().getPixelWidth();
        double pixelHeight = element.getModel().getPixelHeight();
        double spotWidth = ((TGriddedElement)element).getGridModel().getRootBlock().getSpotWidth();
        double spotHeight = ((TGriddedElement)element).getGridModel().getRootBlock().getSpotHeight();
        // 2006/01/04 - Modification for the #180 bug of mulcyber: comparison must be done with the current language words!
        if (((String)unitResizeComboBox.getSelectedItem()).equals(Messages.getString("TGridPositioningDialog.11"))){//�m 
        // if (((String)unitResizeComboBox.getSelectedItem()).equals("�m")){//"\u00B5m")) { //"µm" //$NON-NLS-1$ 
          dx /= pixelWidth;
          dy /= pixelHeight;
        }
        //else if (((String)unitResizeComboBox.getSelectedItem()).equals("spots")) {
        // 2006/01/04 - Modification for the #180 bug of mulcyber: comparison must be done with the current language words!
        	  else if (((String)unitResizeComboBox.getSelectedItem()).equals(Messages.getString("TGridPositioningDialog.13"))) { 
          dx *= (spotWidth / pixelWidth);
          dy *= (spotHeight / pixelHeight);
        }
        double d = dx;
        if ((message == TGridPositionControler.RESIZE_NORTH) || (message == TGridPositionControler.RESIZE_SOUTH)) d = dy;
        ev = new TEvent(TEventHandler.DATA_MANAGER, message, null, new Double(d), new Double(pixelWidth), new Double(pixelHeight));
        TEventHandler.handleMessage(ev);
        ugr.addFinalState();
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugr);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
        TEventHandler.handleMessage(ev);
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
      }
    }
  }
  private JButton closeButton = new JButton();
  private JComboBox unitTranslateComboBox = new JComboBox();
  private JTextField translateValueTextField = new JTextField();
  private JButton leftTranslateButton = new JButton();
  private JButton topTranslateButton = new JButton();
  private JButton rightTranslateButton = new JButton();
  private JButton bottomTranslateButton = new JButton();
  private JComboBox unitRotateComboBox = new JComboBox();
  private JTextField rotateValueTextField = new JTextField();
  private JButton topRotateButton = new JButton();
  private JButton bottomRotateButton = new JButton();
  private JComboBox unitResizeComboBox = new JComboBox();
  private JTextField resizeValueTextField = new JTextField();
  private JButton leftResizeButton = new JButton();
  private JButton rightResizeButton = new JButton();
  private JButton topResizeButton = new JButton();
  private JButton bottomResizeButton = new JButton();

  private static TDialog instance;

  protected TGridPositioningDialog(TDataElement element) {
    try {
      jbInit();
      init(element);
      initListeners();
      pack();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void init(TDataElement element) {
    if (element instanceof TAlignment) {
      unitTranslateComboBox.setEnabled(true);
      translateValueTextField.setEditable(true);
      leftTranslateButton.setEnabled(true);
      topTranslateButton.setEnabled(true);
      rightTranslateButton.setEnabled(true);
      bottomTranslateButton.setEnabled(true);
      unitRotateComboBox.setEnabled(true);
      rotateValueTextField.setEditable(true);
      topRotateButton.setEnabled(true);
      bottomRotateButton.setEnabled(true);
      unitResizeComboBox.setEnabled(true);
      resizeValueTextField.setEditable(true);
      leftResizeButton.setEnabled(true);
      rightResizeButton.setEnabled(true);
      topResizeButton.setEnabled(true);
      bottomResizeButton.setEnabled(true);
    }
    else {
      unitTranslateComboBox.setEnabled(false);
      translateValueTextField.setEditable(false);
      leftTranslateButton.setEnabled(false);
      topTranslateButton.setEnabled(false);
      rightTranslateButton.setEnabled(false);
      bottomTranslateButton.setEnabled(false);
      unitRotateComboBox.setEnabled(false);
      rotateValueTextField.setEditable(false);
      topRotateButton.setEnabled(false);
      bottomRotateButton.setEnabled(false);
      unitResizeComboBox.setEnabled(false);
      resizeValueTextField.setEditable(false);
      leftResizeButton.setEnabled(false);
      rightResizeButton.setEnabled(false);
      topResizeButton.setEnabled(false);
      bottomResizeButton.setEnabled(false);
    }
  }
  private void jbInit() throws Exception {
    setResizable(false);
    setTitle(Messages.getString("TGridPositioningDialog.5")); //$NON-NLS-1$
    unitTranslateComboBox.addItem(Messages.getString("TGridPositioningDialog.6")); //$NON-NLS-1$
    unitTranslateComboBox.addItem(Messages.getString("TGridPositioningDialog.7")); //$NON-NLS-1$
    unitTranslateComboBox.addItem(Messages.getString("TGridPositioningDialog.8")); //$NON-NLS-1$
    unitRotateComboBox.addItem(Messages.getString("TGridPositioningDialog.9")); //$NON-NLS-1$
    unitRotateComboBox.addItem(Messages.getString("TGridPositioningDialog.10")); //$NON-NLS-1$
    unitResizeComboBox.addItem(Messages.getString("TGridPositioningDialog.11")); //$NON-NLS-1$
    unitResizeComboBox.addItem(Messages.getString("TGridPositioningDialog.12")); //$NON-NLS-1$
    unitResizeComboBox.addItem(Messages.getString("TGridPositioningDialog.13")); //$NON-NLS-1$
    this.getContentPane().setLayout(new BorderLayout());
    closeButton.setMinimumSize(new Dimension(75, 21));
    closeButton.setPreferredSize(new Dimension(75, 21));
    closeButton.setText(Messages.getString("TGridPositioningDialog.14")); //$NON-NLS-1$
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new GridBagLayout());
    JPanel translatePanel = new JPanel();
    translatePanel.setLayout(new GridBagLayout());
    JPanel rotatePanel = new JPanel();
    rotatePanel.setLayout(new GridBagLayout());
    JPanel resizePanel = new JPanel();
    resizePanel.setLayout(new GridBagLayout());
    translatePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),Messages.getString("TGridPositioningDialog.15"))); //$NON-NLS-1$
    rotatePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),Messages.getString("TGridPositioningDialog.16"))); //$NON-NLS-1$
    resizePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),Messages.getString("TGridPositioningDialog.17"))); //$NON-NLS-1$
    JLabel unitTranslateLabel = new JLabel(Messages.getString("TGridPositioningDialog.18")); //$NON-NLS-1$
    leftTranslateButton.setText(""); //$NON-NLS-1$
    leftTranslateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/left.gif"))); //$NON-NLS-1$
    leftTranslateButton.setMargin(new Insets(0, 0, 0, 0));
    topTranslateButton.setText(""); //$NON-NLS-1$
    topTranslateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/top.gif"))); //$NON-NLS-1$
    topTranslateButton.setMargin(new Insets(0, 5, 0, 5));
    rightTranslateButton.setText(""); //$NON-NLS-1$
    rightTranslateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/right.gif"))); //$NON-NLS-1$
    rightTranslateButton.setMargin(new Insets(0, 0, 0, 0));
    bottomTranslateButton.setText(""); //$NON-NLS-1$
    bottomTranslateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/bottom.gif"))); //$NON-NLS-1$
    bottomTranslateButton.setMargin(new Insets(0, 5, 0, 5));
    JLabel unitRotateLabel = new JLabel(Messages.getString("TGridPositioningDialog.0")); //$NON-NLS-1$
    topRotateButton.setMargin(new Insets(0, 5, 0, 5));
    topRotateButton.setText(""); //$NON-NLS-1$
    topRotateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/up_rotation.gif"))); //$NON-NLS-1$
    bottomRotateButton.setMargin(new Insets(0, 5, 0, 5));
    bottomRotateButton.setText(""); //$NON-NLS-1$
    bottomRotateButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/bottom_rotation.gif"))); //$NON-NLS-1$
    JLabel unitResizeLabel = new JLabel(Messages.getString("TGridPositioningDialog.1")); //$NON-NLS-1$
    leftResizeButton.setMargin(new Insets(0, 0, 0, 0));
    leftResizeButton.setText(""); //$NON-NLS-1$
    leftResizeButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/hor_resize.gif"))); //$NON-NLS-1$
    rightResizeButton.setMargin(new Insets(0, 0, 0, 0));
    rightResizeButton.setText(""); //$NON-NLS-1$
    rightResizeButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/hor_resize.gif"))); //$NON-NLS-1$
    topResizeButton.setMargin(new Insets(0, 5, 0, 5));
    topResizeButton.setText(""); //$NON-NLS-1$
    topResizeButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/vert_resize.gif"))); //$NON-NLS-1$
    bottomResizeButton.setMargin(new Insets(0, 5, 0, 5));
    bottomResizeButton.setText(""); //$NON-NLS-1$
    bottomResizeButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/vert_resize.gif"))); //$NON-NLS-1$
    this.getContentPane().add(centerPanel, BorderLayout.CENTER);
    JPanel southPanel = new JPanel();
    this.getContentPane().add(southPanel,  BorderLayout.SOUTH);
    southPanel.add(closeButton, null);
    centerPanel.add(translatePanel,     new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    translatePanel.add(unitTranslateLabel,       new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    centerPanel.add(rotatePanel,    new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    rotatePanel.add(unitRotateLabel,   new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    centerPanel.add(resizePanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    resizePanel.add(unitResizeLabel,       new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    translatePanel.add(unitTranslateComboBox,       new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 10), 0, 0));
    translatePanel.add(translateValueTextField,        new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    translatePanel.add(leftTranslateButton,    new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    translatePanel.add(topTranslateButton,   new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    translatePanel.add(rightTranslateButton,    new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
    translatePanel.add(bottomTranslateButton,    new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 10, 0), 0, 0));
    rotatePanel.add(unitRotateComboBox,   new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 10), 0, 0));
    rotatePanel.add(rotateValueTextField,    new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
    rotatePanel.add(topRotateButton,   new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
    rotatePanel.add(bottomRotateButton,    new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 10, 10), 0, 0));
    resizePanel.add(unitResizeComboBox,       new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 10), 0, 0));
    resizePanel.add(resizeValueTextField,      new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    resizePanel.add(leftResizeButton,     new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    resizePanel.add(rightResizeButton,     new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
    resizePanel.add(topResizeButton,   new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    resizePanel.add(bottomResizeButton,   new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 10, 0), 0, 0));
  }
  public void initListeners() {
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        notifyClosing();
        dispose();
      }
    });
    leftTranslateButton.addActionListener(new TranslateListener(-1.0D, 0.0D));
    rightTranslateButton.addActionListener(new TranslateListener(1.0D, 0.0D));
    topTranslateButton.addActionListener(new TranslateListener(0.0D, -1.0D));
    bottomTranslateButton.addActionListener(new TranslateListener(0.0D, 1.0D));
    topRotateButton.addActionListener(new RotateListener(true));
    bottomRotateButton.addActionListener(new RotateListener(false));
    leftResizeButton.addActionListener(new ResizeListener(TGridPositionControler.RESIZE_WEST));
    rightResizeButton.addActionListener(new ResizeListener(TGridPositionControler.RESIZE_EAST));
    topResizeButton.addActionListener(new ResizeListener(TGridPositionControler.RESIZE_NORTH));
    bottomResizeButton.addActionListener(new ResizeListener(TGridPositionControler.RESIZE_SOUTH));
  }
  public static TDialog getInstance(TDataElement element) {
    if (instance == null)
      instance = new TGridPositioningDialog(element);
    else
      instance.init(element);
    return instance;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
