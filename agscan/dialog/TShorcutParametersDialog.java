package agscan.dialog;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.event.TEvent;
import agscan.menu.action.TGlobalAlignmentDefaultAction;
import agscan.menu.action.TLocalAlignmentDefaultAction;
import agscan.menu.action.TShorcutParametersAction;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.TPluginManager;

public class TShorcutParametersDialog extends JDialog implements ActionListener {
	
	private JPanel global;
	private JPanel choix;
	private JPanel pan_ga;
	private JPanel pan_la;
	private JPanel valid;
	private ButtonGroup ga_choix;
	private ButtonGroup la_choix;
	private JButton b_ok ;
	private JButton b_an ;
	private JButton b_pic_ga;
	private JButton b_pic_la;
	Vector<Integer> indexes;
	
	protected JRadioButton ga_snif;
	protected JRadioButton ga_temp;
	protected JRadioButton la_bloc;
	protected JRadioButton la_grid;
	protected JRadioButton la_temp;
	protected JRadioButton[] plugins;
	
	public TShorcutParametersDialog()
	{
		// titre
		this.setTitle("Boutons d'actions rapides"); //$NON-NLS-1$
		
		// init
		init();
		
		// listeners
		initList();
		
		// compactage de la fenetre
		pack();
	}
	
	public void init()
	{
		// panneau global
		global = new JPanel();
		global.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TShorcutParametersDialog.0"))); //$NON-NLS-1$
		global.setLayout(new BoxLayout(global,BoxLayout.Y_AXIS));
		
		// panneau des choix
		choix = new JPanel();
		choix.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TShorcutParametersDialog.1"))); //$NON-NLS-1$
		choix.setLayout(new BoxLayout(choix,BoxLayout.Y_AXIS));
		pan_ga = new JPanel();
		pan_ga.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TShorcutParametersDialog.2"))); //$NON-NLS-1$
		pan_ga.setLayout(new BoxLayout(pan_ga,BoxLayout.Y_AXIS));
		
		pan_la = new JPanel();
		pan_la.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TShorcutParametersDialog.3"))); //$NON-NLS-1$
		pan_la.setLayout(new BoxLayout(pan_la,BoxLayout.Y_AXIS));
		
		b_pic_ga = new JButton(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/global_alignment.gif"))); //$NON-NLS-1$
		b_pic_ga.setEnabled(false);
		ga_choix = new ButtonGroup();
		ga_snif = new JRadioButton(Messages.getString("TShorcutParametersDialog.5")); //$NON-NLS-1$
		ga_temp = new JRadioButton(Messages.getString("TShorcutParametersDialog.16")); //$NON-NLS-1$
		ga_choix.add(ga_snif);
		ga_choix.add(ga_temp);
		pan_ga.add(b_pic_ga);
		pan_ga.add(ga_snif);
		pan_ga.add(ga_temp);
		
		// plugins
		initPlugins();
		for(int i=0;i<plugins.length;i++)
		{
			ga_choix.add(plugins[i]);
			pan_ga.add(plugins[i]);
		}
		
		b_pic_la = new JButton(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/local_alignment.gif"))); //$NON-NLS-1$
		b_pic_la.setEnabled(false);
		la_choix = new ButtonGroup();
		la_bloc = new JRadioButton(Messages.getString("TShorcutParametersDialog.8")); //$NON-NLS-1$
		la_grid = new JRadioButton(Messages.getString("TShorcutParametersDialog.9")); //$NON-NLS-1$
		la_temp = new JRadioButton(Messages.getString("TShorcutParametersDialog.10")); //$NON-NLS-1$
		
		la_choix.add(la_bloc);
		la_choix.add(la_grid);
		la_choix.add(la_temp);
		pan_la.add(b_pic_la);
		pan_la.add(la_bloc);
		pan_la.add(la_grid);
		pan_la.add(la_temp);
		choix.add(pan_ga);
		choix.add(pan_la);
		
		// recuperation des valeurs precedentes
		int previousGA = AGScan.prop.getIntProperty("TShorcutParameters.ga"); //$NON-NLS-1$
		int previousLA = AGScan.prop.getIntProperty("TShorcutParameters.la"); //$NON-NLS-1$
		switch(previousGA)
		{
				case TGlobalAlignmentDefaultAction.G_SNIFFER : 
					ga_snif.setSelected(true);break;
				case TGlobalAlignmentDefaultAction.G_TEMPMATCH : 
					ga_temp.setSelected(true);break;
				default : 
					previousGA = Integer.parseInt(Integer.toString(previousGA).substring(1,3));
					if (indexes.indexOf(previousGA) != -1){
						plugins[indexes.indexOf(previousGA)].setSelected(true);break;
					}
					else{
						ga_temp.setSelected(true);break;
					}
		}
		switch(previousLA)
		{
				case TLocalAlignmentDefaultAction.L_BLOC : 
					la_bloc.setSelected(true);break;
				case TLocalAlignmentDefaultAction.L_GRID : 
					la_grid.setSelected(true);break;
				case TLocalAlignmentDefaultAction.L_TEMPMATCH : 
					la_temp.setSelected(true);break;
		}

		// panneau de validation
		valid = new JPanel();
		valid.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.ORANGE),Messages.getString(Messages.getString("TShorcutParametersDialog.13")))); //$NON-NLS-1$
		valid.setLayout(new FlowLayout(FlowLayout.CENTER));
		b_ok = new JButton(Messages.getString("TShorcutParametersDialog.14")); //$NON-NLS-1$
		b_an = new JButton(Messages.getString("TShorcutParametersDialog.15")); //$NON-NLS-1$
		valid.add(b_ok);
		valid.add(b_an);
		
		// assemblage
		global.add(choix);
		global.add(valid);
		
		this.setContentPane(global);
		
	}
	
	public void initPlugins()
	{
		int tot = TPluginManager.plugs.length;
		indexes = new Vector<Integer>();
		for (int i=0;i<tot;i++)
		{
			if (TPluginManager.plugs[i] instanceof SAlignPlugin)	
			{
				if(TPluginManager.plugs[i].isBatchPlugin())
					indexes.addElement(i);
				System.out.println(i+" ");
			}
		}
		
		plugins = new JRadioButton[indexes.size()];
		for (int i=0;  i<indexes.size();i++)
		{
			plugins[i] = new JRadioButton(TPluginManager.plugs[indexes.get(i)].getName());
		}
	}
	
	public void initList()
	{
		b_ok.addActionListener(this) ;
		b_an.addActionListener(this) ;
	}
	
	public int pluginSelected()
	{
		for(int i=0;i<plugins.length;i++)
		{
			if (plugins[i].isSelected())return indexes.get(i);
		}
		return -1;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource().equals(b_ok))
		{
			if (		(ga_snif.isSelected()
					||	ga_temp.isSelected()
					||  pluginSelected()!=-1)
			&& (		(la_bloc.isSelected()
					||	 la_grid.isSelected()
					||	 la_temp.isSelected())))
			{
				// ALI. GLOBAL
				if (ga_snif.isSelected())
				{
					AGScan.prop.setProperty("TShorcutParameters.ga",TGlobalAlignmentDefaultAction.G_SNIFFER); //$NON-NLS-1$
				}
				else if (ga_temp.isSelected())
				{
					AGScan.prop.setProperty("TShorcutParameters.ga",TGlobalAlignmentDefaultAction.G_TEMPMATCH); //$NON-NLS-1$
				}
				else if (pluginSelected()!=-1)
				{
					int i = pluginSelected();
					// on ajoute 100 a l indice du plugin comme ca on est sur qu'on 
					// ne confondra pas avec les algos
					AGScan.prop.setProperty("TShorcutParameters.ga",i+100); //$NON-NLS-1$
				}
				
				// ALI. LOCAL
				if (la_bloc.isSelected())
				{
					AGScan.prop.setProperty("TShorcutParameters.la",TLocalAlignmentDefaultAction.L_BLOC); //$NON-NLS-1$
				}
				else if (la_grid.isSelected())
				{
					AGScan.prop.setProperty("TShorcutParameters.la",TLocalAlignmentDefaultAction.L_GRID); //$NON-NLS-1$
				}
				else if (la_temp.isSelected())
				{
					AGScan.prop.setProperty("TShorcutParameters.la",TLocalAlignmentDefaultAction.L_TEMPMATCH); //$NON-NLS-1$
				}
				dispose();
			}
			else
			{
				TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Veuillez choisir...");// externalized 2006/02/02  //$NON-NLS-1$
				TEventHandler.handleMessage(ev);
				return;
			}
			
		}
		else if (e.getSource().equals(b_an))
		{
			dispose();
		}
	}
}
