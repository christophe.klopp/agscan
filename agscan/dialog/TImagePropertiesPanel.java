/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.dialog;

import ij.ImagePlus;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import agscan.Messages;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;

public class TImagePropertiesPanel extends JPanel {
  private JTextField widthTextField = new JTextField();
  private JTextField filenameTextField = new JTextField();
  private JTextField heightTextField = new JTextField();
  private JTextField pixelWidthTextField = new JTextField();
//  private JTextField sensitivityTextField = new JTextField();//viré le 26/08/05
  private JTextField unitTextField = new JTextField();
  private JTextField ipTypeTextField = new JTextField();
  private JTextField pixelHeightTextField = new JTextField();
  private JTextField latitudeTextField = new JTextField();
  private JTextField bppTextField = new JTextField();

  public TImagePropertiesPanel() {
    Border textFieldBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 5, 0, 5));
    JPanel varPanel = new JPanel(new GridBagLayout());
    varPanel.setBorder(BorderFactory.createCompoundBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    JLabel latitudeLabel = new JLabel(Messages.getString("TImagePropertiesPanel.0")); //$NON-NLS-1$
  //  JLabel sensitivityLabel = new JLabel(Messages.getString("TImagePropertiesPanel.1")); //$NON-NLS-1$
    JLabel bppLabel = new JLabel(Messages.getString("TImagePropertiesPanel.2")); //$NON-NLS-1$
    JLabel pixelWidthLabel = new JLabel(Messages.getString("TImagePropertiesPanel.3")); //$NON-NLS-1$
    JLabel pixelHeightLabel = new JLabel(Messages.getString("TImagePropertiesPanel.4")); //$NON-NLS-1$
    JLabel unitLabel = new JLabel(Messages.getString("TImagePropertiesPanel.5")); //$NON-NLS-1$
    JLabel ipTypeLabel = new JLabel(Messages.getString("TImagePropertiesPanel.6")); //$NON-NLS-1$
    ipTypeTextField.setEditable(true);
    JLabel heightLabel = new JLabel(Messages.getString("TImagePropertiesPanel.7")); //$NON-NLS-1$
    widthTextField.setBorder(textFieldBorder);
    widthTextField.setEditable(false);
    JLabel widthLabel = new JLabel(Messages.getString("TImagePropertiesPanel.8")); //$NON-NLS-1$
    filenameTextField.setEnabled(true);
    filenameTextField.setBorder(textFieldBorder);
    filenameTextField.setEditable(false);
    JLabel filenameLabel = new JLabel(Messages.getString("TImagePropertiesPanel.9")); //$NON-NLS-1$
    heightTextField.setBorder(textFieldBorder);
    heightTextField.setEditable(false);
    JPanel fixPanel = new JPanel();
    fixPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.
        createEtchedBorder(Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    fixPanel.setLayout(new GridBagLayout());
    setLayout(new BorderLayout());
    JPanel centerPanel = new JPanel();
    centerPanel.add(varPanel, null);
    bppTextField.setBorder(textFieldBorder);
    bppTextField.setEditable(false);

    JPanel northPanel = new JPanel();
    northPanel.add(fixPanel, null);
    add(northPanel, BorderLayout.NORTH);
    pixelWidthTextField.setEditable(false);
    pixelHeightTextField.setEditable(false);
    unitTextField.setEditable(false);
    latitudeTextField.setEditable(false);
   // sensitivityTextField.setEditable(false);
    ipTypeTextField.setEditable(false);
    fixPanel.add(filenameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                       GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    fixPanel.add(filenameTextField, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                           GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    fixPanel.add(widthLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                    GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    fixPanel.add(widthTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                        GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    fixPanel.add(heightLabel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                     GridBagConstraints.NONE, new Insets(0, 20, 5, 0), 0, 0));
    fixPanel.add(heightTextField, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                         GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));
    fixPanel.add(bppLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                  GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    fixPanel.add(bppTextField, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                      GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    add(centerPanel, BorderLayout.CENTER);
    varPanel.add(pixelWidthLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                         GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    varPanel.add(pixelWidthTextField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                                                             GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    varPanel.add(pixelHeightLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                          GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    varPanel.add(pixelHeightTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                              GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    varPanel.add(unitLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                   GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    varPanel.add(unitTextField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                      GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    varPanel.add(ipTypeLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                       GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    varPanel.add(ipTypeTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                           GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
    varPanel.add(latitudeLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                                                       GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    varPanel.add(latitudeTextField, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                                                           GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
 //   varPanel.add(sensitivityLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
   //                                                       GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
   // varPanel.add(sensitivityTextField, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
    //                                                        GridBagConstraints.HORIZONTAL, new Insets(0, 5, 5, 0), 0, 0));
  }
  public void init(TImage image) {
    if (image == null) {
      widthTextField.setText(""); //$NON-NLS-1$
      filenameTextField.setText(""); //$NON-NLS-1$
      heightTextField.setText(""); //$NON-NLS-1$
      bppTextField.setText(""); //$NON-NLS-1$
      pixelWidthTextField.setText(""); //$NON-NLS-1$
      pixelHeightTextField.setText(""); //$NON-NLS-1$
      latitudeTextField.setText(""); //$NON-NLS-1$
  //    sensitivityTextField.setText(""); //$NON-NLS-1$
      ipTypeTextField.setText(""); //$NON-NLS-1$
      }
    else {
      TImageModel model = image.getImageModel();
      ImagePlus pi = image.getImageModel().getInitialImage();
      widthTextField.setText(String.valueOf(model.getElementWidth()));
      filenameTextField.setText(" " + image.getName()); //$NON-NLS-1$
      heightTextField.setText(String.valueOf(model.getElementHeight()));
      bppTextField.setText(String.valueOf(model.getImageBitDepth()));
      pixelWidthTextField.setText(String.valueOf(model.getPixelWidth()));
      pixelHeightTextField.setText(String.valueOf(model.getPixelHeight()));
      unitTextField.setText(model.getUnit());
      latitudeTextField.setText(String.valueOf(ImagePlusTools.getLatitude()));
      //sensitivityTextField.setText(String.valueOf(model.getSensitivity()));
      ipTypeTextField.setText(model.getIPType());
    }
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
