package agscan.dialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TAction;
import agscan.menu.action.TComputeDiameterAction;
import agscan.menu.action.TComputeFitCorrectionAction;
import agscan.menu.action.TComputeOvershiningCorrectionAction;
import agscan.menu.action.TComputeQMAction;
import agscan.menu.action.TComputeQuantifFitConstAction;
import agscan.menu.action.TComputeQuantifFitVarAction;
import agscan.menu.action.TComputeQuantifImageConstAction;
import agscan.menu.action.TComputeQuantifImageVarAction;
import agscan.menu.action.TComputeSpotQualityDefaultAction;
import agscan.menu.action.TComputeSpotQualityFourProfilesAction;
import agscan.menu.action.TComputeSpotQualityMultiChannelAction;
import agscan.menu.action.TComputeSpotQualityTwoProfilesAction;
import agscan.menu.action.TExportAsTextAction;
import agscan.menu.action.TGlobalAlignmentDefaultAction;
import agscan.menu.action.TGlobalAlignmentEdgesFinderAction;
import agscan.menu.action.TGlobalAlignmentSnifferAction;
import agscan.menu.action.TGlobalAlignmentTemplateMatchingAction;
import agscan.menu.action.TLocalAlignmentBlocAction;
import agscan.menu.action.TLocalAlignmentBlocTemplateAction;
import agscan.menu.action.TLocalAlignmentGridAction;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.SGridPlugin;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPlugin;
import agscan.plugins.TPluginManager;


public class TProcessChooserDialog extends JDialog{

	/**** ATTRIBUTS ****/
	// Constantes
	private final static int ALIGN = 0;
	private final static int QUANTIF = 1;
	private final static int OPTION_SNAP = 3;
	private final static int OPTION_TIFF = 4;
	private final static int OPTION_EXPORT = 5;
	
	// Liste des noms d'algos d'alignement
	private Vector<String> 	v_align;
	// Liste des noms d'algos de quantification
	private Vector<String> 	v_quantif;
	// Liste des noms de plugins+algos d'alignement
	private Vector<String> 	modalign;
	// Liste des noms de plugins+algos de quantification
	private Vector<String> 	modquantif;
	// Liste des noms de plugins+algos+options de la configuration
	private Vector<String> 	modconfig;
	// Liste d'entiers representant les "categories" des elements de la configuration
	// 0 : plugin/algo d'alignement
	// 1 : plugin/algo de quantification
	// 3 : option snapshot
	// 4 : option exportation des resultats dans un fichier
	private Vector<Integer>	etatconfig;
	// JList des quantifications
	private JList quantif;
	// JList des alignements
	private JList align;
	// JList representant la configuration cree
	private JList config;
	// Bouton pour ajouter l'option export
	private JButton b_export;
	// Bouton pour ajouter l'option snapshot
	private JButton b_snap;
	// Bouton pour ajouter un plugin/algo d'alignement ou de quantification
	private JButton b_tiff;
	// Bouton pour ajouter un plugin/algo d'alignement ou de quantification
	private JButton b_add;
	// Bouton pour supprimer un plugin/algo d'alignement ou de quantification
	private JButton b_rem;
	// Bouton Ok
	private JButton b_ok;
	// Bouton Annuler
	private JButton b_an;
	// Memorise le nombre d'alignement presents dans la configuration
	private int conf_nbalign;
	// Memorise le nombre de quantifications presentes dans la configuration
	private int conf_nbquantif;

	
	/**** CLASSE POUR SURLIGNER EN COULEUR DANS UNE JLIST ****/
	public class CellulesCouleur extends JLabel implements ListCellRenderer {
	     // This is the only method defined by ListCellRenderer.
	     // We just reconfigure the JLabel each time we're called.

	     public Component getListCellRendererComponent(
	       JList list,
	       Object value,            // value to display
	       int index,               // cell index
	       boolean isSelected,      // is the cell selected
	       boolean cellHasFocus)    // the list and the cell have the focus
	     {
	        
	    	 String s = value.toString();
	         setText(s);
	   	   if (isSelected) {
	             setBackground(list.getSelectionBackground());
		       setForeground(list.getSelectionForeground());
		   }
	         else {
		       setBackground(list.getBackground());
		       setForeground(list.getForeground());
		   }
		   // Ici, tu rajoutes ton code pour changer la couleur de ta ligne :
		   if (etatconfig.get(index)==ALIGN)
		       setBackground(Color.CYAN);
		   else if (etatconfig.get(index)==QUANTIF)
			   setBackground(Color.YELLOW);
		   else if ((etatconfig.get(index)==OPTION_SNAP)||(etatconfig.get(index)==OPTION_TIFF)
				   ||(etatconfig.get(index)==OPTION_EXPORT))
			   setBackground(Color.ORANGE);

		   setEnabled(list.isEnabled());
		   setFont(list.getFont());
	         setOpaque(true);
	         return this;
	     }
	 }
	
	/**** CONSTRUCTEUR ****/
	public TProcessChooserDialog(String name)
	{
		// Titre
		this.setTitle("Choix des traitements");
		
		// Allocations memoires pour les listes de donnees
		v_align = new Vector<String>();
		v_quantif = new Vector<String>();
		etatconfig = new Vector<Integer>();
		
		/* Recuperation EN DUR des noms d'algos d'alignement */
		//detection de spots
		String spotdefault =((TAction)TMenuManager.getAction(TComputeSpotQualityDefaultAction.getID())).getValue(name).toString();
		String spot2prof = ((TAction)TMenuManager.getAction(TComputeSpotQualityTwoProfilesAction.getID())).getValue(name).toString();
		String spotmultichan = ((TAction)TMenuManager.getAction(TComputeSpotQualityMultiChannelAction.getID())).getValue(name).toString();
		String spot4prof =  ((TAction)TMenuManager.getAction(TComputeSpotQualityFourProfilesAction.getID())).getValue(name).toString();
		//alignement local
		String locdefault= ((TAction)TMenuManager.getAction(TGlobalAlignmentDefaultAction.getID())).getValue(name).toString();
		String locgrid= ((TAction)TMenuManager.getAction(TLocalAlignmentGridAction.getID())).getValue(name).toString();
		String loctempmatch = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocTemplateAction.getID())).getValue(name).toString();
		String locbloc = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocAction.getID())).getValue(name).toString();
		//alignement global
		String globdefault = ((TAction)TMenuManager.getAction(TGlobalAlignmentDefaultAction.getID())).getValue(name).toString();
		String globedges = ((TAction)TMenuManager.getAction(TGlobalAlignmentEdgesFinderAction.getID())).getValue(name).toString();
		String globtempmatch = ((TAction)TMenuManager.getAction(TGlobalAlignmentTemplateMatchingAction.getID())).getValue(name).toString();
		String globsniffer = ((TAction)TMenuManager.getAction(TGlobalAlignmentSnifferAction.getID())).getValue(name).toString();
		

		/* Recuperation EN DUR des noms d'algos de quantification */
		String quantimconst= ((TAction)TMenuManager.getAction(TComputeQuantifImageConstAction.getID())).getValue(name).toString();
		String quantimvar= ((TAction)TMenuManager.getAction(TComputeQuantifImageVarAction.getID())).getValue(name).toString();
		String quantfitconst= ((TAction)TMenuManager.getAction(TComputeQuantifFitConstAction.getID())).getValue(name).toString();
		String quantfitvar = ((TAction)TMenuManager.getAction(TComputeQuantifFitVarAction.getID())).getValue(name).toString();
		String quantdiam= ((TAction)TMenuManager.getAction(TComputeDiameterAction.getID())).getValue(name).toString();
		String quantovershcorr= ((TAction)TMenuManager.getAction(TComputeOvershiningCorrectionAction.getID())).getValue(name).toString();
		String quantfitcorr = ((TAction)TMenuManager.getAction(TComputeFitCorrectionAction.getID())).getValue(name).toString();
		String quantQM = ((TAction)TMenuManager.getAction(TComputeQMAction.getID())).getValue(name).toString();
		
		
		/* Ajout des noms aux vecteurs v_align et v_quantif */
		v_align.add(locdefault);
		v_align.add(locgrid);
		v_align.add(loctempmatch);
		v_align.add(locbloc);
		v_align.add(globdefault);
		v_align.add(globedges);
		v_align.add(globtempmatch);
		v_align.add(globsniffer);
		v_align.add(spotdefault);
		v_align.add(spot2prof);
		v_align.add(spotmultichan);
		v_align.add(spot4prof);
		v_quantif.add(quantimconst);
		v_quantif.add(quantimvar);
		v_quantif.add(quantfitconst);
		v_quantif.add(quantfitvar);
		v_quantif.add(quantdiam);
		v_quantif.add(quantovershcorr);
		v_quantif.add(quantfitcorr);
		v_quantif.add(quantQM);
		
		// bouton ok
		b_ok = new JButton("Ok");
		b_ok.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// si on a au moins 2 alignements et 1 quantif
				if(conf_nbalign>=2 && conf_nbquantif >=1)
				{
				
					// ajout des plugins/algo/options a la liste des taches (fichier des parametres)
					initParameters();
					
					// pour chaque element ajoute a la liste de config
					for(int i=0;i<etatconfig.size();i++)
					{
						switch(etatconfig.get(i))
						{
							case ALIGN : 			AGScan.prop.setProperty(
															"TProcessChooserAction.align",
															AGScan.prop.getProperty("TProcessChooserAction.align")+"#"+modconfig.get(i));
										 			break;
							case QUANTIF : 			AGScan.prop.setProperty(
															"TProcessChooserAction.quantif",
															modconfig.get(i));
													break;
							case OPTION_SNAP :		AGScan.prop.setProperty(
															"TProcessChooserAction.snap",
															"1");
													break;
							case OPTION_TIFF :		AGScan.prop.setProperty(
															"TProcessChooserAction.snap",
															"1");
													break;
							case OPTION_EXPORT :	AGScan.prop.setProperty(
															"TProcessChooserAction.export",
															"1");
													break;
						}
					}
					
					// fermeture de la fenetre
					dispose();
				}
			 	else{			
					TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Veuillez choisir au moins 2 alignements et 1 quantification.");// externalized 2006/02/02  //$NON-NLS-1$
					TEventHandler.handleMessage(event);
					return;
			 	}
			}
		});
		b_an = new JButton("Annuler");
		b_an.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// fermeture de la fenetre
				dispose();
			}
		});

			// bouton d'ajout d'un algo/plugin a la config.
			Icon ic_add = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/right.gif"));
			Action act_add = new AbstractAction("Ajouter",ic_add) {
				public void actionPerformed(ActionEvent e)
				{
					// si une quantif est selectionnee 
					// et qu il n y en a pas encore dans la config
					if(!quantif.isSelectionEmpty() && conf_nbquantif<1) 
					{
						int sel = quantif.getSelectedIndex();
						
						// on ajoute la quantif. a la liste de config.
						modconfig.add(modquantif.get(sel));
						config.setListData(modconfig); 		//refresh
						
						// on retire la quantif. de la liste des quantif.
						modquantif.removeElementAt(sel);
						quantif.setListData(modquantif); 	//refresh
						etatconfig.add(QUANTIF);
						conf_nbquantif++;
					}
					else if (!align.isSelectionEmpty() && conf_nbalign<2)
					{
						int sel = align.getSelectedIndex();
						
						// on ajoute l align. a la liste de config.
						modconfig.add(modalign.get(sel));
						config.setListData(modconfig); 		//refresh
						
						// on retire l align. de la liste des align.
						modalign.removeElementAt(sel);
						align.setListData(modalign); 		//refresh
						etatconfig.add(ALIGN);
						conf_nbalign++;
					}
				}
			};
			b_add = new JButton(act_add);
			
			// bouton de suppression d'un algo/plugin de la config.
			Icon ic_rem = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/undo_edit.gif"));
			Action act_rem = new AbstractAction("Supprimer",ic_rem) {
				public void actionPerformed(ActionEvent e)
				{
					// si on a selectionne un element dans la liste de config.
					if (!config.isSelectionEmpty())
					{
						int sel = config.getSelectedIndex();
						
						switch(etatconfig.get(sel))
						{
							// si c est un alignement
							case ALIGN : 	
											// on le remet dans la liste des align.
											modalign.add(modconfig.get(sel));
											align.setListData(modalign);		//refresh
										 	conf_nbalign--;
										 	break;
							
							// si c est une quantification
							case QUANTIF : 	
											// on le remet dans la liste des quantif.
											modquantif.add(modconfig.get(sel));
							 				quantif.setListData(modquantif);	//refresh
							 				conf_nbquantif--;
							 				break;
							
							// si c est une option 
							// on reactive le bouton correspondant
							case OPTION_SNAP : 	
											b_snap.setEnabled(true);
				 							break;
							case OPTION_TIFF : 	
											b_tiff.setEnabled(true);
											break;
							case OPTION_EXPORT : 	
											b_export.setEnabled(true);
				 							break;				
				 							
						}
						// on supprime l element selectionne de la liste de config.
						etatconfig.removeElementAt(sel);
						modconfig.removeElementAt(sel);		
						config.setListData(modconfig);		//refresh
					}
				}
			};
			b_rem = new JButton(act_rem);
			
			
			
			// Panel de gauche "pan" : listes des algos/plugins/options pour creer la config.
			// Panel de droite "configpan" :  liste des algos/plugins choisis 
			//								+ boutons ajout/suppression & boutons ok/annuler
			JPanel pan = new JPanel();
			//JPanel configpan = new JPanel();
			
			pan.setLayout(new BoxLayout(pan,BoxLayout.Y_AXIS));
			//configpan.setLayout(new BoxLayout(configpan,BoxLayout.Y_AXIS));
			
			// 	1 JList pour les quantifications disponibles
			// +1 JList pour les alignements disponibles
			// +1 JList pour la configuration choisie
			// +3 Vecteurs contenant les donnees associees a chacune des JList
			modalign = new Vector<String>();
			modquantif = new Vector<String>();
			modconfig = new Vector<String>();
			align = new JList(modalign);
			align.setVisibleRowCount(5);
			quantif = new JList(modquantif);
			quantif.setVisibleRowCount(5);
			config = new JList(modconfig);
			config.setVisibleRowCount(4);
			config.setCellRenderer(new CellulesCouleur());
			
			// Bouton pour activer le mode snapshot
			Icon ic_snap = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/zoozoom.gif"));
			Action act_snap = new AbstractAction(Messages.getString("TProcessChooserDialog.4"),ic_snap)
			{
				public void actionPerformed(ActionEvent e)
				{
						// on ajoute l option a la liste de config.
						modconfig.add("Mode Snapshot");			// A MODIFIER !!!
						config.setListData(modconfig); 			//refresh
						
						// on desactive le bouton de l'option
						b_snap.setEnabled(false);
						
						etatconfig.add(OPTION_SNAP);
				}
			};
			b_snap = new JButton(act_snap);
			
			// Button to activate tiff saving
			Icon ic_tiff = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/zoozoom.gif"));
			Action act_tiff = new AbstractAction(Messages.getString("TProcessChooserDialog.4"),ic_snap)
			{
				public void actionPerformed(ActionEvent e)
				{
						// on ajoute l option a la liste de config.
						modconfig.add("Mode Tiff");			// A MODIFIER !!!
						config.setListData(modconfig); 			//refresh
						
						// on desactive le bouton de l'option
						b_tiff.setEnabled(false);
						
						etatconfig.add(OPTION_TIFF);
				}
			};
			b_tiff = new JButton(act_tiff);
			
			// Bouton pour activer l'exportation des resultats 
			// de la quantification dans un fichier
			Icon ic_export = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/close.gif"));
			Action act_export = new AbstractAction(Messages.getString("TProcessChooserDialog.5"),ic_export)
			{
				public void actionPerformed(ActionEvent e)
				{
						// on ajoute l option a la liste de config.
						modconfig.add("Exportation des resultats");	// A MODIFIER !!!
						config.setListData(modconfig); 				//refresh
						
						// on desactive le bouton de l'option
						b_export.setEnabled(false);
						
						etatconfig.add(OPTION_EXPORT);
				}
			};
			b_export = new JButton(act_export);
			
			// Alignements : liste a selection unique
			// JList associee a 1 JScrollPane
			// 1 seul algo ou plugin de quantif/align peut etre selectionne
			align.addListSelectionListener(new ListSelectionListener()
			{
				// fonction appelee lorsqu'un element de la JList align est selectionne
				public void valueChanged(ListSelectionEvent e)
				{
					// si un autre element est selectionne dans une autre JList
					if(!(quantif.isSelectionEmpty()&&config.isSelectionEmpty()))
					{
						// on efface cette selection et on ne garde que la nouvelle
						int sel = align.getSelectedIndex();
						quantif.clearSelection();
						config.clearSelection();
						align.setSelectedIndex(sel);
					}
				}
			});
			align.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			JScrollPane scrollali = new JScrollPane(align);
			scrollali.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			
			// Quantifications : liste a selection unique
			// JList associee a 1 JScrollPane
			// 1 seul algo ou plugin de quantif/align peut etre selectionne
			quantif.addListSelectionListener(new ListSelectionListener()
			{
				// fonction appelee lorsqu'un element de la JList quantif est selectionne
				public void valueChanged(ListSelectionEvent e)
				{
					
					// si un autre element est selectionne dans une autre JList
					if(!(align.isSelectionEmpty()&&config.isSelectionEmpty()))
					{
						// on efface cette selection et on ne garde que la nouvelle
						int sel = quantif.getSelectedIndex();
						align.clearSelection();	
						config.clearSelection();
						quantif.setSelectedIndex(sel);
					}
					
				}
			});
			quantif.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			JScrollPane scrollquantif = new JScrollPane(quantif);
			scrollquantif.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			
			
			// Configuration choisie
			config.addListSelectionListener(new ListSelectionListener()
			{
				// fonction appelee lorsqu'un element de la JList config est selectionne
				public void valueChanged(ListSelectionEvent e)
				{
					
					// si un autre element est selectionne dans une autre JList
					if(!(align.isSelectionEmpty()&&quantif.isSelectionEmpty()))
					{
						// on efface cette selection et on ne garde que la nouvelle
						int sel = config.getSelectedIndex();
						align.clearSelection();	
						quantif.clearSelection();
						config.setSelectedIndex(sel);
					}
					
				}
			});
			config.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			config.setSelectionForeground(Color.WHITE);
			JScrollPane scrollconfig= new JScrollPane(config);
			scrollconfig.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	
			
			// Panel options : 3 boutons (snap & tiff & export)
			JPanel options = new JPanel();
			options.setLayout(new GridLayout(3,1));
			options.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.2")));
			options.add(b_snap);
			options.add(b_tiff);
			options.add(b_export);
			
			
			// Intitules des ScrollPane
			scrollali.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.0")));
			scrollquantif.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.1")));
			scrollconfig.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TProcessChooserDialog.6")));
			
			// Panel "boutpan" qui contient les boutons d'ajout/suppression
			JPanel boutpan = new JPanel();
			boutpan.setLayout(new FlowLayout());
			boutpan.add(b_add);
			boutpan.add(b_rem);
			
			// Panel "valpan" qui contient les boutons ok/annuler
			JPanel valpan = new JPanel();
			valpan.setLayout(new FlowLayout());
			valpan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.ORANGE),"Valider la configuration?"));
			valpan.add(b_ok);
			valpan.add(b_an);
			
			// Assemblage
			pan.add(scrollali);
			pan.add(scrollquantif);
			pan.add(boutpan);
			pan.add(scrollconfig);
			pan.add(options);
			pan.add(valpan);
			pan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),"Selection des traitements"));
			this.setContentPane(pan);
		
			// Initialisations
			init();
		
		
	}
	
	

	/**** INITIALISATIONS ****/
	public void init()
	{
		// Ajoute un titre 
		this.setTitle("Liste des plugins charges");
		// Ajuste la taille de la fenetre et l'affiche avec le Look&FeelDeco
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.setVisible(true);
		this.pack();
		
		// A l'init on a choisi 0 alignement et 0 quantif
		this.conf_nbalign = 0;
		this.conf_nbquantif = 0;

		// tri des plugins
		for (int i=0; i< TPluginManager.plugs.length;i++){
			TPlugin plugin = TPluginManager.plugs[i];
			if (plugin != null) {
				
				// plugin d'alignement ajoute a la liste correspondante
				if ((plugin instanceof SAlignPlugin)
						/*||(plugin instanceof SGridPlugin)*/)
				{
					this.modalign.add(plugin.getName());
					this.align.setListData(this.modalign);		//refresh
				}
				// plugin de quantification ajoute a la liste correspondante
				else if (plugin instanceof SQuantifPlugin)
				{
					this.modquantif.add(plugin.getName());
					this.quantif.setListData(this.modquantif);	//refresh
				}
			}
		}
		
		//tri des algos align
		for(int i=0;i<v_align.size();i++)
		{
			this.modalign.add(v_align.get(i));
			this.align.setListData(this.modalign);		//refresh
		}
		//tri des algos quantif
		for(int i=0;i<v_quantif.size();i++)
		{
			this.modquantif.add(v_quantif.get(i));
			this.quantif.setListData(this.modquantif); 	//refresh
		}
		
	}
	
	/**** INIT DES PARAMETRES ****/
	public void initParameters()
	{
		// Par defaut choisie la configuration est vide
		AGScan.prop.setProperty("TProcessChooserAction.export","0");	
		AGScan.prop.setProperty("TProcessChooserAction.snap","0");		
		AGScan.prop.setProperty("TProcessChooserAction.tiff","0");	
		AGScan.prop.setProperty("TProcessChooserAction.align","");
		AGScan.prop.setProperty("TProcessChooserAction.quantif","");
	}
	

}
