/*
 * Created on Aug 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.utils;

import javax.swing.table.DefaultTableModel;

import agscan.DefaultProperties;
import agscan.Messages;
import agscan.AGScan;

/**
 * @author rcathelin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TableParametersModel extends DefaultTableModel {
	String[] columnNames = {Messages.getString("TableParametersModel.0"),
			Messages.getString("TableParametersModel.1"),
			Messages.getString("TableParametersModel.2")
			};// les noms des colonnes //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	private int columnCount = 0; // le nombre de colonnes
	//	  private List rowValues = new ArrayList(); // Une liste pour stocker chaque ligne de valeur
	private boolean[] editable ; // booléen indiquant si la colonne du  modèle est éditable	
	private Object[][] data = new Object[AGScan.defaultProp.size()][3];
	private	Object[] keys = AGScan.defaultProp.keySet().toArray();//keys
	private Object[] defaultValues = AGScan.defaultProp.values().toArray();//default values
	private Object[] userValues = new Object[AGScan.defaultProp.size()];//user values, pas encore rempli...
	
	public TableParametersModel(){
		super();
		// remplissage des données pour la table 
		for (int i=0;i<AGScan.defaultProp.size();i++){
			data[i][0] = keys[i];
			//data[i][1] = userValues[i];
			data[i][2] = defaultValues[i];
		}
		updateModel();
	}
	/**
	 * update du model...
	 * 
	 */
	public void updateModel() {
		for (int i=0;i<userValues.length;i++){
			if (AGScan.prop.containsKey(keys[i]))
				userValues[i] = AGScan.prop.get(keys[i]);
			else userValues[i] = null;//champ a null si pas défini
		}
		
		// remplissage des données pour la table ( juste les prop qiu peuvent avoir changées = celles de l'utilisateur
		for (int i=0;i<AGScan.defaultProp.size();i++){
			//data[i][0] = keys[i];
			data[i][1] = userValues[i];
			//	data[i][2] = defaultValues[i];
		}
		
		// signale au Listener de ce TableModel qu'une nouvelle table a été constituée
		fireTableChanged(null);
		
	}
	
	
	// rédéfinitions des méthodes de l'interface TableModel
	
	
	//toutes les données sont des chaînes de caractères
	public Class getColumnClass(int columnIndex){
		return String.class;
	}
	
	public int getColumnCount() {
		return 3; // 3 colonnes
	}
	
	public int getRowCount() {
		return new DefaultProperties().size();// nb de param
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	}
	
	public Object getValueAt(int row, int col) {
		try{
			return data[row][col];
		}
		catch (NullPointerException ne){
			System.out.println("null pointer catché!"); //$NON-NLS-1$
			return null;
		}
	}
	
	public void setValueAt(Object o,int row, int col) {
		data[row][col] = o;
	}
	
	//	 surcharge de la methode afin de definir quelles celulles sont editables
	// on ne veut pouvoir editer que les propriétés de l'utilisateur
	//modification 2005/10/28 -  columns are non-editable (before the first was editable)...
	public boolean isCellEditable (int row, int column){   
		//if ( column == 1 )return true;
		//else
		return  false ;
	
	} 
	
	/*
	 public class MonListener implements TableModelListener{
	 public void tableChanged(TableModelEvent e){
	 System.out.println("modif!!!!!!!!");
	 //	 la boucle qui parcourt la table
	  //	 et qui teste si les cellules sont vides
	   }
	   }
	   */
	
}
