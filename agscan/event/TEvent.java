package agscan.event;

public class TEvent {
  int dest;
  int action;
  Object[] paramList;

  public TEvent() {
    dest = action = -1;
    paramList = null;
  }
  
  // les TEvent 
  // 3 infos par defaut:
  // -le destinataire = une des 6 classes qui implémentent l'interface TBZMessageListener
  // -l'action (que le destinataire devra traiter). Selon le destainataire, l'action est gérée ou sous traitée.ex: le TDataManager peut faire traiter l'action par le controleur de l'element ouvert (image, ali, grille ou batch)
  // -un ou plusieurs parametres: par ex l'algo utilisé pour au ali, la valeur d'un zoom
  // les params sont recupérés par event.getParam(i) avec i de 0 au nb de param)
  /*public TEvent(int dest, int act, Object[] params) {
	    this.dest = dest;
	    action = act;
	    paramList = params;
  }*/
  
  public TEvent(int dest, int act, Object param) {
    this.dest = dest;
    action = act;
    paramList = new Object[1];
    paramList[0] = param;
  }
  public TEvent(int dest, int act, Object param1, Object param2) {
    this.dest = dest;
    action = act;
    paramList = new Object[2];
    paramList[0] = param1;
    paramList[1] = param2;
  }
  public TEvent(int dest, int act, Object param1, Object param2, Object param3) {
    this.dest = dest;
    action = act;
    paramList = new Object[3];
    paramList[0] = param1;
    paramList[1] = param2;
    paramList[2] = param3;
  }
  public TEvent(int dest, int act, Object param1, Object param2, Object param3, Object param4) {
    this.dest = dest;
    action = act;
    paramList = new Object[4];
    paramList[0] = param1;
    paramList[1] = param2;
    paramList[2] = param3;
    paramList[3] = param4;
  }
  
  public int getDest() {
    return dest;
  }
  public int getAction() {
    return action;
  }
  public Object getParam(int i) {
    if (paramList != null) {
      if (paramList.length > i) {
        return paramList[i];
      }
    }
    return null;
  }
  public Object[] getParamList() {
    return paramList;
  }
  public void setParam(int i, Object param) {
    if (paramList.length > i) paramList[i] = param;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
