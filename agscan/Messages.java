/**
 * Created on Aug 8, 2005
 * Modification : 2005/12/06: modifications in order to access properties files outside of classes! 
 * In fact we use "Properties" class instead of "ResourceBundle" class
 * Now, a properties file is initialized into the init() method and used at call
 * @version 2005/12/06
 * @author rcathelin
 * 
 */
package agscan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import javax.swing.JOptionPane;


/**
 * @author rcathelin
 * @since August 10, 2005
 * This class allows the externationnalisation and the internationnalisation (translation)
 *  of the strings of the application thanks to the use of the files properties 
 */
public class Messages {
	
	private static final String JVM_LANGUAGE =  System.getProperty("user.dir")+File.separator+"languages"+File.separator+"messages_"+Locale.getDefault().getLanguage()+".properties"; //modified path 2005/12/05
	private static final String USERCONFIG_LANGUAGE = System.getProperty("user.dir")+File.separator+"languages"+File.separator+"messages_"+AGScan.prop.getProperty("language")+".properties";//path modified 2005/12/06	
	private static final String DEFAULT_LANGUAGE = System.getProperty("user.dir")+File.separator+"languages"+File.separator+"messages_en.properties"; //modified path  2005/12/06
	
	public static MyProperties languageProperties = new MyProperties();//TEST
	
	public Messages() {  
		FileInputStream in = null;
		
		//		 initialisation des parametres de l'application
		try {
			//at first we test languages paths and create the FileInputStream 
			//order of tests: perso config, JVM config and default config
			if (new File(USERCONFIG_LANGUAGE).exists())	
				in = new FileInputStream(USERCONFIG_LANGUAGE);
			else if (new File(JVM_LANGUAGE).exists())	
				in = new FileInputStream(JVM_LANGUAGE);
			else if (new File(DEFAULT_LANGUAGE).exists())	
				in = new FileInputStream(DEFAULT_LANGUAGE);
			else {
				System.err.println("NO LANGUAGE FILE FOUND!!!!!!");
				JOptionPane.showMessageDialog(null, "No languages file found!" ,"ERROR AT LAUNCH",JOptionPane.ERROR_MESSAGE);
				JOptionPane.showMessageDialog(null, "Please verify that the language directory contains at least a language file","TRY IT:",JOptionPane.INFORMATION_MESSAGE);
				System.exit(1);
			}
			//Loading of properties 
			languageProperties.load(in);//added 2005/12/06
			in.close();
			/*
			 } catch (FileNotFoundException e) {
			 e.printStackTrace();*/
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
	}  
	
	public static void display(){
		System.out.println("USERCONFIG_LANGUAGE="+USERCONFIG_LANGUAGE);//PERSO
		System.out.println("JVM_LANGUAGE="+JVM_LANGUAGE);//JVM
		System.out.println("DEFAULT_LANGUAGE="+DEFAULT_LANGUAGE);//DEFAULT
	}
	
	public static String getString(String key)  {
		return  languageProperties.getProperty(key);//TEST TOTAL
	}
}
