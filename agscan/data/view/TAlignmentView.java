package agscan.data.view;

import ij.ImagePlus;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import agscan.data.element.alignment.TAlignment;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridModel;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.element.grid.TGrid;

public class TAlignmentView extends TViewImpl {
  private TImageView imageView;
  private boolean scrolled, computedDiameters;

  public TAlignmentView(TAlignment alignment, TImageView imageView, TGridModel gridModel) {
    super(alignment);
    System.out.println("TAlignmentView");
    this.imageView = imageView;
    graphicPanel = new TAlignmentGraphicPanel(this, gridModel);
    setGraphicPanel();
    tablePanel = ((TGrid)gridModel.getReference()).getGridView().getTablePanel();
    gridModel.getConfig().getColumn(TGridConfig.COLUMN_DIAM).setEditable(true);
    setTablePanel();
    scrolled = computedDiameters = false;
  }
  public void setGridModel(TGridModel gm) {
    graphicPanel = new TAlignmentGraphicPanel(this, gm);
    setGraphicPanel();
    graphicScrollPane.repaint();
    tablePanel = ((TGrid)gm.getReference()).getGridView().getTablePanel();
    gm.getConfig().getColumn(TGridConfig.COLUMN_DIAM).setEditable(true);
    setTablePanel();
  }
  public ImagePlus getDisplayImage() {
    return imageView.getDisplayImage();
  }
  public boolean isScrolled() {
    return scrolled;
  }
  public boolean showComputedDiameters() {
    return computedDiameters;
  }
  public void setComputedDiameters(boolean b) {
    computedDiameters = b;
  }
  public TImageGraphicPanel getImageGraphicPanel() {
    return imageView.getImageGraphicPanel();
  }
  public TImageView getImageView() {
    return imageView;
  }
  public void setGraphicPanel() {
    super.setGraphicPanel();
    graphicScrollPane.getHorizontalScrollBar().addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent event) {
        scrolled = true;
        graphicScrollPane.repaint();
      }
      public void mouseReleased(MouseEvent event) {
        scrolled = false;
        graphicScrollPane.repaint();
      }
    });
    graphicScrollPane.getVerticalScrollBar().addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent event) {
        scrolled = true;
        graphicScrollPane.repaint();
      }
      public void mouseReleased(MouseEvent event) {
        scrolled = false;
        graphicScrollPane.repaint();
      }
    });
  }

}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
