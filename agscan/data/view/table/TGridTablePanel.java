/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.table;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;

import agscan.TEventHandler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TGridSelectionModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TGridView;

public class TGridTablePanel extends TTablePanel {
  class TableHeaderMouseListener extends MouseAdapter {
    public void mousePressed(MouseEvent event) {
      boolean paste = true;
      int i = table.getTableHeader().columnAtPoint(new Point(event.getX(), event.getY()));
      if (i > 0) {
        int b = ((TGridModel)table.getModel()).getSelectedColumn();
        //if (event.getButton() == event.BUTTON1)
        if (event.getButton() == MouseEvent.BUTTON1)
          if (b != i)
            ((TGridModel)table.getModel()).selectColumn(i);
          else
            ((TGridModel)table.getModel()).selectColumn(0);
        //else if (event.getButton() == event.BUTTON3)
        else if (event.getButton() == MouseEvent.BUTTON3)
        	
          ((TGridModel)table.getModel()).selectColumn(i);
        table.getTableHeader().repaint();
        //if (event.getButton() == event.BUTTON3) {
        if (event.getButton() == MouseEvent.BUTTON3) {
          TColumn col = (TColumn)((TGridModel)table.getModel()).getConfig().getColumns().getColumn(i);
          Vector spots;
          if (((TGridModel)table.getModel()).getSelectionModel().getRecursivelySelectedSpots().size() > 0)
            spots = ((TGridModel)table.getModel()).getSelectionModel().getRecursivelySelectedSpots();
          else
            spots = ((TGridModel)table.getModel()).getSpots();
          try {
            Transferable text = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this);
            String buffer = "";
            String[] tab = null;
            paste = text.isDataFlavorSupported(DataFlavor.stringFlavor);
            if (paste) {
              buffer = (String)text.getTransferData(DataFlavor.stringFlavor);
              tab = buffer.split("\n");
              paste = (tab.length == spots.size());
            }
            if (paste) paste = col.isCompatible(tab);
            col.getPopupMenu((TGrid)(getView().getReference()), paste).show(table.getTableHeader(), event.getX(), event.getY());
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        }
      }
      else if ((i == 0) && (event.getButton() == MouseEvent.BUTTON3)) {
        TColumn col = (TColumn)((TGridModel)table.getModel()).getConfig().getColumns().getColumn(i);
        col.getPopupMenu((TGrid)(getView().getReference()), false).show(table.getTableHeader(), event.getX(), event.getY());
      }
    }
  }
  private ListSelectionListener lss = new ListSelectionListener() {
    public void valueChanged(ListSelectionEvent event) {
      TEvent ev;
      ListSelectionModel lsm = (ListSelectionModel)event.getSource();
      int firstIndex = event.getFirstIndex();
      int lastIndex = event.getLastIndex();
      for (int i = firstIndex; i <= lastIndex; i++) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_SPOT_INDEX, null, new Integer(i));
        if (lsm.isSelectedIndex(i))
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT, null, TEventHandler.handleMessage(ev)[0],
                          new Boolean(!lsm.getValueIsAdjusting()));
        else
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT, null, TEventHandler.handleMessage(ev)[0]);
        TEventHandler.handleMessage(ev);
      }
    }
  };

  private TableHeaderMouseListener thml;

  public TGridTablePanel(TGridView v) {
    super();
    setView(v);
    table.setModel((TGridModel)v.getReference().getModel());
    table.setRowHeight(25);
    table.setSelectionBackground(Color.yellow);
    table.setSelectionForeground(Color.black);
    DefaultTableColumnModel columns = ((TGridModel)v.getReference().getModel()).getConfig().getColumns();
    table.setColumnModel(columns);
    initColumnSizes(false);
    table.getSelectionModel().addListSelectionListener(lss);
    thml = new TableHeaderMouseListener();
    table.getTableHeader().addMouseListener(thml);
  }
  public void updateSelection(TGridSelectionModel selectionModel) {
    table.getSelectionModel().removeListSelectionListener(lss);
    Vector v = selectionModel.getRecursivelySelectedSpots();
    TSpot spot;
    table.clearSelection();
    int i;
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
      spot = (TSpot)enume.nextElement();
      i = spot.getModel().getSpotRow(spot.getIndex());
      table.addRowSelectionInterval(i, i);
    }
    table.getSelectionModel().addListSelectionListener(lss);
  }
  public void showSpot(TSpot spot) {
    tableScrollPane.getViewport().setViewPosition(new Point(0, 0));
    tableScrollPane.getViewport().scrollRectToVisible(
        new Rectangle(0, (int)(spot.getModel().getSpotRow(spot.getIndex()) * table.getRowHeight() + tableScrollPane.getViewport().getViewRect().getHeight() / 2), 10, 10));
  }
  public void unselect(Vector v) {
    table.getSelectionModel().removeListSelectionListener(lss);
    if (v != null) {
      TSpot spot;
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        spot = (TSpot)enume.nextElement();
        table.removeRowSelectionInterval(spot.getIndex(), spot.getModel().getSpotIndex(spot.getIndex()));
      }
    }
    else
      table.clearSelection();
    table.getSelectionModel().addListSelectionListener(lss);
  }
  public void addColumn(TColumn column) {
    table.addColumn(column);
    initColumnSizes(column);
  }
  public ListSelectionListener getListSelectionListener() {
    return lss;
  }
  public void setSelectable(boolean s) {
    super.setSelectable(s);
    if (s)
      table.getTableHeader().addMouseListener(thml);
    else
      table.getTableHeader().removeMouseListener(thml);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
