package agscan.data.view.table;

import javax.swing.JPopupMenu;


import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TJob;
import agscan.data.view.table.action.TCancelAction;
import agscan.data.view.table.action.TMakePlotAction;
import agscan.data.view.table.action.TOpenAlignmentAction;
import agscan.data.view.table.action.TRAZStatusAction;
import agscan.data.view.table.action.TRemoveSelectionAction;
import agscan.data.view.table.action.TShowAlignmentAction;
import agscan.data.element.batch.TBatch;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchTablePopupMenu extends JPopupMenu {
  private TMakePlotAction mpa;
  private TRemoveSelectionAction rsa;
  private TRAZStatusAction razsa;
  private TShowAlignmentAction saa;
  private TOpenAlignmentAction oaa;
  private TCancelAction ca;
  public TBatchTablePopupMenu() {
    super();
    mpa = new TMakePlotAction("Plot", null);
    rsa = new TRemoveSelectionAction(null);
    razsa = new TRAZStatusAction(null);
    saa = new TShowAlignmentAction(null);
    oaa = new TOpenAlignmentAction(null);
    ca = new TCancelAction(null);
    add(rsa);
    add(razsa);
    add(saa);
    add(oaa);
    add(mpa);
    addSeparator();
    add(ca);
  }
  public void setPlotLabel(String s) {
    removeAll();
    mpa = new TMakePlotAction(s, null);
    add(rsa);
    add(razsa);
    add(saa);
    add(oaa);
    add(mpa);
    addSeparator();
    add(ca);
  }
  public void refresh(TBatch batch) {
  	System.out.println("c quoi ici?");
    boolean b;
    int[] sel = ((TBatchTablePanel)batch.getView().getTablePanel()).getSelectedRows();
    System.out.println("TAILLE DE SEL : "+sel.length);
    int n = 0, v;
    for (int i = 0; i < sel.length; i++) {
      TBatchModelNImages mod = ((TBatchModelNImages)batch.getModel());
      v = ((Integer)mod.getValueAt(i, mod.getColumnCount()+1)).intValue();
      if (v == TJob.DONE) n++;
    }
    if (!batch.isBatchRunning()) {
      mpa.setEnabled(n >= 2);
      rsa.setEnabled(sel.length >= 0);
      razsa.setEnabled(sel.length >= 0);
      saa.setEnabled(sel.length == 1);
      oaa.setEnabled(sel.length == 1);
      ca.setEnabled(true);
    }
    else {
      mpa.setEnabled(false);
      rsa.setEnabled(false);
      razsa.setEnabled(false);
      b = (sel.length == 1);
      if (!b) {
        saa.setEnabled(false);
        oaa.setEnabled(false);
      }
      else {
        int i = sel[0];
        v = ((Integer)((TBatchModelNImages)batch.getModel()).getValueAt(i,((TBatchModelNImages)batch.getModel()).getColumnCount()+1)).intValue();
        b = b && ((v == TJob.DONE) || (v == TJob.FAILED));
        saa.setEnabled(b);
        oaa.setEnabled(b);
      }
      ca.setEnabled(true);
    }
  }
}
