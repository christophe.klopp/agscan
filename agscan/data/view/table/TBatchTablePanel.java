package agscan.data.view.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;


import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.element.batch.TBatch;
import agscan.data.view.TBatchComPanel;
import agscan.data.view.TBatchComandPanel;
import agscan.data.view.TBatchView;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchTablePanel extends TTablePanel {
  public static class BatchTableHeaderRenderer extends DefaultTableCellRenderer {
    private Font font;
    public BatchTableHeaderRenderer() {
      font = new Font("serif", Font.BOLD, 12);
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      JLabel lab = new JLabel((String)value);
      if (column > 0) {
        lab.setBorder(BorderFactory.createRaisedBevelBorder());
        lab.setHorizontalAlignment(CENTER);
        lab.setHorizontalTextPosition(JLabel.LEFT);
        lab.setFont(font);
      }
      return lab;
    }
  }
  private ListSelectionListener lss = new ListSelectionListener() {
    public void valueChanged(ListSelectionEvent event) {
     // popupMenu.refresh((TBatch)view.getReference());
    }
  };
  private JSplitPane splitPane;
 
  private TBatchComPanel bcp;
  
  private TBatchTablePopupMenu popupMenu;

  public TBatchTablePanel(TBatchView v) {
    super();
    setView(v);
    bcp = new TBatchComPanel((TBatch)v.getReference());
    bcp.init((TBatch)v.getReference());
    
    /* NMARY : modif 4/04 :  
    	echange des parties gauche et droite
   		reorganisation du panel
   		prise en compte de l ouverture de N images
   	*/
    splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, new JScrollPane(bcp), tableScrollPane);
    splitPane.setResizeWeight(0.6);
    add(splitPane, BorderLayout.CENTER);
    table.setModel((TBatchModelNImages)v.getReference().getModel());
    table.setRowHeight(25);
    table.getSelectionModel().addListSelectionListener(lss);
    table.setSelectionBackground(Color.yellow);
    table.setSelectionForeground(Color.black);
    DefaultTableColumnModel columns = ((TBatchModelNImages)v.getReference().getModel()).getColumns();
    table.setColumnModel(columns);
    //initColumnSizes(false);
    popupMenu = new TBatchTablePopupMenu();
    table.add(popupMenu);
    table.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent event) {
        if ((event.getButton() == MouseEvent.BUTTON3) && (table.getSelectedRowCount() > 0)){
          if (table.getSelectedRowCount() > 2)
            popupMenu.setPlotLabel("RBC Plot");
          else if (table.getSelectedRowCount() == 2)
            popupMenu.setPlotLabel("Plot");
          popupMenu.refresh((TBatch)view.getReference());
          popupMenu.show(table, event.getX(), event.getY());
        }
      }
    });
  }
  public TBatchTablePopupMenu getPopupMenu() {
    return popupMenu;
  }
  public void refresh() {
    table.tableChanged(new TableModelEvent(table.getModel()));
    table.invalidate();
    table.repaint();

    bcp.init((TBatch)view.getReference());
    //bcp.refreshJob();
    repaint();
  }
  public int[] getSelection() {
    return table.getSelectedRows();
  }
  public int[] getSelectedRows() {
    return table.getSelectedRows();
  }
  public void selectAll() {
    table.selectAll();
  }
  public void update() {
    bcp.update();
  }
  public TBatchComPanel getBCP() {
	  return bcp;
  }
  /*
  public void setShowCurrentAlignmentCheckBoxSelected(boolean b) {
    bcp.setShowCurrentAlignmentCheckBoxSelected(b);
  }*/
}
