package agscan.data.view.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import agscan.TEventHandler;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TViewImpl;

public abstract class TTablePanel extends JPanel {
  protected JTable table;
  protected TViewImpl view;//private avant integration 13/09/05
  protected JScrollPane tableScrollPane;

  // modif NMARY 05/04/07
  public JTable getTable()
  {
	  return table;
  }
  
  public TTablePanel() {
    view = null;
    table = new JTable();
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.getTableHeader().setReorderingAllowed(false);
    setLayout(new BorderLayout());
    tableScrollPane = new JScrollPane(table);
    add(tableScrollPane, BorderLayout.CENTER);
  }
  public void refresh() {
    remove(tableScrollPane);
    tableScrollPane = new JScrollPane(table);
    add(tableScrollPane, BorderLayout.CENTER);
    revalidate();
    repaint();
  }
  public TViewImpl getView() {
    return view;
  }
  public void setView(TViewImpl v) {
    view = v;
  }
  public JScrollPane getScrollPane() {
    return tableScrollPane;
  }
  public void initColumnSizes(boolean progBar) {
    Enumeration columns = table.getColumnModel().getColumns();
    if (columns.hasMoreElements()) {
      TColumn column;
      int inc = 70 / table.getColumnModel().getColumnCount(), ix = 0;
      while (columns.hasMoreElements()) {
        column = (TColumn) columns.nextElement();
        int index = column.getModelIndex();
        Component comp = null;
        TableCellRenderer headerRenderer = column.getHeaderRenderer();
        if (headerRenderer == null) headerRenderer = table.getTableHeader().getDefaultRenderer();
        comp = headerRenderer.getTableCellRendererComponent(table, column.getHeaderValue(), false, false, 0, index);
        int headerWidth = comp.getPreferredSize().width;
        int cellWidth = 0;
        for (int i = 0; i < table.getModel().getRowCount(); i++) {
          comp = column.getCellRenderer().getTableCellRendererComponent(table, table.getModel().getValueAt(i, index), false,
              false, i, index);
          cellWidth = Math.max(comp.getPreferredSize().width, cellWidth);
        }
        column.setPreferredWidth(Math.max(headerWidth, cellWidth) + 10);
        ix++;
        if (progBar) {
          TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null,
                                    new Integer(20 + inc * ix));
          TEventHandler.handleMessage(event);
        }
      }
    }
  }
  public void initColumnSizes(TColumn column) {
    int index = column.getModelIndex();
    Component comp = null;
    TableCellRenderer headerRenderer = column.getHeaderRenderer();
    if (headerRenderer == null) headerRenderer = table.getTableHeader().getDefaultRenderer();
    comp = headerRenderer.getTableCellRendererComponent(null, column.getHeaderValue(), false, false, 0, 0);
    int headerWidth = comp.getPreferredSize().width;
    int cellWidth = 0;
    for (int i = 0; i < table.getModel().getRowCount(); i++) {
      comp = column.getCellRenderer().getTableCellRendererComponent(table, table.getModel().getValueAt(i, index), false, false, i, index);
      cellWidth = Math.max(comp.getPreferredSize().width, cellWidth);
    }
    column.setPreferredWidth(Math.max(headerWidth, cellWidth) + 10);
  }
  public Vector getSynchroneColumnsKeys() {
    Vector v = new Vector();
    Enumeration columns = table.getColumnModel().getColumns();
    TColumn col;
    while (columns.hasMoreElements()) {
      col = (TColumn)columns.nextElement();
      if (col.isSynchrone()) {
        v.addElement(col.getSpotKey());
      }
    }
    return v;
  }
  public void setSelectable(boolean s) {
    table.setRowSelectionAllowed(s);
    //table.setCellSelectionEnabled(s);
    table.setEnabled(s);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
