package agscan.data.view.table.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import agscan.TEventHandler;
import agscan.data.controler.TBatchControler;
import agscan.event.TEvent;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TRemoveSelectionAction extends AbstractAction {
  public TRemoveSelectionAction(ImageIcon icon) {
    super("Supprimer", icon);
  }
  public void actionPerformed(ActionEvent e) {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.REMOVE_SELECTED_JOBS, null);
    TEventHandler.handleMessage(event);
  }
}
