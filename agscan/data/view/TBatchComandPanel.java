package agscan.data.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.globalalignment.TEdgesFinderGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.TImageControler;
import agscan.data.model.batch.TBatchModel;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.batch.job.TExportResultsJob;
import agscan.data.model.batch.job.TJobBuilder;
import agscan.data.view.table.TBatchTablePanel;
import agscan.dialog.TGlobalAlignmentDialog;
import agscan.dialog.TLocalAlignmentDialog;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;
import agscan.data.element.batch.TBatch;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 * modified 2005/11/04
 * modified: string externalization since 2006/02/20 
 * @author Fabrice Lopez
 */
public class TBatchComandPanel extends JPanel {
  private JPanel northPanel = new JPanel();
  private JPanel centerPanel = new JPanel();
  private JPanel southPanel = new JPanel();
  private JLabel gridLabel = new JLabel();
  private JTextField gridTextField = new JTextField();
  private JButton gridButton = new JButton();
  private JButton addButton = new JButton();
  private JButton selectAllButton = new JButton();
  private JLabel outputDirectoryLabel = new JLabel();
  private JTextField outputDirectoryTextField = new JTextField();
  private JButton outputDirectoryButton = new JButton();
  private JCheckBox saveImagesCheckBox = new JCheckBox();
  private JPanel northButtonsPanel = new JPanel();
  private JButton launchButton = new JButton();
  private JPanel showSouthPanel = new JPanel();
  private JCheckBox showCurrentAlignmentCheckBox = new JCheckBox();
  private JButton RAZGraphicButton = new JButton();
  private JPanel treatmentPanel = new JPanel();
  private JCheckBox treatmentCheckBox = new JCheckBox();
  private JLabel rotationLabel = new JLabel();
  private JLabel flipLabel = new JLabel();
  private JPanel treatmentParamsPanel = new JPanel();
  private JRadioButton horizontalFlipRadioButton = new JRadioButton();
  private JRadioButton verticalFlipRadioButton = new JRadioButton();
  private JRadioButton plus90RotationRadioButton = new JRadioButton();
  private JRadioButton minus90RotationRadioButton = new JRadioButton();
  private JRadioButton plus180RotationRadioButton = new JRadioButton();
  private JPanel globalAlignmentPanel = new JPanel();
  private JCheckBox globalAlignmentCheckBox = new JCheckBox();
  private JPanel globalAlignmentParamsPanel = new JPanel();
  private JRadioButton edgesFinderRadioButton = new JRadioButton();
  private JRadioButton snifferRadioButton = new JRadioButton();
  private JButton globalAlignmentParamsButton = new JButton();
  private JPanel localAlignmentPanel = new JPanel();
  private JCheckBox localAlignmentCheckBox = new JCheckBox();
  private JPanel localAlignmentParamsPanel = new JPanel();
  private JRadioButton localAlignmentGridRadioButton = new JRadioButton();
  private JRadioButton localAlignmentBlockRadioButton = new JRadioButton();
  private JButton localAlignmentButton = new JButton();
  private JPanel quantifPanel = new JPanel();
  private JCheckBox quantifCheckBox = new JCheckBox();
  private JPanel quantifListPanel = new JPanel();
  private JCheckBox quantifImageConstantCheckBox = new JCheckBox();
  private JCheckBox quantifImageComputedCheckBox = new JCheckBox();
  private JCheckBox quantifFitConstantCheckBox = new JCheckBox();
  private JCheckBox quantifFitComputedCheckBox = new JCheckBox();
  private JCheckBox qmCheckBox = new JCheckBox();
  private JCheckBox fitCorrectionCheckBox = new JCheckBox();
  private JCheckBox overshiningCorrectionCheckBox = new JCheckBox();
  private JPanel exportPanel = new JPanel();
  private JPanel exportListPanel = new JPanel();
  private JCheckBox exportResultsCheckBox = new JCheckBox();
  private JCheckBox exportTextCheckBox = new JCheckBox();
 // private JCheckBox exportMageMLCheckBox = new JCheckBox();//2005/12/01 removed as long as MageML is not available

  private TBatch batch = null;
  //private File currentDirectoryImages = null;//removed 2005/11/29
  //private File currentDirectoryGrid = null;
 // private File currentDirectoryOutput = null;
  private ButtonGroup bgTreatment, bgGA, bgLA;
  
  //added 2005/11/04
  private JLabel jLabelChannels = new JLabel();  
  private JSpinner jSpinnerChannels = new JSpinner(new SpinnerNumberModel(1,1,3,1));//default,min,max and step values
  private int nbChannels = 1;
  //TODO max images mis a 3 mais mettre cette valeur en param globale pour augmenter par la suite

  public TBatchComandPanel(TBatch b) {
    batch = b;
    try {
      jbInit();
      initListeners();
      init(batch);
      refresh();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public void initListeners() {
    gridButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("gridsPath"));//remi 2005/11/29
        //if (currentDirectoryGrid != null) chooser.setCurrentDirectory(currentDirectoryGrid);//removed 2005/11/29
        ImageFilter ff = new ImageFilter("grd", "Modeles de grille (*.grd)");
        chooser.addChoosableFileFilter(ff);
        //chooser.addChoosableFileFilter(new ImageFilter("xml", "Fichiers MAGE-ML (*.xml)"));//removed 2005/11/24
        chooser.setFileFilter(ff);
        int returnVal = chooser.showOpenDialog(null);
       // currentDirectoryGrid = chooser.getCurrentDirectory();//removed 2005/11/29
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File gridFile = chooser.getSelectedFile();
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SET_GRID, null, gridFile);
          TEventHandler.handleMessage(ev);
          gridTextField.setText(gridFile.getName());
          batch.getModel().addModif(1);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          AGScan.prop.setProperty("gridsPath",gridFile.getParent());//added remi 2005/11/29
        }
      }
    });
    outputDirectoryButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("batchPath"));//remi 2005/11/29
    //    if (currentDirectoryOutput != null) chooser.setCurrentDirectory(currentDirectoryOutput);//removed 2005/11/29
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        //currentDirectoryOutput = chooser.getCurrentDirectory();//removed 2005/11/29
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File outputDir = chooser.getSelectedFile();
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SET_OUTPUT_PATH, null, outputDir.getAbsolutePath());
          TEventHandler.handleMessage(ev);          
          outputDirectoryTextField.setText(outputDir.getAbsolutePath());
          AGScan.prop.setProperty("batchPath",outputDir.getAbsolutePath());//added remi 2005/11/29
          batch.getModel().addModif(1);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
        }
      }
    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
      //	for (int i=0;i<nbChannels;i++){//aded 2005/11/04
        JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("imagesPath"));//modified 2005/11/29
        //if (currentDirectoryImages != null) chooser.setCurrentDirectory(currentDirectoryImages);//replaced remi 2005/11/29
        chooser.setMultiSelectionEnabled(true);
        chooser.addChoosableFileFilter(new ImageFilter("inf", "Fuji images (*.inf)"));
        //modified 2005/11/24
        //TODO make filter intelligent (plugins recognize...)
       // chooser.addChoosableFileFilter(new ImageFilter("pcb", "Tina images (*.pcb)"));
       // chooser.addChoosableFileFilter(new ImageFilter("jpg", "JPEG images (*.jpg)"));
        chooser.addChoosableFileFilter(new ImageFilter("tif", "TIFF images (*.tif)"));
       // chooser.addChoosableFileFilter(new ImageFilter("gif", "GIF images (*.gif)"));
       // chooser.addChoosableFileFilter(new ImageFilter("png", "PNG images (*.png)"));
       // chooser.addChoosableFileFilter(new ImageFilter("bmp", "BMP images (*.bmp)"));
        String[] exts = {"inf", "tif"};//reduced 2005/11/24
        chooser.setFileFilter(new ImageFilter(exts, "All images (*.inf *.tif )"));//reduced 2005/11/24
        int returnVal = chooser.showOpenDialog(null);
        //currentDirectoryImages = chooser.getCurrentDirectory();
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File[] files = chooser.getSelectedFiles();        
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.ADD_JOBS, null, files,new Integer(nbChannels));//modified 2005/11/04
          TEventHandler.handleMessage(ev);
          batch.getModel().addModif(1);
          AGScan.prop.setProperty("imagesPath",chooser.getCurrentDirectory().getAbsolutePath());//added remi 2005/11/29
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          //here if an image is chosen, we block the spinner (when a spinner
          jSpinnerChannels.setEnabled(false);
        }
     // 	}
        
      }
    });

    //added 2005/11/04  
     jSpinnerChannels.addChangeListener(new ChangeListener() {
    public void stateChanged(ChangeEvent event) {
        //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.SPOT_HEIGHT, null, level1RowHeightSpinbox.getValue());
        //TEventHandler.handleMessage(ev);
        nbChannels = ((Integer)jSpinnerChannels.getValue()).intValue();
        System.out.println("nbChannels = "+nbChannels);
        // pour maj l'inter-ligne en mm temps on ajoute le contenu de level2RowSpacingSpinboxChangeListener
    	//TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_ROW_SPACING, null, new Double(level2RowSpacing));
        //TEventHandler.handleMessage(ev2);
        //TODO bosser ici
        //TBatchModel model =(TBatchModel)batch.getModel();
        //model.setChannelsColumns(nbChannels);//the number of channels to add - added 2005/11/04
        //batch.setModel(model);
        //((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
       // TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.CHANGE_TABLE, null,new Integer(nbChannels));//modified 2005/11/04
       // TEventHandler.handleMessage(ev);
    //    batch.getModel().addModif(1);
      //  batch.getView().setTablePanel();
   
      }
    });
    //end added
     
    showCurrentAlignmentCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SHOW_CURRENT_ALIGNMENT, null, new Boolean(showCurrentAlignmentCheckBox.isSelected()));
        TEventHandler.handleMessage(ev);
      }
    });
    RAZGraphicButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.RAZ_SHOW, null);
        TEventHandler.handleMessage(ev);
      }
    });
    //action that launch the batch
    launchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        update();
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.RUN, null);
        TEventHandler.handleMessage(ev);
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    treatmentCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        plus90RotationRadioButton.setEnabled(treatmentCheckBox.isSelected());
        minus90RotationRadioButton.setEnabled(treatmentCheckBox.isSelected());
        plus180RotationRadioButton.setEnabled(treatmentCheckBox.isSelected());
        horizontalFlipRadioButton.setEnabled(treatmentCheckBox.isSelected());
        verticalFlipRadioButton.setEnabled(treatmentCheckBox.isSelected());
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    globalAlignmentCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        edgesFinderRadioButton.setEnabled(globalAlignmentCheckBox.isSelected());
        snifferRadioButton.setEnabled(globalAlignmentCheckBox.isSelected());
        globalAlignmentParamsButton.setEnabled(globalAlignmentCheckBox.isSelected());
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    localAlignmentCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        localAlignmentGridRadioButton.setEnabled(localAlignmentCheckBox.isSelected());
        localAlignmentBlockRadioButton.setEnabled(localAlignmentCheckBox.isSelected());
        localAlignmentButton.setEnabled(localAlignmentCheckBox.isSelected());
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    quantifCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        quantifImageConstantCheckBox.setEnabled(quantifCheckBox.isSelected());
        quantifImageComputedCheckBox.setEnabled(quantifCheckBox.isSelected());
        quantifFitConstantCheckBox.setEnabled(quantifCheckBox.isSelected());
        quantifFitComputedCheckBox.setEnabled(quantifCheckBox.isSelected());
        qmCheckBox.setEnabled(quantifCheckBox.isSelected());
        fitCorrectionCheckBox.setEnabled(quantifCheckBox.isSelected());
        overshiningCorrectionCheckBox.setEnabled(quantifCheckBox.isSelected());
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    exportResultsCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        exportTextCheckBox.setEnabled(exportResultsCheckBox.isSelected());
      //  exportMageMLCheckBox.setEnabled(exportResultsCheckBox.isSelected());//2005/12/01 removed as long as MageML is not available
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    globalAlignmentParamsButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        TGlobalAlignmentDialog dialog = null;
        TGlobalAlignmentAlgorithm algo = ((TBatchModel)batch.getModel()).getJob().getGlobalAlignmentAlgorithm();
        if (algo == null) algo = new TSnifferGlobalAlignmentAlgorithm();
        algo.getControlPanel().init(algo);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        dialog = new TGlobalAlignmentDialog(algo);
        if (dialog != null) {
          dialog.pack();
          dialog.setLocation( (screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
          dialog.setVisible(true);
          batch.getModel().addModif(1);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
        }
      }
    });
    localAlignmentButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        TLocalAlignmentDialog dialog = null;
        TLocalAlignmentAlgorithm algo = ((TBatchModel)batch.getModel()).getJob().getLocalAlignmentAlgorithm();
        if (algo == null) algo = new TLocalAlignmentBlockAlgorithm();
        algo.getControlPanel().init(algo);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        dialog = new TLocalAlignmentDialog(algo);
        if (dialog != null) {
          dialog.pack();
          dialog.setLocation( (screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
          dialog.setVisible(true);
          batch.getModel().addModif(1);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
        }
      }
    });
    selectAllButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SELECT_ALL, null);
        TEventHandler.handleMessage(ev);
      }
    });
    saveImagesCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SAVE_IMAGES, null,
                               new Boolean(saveImagesCheckBox.isSelected()));
        TEventHandler.handleMessage(ev);
        batch.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
  }
  private TAbstractJob processJob() {
    String job = "";
    TJobBuilder.createJob();
    Vector v;

    if (exportResultsCheckBox.isSelected()) {
      if (exportTextCheckBox.isSelected()) {
        TJobBuilder.addExportResultsJob(TExportResultsJob.TEXT);
      }
      //2005/12/01 removed as long as MageML is not available
      /*
        if (exportMageMLCheckBox.isSelected()) {
        TJobBuilder.addExportResultsJob(TExportResultsJob.MAGE_ML);
      }*/
    }
    if (quantifCheckBox.isSelected()) {
      if (overshiningCorrectionCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TComputeOvershiningCorrectionAlgorithm.NAME);
      }
      if (fitCorrectionCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TComputeFitCorrectionAlgorithm.NAME);
      }
      if (qmCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TComputeQMAlgorithm.NAME);
      }
      if (quantifFitComputedCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TQuantifFitComputedAlgorithm.NAME);
      }
      if (quantifFitConstantCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TQuantifFitConstantAlgorithm.NAME);
      }
      if (quantifImageComputedCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TQuantifImageComputedAlgorithm.NAME);
      }
      if (quantifImageConstantCheckBox.isSelected()) {
        TJobBuilder.addComputeJob(TQuantifImageConstantAlgorithm.NAME);
      }
    }
    if (localAlignmentCheckBox.isSelected()) {
      if (localAlignmentGridRadioButton.isSelected()) {
        v = new Vector();
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastBootstrapSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastBootstrapIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastFirstRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastFirstRunIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastSecondRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastSecondRunIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastThirdRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastThirdRunIterations));
        v.addElement(new Double(TLocalAlignmentGridAlgorithm.lastSensitivityThreshold));
        Object[] o1 = new Object[4];
        o1[0] = new Double(TLocalAlignmentGridAlgorithm.lastLocT);
        o1[1] = new Double(TLocalAlignmentGridAlgorithm.lastLocC);
        o1[2] = TLocalAlignmentGridAlgorithm.lastLocSpotDetectionAlgorithmName;
        o1[3] = TLocalAlignmentGridAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        v.addElement(o1);
        TJobBuilder.addLocalAlignmentJob(TLocalAlignmentGridAlgorithm.NAME, v);
      }
      else if (localAlignmentBlockRadioButton.isSelected()) {
        v = new Vector();
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunIterations));
        v.addElement(new Double(TLocalAlignmentBlockAlgorithm.lastSensitivityThreshold));
        v.addElement(new Boolean(TLocalAlignmentBlockAlgorithm.lastGridCheck));
        Object[] o1 = new Object[4];
        o1[0] = new Double(TLocalAlignmentBlockAlgorithm.lastLocT);
        o1[1] = new Double(TLocalAlignmentBlockAlgorithm.lastLocC);
        o1[2] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmName;
        o1[3] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        v.addElement(o1);
        TJobBuilder.addLocalAlignmentJob(TLocalAlignmentBlockAlgorithm.NAME, v);
      }
    }
    if (globalAlignmentCheckBox.isSelected()) {
      if (edgesFinderRadioButton.isSelected()) {
        v = new Vector();
        v.addElement(new Double(TEdgesFinderGlobalAlignmentAlgorithm.lastSizeSearchZone));
        v.addElement(new Integer(TEdgesFinderGlobalAlignmentAlgorithm.lastSizeSearchZoneIncrement));
        v.addElement(new Integer(TEdgesFinderGlobalAlignmentAlgorithm.lastEdgesSearchZone));
        v.addElement(new Integer(TEdgesFinderGlobalAlignmentAlgorithm.lastSearchAngle));
        v.addElement(new Integer(TEdgesFinderGlobalAlignmentAlgorithm.lastContrastPercent));
        v.addElement(new Double(TEdgesFinderGlobalAlignmentAlgorithm.lastSearchAngleIncrement));
        v.addElement(TEdgesFinderGlobalAlignmentAlgorithm.lastSpotDetectionAlgorithmName);
        v.addElement(TEdgesFinderGlobalAlignmentAlgorithm.lastLocSpotDetectionAlgorithmParameters);
        TJobBuilder.addGlobalAlignmentJob(TEdgesFinderGlobalAlignmentAlgorithm.NAME, v);
      }
      else if (snifferRadioButton.isSelected()) {
        v = new Vector();
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastSearchAngle));
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastContrastPercent));
        v.addElement(new Double(TSnifferGlobalAlignmentAlgorithm.lastSearchAngleIncrement));
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastSnifferSize));
        Object[] o1 = new Object[11];
        o1[0] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocBootstrapSensitivity);
        o1[1] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocBootstrapIterations);
        o1[2] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocFirstRunSensitivity);
        o1[3] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocFirstRunIterations);
        o1[4] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocSecondRunSensitivity);
        o1[5] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocSecondRunIterations);
        o1[6] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocThirdRunSensitivity);
        o1[7] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocThirdRunIterations);
        o1[8] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocSensitivityThreshold);
        o1[9] = new Boolean(TSnifferGlobalAlignmentAlgorithm.lastLocGridCheck);
        Object[] o2 = new Object[4];
        o2[0] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocT);
        o2[1] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocC);
        o2[2] = TSnifferGlobalAlignmentAlgorithm.lastLocSpotDetectionAlgorithmName;
        o2[3] = TSnifferGlobalAlignmentAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        o1[10] = o2;
        v.addElement(o1);
        TJobBuilder.addGlobalAlignmentJob(TSnifferGlobalAlignmentAlgorithm.NAME, v);
      }
    }
    if (treatmentCheckBox.isSelected()) {
      if (plus90RotationRadioButton.isSelected())
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_PLUS_90);
      else if (minus90RotationRadioButton.isSelected())
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_MOINS_90);
      else if (plus180RotationRadioButton.isSelected())
        TJobBuilder.addTreatmentJob(TImageControler.ROTATE_180);
      else if (horizontalFlipRadioButton.isSelected())
        TJobBuilder.addTreatmentJob(TImageControler.HORIZONTAL_FLIP);
      else if (verticalFlipRadioButton.isSelected())
          TJobBuilder.addTreatmentJob(TImageControler.VERTICAL_FLIP);
    }
    return TJobBuilder.getJob();
  }

  private void jbInit() throws Exception {
    this.setLayout(new BorderLayout());
    centerPanel.setLayout(new GridBagLayout());
    northPanel.setLayout(new GridBagLayout());
    gridLabel.setText(Messages.getString("TBatchComandPanel.0"));
    gridTextField.setMinimumSize(new Dimension(200, 21));
    gridTextField.setPreferredSize(new Dimension(200, 21));
    gridTextField.setEditable(false);
    gridTextField.setText(Messages.getString("TBatchComandPanel.1"));
    gridButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/openFile.gif")));
    addButton.setText(Messages.getString("TBatchComandPanel.2"));
    jLabelChannels.setText(Messages.getString("TBatchComandPanel.3"));
    selectAllButton.setText(Messages.getString("TBatchComandPanel.4"));
    outputDirectoryLabel.setText(Messages.getString("TBatchComandPanel.5"));
    outputDirectoryTextField.setMinimumSize(new Dimension(200, 21));
    outputDirectoryTextField.setPreferredSize(new Dimension(200, 21));
    outputDirectoryTextField.setToolTipText(Messages.getString("TBatchComandPanel.6"));
    outputDirectoryTextField.setEditable(false);
    outputDirectoryTextField.setText(Messages.getString("TBatchComandPanel.7"));
    outputDirectoryButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/openFile.gif")));
    saveImagesCheckBox.setText(Messages.getString("TBatchComandPanel.8"));
    centerPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), " Job "));
    northPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    northButtonsPanel.setLayout(new FlowLayout());
    southPanel.setLayout(new GridBagLayout());
    launchButton.setFont(new java.awt.Font("Dialog", Font.BOLD, 15));
    launchButton.setForeground(Color.blue);
    launchButton.setText(Messages.getString("TBatchComandPanel.9"));
    southPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    showSouthPanel.setLayout(new GridBagLayout());
    showCurrentAlignmentCheckBox.setText(Messages.getString("TBatchComandPanel.10"));
    RAZGraphicButton.setText(Messages.getString("TBatchComandPanel.11"));
    treatmentPanel.setLayout(new GridBagLayout());
    treatmentCheckBox.setText(Messages.getString("TBatchComandPanel.12"));
    rotationLabel.setText(Messages.getString("TBatchComandPanel.13"));
    flipLabel.setText(Messages.getString("TBatchComandPanel.14"));
    treatmentParamsPanel.setLayout(new GridBagLayout());
    treatmentParamsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    horizontalFlipRadioButton.setText(Messages.getString("TBatchComandPanel.15"));
    verticalFlipRadioButton.setText(Messages.getString("TBatchComandPanel.16"));
    plus90RotationRadioButton.setText(Messages.getString("TBatchComandPanel.17"));
    minus90RotationRadioButton.setText(Messages.getString("TBatchComandPanel.18"));
    plus180RotationRadioButton.setText(Messages.getString("TBatchComandPanel.19"));
    globalAlignmentPanel.setLayout(new GridBagLayout());
    globalAlignmentCheckBox.setText(Messages.getString("TBatchComandPanel.20"));
    globalAlignmentParamsPanel.setLayout(new GridBagLayout());
    edgesFinderRadioButton.setText(Messages.getString("TBatchComandPanel.21"));
    globalAlignmentPanel.setBorder(null);
    globalAlignmentParamsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    snifferRadioButton.setText(Messages.getString("TBatchComandPanel.22"));
    globalAlignmentParamsButton.setText(Messages.getString("TBatchComandPanel.23"));
    localAlignmentPanel.setLayout(new GridBagLayout());
    localAlignmentCheckBox.setText(Messages.getString("TBatchComandPanel.24"));
    localAlignmentParamsPanel.setLayout(new GridBagLayout());
    localAlignmentParamsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    localAlignmentGridRadioButton.setText(Messages.getString("TBatchComandPanel.25"));
    localAlignmentBlockRadioButton.setText(Messages.getString("TBatchComandPanel.26"));
    localAlignmentButton.setText(Messages.getString("TBatchComandPanel.27"));
    quantifPanel.setLayout(new GridBagLayout());
    quantifCheckBox.setText(Messages.getString("TBatchComandPanel.28"));
    quantifListPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    quantifListPanel.setLayout(new GridLayout(7, 1));
    quantifImageConstantCheckBox.setText(Messages.getString("TBatchComandPanel.29"));
    quantifImageComputedCheckBox.setText(Messages.getString("TBatchComandPanel.30"));
    quantifFitConstantCheckBox.setText(Messages.getString("TBatchComandPanel.31"));
    quantifFitComputedCheckBox.setText(Messages.getString("TBatchComandPanel.32"));
    qmCheckBox.setText(Messages.getString("TBatchComandPanel.33"));
    fitCorrectionCheckBox.setText(Messages.getString("TBatchComandPanel.34"));
    overshiningCorrectionCheckBox.setText(Messages.getString("TBatchComandPanel.35"));
    exportPanel.setLayout(new GridBagLayout());
    exportListPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    exportListPanel.setLayout(new GridLayout(2, 1));
    exportListPanel.add(exportTextCheckBox);
    //exportListPanel.add(exportMageMLCheckBox);//2005/12/01 removed as long as MageML is not available
    exportResultsCheckBox.setText(Messages.getString("TBatchComandPanel.36"));
    exportTextCheckBox.setText(Messages.getString("TBatchComandPanel.37"));
//    exportMageMLCheckBox.setText("MAGE-ML");//2005/12/01 removed as long as MageML is not available

    bgTreatment = new ButtonGroup();
    bgTreatment.add(plus90RotationRadioButton);
    bgTreatment.add(minus90RotationRadioButton);
    bgTreatment.add(plus180RotationRadioButton);
    bgTreatment.add(horizontalFlipRadioButton);
    bgTreatment.add(verticalFlipRadioButton);
    bgGA = new ButtonGroup();
    bgGA.add(edgesFinderRadioButton);
    bgGA.add(snifferRadioButton);
    bgLA = new ButtonGroup();
    bgLA.add(localAlignmentGridRadioButton);
    bgLA.add(localAlignmentBlockRadioButton);
    this.add(centerPanel, java.awt.BorderLayout.CENTER);
    this.add(northPanel, java.awt.BorderLayout.NORTH);
    northPanel.add(gridLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    northPanel.add(gridTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
    northButtonsPanel.add(jLabelChannels);//2005/11/04
    northButtonsPanel.add(jSpinnerChannels);//2005/11/04
    northButtonsPanel.add(addButton);
    northButtonsPanel.add(selectAllButton);
    northPanel.add(outputDirectoryLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    northPanel.add(outputDirectoryTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));
    northPanel.add(saveImagesCheckBox, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    northPanel.add(northButtonsPanel, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    showSouthPanel.add(showCurrentAlignmentCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    //showSouthPanel.add(showPlotsButton, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
    //    , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    showSouthPanel.add(RAZGraphicButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    this.add(southPanel, java.awt.BorderLayout.SOUTH);
    southPanel.add(showSouthPanel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    treatmentParamsPanel.add(plus90RotationRadioButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    treatmentParamsPanel.add(plus180RotationRadioButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    treatmentParamsPanel.add(minus90RotationRadioButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    treatmentParamsPanel.add(flipLabel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    treatmentParamsPanel.add(horizontalFlipRadioButton, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    treatmentParamsPanel.add(verticalFlipRadioButton, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
    treatmentParamsPanel.add(rotationLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    treatmentPanel.add(treatmentCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    globalAlignmentPanel.add(globalAlignmentCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    globalAlignmentParamsPanel.add(globalAlignmentParamsButton, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    globalAlignmentPanel.add(globalAlignmentParamsPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    localAlignmentPanel.add(localAlignmentParamsPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    localAlignmentPanel.add(localAlignmentCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    localAlignmentParamsPanel.add(localAlignmentButton, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    localAlignmentParamsPanel.add(localAlignmentGridRadioButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    localAlignmentParamsPanel.add(localAlignmentBlockRadioButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    treatmentPanel.add(treatmentParamsPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    quantifPanel.add(quantifCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    quantifPanel.add(quantifListPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
    quantifListPanel.add(quantifImageConstantCheckBox);
    quantifListPanel.add(quantifImageComputedCheckBox);
    quantifListPanel.add(quantifFitConstantCheckBox);
    quantifListPanel.add(quantifFitComputedCheckBox);
    quantifListPanel.add(qmCheckBox);
    quantifListPanel.add(fitCorrectionCheckBox);
    quantifListPanel.add(overshiningCorrectionCheckBox);
    exportPanel.add(exportResultsCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    exportPanel.add(exportListPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
    centerPanel.add(globalAlignmentPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
    centerPanel.add(localAlignmentPanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(treatmentPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(quantifPanel, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(exportPanel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    southPanel.add(launchButton, new GridBagConstraints(0, 0, 1, 13, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 50), 0, 0));
    globalAlignmentParamsPanel.add(edgesFinderRadioButton, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    globalAlignmentParamsPanel.add(snifferRadioButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    northPanel.add(gridButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    northPanel.add(outputDirectoryButton, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 10, 0, 0), 0, 0));
  }
  public void init(TBatch batch) {
    this.batch = batch;
    String s = ((TBatchModel)batch.getModel()).getJobString();
    StringTokenizer strtok = new StringTokenizer(s, "|");
    Vector tokens = new Vector();
    while (strtok.hasMoreTokens()) {
      tokens.addElement(strtok.nextToken());
    }
    treatmentCheckBox.setSelected(false);
    plus90RotationRadioButton.setSelected(true);
    minus90RotationRadioButton.setSelected(false);
    plus180RotationRadioButton.setSelected(false);
    horizontalFlipRadioButton.setSelected(false);
    verticalFlipRadioButton.setSelected(false);
    plus90RotationRadioButton.setEnabled(false);
    minus90RotationRadioButton.setEnabled(false);
    plus180RotationRadioButton.setEnabled(false);
    horizontalFlipRadioButton.setEnabled(false);
    verticalFlipRadioButton.setEnabled(false);
    globalAlignmentCheckBox.setSelected(false);
    edgesFinderRadioButton.setSelected(false);
    edgesFinderRadioButton.setEnabled(false);
    snifferRadioButton.setSelected(true);
    snifferRadioButton.setEnabled(false);
    localAlignmentCheckBox.setSelected(false);
    localAlignmentGridRadioButton.setSelected(false);
    localAlignmentGridRadioButton.setEnabled(false);
    localAlignmentBlockRadioButton.setSelected(true);
    localAlignmentBlockRadioButton.setEnabled(false);
    quantifCheckBox.setSelected(false);
    quantifImageConstantCheckBox.setSelected(false);
    quantifImageConstantCheckBox.setEnabled(false);
    quantifImageComputedCheckBox.setSelected(false);
    quantifImageComputedCheckBox.setEnabled(false);
    quantifFitConstantCheckBox.setSelected(false);
    quantifFitConstantCheckBox.setEnabled(false);
    quantifFitComputedCheckBox.setSelected(false);
    quantifFitComputedCheckBox.setEnabled(false);
    qmCheckBox.setSelected(false);
    qmCheckBox.setEnabled(false);
    fitCorrectionCheckBox.setSelected(false);
    fitCorrectionCheckBox.setEnabled(false);
    overshiningCorrectionCheckBox.setSelected(false);
    overshiningCorrectionCheckBox.setEnabled(false);
    exportResultsCheckBox.setSelected(false);
    exportTextCheckBox.setSelected(false);
    //exportMageMLCheckBox.setSelected(false);//2005/12/01 removed as long as MageML is not available
    if (tokens.contains("R+90") || tokens.contains("R-90") || tokens.contains("R180") || tokens.contains("FH") || tokens.contains("FV")) {
      treatmentCheckBox.setSelected(true);
      plus90RotationRadioButton.setEnabled(true);
      plus180RotationRadioButton.setEnabled(true);
      minus90RotationRadioButton.setEnabled(true);
      horizontalFlipRadioButton.setEnabled(true);
      verticalFlipRadioButton.setEnabled(true);
      if (tokens.contains("R+90"))
        plus90RotationRadioButton.setSelected(true);
      else if (tokens.contains("R-90"))
        minus90RotationRadioButton.setSelected(true);
      else if (tokens.contains("R180"))
        plus180RotationRadioButton.setSelected(true);
      else if (tokens.contains("FH"))
        horizontalFlipRadioButton.setSelected(true);
      else if (tokens.contains("FV0"))
        verticalFlipRadioButton.setSelected(true);
    }

    if (tokens.contains("AG1") || tokens.contains("AG2")) {
        System.out.println("ligne qui suit on va modifier 1e fois!");
    	globalAlignmentCheckBox.setSelected(true);//why a modif is added to the model?
    	edgesFinderRadioButton.setEnabled(true);
    	snifferRadioButton.setEnabled(true);
    	globalAlignmentParamsButton.setEnabled(true);    	
    	if (tokens.contains("AG1")) {
    		edgesFinderRadioButton.setSelected(true);
    	}
    	else {
    		snifferRadioButton.setSelected(true);
    	}
    }
    if (tokens.contains("AL1") || tokens.contains("AL2")) {
      System.out.println("ligne qui suit on va modifier 2e fois!");//why a modif is added to the model?
      localAlignmentCheckBox.setSelected(true);
      localAlignmentGridRadioButton.setEnabled(true);
      localAlignmentBlockRadioButton.setEnabled(true);
      localAlignmentButton.setEnabled(true);      
      if (tokens.contains("AL1")) {
        localAlignmentGridRadioButton.setSelected(true);
      }
      else {
        localAlignmentBlockRadioButton.setSelected(true);
      }
    }
    if (tokens.contains("QIC") || tokens.contains("QIV") || tokens.contains("QFC") || tokens.contains("QFV") || tokens.contains("QM") || tokens.contains("CF") || tokens.contains("CO")) {
    	System.out.println("ligne qui suit on va modifier 3e fois!");//why a modif is added to the model?
      quantifCheckBox.setSelected(true);
      quantifImageConstantCheckBox.setEnabled(true);
      quantifImageComputedCheckBox.setEnabled(true);
      quantifFitConstantCheckBox.setEnabled(true);
      quantifFitComputedCheckBox.setEnabled(true);
      qmCheckBox.setEnabled(true);
      fitCorrectionCheckBox.setEnabled(true);
      overshiningCorrectionCheckBox.setEnabled(true);
      quantifImageConstantCheckBox.setSelected(tokens.contains("QIC"));
      quantifImageComputedCheckBox.setSelected(tokens.contains("QIV"));
      quantifFitConstantCheckBox.setSelected(tokens.contains("QFC"));
      quantifFitComputedCheckBox.setSelected(tokens.contains("QFV"));
      qmCheckBox.setSelected(tokens.contains("QM"));
      fitCorrectionCheckBox.setSelected(tokens.contains("CF"));
      overshiningCorrectionCheckBox.setSelected(tokens.contains("CO"));
    }
    
    //EX1 = export1 => text
    if (tokens.contains("EX1")) {
      exportResultsCheckBox.setSelected(true);
      exportTextCheckBox.setSelected(true);
      //exportMageMLCheckBox.setSelected(false);//2005/12/01 removed as long as MageML is not available
    }
    //EX2 = export1 => MAGE-ML not implemented! 
    if (tokens.contains("EX2")) {
      exportResultsCheckBox.setSelected(true);
      exportTextCheckBox.setSelected(false);
     // exportMageMLCheckBox.setSelected(true);//2005/12/01 removed as long as MageML is not available
    }
    TLocalAlignmentAlgorithm la = ((TBatchModel)batch.getModel()).getJob().getLocalAlignmentAlgorithm();
    if (la != null) {
      la.getControlPanel().init(la);
      la.setLasts();
    }
    TGlobalAlignmentAlgorithm ga = ((TBatchModel)batch.getModel()).getJob().getGlobalAlignmentAlgorithm();
    if (ga != null) {
      ga.getControlPanel().init(ga);
      ga.setLasts();
    }
    //init of the path written in the chooser
    //Here we test if the directory saved exists.
//  added remi 2005/12/01
    File output= new File(batch.getOutputPath());
    if (output.exists()) outputDirectoryTextField.setText(batch.getOutputPath());
    else {
    	outputDirectoryTextField.setText("."+File.separator);
    	batch.setOutputPath("."+File.separator);
    	AGScan.prop.setProperty("batchPath","."+File.separator);
    }
    //outputDirectoryTextField.setText(batch.getOutputPath());//modified 2005/12/01
    gridTextField.setText(((TBatchModel)batch.getModel()).getGridName());
    saveImagesCheckBox.setSelected(((TBatchModel)batch.getModel()).getSaveImages());
    refresh();
  }
  public void update() {
    TAbstractJob job = processJob();
    ((TBatchModel)batch.getModel()).setJob(job);
  }
  public void refresh() {
    RAZGraphicButton.setEnabled(batch.isShowingImage());
    if (!batch.isBatchRunning()) {
      gridButton.setEnabled(true);
      outputDirectoryButton.setEnabled(true);
      saveImagesCheckBox.setEnabled(true);
      addButton.setEnabled(true);
      selectAllButton.setEnabled(batch.getModel().getRowCount() > 0);
      setJobEnabled(true);
      launchButton.setEnabled((batch.getModel().getRowCount() > 0) && !gridTextField.getText().equals(""));
    }
    else {
      gridButton.setEnabled(false);
      outputDirectoryButton.setEnabled(false);
      saveImagesCheckBox.setEnabled(false);
      addButton.setEnabled(false);
      selectAllButton.setEnabled(false);
      setJobEnabled(false);
      launchButton.setEnabled(false);
    }
    if (batch.getView() != null)
      ((TBatchTablePanel)batch.getView().getTablePanel()).getPopupMenu().refresh(batch);
  }
  public void setJobEnabled(boolean b) {
    treatmentCheckBox.setEnabled(b);
    globalAlignmentCheckBox.setEnabled(b);
    localAlignmentCheckBox.setEnabled(b);
    quantifCheckBox.setEnabled(b);
    plus90RotationRadioButton.setEnabled(b);
    minus90RotationRadioButton.setEnabled(b);
    plus180RotationRadioButton.setEnabled(b);
    horizontalFlipRadioButton.setEnabled(b);
    verticalFlipRadioButton.setEnabled(b);
    edgesFinderRadioButton.setEnabled(b);
    snifferRadioButton.setEnabled(b);
    globalAlignmentParamsButton.setEnabled(b);
    localAlignmentGridRadioButton.setEnabled(b);
    localAlignmentBlockRadioButton.setEnabled(b);
    localAlignmentButton.setEnabled(b);
    quantifImageConstantCheckBox.setEnabled(b);
    quantifImageComputedCheckBox.setEnabled(b);
    quantifFitConstantCheckBox.setEnabled(b);
    quantifFitComputedCheckBox.setEnabled(b);
    qmCheckBox.setEnabled(b);
    fitCorrectionCheckBox.setEnabled(b);
    overshiningCorrectionCheckBox.setEnabled(b);
    exportResultsCheckBox.setEnabled(b);
    exportTextCheckBox.setEnabled(b);
    //exportMageMLCheckBox.setEnabled(b);//2005/12/01 removed as long as MageML is not available
  }
  public void setShowCurrentAlignmentCheckBoxSelected(boolean b) {
    showCurrentAlignmentCheckBox.setSelected(b);
  }
}
