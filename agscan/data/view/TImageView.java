package agscan.data.view;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import ij.ImagePlus;
import ij.process.ImageProcessor;
import agscan.data.element.TDataElement;
import agscan.data.element.image.TImage;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.table.TTablePanel;

public class TImageView extends TViewImpl {
	private ImagePlus displayImage;//8 bits?
	//private KeyListener keyListener;

	public TImageView(TImage image) {
		super(image);
		//displayImage = image.getImageModel().getInitialImage();// on transforme plutot l'image en 8bits
		ImagePlus image16bits = image.getImageModel().getInitialImage();
		ImageProcessor proc = image16bits.getProcessor().convertToByte(true);// doScaling à true pour l'instant...
		ImagePlus image8bits = new ImagePlus(null,proc);//null pour le titre de l'image (celui de la fenetre = inutile)
		displayImage = image8bits;
		graphicPanel = new TImageGraphicPanel(this);
		/*
		 * to add KeyListener to graphical window
		 */
		//graphicPanel.setFocusable(true);
		graphicPanel.setRequestFocusEnabled(true);
		graphicPanel.requestFocus();
		// CK -- end 
		setGraphicPanel();
	}
	public int getMin() {
		return getImageGraphicPanel().getMin();
	}
	public int getMax() {
		return getImageGraphicPanel().getMax();
	}
	public TImageGraphicPanel getImageGraphicPanel() {
		return (TImageGraphicPanel)graphicPanel;
	}
	public TTablePanel getTablePanel() {
		return tablePanel;
	}
	public ImagePlus getDisplayImage() {
		return displayImage;
	}
	public void setMin(int min) {
		getImageGraphicPanel().setMin(min);
	}
	public void setMax(int max) {
		getImageGraphicPanel().setMax(max);
	}
	public void setMinAndMax(int min, int max) {
		getImageGraphicPanel().setMinAndMax(min, max);
	}
	public void setDisplayImage(ImagePlus pImage) {
		displayImage = pImage;
		graphicPanel.repaint();
	}
	public TDataElement getReference() {
		return super.getReference();
	}

}

/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
