package agscan.data.view.graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Vector;

import agscan.data.view.graphic.interactor.TRoiInteractor;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
//import agscan.dialog.histogram.transfercurve.TTransferCurve;//2005/11/16 transfer curves removed
//import agscan.dialog.histogram.transfercurve.TTransferCurveFactory;//2005/11/16 transfer curves removed
import agscan.data.view.TAlignmentView;
import agscan.data.view.TImageView;
import agscan.data.view.TViewImpl;

public class TImageGraphicPanel extends TGraphicPanel {
  public static class HotSpot {
    private Color color;
    private int x, y;
    public HotSpot(Color col, int x, int y) {
      color = col;
      this.x = x;
      this.y = y;
    }
    public void draw(Graphics g, double zoom) {
      g.setColor(color);
      g.drawLine((int)(x * zoom), (int)(y * zoom), (int)(x * zoom), (int)(y * zoom));
    }
  }
  public static class HotLine {
    private Color color;
    private int x1, y1, x2, y2;
    public HotLine(Color col, int x1, int y1, int x2, int y2) {
      color = col;
      this.x1 = x1;
      this.y1 = y1;
      this.x2 = x2;
      this.y2 = y2;
    }
    public void draw(Graphics g, double zoom) {
      g.setColor(color);
      g.drawLine((int)(x1 * zoom), (int)(y1 * zoom), (int)(x2 * zoom), (int)(y2 * zoom));
    }
  }
  private int min, max;
//  private TTransferCurve transferCurve;//2005/11/16 transfer curves removed
  private Vector hotSpots;

  public TImageGraphicPanel() {
    super();
    min = 0;
    max = 255;
   // transferCurve = TTransferCurveFactory.getInstance().getDefaultTransferCurve();//2005/11/16 transfer curves removed
    hotSpots = new Vector();
  }
  public TImageGraphicPanel(TViewImpl view) {
    super(view);
    min = 0;
    max = 255;
   // transferCurve = TTransferCurveFactory.getInstance().getDefaultTransferCurve();//2005/11/16 transfer curves removed
    hotSpots = new Vector();
  }
  public void addHotSpot(HotSpot hs) {
    hotSpots.addElement(hs);
  }
  public void addHotLine(HotLine hl) {
    hotSpots.addElement(hl);
  }
  public void clearHotSpots() {
    hotSpots.clear();
  }
  public void setHotSpots(Vector v) {
    hotSpots = v;
  }
  public Vector getHotSpots() {
    return hotSpots;
  }
  public Dimension getPreferredSize() {
    Dimension dim;
    if (getView() != null) {
      if (zoom > 0)
        dim = new Dimension((int)(getView().getReference().getModel().getElementWidth() * zoom),
                           (int)(getView().getReference().getModel().getElementHeight() * zoom));
      else
        dim = new Dimension((int)(getView().getReference().getModel().getElementWidth() / -zoom),
                           (int)(getView().getReference().getModel().getElementHeight() / -zoom));
      return dim;
    }
    return new Dimension(0, 0);
  }
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    double z = 0;
    if (zoom > 0)
      z = zoom;
    else
      z = -1.0D / zoom;
    if (getView() instanceof TImageView)
    //  ((Graphics2D)g).drawRenderedImage(((TImageView)getView()).getDisplayImage(), AffineTransform.getScaleInstance(z, z));
    	((Graphics2D)g).drawImage(((TImageView)getView()).getDisplayImage().getProcessor().createImage(), AffineTransform.getScaleInstance(z, z),null);//modifié le 11/07/05
    else if (getView() instanceof TAlignmentView)
      //((Graphics2D)g).drawRenderedImage(((TAlignmentView)getView()).getDisplayImage(), AffineTransform.getScaleInstance(z, z));
    	((Graphics2D)g).drawImage(((TAlignmentView)getView()).getDisplayImage().getProcessor().createImage(), AffineTransform.getScaleInstance(z, z),null);//modifié le 13/07/05
    if (currentInteractor == TGraphicInteractor.ROI) TRoiInteractor.getInstance().draw(g, z);
    Object o;
    for (Enumeration enume = hotSpots.elements(); enume.hasMoreElements(); ) {
      o = enume.nextElement();
      if (o instanceof HotSpot) {
        ((HotSpot)o).draw(g, z);
      }
      else {
        ((HotLine)o).draw(g, z);
      }
    }
  }
  protected void paintComponent(Graphics2D g, double z) {
    super.paintComponent(g);
    g.scale(z, z);
    g.drawImage(((TImageView)getView()).getDisplayImage().getProcessor().createImage(), new AffineTransform(),null);
    if (currentInteractor == TGraphicInteractor.ROI) TRoiInteractor.getInstance().draw(g, 1.0D);
  }
  
  
//2005/11/16 transfer curves removed
  /*
  public TTransferCurve getTransferCurve() {
    return transferCurve;
  }
  public void setTransferCurve(TTransferCurve tc) {
    transferCurve = tc;
    revalidate();
  }
  */
  public int getMin() {
    return min;
  }
  public int getMax() {
    return max;
  }
  public void setMin(int min) {
    this.min = min;
    revalidate();
  }
  public void setMax(int max) {
    this.max = max;
    revalidate();
  }
  public void setMinAndMax(int min, int max) {
    this.min = min;
    this.max = max;
    revalidate();
  }
  public BufferedImage getSnapshot(int dimMax) {
    double z;
    BufferedImage outImage = null;

    int width = ((TImageView)getView()).getDisplayImage().getWidth();
    int height = ((TImageView)getView()).getDisplayImage().getHeight();

    if (width >= height) {
      outImage = new BufferedImage(Math.min(dimMax, width), (int)(Math.min(dimMax, width) * height / width), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, width) / width;
    }
    else {
      outImage = new BufferedImage((int)(Math.min(dimMax, height) * width / height), Math.min(dimMax, height), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, height) / height;
    }
    Graphics2D g2d = outImage.createGraphics();
    paintComponent(g2d, z);
    return outImage;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
