package agscan.data.view.graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JComponent;

import agscan.data.element.TDataElement;

public class TRule extends JComponent {
  public static final int HORIZONTAL = 0;
  public static final int VERTICAL = 1;
  public static final int[] incrs = {          1,          2,                      5,
                                              10,         20,         25,         50,
                                             100,        200,        250,        500,
                                            1000,       2000,       2500,       5000,
                                           10000,      20000,      25000,      50000,
                                          100000,     200000,     250000,     500000,
                                         1000000,    2000000,    2500000,    5000000,
                                        10000000,   20000000,   25000000,   50000000,
                                       100000000,  200000000,  250000000,  500000000,
                                      1000000000, 2000000000 };
  private int orientation;
  private int increment;
  private double increment_pix, minTicksDiv;
  private int res;
  private int size;
  private double zoom;
  private Font laFonte;
  private int majTickLength, minTickLength;
  private int imageWidth, imageHeight;
  private int xMarkPos, yMarkPos;

  public TRule(int o, int s, int r, int imageW, int imageH) {
    orientation = o;
    size = s;
    res = r;
    increment = 100 * res;
    for (int i = 0; i < incrs.length; i++) {
      if (incrs[i] == increment) break;
      if (incrs[i] > increment) {
        if ((incrs[i] - increment) > (increment - incrs[i - 1]))
          increment = incrs[i - 1];
        else
          increment = incrs[i];
        break;
      }
    }
    zoom = 1.0D;
    laFonte = new Font("SansSerif", Font.PLAIN, 10);
    increment_pix = increment * zoom / res;
    majTickLength = 6;
    minTickLength = 2;
    imageWidth = imageW;
    imageHeight = imageH;
    if (((increment % 10.0F) == 0) && ((increment_pix / 10) >= 20))
      minTicksDiv = 10;
    else if (((increment % 5.0F) == 0) && ((increment_pix / 5) >= 20))
      minTicksDiv = 5;
    else if (((increment % 4.0F) == 0) && ((increment_pix / 4) >= 20))
      minTicksDiv = 4;
    else if (((increment % 2.0F) == 0) && ((increment_pix / 2) >= 20))
      minTicksDiv = 2;
    else
      minTicksDiv = 0;
    xMarkPos = yMarkPos = -1;
  }
  //remplacé integration 23/09/05
  public void init(TDataElement element) {
    double z;
    if (element.getView().getGraphicPanel() != null)
      z = element.getView().getGraphicPanel().getZoom();
    else
      z = 1;
    if (z < 0) z = 1.0D / -z;
    setZoom(z);
    imageWidth = element.getModel().getElementWidth(); // getView().getGraphicPanel().getWidth();
    imageHeight = element.getModel().getElementHeight(); // getView().getGraphicPanel().getHeight();
    setXMarkPos(-1);
    setYMarkPos(-1);
  }
  public void setZoom(double z) {
    zoom = z;
    increment_pix = increment * zoom / res;
    revalidate();
    repaint();
  }
  public void setXMarkPos(int p) {
    int p0 = xMarkPos;
    xMarkPos = p;
    revalidate();
    repaint();
  }
  public void setYMarkPos(int p) {
    int p0 = yMarkPos;
    yMarkPos = p;
    revalidate();
    repaint();
  }
  public int getIncrement() { return increment; }
  public Dimension getPreferredSize() {
    if (orientation == HORIZONTAL) return new Dimension((int)(imageWidth * zoom), size);
    return new Dimension(size, (int)(imageHeight * zoom * 5));
  }
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Rectangle rect = g.getClipBounds();
    int x0 = rect.x;
    int y0 = rect.y;
    int width = Math.min(rect.width, (int)(imageWidth * zoom));
    int height = Math.min(rect.height, (int)(imageHeight * zoom));
    g.setColor(Color.lightGray);
    g.fillRect(rect.x, rect.y, rect.width, rect.height);
    g.setFont(laFonte);
    g.setColor(Color.black);
    int end = 0;
    int start = 0;
    int stringWidth;
    String text = null;

    increment = (int)(100 * res / zoom);
    for (int i = 0; i < incrs.length; i++) {
      if (incrs[i] == increment) break;
      if (incrs[i] > increment) {
        if ((incrs[i] - increment) > (increment - incrs[i - 1]))
          increment = incrs[i - 1];
        else
          increment = incrs[i];
        break;
      }
    }
    increment = Math.max(increment, res);
    increment_pix = increment * zoom / res;
    if (((increment % 10.0F) == 0) && ((increment_pix / 10) >= 10))
      minTicksDiv = 10;
    else if (((increment % 5.0F) == 0) && ((increment_pix / 5) >= 10))
      minTicksDiv = 5;
    else if (((increment % 4.0F) == 0) && ((increment_pix / 4) >= 10))
      minTicksDiv = 4;
    else if (((increment % 2.0F) == 0) && ((increment_pix / 2) >= 10))
      minTicksDiv = 2;
    else
      minTicksDiv = 0;
    if (orientation == HORIZONTAL) {
      start = (int)(x0 * (imageWidth-1) / (getWidth()-1) / increment) * increment;
      end = ((int)((x0 + width) / increment_pix)) * increment;
    }
    else {
      start = (int)(y0 / increment_pix) * increment;
      end = ((int)((y0 + height) / increment_pix)) * increment;
    }
    if (start == 0) {
      text = Integer.toString(0);
      if (orientation == HORIZONTAL) {
        g.drawLine(0, size - 1, 0, size - majTickLength - 1);
        g.drawString(text, 2, size - majTickLength - 3);
      }
      else {
        g.drawLine(size - 1, 0, size - majTickLength - 1, 0);
        ((Graphics2D)g).rotate(-Math.PI / 2, 15, 10);
        g.drawString(text, 15, 10);
        ((Graphics2D)g).rotate(Math.PI / 2, 15, 10);
      }
      start = increment;
      for (int k = 1; k < minTicksDiv; k++) {
        if (orientation == HORIZONTAL)
          g.drawLine((int)((double)(increment / minTicksDiv * k) * zoom / res), size - 1,
                     (int)((double)(increment / minTicksDiv * k) * zoom / res), size - minTickLength - 1);
        else
          g.drawLine(size - 1, (int)((double)(increment / minTicksDiv * k) * zoom / res), size - minTickLength - 1,
                               (int)((double)(increment / minTicksDiv * k) * zoom / res));
      }
    }
    for (int i = start; i <= end; i += increment) {
      text = Integer.toString(i);
      stringWidth = g.getFontMetrics(laFonte).stringWidth(text);
      if (orientation == HORIZONTAL) {
        g.drawLine((int)(i * zoom / res), size - 1, (int)(i * zoom / res), size - majTickLength - 1);
        g.drawString(text, (int)(i * zoom / res) - stringWidth / 2, size - majTickLength - 3);
      }
      else {
        g.drawLine(size - 1, (int)(i * zoom / res), size - majTickLength - 1, (int)(i * zoom / res));
        ((Graphics2D)g).rotate(-Math.PI / 2, 15, (int)(i * zoom / res) + stringWidth / 2);
        g.drawString(text, 15, (int)(i * zoom / res) + stringWidth / 2);
        ((Graphics2D)g).rotate(Math.PI / 2, 15, (int)(i * zoom / res) + stringWidth / 2);
      }
      for (int k = 1; k < minTicksDiv; k++) {
        if (orientation == HORIZONTAL)
          g.drawLine((int)((double)(i + increment / minTicksDiv * k) * zoom / res), size - 1,
                     (int)((double)(i + increment / minTicksDiv * k) * zoom / res), size - minTickLength - 1);
        else
          g.drawLine(size - 1, (int)((double)(i + increment / minTicksDiv * k) * zoom / res), size - minTickLength - 1,
                               (int)((double)(i + increment / minTicksDiv * k) * zoom / res));
      }
    }
    g.setColor(Color.yellow);
    if ((xMarkPos > -1) && (orientation == HORIZONTAL)) g.drawLine(xMarkPos, 2, xMarkPos, size - 2);
    if ((yMarkPos > -1) && (orientation == VERTICAL)) g.drawLine(2, yMarkPos, size - 2, yMarkPos);
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
