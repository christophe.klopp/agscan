/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.graphic;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Vector;

import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TGridSelectionModel;
import agscan.data.view.graphic.interactor.TAlignmentInteractor;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.data.view.graphic.interactor.TGridInteractor;
//import agscan.dialog.histogram.transfercurve.TTransferCurve;//2005/11/16 transfer curves removed
import agscan.data.view.TAlignmentView;
import agscan.data.view.TImageView;

public class TAlignmentGraphicPanel extends TImageGraphicPanel  {
  private TGridModel gridModel;
  private TImageView imageView;
  

  public TAlignmentGraphicPanel(TAlignmentView view, TGridModel gridModel) {
    super(view);
    this.imageView = view.getImageView();
    this.gridModel = gridModel;
    setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
  }
  public int getMin() {
    return ((TImageGraphicPanel)imageView.getGraphicPanel()).getMin();
  }

  public int getMax() {
    return ((TImageGraphicPanel)imageView.getGraphicPanel()).getMax();
  }
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    
    double z = 0;
    if (zoom > 0)
      z = zoom;
    else
      z = -1.0D / zoom;
    boolean scroll, drag;
    drag = false;
    scroll = ((TAlignmentView)getView()).isScrolled();
    if (currentInteractor == TGraphicInteractor.ALIGNMENT)
      drag = ((TGridInteractor)interactors.get(new Integer(currentInteractor))).isDragged();
    Rectangle rect = getView().getGraphicScrollPane().getViewport().getViewRect();
    //Rectangle rect = new Rectangle((int)(gridModel.getElementWidth() * z * gridModel.getPixelWidth()),
    //                               (int)(gridModel.getElementHeight() * z * gridModel.getPixelHeight()));
    gridModel.draw((Graphics2D)g, drag, scroll, rect, z);
    ((TAlignmentInteractor)TAlignmentInteractor.getInstance()).draw(g, z);
  }
  protected void paintComponent(Graphics2D g, double z) {
    super.paintComponent(g);
    g.scale(z, z);
    //g.drawRenderedImage(((TAlignmentView)getView()).getDisplayImage(), new AffineTransform());//replaced the remplacle 2005/08/08
	g.drawImage(((TAlignmentView)getView()).getDisplayImage().getProcessor().createImage(),new AffineTransform(),null);//error before the 2005/11/30:TImageView=>TAlignmentView
	
    Rectangle rect = new Rectangle((int)(gridModel.getElementWidth() * gridModel.getPixelWidth()), (int)(gridModel.getElementHeight() * gridModel.getPixelHeight()));
    gridModel.draw(g, true, false, rect, 1.0D);
  }
  
  
  public void repaint(TGridSelectionModel sModel, Vector v) {
    TGridModel model = ((TGriddedElement)getView().getReference()).getGridModel();
    double z = 0;
    if (zoom > 0)
      z = zoom;
    else
      z = -1.0D / zoom;
   
    Rectangle rect = getView().getGraphicScrollPane().getViewport().getViewRect();
    if (v == null) {
      v = new Vector(sModel.getRecursivelySelectedSpots());
      v.addAll(sModel.getPreSelection());
    }
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); )
      ((TGridElement)enume.nextElement()).draw((Graphics2D)getGraphics(), false, false, rect, z);
    ((TAlignmentInteractor)TAlignmentInteractor.getInstance()).draw((Graphics2D)getGraphics(), z);
  }
  //2005/11/16 transfer curves removed
  /*
  public TTransferCurve getTransferCurve() {
    return imageView.getImageGraphicPanel().getTransferCurve();
  }
  */
  public BufferedImage getSnapshot(int dimMax) {
    double z;
    BufferedImage outImage = null;

    int width = ((TAlignmentView)getView()).getDisplayImage().getWidth();
    int height = ((TAlignmentView)getView()).getDisplayImage().getHeight();

    if (width >= height) {
      outImage = new BufferedImage(Math.min(dimMax, width), (int)(Math.min(dimMax, width) * height / width), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, width) / width;
    }
    else {
      outImage = new BufferedImage((int)(Math.min(dimMax, height) * width / height), Math.min(dimMax, height), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, height) / height;
    }   
    Graphics2D g2d = outImage.createGraphics();
    paintComponent(g2d, z);
    System.out.println("SNAPSHOT dans TAlignementGraphicPanel.java");
    return outImage;
  }

}


/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
