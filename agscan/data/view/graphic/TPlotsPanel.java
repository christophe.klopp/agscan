package agscan.data.view.graphic;

import java.awt.BorderLayout;

import javax.swing.JEditorPane;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TPlotsPanel extends TGraphicPanel {
  public TPlotsPanel(JEditorPane jep) {
    super();
    setLayout(new BorderLayout());
    add(jep, BorderLayout.CENTER);
  }
}
