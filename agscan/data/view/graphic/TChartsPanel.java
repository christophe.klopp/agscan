package agscan.data.view.graphic;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TChartsPanel extends TGraphicPanel {
  private JTabbedPane tabbedPane;
  private JCheckBox logCheckBox;
  private Vector panels;

  public TChartsPanel() {
    super();
    panels = new Vector();
    tabbedPane = new JTabbedPane();
    setLayout(new BorderLayout());
    JPanel centerPanel = new JPanel();
    centerPanel.add(tabbedPane);
    add(centerPanel, BorderLayout.CENTER);
    logCheckBox = new JCheckBox("Echelles logarithmiques");
    JPanel southPanel = new JPanel();
    southPanel.add(logCheckBox);
    add(southPanel, BorderLayout.SOUTH);
    logCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        XYPlot plot;
        if (logCheckBox.isSelected()) {
          for (int i = 0; i < panels.size(); i++) {
            plot = ((ChartPanel)panels.elementAt(i)).getChart().getXYPlot();
            LogarithmicAxis v = new LogarithmicAxis("<LOG> " + plot.getRangeAxis().getLabel().replaceAll("<LOG> ", ""));
            v.setAllowNegativesFlag(true);
            LogarithmicAxis h = new LogarithmicAxis("<LOG> " + plot.getDomainAxis().getLabel().replaceAll("<LOG> ", ""));
            h.setAllowNegativesFlag(true);
            plot.setRangeAxis(v);
            plot.setDomainAxis(h);
          }
        }
        else {
          for (int i = 0; i < panels.size(); i++) {
            plot = ((ChartPanel)panels.elementAt(i)).getChart().getXYPlot();
            NumberAxis v = new NumberAxis(plot.getRangeAxis().getLabel().replaceAll("<LOG> ", ""));
            NumberAxis h = new NumberAxis(plot.getDomainAxis().getLabel().replaceAll("<LOG> ", ""));
            plot.setRangeAxis(v);
            plot.setDomainAxis(h);
          }
        }
      }
    });
  }
  public void addChartPanel(ChartPanel cp, String title) {
    tabbedPane.add(title, cp);
    panels.addElement(cp);
  }
}
