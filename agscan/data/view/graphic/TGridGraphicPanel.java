/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Vector;

import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TGridSelectionModel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.data.view.graphic.interactor.TGridInteractor;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TGridView;

public class TGridGraphicPanel extends TGraphicPanel {
  public TGridGraphicPanel(TGridView view) {
    super(view);
    setCurrentInteractor(TGraphicInteractor.GRID);
  }
  public Dimension getPreferredSize() {
    Dimension dim;
    if (getView() != null) {
      if (zoom > 0)
        dim = new Dimension((int)(getView().getReference().getModel().getElementWidth() * zoom) + 1,
                            (int)(getView().getReference().getModel().getElementHeight() * zoom) + 1);
      else
        dim = new Dimension((int)(getView().getReference().getModel().getElementWidth() / -zoom) + 1,
                            (int)(getView().getReference().getModel().getElementHeight() / -zoom) + 1);
      return dim;
    }
    return new Dimension(0, 0);
  }
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    double z = 0;
    if (zoom > 0)
      z = zoom;
    else
      z = -1.0D / zoom;
    TGridModel model = ((TGrid)getView().getReference()).getGridModel();
    g.setColor(Color.white);
    g.fillRect(0, 0, (int)(model.getElementWidth() * z), (int)(model.getElementHeight() * z));
    boolean scroll, drag;
    scroll = drag = false;
    //Rectangle rect = getView().getGraphicScrollPane().getViewport().getViewRect();
    Rectangle rect = new Rectangle((int)(model.getElementWidth() * z * model.getPixelWidth()),
                                   (int)(model.getElementHeight() * z * model.getPixelHeight()));
    model.draw((Graphics2D)g, drag, scroll, rect, z);
    ((TGridInteractor)TGridInteractor.getInstance()).draw(g, z);
  }
  protected void paintComponent(Graphics2D g, double z) {
    super.paintComponent(g);
    TGridModel model = ((TGrid)getView().getReference()).getGridModel();
    g.setColor(Color.white);
    g.fillRect(0, 0, (int)(model.getElementWidth() * z), (int)(model.getElementHeight() * z));
    Rectangle rect = new Rectangle((int)(model.getElementWidth() * model.getPixelWidth()),
                                   (int)(model.getElementHeight() * model.getPixelHeight()));
    model.draw(g, false, true, rect, z);
    ((TGridInteractor)TGridInteractor.getInstance()).draw(g, z);
  }
  public void repaint(TGridSelectionModel sModel, Vector v) {
    TGridModel model = ((TGrid)getView().getReference()).getGridModel();
    double z = 0;
    if (zoom > 0)
      z = zoom;
    else
      z = -1.0D / zoom;
    Rectangle rect = getView().getGraphicScrollPane().getViewport().getViewRect();
    if (v == null) {
      v = new Vector(sModel.getRecursivelySelectedSpots());
      v.addAll(sModel.getPreSelection());
    }
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); )
      ((TGridElement)enume.nextElement()).draw((Graphics2D)getGraphics(), false, false, rect, z);
    ((TGridInteractor)TGridInteractor.getInstance()).draw((Graphics2D)getGraphics(), z);
  }
  public BufferedImage getSnapshot(int dimMax) {
    double z;
    BufferedImage outImage = null;
    TGridModel model = ((TGrid)getView().getReference()).getGridModel();
    int width = model.getElementWidth();
    int height = model.getElementHeight();

    if (width >= height) {
      outImage = new BufferedImage(Math.min(dimMax, width), (int)(Math.min(dimMax, width) * height / width), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, width) / width;
    }
    else {
      outImage = new BufferedImage((int)(Math.min(dimMax, height) * width / height), Math.min(dimMax, height), BufferedImage.TYPE_BYTE_INDEXED);
      z = (double)Math.min(dimMax, height) / height;
    }
    Graphics2D g2d = outImage.createGraphics();
    paintComponent(g2d, z);
    return outImage;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
