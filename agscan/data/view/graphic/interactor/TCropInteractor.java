/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * Modification : 2005/10/26 - modification of the cropped image name
 * @version 1.0
 */

package agscan.data.view.graphic.interactor;

import ij.ImageStack;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TImageControler;
import agscan.data.element.TDataElement;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.factory.TImageModelFactory;
import agscan.menu.TMenuManager;
import agscan.menu.action.TCropAction;

public class TCropInteractor extends TGraphicInteractor {
  private static TCropInteractor instance = null;
  private Rectangle cropRect;
  private int x0, y0;
  private boolean rectOver, resizing, nResize, sResize, wResize, eResize, neResize, seResize, swResize, nwResize;

  protected TCropInteractor() {
    super();
    cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
    id = TGraphicInteractor.CROP;
    instance = this;
    cropRect = null;
    rectOver = resizing = nResize = sResize = wResize = eResize = neResize = seResize = swResize = nwResize = false;
  }
  public static TCropInteractor getInstance() {
    if (instance == null)
      instance = new TCropInteractor();
    return instance;
  }
  public void mousePressed(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    Graphics g = element.getView().getGraphicPanel().getGraphics();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    x0 = (int)((double)event.getX() / zoom);
    y0 = (int)((double)event.getY() / zoom);
    boolean resize = nResize || neResize || eResize || seResize || sResize || swResize || wResize || nwResize;
    if (cropRect == null) {
      cropRect = new Rectangle(x0, y0, 0, 0);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
    else if (!rectOver) {
      cropRect = new Rectangle(x0, y0, 0, 0);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
    else if (cropRect.contains(x0, y0)) {

    }
    else if (!resize) {
      cropRect = new Rectangle(x0, y0, 0, 0);
      rectOver = false;
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
  }
  public void mouseReleased(MouseEvent event) {
    rectOver = true;
  }
  public void mouseMoved(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    TGraphicPanel panel = element.getView().getGraphicPanel();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);

    nResize = sResize = wResize = eResize = neResize = seResize = swResize = nwResize = false;
    if (rectOver) {
      if ((x > (cropRect.x - 3)) && (x < (cropRect.x + 3)) && (y > (cropRect.y - 3)) && (y < (cropRect.y + 3))) {
        panel.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
        nwResize = true;
        neResize = eResize = seResize = sResize = swResize = wResize = nResize = false;
      }
      else if ((x > (cropRect.x - 3)) && (x < (cropRect.x + 3)) && (y > (cropRect.height + cropRect.y - 3)) && (y < (cropRect.height + cropRect.y + 3))) {
        panel.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
        swResize = true;
        neResize = eResize = seResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (cropRect.x + cropRect.width - 3)) && (x < (cropRect.x + cropRect.width + 3)) && (y > (cropRect.y - 3)) && (y < (cropRect.y + 3))) {
        panel.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
        neResize = true;
        swResize = eResize = seResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (cropRect.x + cropRect.width - 3)) && (x < (cropRect.x + cropRect.width + 3)) && (y > (cropRect.height + cropRect.y - 3)) && (y < (cropRect.height + cropRect.y + 3))) {
        panel.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
        seResize = true;
        swResize = eResize = neResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (cropRect.x + 3)) && (x < (cropRect.x + cropRect.width - 3)) && (y > (cropRect.y - 3)) && (y < (cropRect.y + 3))) {
        panel.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        nResize = true;
        swResize = eResize = neResize = sResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (cropRect.x + cropRect.width - 3)) && (x < (cropRect.x + cropRect.width + 3)) && (y > (cropRect.y + 3)) && (y < (cropRect.y + cropRect.height - 3))) {
        panel.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        eResize = true;
        swResize = nResize = neResize = sResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (cropRect.x + 3)) && (x < (cropRect.x + cropRect.width - 3)) && (y > (cropRect.y + cropRect.height - 3)) && (y < (cropRect.y + cropRect.height + 3))) {
        panel.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
        sResize = true;
        swResize = nResize = neResize = eResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (cropRect.x - 3)) && (x < (cropRect.x + 3)) && (y > (cropRect.y + 3)) && (y < (cropRect.y + cropRect.height - 3))) {
        panel.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
        wResize = true;
        swResize = nResize = neResize = eResize = nwResize = sResize = seResize = false;
      }
      else if ((x > (cropRect.x + 3)) && (x < (cropRect.x + cropRect.width - 3)) && (y > (cropRect.y + 3)) && (y < (cropRect.y + cropRect.height - 3))) {
        panel.setCursor(new Cursor(Cursor.MOVE_CURSOR));
        swResize = nResize = neResize = eResize = nwResize = sResize = seResize = wResize = false;
      }
      else
        panel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }
  }
  public void mouseDragged(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    Graphics g = element.getView().getGraphicPanel().getGraphics();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);

    if (!rectOver) {
      if ((x < element.getModel().getElementWidth()) && (y < element.getModel().getElementHeight())) {
        cropRect.setSize(x - cropRect.x, y - cropRect.y);
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
        TEventHandler.handleMessage(ev);
        element.getView().getGraphicPanel().repaint();
      }
    }
    else {
      if (nResize) {
        if (((cropRect.height + y0 - y) >= 10) && ((cropRect.y + y - y0) >= 0)) {
          cropRect.setSize(cropRect.width, cropRect.height + y0 - y);
          cropRect.translate(0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (sResize) {
        if (((cropRect.height - y0 + y) >= 10) &&
            ((cropRect.y + cropRect.height - y0 + y) < element.getModel().getElementHeight())) {
          cropRect.setSize(cropRect.width, cropRect.height - y0 + y);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (wResize) {
        if (((cropRect.width + x0 - x) >= 10) && ((cropRect.x + x - x0) >= 0)) {
          cropRect.setSize(cropRect.width + x0 - x, cropRect.height);
          cropRect.translate(x - x0, 0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (eResize) {
        if (((cropRect.width - x0 + x) >= 10) &&
            ((cropRect.x + cropRect.width - x0 + x) < element.getModel().getElementWidth())) {
          cropRect.setSize(cropRect.width - x0 + x, cropRect.height);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (neResize) {
        if ((cropRect.y + y - y0) < 0)
          y0 = y;
        else if ((cropRect.x + cropRect.width - x0 + x) >= element.getModel().getElementWidth())
          x0 = x;
        else if (((cropRect.height + y0 - y) >= 10) && ((cropRect.width - x0 + x) >= 10)) {
          cropRect.setSize(cropRect.width - x0 + x, cropRect.height + y0 - y);
          cropRect.translate(0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (nwResize) {
        if ((cropRect.y + y - y0) < 0)
          y0 = y;
        else if ((cropRect.x + x - x0) < 0)
          x0 = x;
        else if (((cropRect.height + y0 - y) >= 10) && ((cropRect.width + x0 - x) >= 10)) {
          cropRect.setSize(cropRect.width + x0 - x, cropRect.height + y0 - y);
          cropRect.translate(x - x0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (seResize) {
        if ((cropRect.y + cropRect.height - y0 + y) >= element.getModel().getElementHeight())
          y0 = y;
        else if ((cropRect.x + cropRect.width - x0 + x) >= element.getModel().getElementWidth())
          x0 = x;
        else if (((cropRect.height - y0 + y) >= 10) && ((cropRect.width - x0 + x) >= 10)) {
          cropRect.setSize(cropRect.width - x0 + x, cropRect.height - y0 + y);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (swResize) {
        if ((cropRect.y + cropRect.height - y0 + y) >= element.getModel().getElementHeight())
          y0 = y;
        else if ((cropRect.x + x - x0) < 0)
          x0 = x;
        else if (((cropRect.height - y0 + y) >= 10) && ((cropRect.width + x0 - x) >= 10)) {
          cropRect.setSize(cropRect.width + x0 - x, cropRect.height - y0 + y);
          cropRect.translate(x - x0, 0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else {
        if (((cropRect.x + x - x0) >= 0) && ((cropRect.y + y - y0) >= 0) &&
            ((cropRect.x + cropRect.width + x - x0) < element.getModel().getElementWidth()) &&
            ((cropRect.y + cropRect.height + y - y0) < element.getModel().getElementHeight())) {
          cropRect.translate(x - x0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
        }
        x0 = x;
        y0 = y;
      }
    }
  }
  public void mouseClicked(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);
    if ((event.getClickCount() == 2) && (x > (cropRect.x + 3)) && (x < (cropRect.x + cropRect.width - 3)) && (y > (cropRect.y + 3)) && (y < (cropRect.y + cropRect.height - 3))) {
      TImageModel tim = (TImageModel)element.getModel();
      String fileName = ((TImage)element).getName();
   
      //test remi
      System.out.println("test = "+tim.getFileFormat());
      //fin test remi
      
      
      
      // recupération de la zone "cropée"
     
            // ParameterBlock pb = new ParameterBlock();
      //pb.addSource(tim.getInitialImage());
      //pb.add((float)cropRect.x).add((float)cropRect.y).add((float)cropRect.width).add((float)cropRect.height);
      
      
      //création d'une image JAI avec cette zone
            
      //PlanarImage pi = JAI.create("Crop", pb);
      //pb = new ParameterBlock();
      //pb.addSource(pi);
      //pb.add((float)-pi.getMinX()).add((float)-pi.getMinY()).add(null);
      //pi = JAI.create("Translate", pb);
      
      
      //remi
      //2005/10/10 modif travail sur un vecteur et plus sur 1 image...
      //2005/10/14 modif travail sur un stack et plus sur un Vector!!!
     // Vector imagesVector = tim.getInitialImages();
      //Vector imagesCropVector = new Vector();
      ImageStack stack = tim.getImages();
     //ImageStack cropStack = new ImageStack(stack.getWidth(),stack.getHeight());
      ImageStack cropStack = new ImageStack(cropRect.width,cropRect.height);
      //ImagePlus currentImage = null;
      ImageProcessor currentProc = null;
      //ImagePlus cropImage = null;
      ImageProcessor cropProc = null;
      //for (int i=0;i<imagesVector.size();i++){
      for (int i=1;i<=stack.getSize();i++){
        currentProc = stack.getProcessor(i); 
        currentProc.setRoi(cropRect);
        //cropProc = new ImagePlus(null,currentImage.getProcessor().crop());
        cropProc = currentProc.crop();
        cropStack.addSlice(stack.getSliceLabel(i),cropProc);//on renomme pareil le proc
      }
      
     // ImagePlus initCopy = tim.getInitialImage(); 
     // initCopy.setRoi(cropRect);
      //ImagePlus cropImage = new ImagePlus(null,initCopy.getProcessor().crop());
      //cropImage = TImageControler.setInvertedLUT(cropImage,true);
     // System.out.println("cropImage.getBitDepth()= "+cropImage.getBitDepth());
      //fin remi
    
	  
      //creation du model de la nouvelle image
      TImageModelFactory timf = TImageModelFactory.getImageModelFactory(tim.getFileFormat());
      tim = timf.createImageModel(cropStack, ImagePlusTools.getImageData(tim.getInitialImage(),16), (int)tim.getPixelWidth(), (int)tim.getPixelHeight(),
                                  tim.getElementWidth(), tim.getImageBitDepth(), tim.getUnit(), tim.getIPType(),
                                  ImagePlusTools.getLatitude(), tim.getFileFormat(),cropRect);
      TImage image = null;
    
    
      if (tim != null) {
      	// creation of the cropped image
      	//2005/10/26 - modification of the cropped image name
          	int i = fileName.lastIndexOf((int)'.');//-1 if no extension
    	String cropName = null;//name of the crop image
    	if (i==-1){
    		cropName = fileName + "_CROP";
    	}
    	else{
    		cropName = fileName.substring(0,i)+"_CROP"+fileName.substring(i,fileName.length());
    	}
        image = new TImage(cropName, tim);
        /*
         if (tim.getFileFormat().equalsIgnoreCase("BAS_IMAGE_FILE"))
          image = new TBASImage(fileName + "_CROP", tim);
        else if (tim.getFileFormat().equalsIgnoreCase("PCB_IMAGE_FILE"))
          image = new TPCBImage(fileName + "_CROP", tim);
        else if (tim.getFileFormat().equalsIgnoreCase("jpeg") || tim.getFileFormat().equalsIgnoreCase("gif") ||
                 tim.getFileFormat().equalsIgnoreCase("tiff") || tim.getFileFormat().equalsIgnoreCase("bmp") || tim.getFileFormat().equalsIgnoreCase("png"))
          image = new TImage(fileName + "_CROP", tim);
       */
             
        tim.setReference(image);
        image.setPath("");
        image.getView().setZoom(element.getView().getGraphicPanel().getZoom());
        image.getModel().addModif(1);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, image);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, image.getView());
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.MIN_AND_MAX, null, new Double(((TImage)element).getImageView().getImageGraphicPanel().getMin()), new Double(((TImage)element).getImageView().getImageGraphicPanel().getMax()));
        TEventHandler.handleMessage(ev);
      //  ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.TRANSFER_CURVE, null, ((TImage)element).getImageView().getImageGraphicPanel().getTransferCurve().getCopy());//2005/11/16 transfer curves removed
       // TEventHandler.handleMessage(ev);//2005/11/16 transfer curves removed
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.CROP_MODE, new Boolean(false));
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TCropAction.getID()), new Boolean(false));//TMenuManager.CROP avant
        TEventHandler.handleMessage(ev);
        element.getView().getGraphicPanel().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      }
    }
  }
  public void draw(Graphics g, double z) {
    if (cropRect != null) {
      g.setColor(Color.red);
      g.drawRect((int)(cropRect.x * z), (int)(cropRect.y * z), (int)(cropRect.width * z), (int)(cropRect.height * z));
    }
  }
  public void reset() {
    cropRect = null;
    rectOver = false;
  }
public void keyPressed(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
