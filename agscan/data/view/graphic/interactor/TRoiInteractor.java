/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * Modification : 2005/10/26 - modification of the cropped image name
 * Modification : 2005/12/19 - TCropInteractor renamed TRoiInteractor...
 * @version 1.0
 */

package agscan.data.view.graphic.interactor;

import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.RGBStackMerge;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Vector;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TImageControler;
import agscan.data.element.TDataElement;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.factory.TImageModelFactory;
import agscan.menu.TMenuManager;
import agscan.menu.action.TRoiAction;

public class TRoiInteractor extends TGraphicInteractor {
  private static TRoiInteractor instance = null;
  private Rectangle roi;
  private int x0, y0;
  private boolean rectOver, resizing, nResize, sResize, wResize, eResize, neResize, seResize, swResize, nwResize;
  private int index_crop;
  
  
  protected TRoiInteractor() {
    super();
    cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
    id = TGraphicInteractor.ROI;
    instance = this;
    roi = null;
    index_crop = 0;
    rectOver = resizing = nResize = sResize = wResize = eResize = neResize = seResize = swResize = nwResize = false;
  }
  public static TRoiInteractor getInstance() {
    if (instance == null)
      instance = new TRoiInteractor();
    return instance;
  }
  public void mousePressed(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    Graphics g = element.getView().getGraphicPanel().getGraphics();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    x0 = (int)((double)event.getX() / zoom);
    y0 = (int)((double)event.getY() / zoom);
    boolean resize = nResize || neResize || eResize || seResize || sResize || swResize || wResize || nwResize;
    if (roi == null) {
    	roi = new Rectangle(x0, y0, 0, 0);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
    else if (!rectOver) {
    	roi = new Rectangle(x0, y0, 0, 0);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
    else if (roi.contains(x0, y0)) {

    }
    else if (!resize) {
    	roi = new Rectangle(x0, y0, 0, 0);
      rectOver = false;
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
    }
  }
  public void mouseReleased(MouseEvent event) {
    rectOver = true;
  }
  public void mouseMoved(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    TGraphicPanel panel = element.getView().getGraphicPanel();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);

    nResize = sResize = wResize = eResize = neResize = seResize = swResize = nwResize = false;
    if (rectOver) {
      if ((x > (roi.x - 3)) && (x < (roi.x + 3)) && (y > (roi.y - 3)) && (y < (roi.y + 3))) {
        panel.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
        nwResize = true;
        neResize = eResize = seResize = sResize = swResize = wResize = nResize = false;
      }
      else if ((x > (roi.x - 3)) && (x < (roi.x + 3)) && (y > (roi.height + roi.y - 3)) && (y < (roi.height + roi.y + 3))) {
        panel.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
        swResize = true;
        neResize = eResize = seResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (roi.x + roi.width - 3)) && (x < (roi.x + roi.width + 3)) && (y > (roi.y - 3)) && (y < (roi.y + 3))) {
        panel.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
        neResize = true;
        swResize = eResize = seResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (roi.x + roi.width - 3)) && (x < (roi.x + roi.width + 3)) && (y > (roi.height + roi.y - 3)) && (y < (roi.height + roi.y + 3))) {
        panel.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
        seResize = true;
        swResize = eResize = neResize = sResize = nwResize = wResize = nResize = false;
      }
      else if ((x > (roi.x + 3)) && (x < (roi.x + roi.width - 3)) && (y > (roi.y - 3)) && (y < (roi.y + 3))) {
        panel.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        nResize = true;
        swResize = eResize = neResize = sResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (roi.x + roi.width - 3)) && (x < (roi.x + roi.width + 3)) && (y > (roi.y + 3)) && (y < (roi.y + roi.height - 3))) {
        panel.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        eResize = true;
        swResize = nResize = neResize = sResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (roi.x + 3)) && (x < (roi.x + roi.width - 3)) && (y > (roi.y + roi.height - 3)) && (y < (roi.y + roi.height + 3))) {
        panel.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
        sResize = true;
        swResize = nResize = neResize = eResize = nwResize = wResize = seResize = false;
      }
      else if ((x > (roi.x - 3)) && (x < (roi.x + 3)) && (y > (roi.y + 3)) && (y < (roi.y + roi.height - 3))) {
        panel.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
        wResize = true;
        swResize = nResize = neResize = eResize = nwResize = sResize = seResize = false;
      }
      else if ((x > (roi.x + 3)) && (x < (roi.x + roi.width - 3)) && (y > (roi.y + 3)) && (y < (roi.y + roi.height - 3))) {
        panel.setCursor(new Cursor(Cursor.MOVE_CURSOR));
        swResize = nResize = neResize = eResize = nwResize = sResize = seResize = wResize = false;
      }
      else
        panel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }
  }
  public void mouseDragged(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    Graphics g = element.getView().getGraphicPanel().getGraphics();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);

    if (!rectOver) {
      if ((x < element.getModel().getElementWidth()) && (y < element.getModel().getElementHeight())) {
        roi.setSize(x - roi.x, y - roi.y);
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
        TEventHandler.handleMessage(ev);
        element.getView().getGraphicPanel().repaint();
      }
    }
    else {
      if (nResize) {
        if (((roi.height + y0 - y) >= 10) && ((roi.y + y - y0) >= 0)) {
          roi.setSize(roi.width, roi.height + y0 - y);
          roi.translate(0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (sResize) {
        if (((roi.height - y0 + y) >= 10) &&
            ((roi.y + roi.height - y0 + y) < element.getModel().getElementHeight())) {
          roi.setSize(roi.width, roi.height - y0 + y);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (wResize) {
        if (((roi.width + x0 - x) >= 10) && ((roi.x + x - x0) >= 0)) {
          roi.setSize(roi.width + x0 - x, roi.height);
          roi.translate(x - x0, 0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (eResize) {
        if (((roi.width - x0 + x) >= 10) &&
            ((roi.x + roi.width - x0 + x) < element.getModel().getElementWidth())) {
          roi.setSize(roi.width - x0 + x, roi.height);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (neResize) {
        if ((roi.y + y - y0) < 0)
          y0 = y;
        else if ((roi.x + roi.width - x0 + x) >= element.getModel().getElementWidth())
          x0 = x;
        else if (((roi.height + y0 - y) >= 10) && ((roi.width - x0 + x) >= 10)) {
          roi.setSize(roi.width - x0 + x, roi.height + y0 - y);
          roi.translate(0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (nwResize) {
        if ((roi.y + y - y0) < 0)
          y0 = y;
        else if ((roi.x + x - x0) < 0)
          x0 = x;
        else if (((roi.height + y0 - y) >= 10) && ((roi.width + x0 - x) >= 10)) {
          roi.setSize(roi.width + x0 - x, roi.height + y0 - y);
          roi.translate(x - x0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (seResize) {
        if ((roi.y + roi.height - y0 + y) >= element.getModel().getElementHeight())
          y0 = y;
        else if ((roi.x + roi.width - x0 + x) >= element.getModel().getElementWidth())
          x0 = x;
        else if (((roi.height - y0 + y) >= 10) && ((roi.width - x0 + x) >= 10)) {
          roi.setSize(roi.width - x0 + x, roi.height - y0 + y);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else if (swResize) {
        if ((roi.y + roi.height - y0 + y) >= element.getModel().getElementHeight())
          y0 = y;
        else if ((roi.x + x - x0) < 0)
          x0 = x;
        else if (((roi.height - y0 + y) >= 10) && ((roi.width + x0 - x) >= 10)) {
          roi.setSize(roi.width + x0 - x, roi.height - y0 + y);
          roi.translate(x - x0, 0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
          x0 = x;
          y0 = y;
        }
      }
      else {
        if (((roi.x + x - x0) >= 0) && ((roi.y + y - y0) >= 0) &&
            ((roi.x + roi.width + x - x0) < element.getModel().getElementWidth()) &&
            ((roi.y + roi.height + y - y0) < element.getModel().getElementHeight())) {
          roi.translate(x - x0, y - y0);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element, null);
          TEventHandler.handleMessage(ev);
          element.getView().getGraphicPanel().repaint();
        }
        x0 = x;
        y0 = y;
      }
    }
  }
  public void mouseClicked(MouseEvent event) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = 1.0D / -zoom;
    int x = (int)((double)event.getX() / zoom);
    int y = (int)((double)event.getY() / zoom);
    if ((event.getClickCount() == 2) && (x > (roi.x + 3)) && (x < (roi.x + roi.width - 3)) && (y > (roi.y + 3)) && (y < (roi.y + roi.height - 3))) {
      TImageModel tim = (TImageModel)element.getModel();
      String fileName = ((TImage)element).getName();
   
      //test remi
      System.out.println("test = "+tim.getFileFormat());
      //fin test remi
      ImageStack cropStack = getRoiStack(element);
  
         //creation du model de la nouvelle image
      TImageModelFactory timf = TImageModelFactory.getImageModelFactory(tim.getFileFormat());
      tim = timf.createImageModel(cropStack, ImagePlusTools.getImageData(tim.getInitialImage(),16), (int)tim.getPixelWidth(), (int)tim.getPixelHeight(),
                                  tim.getElementWidth(), tim.getImageBitDepth(), tim.getUnit(), tim.getIPType(),
                                  ImagePlusTools.getLatitude(), tim.getFileFormat(),roi);
      TImage image = null;
    
    
      if (tim != null) {
      	// creation of the cropped image
      	//2005/10/26 - modification of the cropped image name
        int i = fileName.lastIndexOf((int)'.');//-1 if no extension
    	String cropName = null;//name of the crop image
    	
    	if (i==-1){
    		cropName = fileName + "_CROP"+index_crop;
    	}
    	else{
    		cropName = fileName.substring(0,i)+"_CROP"+index_crop+fileName.substring(i,fileName.length());
    	}
    	
    	System.out.println("========= Affichage RGB");
    	image = new TImage(cropName, tim);
    	RGBStackMerge stackMerge = new RGBStackMerge();
    	ImageStack rgbStack = new ImageStack();
    	ImagePlus[] ip = new ImagePlus[cropStack.getSize()];
    	System.out.println("========= cropStack.getSize()"+cropStack.getSize());
		
		
    	for (int j = 0; j < cropStack.getSize(); j++){
    		ip[j] = new ImagePlus(cropName,cropStack.getProcessor(j+1));
    	}
    	
    	if (cropStack.getSize() > 1){
			for (int k=0;k<cropStack.getSize();k++)
			{
				ip[k] = TImageControler.setInvertedLUT(ip[k],false);
			}
		}

		if (cropStack.getSize() > 2){
			rgbStack = stackMerge.mergeStacks(cropStack.getWidth(), cropStack.getHeight(),ip[0].getStack().getSize(),ip[0].getStack(),ip[1].getStack(),ip[2].getStack(),true);
		}
		if (cropStack.getSize() == 2){
			rgbStack = stackMerge.mergeStacks(cropStack.getWidth(), cropStack.getHeight(),ip[0].getStack().getSize(),ip[0].getStack(),ip[1].getStack(),null,true);
		}
		if (cropStack.getSize() == 1){
			rgbStack = ip[0].getStack();
		}
    	ImagePlus rgb = new ImagePlus(cropName, rgbStack);
    	//rgb.show();
        image.getImageView().setDisplayImage(rgb);
        
        System.out.println("nom image "+image.getName());
         tim.setReference(image);
        image.setPath("");
        image.getView().setZoom(element.getView().getGraphicPanel().getZoom());
        image.getModel().addModif(1);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, image);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, image.getView());
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.MIN_AND_MAX, null, new Double(((TImage)element).getImageView().getImageGraphicPanel().getMin()), new Double(((TImage)element).getImageView().getImageGraphicPanel().getMax()));
        TEventHandler.handleMessage(ev);
      //  ev = new TEvent(TEventHandler.DATA_MANAGER, TImageControler.TRANSFER_CURVE, null, ((TImage)element).getImageView().getImageGraphicPanel().getTransferCurve().getCopy());//2005/11/16 transfer curves removed
       // TEventHandler.handleMessage(ev);//2005/11/16 transfer curves removed
        ev = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ROI_MODE, new Boolean(false));
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.SET_SELECTED, new Integer(TRoiAction.getID()), new Boolean(false));//TMenuManager.CROP avant
        TEventHandler.handleMessage(ev);
        element.getView().getGraphicPanel().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      }
    }
  }
  public void draw(Graphics g, double z) {
    if (roi != null) {
      g.setColor(Color.red);
      g.drawRect((int)(roi.x * z), (int)(roi.y * z), (int)(roi.width * z), (int)(roi.height * z));
    }
  }
  public void reset() {
    roi = null;
    rectOver = false;
     }
/**
 * @return roi
 * 2005/12/19
 */
public Rectangle getROI() {
	return roi;
}


/*
 * @return cropStack - the ImageStack corresponding to the cropped ROI area
 * 2005/12/19
 */
//TODO possible aussi de faire cette methode sans l'element donne (on interroge alors le manager...)
public ImageStack getRoiStack(TDataElement element){

	 TImageModel tim = (TImageModel)element.getModel();
     String fileName = ((TImage)element).getName();
     
    ImageStack stack = tim.getImages();
      ImageStack cropStack = new ImageStack(roi.width,roi.height);
       ImageProcessor currentProc = null;
        ImageProcessor cropProc = null;
 
    for (int i=1;i<=stack.getSize();i++){
      currentProc = stack.getProcessor(i); 
      currentProc.setRoi(roi);
      cropProc = currentProc.crop();
      //2005/12/12 rename slices of the image with the "crop" info
        System.out.println(i+" slice name "+stack.getSliceLabel(i));
      int j = tim.getChannelImage(i).getTitle().lastIndexOf((int)'.');//-1 if no extension
      System.out.println("extention of crop = "+j+" i= "+i);
     
//    //recuperation des titres des onglets ouverts
  	TEvent ev = new TEvent(TEventHandler.MAIN_PANE,TMainPane.GET_NAME_TAB, null);
      Vector v = (Vector)TEventHandler.handleMessage(ev)[0];
      System.out.println(v.toString());
      
    
      String name = null;
  	if (j==-1){
  		name = fileName;
  	}
  	else{
  		name = fileName.substring(0,j);
  		
  	}
      System.out.println("name = "+name);
  	  name = name + "_CROP";
  	  index_crop = 0;
      for(int m = 0 ; m < v.size() ; m++){
      	String curr = (String)v.get(m);
      	System.out.println("curr "+curr+" name = "+name);
      	if(curr.startsWith(name)){
      		index_crop++;
      	}
      }
      
      
      
      String cropName = null;//name of the crop image
      if (j==-1){
    	  cropName = tim.getChannelImage(i).getTitle()+"_CROP"+index_crop;
      }else{
    	  cropName = tim.getChannelImage(i).getTitle().substring(0,j)+"_CROP"+index_crop+tim.getChannelImage(i).getTitle().substring(j,tim.getChannelImage(i).getTitle().length());
    	  //System.err.println("save crop"+cropName);
      }
      //the image channel is added to the stack
      cropStack.addSlice(cropName,cropProc);//on renomme 
    }
    return cropStack;
}

//the max image is the image displayed 
public ImagePlus getMaxImageRoi(){
	 TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
	    TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
	 TImageModel tim = (TImageModel)element.getModel();
     String fileName = ((TImage)element).getName();
     ImageProcessor cropProc = tim.getMaxImage().getProcessor();
     cropProc.setRoi(roi);
     ImagePlus cropImagePlus = new ImagePlus("ROI_MAX_"+fileName,cropProc.crop());
     return cropImagePlus;     
}
public void keyPressed(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
