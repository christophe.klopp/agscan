/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.graphic.interactor;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;

public class TZoomInteractor extends TGraphicInteractor {
  private static TZoomInteractor instance = null;
  private int newX, newY;
  protected TZoomInteractor() {
    super();
    ImageIcon zoomIcon = new ImageIcon(agscan.FenetrePrincipale.application.getClass().getResource("ressources/zoozoom.gif"));
    cursor = Toolkit.getDefaultToolkit().createCustomCursor(zoomIcon.getImage(), (new Point(16, 16)), "zoom");
    id = TGraphicInteractor.ZOOM;
    instance = this;
  }
  public void mousePressed(MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    if ((e.getModifiers() & MouseEvent.BUTTON1_MASK) == MouseEvent.BUTTON1_MASK)
      processZoom(x, y, TMainPane.INC_ZOOM);
    else if ((e.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
      processZoom(x, y, TMainPane.DEC_ZOOM);
  }
  public static TZoomInteractor getInstance() {
    if (instance == null)
      instance = new TZoomInteractor();
    return instance;
  }
  private void processZoom(int x, int y, int z) {
    TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
    double oldValue = ((Integer)TEventHandler.handleMessage(event)[0]).intValue();
    if (oldValue < 0) oldValue = 1.0D / -oldValue;
    event = new TEvent(TEventHandler.MAIN_PANE, z, null);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
    double newValue = ((Integer)TEventHandler.handleMessage(event)[0]).intValue();
    if (newValue < 0) newValue = 1.0D / -newValue;
    x = (int)(x * newValue / oldValue);
    y = (int)(y * newValue / oldValue);
    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_GRAPHIC_SCROLL_PANE, null);
    JScrollPane jsp = (JScrollPane)TEventHandler.handleMessage(event)[0];
    jsp.revalidate();
    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_GRAPHIC_VIEW_SIZE, null);
    Dimension dim = (Dimension)TEventHandler.handleMessage(event)[0];
    int maxX = (int)((double)dim.getWidth() - (double)jsp.getViewport().getWidth());
    int maxY = (int)((double)dim.getHeight() - (double)jsp.getViewport().getHeight());
    if (maxX < 0) maxX = 0;
    if (maxY < 0) maxY = 0;
    newX = x - jsp.getViewport().getWidth() / 2;
    if (newX < 0) newX = 0;
    if (newX > maxX) newX = maxX;
    newY = y - jsp.getViewport().getHeight() / 2;
    if (newY < 0) newY = 0;
    if (newY > maxY) newY = maxY;
    jsp.getViewport().setViewPosition(new Point(newX, newY));
    while ((jsp.getViewport().getViewPosition().getX() != newX) || (jsp.getViewport().getViewPosition().getY() != newY))
      jsp.getViewport().setViewPosition(new Point(newX, newY));
    jsp.revalidate();
    event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement el = (TDataElement)TEventHandler.handleMessage(event)[0];
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, el, null);
    TEventHandler.handleMessage(event);
  }
public void keyPressed(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	if (e.getKeyChar() == '+'){ processZoom(1, 1, TMainPane.INC_ZOOM);}
	if (e.getKeyChar() == '+'){ processZoom(1, 1, TMainPane.DEC_ZOOM);}
	System.out.println("ZOOM "+e.getKeyChar());
	
}
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
