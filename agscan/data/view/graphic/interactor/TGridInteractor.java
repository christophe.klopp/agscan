/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.graphic.interactor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TSpot;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TGridSelectionModeAction;
import agscan.menu.action.TLevel1SelectionModeAction;
import agscan.menu.action.TLevel2SelectionModeAction;
import agscan.menu.action.TSpotSelectionModeAction;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;

public class TGridInteractor extends TGraphicInteractor {
  private static TGridInteractor instance = null;
  protected int x0, y0;
  private Rectangle selectionRect, lastSelectionRect;
  protected boolean dragged;

  protected TGridInteractor() {
    selectionRect = lastSelectionRect = null;
    dragged = false;
  }
  public static TGraphicInteractor getInstance() {
    if (instance == null)
      instance = new TGridInteractor();
    return instance;
  }
  public void mouseMoved(MouseEvent e) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = -1.0D / zoom;
    if ((element instanceof TAlignment) || (element instanceof TGrid)) {
      TSpot spot = ((TGriddedElement)element).getGridModel().getSpot((double)e.getX() / zoom, (double)e.getY() / zoom);
      if (spot != null)
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, null, spot.getName(), Color.blue);
      else
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, null, "", Color.black);
      TEventHandler.handleMessage(ev);
      if (spot != null) {
        //double[] dbl = ((TGriddedElement)element).getGridModel().getSouthAnchor(spot, (TGridBlock)spot.getParent());
        //TSpot leftSpot = ((TGriddedElement)element).getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent());
        //TSpot rightSpot = ((TGriddedElement)element).getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent());
        //TSpot topSpot = ((TGriddedElement)element).getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent());
        //TSpot bottomSpot = ((TGriddedElement)element).getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent());
        //    ((TGriddedElement)element).getGridModel().getRootBlock());
        //System.err.println(dbl[0] + " , " + dbl[1]);
        /*if (leftSpot != null)
          System.err.println("left = " + leftSpot.getName());
        else
          System.err.println("left = null");
        if (rightSpot != null)
          System.err.println("right = " + rightSpot.getName());
        else
          System.err.println("right = null");
        if (topSpot != null)
          System.err.println("top = " + topSpot.getName());
        else
          System.err.println("top = null");
        if (bottomSpot != null)
          System.err.println("bottom = " + bottomSpot.getName());
        else
          System.err.println("bottom = null");*/
      }
    }
  }
  public void mousePressed(MouseEvent e) {
  	System.out.println("plouf");
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_SELECTION_MODE, null);
    int sm = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = -1.0D / zoom;
    Point point = new Point((int)(e.getX() / zoom), (int)(e.getY() / zoom));
    TGridElement concernedElement = getElement(point.x, point.y);
    if (sm == TSpotSelectionModeAction.getID()) {//TMenuManager.SPOT_SELECTION_MODE avant
      x0 = e.getX();
      y0 = e.getY();
      if (!e.isControlDown()) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL, null);
        TEventHandler.handleMessage(ev);
      }
      selectionRect = new Rectangle( (int) (x0 / zoom), (int) (y0 / zoom), (int) ( (e.getX() - x0) / zoom), (int) ( (e.getY() - y0) / zoom));
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.CLEAR_PRESELECTION, null, element);
      TEventHandler.handleMessage(ev);
      Rectangle rect = new Rectangle( (int) (x0 / zoom), (int) (y0 / zoom), (int) ( (e.getX() - x0) / zoom), (int) ( (e.getY() - y0) / zoom));
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.PRESELECT, null, rect, element);
      TEventHandler.handleMessage(ev);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
      TEventHandler.handleMessage(ev);
      dragged = true;
    }
    else {
      if (concernedElement != null) {
        if (e.isControlDown()) {
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.TOGGLE_SELECT_POINT, null, point, new Integer(sm), new Boolean(true));
          TEventHandler.handleMessage(ev);
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
          TEventHandler.handleMessage(ev);
        }
        else {
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.IS_SELECTED, null, concernedElement);
          boolean selected = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
          if (!selected) {
            ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL, null);
            TEventHandler.handleMessage(ev);
            ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.TOGGLE_SELECT_POINT, null, point, new Integer(sm), new Boolean(false));
            TEventHandler.handleMessage(ev);
            ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
            TEventHandler.handleMessage(ev);
          }
        }
        x0 = e.getX();
        y0 = e.getY();
      }
      else if (!e.isControlDown()) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL, null);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
        TEventHandler.handleMessage(ev);
      }
      element.getView().getGraphicPanel().repaint();
    }
  }
  public void mouseDragged(MouseEvent e) {
    dragged = true;
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_SELECTION_MODE, null);
    int sm = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    if (sm == TSpotSelectionModeAction.getID()) {//TMenuManager.SPOT_SELECTION_MODE avant
      ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
      TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
      double zoom = element.getView().getGraphicPanel().getZoom();
      if (zoom < 0) zoom = -1.0D / zoom;
      selectionRect = new Rectangle((int)(x0 / zoom), (int)(y0 / zoom), (int)((e.getX() - x0) / zoom), (int)((e.getY() - y0) / zoom));
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.CLEAR_PRESELECTION, null, element);
      TEventHandler.handleMessage(ev);
      Rectangle rect = new Rectangle((int)(x0 / zoom), (int)(y0 / zoom), (int)((e.getX() - x0) / zoom), (int)((e.getY() - y0) / zoom));
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.PRESELECT, null, rect, element);
      TEventHandler.handleMessage(ev);
      ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
      TEventHandler.handleMessage(ev);
    }
  }
  public void mouseReleased(MouseEvent event) {
    if (dragged) {
      TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_SELECTION_MODE, null);
      int sm = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
      if (sm == TSpotSelectionModeAction.getID()) {//TMenuManager.SPOT_SELECTION_MODE avant
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
        double zoom = element.getView().getGraphicPanel().getZoom();
        if (zoom < 0) zoom = -1.0D / zoom;
        Rectangle rect = new Rectangle((int)(x0 / zoom), (int)(y0 / zoom), (int)((event.getX() - x0) / zoom), (int)((event.getY() - y0) / zoom));
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.CLEAR_PRESELECTION, null, element);
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.TOGGLE_SELECT_RECT, null, rect, new Integer(sm));
        TEventHandler.handleMessage(ev);
        selectionRect = lastSelectionRect = null;
        element.getView().getGraphicPanel().repaint();
        ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
        TEventHandler.handleMessage(ev);
      }
    }
    dragged = false;
  }
  public void draw(Graphics g, double z) {
    if (g != null) {
      g.setXORMode(Color.white);
      g.setColor(Color.green);
      if (lastSelectionRect != null) {
        g.drawRect( (int) (lastSelectionRect.x * z), (int) (lastSelectionRect.y * z), (int) (lastSelectionRect.width * z), (int) (lastSelectionRect.height * z));
      }
      if (selectionRect != null) {
        g.drawRect( (int) (selectionRect.x * z), (int) (selectionRect.y * z), (int) (selectionRect.width * z), (int) (selectionRect.height * z));
      }
      lastSelectionRect = selectionRect;
      g.setPaintMode();
    }
  }
  public boolean isDragged() {
    return dragged;
  }
  protected TGridElement getElement(double x, double y) {
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_SELECTION_MODE, null);
    int sm = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    /*switch (sm) {
    case TSpotSelectionModeAction.getID():  //TMenuManager.SPOT_SELECTION_MODE avant
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_SPOT, null, new Double(x), new Double(y));
        break;
      case TLevel1SelectionModeAction.getID(): //TMenuManager.LEVEL1_SELECTION_MODE avant
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_1, null, new Double(x), new Double(y));
        break;
      case TLevel2SelectionModeAction.getID(): //TMenuManager.LEVEL2_SELECTION_MODE avant
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_2, null, new Double(x), new Double(y));
        break;
      case TGridSelectionModeAction.getID(): //TMenuManager.GRID_SELECTION_MODE avant
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_GRID, null, new Double(x), new Double(y));
        break;
    }*/
    
    //apres un case l'expression doit etre une constante...donc on mets des if à la place...
    if (sm ==TSpotSelectionModeAction.getID()) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_SPOT, null, new Double(x), new Double(y));
    else if (sm == TLevel1SelectionModeAction.getID()) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_1, null, new Double(x), new Double(y));
    else if (sm == TLevel2SelectionModeAction.getID())  ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_2, null, new Double(x), new Double(y));
    else if (sm == TGridSelectionModeAction.getID()) ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_GRID, null, new Double(x), new Double(y));
    // fin modif remi
    return (TGridElement)TEventHandler.handleMessage(ev)[0];
  }
  protected TGridElement getSelectedElement(double x, double y) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_GRID, null, new Double(x), new Double(y));
    TGridElement element = (TGridElement)TEventHandler.handleMessage(ev)[0];
    boolean selected = false;
    if (element != null) {
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.IS_SELECTED, null, element);
      selected = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    }
    if (!selected) {
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_2, null, new Double(x), new Double(y));
      element = (TGridElement)TEventHandler.handleMessage(ev)[0];
      if (element != null) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.IS_SELECTED, null, element);
        selected = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      }
    }
    if (!selected) {
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_BLOCK_1, null, new Double(x), new Double(y));
      element = (TGridElement)TEventHandler.handleMessage(ev)[0];
      if (element != null) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.IS_SELECTED, null, element);
        selected = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      }
    }
    if (!selected) {
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.GET_SPOT, null, new Double(x), new Double(y));
      element = (TGridElement)TEventHandler.handleMessage(ev)[0];
      if (element != null) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.IS_SELECTED, null, element);
        selected = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      }
    }
    if (selected) return element;
    return null;
  }
public void keyPressed(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
