/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.view.graphic.interactor;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.memento.TUndoGridPosition;
import agscan.data.controler.memento.TUndoGridResizing;
import agscan.data.controler.memento.TUndoGridRotation;
import agscan.data.controler.memento.TUndoGridSpotResizing;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TSpot;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

public class TAlignmentInteractor extends TGridInteractor {
  private static TAlignmentInteractor instance = null;
  private boolean resizing, spotResizing, dragging, rotating, northBorder, southBorder, eastBorder, westBorder;
  private int sl;
  private TUndoGridRotation ugo;
  private TUndoGridResizing ugr;
  private TUndoGridSpotResizing ugsr;
  private TUndoGridPosition ugp;

  protected TAlignmentInteractor() {
    super();
    resizing = dragging = spotResizing = rotating = northBorder = southBorder = eastBorder = westBorder = false;
    sl = 0;
  }
  public static TGraphicInteractor getInstance() {
    if (instance == null)
      instance = new TAlignmentInteractor();
    return instance;
  }
  public void mouseReleased(MouseEvent e) {
    if (rotating) {
      ugo.addFinalPosition();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugo);
      TEventHandler.handleMessage(event);
    }
    else if (resizing) {
      ugr.addFinalState();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugr);
      TEventHandler.handleMessage(event);
    }
    else if (spotResizing) {
      ugsr.addFinalState();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugsr);
      TEventHandler.handleMessage(event);
    }
    else if (dragging) {
      ugp.addFinalPosition();
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, ugp);
      TEventHandler.handleMessage(event);
    }
    if (!dragging && !resizing) super.mouseReleased(e);
    dragging = dragged = resizing = spotResizing = rotating = false;
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    element.getView().getGraphicPanel().repaint();
    ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, element);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, element);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null, element);
    TEventHandler.handleMessage(ev);
  }
  public void mousePressed(MouseEvent e) {
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_GRID_MOVABLE, null);
    boolean gridMovable = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    if ((e.getModifiers() & InputEvent.BUTTON1_MASK) > 0) {
      if ((northBorder || westBorder || southBorder || eastBorder) && gridMovable) {
        x0 = e.getX();
        y0 = e.getY();
        resizing = true;
        ugr = new TUndoGridResizing();
      }
      else if (spotResizing && gridMovable) {
        x0 = e.getX();
        y0 = e.getY();
        ugsr = new TUndoGridSpotResizing();
      }
      else {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
        double zoom = element.getView().getGraphicPanel().getZoom();
        if (zoom < 0) zoom = -1.0D / zoom;
        Point point = new Point((int)(e.getX() / zoom), (int)(e.getY() / zoom));
        TGridElement concernedElement = getSelectedElement(point.x, point.y);
        if (concernedElement == null)
          super.mousePressed(e);
        else {
          if (e.isControlDown() || !gridMovable)
            super.mousePressed(e);
          else if (gridMovable) {
            dragging = true;
            ugp = new TUndoGridPosition();
            x0 = e.getX();
            y0 = e.getY();
          }
        }
      }
    }
    else if (((e.getModifiers() & InputEvent.BUTTON3_MASK) > 0) && gridMovable) {
      x0 = e.getX();
      y0 = e.getY();
      rotating = true;
      ugo = new TUndoGridRotation();
    }
  }
  public void mouseMoved(MouseEvent e) {
    super.mouseMoved(e);
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_GRID_MOVABLE, null);
     boolean gridMovable = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = -1.0D / zoom;
    if ((gridMovable) && (element instanceof TAlignment)) {
      double x = (int)(e.getX() / zoom) * element.getModel().getPixelWidth();
      double y = (int)(e.getY() / zoom) * element.getModel().getPixelHeight();
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.IS_ON_NORTH_BORDER, null,
                      new Double(x), new Double(y), new Double(element.getModel().getPixelHeight()));
      northBorder = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.IS_ON_SOUTH_BORDER, null,
                      new Double(x), new Double(y), new Double(element.getModel().getPixelHeight()));
      southBorder = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.IS_ON_EAST_BORDER, null,
                      new Double(x), new Double(y), new Double(element.getModel().getPixelWidth()));
      eastBorder = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.IS_ON_WEST_BORDER, null,
                      new Double(x), new Double(y), new Double(element.getModel().getPixelWidth()));
      westBorder = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_SPOT_RESIZING_MODE, null);
      boolean spotResizingMode = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      spotResizing = false;

      if (!northBorder && !southBorder && !eastBorder && !westBorder && spotResizingMode) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.IS_ON_SPOT_BORDER, null, new Double(x), new Double(y));
        sl = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
        spotResizing = (sl > 0);
        if (sl == TSpot.NE)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
        else if (sl == TSpot.NW)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
        else if (sl == TSpot.SE)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
        else if (sl == TSpot.SW)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
        else
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
      }
      else if (northBorder)
        if (eastBorder)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
      else if (westBorder)
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
      else
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
      else if (southBorder)
        if (eastBorder)
          element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
      else if (westBorder)
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
      else
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
      else if (eastBorder)
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
      else if (westBorder)
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
      else
        element.getView().getGraphicPanel().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
      TSpot spot = ((TAlignmentModel)element.getModel()).getGridModel().getSpot((double)e.getX() / zoom, (double)e.getY() / zoom);
      ((TAlignmentModel)element.getModel()).getGridModel().getConfig().exemptSpotFromColouring(spot);
    }
    element.getView().getGraphicPanel().repaint();
  }

  public void mouseDragged(MouseEvent e) {
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
    TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_GRID_MOVABLE, null);
    boolean gridMovable = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
    double zoom = element.getView().getGraphicPanel().getZoom();
    if (zoom < 0) zoom = -1.0D / zoom;
    double dx = (e.getX() - x0) / zoom;
    double dy = (e.getY() - y0) / zoom;
    if ((e.getModifiers() & InputEvent.BUTTON1_MASK) > 0) {
      if (northBorder && gridMovable) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.RESIZE_NORTH, null, new Double(-dy),
                        new Double(element.getModel().getPixelWidth()), new Double(element.getModel().getPixelHeight()));
        TEventHandler.handleMessage(ev);
      }
      else if (southBorder && gridMovable) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.RESIZE_SOUTH, null, new Double(dy),
                        new Double(element.getModel().getPixelWidth()), new Double(element.getModel().getPixelHeight()));
        TEventHandler.handleMessage(ev);
      }
      if (eastBorder && gridMovable) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.RESIZE_EAST, null, new Double(dx),
                        new Double(element.getModel().getPixelWidth()), new Double(element.getModel().getPixelHeight()));
        TEventHandler.handleMessage(ev);
      }
      else if (westBorder && gridMovable) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.RESIZE_WEST, null, new Double(-dx),
                        new Double(element.getModel().getPixelWidth()), new Double(element.getModel().getPixelHeight()));
        TEventHandler.handleMessage(ev);
      }
      else if (spotResizing && gridMovable) {
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.RESIZE_DIAMETER, null,
                        new Double(dx * element.getModel().getPixelWidth()),
                        new Double(dy * element.getModel().getPixelHeight()), new Integer(sl));
        TEventHandler.handleMessage(ev);
      }
      if (!spotResizing) dragged = true;
      if (!resizing && !spotResizing) {
        if (!dragging)
          super.mouseDragged(e);
        else if (gridMovable){
          double deltax = (e.getX() - x0) / zoom * element.getModel().getPixelWidth();
          double deltay = (e.getY() - y0) / zoom * element.getModel().getPixelHeight();
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ORTHO_TRANSLATE, null, new Double(deltax), new Double(deltay));
          TEventHandler.handleMessage(ev);
          x0 = e.getX();
          y0 = e.getY();
          element.getView().getGraphicPanel().repaint();
          element.getView().getTablePanel().repaint();
        }
      }
      else {
        x0 = e.getX();
        y0 = e.getY();
        element.getView().getGraphicPanel().repaint();
        element.getView().getTablePanel().repaint();
      }
    }
    else if (((e.getModifiers() & InputEvent.BUTTON3_MASK) > 0) && rotating) {
      dragged = true;
      ev = new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, null, new Double(
          (dy * element.getModel().getPixelHeight()) / (e.getX() / zoom * element.getModel().getPixelWidth())));
      TEventHandler.handleMessage(ev);
      element.getView().getGraphicPanel().repaint();
      element.getView().getTablePanel().repaint();
      x0 = e.getX();
      y0 = e.getY();
    }
  }
  public void keyReleased(MouseEvent e) { 
	  System.out.println("keyReleased");
  }
  public void keyPressed(MouseEvent e) {
	  System.out.println("keyPressed");
  }
  public void keyTyped(MouseEvent e) {
	  System.out.println("keyTyped");
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
