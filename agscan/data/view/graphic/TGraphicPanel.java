package agscan.data.view.graphic;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JPanel;

import agscan.data.model.grid.TGridSelectionModel;
import agscan.data.view.graphic.interactor.TAlignmentInteractor;
import agscan.data.view.graphic.interactor.TRoiInteractor;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.data.view.graphic.interactor.TGridInteractor;
import agscan.data.view.graphic.interactor.TMousePositionInteractor;
import agscan.data.view.graphic.interactor.TZoomInteractor;
import agscan.data.view.TViewImpl;

public abstract class TGraphicPanel extends JPanel {
  protected int zoom;
  private TViewImpl view;
  protected Hashtable interactors;
  protected int currentInteractor;

  public TGraphicPanel() {
    view = null;
    zoom = 1;
    interactors = new Hashtable();
    currentInteractor = -1;
    /*
	 * to add KeyListener to graphical window
	 */
	this.setFocusable(true);
	this.setRequestFocusEnabled(true);
	this.requestFocusInWindow(); 
	//this.requestFocus();
	// CK end 
	
    interactors.put(new Integer(TGraphicInteractor.ZOOM), TZoomInteractor.getInstance());
    interactors.put(new Integer(TGraphicInteractor.ROI), TRoiInteractor.getInstance());
    interactors.put(new Integer(TGraphicInteractor.MOUSE_POSITION), TMousePositionInteractor.getInstance());
    interactors.put(new Integer(TGraphicInteractor.GRID), TGridInteractor.getInstance());
    interactors.put(new Integer(TGraphicInteractor.ALIGNMENT), TAlignmentInteractor.getInstance());
    interactors.put(new Integer(TGraphicInteractor.KEY), TAlignmentInteractor.getInstance());

    setDoubleBuffered(true);
    TGraphicInteractor gi = (TGraphicInteractor)interactors.get(new Integer(TGraphicInteractor.MOUSE_POSITION));
    TGraphicInteractor gk = (TGraphicInteractor)interactors.get(new Integer(TGraphicInteractor.KEY));
    if (gi != null) {
      addMouseListener(gi);
      addMouseMotionListener(gi);
      setCursor(gi.getCursor());
    }
    if (gk != null){
      addKeyListener(gk);
    }
  }
  
  public TGraphicPanel(TViewImpl v) {
    this();
    setView(v);
  }
  public int getZoom() {
    return zoom;
  }
  public void setZoom(int z) {
    zoom = z;
    validate();
    repaint();
  }
  public void setView(TViewImpl v) {
    view = v;
    addMouseMotionListener(view);
    revalidate();
  }
  public TViewImpl getView() {
    return view;
  }
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
  }
  public void removeInteractors() {
    removeMouseListener((TGraphicInteractor)interactors.get(new Integer(currentInteractor)));
    removeMouseMotionListener((TGraphicInteractor)interactors.get(new Integer(currentInteractor)));
    setCursor(Cursor.getDefaultCursor());
    currentInteractor = -1;
    TRoiInteractor.getInstance().reset();
  }
  public void setCurrentInteractor(int i) {
    removeInteractors();
    TGraphicInteractor gi = (TGraphicInteractor)interactors.get(new Integer(i));
    if (gi != null) {
      currentInteractor = i;
      addMouseListener(gi);
      addMouseMotionListener(gi);
      setCursor(gi.getCursor());
    }
  }
  public BufferedImage getSnapshot(int dimMax) {
    return null;
  }
  public void refresh() {
    repaint();
  }
  public void repaint(TGridSelectionModel sModel, Vector v) {
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
