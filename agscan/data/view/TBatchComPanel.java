package agscan.data.view;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.List;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.globalalignment.TEdgesFinderGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.algo.globalalignment.TTemplateMatchingGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockTemplateAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentGridAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.data.TDataManager;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.TGridStructureControler;
import agscan.data.controler.TImageControler;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.batch.job.TExportResultsJob;
import agscan.data.model.batch.job.TJobBuilder;
import agscan.data.view.table.TBatchTablePanel;
import agscan.data.view.table.action.TOpenAlignmentAction;
import agscan.data.view.table.action.TRemoveSelectionAction;
import agscan.dialog.TDialogManager;
import agscan.dialog.TGlobalAlignmentDialog;
import agscan.dialog.TLocalAlignmentDialog;
import agscan.dialog.TProcessChooserDialog;
import agscan.dialog.TProcessChooserDialog.CellulesCouleur;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TAction;
import agscan.menu.action.TComputeDiameterAction;
import agscan.menu.action.TComputeFitCorrectionAction;
import agscan.menu.action.TComputeOvershiningCorrectionAction;
import agscan.menu.action.TComputeQMAction;
import agscan.menu.action.TComputeQuantifFitConstAction;
import agscan.menu.action.TComputeQuantifFitVarAction;
import agscan.menu.action.TComputeQuantifImageConstAction;
import agscan.menu.action.TComputeQuantifImageVarAction;
import agscan.menu.action.TComputeSpotQualityDefaultAction;
import agscan.menu.action.TComputeSpotQualityFourProfilesAction;
import agscan.menu.action.TComputeSpotQualityMultiChannelAction;
import agscan.menu.action.TComputeSpotQualityTwoProfilesAction;
import agscan.menu.action.TGlobalAlignmentDefaultAction;
import agscan.menu.action.TGlobalAlignmentEdgesFinderAction;
import agscan.menu.action.TGlobalAlignmentSnifferAction;
import agscan.menu.action.TGlobalAlignmentTemplateMatchingAction;
import agscan.menu.action.TLocalAlignmentBlocAction;
import agscan.menu.action.TLocalAlignmentBlocTemplateAction;
import agscan.menu.action.TLocalAlignmentGridAction;
import agscan.menu.action.TOpenFileAction;
import agscan.menu.action.TProcessChooserAction;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPlugin;
import agscan.plugins.TPluginManager;
import agscan.statusbar.TStatusBar;
import agscan.data.element.batch.TBatch;

/**
 * <p>Titre : AGScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : SIGENAE 2007</p>
 * modified 2005/11/04
 * modified: string externalization since 2006/02/20 
 * @author Nicolas Mary
 */
public class TBatchComPanel extends JPanel {
	
  /**** ATTRIBUTS ****/	
  
  // panels : 
  // Globaux : Nord / Sud
  private JPanel northPanel = new JPanel();
  private JPanel southPanel = new JPanel();
  //
  // 	Nord : 	* Choix du nombre de canaux
  private JPanel nbChanPanel = new JPanel();
  //			* Choix : Alignements ou images
  private JPanel cbAlipan = new JPanel();
  //			* Choix de la grille
  private JPanel gridPanel = new JPanel();
  //			* Choix du repertoire de sortie
  private JPanel outputPanel = new JPanel();
  //			* Ajout/Suppression d'images
  private JPanel northButtonsPanel = new JPanel();
  //
  // 	Sud :	* Choix de la configuration
  private TProcessChooser procPan = new TProcessChooser(TProcessChooserAction.NAME);
  // 			* Lancement du batch
  private JPanel southButtonsPanel = new JPanel();


  // selection du nombre des canaux : label+spinner+entier(memorisation)
  // OU mode ouverture d'alignement : checkbox
  private JCheckBox cbAlignment = new JCheckBox(Messages.getString("TBatchComPanel.0")); //$NON-NLS-1$
  private JLabel jLabelChannels = new JLabel();  
  private JSpinner jSpinnerChannels = new JSpinner(propToList());
  private int nbChannels = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
  
  // boutons d'ajout et suppression d'images
  private JButton addButton = new JButton(new TOpenFileAction(null,true));
  private JButton delButton = new JButton(new TRemoveSelectionAction(null));

  // selection de la grille : label+textfield+bouton("parcourir")
  private JLabel gridLabel = new JLabel();
  private JTextField gridTextField = new JTextField();
  private JButton gridButton = new JButton();

  // selection du repertoire de sortie : label+textfield+bouton("parcourir")
  private JLabel outputDirectoryLabel = new JLabel();
  private JTextField outputDirectoryTextField = new JTextField();
  private JButton outputDirectoryButton = new JButton();
  
  // checkbox : option de sauvegarde des images ds les alignements
  private JCheckBox saveImagesCheckBox = new JCheckBox();

  // bouton de lancement du batch
  private JButton launchButton = new JButton();

  // variable memorisant le batch (modele,vue,controleur)
  private TBatch batch = null;


  
  /**** CONSTRUCTEUR ****/
  public TBatchComPanel(TBatch b) {
    
	// on construit ce panel avec un TBatch
	batch = b;
    try {
      // init des composants
      jbInit();
      // definitions des ecouteurs
      initListeners();
      // init du batch
      init(batch);
      
      if(nbChannels>1)procPan.b_tiff.setEnabled(false);
      
      // mise a jour des listes
      updateScroll(AGScan.prop.getIntProperty("TColorParametersAction.nbcolors")); //$NON-NLS-1$
      // raffraichissement du panel
      refresh();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  /**** DEFINITIONS DES ECOUTEURS ****/
  public void initListeners() {
	  
	  // ecouteur sur le bouton parcourir pour le choix de la grille
	  gridButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        // on declare un selecteur de fichier
    	JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("gridsPath"));//remi 2005/11/29 //$NON-NLS-1$
        // filtrage (on ne peut ouvrir que les .grd)
    	ImageFilter ff = new ImageFilter("grd", Messages.getString("TBatchComPanel.5")); //$NON-NLS-1$ //$NON-NLS-2$
        chooser.addChoosableFileFilter(ff);
        chooser.setFileFilter(ff);
        
        // si on valide le choix de la grille
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          // on memorise le fichier selectionne
          File gridFile = chooser.getSelectedFile();
          // on fournit au batch la grille a utiliser par l'envoi d'un event
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SET_GRID, null, gridFile);
          TEventHandler.handleMessage(ev);
          // on remplit le textfield 
          gridTextField.setText(gridFile.getName());
          // on signale une modification au modele du batch
          batch.getModel().addModif(1);
          // on envoie un event pour la mise a jour de la barre de statut
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          // repertoire d'acces devient le nouveau repertoire par defaut
          // pour la selection de grille
          AGScan.prop.setProperty("gridsPath",gridFile.getParent());//added remi 2005/11/29 //$NON-NLS-1$
        }
      }
    });
	  
	// ecouteur sur le bouton parcourir pour le choix du repertoire de sortie
	// ...meme principe que pour le choix de la grille
    outputDirectoryButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("batchPath"));//remi 2005/11/29 //$NON-NLS-1$
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File outputDir = chooser.getSelectedFile();
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SET_OUTPUT_PATH, null, outputDir.getAbsolutePath());
          TEventHandler.handleMessage(ev);          
          outputDirectoryTextField.setText(outputDir.getAbsolutePath());
          batch.getModel().addModif(1);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          AGScan.prop.setProperty("batchPath",outputDir.getAbsolutePath());//added remi 2005/11/29 //$NON-NLS-1$
       
        
        }
      }
    });
    
    // ecouteur sur le bouton d'ajout d'images
    // une TAction est deja associe au bouton 
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
      
          
    	  // on signale une modification au modele du batch
    	  batch.getModel().addModif(1);
          // on envoie un event pour la mise a jour de la barre de statut
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          //if(((TBatchTablePanel)batch.getView().getTablePanel()).getTable().getRowCount()==0)  
        	  jSpinnerChannels.setEnabled(false); 
      }
    });

    // ecouteur sur le spinner du choix du nombre de canaux
    jSpinnerChannels.addChangeListener(new ChangeListener() {
    public void stateChanged(ChangeEvent event) {
      
    	// on recupere le nombre specifie par le spinner
        nbChannels = Integer.parseInt(jSpinnerChannels.getValue().toString());
        // on met a jour le fichier des parametres avec ce nombre
        AGScan.prop.setProperty("TColorParametersAction.nbcolors",nbChannels); //$NON-NLS-1$
      
        // on envoie un event pour modifier le tableau des images
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.CHANGE_TABLE, null,new Integer(nbChannels));//modified 2005/11/04
        TEventHandler.handleMessage(ev);
        
        updateScroll(nbChannels);
        
        if(nbChannels>1)procPan.b_tiff.setEnabled(false);
        else procPan.b_tiff.setEnabled(true);
        
		// on signale une modification au modele
        batch.getModel().addModif(1);
        // on envoie un event pour la mise a jour de la barre de statut
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      
        //TODO NMARY : voir a quoi ca sert ??
        //batch.getView().setTablePanel();
   
      }
    });
   
    // ecouteur sur le bouton de lancement du batch
    launchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
    	  	// TODO NMARY : apres les tests reactiver cette condition
    	    // si on a au moins 2 alignements et 1 quantif
			if((procPan.conf_nbalign>=1 || cbAlignment.isSelected()) && procPan.conf_nbquantif >=0)
			{
			
				// ajout des plugins/algo/options a la liste des taches (fichier des parametres)
				procPan.initParameters();
				
				// pour chaque element de la configuration
				for(int i=0;i<procPan.etatconfig.size();i++)
				{
					// on regarde quelle est sa categorie
					switch(procPan.etatconfig.get(i))
					{
						// si c'est un alignement
						// on met son nom dans le champ align 
						// du fichier des parametres
						case TProcessChooser.ALIGN : 			
																AGScan.prop.setProperty(
																"TProcessChooserAction.align", //$NON-NLS-1$
																AGScan.prop.getProperty("TProcessChooserAction.align")+"#"+procPan.modconfig.get(i)); //$NON-NLS-1$ //$NON-NLS-2$
									 							break;
						// si c'est une quantification
					    // idem avec le champ quantif
						case TProcessChooser.QUANTIF : 			AGScan.prop.setProperty(
																"TProcessChooserAction.quantif", //$NON-NLS-1$
																procPan.modconfig.get(i));
																break;
						// si c'est l'option snapshot
						// on met 1 dans le champ snap
						case TProcessChooser.OPTION_SNAP :		AGScan.prop.setProperty(
																"TProcessChooserAction.snap", //$NON-NLS-1$
																"1"); //$NON-NLS-1$
																break;
																// si c'est l'option snapshot
																// on met 1 dans le champ snap
						case TProcessChooser.OPTION_TIFF :		AGScan.prop.setProperty(
																"TProcessChooserAction.tiff", //$NON-NLS-1$
																"1"); //$NON-NLS-1$
																break;
						// si c'est l'option export
						// on met 1 dans le champ export
						case TProcessChooser.OPTION_EXPORT :	AGScan.prop.setProperty(
																"TProcessChooserAction.export", //$NON-NLS-1$
																"1"); //$NON-NLS-1$
																break;
					}
				}
				
				// mise a jour
				update();
				
				
				// envoi de l'event commandant le lancement du batch
		        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.RUN, null);
		        TEventHandler.handleMessage(ev);
		        
		        
		        /***********************************************************/
		        
		        /***********************************************************/
		    	
		        
		        
		        // on signale une modification au modele
		        batch.getModel().addModif(1);
		        // on envoie un event pour la mise a jour de la barre de statut
		        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
				
			}   
		 	else{			
				TEvent ev = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.MESSAGE_DIALOG,"Veuillez choisir au moins 1 alignement et 1 quantification.");// externalized 2006/02/02  //$NON-NLS-1$
				TEventHandler.handleMessage(ev);
				return;
		 	}
    	
      }
    });
    
    cbAlignment.addChangeListener(new ChangeListener() {
    	public void stateChanged(ChangeEvent e) {
    	   // on desactive ce qu'il faut
    	   procPan.align.setEnabled(!cbAlignment.isSelected());
    	   saveImagesCheckBox.setEnabled(!cbAlignment.isSelected());
    	   jSpinnerChannels.setEnabled(!cbAlignment.isSelected());
    	   gridTextField.setEnabled(!cbAlignment.isSelected());
    	   gridButton.setEnabled(!cbAlignment.isSelected());
    	    
    	   northButtonsPanel.removeAll();
    	   if (cbAlignment.isSelected())
    	   {
    		   // disable snapshot and tiff saving
    		   procPan.b_tiff.setEnabled(false);
    		   procPan.b_snap.setEnabled(false);
    		   
    		   // on envoie un event pour modifier le tableau des images 
    		   // on n'a plus que 1 colonne
		       TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.CHANGE_TABLE, null,new Integer(1));//modified 2005/11/04
		       TEventHandler.handleMessage(ev);
		       
    		   addButton = new JButton(new AbstractAction(Messages.getString("TBatchComPanel.105")) { //$NON-NLS-1$
    			   public void actionPerformed (ActionEvent e)
    			   {
    				   // on ouvre une boite de dialogue pour choisir le fichier alignement
    				   JFileChooser chooser = new JFileChooser(AGScan.prop.getProperty("imagesPath"));//modified 2005/11/29 //$NON-NLS-1$
    			       chooser.setMultiSelectionEnabled(true);
    			       chooser.addChoosableFileFilter(new ImageFilter("zaf", "Alignements (*.zaf)")); //$NON-NLS-1$ //$NON-NLS-2$
    			       int returnVal = chooser.showOpenDialog(null);
    			       
    			       // si on valide par OK
    			       if (returnVal == JFileChooser.APPROVE_OPTION) {
    			         
    			    	  // on retient les fichier selectionnes
    			    	  File[] files = chooser.getSelectedFiles();        
    			          
    			          // on ajoute les alignements selectionnes a la pile 
    			          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.ADD_JOBS, null, files,new Integer(1));//modified 2005/11/04
    			          TEventHandler.handleMessage(ev);
    			          
    			          // on inscrit le path des alignements dans parameters.properties
    			          AGScan.prop.setProperty("alignmentsPath",chooser.getCurrentDirectory().getAbsolutePath()); //$NON-NLS-1$
    			          
    			          // on met a jour la barre de statut
    			          batch.getModel().addModif(1);
    			          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
    			       }
    			   }
    		   });
    		   northButtonsPanel.add(addButton);
    		   northButtonsPanel.add(delButton);
    		   //System.out.println("CA PASSE MAIS CA FAIT RIEN");
    		   
    		   // on verouille la cb
    	 	   cbAlignment.setEnabled(false);
    	 	   
    	 	   // on vide la table : on selectionne tout et on supprime
    	 	   TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SELECT_ALL, null);
   	 	   	   TEventHandler.handleMessage(event);
    	 	   event = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.REMOVE_SELECTED_JOBS, null);
    	 	   TEventHandler.handleMessage(event);
    	 	   
    	 	    // on selectionne la checkbox saveImages 
    	 	    saveImagesCheckBox.setSelected(true);
    	 	    // et on active l enregistrement des images dans les alignements
    	    	event = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SAVE_IMAGES, null,true);
    	        TEventHandler.handleMessage(event);
    	        // on signale une modification au modele
    	        batch.getModel().addModif(1);
    	        // on envoie un event pour la mise a jour de la barre de statut
    	        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
    	   }
    	   else 
    	   {
    		   addButton = new JButton(new TOpenFileAction(null,true));
    		   addButton.setText(Messages.getString("TBatchComandPanel.2")); //$NON-NLS-1$
    		   northButtonsPanel.add(addButton);
    		   northButtonsPanel.add(delButton);
    	   }
    	   
    	   
    	}
    	
    });
    
    
    // ecouteur sur la checkbox option : sauvegarde des images dans les alignements
    saveImagesCheckBox.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent event) {
        // on envoie un event qui commmande 
    	// la sauvegarde ou non des images dans les alignements (suivant si coche ou pas)
    	TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.SAVE_IMAGES, null,
                               new Boolean(saveImagesCheckBox.isSelected()));
        TEventHandler.handleMessage(ev);
        // on signale une modification au modele
        batch.getModel().addModif(1);
        // on envoie un event pour la mise a jour de la barre de statut
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
      }
    });
    
    delButton.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent event)
    	{
    		if((batch.getModel().getRowCount()==((TBatchTablePanel)batch.getView().getTablePanel()).getTable().getSelectedRowCount())
    		&&(((TBatchTablePanel)batch.getView().getTablePanel()).getTable().isRowSelected(0)))
    		{
    			//System.out.println(((TBatchTablePanel)batch.getView().getTablePanel()).getSelection()[0]);
    			if(!cbAlignment.isSelected())jSpinnerChannels.setEnabled(true);
    		}
    		System.out.println(batch.getModel().getRowCount());
    		
    		// on signale une modification au modele
            batch.getModel().addModif(1);
            // on envoie un event pour la mise a jour de la barre de statut
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
          
    	}
    });
  }

  	/**** LANCEMENT DES TRAITEMENTS ****/
	private TAbstractJob processJob() {
    TJobBuilder.createJob();
    Vector v;
    
    /*** OPTIONS ***/
	//  option export
	if (procPan.etatconfig.contains(procPan.OPTION_EXPORT))
	{
	        TJobBuilder.addExportResultsJob(TExportResultsJob.TEXT);
	}
	    
	// option snapshot
	if (procPan.etatconfig.contains(procPan.OPTION_SNAP))
	{
	        // Aucun traitement ici, c'est le Worker qui 
			// consulte le fichier parametre et fait un snapshot ou pas suivant 
			// la valeur du champ snap (=0 : non, =1 : oui)
			//TJobBuilder.***;
	}
    
	// option snapshot
	if (procPan.etatconfig.contains(procPan.OPTION_TIFF))
	{
	        // Aucun traitement ici, c'est le Worker qui 
			// consulte le fichier parametre et fait un snapshot ou pas suivant 
			// la valeur du champ snap (=0 : non, =1 : oui)
			//TJobBuilder.***;
	}
    
	/*** QUANTIFS ***/
   	// Plugins de quantifs
    for (int k=0;k<TPluginManager.plugs.length;k++)
    {
    	System.out.println("SQUANTIFPLUGIN??? "+TPluginManager.plugs[k].getType().getSuperclass().getName()); //$NON-NLS-1$
    	if((procPan.modconfig.contains(TPluginManager.plugs[k].getName()))
    		&&(TPluginManager.plugs[k].getType().getSuperclass().getName().equals("agscan.plugins.SQuantifPlugin"))) //$NON-NLS-1$
    	{
    		System.out.println("Je suis plugin de quantif"); //$NON-NLS-1$
    		if(k>=10)TJobBuilder.addComputeJob("Q"+k+""+TPluginManager.plugs[k].getName()); //$NON-NLS-1$ //$NON-NLS-2$
    		else TJobBuilder.addComputeJob("Q"+"0"+k+""+TPluginManager.plugs[k].getName()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    	}
    }
    
    // quantif complete
    if (procPan.modconfig.contains(procPan.quantifclass)) 
    {
    	TJobBuilder.addComputeJob(TComputeOvershiningCorrectionAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TComputeFitCorrectionAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TComputeQMAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TQuantifFitComputedAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TQuantifFitConstantAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TQuantifImageComputedAlgorithm.NAME);
    	TJobBuilder.addComputeJob(TQuantifImageConstantAlgorithm.NAME);
    }


    
    /*** ALIGNEMENTS ***/
	// align. local grid
	if (procPan.modconfig.contains(procPan.locgrid)) 
	{
		v = new Vector();
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastBootstrapSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastBootstrapIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastFirstRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastFirstRunIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastSecondRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastSecondRunIterations));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastThirdRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentGridAlgorithm.lastThirdRunIterations));
        v.addElement(new Double(TLocalAlignmentGridAlgorithm.lastSensitivityThreshold));
        Object[] o1 = new Object[4];
        o1[0] = new Double(TLocalAlignmentGridAlgorithm.lastLocT);
        o1[1] = new Double(TLocalAlignmentGridAlgorithm.lastLocC);
        o1[2] = TLocalAlignmentGridAlgorithm.lastLocSpotDetectionAlgorithmName;
        o1[3] = TLocalAlignmentGridAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        v.addElement(o1);
        TJobBuilder.addLocalAlignmentJob(TLocalAlignmentGridAlgorithm.NAME, v);
    }
	// align. local template	
    if (procPan.modconfig.contains(procPan.loctempmatch)) 
    {
        v = new Vector();
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunIterations));
        v.addElement(new Double(TLocalAlignmentBlockAlgorithm.lastSensitivityThreshold));
        v.addElement(new Boolean(TLocalAlignmentBlockAlgorithm.lastGridCheck));
        Object[] o1 = new Object[4];
        o1[0] = new Double(TLocalAlignmentBlockAlgorithm.lastLocT);
        o1[1] = new Double(TLocalAlignmentBlockAlgorithm.lastLocC);
        o1[2] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmName;
        o1[3] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        v.addElement(o1);
        TJobBuilder.addLocalAlignmentJob(TLocalAlignmentBlockTemplateAlgorithm.NAME, v);
    }
 
    // align. local bloc
    else if (procPan.modconfig.contains(procPan.locbloc)) 
    {
    	System.out.println("OKBloc"); //$NON-NLS-1$
    	v = new Vector();
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastBootstrapIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastFirstRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastSecondRunIterations));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunSensitivity));
        v.addElement(new Integer(TLocalAlignmentBlockAlgorithm.lastThirdRunIterations));
        v.addElement(new Double(TLocalAlignmentBlockAlgorithm.lastSensitivityThreshold));
        v.addElement(new Boolean(TLocalAlignmentBlockAlgorithm.lastGridCheck));
        Object[] o1 = new Object[4];
        o1[0] = new Double(TLocalAlignmentBlockAlgorithm.lastLocT);
        o1[1] = new Double(TLocalAlignmentBlockAlgorithm.lastLocC);
        o1[2] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmName;
        o1[3] = TLocalAlignmentBlockAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        v.addElement(o1);
        TJobBuilder.addLocalAlignmentJob(TLocalAlignmentBlockAlgorithm.NAME, v);
    }

    //  align. global template matching
    if (procPan.modconfig.contains(procPan.globtempmatch)) 
    {
        System.out.println("OKTempM"); //$NON-NLS-1$
    	v = new Vector();
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastSearchAngle));
        //v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastGridCenterCross));
        //v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastSpotCicularity));
        //v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastKernelSize));
        //v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMinSpotSize));
        //v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMaxSpotSize));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.laststddevh));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.laststddevv));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastpercentcol));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastpercentrow));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastmaxratioh));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastmaxratiov));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMaxAreaSize));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastMinAreaSize));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastKindKernel));
        v.addElement(new Integer(TTemplateMatchingGlobalAlignmentAlgorithm.lastActiSize));

    	
        TJobBuilder.addGlobalAlignmentJob(TTemplateMatchingGlobalAlignmentAlgorithm.NAME, v);
    }

    // align. global sniffer
    else if (procPan.modconfig.contains(procPan.globsniffer)) 
    {
        v = new Vector();
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastSearchAngle));
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastContrastPercent));
        v.addElement(new Double(TSnifferGlobalAlignmentAlgorithm.lastSearchAngleIncrement));
        v.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.lastSnifferSize));
        Object[] o1 = new Object[11];
        o1[0] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocBootstrapSensitivity);
        o1[1] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocBootstrapIterations);
        o1[2] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocFirstRunSensitivity);
        o1[3] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocFirstRunIterations);
        o1[4] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocSecondRunSensitivity);
        o1[5] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocSecondRunIterations);
        o1[6] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocThirdRunSensitivity);
        o1[7] = new Integer(TSnifferGlobalAlignmentAlgorithm.lastLocThirdRunIterations);
        o1[8] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocSensitivityThreshold);
        o1[9] = new Boolean(TSnifferGlobalAlignmentAlgorithm.lastLocGridCheck);
        Object[] o2 = new Object[4];
        o2[0] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocT);
        o2[1] = new Double(TSnifferGlobalAlignmentAlgorithm.lastLocC);
        o2[2] = TSnifferGlobalAlignmentAlgorithm.lastLocSpotDetectionAlgorithmName;
        o2[3] = TSnifferGlobalAlignmentAlgorithm.lastLocSpotDetectionAlgorithmParameters;
        o1[10] = o2;
        v.addElement(o1);
        TJobBuilder.addGlobalAlignmentJob(TSnifferGlobalAlignmentAlgorithm.NAME, v);
    }

    //  Plugins d'alignement
    for (int k=0;k<TPluginManager.plugs.length;k++)
    {
    	System.out.println("SALIGNPLUGIN??? "+TPluginManager.plugs[k].getType().getSuperclass().getName()); //$NON-NLS-1$
    	if((procPan.modconfig.contains(TPluginManager.plugs[k].getName()))
    		&&(TPluginManager.plugs[k].getType().getSuperclass().getName().equals("agscan.plugins.SAlignPlugin"))) //$NON-NLS-1$
    	{
    		System.out.println("Je suis plugin d'alignement"); //$NON-NLS-1$
    		if(k>=10)TJobBuilder.addComputeJob("A"+k+""+TPluginManager.plugs[k].getName()); //$NON-NLS-1$ //$NON-NLS-2$
    		else TJobBuilder.addComputeJob("A"+Messages.getString("TBatchComPanel.40")+k+""+TPluginManager.plugs[k].getName()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    	}
    }
  
    
    
    
    return TJobBuilder.getJob();
  }

  private void jbInit() throws Exception {
    
	 this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    
    // choix des configurations pour chacun des panels
    northPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    southPanel.setLayout(new BoxLayout(southPanel,BoxLayout.Y_AXIS));
    northButtonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    cbAlipan.setLayout(new FlowLayout(FlowLayout.CENTER));
    nbChanPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    gridPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    outputPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    southButtonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    
    // ajout d'une bordure et d'un titre sur chacun des panels
    northPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TBatchComPanel.42"))); //$NON-NLS-1$
    cbAlipan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TBatchComPanel.43"))); //$NON-NLS-1$
    southPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.ORANGE),Messages.getString("TBatchComPanel.44"))); //$NON-NLS-1$
    northButtonsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TBatchComPanel.45"))); //$NON-NLS-1$
    nbChanPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TBatchComPanel.46"))); //$NON-NLS-1$
    southButtonsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TBatchComPanel.47"))); //$NON-NLS-1$
    
    // init des labels, champs de texte, icones de boutons
    gridLabel.setText(Messages.getString("TBatchComandPanel.0")); //$NON-NLS-1$
    gridTextField.setText(Messages.getString("TBatchComandPanel.1")); //$NON-NLS-1$
    gridTextField.setColumns(15);
    gridButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/open.gif"))); //$NON-NLS-1$
    addButton.setText(Messages.getString("TBatchComandPanel.2")); //$NON-NLS-1$
    jLabelChannels.setText(Messages.getString("TBatchComandPanel.3")); //$NON-NLS-1$
    delButton.setText(Messages.getString("TBatchComPanel.53")); //$NON-NLS-1$
    outputDirectoryLabel.setText(Messages.getString("TBatchComandPanel.5")); //$NON-NLS-1$
    outputDirectoryTextField.setText(Messages.getString("TBatchComandPanel.7")); //$NON-NLS-1$
    outputDirectoryTextField.setColumns(15);
    outputDirectoryButton.setIcon(new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/open.gif"))); //$NON-NLS-1$
    saveImagesCheckBox.setText(Messages.getString("TBatchComandPanel.8")); //$NON-NLS-1$
    launchButton.setFont(new java.awt.Font("Dialog", Font.BOLD, 10)); //$NON-NLS-1$
    launchButton.setForeground(Color.blue);
    launchButton.setText(Messages.getString("TBatchComandPanel.9")); //$NON-NLS-1$
   
    // panel nord
    JPanel northWestPanel = new JPanel();
    JPanel northEastPanel = new JPanel();
    northWestPanel.setLayout(new BoxLayout(northWestPanel,BoxLayout.Y_AXIS));
    northEastPanel.setLayout(new GridLayout(5,1));//new BoxLayout(northEastPanel,BoxLayout.Y_AXIS));    
    
    nbChanPanel.add(jLabelChannels);
    int val = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors"); //$NON-NLS-1$
    ((SpinnerListModel)jSpinnerChannels.getModel()).setValue(Integer.toString(val));
    nbChanPanel.add(jSpinnerChannels);
    northButtonsPanel.add(addButton);
    northButtonsPanel.add(delButton);
    cbAlipan.add(cbAlignment);
    northWestPanel.add(cbAlipan);
    northWestPanel.add(nbChanPanel);
    northWestPanel.add(northButtonsPanel);
    
    
    gridPanel.add(gridLabel);
    gridPanel.add(gridButton);
    northEastPanel.add(gridPanel);
    northEastPanel.add(gridTextField);
    outputPanel.add(outputDirectoryLabel);
    outputPanel.add(outputDirectoryButton);
    northEastPanel.add(outputPanel);
    northEastPanel.add(outputDirectoryTextField);
    northEastPanel.add(saveImagesCheckBox);
    northPanel.add(northWestPanel);
    northPanel.add(northEastPanel);
    
    // panel sud
    southButtonsPanel.add(launchButton);
    southPanel.add(procPan);
    southPanel.add(southButtonsPanel);
    
    // global
    this.add(northPanel);
    this.add(southPanel);
   
  }
  
  /**** INITIALISATION DU BATCH ****/
  public void init(TBatch b) {
  
	batch = b;
	  
	// on recupere le nom des taches a effectuer  
    String s = ((TBatchModelNImages)batch.getModel()).getJobString();
    
    // on separe les taches et on les met dans un vecteur
    StringTokenizer strtok = new StringTokenizer(s, "|"); //$NON-NLS-1$
    Vector tokens = new Vector();
    while (strtok.hasMoreTokens()) {
      tokens.addElement(strtok.nextToken());
    }
    System.out.println("TACHES : "+tokens.toString()); //$NON-NLS-1$
    
    // init des alignements
    TLocalAlignmentAlgorithm la = ((TBatchModelNImages)batch.getModel()).getJob().getLocalAlignmentAlgorithm();
    if (la != null) {
      la.getControlPanel().init(la);
      la.setLasts();
    }
    TGlobalAlignmentAlgorithm ga = ((TBatchModelNImages)batch.getModel()).getJob().getGlobalAlignmentAlgorithm();
    if (ga != null) {
      ga.getControlPanel().init(ga);
      ga.setLasts();
    }
    
    // on cree le fichier de sortie si il n'existe pas deja 
    // on l'initialise a "./"
    File output= new File(batch.getOutputPath());
    if (output.exists()) outputDirectoryTextField.setText(batch.getOutputPath());
    else {
    	outputDirectoryTextField.setText("."+File.separator); //$NON-NLS-1$
    	batch.setOutputPath("."+File.separator); //$NON-NLS-1$
    	AGScan.prop.setProperty("batchPath","."+File.separator); //$NON-NLS-1$ //$NON-NLS-2$
    }
    // init du textfield qui contient le nom du fichier grille
    gridTextField.setText(((TBatchModelNImages)batch.getModel()).getGridName());
    // init de l'option de sauvegarde des images dans les alignements
    saveImagesCheckBox.setSelected(((TBatchModelNImages)batch.getModel()).getSaveImages());
    
    refresh();
  }
  
  /**** MISE A JOUR ****/
  public void update() {
    
	// mise a jour de la liste des taches
	TAbstractJob job = processJob();
    ((TBatchModelNImages)batch.getModel()).setJob(job);
  }
  
  
  /**** RAFFRAICHISSEMENT DU PANEL ****/
  public void refresh() {
    
	// si le batch n'est pas en cours
	if (!batch.isBatchRunning()) {
      // on active les boutons et la checkbox 
	  gridButton.setEnabled(!cbAlignment.isSelected());
      outputDirectoryButton.setEnabled(true);
      saveImagesCheckBox.setEnabled(!cbAlignment.isSelected());
      addButton.setEnabled(true);
      delButton.setEnabled(batch.getModel().getRowCount() > 0);
      launchButton.setEnabled((batch.getModel().getRowCount() > 0) && (!gridTextField.getText().equals("")||cbAlignment.isSelected())); //$NON-NLS-1$
    }
	// sinon on les desactive
    else {
      gridButton.setEnabled(false);
      outputDirectoryButton.setEnabled(false);
      saveImagesCheckBox.setEnabled(false);
      addButton.setEnabled(false);
      delButton.setEnabled(false);
      launchButton.setEnabled(false);
    }
    // si la vue du batch a ete construite, on met a jour le PopMenu
	if (batch.getView() != null)
      ((TBatchTablePanel)batch.getView().getTablePanel()).getPopupMenu().refresh(batch);
  }


   /**** FOURNIT LA LISTE A AFFECTER AU SPINNER (TRIEE PAR ORDRE CROISSANT) 
    * EN FONCTION DES CONFIGURATIONS DU MENU PARAMETRES DES COULEURS
    * @return la liste des nombres de canaux possibles
    */
   public SpinnerListModel propToList()
   {
	   int nbconf = AGScan.prop.getIntProperty("TColorParametersAction.nbconfig"); //$NON-NLS-1$
	   int[] l = new int[nbconf];
	   for(int i=0;i<nbconf;i++)
	   {
		   l[i]=Integer.parseInt(AGScan.prop.getProperty("TColorParametersAction.colors"+(i+1)).substring(0,1)); //$NON-NLS-1$
	   }
	   return new SpinnerListModel(sortList(l));
   }
   
   /**** TRIE LA LISTE PASSEE EN PARAMETRE DE MANIERE CROISSANTE
    * @return la liste triee
    * @param l liste d'entiers non tries sous forme de liste de String 
    */
   public String[] sortList(int[] l)
   {
	   /***PRODECURE Tri_bulle (Tableau a[1:n])

	   VARIABLE permut : Booleen;

	 	REPETER
	     		permut = FAUX
	     		POUR i VARIANT DE 1 a N-1 FAIRE
	         		SI a[i] > a[i+1] ALORS
	 				echanger a[i] et a[i+1]
	 				permut = VRAI
	 			FIN SI
	 		FIN POUR
	 	TANT QUE permut = VRAI

	 	FIN PROCEDURE***/
	   boolean permut;
	   int temp;
	   do
	   {
		   permut = false;
		   for(int i=0;i<l.length-1;i++)
		   {
			   if(l[i]>l[i+1])
			   {
				   temp = l[i];
				   l[i]=l[i+1];
				   l[i+1]=temp;
				   permut = true;
			   }
		   }
	   }
	   while(permut);
	
	   // recopie dans la liste de String
	   String[] liste = new String[l.length];
	   for(int i=0;i<l.length;i++)
	   {
		   liste[i]=Integer.toString(l[i]);
	   }
	   return liste;
	  
   }
   
   
   /**** FOURNIT LA CONFIGURATION DE BATCH CHOISIE SOUS FORME DE LISTE 
    * @return la liste des taches et options a appliquer au batch
    */
   public Vector<String> getProc()
   {
	   Vector<String> proc = new Vector<String>();
	   
	   // alignements
	   proc.add(AGScan.prop.getProperty("TProcessChooserAction.align")); //$NON-NLS-1$
	   // quantif
	   proc.add(AGScan.prop.getProperty("TProcessChooserAction.quantif")); //$NON-NLS-1$
	   // snapshot (0:non, 1:oui)
	   proc.add(AGScan.prop.getProperty("TProcessChooserAction.snap")); //$NON-NLS-1$
	   // tiff (0:non, 1:oui)  CK 20070716
	   proc.add(AGScan.prop.getProperty("TProcessChooserAction.tiff")); //$NON-NLS-1$
	   // export (0:non, 1:oui)
	   proc.add(AGScan.prop.getProperty("TProcessChooserAction.export")); //$NON-NLS-1$
	   
	   return proc;
   }
   
   /**** DECOMPOSE LA CHAINE DE CARACTERES PASSEE EN PARAMETRE EN UNE LISTE 
    * LE CARACTERE '#' EST LE SEPARATEUR
    * @param align chaine de carateres a decomposer
    * @return liste de chaine de caracteres apres decomposition
    */
 	public Vector<String> parseAlign(String align) {
		
 		Vector<String> parsed = new Vector<String>();
		// on recupere le nombre d'alignements
		int nbali=0;
		String temp=align;
		while(temp.lastIndexOf('#')!=-1)
		{
			temp = temp.substring(0,temp.lastIndexOf('#'));
			nbali++;
 		}
		
		// on decompose la chaine : 
		// on recupere les groupes de caracteres separes par des virgules
		String toto = "^#"; //$NON-NLS-1$
		switch(nbali)
		{
			case 1 : break;
			case 2 : toto = toto+"([^#]*)#";break; //$NON-NLS-1$
			default : break;
		}
		toto = toto+"([^#]*)"; //$NON-NLS-1$
		Pattern p = Pattern.compile(toto);
		Matcher m = p.matcher(align);
		
		// chaque element est une couleur que l'on place dans le tableau
		if( m.matches())
		for( int i = 0; i< m.groupCount(); i++)
		{
			//System.out.println(m.group(i+1));
			parsed.add(m.group(i+1));
		}
		return parsed;
 	}
 	
 	public void setChannels(boolean b)
 	{
 		jSpinnerChannels.setEnabled(b);
 	}
 	
 	public void updateScroll(int nbChannels)
 	{
 		// 	 tri des plugins en fonction de la valeur du spinner
		for (int i=0; i< TPluginManager.plugs.length;i++)
		{
			TPlugin plugin = TPluginManager.plugs[i];
			if ((plugin != null)
					&&		((plugin instanceof SQuantifPlugin)
							||(plugin instanceof SAlignPlugin))&&plugin.isBatchPlugin())
			{
					if(plugin instanceof SQuantifPlugin)
					{
							int min = ((SQuantifPlugin)plugin).getImagesMin();
							int max = ((SQuantifPlugin)plugin).getImagesMax();
							if((nbChannels<min || nbChannels>max)
								&&(procPan.modquantif.contains(plugin.getName())))
							{
								procPan.modquantif.remove(plugin.getName());
								procPan.quantif.setListData(procPan.modquantif);	//refresh
							}
							else if ((nbChannels>=min && nbChannels<=max)
								&&(!procPan.modquantif.contains(plugin.getName())))
							{
								procPan.modquantif.add(plugin.getName());
								procPan.quantif.setListData(procPan.modquantif);	//refresh
							}
					}
					else
					{
							int min = ((SAlignPlugin)plugin).getImagesMin();
							int max = ((SAlignPlugin)plugin).getImagesMax();
							if((nbChannels<min || nbChannels>max)
								&&(procPan.modalign.contains(plugin.getName())))
							{
								procPan.modalign.remove(plugin.getName());
								procPan.align.setListData(procPan.modalign);	//refresh
							}
							else if((nbChannels>=min && nbChannels<=max)
								&&(!procPan.modalign.contains(plugin.getName())))
							{
								procPan.modalign.add(plugin.getName());
								procPan.align.setListData(procPan.modalign);	//refresh
							}
					}
					
			}
		}
		
		//tri des algos EN DUR en fonction de la valeur du spinner
		switch (nbChannels)
		{
			case 1 :	
						if (!procPan.modalign.contains(procPan.locgrid))procPan.modalign.add(procPan.locgrid);
						if (!procPan.modalign.contains(procPan.locbloc))procPan.modalign.add(procPan.locbloc);
						if (!procPan.modalign.contains(procPan.loctempmatch))procPan.modalign.add(procPan.loctempmatch);
						if (!procPan.modalign.contains(procPan.globsniffer))procPan.modalign.add(procPan.globsniffer);
						if (!procPan.modalign.contains(procPan.globtempmatch))procPan.modalign.add(procPan.globtempmatch);
						procPan.align.setListData(procPan.modalign);	//refresh
						break;
						
			
			case 2 :
			case 3 :
						if (procPan.modalign.contains(procPan.locgrid))procPan.modalign.remove(procPan.locgrid);
						if (procPan.modalign.contains(procPan.locbloc))procPan.modalign.remove(procPan.locbloc);
						if (!procPan.modalign.contains(procPan.loctempmatch))procPan.modalign.add(procPan.loctempmatch);
						if (procPan.modalign.contains(procPan.globsniffer))procPan.modalign.remove(procPan.globsniffer);
						if (!procPan.modalign.contains(procPan.globtempmatch))procPan.modalign.add(procPan.globtempmatch);
						procPan.align.setListData(procPan.modalign);	//refresh
						break;
						
			default :
						if (procPan.modalign.contains(procPan.locgrid))procPan.modalign.remove(procPan.locgrid);
						if (procPan.modalign.contains(procPan.locbloc))procPan.modalign.remove(procPan.locbloc);
						if (!procPan.modalign.contains(procPan.loctempmatch))procPan.modalign.add(procPan.loctempmatch);
						if (procPan.modalign.contains(procPan.globsniffer))procPan.modalign.remove(procPan.globsniffer);
						if (!procPan.modalign.contains(procPan.globtempmatch))procPan.modalign.add(procPan.globtempmatch);	
						procPan.align.setListData(procPan.modalign);	//refresh
						break;
		}
 		
 	}

 	public void setNbChannels(int n)
 	{
 		jSpinnerChannels.setValue((Object)n);
 	}
 	/**** CLASSE PERMETTANT DE CREER UNE CONFIGURATION DE BATCH 
 	 * @author nmary
 	 ****/
	public class TProcessChooser extends JPanel{
	
		/**** ATTRIBUTS ****/
		// Constantes
		private final static int ALIGN = 0;
		private final static int QUANTIF = 1;
		private final static int OPTION_SNAP = 3;
		private final static int OPTION_EXPORT = 4;
		private final static int OPTION_TIFF = 5;
		
		// Liste des noms d'algos d'alignement
		private Vector<String> 	v_align;
		// Liste des noms d'algos de quantification
		private Vector<String> 	v_quantif;
		// Liste des noms de plugins+algos d'alignement
		private Vector<String> 	modalign;
		// Liste des noms de plugins+algos de quantification
		private Vector<String> 	modquantif;
		// Liste des noms de plugins+algos+options de la configuration
		private Vector<String> 	modconfig;
		// Liste d'entiers representant les "categories" des elements de la configuration
		// 0 : plugin/algo d'alignement
		// 1 : plugin/algo de quantification
		// 3 : option snapshot
		// 4 : option exportation des resultats dans un fichier
		private Vector<Integer>	etatconfig;
		// JList des quantifications
		private JList quantif;
		// JList des alignements
		private JList align;
		// JList representant la configuration cree
		private JList config;
		// Bouton pour ajouter l'option export
		private JButton b_export;
		// Bouton pour ajouter l'option snapshot
		private JButton b_snap;
		// Button to add tiff saving
		private JButton b_tiff;
		// Bouton pour ajouter un plugin/algo d'alignement ou de quantification
		private JButton b_add;
		// Bouton pour supprimer un plugin/algo d'alignement ou de quantification
		private JButton b_rem;
		// Memorise le nombre d'alignement presents dans la configuration
		private int conf_nbalign;
		// Memorise le nombre de quantifications presentes dans la configuration
		private int conf_nbquantif;
		
		// Listes des noms d'algos 
		//alignement local
		String locgrid;
		String loctempmatch;
		String locbloc;
		//alignement global
		String globtempmatch;
		String globsniffer;
		// quantifs
		String quantifclass;
		
		
		/**** CLASSE POUR SURLIGNER EN COULEUR DANS UNE JLIST ****/
		public class CellulesCouleur extends JLabel implements ListCellRenderer {
		     // This is the only method defined by ListCellRenderer.
		     // We just reconfigure the JLabel each time we're called.
	
		     public Component getListCellRendererComponent(
		       JList list,
		       Object value,            // value to display
		       int index,               // cell index
		       boolean isSelected,      // is the cell selected
		       boolean cellHasFocus)    // the list and the cell have the focus
		     {
		        
		    	 String s = value.toString();
		         setText(s);
		   	   if (isSelected) {
		             setBackground(list.getSelectionBackground());
			       setForeground(list.getSelectionForeground());
			   }
		         else {
			       setBackground(list.getBackground());
			       setForeground(list.getForeground());
			   }
			   // Ici, tu rajoutes ton code pour changer la couleur de ta ligne :
			   if (etatconfig.get(index)==ALIGN)
			       setBackground(Color.CYAN);
			   else if (etatconfig.get(index)==QUANTIF)
				   setBackground(Color.YELLOW);
			   else if ((etatconfig.get(index)==OPTION_SNAP)
			   		   ||(etatconfig.get(index)==OPTION_TIFF)
					   ||(etatconfig.get(index)==OPTION_EXPORT))
				   setBackground(Color.ORANGE);
	
			   setEnabled(list.isEnabled());
			   setFont(list.getFont());
		         setOpaque(true);
		         return this;
		     }
		 }
		
		/**** CONSTRUCTEUR ****/
		public TProcessChooser(String name)
		{
			
			// Allocations memoires pour les listes de donnees
			v_align = new Vector<String>();
			v_quantif = new Vector<String>();
			etatconfig = new Vector<Integer>();
			
			/* Recuperation EN DUR des noms d'algos d'alignement */
			//alignement local
			locgrid= ((TAction)TMenuManager.getAction(TLocalAlignmentGridAction.getID())).getValue(name).toString();
			loctempmatch = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocTemplateAction.getID())).getValue(name).toString();
			locbloc = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocAction.getID())).getValue(name).toString();
			//alignement global
			globtempmatch = ((TAction)TMenuManager.getAction(TGlobalAlignmentTemplateMatchingAction.getID())).getValue(name).toString();
			globsniffer = ((TAction)TMenuManager.getAction(TGlobalAlignmentSnifferAction.getID())).getValue(name).toString();
			
			quantifclass = Messages.getString("TBatchComPanel.77"); //$NON-NLS-1$
			
			
			/* Ajout des noms aux vecteurs v_align et v_quantif */
			v_align.add(locgrid);
			v_align.add(loctempmatch);
			v_align.add(locbloc);
			v_align.add(globtempmatch);
			v_align.add(globsniffer);
			v_quantif.add(quantifclass);
			
	
				// bouton d'ajout d'un algo/plugin a la config.
				Icon ic_add = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/right.gif")); //$NON-NLS-1$
				Action act_add = new AbstractAction(Messages.getString("TBatchComPanel.79"),ic_add) { //$NON-NLS-1$
					public void actionPerformed(ActionEvent e)
					{
						// si une quantif est selectionnee 
						// et qu il n y en a pas encore dans la config
						if(!quantif.isSelectionEmpty() && conf_nbquantif<1) 
						{
							int sel = quantif.getSelectedIndex();
							
							// on ajoute la quantif. a la liste de config.
							modconfig.add(modquantif.get(sel));
							config.setListData(modconfig); 		//refresh
							
							// on retire la quantif. de la liste des quantif.
							modquantif.removeElementAt(sel);
							quantif.setListData(modquantif); 	//refresh
							etatconfig.add(QUANTIF);
							conf_nbquantif++;
						}
						else if (!align.isSelectionEmpty() && conf_nbalign<2)
						{
							int sel = align.getSelectedIndex();
							
							// on ajoute l align. a la liste de config.
							modconfig.add(modalign.get(sel));
							config.setListData(modconfig); 		//refresh
							
							// on retire l align. de la liste des align.
							modalign.removeElementAt(sel);
							align.setListData(modalign); 		//refresh
							etatconfig.add(ALIGN);
							conf_nbalign++;
						}
			    		// on signale une modification au modele
			            batch.getModel().addModif(1);
			            // on envoie un event pour la mise a jour de la barre de statut
			            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
			          
					}
				};
				b_add = new JButton(act_add);
				
				// bouton de suppression d'un algo/plugin de la config.
				Icon ic_rem = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/undo_edit.gif")); //$NON-NLS-1$
				Action act_rem = new AbstractAction(Messages.getString("TBatchComPanel.81"),ic_rem) { //$NON-NLS-1$
					public void actionPerformed(ActionEvent e)
					{
						// si on a selectionne un element dans la liste de config.
						if (!config.isSelectionEmpty())
						{
							int sel = config.getSelectedIndex();
							
							switch(etatconfig.get(sel))
							{
								// si c est un alignement
								case ALIGN : 	
												// on le remet dans la liste des align.
												modalign.add(modconfig.get(sel));
												align.setListData(modalign);		//refresh
											 	conf_nbalign--;
											 	break;
								
								// si c est une quantification
								case QUANTIF : 	
												// on le remet dans la liste des quantif.
												modquantif.add(modconfig.get(sel));
								 				quantif.setListData(modquantif);	//refresh
								 				conf_nbquantif--;
								 				break;
								
								// si c est une option 
								// on reactive le bouton correspondant
								case OPTION_SNAP : 	
												b_snap.setEnabled(true);
					 							break;
								case OPTION_TIFF : 	
												b_tiff.setEnabled(true);
												break;
								case OPTION_EXPORT : 	
												b_export.setEnabled(true);
					 							break;				
					 							
							}
							// on supprime l element selectionne de la liste de config.
							etatconfig.removeElementAt(sel);
							modconfig.removeElementAt(sel);		
							config.setListData(modconfig);		//refresh
						}
			    		// on signale une modification au modele
			            batch.getModel().addModif(1);
			            // on envoie un event pour la mise a jour de la barre de statut
			            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
			          
					}
				};
				b_rem = new JButton(act_rem);
				
				
				// Choix d'une configuration BoxLayout verticale
				this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			
				// 	1 JList pour les quantifications disponibles
				// +1 JList pour les alignements disponibles
				// +1 JList pour la configuration choisie
				// +3 Vecteurs contenant les donnees associees a chacune des JList
				modalign = new Vector<String>();
				modquantif = new Vector<String>();
				modconfig = new Vector<String>();
				align = new JList(modalign);
				align.setVisibleRowCount(7);
				quantif = new JList(modquantif);
				quantif.setVisibleRowCount(7);
				config = new JList(modconfig);
				config.setVisibleRowCount(7);
				config.setCellRenderer(new CellulesCouleur());
				
				// Bouton pour activer le mode snapshot
				Icon ic_snap = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/zoozoom.gif")); //$NON-NLS-1$
				Action act_snap = new AbstractAction(Messages.getString("TProcessChooserDialog.4"),ic_snap) //$NON-NLS-1$
				{
					public void actionPerformed(ActionEvent e)
					{
							// on ajoute l option a la liste de config.
							modconfig.add(Messages.getString("TBatchComPanel.84"));			// A MODIFIER !!! //$NON-NLS-1$
							config.setListData(modconfig); 			//refresh
							
							// on desactive le bouton de l'option
							b_snap.setEnabled(false);
							
							etatconfig.add(OPTION_SNAP);
							
				    		// on signale une modification au modele
				            batch.getModel().addModif(1);
				            // on envoie un event pour la mise a jour de la barre de statut
				            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
				          
					}
					
				};
				b_snap = new JButton(act_snap);
				b_snap.setFont(new java.awt.Font("Dialog", Font.PLAIN, 9)); //$NON-NLS-1$
				
				
				// Bouton to activate tiff mode
				Icon ic_tiff = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/save.gif")); //$NON-NLS-1$
				Action act_tiff = new AbstractAction(Messages.getString("TProcessChooserDialog.7"),ic_tiff) //$NON-NLS-1$
				{
					public void actionPerformed(ActionEvent e)
					{
							// on ajoute l option a la liste de config.
							modconfig.add(Messages.getString("TBatchComPanel.85"));			// A MODIFIER !!! //$NON-NLS-1$
							config.setListData(modconfig); 			//refresh
							
							// on desactive le bouton de l'option
							b_tiff.setEnabled(false);
							
							etatconfig.add(OPTION_TIFF);
							
				    		// on signale une modification au modele
				            batch.getModel().addModif(1);
				            // on envoie un event pour la mise a jour de la barre de statut
				            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
				          
					}
					
				};
				b_tiff = new JButton(act_tiff);
				b_tiff.setFont(new java.awt.Font("Dialog", Font.PLAIN, 9)); //$NON-NLS-1$
				
				// Bouton pour activer l'exportation des resultats 
				// de la quantification dans un fichier
				Icon ic_export = new ImageIcon(FenetrePrincipale.application.getClass().getResource("ressources/close.gif")); //$NON-NLS-1$
				Action act_export = new AbstractAction(Messages.getString("TProcessChooserDialog.5"),ic_export) //$NON-NLS-1$
				{
					public void actionPerformed(ActionEvent e)
					{
							// on ajoute l option a la liste de config.
							modconfig.add(Messages.getString("TBatchComPanel.88"));	// A MODIFIER !!! //$NON-NLS-1$
							config.setListData(modconfig); 				//refresh
							
							// on desactive le bouton de l'option
							b_export.setEnabled(false);
							
							etatconfig.add(OPTION_EXPORT);
							
				    		// on signale une modification au modele
				            batch.getModel().addModif(1);
				            // on envoie un event pour la mise a jour de la barre de statut
				            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
				          
					}
				};
				b_export = new JButton(act_export);
				b_export.setFont(new java.awt.Font("Dialog", Font.PLAIN, 9)); //$NON-NLS-1$
				
				// Alignements : liste a selection unique
				// JList associee a 1 JScrollPane
				// 1 seul algo ou plugin de quantif/align peut etre selectionne
				align.addListSelectionListener(new ListSelectionListener()
				{
					// fonction appelee lorsqu'un element de la JList align est selectionne
					public void valueChanged(ListSelectionEvent e)
					{
						// si un autre element est selectionne dans une autre JList
						if(!(quantif.isSelectionEmpty()&&config.isSelectionEmpty()))
						{
							// on efface cette selection et on ne garde que la nouvelle
							int sel = align.getSelectedIndex();
							quantif.clearSelection();
							config.clearSelection();
							align.setSelectedIndex(sel);
						}
					}
				});
				align.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				JScrollPane scrollali = new JScrollPane(align);
				scrollali.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				
				// Quantifications : liste a selection unique
				// JList associee a 1 JScrollPane
				// 1 seul algo ou plugin de quantif/align peut etre selectionne
				quantif.addListSelectionListener(new ListSelectionListener()
				{
					// fonction appelee lorsqu'un element de la JList quantif est selectionne
					public void valueChanged(ListSelectionEvent e)
					{
						
						// si un autre element est selectionne dans une autre JList
						if(!(align.isSelectionEmpty()&&config.isSelectionEmpty()))
						{
							// on efface cette selection et on ne garde que la nouvelle
							int sel = quantif.getSelectedIndex();
							align.clearSelection();	
							config.clearSelection();
							quantif.setSelectedIndex(sel);
						}
						
					}
				});
				quantif.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				JScrollPane scrollquantif = new JScrollPane(quantif);
				scrollquantif.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				
				
				// Configuration choisie
				config.addListSelectionListener(new ListSelectionListener()
				{
					// fonction appelee lorsqu'un element de la JList config est selectionne
					public void valueChanged(ListSelectionEvent e)
					{
						
						// si un autre element est selectionne dans une autre JList
						if(!(align.isSelectionEmpty()&&quantif.isSelectionEmpty()))
						{
							// on efface cette selection et on ne garde que la nouvelle
							int sel = config.getSelectedIndex();
							align.clearSelection();	
							quantif.clearSelection();
							config.setSelectedIndex(sel);
						}
						
					}
				});
				config.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				config.setSelectionForeground(Color.WHITE);
				JScrollPane scrollconfig= new JScrollPane(config);
				scrollconfig.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
				
				// Panel options : 3 boutons (snap & tiff & export)
				JPanel options = new JPanel();
				options.setLayout(new GridLayout(3,1));
				options.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.2"))); //$NON-NLS-1$
				options.add(b_snap);
				options.add(b_tiff);
				options.add(b_export);
				
				
				// Intitules des ScrollPane
				scrollali.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.0"))); //$NON-NLS-1$
				scrollquantif.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),Messages.getString("TProcessChooserDialog.1"))); //$NON-NLS-1$
				scrollconfig.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),Messages.getString("TProcessChooserDialog.6"))); //$NON-NLS-1$
				
				// Panel "boutpan" qui contient les boutons d'ajout/suppression
				JPanel boutpan = new JPanel();
				boutpan.setLayout(new FlowLayout());
				boutpan.add(b_add);
				boutpan.add(b_rem);
				
				// Assemblage
				JPanel scrollPan = new JPanel();
				scrollPan.setLayout(new FlowLayout(FlowLayout.CENTER));
				scrollPan.add(scrollali);
				scrollPan.add(scrollquantif);
				this.add(scrollPan);
				this.add(boutpan);
				JPanel confoptPan = new JPanel();
				confoptPan.setLayout(new FlowLayout(FlowLayout.CENTER));
				confoptPan.add(scrollconfig);
				confoptPan.add(options);
				this.add(confoptPan);
				this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),Messages.getString("TBatchComPanel.94"))); //$NON-NLS-1$
			
				// Initialisations
				init();
			
			
		}
		
		
	
		/**** INITIALISATIONS ****/
		public void init()
		{
			
			// Ajuste la taille de la fenetre et l'affiche avec le Look&FeelDeco
			JFrame.setDefaultLookAndFeelDecorated(true);
			this.setVisible(true);
			
			// A l'init on a choisi 0 alignement et 0 quantif
			this.conf_nbalign = 0;
			this.conf_nbquantif = 0;
	
			// tri des plugins
			for (int i=0; i< TPluginManager.plugs.length;i++){
				TPlugin plugin = TPluginManager.plugs[i];
				if (plugin != null) {
					
					// plugin d'alignement ajoute a la liste correspondante
					if ((plugin instanceof SAlignPlugin)
							&& plugin.isBatchPlugin())
					{
						this.modalign.add(plugin.getName());
						this.align.setListData(this.modalign);		//refresh
						
					}
					// plugin de quantification ajoute a la liste correspondante
					else if (plugin instanceof SQuantifPlugin
							&& plugin.isBatchPlugin())
					{
						this.modquantif.add(plugin.getName());
						this.quantif.setListData(this.modquantif);	//refresh
					}
				}
			}
			
			//tri des algos align
			for(int i=0;i<v_align.size();i++)
			{
				this.modalign.add(v_align.get(i));
				this.align.setListData(this.modalign);		//refresh
			}
			//tri des algos quantif
			for(int i=0;i<v_quantif.size();i++)
			{
				this.modquantif.add(v_quantif.get(i));
				this.quantif.setListData(this.modquantif); 	//refresh
			}
		}
		
		/**** INIT DES PARAMETRES ****/
		public void initParameters()
		{
			// Par defaut choisie la configuration est vide
			AGScan.prop.setProperty("TProcessChooserAction.export","0");	 //$NON-NLS-1$ //$NON-NLS-2$
			AGScan.prop.setProperty("TProcessChooserAction.snap","0");		 //$NON-NLS-1$ //$NON-NLS-2$
			AGScan.prop.setProperty("TProcessChooserAction.tiff","0");		 //$NON-NLS-1$ //$NON-NLS-2$
			AGScan.prop.setProperty("TProcessChooserAction.align",""); //$NON-NLS-1$ //$NON-NLS-2$
			AGScan.prop.setProperty("TProcessChooserAction.quantif",""); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		
	}
	public void addFromAlToConfig(String al)
	{
		// on ajoute la quantif. a la liste de config.
		procPan.modconfig.add(al);
		procPan.config.setListData(procPan.modconfig); 		//refresh
		
		// on retire l'align. de la liste des quantif.
		procPan.modalign.removeElement(al);
		procPan.align.setListData(procPan.modalign); 	//refresh
		procPan.etatconfig.add(procPan.ALIGN);
		procPan.conf_nbalign++;
	}
	
	public void addFromQuToConfig(String qu)
	{
		// on ajoute la quantif. a la liste de config.
		procPan.modconfig.add(qu);
		procPan.config.setListData(procPan.modconfig); 		//refresh
		
		// on retire la quantif. de la liste des quantif.
		procPan.modquantif.removeElement(qu);
		procPan.quantif.setListData(procPan.modquantif); 	//refresh
		procPan.etatconfig.add(procPan.QUANTIF);
		procPan.conf_nbquantif++;
	}
	
	public void addSnapToConfig()
	{
		// on ajoute l option a la liste de config.
		procPan.modconfig.add(Messages.getString("TBatchComPanel.103"));	// A MODIFIER !!! //$NON-NLS-1$
		procPan.config.setListData(procPan.modconfig); 		//refresh
		
		// on desactive le bouton de l'option
		procPan.b_snap.setEnabled(false);
		
		procPan.etatconfig.add(procPan.OPTION_SNAP);
	}
	
	public void addTiffToConfig()
	{
		// on ajoute l option a la liste de config.
		procPan.modconfig.add(Messages.getString("TBatchComPanel.106"));	// A MODIFIER !!! //$NON-NLS-1$
		procPan.config.setListData(procPan.modconfig); 		//refresh
		
		// on desactive le bouton de l'option
		procPan.b_tiff.setEnabled(false);
		
		procPan.etatconfig.add(procPan.OPTION_TIFF);
	}
	
	public void addExportToConfig()
	{
		// on ajoute l option a la liste de config.
		procPan.modconfig.add(Messages.getString("TBatchComPanel.104"));	// A MODIFIER !!! //$NON-NLS-1$
		procPan.config.setListData(procPan.modconfig); 		//refresh
		
		// on desactive le bouton de l'option
		procPan.b_export.setEnabled(false);
		
		procPan.etatconfig.add(procPan.OPTION_EXPORT);
	}
	
	public boolean isSnapSelected()
	{
		return procPan.etatconfig.contains(procPan.OPTION_SNAP);
	}
	
	public boolean isTiffSelected()
	{
		return procPan.etatconfig.contains(procPan.OPTION_TIFF);
	}
	
	public boolean configContains(String elem)
	{
		return (procPan.modconfig.contains(elem));
	}
}