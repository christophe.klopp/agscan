package agscan.data.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.data.view.graphic.TRule;
import agscan.data.view.table.TTablePanel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;

public class TViewImpl extends MouseMotionAdapter {
  protected TGraphicPanel graphicPanel;
  protected TTablePanel tablePanel;
  protected JPanel viewPanel;
  protected JSplitPane splitPane;
  protected JScrollPane graphicScrollPane;
  private TDataElement reference;
  private TRule hRule, vRule;
  private JLabel cornerLabel;

  public TViewImpl(TDataElement dataElement) {
    reference = dataElement;
    viewPanel = new JPanel();
    splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
    viewPanel.setLayout(new BorderLayout(0, 0));
    viewPanel.add(splitPane);
    graphicPanel = null;
    tablePanel = null;
    cornerLabel = new JLabel(Messages.getString("TViewImpl.0")); //$NON-NLS-1$
    cornerLabel.setFont(new Font("SansSerif", Font.PLAIN, 10)); //$NON-NLS-1$
    hRule = new TRule(TRule.HORIZONTAL, 25, (int)reference.getModel().getPixelWidth(),
                      reference.getModel().getElementWidth(),
                      reference.getModel().getElementHeight());
    vRule = new TRule(TRule.VERTICAL, 25, (int)reference.getModel().getPixelHeight(),
                      reference.getModel().getElementWidth(),
                      reference.getModel().getElementHeight());
  }
  //remplacée modif integration (en retard...23/09/05)
  public void refresh() {
    if (hRule != null) hRule.init(reference);
    if (vRule != null) vRule.init(reference);
    if (graphicPanel != null) graphicPanel.refresh();
    if (tablePanel != null) tablePanel.refresh();
  }
  public TGraphicPanel getGraphicPanel() {
    return graphicPanel;
  }
  public TTablePanel getTablePanel() {
    return tablePanel;
  }
  public JPanel getViewPanel() {
    return viewPanel;
  }
  public TDataElement getReference() {
    return reference;
  }
  //remplacée modif integration (en retard...23/09/05)
  public void setGraphicPanel() {
    TEvent ev;
    if (graphicPanel != null) {
      graphicScrollPane = new JScrollPane(graphicPanel);

      ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_RULES, null);
      boolean rul = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
      setRules(rul);
      graphicScrollPane.getViewport().addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent event) {
          TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
          TDataElement el = (TDataElement)TEventHandler.handleMessage(ev)[0];
          ev = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, el, null);
          TEventHandler.handleMessage(ev);
        }
      });
      splitPane.setTopComponent(graphicScrollPane);
    }
    else {
      graphicScrollPane = null;
      splitPane.setTopComponent(graphicScrollPane);
    }
  }
  public void setTablePanel() {
    if (tablePanel != null) {
      splitPane.setBottomComponent(tablePanel);
    }
  }
  public JSplitPane getSplitPane() {
    return splitPane;
  }
  public JScrollPane getGraphicScrollPane() {
    return graphicScrollPane;
  }
  public void setRules(boolean b) {
    if (b) {
      graphicScrollPane.setRowHeaderView(vRule);
      graphicScrollPane.setColumnHeaderView(hRule);
      graphicScrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, cornerLabel);
    }
    else {
      graphicScrollPane.setRowHeaderView(null);
      graphicScrollPane.setColumnHeaderView(null);
      graphicScrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, new JLabel());
    }
  }
  public void setZoom(double z) {
    setZoom((z >= 1.0D) ? (int)z : (int)(-1.0D / z));
  }
  public void setZoom(int z) {
    graphicPanel.setZoom(z);
    hRule.setZoom((z > 0) ? z : (-1.0D / z));
    vRule.setZoom((z > 0) ? z : (-1.0D / z));
  }
  public void incZoom() {
    int z = graphicPanel.getZoom();
    z++;
    if (z == -1)
      z = 1;
    else if (z > 20)
      z = 20;
    setZoom(z);
  }
  public void decZoom() {
    int z = graphicPanel.getZoom();
    z--;
    if (z == 0)
      z = -2;
    else if (z < -20)
      z = -20;
    setZoom(z);
  }
  public void centeredZoom(int z) {
    graphicScrollPane.revalidate();
    double oldValue = graphicPanel.getZoom();
    if ((int)oldValue != z) {
      double x = graphicScrollPane.getViewport().getViewPosition().getX() + Math.min(graphicScrollPane.getViewport().getExtentSize().getWidth(), graphicPanel.getPreferredSize().getWidth()) / 2.0D;
      double y = graphicScrollPane.getViewport().getViewPosition().getY() + Math.min(graphicScrollPane.getViewport().getExtentSize().getHeight(), graphicPanel.getPreferredSize().getHeight()) / 2.0D;
      if (oldValue < 0) oldValue = 1.0D / -oldValue;
      setZoom(z);
      graphicScrollPane.revalidate();
      double newValue = graphicPanel.getZoom();
      if (newValue < 0) newValue = 1.0D / -newValue;
      x = x * newValue / oldValue;
      y = y * newValue / oldValue;
      Dimension dim = graphicPanel.getPreferredSize();
      double maxX = dim.getWidth() - graphicScrollPane.getViewport().getExtentSize().getWidth();
      double maxY = dim.getHeight() - graphicScrollPane.getViewport().getExtentSize().getHeight();
      if (maxX < 0) maxX = 0;
      if (maxY < 0) maxY = 0;
      int newX = (int)(x - Math.min(graphicScrollPane.getViewport().getExtentSize().getWidth(), graphicPanel.getPreferredSize().getWidth()) / 2.0D);
      if (newX < 0) newX = 0;
      if (newX > maxX) newX = (int)maxX;
      int newY = (int)(y - Math.min(graphicScrollPane.getViewport().getExtentSize().getHeight(), graphicPanel.getPreferredSize().getHeight()) / 2.0D);
      if (newY < 0) newY = 0;
      if (newY > maxY) newY = (int)maxY;
      while ((graphicScrollPane.getViewport().getViewPosition().getX() != newX) || (graphicScrollPane.getViewport().getViewPosition().getY() != newY))
        graphicScrollPane.getViewport().setViewPosition(new Point(newX, newY));
      graphicScrollPane.revalidate();
    }
  }
  public void mouseMoved(MouseEvent e) {
    hRule.setXMarkPos(e.getX());
    vRule.setYMarkPos(e.getY());
  }
  public void setDividerLocation(double loc) {
    splitPane.setDividerLocation(loc);
  }
  public void setDividerLocation(int loc) {
    splitPane.setDividerLocation(loc);
  }
  public int getDividerLocation() {
    return splitPane.getDividerLocation();
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
