package agscan.data.view;

import agscan.data.element.TDataElement;
import agscan.data.view.graphic.TChartsPanel;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.data.view.table.TBatchTablePanel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchView extends TViewImpl {
  public TBatchView(TDataElement ref) {
    super(ref);
    graphicPanel = null;
    setGraphicPanel();
    tablePanel = new TBatchTablePanel(this);
    setTablePanel();
  }
  public void setGraphicPanel(TGraphicPanel gp) {
    graphicPanel = gp;
    int dl = splitPane.getDividerLocation();
    setGraphicPanel();
    splitPane.setDividerLocation(dl);
  }
  public void setGraphicPanel(TChartsPanel cp) {
    graphicPanel = cp;
    int dl = splitPane.getDividerLocation();
    setGraphicPanel();
    splitPane.setDividerLocation(dl);
  }
}
