package agscan.data.element.image;

import java.io.BufferedWriter;
import java.util.Enumeration;
import java.util.Vector;

import agscan.data.controler.TImageControler;
import agscan.data.controler.memento.TDataElementMemento;
import agscan.data.controler.memento.TImageAction;
import agscan.data.controler.memento.TImageState;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.element.TDataElement;
import agscan.data.model.TImageModel;
import agscan.event.TEvent;
import agscan.data.view.TImageView;

public class TImage extends TDataElement {
	protected int[] histogramData;
	protected int histogramMax, histogramMinUsed, histogramMaxUsed;
	
	public TImage() {
		super();
		controler = null;
		model = null;
		view = null;
		histogramData = null;
		//histogramMax = valeur de l'histogramme la + utilisée
		//histogramMinUsed = 1ere valeur de l'histogramme utilisée ( = differente de 0)
		//histogramMaxUsed = derniere valeur de l'histogramme utilisée ( = differente de 0)
		histogramMax = histogramMinUsed = histogramMaxUsed = 0;
	}
	public TImage(String filename, TImageModel model) {
		//System.err.println("filename dans TImage= "+filename);
		String pathSeparator = System.getProperty("file.separator");
		int ix = filename.lastIndexOf(pathSeparator);
		if (ix > -1)
			path = filename.substring(0, ix + 1);
		else
			path = pathSeparator;
		name = filename.substring(ix + 1);
		this.model = model;
		view = new TImageView(this);
		controler = new TImageControler(this);
		System.err.print(" Making histogram ...");
		//  ParameterBlock pb = new ParameterBlock();
		// pb.addSource(model.getInitialImage());
		//pb.add(null).add(1).add(1);
		//RenderedOp op = JAI.create("Extrema", pb);
		//double[][] extrema = (double[][]) op.getProperty("Extrema");
		//histogramMinUsed = (int) extrema[0][0];
		//histogramMaxUsed = (int) extrema[1][0];
		//Histogram histogram = new Histogram(256, 0, 256, 1);
		//histogram.countPixels(model.getInitialImage().getData(), null, 0, 0, 1, 1);
		//histogramData = histogram.getBins(0);
		// for (int i = 1; i < (histogramData.length - 1); i++) if (histogramMax < histogramData[i]) histogramMax = histogramData[i];
		
		
		//histogramData = model.getInitialImage().getProcessor().getHistogram();
		histogramData = ((TImageView)view).getDisplayImage().getProcessor().getHistogram();
		histogramMaxUsed = (int)model.getInitialImage().getProcessor().getHistogramMax();
		histogramMinUsed = (int)model.getInitialImage().getProcessor().getHistogramMin();
		
		//test
		histogramMax = histogramData[0];
		histogramMinUsed = 0;
		//	System.out.println("0 ==> "+histogramMax);
		for (int i=1;i<histogramData.length;i++){
			//System.out.println("hist_"+i+" = "+histogramData[i]);
			
			if (histogramData[i]>=histogramMax){
				histogramMax = histogramData[i];
				//	System.out.println(i+" ==> "+histogramData[i]);
			}
			
			if (histogramData[i]>0){
				histogramMaxUsed = i;// on recherche l'indice de la derniere valeur non nulle du tableau
			}
		}
		
		histogramMinUsed = 0;
		do {
			histogramMinUsed++;

		} 
		while (histogramData[histogramMinUsed]<1);
		
		
		
		
	//	System.out.println("histogramMax=  "+histogramMax);
	//	System.out.println("histogramMaxUsed= "+histogramMaxUsed);
	//	System.out.println("histogramMinUsed= "+histogramMinUsed);
		//fin test
		
		System.err.println("  done.");
	}
	
	public TImageModel getImageModel() {
		return (TImageModel)model;
	}
	public TImageView getImageView() {
		return (TImageView)view;
	}
	public TImageControler getImageControler() {
		return (TImageControler)controler;
	}
	/*before 2005/11/03
	 public void saveFileInfo(BufferedWriter bw, int indent, boolean saveImage) {
	 String base = "";
	 for (int i = 0; i < indent; i++) base += " ";
	 try {
	 // path of the file is ./ if image is stored into the zaf file, else the current path of the image is given
	  if (!saveImage)
	  bw.write(base + "<file name=\"" + getName() + "\" localization=\"" + getPath() + "\"/>");
	  else
	  bw.write(base + "<file name=\"" + getName() + "\" localization=\".\"/>");
	  bw.newLine();
	  bw.write(base + "<modifs>");
	  bw.newLine();
	  Vector v = getControler().getMementos().getUsedMementos();
	  TUndoElement tue;
	  TImageState tis = null;
	  for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
	  tue = ((TDataElementMemento)enume.nextElement()).getUndoElement();
	  if (tue instanceof TImageAction) {
	  bw.write(base + "  <modif type=\"" + ((TImageAction)tue).toString() + "\" value=\"" + ((TImageAction)tue).getValue() + "\"/>");
	  bw.newLine();
	  }
	  else if (tue instanceof TImageState)
	  tis = (TImageState)tue;
	  }
	  if (tis != null)
	  if (!tis.getValue().equals("0 255 Linear 0.0")) {
	  bw.write(base + "  <modif type=\"" + tis.toString() + "\" value=\"" + tis.getValue() + "\"/>");
	  bw.newLine();
	  }
	  bw.write(base + "</modifs>");
	  bw.newLine();
	  }
	  catch (Exception ex) {
	  ex.printStackTrace();
	  }
	  }
	  */
	
	//apres: modified the 2005/11/03 for multi-channels image
	public void saveFileInfo(BufferedWriter bw, int indent, boolean saveImage) {
		TImageModel imageModel =getImageModel(); 
		String base = "";
		for (int i = 0; i < indent; i++) base += " ";
		try {
			//add channels tag
			//added 2005/11/03
			//bw.write(base + "<channels number=\"" + imageModel.getNumberOfChannels() + "\"/>");
			//bw.newLine();
			for (int i = 1; i <= imageModel.getNumberOfChannels(); i++){
				
				System.out.println("imageModel.getChannelImage(i).getTitle()="+imageModel.getChannelImage(i).getTitle());
				System.out.println(" localization="+getPath());
				// path of the file is ./ if image is stored into the zaf file, else the current path of the image is given
				if (!saveImage)
					bw.write(base + "<file name=\"" + imageModel.getChannelImage(i).getTitle() + "\" localization=\"" + getPath() + "\"/>");
				else
					bw.write(base + "<file name=\"" + imageModel.getChannelImage(i).getTitle() + "\" localization=\".\"/>");
				bw.newLine();
				bw.write(base + "<modifs>");
				bw.newLine();
				Vector v = getControler().getMementos().getUsedMementos();
				TUndoElement tue;
				TImageState tis = null;
				for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
					tue = ((TDataElementMemento)enume.nextElement()).getUndoElement();
					if (tue instanceof TImageAction) {
						bw.write(base + "  <modif type=\"" + ((TImageAction)tue).toString() + "\" value=\"" + ((TImageAction)tue).getValue() + "\"/>");
						bw.newLine();
					}
					else if (tue instanceof TImageState)
						tis = (TImageState)tue;
				}
				//TODO virer peut etre tout ce if inutile maintenant non? 
				if (tis != null)
					//2005/11/18 ici modif avant:
					//if (!tis.getValue().equals("0 255 Linear 0.0")) {	
					if (!tis.getValue().equals("0 255 ")) {//maintenant car histogramme vir�
						bw.write(base + "  <modif type=\"" + tis.toString() + "\" value=\"" + tis.getValue() + "\"/>");
						bw.newLine();
					}
				bw.write(base + "</modifs>");
				bw.newLine();
			}
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public Object[] BZMessageREsponse(TEvent event) {
		return null;
	}
	public TUndoElement createStateSnapshot() {
		 //2005/11/16 transfer curves removed
		/*TImageState is = new TImageState(getImageView().getMin(), getImageView().getMax(),
				getImageView().getImageGraphicPanel().getTransferCurve(),
				getImageView().getImageGraphicPanel().getTransferCurve().getParam());*/
		TImageState is = new TImageState(getImageView().getMin(), getImageView().getMax());
		
		return is;
	}
	public void initFromStateSnapshot(TImageState state) {
	}
	public int[] getHistogramData() {
		return((TImageModel)model).getInitialImage().getProcessor().getHistogram();// test 13/07/05
		//return histogramData;
	}
	public int getHistogramMax() {
		return histogramMax;
	}
	public int getHistogramMinUsed() {
		return histogramMinUsed;
	}
	public int getHistogramMaxUsed() {
		return histogramMaxUsed;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
