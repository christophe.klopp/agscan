package agscan.data.element;

import agscan.data.controler.TControlerImpl;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.model.TModelImpl;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.data.view.TViewImpl;

public abstract class TDataElement implements TBZMessageListener {
  protected String name, path;
  protected TControlerImpl controler;
  protected TModelImpl model;
  protected TViewImpl view;

  public TDataElement() {
    name = path = "";
    controler = null;
    model = null;
    view = null;
  }
  public abstract TUndoElement createStateSnapshot();
  public Object[] BZMessageResponse(TEvent event) {
    return null;
  }
  public String getName() {
    return name;
  }
  public void setName(String n) {
    name = n;
  }
  public String getPath() {
    return path;
  }
  public void setPath(String p) {
    path = p;
  }
  public TControlerImpl getControler() {
    return controler;
  }
  public TModelImpl getModel() {
    return model;
  }
  public TViewImpl getView() {
    return view;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
