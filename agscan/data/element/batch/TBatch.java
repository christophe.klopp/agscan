package agscan.data.element.batch;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import agscan.AGScan;
import agscan.Messages;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.controler.worker.TBatchWorker;
import agscan.data.controler.worker.TBatchWorkerNImages;
import agscan.data.element.TDataElement;
import agscan.data.model.TModelImpl;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.view.graphic.TChartsPanel;
import agscan.data.view.graphic.TPlotsPanel;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TBatchView;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 *
 * @version 2.0
 */
public class TBatch  extends TDataElement {
  public static final int RUNNING = 1;
  public static final int WAITING = 2;
  public static final int JOB_DISPLAYING = 3;

  private boolean batchRunning, showingImage;
  private TBatchWorkerNImages worker;
  //private TBatchWorker worker;
  private TDataElement showingElement;
  //modified 2005/12/01
  private String outputDirectory =AGScan.prop.getProperty("batchPath"); //"./" before

  public TBatch() {
    super();    
    model = new TBatchModelNImages(this);
    controler = new TBatchControler(this);
    view = new TBatchView(this);
    //Where we open a new batch, 3 modifs are added to the model
    //it's why we clear modif count here in order to quit without the application 
    // ask you to save the batch.
    model.clearModifs();//added 2005/11/30
    name = "newBatch" /*+ new Date().getTime()*/ + ".bzb";
    batchRunning = showingImage = false;
  }
  public TBatch(TGrid grid, Vector[] images) {
    super();
    model = new TBatchModelNImages(this, grid, images);
    controler = new TBatchControler(this);
    view = new TBatchView(this);
    name = "newBatch" /*+ new Date().getTime()*/ + ".bzb";
    batchRunning = showingImage = false;
  }
  public TBatch(TGrid grid, Vector[] images, Enumeration enuStatus) {
    super();
    model = new TBatchModelNImages(this, grid, images, enuStatus);
    controler = new TBatchControler(this);
    view = new TBatchView(this);
    name = "newBatch" /*+ new Date().getTime()*/ + ".bzb";
    batchRunning = showingImage = false;
  }
  public void setOutputPath(String op) {
    outputDirectory = op;
  }
  public TUndoElement createStateSnapshot() {
    return null;
  }
  
  public String getMessage() {
  	  return Messages.getString("TBatch.0");
  }
  public boolean isBatchRunning() {
    return batchRunning;
  }
  public boolean isShowingImage() {
    return showingImage;
  }
  public void setBatchRunning(boolean br) {
    batchRunning = br;
  }
  public void setShowingImage(boolean si) {
    showingImage = si;
  }
  public void setShowingElement(TDataElement element) {
    showingElement = element;
    if (element != null)
      ((TBatchView)view).setGraphicPanel(element.getView().getGraphicPanel());
    else
      ((TBatchView)view).setGraphicPanel(null);
  }
  public void setShowingElement(TChartsPanel panel) {
    ((TBatchView)view).setGraphicPanel(panel);
  }
  public void setShowingElement(TPlotsPanel panel) {
    ((TBatchView)view).setGraphicPanel(panel);
  }
  public TDataElement getShowingElement() {
    return showingElement;
  }
  public TBatchWorkerNImages getWorker() {
    return worker;
  }
  public void setWorker(TBatchWorkerNImages bw) {
    worker = bw;
  }
  /*public void setWorker(TBatchWorker bw) {
	worker = bw;
  }*/
  public String getOutputDirectory() {
    return outputDirectory;
  }
  public void setOutputDirectory(String od) {
    outputDirectory = od;
  }
  public void setModel(TModelImpl mod)
  {
	model = mod;
  }
  public boolean showCurrentAlignment() {
    return ((TBatchControler)controler).showCurrentAlignment();
  }
  public String getOutputPath() {
    return outputDirectory;
  }
}
