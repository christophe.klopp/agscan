/**
 * <p>Titre : BZScan</p> <p>Description : DNA chips scan analysis</p>
 * <p>Copyright : Copyright (c) 2003</p> <p>Soci�t� : TAGC</p>
 * @author Fabrice Lopez
 * @version 2.0
 */
package agscan.data.element.alignment;

import agscan.algo.TAlgorithmListener;
import agscan.data.controler.TAlignmentControler;
import agscan.data.controler.TGridControler;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.element.TGriddedElement;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TGridModel;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.data.view.TAlignmentView;

import ij.ImagePlus;

public class TAlignment extends TGriddedElement {
  private TImage image;
  private boolean algoRunning;
  private TAlgorithmListener algo;
  private TBatch batch;



  public TAlignment(TAlignmentModel alignmentModel, TImage image, TGrid grid) {
    super();
    this.image = image;
    model = alignmentModel;
    model.setReference(this);
    view = new TAlignmentView(this, image.getImageView(), grid.getGridModel());
    controler = new TAlignmentControler(this, image.getImageControler(), grid.getGridControler());
    controler.setReference(this);
    getModel().addModif(1);
    algoRunning = false;
    algo = null;
    batch = null;


  }
  public void setImage(TImage im) {
    image = im;
    model = new TAlignmentModel(image.getImageModel(), getGridModel(), ((TAlignmentModel)model).getSaveImage());
    model.setReference(this);
    view = new TAlignmentView(this, image.getImageView(), getGridModel());
    controler = new TAlignmentControler(this, image.getImageControler(), (TGridControler)getGridModel().getReference().getControler());
    controler.setReference(this);
    getModel().addModif(1);
  }
  public void setBatch(TBatch b) {
    batch = b;
  }
  public TAlignment(TAlignmentModel alignmentModel, TImage image, TGrid grid, TBatch b) {
    this(alignmentModel, image, grid);
    batch = b;
  }
  public boolean isInBatch() {
    return batch != null;
  }
  public TBatch getBatch() {
    return batch;
  }
  public TUndoElement createStateSnapshot() {
    return null;
  }
  public TGridModel getGridModel() {
    return ((TAlignmentModel)model).getGridModel();
  }
  public TImage getImage() {
    return image;
  }
  public boolean isAlgoRunning() {
    return algoRunning;
  }
  public void setAlgoRunning(boolean ar) {
    algoRunning = ar;
  }
  public TAlgorithmListener getAlgorithm() {
    return algo;
  }
  public void setAlgorithm(TAlgorithmListener a) {
    algo = a;
  }

  public float[][] calcCircAGScanKernel(int kdiam, int kspot) {
      int kwidth, kheight;
      kwidth = kheight = kspot; 
      int krad = kdiam/2;
      float[][] newkernel = new float[kwidth][kheight];
      float norm = 0f;
       for (int j=-krad; j<=krad; j++) 
         for (int i=-krad; i<=krad; i++) {
            newkernel[i+kwidth/2][j+kheight/2] = 
               plugins.ImageTools.computePixelArcArea((float)i -0.5f, (float)i +0.5f,
                    (float)j -0.5f, (float)j + 0.5f, (float)krad );
            norm += newkernel[i+kwidth/2][j+kheight/2];
        }
       for (int j=-krad; j<=krad; j++) 
         for (int i=-krad; i<=krad; i++) 
            newkernel[i+kwidth/2][j+kheight/2] /= norm;
        return newkernel;
  } // end calCircKernel
  
 public float[][] calcConicAGScanKernel(int kdiam, int kspot){
      int kwidth, kheight;
      kwidth = kheight = kspot;
      int krad = kdiam/2;
      float[][] newkernel = new float[kwidth][kheight];
	  int xc = kwidth/2;
	  int yc = kheight/2;
	  int pas = 256/krad;
	  for(int i = 0; i < kwidth ; i++){
		  for(int j = 0 ; j < kheight ; j++){
			  
			  //calcul de la distance au centre
			  int norme = (int)Math.rint(Math.sqrt((i - xc)*(i - xc) + (j - yc)*(j - yc)));
			  if(norme > krad){
				  newkernel[i][j] = 0.0f;
				  
			  }
			  else{
				  int compteur = 0;
				  boolean find = false;
				  while(!find){
					  if((norme >= compteur)&&(norme < compteur+1)){
						  newkernel[i][j] = (float)((krad - compteur)*pas);
					  	  find =true;
					  }
					  else
						  compteur++;  
				  }
			  }
			  
		  }
		  
	  }
//	  String s = "";
//	  for(int i = 0; i < kwidth ; i++){
//		  for(int j = 0 ; j < kheight ; j++){
//			  s = s + newkernel[i][j] + "  ";
//			  
//		  }
//		  s =s +'\n';
//		  
//	  }
//      System.out.println(s);
      
      return newkernel;
  }
  
  public float[][] calcGaussAGScanKernel(int kdiam, int kspot){
      int kwidth, kheight;
      kwidth = kheight = kspot;
      int krad = kdiam/2;
      float[][] newkernel = new float[kwidth][kheight];
	  int xc = kwidth/2;
	  int yc = kheight/2;
	  
	  for(int i = 0; i < kwidth ; i++){
		  for(int j = 0 ; j < kheight ; j++){
			  //calcul de la distance au centre
			  double norme = Math.sqrt((i - xc)*(i - xc) + (j - yc)*(j - yc));
			  if(norme > (double)krad){
				  newkernel[i][j] = 0.0f;
				  
			  }
			  else{
				  
				  newkernel[i][j] = 255.0f * (float)(Math.exp(-norme*norme/((double)krad*1.5D)/*/(Math.sqrt(2.0D * Math.PI))*/)); 
				  
			  }
		  
		  }
	  }
//	  String s = "";
//	  for(int i = 0; i < kwidth ; i++){
//		  for(int j = 0 ; j < kheight ; j++){
//			  s = s + newkernel[i][j] + "  ";
//			  
//		  }
//		  s =s +'\n';
//		  
//	  }
//      System.out.println(s);
	  
	  return newkernel;
  }
  

}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
