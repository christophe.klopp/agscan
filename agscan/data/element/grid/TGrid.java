/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.element.grid;

import java.io.BufferedWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import agscan.data.controler.TGridControler;
import agscan.data.controler.memento.TGridState;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.menu.columnmenubuilder.TColumnMenuBuilder;
import agscan.data.view.TGridView;

public class TGrid extends TGriddedElement {
  private TColumnMenuBuilder.TButtonGroup sortButtonGroup, colorizeButtonGroup;
  public TGrid(TGridConfig gc) {
    super();
    model = new TGridModel(gc);
    model.setReference(this);
    view = new TGridView(this);
    controler = new TGridControler(this);
    name = "newGrid" /*+ new Date().getTime()*/ + ".grd";
    getModel().addModif(1);
    sortButtonGroup = new TColumnMenuBuilder.TButtonGroup();
    colorizeButtonGroup = new TColumnMenuBuilder.TButtonGroup();
  }
  public TUndoElement createStateSnapshot() {
    TGridConfig conf = getGridModel().getConfig();
    TGridState gs = new TGridState(
            conf.getNbLevels(), conf.getLev1NbCols(), conf.getSpotsWidth(), conf.getLev1NbRows(), conf.getSpotsHeight(),
            conf.getSpotsDiam(), conf.getLev1StartElement(), conf.getLev1ElementID(), conf.getLev2NbCols(),
            conf.getLev2XSpacing(), conf.getLev2NbRows(), conf.getLev2YSpacing(), conf.getLev2StartElement(),
            conf.getLev2ElementID());
    //, conf.getLev3NbCols(), conf.getLev3XSpacing(), conf.getLev3NbRows(), conf.getLev3YSpacing(),// level3 removed - 20005/10/28
    //        conf.getLev3StartElement(), conf.getLev3ElementID());// level3 removed - 20005/10/28
    return gs;
  }
  public TGridModel getGridModel() {
    return (TGridModel)model;
  }
  public TGridControler getGridControler() {
    return (TGridControler)controler;
  }
  public TColumnMenuBuilder.TButtonGroup getSortButtonGroup() {
    return sortButtonGroup;
  }
  public TColumnMenuBuilder.TButtonGroup getColorizeButtonGroup() {
    return colorizeButtonGroup;
  }
  public TGridView getGridView() {
    return (TGridView)view;
  }
  public void saveFileInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    try {
      bw.write(base + "<file name=\"" + getName() + "\" localization=\"" + getPath() + "\"/>");
      bw.newLine();
      bw.write(base + "<model>");
      bw.newLine();
      getGridModel().saveGrid(bw, 6);
      bw.write(base + "</model>");
      bw.newLine();
      bw.write(base + "<defects>");
      bw.newLine();
      TGridBlock gb = getGridModel().getRootBlock();
      if ((gb.getX() != 0) || (gb.getY() != 0) || (gb.getSpotWidth() != gb.getInitialSpotWidth()) || (gb.getSpotHeight() != gb.getInitialSpotHeight()) || (gb.getAngle() != 0)) {
        bw.write(base + "  <block level=\"3\" x=\"" + gb.getLocalX() + "\" y=\"" + gb.getLocalY() + "\" sw=\"" +
                 gb.getSpotWidth() + "\" sh=\"" + gb.getSpotHeight() + "\" da=\"" + gb.getAngle() + "\"/>");
        bw.newLine();
      }
      Vector v = getGridModel().getLevel2Elements();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        gb = (TGridBlock)enume.nextElement();
        if ((gb.getLocalX() != gb.getInitX()) || (gb.getLocalY() != gb.getInitY()) || (gb.getSpotWidth() != gb.getInitialSpotWidth()) || (gb.getSpotHeight() != gb.getInitialSpotHeight())
            || (gb.getAngle() != 0)) {
          bw.write(base + "  <block r=\"" + gb.getRow() + "\" c=\"" + gb.getCol() + "\" level=\"2\" x=\"" +
                   gb.getLocalX() + "\" y=\"" + gb.getLocalY() + "\" sw=\"" +
                   gb.getSpotWidth() + "\" sh=\"" + gb.getSpotHeight() + "\" da=\"" + gb.getAngle() + "\"/>");
          bw.newLine();
        }
      }
      v = getGridModel().getLevel1Elements();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        gb = (TGridBlock)enume.nextElement();
        if ((gb.getLocalX() != gb.getInitX()) || (gb.getLocalY() != gb.getInitY()) || (gb.getSpotWidth() != gb.getInitialSpotWidth()) || (gb.getSpotHeight() != gb.getInitialSpotHeight())
            || (gb.getAngle() != 0)) {
          bw.write(base + "  <block r=\"" + gb.getRow() + "\" c=\"" + gb.getCol() + "\" level=\"1\" x=\"" +
                   gb.getLocalX() + "\" y=\"" + gb.getLocalY() + "\" sw=\"" +
                   gb.getSpotWidth() + "\" sh=\"" + gb.getSpotHeight() + "\" da=\"" + gb.getAngle() + "\"/>");
          bw.newLine();
        }
      }
      v = getGridModel().getSpots();
      TSpot spot;
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        spot = (TSpot)enume.nextElement();
        if ((spot.getLocalX() != spot.getInitX()) || (spot.getLocalY() != spot.getInitY()) ||
            (spot.getUserDiameter() != spot.getInitDiameter())) {
          bw.write(base + "  <spot r=\"" + spot.getRow() + "\" c=\"" + spot.getCol() + "\" x=\"" +
                   spot.getLocalX() + "\" y=\"" + spot.getLocalY() + "\" d=\"" + spot.getUserDiameter() + "\"/>");
          bw.newLine();
        }
      }
      bw.write(base + "</defects>");
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public void saveFeaturesInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    try {
      Vector v = getGridModel().getSpots();
      TSpot spot;
      int nc;
      TColumn col;
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        spot = (TSpot)enume.nextElement();
        /*
      //  bw.write(base + "<feature>");//remi modif 2005/11/08
        bw.write(base + "<feature ");//on deplace le > test + ajout d'un espace apres 
      //  bw.newLine();//remi modif 2005/11/08
        //bw.write(base + "  name=\"" + spot.getName());//on retire les espaces avant
        bw.write(base + "name=\"" + spot.getName());
        //  bw.newLine();//remi modif 2005/11/08
        //bw.write(base + "  index=\"" + spot.getIndex() + "\">");//ajout du > test
        bw.write(base + "index=\"" + spot.getIndex() + "\">");//+ on retire les espaces avant
        */
        //essai sur une seule ligne
        bw.write(base + "<feature name=\"" + spot.getName() + "\" index=\"" + spot.getIndex() + "\">");//modif OK 2005/11/08
        
        bw.newLine();
        bw.write(base + "  <columns>");
        bw.newLine();
        nc = getGridModel().getConfig().getColumns().getColumnCount();
        for (int i = 1; i < nc; i++) {
          col = (TColumn)getGridModel().getConfig().getColumns().getColumn(i);
          if ((col.getSpotKey() != TSpot.PARAMETER_X) && (col.getSpotKey() != TSpot.PARAMETER_Y) &&
              (col.getSpotKey() != TSpot.PARAMETER_C) && (col.getSpotKey() != TSpot.PARAMETER_R) &&
			  //added the 3 new columns in this condition - 2005/11/18
			  // in order to not add 2 times columns  when open
              (col.getSpotKey() != TSpot.PARAMETER_BLOCK) && (col.getSpotKey() != TSpot.PARAMETER_COLUMN) && (col.getSpotKey() != TSpot.PARAMETER_ROW) &&
              (col.getSpotKey() != TSpot.PARAMETER_NAME)) {          	
            bw.write(base + "    <column name=\"" + col.toString() +
                            "\" value=\"" + spot.getParameter(col.getSpotKey()) +
                            "\" synchronized=\"" + spot.isSynchro(col.getSpotKey()) + "\"/>");
            bw.newLine();
          }
        }
        bw.write(base + "  </columns>");
        bw.newLine();
        bw.write(base + "</feature>");
        bw.newLine();
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
