/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.util.Vector;

import agscan.algo.fit.TFitAlgorithm;

public abstract class TGridElement {
  public static final Color selectedColor = Color.orange;
  protected int col, row;
  protected TGridElement parent;
  protected double x, y, angle, initX, initY;
  public abstract void draw(Graphics2D g2d, boolean drag, boolean scrolled, Rectangle rect, double zoom);
  public abstract double getWidth();
  public abstract double getInitialWidth();
  public abstract double getHeight();
  public abstract double getInitialHeight();
  public abstract double getSpotWidth();
  public abstract double getSpotHeight();
  protected Shape shape;
  protected Color color;
  protected int index;

  /**
   * Returns the spot left corner X coordinate within the image
   * Expessed in micrometers
   */
  public double getX() {
    if (parent != null)
      return parent.getX() + x * Math.cos(parent.getAngleTotal()) - y * Math.sin(parent.getAngleTotal());
    return x;
  }
  public double getXNoRotation() {
    if (parent != null)
      return parent.getX() + x;
    return x;
  }
  public double getYNoRotation() {
    if (parent != null)
      return parent.getY() + y;
    return y;
  }

  /**
   * Returns the spot left corner X coordinate within the grid (parent) calculated after the 
   * grid and sub grid has been placed and before the spot has been moved.
   * Expessed in micrometers
   */
  public double getInitX() {
    /*if (parent != null)
      return parent.getInitX() + initX;*/
    return initX;
  }
  
  /**
   * Returns the spot left corner Y coordinate within the image
   * Expessed in micrometers
   */
  public double getY() {
    if (parent != null)
      return parent.getY() + x * Math.sin(parent.getAngleTotal()) + y * Math.cos(parent.getAngleTotal());
    return y;
  }
  
  /**
   * Returns the spot left corner Y coordinate within the grid (parent) calculated after the 
   * grid and sub grid has been placed and before the spot has been moved.
   * Expessed in micrometers.
   */
  public double getInitY() {
    /*if (parent != null)
      return parent.getInitY() + initY;*/
    return initY;
  }
  public Shape getShape() {
    return shape;
  }
  public void setShape(Shape s) {
    shape = s;
  }
  public double getAngleTotal() {
    if (parent != null) return parent.getAngleTotal() + angle;
    return angle;
  }
  public double getAngle() { return angle; }
  public TGridElement getParent() { return parent; }
  public abstract TSpot getTopLeftSpot();
  //return absolute column
  public int getCol() {
    if (parent != null)
      return col + parent.getCol();
    return col;
  }
  //return absolute row
  public int getRow() {
   if (parent != null)
     return row + parent.getRow();    	
    return row;
  }
  
  public void setCol(int c) { col = c; }
  public void setRow(int r) { row = r; }
  public void addPosition(double DX, double DY) {
    if (parent != null) {
      float[] f = new float[2];
      GeneralPath s = new GeneralPath();
      s.moveTo(0, 0);
      s.lineTo((float)DX, (float)DY);
      s.transform(AffineTransform.getRotateInstance(-(double)parent.getAngleTotal(), 0, 0));
      PathIterator it = s.getPathIterator(AffineTransform.getScaleInstance(1.0, 1.0));
      it.next();
      it.currentSegment(f);
      x += f[0];
      y += f[1];
    }
    else {
      x += DX;
      y += DY;
    }
    updateSpotsPosition();
  }
  public void setPosition(double x, double y) {
    this.x = x;
    this.y = y;
    updateSpotsPosition();
  }
  public abstract void updateSpotsPosition();
  public void addAngle(double a) {
    angle += a;
  }
  public void setAngle(double a) {
    angle = a;
    updateSpotsPosition();
  }
  public abstract void resizeByRap(double xRap, double yRap, boolean xModif);
  protected abstract void resize(double sw, double sh);
  public abstract int getNbRowsInSpots();
  public abstract int getNbColumnsInSpots();
  public abstract Rectangle getBounds(double zoom, int xRes, int yRes);
  public abstract TSpot getSpot(double x, double y);
  public abstract TSpot getSpot(int r, int c);
  public abstract Vector getSpots();
  public TGridElement getSpot(int i) { return this; }
  public abstract Vector getSpots(Rectangle rect);
  public abstract void addParameter(String sk, Object dv, boolean syn);
  public abstract void setSynchro(String sk, boolean b);
  public abstract void reset();
  public int getIndex() {
    if (parent != null)
      return index + parent.getIndex();
    return index;
  }
  public abstract void removeParameter(String sk);
  
  /**
   * Returns the spot left corner X coordinate within the grid (parent) 
   * Expessed in micrometers.
   */
  public double getLocalX() {
    return x;
  }
  
  /**
   * Returns the spot left corner Y coordinate within the grid (parent) 
   * Expessed in micrometers.
   */
  public double getLocalY() {
    return y;
  }
  public int getLocalRow() {
    return row;
  }
  public int getLocalColumn() {
    return col;
  }
  public abstract TFitAlgorithm getFitAlgorithm();
  public abstract void setFitAlgorithm(TFitAlgorithm algo);
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
