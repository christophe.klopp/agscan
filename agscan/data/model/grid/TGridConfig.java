/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import java.awt.Color;
import java.io.BufferedWriter;
import java.util.Enumeration;

import javax.swing.table.DefaultTableColumnModel;

import agscan.Messages;
import agscan.data.model.grid.table.TColumn;
import agscan.data.model.grid.table.TDoubleColumn;
import agscan.data.model.grid.table.TIndexColumn;
import agscan.data.model.grid.table.TIntegerColumn;
import agscan.data.model.grid.table.TStringColumn;

public class TGridConfig {
  public static final int TOP_LEFT = 1;
  public static final int TOP_RIGHT = 2;
  public static final int BOTTOM_LEFT = 3;
  public static final int BOTTOM_RIGHT = 4;
  public static final int ID_ROW_COLUMN = 1;
  public static final int ID_NUMBER = 2;

  public static final String COLUMN_NAME = Messages.getString("TGridConfig.0"); //$NON-NLS-1$
 // public static final String COLUMN_X = Messages.getString("TGridConfig.1"); //$NON-NLS-1$
 // public static final String COLUMN_X = "X (\u00B5m)";// "X (µm)"; codage unicode du µ=\u00B5m
  public static final String COLUMN_X = "X";// codage unicode du µ=\u00B5m
  //public static final String COLUMN_Y = Messages.getString("TGridConfig.2"); //$NON-NLS-1$
  //public static final String COLUMN_Y = "Y (\u00B5m)";// "Y (µm)";
  public static final String COLUMN_Y = "Y";
  //TODO attention ci dessous les noms des colonnes ne sont pas ceux affichés!
  //TODO a dé-internationaliser car en ANGLAIS toujours!
  public static final String COLUMN_ROW = Messages.getString("TGridConfig.3"); //$NON-NLS-1$
  public static final String COLUMN_COL = Messages.getString("TGridConfig.4"); //$NON-NLS-1$
  public static final String COLUMN_DIAM = Messages.getString("TGridConfig.5"); //$NON-NLS-1$
  public static final String COLUMN_QUANTIF_IC = Messages.getString("TGridConfig.6"); //$NON-NLS-1$
  public static final String COLUMN_QUANTIF_IV = Messages.getString("TGridConfig.7"); //$NON-NLS-1$
  public static final String COLUMN_QUANTIF_FC = Messages.getString("TGridConfig.8"); //$NON-NLS-1$
  public static final String COLUMN_QUANTIF_FV = Messages.getString("TGridConfig.9"); //$NON-NLS-1$
  public static final String COLUMN_COMPUTED_DIAM = Messages.getString("TGridConfig.10"); //$NON-NLS-1$
  public static final String COLUMN_FIT_CORRECTION = Messages.getString("TGridConfig.11"); //$NON-NLS-1$
  public static final String COLUMN_OVERSHINING_CORRECTION = Messages.getString("TGridConfig.12"); //$NON-NLS-1$
  public static final String COLUMN_QM = Messages.getString("TGridConfig.13"); //$NON-NLS-1$
//3 columns added 2005/11/18
  public static final String COLUMN_BLOCK ="Block";
  public static final String COLUMN_METAROW ="Row";
  public static final String COLUMN_METACOL ="Column";
  
  private int nbLevels;
  private int L1NbRows, L1NbCols, L1StartElement, L1ElementID;
  private int L2NbRows, L2NbCols, L2StartElement, L2ElementID;
//  private int L3NbRows, L3NbCols, L3StartElement, L3ElementID;// level3 removed - 20005/10/28
  private double spotWidth, spotHeight, spotDiameter, L2XSpacing, L2YSpacing;//, L3XSpacing, L3YSpacing;//// level3 removed - 20005/10/28
  private DefaultTableColumnModel columns;
  private int colorizedColumn;
  private static String[] startElement = { Messages.getString("TGridConfig.14"), Messages.getString("TGridConfig.15"), Messages.getString("TGridConfig.16"), Messages.getString("TGridConfig.17") }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
  private TSpot exemptedSpotFromColouring;
  
  private double deca_X;//shift from template matching global alignment
  private double deca_Y;

  public TGridConfig() {
    nbLevels = 1;
    L1NbRows = 12;
    L1NbCols = 12;
    spotWidth = 375.0D;
    spotHeight = 375.0D;
    spotDiameter = 250.0D;
    L1StartElement = TOP_LEFT;
    L1ElementID = ID_ROW_COLUMN;
    L2NbRows = 1;
    L2NbCols = 1;
    L2XSpacing = 0.0D;
    L2YSpacing = 0.0D;
    L2StartElement = TOP_LEFT;
    L2ElementID = ID_ROW_COLUMN;
    deca_X = 0.0D;
    deca_Y = 0.0D;
  /*// level3 removed - 20005/10/28
    L3NbRows = 1;
    L3NbCols = 1;
    L3XSpacing = 0.0D;
    L3YSpacing = 0.0D;
    L3StartElement = TOP_LEFT;
    L3ElementID = ID_ROW_COLUMN;
    */
    columns = new DefaultTableColumnModel();
    columns.addColumn(new TIndexColumn(columns.getColumnCount()));
    TColumn column;
    column = new TStringColumn(columns.getColumnCount(), COLUMN_NAME, TSpot.PARAMETER_NAME, false);
    column.setRemovable(false);
    ( (TStringColumn) column).setDefaultValue(" "); //$NON-NLS-1$
    columns.addColumn(column);
    column = new TDoubleColumn(columns.getColumnCount(), COLUMN_X, TSpot.PARAMETER_X, false);
    column.setRemovable(false);
    ( (TDoubleColumn) column).setDefaultValue(new Double(0));
    columns.addColumn(column);
    column = new TDoubleColumn(columns.getColumnCount(), COLUMN_Y, TSpot.PARAMETER_Y, false);
    column.setRemovable(false);
    ( (TDoubleColumn) column).setDefaultValue(new Double(0));
    columns.addColumn(column);
    column = new TIntegerColumn(columns.getColumnCount(), COLUMN_ROW, TSpot.PARAMETER_R, false);
    column.setRemovable(false);
    ( (TIntegerColumn) column).setDefaultValue(new Integer(0));
    columns.addColumn(column);
    column = new TIntegerColumn(columns.getColumnCount(), COLUMN_COL, TSpot.PARAMETER_C, false);
    column.setRemovable(false);
    ( (TIntegerColumn) column).setDefaultValue(new Integer(0));
    columns.addColumn(column);
	//3 columns added 2005/11/18 => Block/Row/Column
    column = new TIntegerColumn(columns.getColumnCount(), COLUMN_BLOCK, TSpot.PARAMETER_BLOCK, false);
    column.setRemovable(false);
    ( (TIntegerColumn) column).setDefaultValue(new Integer(0));
    columns.addColumn(column);
    column = new TIntegerColumn(columns.getColumnCount(), COLUMN_METAROW, TSpot.PARAMETER_ROW, false);
    column.setRemovable(false);
    ( (TIntegerColumn) column).setDefaultValue(new Integer(0));
    columns.addColumn(column);
    column = new TIntegerColumn(columns.getColumnCount(), COLUMN_METACOL, TSpot.PARAMETER_COLUMN, false);
    column.setRemovable(false);
    ( (TIntegerColumn) column).setDefaultValue(new Integer(0));
    columns.addColumn(column);
    column = new TDoubleColumn(columns.getColumnCount(), COLUMN_DIAM, TSpot.PARAMETER_DIAMETER, false);
    column.setRemovable(false);
    ( (TDoubleColumn) column).setDefaultValue(new Double(spotDiameter));
    columns.addColumn(column);
    colorizedColumn = 0;
    exemptedSpotFromColouring = null;
  }
  public void setLevel1Params(int nbRows, int nbColumns, double spotW, double spotH, double spotD, int startElement, int elementID) {
    if (nbRows != -1) L1NbRows = nbRows;
    if (nbColumns != -1) L1NbCols = nbColumns;
    if (spotW != -1) spotWidth = spotW;
    if (spotH != -1) spotHeight = spotH;
    if (spotD != -1) spotDiameter = spotD;
    if (startElement != -1) L1StartElement = startElement;
    if (elementID != -1) L1ElementID = elementID;
    TDoubleColumn col = ((TDoubleColumn)getColumnBySpotKey(TSpot.PARAMETER_DIAMETER));
    if (col != null) col.setDefaultValue(new Double(spotDiameter));
  }
  public void setLevel2Params(int nbRows, int nbColumns, double xSpacing, double ySpacing, int startElement, int elementID) {
    if (nbRows != -1) L2NbRows = nbRows;
    if (nbColumns != -1) L2NbCols = nbColumns;
    if (xSpacing != -1) L2XSpacing = xSpacing;
    if (ySpacing != -1) L2YSpacing = ySpacing;
    if (startElement != -1) L2StartElement = startElement;
    if (elementID != -1) L2ElementID = elementID;
  }
  
  /*// level3 removed - 20005/10/28
  public void setLevel3Params(int nbRows, int nbColumns, double xSpacing, double ySpacing, int startElement, int elementID) {
    if (nbRows != -1) L3NbRows = nbRows;
    if (nbColumns != -1) L3NbCols = nbColumns;
    if (xSpacing != -1) L3XSpacing = xSpacing;
    if (ySpacing != -1) L3YSpacing = ySpacing;
    if (startElement != -1) L3StartElement = startElement;
    if (elementID != -1) L3ElementID = elementID;
  }*/
  public void setNbLevels(int n) {
    nbLevels = n;
  //  if (n < 3) setLevel3Params(1, 1, 0, 0, TOP_LEFT, ID_ROW_COLUMN);// level3 removed - 20005/10/28
    if (n < 2) setLevel2Params(1, 1, 0, 0, TOP_LEFT, ID_ROW_COLUMN);
  }
 // public int getLev3NbRows() { return L3NbRows; }// level3 removed - 20005/10/28
 // public int getLev3NbCols() { return L3NbCols; }// level3 removed - 20005/10/28
  //public int getLev3StartElement() { return L3StartElement; }// level3 removed - 20005/10/28
  //public int getLev3ElementID() { return L3ElementID; }// level3 removed - 20005/10/28
  //public double getLev3XSpacing() { return L3XSpacing; }// level3 removed - 20005/10/28
  //public double getLev3YSpacing() { return L3YSpacing; }// level3 removed - 20005/10/28
  public int getLev2NbRows() { return L2NbRows; }
  public int getLev2NbCols() { return L2NbCols; }
  public int getLev2StartElement() { return L2StartElement; }
  public int getLev2ElementID() { return L2ElementID; }
  //getLev2XSpacing renvoie l'espacement inter-colonne * la largeur d'un spot
  public double getLev2XSpacing() { return L2XSpacing; }
 // public double getLev2XSpacing() { return L2XSpacing*spotWidth; }
  public double getLev2YSpacing() { return L2YSpacing; }
  //public double getLev2YSpacing() { return L2YSpacing*spotHeight; }
  public int getLev1NbRows() { return L1NbRows; }
  public int getLev1NbCols() { return L1NbCols; }
  public int getLev1StartElement() { return L1StartElement; }
  public int getLev1ElementID() { return L1ElementID; }
  public double getSpotsWidth() { return spotWidth; }
  public double getSpotsHeight() { return spotHeight; }
  public double getSpotsDiam() { return spotDiameter; }
  public int getNbLevels() { return nbLevels; }
  public static String getStartElementString(int se) {
    return startElement[se - 1];
  }
  public static int getStartValue(String start) {
    int i = -1;
    for (int k = 0; k < startElement.length; k++)
      if (startElement[k].equalsIgnoreCase(start)) {
        i = k + 1;
        break;
      }
    return i;
  }
  
  public double getDeca_X() {
		return deca_X;
	}
	public void setDeca_X(double deca_X) {
		this.deca_X = deca_X;
	}
	public double getDeca_Y() {
		return deca_Y;
	}
	public void setDeca_Y(double deca_Y) {
		this.deca_Y = deca_Y;
	}
  
  public DefaultTableColumnModel getColumns() {
    return columns;
  }
  public void exemptSpotFromColouring(TSpot spot) {
    exemptedSpotFromColouring = spot;
  }
  public void addColumn(TColumn col) {
    col.setindex(columns.getColumnCount());
    columns.addColumn(col);
  }
  public void addColumn(TColumn col, int index) {
    int oldIndex = columns.getColumnCount();
    for (int i = index + 1; i < columns.getColumnCount(); i++) columns.getColumn(i).setModelIndex(i + 1);
    columns.addColumn(col);
    columns.moveColumn(oldIndex, index);
  }
  public TColumn getColumn(String name) {
    TColumn column = null;
    for (Enumeration enume = columns.getColumns(); enume.hasMoreElements(); ) {
      column = (TColumn)enume.nextElement();
      if (column.toString().equals(name))
        break;
      else
        column = null;
    }
    return column;
  }
  public TColumn getColumnBySpotKey(String sk) {
    TColumn column = null;
    for (Enumeration enume = columns.getColumns(); enume.hasMoreElements(); ) {
      column = (TColumn)enume.nextElement();
      if (column.getSpotKey().equals(sk))
        break;
      else
        column = null;
    }
    return column;
  }
  public void hideColumn(TColumn col) {
    col.setPreferredWidth(0);
    col.setMinWidth(0);
    col.setMaxWidth(0);
    col.setResizable(false);
  }
  public void showColumn(TColumn col) {
    col.setMinWidth(0);
    col.setResizable(true);
    col.setPreferredWidth(75);
    col.setMaxWidth(1000);
  }
  public void removeColumn(TColumn column) {
    int index = column.getModelIndex();
    for (int i = index + 1; i < columns.getColumnCount(); i++) columns.getColumn(i).setModelIndex(i - 1);
    columns.removeColumn(column);
  }
  public void setColorizedColumn(int c) {
    colorizedColumn = c;
  }
  public Color getSpotColor(TSpot spot) {
    if ((colorizedColumn > 0) && (spot != exemptedSpotFromColouring)) {
      TColumn col = (TColumn)columns.getColumn(colorizedColumn);
      Comparable value = (Comparable)spot.getParameter(col.getSpotKey());
      Comparable infValue = (Comparable)col.getInfValue();
      Comparable supValue = (Comparable)col.getSupValue();
      Comparable betweenValue1 = (Comparable)col.getBetweenValue1();
      Comparable betweenValue2 = (Comparable)col.getBetweenValue2();
      if (supValue != null)
        if (value.compareTo(supValue) > 0)
          return col.getSupColor();
      if (infValue != null)
        if (value.compareTo(infValue) < 0)
          return col.getInfColor();
      if ((betweenValue1 != null) && (betweenValue2 != null))
        if ((value.compareTo(betweenValue1) >= 0) && (value.compareTo(betweenValue2) <= 0))
          return col.getBetweenColor();
    }
    return null;
  }
  public void saveStructure(BufferedWriter bw, int indent) {
    String base = ""; //$NON-NLS-1$
    for (int i = 0; i < indent; i++) base += " "; //$NON-NLS-1$
    String ind = "  "; //$NON-NLS-1$
    try {
      bw.write(base + "<params>"); //$NON-NLS-1$
      bw.newLine();

      bw.write(base + ind + "<level>" + nbLevels + "</level>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();

      bw.write(base + ind + "<level1>"); //$NON-NLS-1$
      bw.newLine();

      bw.write(base + ind + ind + "<nbColumns>" + L1NbCols + "</nbColumns>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<columnWidth>" + spotWidth + "</columnWidth>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<nbRows>" + L1NbRows + "</nbRows>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<rowHeight>" + spotHeight + "</rowHeight>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<spotDiameter>" + spotDiameter + "</spotDiameter>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<startElement>" + L1StartElement + "</startElement>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<elementID>" + L1ElementID + "</elementID>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();

      bw.write(base + ind + "</level1>"); //$NON-NLS-1$
      bw.newLine();

      bw.write(base + ind + "<level2>"); //$NON-NLS-1$
      bw.newLine();

      bw.write(base + ind + ind + "<nbColumns>" + L2NbCols + "</nbColumns>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<columnSpacing>" + L2XSpacing + "</columnSpacing>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<nbRows>" + L2NbRows + "</nbRows>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<rowSpacing>" + L2YSpacing + "</rowSpacing>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<startElement>" + L2StartElement + "</startElement>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<elementID>" + L2ElementID + "</elementID>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();

      bw.write(base + ind + "</level2>"); //$NON-NLS-1$
      bw.newLine();
/*// level3 removed - 20005/10/28
      bw.write(base + ind + "<level3>"); //$NON-NLS-1$
      bw.newLine();

      bw.write(base + ind + ind + "<nbColumns>" + L3NbCols + "</nbColumns>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<columnSpacing>" + L3XSpacing + "</columnSpacing>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<nbRows>" + L3NbRows + "</nbRows>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<rowSpacing>" + L3YSpacing + "</rowSpacing>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<startElement>" + L3StartElement + "</startElement>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();
      bw.write(base + ind + ind + "<elementID>" + L3ElementID + "</elementID>"); //$NON-NLS-1$ //$NON-NLS-2$
      bw.newLine();

      bw.write(base + ind + "</level3>"); //$NON-NLS-1$
      bw.newLine();
*/
      bw.write(base + "</params>"); //$NON-NLS-1$
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  
  public void saveShift(BufferedWriter bw, int indent){
	  
	  String base = ""; //$NON-NLS-1$
	    for (int i = 0; i < indent; i++) base += " "; //$NON-NLS-1$
	    String ind = "  "; //$NON-NLS-1$
	    try {
	      bw.write(base + "<shift shift_x = \""+this.getDeca_X()+"\" shift_y = \""+this.getDeca_Y()+"\" />"); //$NON-NLS-1$
	      bw.newLine();

//	      bw.write(base + ind + "<shift_x>" +  this.getDeca_X()+ "</shift_x>"); //$NON-NLS-1$ //$NON-NLS-2$
//	      bw.newLine();
//
//	      bw.write(base + ind + "<shift_y>" +  this.deca_Y+ "</shift_y>"); //$NON-NLS-1$ //$NON-NLS-2$
//	      bw.newLine();
//	      
//	      bw.write(base + "</shift>"); //$NON-NLS-1$
//	      bw.newLine();
	      
	    }
	    catch (Exception ex) {
	        ex.printStackTrace();
	     }
	  
  }
  
  public void saveColumns(BufferedWriter bw, int indent) {
    String base = ""; //$NON-NLS-1$
    for (int i = 0; i < indent; i++) base += " "; //$NON-NLS-1$
    String ind = "  "; //$NON-NLS-1$
    try {
      bw.write(base + "<columns>"); //$NON-NLS-1$
      bw.newLine();
      for (int i = 0; i < columns.getColumnCount(); i++) ((TColumn)columns.getColumn(i)).save(bw, indent + 2);
      bw.write(base + "</columns>"); //$NON-NLS-1$
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
