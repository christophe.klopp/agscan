/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import java.awt.Point;

public class TGridIterator {
  private int startElement, nbRows, nbColumns;
  private int x, y;

  public TGridIterator(int startElement, int nbRows, int nbColumns) {
    this.startElement = startElement;
    this.nbRows = nbRows;
    this.nbColumns = nbColumns;
    reset();
  }
  public Point next() {
    Point p = new Point(x, y);
    switch (startElement) {
      case TGridConfig.TOP_LEFT:
        x++;
        if ((x % nbColumns) == 0) {
          x = 0;
          y++;
        }
        break;
      case TGridConfig.TOP_RIGHT:
        x--;
        if (x < 0) {
          x = nbColumns - 1;
          y++;
        }
        break;
      case TGridConfig.BOTTOM_LEFT:
        x++;
        if ((x % nbColumns) == 0) {
          x = 0;
          y--;
        }
        break;
      case TGridConfig.BOTTOM_RIGHT:
        x--;
        if (x < 0) {
          x = nbColumns - 1;
          y--;
        }
        break;
    }
    return p;
  }
  public boolean hasNext() {
    return (((startElement == TGridConfig.TOP_LEFT)     && (y < nbRows)) ||
            ((startElement == TGridConfig.TOP_RIGHT)    && (y < nbRows)) ||
            ((startElement == TGridConfig.BOTTOM_LEFT)  && (y >= 0)) ||
            ((startElement == TGridConfig.BOTTOM_RIGHT) && (y >= 0)));
  }
  public void reset() {
    switch (startElement) {
      case TGridConfig.TOP_LEFT:
        x = y = 0;
        break;
      case TGridConfig.TOP_RIGHT:
        x = nbColumns - 1;
        y = 0;
        break;
      case TGridConfig.BOTTOM_LEFT:
        x = 0;
        y = nbRows - 1;
        break;
      case TGridConfig.BOTTOM_RIGHT:
        x = nbColumns - 1;
        y = nbRows - 1;
        break;
    }
  }
  public String getName(int n, int eid) {
    String s = "";
    int c, r, rg;
    c = (n - 1) % nbColumns;
    r = (n - 1) / nbColumns;
    rg = r * nbColumns + c + 1;
    if (eid == TGridConfig.ID_ROW_COLUMN)
      s =  String.valueOf('A' + c) + ((r < 10) ? "0" : "") + String.valueOf(r);
    else
      s = ((rg < 0) ? "0" : "") + String.valueOf(rg);
    return s;
  }
  public Point getPoint(int i) {
    return new Point(i / nbColumns, i % nbColumns);
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
