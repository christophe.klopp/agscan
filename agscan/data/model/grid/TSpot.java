/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import ij.ImagePlus;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.table.DefaultTableColumnModel;

import agscan.TEventHandler;
import agscan.algo.fit.TFitAlgorithm;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.table.TTablePanel;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;

public class TSpot extends TGridElement {
	public static final String PARAMETER_NAME = "Name";//"Nom";//modified 2005/11/17
	public static int test_cpt = 0;
	public static final String PARAMETER_X = "X";
	public static final String PARAMETER_Y = "Y";
	public static final String PARAMETER_R = "R";
	public static final String PARAMETER_C = "C";
	public static final String PARAMETER_DIAMETER = "Diameter";//"Diametre";//modified 2005/11/17
	public static final String PARAMETER_COMPUTED_DIAMETER = "Computed Diameter";//"Diametre calcule";//modified 2005/11/17
//	3 parameters added 2005/11/18
	public static final String PARAMETER_BLOCK = "Block";
	public static final String PARAMETER_ROW = "Column";
	public static final String PARAMETER_COLUMN = "Row";
	
	/*public static final String PARAMETER_Q_IMAGE_CONSTANT = "Q(image, constant)";
	 public static final String PARAMETER_Q_IMAGE_VARIABLE = "Q(image,variable)";
	 public static final String PARAMETER_Q_FIT_CONSTANT = "Q(fit,constant)";
	 public static final String PARAMETER_Q_FIT_VARIABLE = "Q(fit,variable)";
	 public static final String PARAMETER_COMPUTED_DIAMETER = "Diameetre calculee";
	 public static final String PARAMETER_FIT_CORRECTION = "Correction de fit";
	 public static final String PARAMETER_OVERSHINING_CORRECTION = "Correction d'overshining";
	 public static final String PARAMETER_QM = "QM";*/
	
	
	public static final int NE = 1;
	public static final int NW = 2;
	public static final int SE = 3;
	public static final int SW = 4;
	
	private double userDiameter, initDiameter, computedDiameter;
	private Hashtable parameters, synchro, fitParameters;
	private int quality;
	private TSpot left, right, top, bottom, globalLeft, globalRight, globalTop, globalBottom;
	private TFitAlgorithm fitAlgo;
	
	public TSpot(int r, int c, TGridElement p, TGridConfig config, String name) {
		row = r;
		col = c;
		parent = p;
		quality = -1;
		fitAlgo = null;//modif integration 14/09/05
		int xI = c - 1;
		if ((config.getLev1StartElement() == TGridConfig.TOP_RIGHT) || (config.getLev1StartElement() == TGridConfig.BOTTOM_RIGHT))
			xI = config.getLev1NbCols() - c;
		int yI = r - 1;
		if ((config.getLev1StartElement() == TGridConfig.BOTTOM_LEFT) || (config.getLev1StartElement() == TGridConfig.BOTTOM_RIGHT))
			yI = config.getLev1NbRows() - r;
		x = initX = xI * config.getSpotsWidth();
		y = initY = yI * config.getSpotsHeight();
		userDiameter = initDiameter = config.getSpotsDiam();
		computedDiameter = -1;//modif integration 14/09/05
		angle = 0.0F;
		parameters = new Hashtable();
		fitParameters = new Hashtable();
		synchro = new Hashtable();
		shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
		((GeneralPath)shape).moveTo((float)((config.getSpotsWidth() + config.getSpotsDiam()) / 2.0D),
				(float)config.getSpotsHeight() / 2.0F);
		double a;
		
		/*
		 * Spot drawing
		 */
		
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(config.getSpotsWidth() / 2.0D + config.getSpotsDiam() * Math.cos(a) / 2.0D),
					(float)(config.getSpotsHeight() / 2.0D + config.getSpotsDiam() * Math.sin(a) / 2.0D));
		}    
		addParameter(PARAMETER_X, new Double(getXCenter()), true);
		addParameter(PARAMETER_Y, new Double(getYCenter()), true);
		addParameter(PARAMETER_R, new Integer(getRow()), true);
		addParameter(PARAMETER_C, new Integer(getCol()), true);
		//here filling of Block, Row and Column parameters for spots.
		//added 2005/11/19
		//r1a and c1a are ok for a classical numbering of block 1->... 
		int r1a =  ((int)Math.floor((getRow()-1)/config.getLev1NbRows()))*config.getLev2NbCols();//nb de blocs correspondants aux lignes completes de blocs precedentes
		int c1a =  ((int)Math.floor((getCol()-1)/config.getLev1NbCols()))+1;//index du bloc courant dans la ligne courante
		int block = r1a+c1a;
		//r1 and c1 are ok for a inverse numbering of block : block 1 is the last bottom right...
		//int r1 =  ((int)Math.floor((getCol()-1)/config.getLev1NbCols()))*config.getLev2NbRows();//nb de blocs correspondants aux lignes completes de blocs precedentes
		//int c1 =  ((int)Math.floor((getRow()-1)/config.getLev1NbRows()))+1;//index du bloc courant dans la ligne courante
		//int block = r1+c1;
	
	
		addParameter(PARAMETER_BLOCK, new Integer(block), true);
		//TODO WARNING special adaptation : this version bugs if we inverse spot numbering in blocks.
		// that's why we inverse only on display!
//		addParameter(PARAMETER_ROW, new Integer(config.getLev1NbRows()+1 - getLocalRow()), true);//modif 2005/11/23 for LENA
	//	addParameter(PARAMETER_COLUMN, new Integer(config.getLev1NbCols()+1 - getLocalColumn()), true);//modif 2005/11/23 for LENA
		addParameter(PARAMETER_ROW, new Integer(getLocalRow()), true);
		addParameter(PARAMETER_COLUMN, new Integer(getLocalColumn()), true);
		
		
		addParameter(PARAMETER_DIAMETER, new Double(config.getSpotsDiam()), true);// new Double(userDiameter)
		DefaultTableColumnModel cols = config.getColumns();
		if (cols.getColumnCount() > 9)//7 before, 9 now
			for (int i = 9; i < cols.getColumnCount(); i++)//idem
				parameters.put(((TColumn)cols.getColumn(i)).getSpotKey(), ((TColumn)cols.getColumn(i)).getDefaultValue());
		color = Color.blue;
		index = (r - 1) * config.getLev1NbCols() + c;
		if (config.getLev1ElementID() == TGridConfig.ID_ROW_COLUMN)
			if (c <= 26)
				name +=  String.valueOf((char)('A' + c - 1)) + ((r < 10) ? "0" : "") + String.valueOf(r);
			else
				name +=  String.valueOf((char)('a' + c - 27)) + ((r < 10) ? "0" : "") + String.valueOf(r);
		else
			name += ((index < 10) ? "0" : "") + String.valueOf(index);
		index = (r - 1) * config.getLev1NbCols() + c - 1;
		addParameter(PARAMETER_NAME, name, true);
		/*addParameter(PARAMETER_Q_IMAGE_CONSTANT, new Double(0), false);
		 addParameter(PARAMETER_Q_IMAGE_VARIABLE, new Double(0), false);
		 addParameter(PARAMETER_Q_FIT_CONSTANT, new Double(0), false);
		 addParameter(PARAMETER_Q_FIT_VARIABLE, new Double(0), false);
		 addParameter(PARAMETER_COMPUTED_DIAMETER, new Double(0), false);
		 addParameter(PARAMETER_FIT_CORRECTION, new Double(0), false);
		 addParameter(PARAMETER_OVERSHINING_CORRECTION, new Double(0), false);
		 addParameter(PARAMETER_QM, new Double(0), false);*/
	}
	public void reset() {
		x = initX;
		y = initY;
		addParameter(PARAMETER_X, new Double(getXCenter()), true);
		addParameter(PARAMETER_Y, new Double(getYCenter()), true);
	}
	public void addParameter(String sk, Object dv, boolean syn) {
		if (dv != null) {
			parameters.remove(sk);
			parameters.put(sk, dv);
			synchro.put(sk, new Boolean(syn));
			if (sk.equals(PARAMETER_DIAMETER)) {
				userDiameter = ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue();
				updateShapeWithUser();
			}
			/*if (sk.equals(PARAMETER_X))
			 x += ((Double)dv).doubleValue() - getXCenter();
			 if (sk.equals(PARAMETER_Y))
			 y += ((Double)dv).doubleValue() - getYCenter();*/
		}
	}  
	public void addFitParameter(Object key, Object value) {
		fitParameters.put(key, value);
	}
	public Object getFitParameter(Object key) {
		return fitParameters.get(key);
	}
	public String getFitParameters() {
		if (fitAlgo != null) return fitAlgo.getParameters(this);
		return "";
	}
	public String getFitName() {
		if (fitAlgo != null) return fitAlgo.getName();
		return "";
	}
	public void setComputedDiameter(double d) {
		computedDiameter = d;
		parameters.remove(PARAMETER_COMPUTED_DIAMETER);
		parameters.put(PARAMETER_COMPUTED_DIAMETER, new Double(computedDiameter));
		synchro.put(PARAMETER_COMPUTED_DIAMETER, new Boolean(true));
	}
	public void setFitAlgorithm(TFitAlgorithm algo) {
		fitAlgo = algo;
		synchro.put("FIT", new Boolean(algo != null));
	}
	public TFitAlgorithm getFitAlgorithm() {
		return fitAlgo;
	}
	public double[] getFitData() {
		if (fitAlgo != null)
			return fitAlgo.getFitData(this);
		return null;
	}
	//ajout integration 14/09/05
	public String getSync() {
		Enumeration en = synchro.keys();
		String key, value, s = "";
		while (en.hasMoreElements()) {
			key = (String)en.nextElement();
			value = ((Boolean)synchro.get(key)).toString();
			s = s + key + "," + value + ",";
		}
		return s;
	}
	//ajout integration 14/09/05
	public void setSync(String s) {
		String key, value;
		StringTokenizer strtok = new StringTokenizer(s, ",");
		while (strtok.hasMoreTokens()) {
			key = strtok.nextToken();
			if (strtok.hasMoreTokens()) {
				value = strtok.nextToken();
				synchro.put(key, new Boolean(value));
			}
		}
	}
	public void updateSpotsPosition() {
		addParameter(PARAMETER_X, new Double(getXCenter()), true);
		addParameter(PARAMETER_Y, new Double(getYCenter()), true);
	}
	public void updateDiameterWithUser() {
		parameters.remove(PARAMETER_DIAMETER);
		parameters.put(PARAMETER_DIAMETER, new Double((int)(userDiameter * 100000.0D) / 100000.0D));
		updateShapeWithUser();
	}
	public void updateDiameterWithComputed() {
		parameters.remove(PARAMETER_COMPUTED_DIAMETER);
		parameters.put(PARAMETER_COMPUTED_DIAMETER, new Double(computedDiameter));
		if (computedDiameter == 0)
			updateShapeWithInit();
		else
			updateShapeWithComputed();
	}
	public void updateDiameterWithInit() {
		parameters.remove(PARAMETER_DIAMETER);
		parameters.put(PARAMETER_DIAMETER, new Double((int)(initDiameter * 100000.0D) / 100000.0D));
		updateShapeWithInit();
	}
	public void setSynchro(String sk, boolean b) {
		synchro.put(sk, new Boolean(b));
	}
	//ajout integration 13/09/05
	public void setSynchro(boolean b, TTablePanel tp) {
		Enumeration columns = tp.getSynchroneColumnsKeys().elements();
		while (columns.hasMoreElements()) {
			setSynchro((String)columns.nextElement(), b);
		}
	}
	public void removeParameter(String sk) {
		parameters.remove(sk);
		synchro.remove(sk);
	}
	public void updateShapeWithInit() {
		shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
		//userDiameter = ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue();
		((GeneralPath)shape).moveTo((float)((getWidth() + initDiameter) / 2.0D), (float)getHeight() / 2.0F);
		double a;
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(getWidth() / 2.0D + initDiameter * Math.cos(a) / 2.0D),
					(float)(getHeight() / 2.0D + initDiameter * Math.sin(a) / 2.0D));
		}
	}
	public void updateShapeWithComputed() {
		shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
		//userDiameter = ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue();
		((GeneralPath)shape).moveTo((float)((getWidth() + computedDiameter) / 2.0D), (float)getHeight() / 2.0F);
		double a;
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(getWidth() / 2.0D + computedDiameter * Math.cos(a) / 2.0D),
					(float)(getHeight() / 2.0D + computedDiameter * Math.sin(a) / 2.0D));
		}
	}
	public void updateShapeWithUser() {
		shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
		//userDiameter = ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue();
		((GeneralPath)shape).moveTo((float)((getWidth() + userDiameter) / 2.0D), (float)getHeight() / 2.0F);
		double a;
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(getWidth() / 2.0D + userDiameter * Math.cos(a) / 2.0D),
					(float)(getHeight() / 2.0D + userDiameter * Math.sin(a) / 2.0D));
		}
	}
	/* integration ==> methode viree
	 private void updateShape() {
	 shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
	 userDiameter = ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue();
	 ((GeneralPath)shape).moveTo((float)((getWidth() + userDiameter) / 2.0D), (float)getHeight() / 2.0F);
	 double a;
	 for (int k = 20; k <= 360; k += 20) {
	 a = k * Math.PI / 180.0D;
	 ((GeneralPath)shape).lineTo((float)(getWidth() / 2.0D + userDiameter * Math.cos(a) / 2.0D),
	 (float)(getHeight() / 2.0D + userDiameter * Math.sin(a) / 2.0D));
	 }
	 }
	 */
	public void draw(Graphics2D g2d, boolean drag, boolean scrolled, Rectangle rect, double zoom) {
		//  System.out.println("angle="+getAngleTotal());
			int x, y, d;
		TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_SIMPLIFIED_SPOTS, null);
		boolean ss = ((Boolean)TEventHandler.handleMessage(event)[0]).booleanValue();
		event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_COMPUTED_DIAMETERS, null);//ajout integration 14/09/05
		boolean cd = ( (Boolean) TEventHandler.handleMessage(event)[0]).booleanValue();//ajout integration 14/09/05
		double zx = zoom / getModel().getPixelWidth();
		double zy = zoom / getModel().getPixelHeight();
		if (g2d != null) {
			if ( ( (!drag) && (!scrolled)) || !ss) {
				GeneralPath gp = new GeneralPath(shape);
				//08/09/05 - add rotation (useful if spot is'nt a circle)
				gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
						getTopLeftSpot().getHeight() / 2));
				gp.transform(AffineTransform.getScaleInstance(zx, zy));
				gp.transform(AffineTransform.getTranslateInstance(getX() * zx, getY() * zy));
				if (rect.width != 0)
					if (!gp.intersects(rect))return;
				if (getModel().getConfig().getSpotColor(this) != null) {
					g2d.setColor(getModel().getConfig().getSpotColor(this));
					g2d.fill(gp);
				}
				if (!isSelected())
					g2d.setColor(color);
				else {
					if (cd && (computedDiameter == 0)) {
						g2d.setColor(Color.magenta);
					}
					else if (quality == 0)
						g2d.setColor(Color.green);
					else if (quality == 1)
						g2d.setColor(Color.red);
					else{
						g2d.setColor(selectedColor);
						//System.out.println("test:"+test_cpt);
						 //test_cpt++;
					}
				}
				g2d.draw(gp);
			}
			else {		
				g2d.setColor(Color.yellow);			
				x = (int) (getXCenter() * zx);
				//x = (int)Math.round(getXCenter() * zx);//modif 2005/10/25
				y = (int) (getYCenter() * zy);
			//	y = (int)Math.round(getYCenter() * zy);//modif 2005/10/25
				d = Math.max( (int) zoom, 1);
				g2d.drawLine(x - d, y, x + d, y);
				g2d.drawLine(x, y - d, x, y + d);
			}
		}
	}
	public double getSpotWidth() { return ((TGridBlock)parent).getSpotWidth(); }
	public double getSpotHeight() { return ((TGridBlock)parent).getSpotHeight(); }
	//renvoie la largeur de la zone rectangulaire du spot
	public double getWidth() { return getSpotWidth(); }
	//renvoie la hauteur de la zone rectangulaire du spot
	public double getHeight() { return getSpotHeight(); }
	public double getInitialWidth() { return ((TGridBlock)parent).getInitialSpotWidth(); }
	public double getInitialHeight() { return ((TGridBlock)parent).getInitialSpotHeight(); }
	//getX renvoie le x du coin sup gauche du rectangle du spot
	//getXCenter renvoie le x du centre du spot (calcule e partir du getX et du getWidth)
	public double getXCenter() {
		return (int)Math.rint((getX() + getSpotWidth() / 2.0F) * 1000.0D) / 1000.0D ;
	}
	public double getYCenter() {
		return (int)Math.rint((getY() + getSpotHeight() / 2.0F) * 1000.0D) / 1000.0D;
	}
	public TSpot getTopLeftSpot() { return this; }
	public double getUserDiameter() { return userDiameter; }
	public double getComputedDiameter() { return computedDiameter; }//integration 12/09/05
	public double getInitDiameter() { return initDiameter; }
	public void setX(double f) { x = f; }
	public void setY(double f) { y = f; }
	public void resizeByRap(double xRap, double yRap, boolean xModif) {
		//	System.out.println("__________resizeByRap______________");
		x *= xRap;
		y *= yRap;
		((GeneralPath)shape).reset();
		//initial "moveto": on se place en 0 du cercle trigo (souvenir de college un effort: c'est un rayon e droite du centre
		((GeneralPath)shape).moveTo((float)((parent.getSpotWidth() + ((Double)getParameter(PARAMETER_DIAMETER)).doubleValue()) / 2.0D),
				(float)(parent.getSpotHeight() / 2.0D));
		double a;
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(parent.getSpotWidth() / 2.0D + getUserDiameter() * Math.cos(a) / 2.0D),
					(float)(parent.getSpotHeight() / 2.0D + getUserDiameter() * Math.sin(a) / 2.0D));
		}
		
		/*
		 //test carre
		  //initial "moveto": on se place en haut a gauche = par defaut du spot ?
		   ((GeneralPath)shape).moveTo((float)((parent.getSpotWidth() - getUserDiameter()) / 2.0D),(float)((parent.getSpotHeight() - getUserDiameter()) / 2.0D));
		   // on va au bout a droite
		    ((GeneralPath)shape).lineTo((float)(getUserDiameter()+((parent.getSpotWidth() - getUserDiameter()) / 2.0D)),(float)((parent.getSpotHeight() - getUserDiameter()) / 2.0D));
		    // on dessend
		     ((GeneralPath)shape).lineTo((float)(getUserDiameter()+((parent.getSpotWidth() - getUserDiameter()) / 2.0D)),(float)(getUserDiameter()+((parent.getSpotHeight() - getUserDiameter()) / 2.0D)));
		     // on va a gauche 
		      ((GeneralPath)shape).lineTo((float)((parent.getSpotWidth() - getUserDiameter()) / 2.0D),(float)(getUserDiameter()+((parent.getSpotHeight() - getUserDiameter()) / 2.0D)));
		      // on remonte au point de depart
		       ((GeneralPath)shape).lineTo((float)((parent.getSpotWidth() - getUserDiameter()) / 2.0D),(float)((parent.getSpotHeight() - getUserDiameter()) / 2.0D));
		       //((GeneralPath)shape).closePath(); fait le mme effet
		        ((GeneralPath)shape).transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
		        getTopLeftSpot().getHeight() / 2));
		        */  
		
		
		
	}
	protected void resize(double sw, double sh) {
		x = (col - 1) * sw;
		y = (row - 1) * sh;
		((GeneralPath)shape).reset();
		((GeneralPath)shape).moveTo((float)((sw + getUserDiameter()) / 2.0D), (float)(sh / 2.0D));
		double a;
		for (int k = 20; k <= 360; k += 20) {
			a = k * Math.PI / 180.0D;
			((GeneralPath)shape).lineTo((float)(sw / 2.0D + getUserDiameter() * Math.cos(a) / 2.0D),
					(float)(sh / 2.0D + getUserDiameter() * Math.sin(a) / 2.0D));
		}
	}
	public int getNbRowsInSpots() { return 1; }
	public int getNbColumnsInSpots() { return 1; }
	public Rectangle getBounds(double zoom, int xRes, int yRes) {
		GeneralPath gp = new GeneralPath(shape);
		double zx = zoom / xRes;
		double zy = zoom / yRes;
		//08/09/05 - add rotation (useful if spot is'nt a circle)
		gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
				getTopLeftSpot().getHeight() / 2));
		gp.transform(AffineTransform.getScaleInstance(zx, zy));
		gp.transform(AffineTransform.getTranslateInstance(getX() * zx, getY() * zy));
		return gp.getBounds();
	}
	
	//retourne le tableau des 4 coordonnees du rectangle du spot en taille reel (avec resolution)
	public int[][] getPoints(double xr, double yr) {
		//db contient les positions x et y des 4 points du rectangle correspondant au spot 1-->2
		//																			       |   |
		//																			       4<--3
		// exemple sur une image 1788x1044:
		//  dbl[0]= 15743.060546875
		//  dbl[1]= 8492.0791015625
		//  dbl[0]= 16093.0517578125
		//  dbl[1]= 8494.625
		//  dbl[0]= 16090.5048828125
		//  dbl[1]= 8844.6162109375
		//  dbl[0]= 15740.5146484375
		//  dbl[1]= 8842.0693359375
		// si on fait un rectangle de la shape (getRectangle et que l'on demande ses infos:)
		//  X=15740
		//  Y=8492
		//  height=353
		//  width=354
		// c'est honnete mais il y a toujours le decalage de 2 provoque par getAlignedArea...
		
		int[][] db = new int[2][4];
		Shape shape = getAlignedArea();
		PathIterator pi = shape.getPathIterator(AffineTransform.getScaleInstance(1.0D, 1.0D));
		double[] dbl = new double[2];
		pi.currentSegment(dbl);
		// System.out.println("xr= "+xr);
		// System.out.println("yr= "+yr);
		//Les rounds ici rendent l'alignement faux sur fluo
		db[0][0] = (int)(dbl[0] / xr);//modif 2005/10/25
		db[1][0] = (int)(dbl[1] / yr);//modif 2005/10/25
	//	db[0][0] = (int)Math.round(dbl[0] / xr);//modif 2005/10/25
	//	db[1][0] = (int)Math.round(dbl[1] / yr);//modif 2005/10/25
	//	System.out.println("dbl[0]= "+dbl[0]);
	//	System.out.println("dbl[1]= "+dbl[1]);
	//	System.out.println("db[0]= "+db[0][0]);
	//	System.out.println("db[1]= "+db[1][0]);
		pi.next();
		pi.currentSegment(dbl);
		//db[0][1] = (int)Math.round(dbl[0] / xr);//modif 2005/10/25
		//db[1][1] = (int)Math.round(dbl[1] / yr);//modif 2005/10/25
		db[0][1] = (int)(dbl[0] / xr);//modif 2005/10/25
		db[1][1] = (int)(dbl[1] / yr);//modif 2005/10/25
		pi.next();
		pi.currentSegment(dbl);
		//db[0][2] = (int)Math.round(dbl[0] / xr);//modif 2005/10/25
		//db[1][2] = (int)Math.round(dbl[1] / yr);//modif 2005/10/25
		db[0][2] = (int)(dbl[0] / xr);//modif 2005/10/25
		db[1][2] = (int)(dbl[1] / yr);//modif 2005/10/25
		pi.next();
		pi.currentSegment(dbl);
		//db[0][3] = (int)Math.round(dbl[0] / xr);//modif 2005/10/25
		//db[1][3] = (int)Math.round(dbl[1] / yr);//modif 2005/10/25
		db[0][3] = (int)(dbl[0] / xr);//modif 2005/10/25
		db[1][3] = (int)(dbl[1] / yr);//modif 2005/10/25
		/*
		 Rectangle testRect = getRectangle();
		 
		 System.out.println("height"+testRect.height);
		 System.out.println("width"+testRect.width);
		 System.out.println("X"+testRect.x);
		 System.out.println("Y"+testRect.y);
		 */
		return db;
	}
	
	//renvoie la zone alignee = un rectangle (en micrometres et pas en taille de pixels!....donc du 
	// genre: rectX=5666.0	rectY=4545.0	rectH=353.0	rectW=355.0
	
	//TODO demander e lopez pourquoi + ou - 2 (cf getPoints du coup l'exemple avec le petit decalage dans les points...
	// il ne sait plus (appel le 2005/10/25 donc on pourra essayer de les virer
	public Shape getAlignedArea() {
		GeneralPath gp = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
		//  08/09/05 - add rotation (useful if spot is'nt a circle)
		gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
				getTopLeftSpot().getHeight() / 2));
		gp.moveTo(-2.0F, -2.0F);
		gp.lineTo((float)getWidth() + 2, -2.0F);
		gp.lineTo((float)getWidth() + 2, (float)getHeight() + 2);
		gp.lineTo(-2.0F, (float)getHeight() + 2);
		gp.closePath();
		gp.transform(AffineTransform.getRotateInstance(parent.getAngleTotal(), 0, 0));
		gp.transform(AffineTransform.getTranslateInstance(getX(), getY()));
		return gp;
		
	}
	//remi 2005/12/15 added
	 public Vector getShapePixels(double xRes, double yRes) {
	    Vector v = getPoints2D(xRes, yRes);
	    Vector v2 = new Vector();
	    Enumeration enu = v.elements();
	    Point2D.Double point;
	    boolean north, south, east, west;

	    while (enu.hasMoreElements()) {
	      point = (Point2D.Double)enu.nextElement();
	      point.x = (int)point.x;
	      point.y = (int)point.y;
	      north = v.contains(new Point2D.Double(point.x, point.y - 1));
	      south = v.contains(new Point2D.Double(point.x, point.y + 1));
	      east = v.contains(new Point2D.Double(point.x + 1, point.y));
	      west = v.contains(new Point2D.Double(point.x - 1, point.y));
	      if (!north || !south || !east || !west) v2.addElement(point);
	    }
	    return v2;
	  }
	
	
	// ajout integration 14/09/05 (getPixels devient getPixels2 et getPixel est different
	// une des methodes renvoie le vecteur de tous les points de la zone rectangulaire du spot,
	// l'autre seulement les points a l'interieur du cercle du spot ??? a verifier!
	//xRes = PixelWidth
	//yRes = PixelHeight
	//TODO bien comprendre la difference sur un exemple!
	// 2005/10/24 getPixels2 renomme getPoints2D_2 => getPoints2DRectangle
	public Vector getPoints2DRectangle(double xRes, double yRes) {
		Vector v = new Vector();
		Shape sh = getAlignedArea();
		Rectangle rect = sh.getBounds();
		double x1 = rect.getX() / xRes;
		double x2 = rect.getX() / xRes + rect.getWidth() / xRes;
		double y1 = rect.getY() / yRes;
		double y2 = rect.getY() / yRes + rect.getHeight() / yRes;
		for (double x = x1; x < x2; x++)
			for (double y = y1; y < y2; y++)
				v.addElement(new Point2D.Double(x, y));
		return v;
	}
	// 2005/10/24 getPixels renomme getPoints2D
	public Vector getPoints2D(double xRes, double yRes) {
		Vector v = new Vector();
		Shape sh = getAlignedArea();
		Rectangle rect = sh.getBounds();
		double x1 = rect.getX() / xRes;
		double x2 = (rect.getX() + rect.getWidth()) / xRes;
		double y1 = rect.getY() / yRes;
		double y2 = (rect.getY() + rect.getHeight()) / yRes;
		//for (double x = x1; x < x2; x++)
		//	for (double y = y1; y < y2; y++)
		//		if (sh.contains(x * xRes, y * yRes)) v.addElement(new Point2D.Double(x, y));
				//2005/10/25 - test inversion de lordre x ou y en premier (balayage en ordonnee au lieu d'en abcisse
		for (double y = y1; y < y2; y++)	
			for (double x = x1; x < x2; x++)
				if (sh.contains(x * xRes, y * yRes)) v.addElement(new Point2D.Double(x, y));
		return v;
	}
	
	//test remi 03/10/05 renvoie le rectangle en micrometres...
	public Rectangle getRectangle() {
		Shape sh = getAlignedArea();
		Rectangle rect = sh.getBounds();
		return rect;
	}
	//renvoie le rectangle en pixels
	public Rectangle getRectangleInPix(){
		int pixelWidth = (int)  getModel().getPixelWidth();//largeur d'un pixel en em (par ex 25em)
		int pixelHeight = (int)  getModel().getPixelHeight();//hauteur d'un pixel en em (par ex 25em)
		Rectangle rect = getRectangle();
		//TODO attention au probleme de division au resultat entier : 3.96 sera 3 par defaut!
		//rect.setBounds((int)Math.ceil(rect.x/pixelWidth),(int)Math.ceil(rect.y/pixelHeight),Math.round(rect.width/pixelWidth),Math.round(rect.height/pixelHeight));//mise a l'echelle bidon??!!!
		rect.setBounds((rect.x/pixelWidth),(rect.y/pixelHeight),(rect.width/pixelWidth),(rect.height/pixelHeight));//mise a l'echelle bidon??!!!
		return rect;
	}
	
	public short[] getROIPixels(ImagePlus fullImage){
		ImagePlus imagePart = fullImage;
		Rectangle selection = getRectangleInPix();
		imagePart.setRoi(selection);
		short[] pixels = (short[]) imagePart.getProcessor().crop().getPixels();
		return pixels;
	}
	
	//test remi 03/10/05
	//getRectanglePixels devient getRectanglePoints le 24/10/2005
	public Vector getRectanglePixels(double xRes, double yRes) {
		Vector v = new Vector();
		Shape sh = getAlignedArea();
		Rectangle rect = sh.getBounds();
		double x1 = rect.getX() / xRes;
		double x2 = (rect.getX() + rect.getWidth()) / xRes;
		double y1 = rect.getY() / yRes;
		double y2 = (rect.getY() + rect.getHeight()) / yRes;
		for (double x = x1; x < x2; x++)
			for (double y = y1; y < y2; y++)
				v.addElement(new Point2D.Double(x, y));
		return v;
	}
	
	public TSpot getSpot(double x, double y) {
		GeneralPath gp = new GeneralPath(shape);
		//  08/09/05 - add rotation (useful if spot is'nt a circle)
		//  gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
		//		getTopLeftSpot().getHeight() / 2));
		gp.transform(AffineTransform.getTranslateInstance(getX(), getY()));
		if (gp.contains(x, y)) return this;
		return null;
	}
	public TSpot getSpot(int r, int c) {
		if ((getRow() == r) && (getCol() == c)) return this;
		return null;
	}
	public Vector getSpots(Rectangle rect) {
		Vector v = new Vector();
		GeneralPath gp = new GeneralPath(shape);
		//  08/09/05 - add rotation (useful only if the spot is not a circle)
		gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
				getTopLeftSpot().getHeight() / 2));
		gp.transform(AffineTransform.getTranslateInstance(getX(), getY()));
		if (gp.intersects(rect)) v.addElement(this);
		return v;
	}
	public Vector getSpots() {
		Vector v = new Vector(1);
		v.addElement(this);
		return v;
	}
	public void setDrawingColor(Color c) { color = c; }
	public void addToDiameter(double d) {
		if ((userDiameter + d) > 0) {
			userDiameter += (float)d;
			updateDiameterWithUser();
		}
	}
	public void setUserDiameter(double d) {
		userDiameter = d;
		updateDiameterWithUser();
	}
	public String getName() {
		return (String)parameters.get(PARAMETER_NAME);
	}
	public Object getParameter(Object key) {
		return parameters.get(key);
	}
	public boolean isSynchro(Object key) {
		if (synchro.get(key) == null) return true;
		return ((Boolean)synchro.get(key)).booleanValue();
	}
	public String getParameterString(Object key) {
		Object o = getParameter(key);
		String s = o.toString();
		if (o instanceof Vector)
			s = (String)((Vector)o).firstElement() + ";" + (String)((Vector)o).lastElement();
		else if (o instanceof Boolean)
			if (((Boolean)o).booleanValue())
				s = "1";
			else
				s = "0";
		return s;
	}
	public int getIndex() {
		if (parent != null) return index + parent.getIndex();
		return index;
	}
	public TGridModel getModel() {
		return ((TGridBlock)parent).getModel();
	}
	public boolean isSelected() {
		return getModel().getSelectionModel().isRecursivelySelected(this);
	}
	public int isOnShape(double xm, double ym) {
		int ret = 0;
		GeneralPath gp = new GeneralPath(shape);
		//  08/09/05 - add rotation (useful if spot is'nt a circle)
		gp.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
				getTopLeftSpot().getHeight() / 2));
		gp.transform(AffineTransform.getTranslateInstance(getX(), getY()));
		if (isOnNEBorder(xm, ym))
			ret = NE;
		else if (isOnNWBorder(xm, ym))
			ret = NW;
		else if (isOnSEBorder(xm, ym))
			ret = SE;
		else if (isOnSWBorder(xm, ym))
			ret = SW;
		return ret;
	}
	public void printParameters() {
		Object key;
		for (Enumeration enume = parameters.keys(); enume.hasMoreElements(); ) {
			key = enume.nextElement();
			System.err.println(key + " : " + parameters.get(key));
		}
		System.err.println("");
	}
	public void save(BufferedWriter bw, int indent, DefaultTableColumnModel columns) {
		String base = "";
		for (int i = 0; i < indent; i++) base += " ";
		String ind = "  ";
		Enumeration cols = columns.getColumns();
		TColumn column;
		Object def, val;
		String spotKey;
		boolean modified = false;
		try {
			while (cols.hasMoreElements()) {
				column = (TColumn) cols.nextElement();
				def = column.getDefaultValue();
				spotKey = column.getSpotKey();
				if (spotKey == PARAMETER_DIAMETER)
					val = new Double(getUserDiameter());//modif 14/09/05
				else
					val = parameters.get(spotKey);
				
				if (column.isEditable() && (!val.toString().equals(def.toString()))) {
					if (!modified) {
						bw.write(base + "<spot name=\"" + getName() + "\" index=\"" + (getIndex() + 1) + "\">");
						bw.newLine();
						modified = true;
					}
					bw.write(base + ind + "<column name=\"" + column.toString() + "\" value=\"" + val + "\"/>");
					bw.newLine();
				}
			}
			if (modified) {
				bw.write(base + "</spot>");
				bw.newLine();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public void exportAsText(BufferedWriter bw, TGridModel gridModel) {
		Enumeration cols = gridModel.getConfig().getColumns().getColumns();
		TColumn column;
		Object val;
		try {
			while (cols.hasMoreElements()) {
				column = (TColumn) cols.nextElement();
				if (column.isVisible()) {
					val = gridModel.getValueAt(getIndex(), gridModel.getConfig().getColumns().getColumnIndex(column.getIdentifier()));
					bw.write(val + "\t");
				}
			}
			bw.write(new Double(quality).toString());
			bw.newLine();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	private boolean isOnNEBorder(double x, double y) {
		double x0 = getXCenter();
		double y0 = getYCenter();
		if ((x >= x0) && (y <= y0)) {
			double d = Math.abs(Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0)) -
					(((Double)getParameter(PARAMETER_DIAMETER)).doubleValue() / 2));
			return (d <= 25);
		}
		return false;
	}
	private boolean isOnSEBorder(double x, double y) {
		double x0 = getXCenter();
		double y0 = getYCenter();
		if ((x >= x0) && (y >= y0)) {
			double d = Math.abs(Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0)) -
					(((Double)getParameter(PARAMETER_DIAMETER)).doubleValue() / 2));
			return (d <= 25);
		}
		return false;
	}
	private boolean isOnSWBorder(double x, double y) {
		double x0 = getXCenter();
		double y0 = getYCenter();
		if ((x <= x0) && (y >= y0)) {
			double d = Math.abs(Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0)) -
					(((Double)getParameter(PARAMETER_DIAMETER)).doubleValue() / 2));
			return (d <= 25);
		}
		return false;
	}
	private boolean isOnNWBorder(double x, double y) {
		double x0 = getXCenter();
		double y0 = getYCenter();
		if ((x <= x0) && (y <= y0)) {
			double d = Math.abs(Math.sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0)) -
					(((Double)getParameter(PARAMETER_DIAMETER)).doubleValue() / 2));
			return (d <= 25);
		}
		return false;
	}
	public void setQuality(int q) {
		quality = q;
	}
	public int getQuality() {
		return quality;
	}
	public TSpot getLeft() {
		return left;
	}
	public TSpot getRight() {
		return right;
	}
	public TSpot getTop() {
		return top;
	}
	public TSpot getBottom() {
		return bottom;
	}
	public void setLeft(TSpot sp) {
		left = sp;
	}
	public void setRight(TSpot sp) {
		right = sp;
	}
	public void setTop(TSpot sp) {
		top = sp;
	}
	public void setBottom(TSpot sp) {
		bottom = sp;
	}
	public TSpot getGlobalLeft() {
		return globalLeft;
	}
	public TSpot getGlobalRight() {
		return globalRight;
	}
	public TSpot getGlobalTop() {
		return globalTop;
	}
	public TSpot getGlobalBottom() {
		return globalBottom;
	}
	public void setGlobalLeft(TSpot sp) {
		globalLeft = sp;
	}
	public void setGlobalRight(TSpot sp) {
		globalRight = sp;
	}
	public void setGlobalTop(TSpot sp) {
		globalTop = sp;
	}
	public void setGlobalBottom(TSpot sp) {
		globalBottom = sp;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
