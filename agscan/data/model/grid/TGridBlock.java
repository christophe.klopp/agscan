/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.util.Vector;

import agscan.algo.fit.TFitAlgorithm;

public class TGridBlock extends TGridElement {
  protected TGridElement[][] gridArray;
  protected int nbCol, nbRow;
  protected double spotWidth, initialSpotWidth, spotHeight, initialSpotHeight;

  /**
   * @link aggregation
   * @undirected
   */
  protected TGridIterator iterator;
  private TGridModel model;

  public TGridBlock() {
    super();
  }
  private TGridBlock(TGridElement p, TGridConfig config, int r, int c, String name) {
    parent = p;
    model = ((TGridBlock)p).getModel();
    iterator = new TGridIterator(config.getLev1StartElement(), config.getLev1NbRows(), config.getLev1NbCols());
    col = c;
    row = r;
    nbCol = config.getLev1NbCols();
    nbRow = config.getLev1NbRows();
    spotWidth = initialSpotWidth = config.getSpotsWidth();
    spotHeight = initialSpotHeight = config.getSpotsHeight();
    double b1w = spotWidth * config.getLev1NbCols();
    double b1h = spotHeight * config.getLev1NbRows();
    int xI = c - 1;
    if ((config.getLev2StartElement() == TGridConfig.TOP_RIGHT) || (config.getLev2StartElement() == TGridConfig.BOTTOM_RIGHT))
      xI = config.getLev2NbCols() - c;
    int yI = r - 1;
    if ((config.getLev2StartElement() == TGridConfig.BOTTOM_LEFT) || (config.getLev2StartElement() == TGridConfig.BOTTOM_RIGHT))
      yI = config.getLev2NbRows() - r;
    gridArray = new TGridElement[nbRow][nbCol];
    x = initX = xI * (b1w + config.getLev2XSpacing());
    y = initY = yI * (b1h + config.getLev2YSpacing());
    if (config.getNbLevels() >= 2) {
      index = (r - 1) * config.getLev2NbCols() + c;
      if (config.getLev2ElementID() == TGridConfig.ID_ROW_COLUMN)
        if (c <= 26)
          name +=  String.valueOf((char)('A' + c - 1)) + ((r < 10) ? "0" : "") + String.valueOf(r) + "-";
        else
          name +=  String.valueOf((char)('a' + c - 27)) + ((r < 10) ? "0" : "") + String.valueOf(r) + "-";
      else
        name += ((index < 10) ? "0" : "") + String.valueOf(index) + "-";
      index = ((r - 1) * config.getLev2NbCols() + c - 1) * (nbCol * nbRow);
    }
    col = (c - 1) * nbCol;
    row = (r - 1) * nbRow;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j] = new TSpot(i + 1, j + 1, this, config, name);
    shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    ((GeneralPath)shape).moveTo(0, 0);
    ((GeneralPath)shape).lineTo((float)b1w, 0);
    ((GeneralPath)shape).lineTo((float)b1w, (float)b1h);
    ((GeneralPath)shape).lineTo(0, (float)b1h);
    ((GeneralPath)shape).lineTo(0, 0);
    color = Color.blue;
    angle = 0.0F;
  }

  private TGridBlock(TGridConfig config, int c, int r, TGridElement p) {
    parent = p;
    model = ((TGridBlock)p).getModel();
    iterator = new TGridIterator(config.getLev2StartElement(), config.getLev2NbRows(), config.getLev2NbCols());
    col = c;
    row = r;
    nbCol = config.getLev2NbCols();
    nbRow = config.getLev2NbRows();
    spotWidth = initialSpotWidth = config.getSpotsWidth();
    spotHeight = initialSpotHeight = config.getSpotsHeight();
    int xI = c - 1;
    
   // if ((config.getLev3StartElement() == TGridConfig.TOP_RIGHT) || (config.getLev3StartElement() == TGridConfig.BOTTOM_RIGHT))
     // xI = config.getLev3NbCols() - c;// level3 removed - 20005/10/28
    int yI = r - 1;
   // if ((config.getLev3StartElement() == TGridConfig.BOTTOM_LEFT) || (config.getLev3StartElement() == TGridConfig.BOTTOM_RIGHT))
     // yI = config.getLev3NbRows() - r;// level3 removed - 20005/10/28
    double b1w = spotWidth * config.getLev1NbCols();
    double b1h = spotHeight * config.getLev1NbRows();
    double b2w = b1w * nbCol + (nbCol - 1) * config.getLev2XSpacing();
    double b2h = b1h * nbRow + (nbRow - 1) * config.getLev2YSpacing();
    gridArray = new TGridElement[nbRow][nbCol];
    //  level3 removed - 20005/10/28
    x = initX = xI * b2w ;    //x = initX = xI * (b2w + config.getLev3XSpacing());
    y = initY = yI * b2h ;//y = initY = yI * (b2h + config.getLev3YSpacing());
  
    String name = "";
    //  level3 removed - 20005/10/28
/*
    if (config.getNbLevels() == 3) {
      index = r * config.getLev3NbCols() + c;
      if (config.getLev3ElementID() == TGridConfig.ID_ROW_COLUMN)
        if (c <= 26)
          name +=  String.valueOf((char)('A' + c - 1)) + ((r < 10) ? "0" : "") + String.valueOf(r) + "-";
        else
          name +=  String.valueOf((char)('a' + c - 27)) + ((r < 10) ? "0" : "") + String.valueOf(r) + "-";
      else
        name = ((index < 0) ? "0" : "") + String.valueOf(index) + "-";
      index = ((r - 1) * config.getLev3NbCols() + c - 1) * (nbCol * nbRow * config.getLev1NbCols() * config.getLev1NbRows());
    }*/
    col = (c - 1) * nbCol * config.getLev1NbCols();
    row = (r - 1) * nbRow * config.getLev1NbRows();
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j] = new TGridBlock(this, config, i + 1, j + 1, name);
    shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    ((GeneralPath)shape).moveTo(0, 0);
    ((GeneralPath)shape).lineTo((float)b2w, 0);
    ((GeneralPath)shape).lineTo((float)b2w, (float)b2h);
    ((GeneralPath)shape).lineTo(0, (float)b2h);
    ((GeneralPath)shape).lineTo(0, 0);
    color = Color.blue;
    angle = 0.0F;
  }

  public TGridBlock(TGridConfig config, TGridModel model) {
    parent = null;
    this.model = model;
     //iterator = new TGridIterator(config.getLev3StartElement(), config.getLev3NbRows(), config.getLev3NbCols());//  level3 removed - 20005/10/28
    iterator = new TGridIterator(1,1,1);//modif valeurs par defaut du 3eme niveau 
    //nbCol = config.getLev3NbCols();    //  level3 removed - 20005/10/28
    //nbRow = config.getLev3NbRows();    //  level3 removed - 20005/10/28
    nbCol =1;//modif
    nbRow =1;//modif
    x = y = initX = initY = 0.0F;
    col = 0;
    row = 0;
    gridArray = new TGridElement[nbRow][nbCol];
    spotHeight = initialSpotHeight = config.getSpotsHeight();
    spotWidth = initialSpotWidth = config.getSpotsWidth();
    double sd = config.getSpotsDiam();
    int ncs = config.getLev1NbCols();
    int nrs = config.getLev1NbRows();
    int nc1 = config.getLev2NbCols();
    int nr1 = config.getLev2NbRows();
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j] = new TGridBlock(config, j + 1, i + 1, this);
    index = 0;
    shape = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    double b1w = spotWidth * config.getLev1NbCols();
    double b1h = spotHeight * config.getLev1NbRows();
    double b2w = b1w * config.getLev2NbCols() + (config.getLev2NbCols() - 1) * config.getLev2XSpacing();
    double b2h = b1h * config.getLev2NbRows() + (config.getLev2NbRows() - 1) * config.getLev2YSpacing();
    //double b3w = b2w * config.getLev3NbCols() + (config.getLev3NbCols() - 1) * config.getLev3XSpacing();//  level3 removed - 20005/10/28
    //double b3h = b2h * config.getLev3NbRows() + (config.getLev3NbRows() - 1) * config.getLev3YSpacing();//  level3 removed - 20005/10/28
   // ((GeneralPath)shape).moveTo(0, 0);//  level3 removed - 20005/10/28
  // ((GeneralPath)shape).lineTo((float)b3w, 0);//  level3 removed - 20005/10/28
   //((GeneralPath)shape).lineTo((float)b3w, (float)b3h);//  level3 removed - 20005/10/28
   //((GeneralPath)shape).lineTo(0, (float)b3h);//  level3 removed - 20005/10/28   
   //((GeneralPath)shape).lineTo(0, 0);// level3 removed - 20005/10/28   
   ((GeneralPath)shape).moveTo(0, 0);// modif level 3=>2
   ((GeneralPath)shape).lineTo((float)b2w, 0);// modif level 3=>2
   ((GeneralPath)shape).lineTo((float)b2w, (float)b2h);// modif level 3=>2
   ((GeneralPath)shape).lineTo(0, (float)b2h);// modif level 3=>2
   ((GeneralPath)shape).lineTo(0, 0);// modif level 3=>2
  
   color = Color.blue;
      angle = 0.0F;
  }
  public void reset() {
    if (parent != null) {
      x = initX;
      y = initY;
    }
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j].reset();
  }
  public void draw(Graphics2D g2d, boolean drag, boolean scrolled, Rectangle rect, double zoom) {
    GeneralPath s = new GeneralPath(shape);
    double zx = zoom / model.getPixelWidth();
    double zy = zoom / model.getPixelHeight();
    s.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2,
                                                  getTopLeftSpot().getHeight() / 2));
    s.transform(AffineTransform.getScaleInstance(zx, zy));
    s.transform(AffineTransform.getTranslateInstance(getX() * zx, getY() * zy));
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j].draw(g2d, drag, scrolled, rect, zoom);
    if (!model.getSelectionModel().isSelected(this))
      g2d.setColor(color);
    else
      g2d.setColor(selectedColor);
    g2d.draw(s);
  }
  public Shape getAlignedShape() {
    GeneralPath s = new GeneralPath(shape);
    s.transform(AffineTransform.getRotateInstance(getAngleTotal(), getTopLeftSpot().getWidth() / 2.0D, getTopLeftSpot().getHeight() / 2.0D));
    s.transform(AffineTransform.getTranslateInstance(getX(), getY()));
    return s;
  }
  public double getWidth() {
    float w = 0;
    for (int i = 0; i < nbCol; i++) w += gridArray[0][i].getWidth();
    return w;
  }
  public double getInitialWidth() {
    float w = 0;
    for (int i = 0; i < nbCol; i++) w += gridArray[0][i].getInitialWidth();
    return w;
  }
  public double getHeight() {
    float h = 0;
    for (int i = 0; i < nbRow; i++) h += gridArray[i][0].getHeight();
    return h;
  }
  public double getInitialHeight() {
    float h = 0;
    for (int i = 0; i < nbRow; i++) h += gridArray[i][0].getInitialHeight();
    return h;
  }
  public TSpot getTopLeftSpot() { return gridArray[0][0].getTopLeftSpot(); }
  public void resizeByRap(double xRap, double yRap, boolean xModif) {
    if (xModif) {
      x *= xRap;
      y *= yRap;
    }
    ((GeneralPath)shape).transform(AffineTransform.getScaleInstance(xRap, yRap));
    spotWidth *= xRap;
    spotHeight *= yRap;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j].resizeByRap(xRap, yRap, true);
  }
  protected void resize(double sw, double sh) {
    ((GeneralPath)shape).transform(AffineTransform.getScaleInstance(sw / spotWidth, sh / spotHeight));
    x = x * sw / spotWidth;
    y = y * sh / spotHeight;
    spotWidth = sw;
    spotHeight = sh;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        gridArray[i][j].resize(spotWidth, spotHeight);
  }
  public double getSpotWidth() { return spotWidth; }
  public double getInitialSpotWidth() { return initialSpotWidth; }
  public double getSpotHeight() { return spotHeight; }
  public double getInitialSpotHeight() { return initialSpotHeight; }
  public void setSpotSize(double w, double h) {
    ((GeneralPath)shape).transform(AffineTransform.getScaleInstance(w / spotWidth, h / spotHeight));
    spotWidth = w;
    spotHeight = h;
  }
  public int getNbRowsInSpots() {
    int n = 0;
    for (int i = 0; i < nbRow; i++)
      n += gridArray[i][0].getNbRowsInSpots();
    return n;
  }
  public int getNbColumnsInSpots() {
    int n = 0;
    for (int i = 0; i < nbCol; i++)
      n += gridArray[0][i].getNbColumnsInSpots();
    return n;
  }
  public boolean isOnGrid(double x, double y) {
    return getAlignedShape().contains(x, y);
  }

  public Rectangle getBounds(double zoom, int xRes, int yRes) {
    GeneralPath s = new GeneralPath(shape);
    s.transform(AffineTransform.getRotateInstance((double)getAngleTotal(), getTopLeftSpot().getWidth() / 2, getTopLeftSpot().getHeight() / 2));
    s.transform(AffineTransform.getScaleInstance(zoom / xRes, zoom / yRes));
    s.transform(AffineTransform.getTranslateInstance(getX() * zoom / xRes, getY() * zoom / yRes));
    Rectangle rect = s.getBounds();
    Rectangle r;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++) {
        r = gridArray[i][j].getBounds(zoom, xRes, yRes);
        rect.add(r);
      }
    return rect;
  }
  public TSpot getSpot(double x, double y) {
    TSpot spot = null;
    for (int i = 0; i < nbRow; i++) {
      for (int j = 0; j < nbCol; j++) {
        spot = gridArray[i][j].getSpot(x, y);
        if (spot != null)
          break;
      }
      if (spot != null)
        break;
    }
    return spot;
  }
  public TSpot getSpot(int r, int c) {
    TSpot spot = null;
    for (int i = 0; i < nbRow; i++) {
      for (int j = 0; j < nbCol; j++) {
        if ((c >= (gridArray[i][j].getCol() + 0)) && (r >= (gridArray[i][j].getRow() + 0)) &&
            (c <= (gridArray[i][j].getCol() + 0 + gridArray[i][j].getNbColumnsInSpots())) &&
            (r <= (gridArray[i][j].getRow() + 0 + gridArray[i][j].getNbRowsInSpots())))
          spot = gridArray[i][j].getSpot(r, c);
        if (spot != null) break;
      }
      if (spot != null)  break;
    }
    return spot;
  }
  public TGridBlock getBlock(int r, int c) {
    TGridBlock block = null;
    if ((r == getRow()) && (c == getCol()))
      block = this;
    else {
      if (gridArray[0][0] instanceof TGridBlock)
        for (int i = 0; i < nbRow; i++) {
          for (int j = 0; j < nbCol; j++) {
            block = ((TGridBlock)gridArray[i][j]).getBlock(r, c);
            if (block != null) break;
          }
          if (block != null) break;
        }
    }
    return block;
  }
  public TGridElement getSpot(int i) {
    int rg = i / (gridArray[0][0].getNbColumnsInSpots() * gridArray[0][0].getNbRowsInSpots());
    Point p = iterator.getPoint(rg);
    return gridArray[p.x][p.y].getSpot(i % (gridArray[0][0].getNbColumnsInSpots() * gridArray[0][0].getNbRowsInSpots()));
  }
  public Vector getSpots(Rectangle rect) {
    Vector v = new Vector();
    for (int i = 0; i < nbRow; i++) {
      for (int j = 0; j < nbCol; j++) {
        v.addAll(gridArray[i][j].getSpots(rect));
      }
    }
    return v;
  }
  public TGridBlock getLevel2Element(double x, double y) {
    TGridBlock gb;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++) {
        gb = (TGridBlock)gridArray[i][j];
        if (gb.isOnGrid(x ,y)) return gb;
      }
    return null;
  }
  public TGridBlock getLevel2Element(int r, int c) {
    TGridBlock gb;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++) {
        gb = (TGridBlock)gridArray[i][j];
        if ((gb.getRow() == r) && (gb.getCol() == c)) return gb;
      }
    return null;
  }
  public Vector getLevel2Elements() {
    Vector v = new Vector();
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        v.addElement(gridArray[i][j]);
    return v;
  }
  public TGridBlock getLevel1Element(double x, double y, int nbLevels) {
    TGridBlock gb;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++){
        gb = (TGridBlock)gridArray[i][j];
        for (int i2 = 0; i2 < gb.nbRow; i2++)
          for (int j2 = 0; j2 < gb.nbCol; j2++)
            if (((TGridBlock)gb.gridArray[i2][j2]).isOnGrid(x ,y)) return (TGridBlock)gb.gridArray[i2][j2];
      }
    return null;
  }
  public TGridBlock getLevel1Element(int r, int c) {
    TGridBlock gb2, gb1;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++){
        gb2 = (TGridBlock)gridArray[i][j];
        for (int i2 = 0; i2 < gb2.nbRow; i2++)
          for (int j2 = 0; j2 < gb2.nbCol; j2++) {
            gb1 = (TGridBlock)gb2.gridArray[i2][j2];
            if ((gb1.getRow() == r) && (gb1.getCol() == c)) return gb1;
          }
      }
    return null;
  }
  public Vector getLevel1Elements() {
    Vector v = new Vector();
    TGridBlock gb;
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++) {
        gb = (TGridBlock)gridArray[i][j];
        if (gb.getFirstElement() instanceof TSpot)
          v.addElement(gb);
        else
          for (int i2 = 0; i2 < gb.nbRow; i2++)
            for (int j2 = 0; j2 < gb.nbCol; j2++)
              v.addElement(gb.gridArray[i2][j2]);
      }
    return v;
  }
  public Vector getSpots() {
    Vector v = new Vector();
    iterator.reset();
    Point p;
    while (iterator.hasNext()) {
      p = iterator.next();
      v.addAll(gridArray[p.y][p.x].getSpots());
    }
    return v;
  }
  public void addParameter(String sk, Object dv, boolean syn) {
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        ((TGridElement)gridArray[i][j]).addParameter(sk, dv, syn);
  }
  public void updateSpotsPosition() {
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        ((TGridElement)gridArray[i][j]).updateSpotsPosition();
  }
  public void removeParameter(String sk) {
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        ((TGridElement)gridArray[i][j]).removeParameter(sk);
  }
  public void setSynchro(String sk, boolean b) {
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        ((TGridElement)gridArray[i][j]).setSynchro(sk, b);
  }
  public TGridElement getFirstElement() {
    return gridArray[0][0];
  }
  public TGridElement getLastElement() {
    return gridArray[nbRow - 1][nbCol - 1];
  }
  public TGridModel getModel() {
    return model;
  }
  public Vector getElements() {
    Vector v = new Vector();
    for (int i = 0; i < nbRow; i++)
      for (int j = 0; j < nbCol; j++)
        v.addElement((TGridElement)gridArray[i][j]);
    return v;
  }
  public boolean contains(TGridBlock block) {
    boolean ret = false;
    for (int i = 0; i < nbRow; i++) {
      for (int j = 0; j < nbCol; j++)
        if (gridArray[i][j].equals(block)) {
          ret = true;
          break;
        }
      if (ret) break;
    }
    return ret;
  }
  public void setFitAlgorithm(TFitAlgorithm algo) {

  }
  public TFitAlgorithm getFitAlgorithm() {
    return null;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
