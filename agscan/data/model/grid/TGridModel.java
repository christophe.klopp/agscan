/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TGridControler;
import agscan.data.controler.memento.TGridCellAction;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.data.model.TModelImpl;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.grid.table.TColumn;
import agscan.event.TEvent;
import agscan.menu.action.TGridSelectionModeAction;
import agscan.menu.action.TLevel1SelectionModeAction;
import agscan.menu.action.TLevel2SelectionModeAction;
import agscan.menu.action.TSpotSelectionModeAction;

public class TGridModel extends TModelImpl {
  private TGridConfig config;
  private TGridBlock rootBlock;
  private int nbLevels, sortedColumn, colorizedColumn, selectedColumn;

  /**
   * @link aggregation
   * @undirected
   */
  private TGridSelectionModel selectionModel;
  private int[] sortOrder;
  private int[] rowOrder;
  private boolean sortedInAscendingOrder;


  public TGridModel() {
    config = new TGridConfig();
    rootBlock = new TGridBlock(config, this);
    nbLevels = config.getNbLevels();
    int nbSpots = rootBlock.getNbColumnsInSpots() * rootBlock.getNbRowsInSpots();
    selectionModel = new TGridSelectionModel(nbSpots);
    sortedColumn = colorizedColumn = selectedColumn = 0;
    sortOrder = new int[nbSpots];
    rowOrder = new int[nbSpots];
    sortedInAscendingOrder = true;
    for (int i = 0; i < nbSpots; i++) {
      sortOrder[i] = i;
      rowOrder[i] = i;
    }

  }
  public TGridModel(TGridConfig cfg) {
    config = cfg;
    rootBlock = new TGridBlock(config, this);
    nbLevels = config.getNbLevels();
    int nbSpots = rootBlock.getNbColumnsInSpots() * rootBlock.getNbRowsInSpots();
    selectionModel = new TGridSelectionModel(nbSpots);
    sortedColumn = colorizedColumn = selectedColumn = 0;
    sortOrder = new int[nbSpots];
    rowOrder = new int[nbSpots];
    sortedInAscendingOrder = true;
    for (int i = 0; i < nbSpots; i++) {
      sortOrder[i] = i;
      rowOrder[i] = i;
    }
  }
  public void initialize() {
    rootBlock = new TGridBlock(config, this);
    nbLevels = config.getNbLevels();
    int nbSpots = rootBlock.getNbColumnsInSpots() * rootBlock.getNbRowsInSpots();
    selectionModel = new TGridSelectionModel(nbSpots);
    sortedColumn = colorizedColumn = selectedColumn = 0;
    sortOrder = new int[nbSpots];
    rowOrder = new int[nbSpots];
    sortedInAscendingOrder = true;
    for (int i = 0; i < nbSpots; i++) {
      sortOrder[i] = i;
      rowOrder[i] = i;
    }
    Enumeration enume = getSpots().elements();
    TSpot spot;
    while (enume.hasMoreElements()) {
      spot = (TSpot)enume.nextElement();
      spot.setLeft(getLeftSpot(spot, (TGridBlock)spot.getParent()));
      spot.setRight(getRightSpot(spot, (TGridBlock)spot.getParent()));
      spot.setTop(getTopSpot(spot, (TGridBlock)spot.getParent()));
      spot.setBottom(getBottomSpot(spot, (TGridBlock)spot.getParent()));
      spot.setGlobalLeft(getLeftSpot(spot, getRootBlock()));
      spot.setGlobalRight(getRightSpot(spot, getRootBlock()));
      spot.setGlobalTop(getTopSpot(spot, getRootBlock()));
      spot.setGlobalBottom(getBottomSpot(spot, getRootBlock()));
    }
  }
  public void draw(Graphics2D g2d, boolean drag, boolean scrolled, Rectangle rect, double zoom) {
    rootBlock.draw(g2d, drag, scrolled, rect, zoom);
  }
  public double getWidth() { return rootBlock.getWidth(); }
  public double getHeight() { return rootBlock.getHeight(); }
  public void addPosition(double dx, double dy) { rootBlock.addPosition(dx, dy); }
  public double getAngle() { return rootBlock.getAngleTotal(); }
  public boolean isOnGrid(double x, double y) { return rootBlock.isOnGrid(x * pixelWidth, y * pixelHeight); }
  public TSpot getSpot(double x, double y) { return rootBlock.getSpot(x * pixelWidth, y * pixelHeight); }
  public Vector getSpots(Rectangle rect) { return rootBlock.getSpots(rect); }
  public Vector getSpots() { return rootBlock.getSpots(); }
  public Vector getLineOfSpots(int i) {
    Vector v = new Vector();
    Vector spots = getSpots();
    TSpot spot;
    for (Enumeration enume = spots.elements(); enume.hasMoreElements(); ) {
      spot = (TSpot)enume.nextElement();
      if (spot.getRow() == (i + 1)) v.addElement(spot);
    }
    return v;
  }
  public TGridBlock getGrid(double x, double y) {
    if (isOnGrid(x, y)) return rootBlock;
    return null;
  }
  public TGridBlock getLevel2Element(double x, double y) {
    if (nbLevels == 3) return rootBlock.getLevel2Element(x * pixelWidth, y * pixelHeight);
    return null;
  }
  public TGridBlock getLevel2Element(int r, int c) {
    return rootBlock.getLevel2Element(r, c);
  }
  public Vector getLevel2Elements() {
    return rootBlock.getLevel2Elements();
  }
  public TGridBlock getLevel1Element(double x, double y) {
    if (nbLevels >= 2) return rootBlock.getLevel1Element(x * pixelWidth, y * pixelHeight, nbLevels);
    return null;
  }
  public TGridBlock getLevel1Element(int r, int c) {
    return rootBlock.getLevel1Element(r, c);
  }
  public Vector getLevel1Elements() {
    return rootBlock.getLevel1Elements();
  }
  public TGridBlock getRootBlock() {
    return rootBlock;
  }
  public TGridConfig getConfig() {
    return config;
  }
  public int getElementWidth() {
    double b1w = config.getSpotsWidth() * config.getLev1NbCols();
    double b2w = b1w * config.getLev2NbCols() + (config.getLev2NbCols() - 1) * config.getLev2XSpacing();
//  level3 removed - 20005/10/28
 //   double b3w = b2w * config.getLev3NbCols() + (config.getLev3NbCols() - 1) * config.getLev3XSpacing();
    //return (int)(b3w / pixelWidth);
    return (int)(b2w / pixelWidth);//modif
  }
  public int getElementHeight() {
    double b1h = config.getSpotsHeight() * config.getLev1NbRows();
    double b2h = b1h * config.getLev2NbRows() + (config.getLev2NbRows() - 1) * config.getLev2YSpacing();
//  level3 removed - 20005/10/28
    //double b3h = b2h * config.getLev3NbRows() + (config.getLev3NbRows() - 1) * config.getLev3YSpacing();
    //return (int)(b3h / pixelHeight);
    return (int)(b2h / pixelHeight);//modif
  }
  public void update() {
    rootBlock = new TGridBlock(config, this);
    nbLevels = config.getNbLevels();
    int nbSpots = rootBlock.getNbColumnsInSpots() * rootBlock.getNbRowsInSpots();
    selectionModel.update(nbSpots);
    sortedColumn = 0;
    sortOrder = new int[nbSpots];
    rowOrder = new int[nbSpots];
    sortedInAscendingOrder =  true;
    for (int i = 0; i < nbSpots; i++) {
      sortOrder[i] = i;
      rowOrder[i] = i;
    }
  }
  public TSpot getSpot(int i) {
    return (TSpot)rootBlock.getSpot(i);
  }
  public TSpot getSpot(int row, int col) {
    return (TSpot)rootBlock.getSpot((int)row, (int)col);
  }
  public TSpot getLeftSpot(TSpot spot, TGridBlock gb) {
    TSpot leftSpot = getSpot(spot.getRow(), spot.getCol() - 1);
    if (gb == rootBlock)
      return leftSpot;
    else if (leftSpot != null)
      if (gb.getSpots().contains(leftSpot))
        return leftSpot;
    return null;
  }
  public TSpot getRightSpot(TSpot spot, TGridBlock gb) {
    TSpot rightSpot = getSpot(spot.getRow(), spot.getCol() + 1);
    if (gb == rootBlock)
      return rightSpot;
    else if (rightSpot != null)
      if (gb.getSpots().contains(rightSpot))
        return rightSpot;
    return null;
  }
  public TSpot getTopSpot(TSpot spot, TGridBlock gb) {
    TSpot topSpot = getSpot(spot.getRow() - 1, spot.getCol());
    if (gb == rootBlock)
      return topSpot;
    else if (topSpot != null)
      if (gb.getSpots().contains(topSpot))
        return topSpot;
    return null;
  }
  public TSpot getBottomSpot(TSpot spot, TGridBlock gb) {
    TSpot bottomSpot = getSpot(spot.getRow() + 1, spot.getCol());
    if (gb == rootBlock)
      return bottomSpot;
    else if (bottomSpot != null)
      if (gb.getSpots().contains(bottomSpot))
        return bottomSpot;
    return null;
  }
  public double[] getNorthAnchor(TSpot spot, TGridBlock gb) {
    double[] dbl = { -1, -1 };
    int C = (spot.getCol() - 1) % gb.getNbColumnsInSpots();
    GeneralPath gp = new GeneralPath();
    gp.moveTo(((float)C + 0.5F) * (float)spot.getWidth(), - (float)spot.getHeight() / 2.0F);
    gp.transform(AffineTransform.getRotateInstance(gb.getAngleTotal(), spot.getWidth() / 2, spot.getHeight() / 2));
    gp.getPathIterator(AffineTransform.getTranslateInstance(gb.getX(), gb.getY())).currentSegment(dbl);
    return dbl;
  }
  public double[] getSouthAnchor(TSpot spot, TGridBlock gb) {
    int C = (spot.getCol() - 1) % gb.getNbColumnsInSpots();
    double[] dbl = { -1, -1 };
    GeneralPath gp = new GeneralPath();
    gp.moveTo(((float)C + 0.5F) * (float)spot.getWidth(), (float)spot.getHeight() * ((float)gb.getNbRowsInSpots() + 0.5F));
    gp.transform(AffineTransform.getRotateInstance(gb.getAngleTotal(), spot.getWidth() / 2, spot.getHeight() / 2));
    gp.getPathIterator(AffineTransform.getTranslateInstance(gb.getX(), gb.getY())).currentSegment(dbl);
    return dbl;
  }
  public double[] getEastAnchor(TSpot spot, TGridBlock gb) {
    double[] dbl = { -1, -1 };
    int R = (spot.getRow() - 1) % gb.getNbRowsInSpots();
    GeneralPath gp = new GeneralPath();
    gp.moveTo(((float)spot.getWidth() * ((float)gb.getNbColumnsInSpots() + 0.5F)), (float)spot.getHeight() * ((float)R + 0.5F));
    gp.transform(AffineTransform.getRotateInstance(gb.getAngleTotal(), spot.getWidth() / 2, spot.getHeight() / 2));
    gp.getPathIterator(AffineTransform.getTranslateInstance(gb.getX(), gb.getY())).currentSegment(dbl);
    return dbl;
  }
  public double[] getWestAnchor(TSpot spot, TGridBlock gb) {
    double[] dbl = { -1, -1 };
    int R = (spot.getRow() - 1) % gb.getNbRowsInSpots();
    GeneralPath gp = new GeneralPath();
    gp.moveTo(- (float)spot.getWidth() / 2.0F, (float)spot.getHeight() * ((float)R + 0.5F));
    gp.transform(AffineTransform.getRotateInstance(gb.getAngleTotal(), spot.getWidth() / 2, spot.getHeight() / 2));
    gp.getPathIterator(AffineTransform.getTranslateInstance(gb.getX(), gb.getY())).currentSegment(dbl);
    return dbl;
  }
  

  

  private void clicOnSpot(TSpot sp) {
    TSpot spot = null;
    TGridBlock block = null;
    if (selectionModel.isSelected(sp))
      selectionModel.removeSpot(sp);
    else if (!selectionModel.isSelected( (TGridBlock) sp.getParent()) &&
             !selectionModel.isSelected( (TGridBlock) sp.getParent().getParent()) &&
             !selectionModel.isSelected( (TGridBlock) sp.getParent().getParent().getParent())) {
      selectionModel.addSpot(sp);
      Enumeration enume = ( (TGridBlock) sp.getParent()).getElements().elements();
      boolean allSelected = true;
      while (enume.hasMoreElements())
        allSelected &= selectionModel.isSelected( (TSpot) enume.nextElement());
      if (allSelected) {
        enume = ( (TGridBlock) sp.getParent()).getElements().elements();
        while (enume.hasMoreElements())
          selectionModel.removeSpot( (TSpot) enume.nextElement());
        selectionModel.addBlock( (TGridBlock) sp.getParent());
      }
      enume = ( (TGridBlock) sp.getParent().getParent()).getElements().elements();
      allSelected = true;
      while (enume.hasMoreElements())
        allSelected &= selectionModel.isSelected( (TGridBlock) enume.nextElement());
      if (allSelected) {
        enume = ( (TGridBlock) sp.getParent().getParent()).getElements().elements();
        while (enume.hasMoreElements())
          selectionModel.removeBlock( (TGridBlock) enume.nextElement());
        selectionModel.addBlock( (TGridBlock) sp.getParent().getParent());
      }
      enume = ( (TGridBlock) sp.getParent().getParent().getParent()).getElements().elements();
      allSelected = true;
      while (enume.hasMoreElements())
        allSelected &= selectionModel.isSelected( (TGridBlock) enume.nextElement());
      if (allSelected) {
        enume = ( (TGridBlock) sp.getParent().getParent().getParent()).getElements().elements();
        while (enume.hasMoreElements())
          selectionModel.removeBlock( (TGridBlock) enume.nextElement());
        selectionModel.addBlock( (TGridBlock) sp.getParent().getParent().getParent());
      }
    }
    else if (selectionModel.isSelected( (TGridBlock) sp.getParent())) {
      selectionModel.removeBlock( (TGridBlock) sp.getParent());
      Enumeration enume = ( (TGridBlock) sp.getParent()).getElements().elements();
      while (enume.hasMoreElements()) {
        spot = (TSpot) enume.nextElement();
        if (!spot.equals(sp))
          selectionModel.addSpot(spot);
      }
    }
    else if (selectionModel.isSelected( (TGridBlock) sp.getParent().getParent())) {
      selectionModel.removeBlock( (TGridBlock) sp.getParent().getParent());
      Enumeration enume = ( (TGridBlock) sp.getParent().getParent()).getElements().elements();
      TGridBlock block2 = null;
      while (enume.hasMoreElements()) {
        block = (TGridBlock) enume.nextElement();
        if (!block.equals(sp.getParent()))
          selectionModel.addBlock(block);
        else
          block2 = block;
      }
      enume = block2.getElements().elements();
      while (enume.hasMoreElements()) {
        spot = (TSpot) enume.nextElement();
        if (!spot.equals(sp))
          selectionModel.addSpot(spot);
      }
    }
    else if (selectionModel.isSelected( (TGridBlock) sp.getParent().getParent().getParent())) {
      selectionModel.removeBlock( (TGridBlock) sp.getParent().getParent().getParent());
      Enumeration enume = ( (TGridBlock) sp.getParent().getParent().getParent()).getElements().elements();
      TGridBlock block2 = null;
      while (enume.hasMoreElements()) {
        block = (TGridBlock) enume.nextElement();
        if (!block.equals(sp.getParent().getParent()))
          selectionModel.addBlock(block);
        else
          block2 = block;
      }
      enume = block2.getElements().elements();
      while (enume.hasMoreElements()) {
        block = (TGridBlock) enume.nextElement();
        if (!block.equals(sp.getParent()))
          selectionModel.addBlock(block);
        else
          block2 = block;
      }
      enume = block2.getElements().elements();
      while (enume.hasMoreElements()) {
        spot = (TSpot) enume.nextElement();
        if (!spot.equals(sp))
          selectionModel.addSpot(spot);
      }
    }
  }
  public TGridElement toggleSelect(Point p, int mode, boolean ctrl) {
    TGridBlock gb = null, block;
    TSpot sp = null;
    if (mode == TSpotSelectionModeAction.getID()) {//TMenuManager.SPOT_SELECTION_MODE avant
      sp = getSpot(p.x, p.y);
      if (sp != null) {
        clicOnSpot(sp);
        return sp;
      }
    }
    else if (mode == TLevel1SelectionModeAction.getID()) {//TMenuManager.LEVEL1_SELECTION_MODE avant
      gb = getLevel1Element((double)p.x, (double)p.y);
      if (gb != null) {
        if (selectionModel.isSelected(gb))
          selectionModel.removeBlock(gb);
        else if (!selectionModel.isSelected( (TGridBlock) gb.getParent()) && !selectionModel.isSelected( (TGridBlock) gb.getParent().getParent())) {
          selectionModel.addBlock(gb);
          Enumeration enume = ((TGridBlock)gb.getParent()).getElements().elements();
          boolean allSelected = true;
          while (enume.hasMoreElements())
            allSelected &= selectionModel.isSelected( (TGridBlock) enume.nextElement());
          if (allSelected) {
            enume = ( (TGridBlock) gb.getParent()).getElements().elements();
            while (enume.hasMoreElements())
              selectionModel.removeBlock( (TGridBlock) enume.nextElement());
            selectionModel.addBlock( (TGridBlock) gb.getParent());
          }
          enume = ( (TGridBlock) gb.getParent().getParent()).getElements().elements();
          allSelected = true;
          while (enume.hasMoreElements())
            allSelected &= selectionModel.isSelected( (TGridBlock) enume.nextElement());
          if (allSelected) {
            enume = ( (TGridBlock) gb.getParent().getParent()).getElements().elements();
            while (enume.hasMoreElements())
              selectionModel.removeBlock( (TGridBlock) enume.nextElement());
            selectionModel.addBlock( (TGridBlock) gb.getParent().getParent());
          }
        }
        else if (selectionModel.isSelected( (TGridBlock) gb.getParent())) {
          selectionModel.removeBlock( (TGridBlock) gb.getParent());
          Enumeration enume = ( (TGridBlock) gb.getParent()).getElements().elements();
          while (enume.hasMoreElements()) {
            block = (TGridBlock) enume.nextElement();
            if (!block.equals(gb))
              selectionModel.addBlock(block);
          }
        }
        else if (selectionModel.isSelected( (TGridBlock) gb.getParent().getParent())) {
          selectionModel.removeBlock( (TGridBlock) gb.getParent().getParent());
          Enumeration enume = ( (TGridBlock) gb.getParent().getParent()).getElements().elements();
          TGridBlock block2 = null;
          while (enume.hasMoreElements()) {
            block = (TGridBlock) enume.nextElement();
            if (!block.contains(gb))
              selectionModel.addBlock(block);
            else
              block2 = block;
          }
          enume = block2.getElements().elements();
          while (enume.hasMoreElements()) {
            block = (TGridBlock) enume.nextElement();
            if (!block.equals(gb))
              selectionModel.addBlock(block);
          }
        }
      }
    }
    else if (mode == TLevel2SelectionModeAction.getID()) {//TMenuManager.LEVEL2_SELECTION_MODE avant
      gb = getLevel2Element((double)p.x, (double)p.y);
      if (gb != null) {
        if (selectionModel.isSelected(gb))
          selectionModel.removeBlock(gb);
        else if (!selectionModel.isSelected((TGridBlock)gb.getParent())) {
          selectionModel.addBlock(gb);
          Enumeration enume = ((TGridBlock)gb.getParent()).getElements().elements();
          boolean allSelected = true;
          while (enume.hasMoreElements()) allSelected &= selectionModel.isSelected((TGridBlock)enume.nextElement());
          if (allSelected) {
            enume = ((TGridBlock)gb.getParent()).getElements().elements();
            while (enume.hasMoreElements()) selectionModel.removeBlock((TGridBlock)enume.nextElement());
            selectionModel.addBlock((TGridBlock)gb.getParent());
          }
        }
        else {
          selectionModel.removeBlock((TGridBlock)gb.getParent());
          Enumeration enume = ((TGridBlock)gb.getParent()).getElements().elements();
          while (enume.hasMoreElements()) {
            block = (TGridBlock)enume.nextElement();
            if (!block.equals(gb)) selectionModel.addBlock(block);
          }
        }
      }
    }
    else if (mode == TGridSelectionModeAction.getID()) {//TMenuManager.GRID_SELECTION_MODE avant
      if (isOnGrid(p.x, p.y)) {
        if (selectionModel.isSelected(rootBlock))
          selectionModel.clearAll();
        else {
          selectionModel.clearAll();
          selectionModel.addBlock(rootBlock);
        }
      }
    }
    else
      selectionModel.clearAll();
    return gb;
  }
  public Vector toggleSelect(Rectangle rect, int mode) {
    rect.x *= pixelWidth;
    rect.y *= pixelHeight;
    rect.width *= pixelWidth;
    rect.height *= pixelHeight;
    Vector v = getSpots(rect);
    if (v != null) {
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); )
        clicOnSpot((TSpot)enume.nextElement());
    }
    return v;
  }
  public void preselect(Rectangle rect) {
    rect.x *= pixelWidth;
    rect.y *= pixelHeight;
    rect.width *= pixelWidth;
    rect.height *= pixelHeight;
    Vector v = getSpots(rect);
    selectionModel.preselect(v);
  }
  public void select(Rectangle rect) {
    Vector v = getSpots(rect);
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); )
      selectionModel.addSpot((TSpot)enume.nextElement());
  }
  public void select(int i) {
    TSpot spot = (TSpot)rootBlock.getSpot(i);
    if (!selectionModel.isRecursivelySelected(spot)) clicOnSpot(spot);
    //selectionModel.addSpot(spot);
  }
  public void unselect(int i) {
    TSpot spot = (TSpot)rootBlock.getSpot(i);
    if (selectionModel.isRecursivelySelected(spot)) clicOnSpot(spot);
    //selectionModel.removeSpot(spot);
  }
  public boolean isSelected(int x, int y) {
    TSpot spot = getSpot(x, y);
    if (spot == null) return false;
    return selectionModel.isSelected(spot);
  }
  public void unselectAll() {
    selectionModel.clearAll();
  }
  public void clearPreSelection() {
    Vector v = new Vector(selectionModel.getPreSelection());
    selectionModel.clearPreSelection();
  }
  public TGridSelectionModel getSelectionModel() {
    return selectionModel;
  }
  public void save(File file) {
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");//modif remi remis 2005/11/16 pb open XP
     // bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");//modif remi le 22/08/05 pour le
      bw.newLine();
      bw.write("<grid>");
      bw.newLine();
      saveGrid(bw, 2);
      bw.write("</grid>");
      bw.newLine();
      bw.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public void exportAsText(File file) {
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      // export header file modification - 2006/03/20
      // add of several data for BASE
      bw.write("\"Creator="+Messages.getString("FenetrePrincipale.0")+"\"");//2005/11/23 version added into .txt export file
      bw.newLine();
      bw.write("\"File="+file.getName()+"\"");
      bw.newLine();     
      int pixelSize = (int)pixelWidth;
      bw.write("\"PixelSize="+pixelSize+"\"");// size of a pixel in micrometer (notice that only one dimension is done, by default width...)
      bw.newLine();
      bw.write("\"ImageOrigin=0,0\"");//default image origin
      bw.newLine();
      
      TEvent event = null;
      event = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
      TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(event)[0]; 
      TControlerImpl c = currentElement.getControler();
      int channelNb = 0;
      
      System.out.println("exportAsText ");
      
      if (currentElement instanceof TAlignment){
      	TAlignment ali =(TAlignment)currentElement; 
      	channelNb = ali.getImage().getImageModel().getImages().getSize();//number of channels. radio = 1
      }
      if (currentElement instanceof TBatch){
    	  TBatch batch = (TBatch)currentElement;
          channelNb = ((TBatchModelNImages)batch.getModel()).getColumnCount();   // .getImages((int)0).size();
          System.out.println("channelNb = "+channelNb);
        		  //getModel()).getImages().getSize();
          //batch.getImage().getImageModel().getImages().getSize();//number of channels. radio = 1
    	  //channelNb = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");
        }
      if (channelNb==1){
      	bw.write("\"SpotImageSize=10\"");// 10 for BASE spot images creation
      	bw.newLine();
      }
      
      
      Enumeration spots = getSpots().elements();
      TSpot spot;
      Enumeration cols = getConfig().getColumns().getColumns();
      TColumn column;
      String s;
      while (cols.hasMoreElements()) {
        column = (TColumn) cols.nextElement();
        if (column.isVisible()) {
          s = column.getHeaderValue().toString();
          if (s.replaceAll(" ", "").equals(""))
            s = "Index";
          bw.write(s + "\t");
        }
      }
      bw.write("Qualite");
      bw.newLine();
      while (spots.hasMoreElements()) {
        spot = (TSpot)spots.nextElement();
        spot.exportAsText(bw, this);
      }
      bw.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public int[] getIntSerie(String key) {
    Enumeration spots = getSpots().elements();
    TSpot spot;
    int[] v = new int[getSpots().size()];
    int k = 0;
    while (spots.hasMoreElements()) {
      spot = (TSpot)spots.nextElement();
      if (key.equals("RG"))
        v[k] = spot.getQuality();
      else
        v[k] = ((Integer)spot.getParameter(key)).intValue();
      k++;
    }
    return v;
  }
  public void saveGrid(BufferedWriter bw, int indent) throws java.io.IOException {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    config.saveStructure(bw, indent);
    config.saveShift(bw,indent);
    config.saveColumns(bw, indent);
    Enumeration spots = getSpots().elements();
    TSpot spot;
    bw.write(base + "<modifiedValues>");
    bw.newLine();
    while (spots.hasMoreElements()) {
      spot = (TSpot)spots.nextElement();
      spot.save(bw, indent + 2, config.getColumns());
    }
    bw.write(base + "</modifiedValues>");
    bw.newLine();
  }
  public int getRowCount() {
    return rootBlock.getNbRowsInSpots() * rootBlock.getNbColumnsInSpots();
  }
  public int getColumnCount() {
    return config.getColumns().getColumnCount();
  }
  public Object getValueAt(int rowIndex, int columnIndex) {
    rowIndex = sortOrder[rowIndex];
    if (columnIndex == 0) return new Integer(rowIndex + 1);
    TSpot spot = (TSpot)rootBlock.getSpot(rowIndex);
    return spot.getParameter(((TColumn)config.getColumns().getColumn(columnIndex)).getSpotKey());
  }
  public void notifyView() {
    /**@todo Implementer cette methode image.data.model.TModelImpl abstract*/
  }
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return ((TColumn)config.getColumns().getColumn(columnIndex)).isEditable();
  }
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    rowIndex = sortOrder[rowIndex];
    TSpot spot = (TSpot)rootBlock.getSpot(rowIndex);
    String sk = ((TColumn)config.getColumns().getColumn(columnIndex)).getSpotKey();
    if (!aValue.equals(spot.getParameter(sk))) {
      TGridCellAction gca = new TGridCellAction(spot.getParameter(sk), aValue, columnIndex, rowIndex);
      TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, null, gca);
      TEventHandler.handleMessage(event);
      spot.addParameter(sk, aValue, true);
      event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REPAINT_VIEW, null);
      TEventHandler.handleMessage(event);
    }
  }
  public void addParameter(String sk, Object dv, boolean syn) {
    rootBlock.addParameter(sk, dv, syn);
  }
  public void removeParameter(String sk) {
    rootBlock.removeParameter(sk);
  }
  public int getSortedColumn() { return sortedColumn; }
  public int getColorizedColumn() { return colorizedColumn; }
  public int getSelectedColumn() { return selectedColumn; }
  public boolean isSortedInAscendingOrder() { return sortedInAscendingOrder; }
  public void unsort() {
    int nbSpots = rootBlock.getNbColumnsInSpots() * rootBlock.getNbRowsInSpots();
    sortedColumn = 0;
    sortOrder = new int[nbSpots];
    rowOrder = new int[nbSpots];
    sortedInAscendingOrder = true;
    for (int i = 0; i < nbSpots; i++) {
      sortOrder[i] = i;
      rowOrder[i] = i;
    }
  }
  public void sortAscending(TColumn col) {
    Vector v;
    Object value;
    Hashtable hash = new Hashtable();
    sortedColumn = col.getModelIndex();
    sortedInAscendingOrder = true;
    for (int j = 0; j < getRowCount(); j++) {
      if (!hash.containsKey(getValueAt(j, sortedColumn)))
        v = new Vector();
      else
        v = (Vector)hash.get(getValueAt(j, sortedColumn));
      v.addElement(new Integer(sortOrder[j]));
      hash.put(getValueAt(j, sortedColumn), v);
    }
    TreeMap tm = new TreeMap(hash);
    Iterator itValue = tm.keySet().iterator();
    Iterator itLine = tm.values().iterator();
    int i = 0;
    Integer I;
    while (itLine.hasNext()) {
      value = itValue.next();
      v = (Vector)itLine.next();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); i++) {
        I = (Integer)enume.nextElement();
        sortOrder[i] = I.intValue();
        rowOrder[I.intValue()] = i;
      }
    }
  }
  public void sortDescending(TColumn col) {
    Vector v;
    Object value;
    Hashtable hash = new Hashtable();
    sortedColumn = col.getModelIndex();
    sortedInAscendingOrder = false;
    for (int j = 0; j < getRowCount(); j++) {
      if (!hash.containsKey(getValueAt(j, sortedColumn)))
        v = new Vector();
      else
        v = (Vector)hash.get(getValueAt(j, sortedColumn));
      v.addElement(new Integer(sortOrder[j]));
      hash.put(getValueAt(j, sortedColumn), v);
    }
    TreeMap tm = new TreeMap(hash);
    Iterator itValue = tm.keySet().iterator();
    Iterator itLine = tm.values().iterator();
    int i = getRowCount() - 1;
    Integer I;
    while (itLine.hasNext()) {
      value = itValue.next();
      v = (Vector)itLine.next();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); i--) {
        I = (Integer)enume.nextElement();
        sortOrder[i] = I.intValue();
        rowOrder[I.intValue()] = i;
      }
    }
  }
  public int getSpotIndex(int row) {
    return sortOrder[row];
  }
  public int getSpotRow(int index) {
    return rowOrder[index];
  }
  public void colorizeColumn(TColumn col) {
    colorizedColumn = col.getModelIndex();
    getConfig().setColorizedColumn(colorizedColumn);
  }
  public void decolorize() {
    colorizedColumn = 0;
    getConfig().setColorizedColumn(colorizedColumn);
  }
  public void selectColumn(int col) {
    selectedColumn = col;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
