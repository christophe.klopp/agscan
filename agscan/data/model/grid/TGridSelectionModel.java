package agscan.data.model.grid;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Vector;

public class TGridSelectionModel {
  private Vector spotSelection, preSelection, blockSelection;
  private BitSet bitSet;

  public TGridSelectionModel(int n) {
    spotSelection = new Vector();
    preSelection = new Vector();
    blockSelection = new Vector();
    bitSet = new BitSet(n);
  }
  public void update(int n) {
    clearAll();
    bitSet = new BitSet(n);
  }
  public void addSpot(TSpot spot) {
    spotSelection.addElement(spot);
    bitSet.set(spot.getIndex(), true);
  }
  public void addBlock(TGridBlock block) {
    blockSelection.addElement(block);
    Enumeration enume = block.getSpots().elements();
    while (enume.hasMoreElements()) bitSet.set(((TSpot)enume.nextElement()).getIndex(), true);
  }
  public void removeSpot(TSpot spot) {
    spotSelection.removeElement(spot);
    bitSet.set(spot.getIndex(), false);
  }
  public void removeBlock(TGridBlock block) {
    blockSelection.removeElement(block);
    Enumeration enume = block.getSpots().elements();
    while (enume.hasMoreElements()) bitSet.set(((TSpot)enume.nextElement()).getIndex(), false);
  }
  public void toggleInterval(Vector v) {
    TSpot spot = (TSpot)v.firstElement();
    int first = spot.getIndex();
    spot = (TSpot)v.lastElement();
    int last = spot.getIndex();
    if ((last - first + 1) == v.size())
      bitSet.flip(first, last + 1);
    else
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        spot = (TSpot)enume.nextElement();
        bitSet.flip(spot.getIndex());
      }
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
      spot = (TSpot)enume.nextElement();
      if (spotSelection.contains(spot))
        spotSelection.removeElement(spot);
      else
        spotSelection.addElement(spot);
    }
  }
  public void toggleSpot(TSpot spot) {
    bitSet.flip(spot.getIndex());
    if (spotSelection.contains(spot))
      spotSelection.removeElement(spot);
    else
      spotSelection.addElement(spot);
  }
  public void toggleBlock(TGridBlock block) {
    if (blockSelection.contains(block))
      blockSelection.removeElement(block);
    else
      blockSelection.addElement(block);
  }
  public void clearPreSelection() {
    preSelection.clear();
  }
  public void clearSpotSelection() {
    spotSelection.clear();
  }
  public void clearAll() {
    spotSelection.clear();
    preSelection.clear();
    blockSelection.clear();
    bitSet.clear();
  }
  public void preselect(TSpot spot) {
    preSelection.addElement(spot);
  }
  public void preselect(Vector v) {
    preSelection.addAll(v);
  }
  public Vector getSelectedSpots() {
    return spotSelection;
  }
  public Vector getRecursivelySelectedSpots() {
    Vector v = new Vector(spotSelection);
    Enumeration enume = blockSelection.elements();
    while (enume.hasMoreElements()) v.addAll(((TGridBlock)enume.nextElement()).getSpots());
    return v;
  }
  public Vector getPreSelection() {
    return preSelection;
  }
  public Vector getSelection() {
    Vector v = new Vector(blockSelection);
    v.addAll(spotSelection);
    return v;
  }
  public Vector getBlockSelection() {
    return (Vector)blockSelection.clone();
  }
  public Vector getBlock1Selection() {
    Enumeration enume = blockSelection.elements();
    Vector b1Sel = new Vector();
    TGridBlock bl;
    while (enume.hasMoreElements()) {
      bl = (TGridBlock)enume.nextElement();
      if (bl.getFirstElement() instanceof TSpot)
        b1Sel.addElement(bl);
      else
        b1Sel.addAll(bl.getLevel1Elements());
    }
    return b1Sel;
  }
  public boolean isRecursivelySelected(TGridBlock block) {
    if (block.getParent() != null)
      return isSelected((TGridBlock)block.getParent()) || blockSelection.contains(block);
    return blockSelection.contains(block);
  }
  public boolean isSelected(TGridBlock block) {
    return blockSelection.contains(block);
  }
  public boolean isRecursivelySelected(TSpot spot) {
    return (bitSet.get(spot.getIndex()) && !preSelection.contains(spot)) ||
           (!bitSet.get(spot.getIndex()) && preSelection.contains(spot));
  }
  public boolean isSelected(TSpot spot) {
    return spotSelection.contains(spot);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
