/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid.table;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

import agscan.dialog.TSpinnerDoubleModel;

public class TDoubleEditor extends AbstractCellEditor implements ChangeListener, TableCellEditor {
  private Double value;
  private JSpinner spinner;
  protected int clickCountToStart = 2;

  public TDoubleEditor(TDoubleColumn column) {
    super();
    value = new Double(0);
    spinner = new JSpinner(new TSpinnerDoubleModel(((Double)column.getDefaultValue()).doubleValue(), -Double.MAX_VALUE, Double.MAX_VALUE, 1.0D, 100000000));
    spinner.setEditor(new JSpinner.NumberEditor(spinner, "########.########"));
    spinner.addChangeListener(this);
  }
  public TDoubleEditor(TProbaColumn column) {
    super();
    value = new Double(0);
    spinner = new JSpinner(new TSpinnerDoubleModel(((Double)column.getDefaultValue()).doubleValue(), 0.0D, 1.0D, 0.01D, 100000000));
    spinner.setEditor(new JSpinner.NumberEditor(spinner, "########.########"));
    spinner.addChangeListener(this);
  }
  public Object getCellEditorValue() {
    return value;
  }
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    this.value = (Double)value;
    spinner.setValue(this.value);
    return spinner;
  }
  public void stateChanged(ChangeEvent event) {
    value = (Double)spinner.getValue();
  }
  public boolean isCellEditable(EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      return ((MouseEvent)anEvent).getClickCount() >= clickCountToStart;
    }
    return true;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
