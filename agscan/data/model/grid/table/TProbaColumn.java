package agscan.data.model.grid.table;

import java.awt.Color;
import java.io.BufferedWriter;
import java.util.Vector;
import java.util.zip.DataFormatException;

import agscan.data.controler.memento.TColumnState;
import agscan.menu.columnmenubuilder.TProbaColumnMenuBuilder;

public class TProbaColumn extends TColumn {
  private Color infColor, betweenColor, supColor;
  private Double supValue, betweenValue1, betweenValue2, infValue;
  private Double def;
  public TProbaColumn(int index, String name, String sk, boolean ed) {
    super(index);
    setHeaderValue(name);
    menuBuilder = TProbaColumnMenuBuilder.getInstance();
    spotKey = sk;
    editable = ed;
    infColor = betweenColor = supColor = Color.white;
    def = new Double(0.0D);
    setCellRenderer(new TProbaRenderer());
    setCellEditor(new TDoubleEditor(this));
  }
  public Color getInfColor() { return infColor; }
  public Color getBetweenColor() { return betweenColor; }
  public Color getSupColor() { return supColor; }
  public void setInfColor(Color c) { infColor = c; }
  public void setBetweenColor(Color c) { betweenColor = c; }
  public void setSupColor(Color c) { supColor = c; }
  public Object getDefaultValue() { return def; }
  public String getDefaultValueString() { return def.toString(); }
  public void setDefaultValue(Double dv) { def = dv; }
  public int getType() { return TColumn.TYPE_PROBA; }
  public Object getSupValue() { return supValue; }
  public Object getBetweenValue1() { return betweenValue1; }
  public Object getBetweenValue2() { return betweenValue2; }
  public Object getInfValue() { return infValue; }
  public void setSupValue(Double d) { supValue = d; }
  public void setBetweenValue1(Double d) { betweenValue1 = d; }
  public void setBetweenValue2(Double d) { betweenValue2 = d; }
  public void setInfValue(Double d) { infValue = d; }
  public boolean isCompatible(String[] tab) {
    double d;
    for (int k = 0; k < tab.length; k++)
      try {
        d = Double.parseDouble(tab[k]);
        if ((d < 0) || (d > 1)) return false;
      }
      catch (NumberFormatException ex) {
        return false;
      }
    return true;
  }
  public Object[] getCompatibleValuesInBuffer(String[] tab) {
    Double[] values = new Double[tab.length];
    for (int k = 0; k < tab.length; k++) values[k] = Double.valueOf(tab[k]);
    return values;
  }
  public static Vector getParams(TColumnState cs) {
    Vector params = new Vector();
    params.addElement(cs.getName());
    params.addElement(cs.getSpotKey());
    params.addElement(cs.getDefaultValue());
    params.addElement(new Boolean(cs.getInf()));
    params.addElement(cs.getInfColor());
    params.addElement(cs.getInfValue());
    params.addElement(new Boolean(cs.getBetween()));
    params.addElement(cs.getBetweenColor());
    params.addElement(cs.getBetweenValue1());
    params.addElement(cs.getBetweenValue2());
    params.addElement(new Boolean(cs.getSup()));
    params.addElement(cs.getSupColor());
    params.addElement(cs.getSupValue());
    return params;
  }
  public void save(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    String ind = "  ";
    try {
      bw.write(base + "<probaColumn>");
      bw.newLine();
      bw.write(base + ind + "<name>" + toString() + "</name>");
      bw.newLine();
      bw.write(base + ind + "<defaultValue>" + def + "</defaultValue>");
      bw.newLine();
      bw.write(base + ind + "<spotKey>" + spotKey + "</spotKey>");
      bw.newLine();
      bw.write(base + ind + "<editable>" + editable + "</editable>");
      bw.newLine();
      bw.write(base + ind + "<removable>" + removable + "</removable>");
      bw.newLine();
      bw.write(base + ind + "<colors>");
      bw.newLine();
      bw.write(base + ind + ind + "<inf>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<used>" + booleanInfColor + "</used>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<value>" + infValue + "</value>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<color>" + infColor.getRed() + "," + infColor.getGreen() + "," + infColor.getBlue() + "</color>");
      bw.newLine();
      bw.write(base + ind + ind + "</inf>");
      bw.newLine();
      bw.write(base + ind + ind + "<between>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<used>" + booleanBetweenColor + "</used>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<value1>" + betweenValue1 + "</value1>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<value2>" + betweenValue2 + "</value2>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<color>" + betweenColor.getRed() + "," + betweenColor.getGreen() + "," + betweenColor.getBlue() + "</color>");
      bw.newLine();
      bw.write(base + ind + ind + "</between>");
      bw.newLine();
      bw.write(base + ind + ind + "<sup>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<used>" + booleanSupColor + "</used>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<value>" + supValue + "</value>");
      bw.newLine();
      bw.write(base + ind + ind + ind + "<color>" + supColor.getRed() + "," + supColor.getGreen() + "," + supColor.getBlue() + "</color>");
      bw.newLine();
      bw.write(base + ind + ind + "</sup>");
      bw.newLine();
      bw.write(base + ind + "</colors>");
      bw.newLine();
      bw.write(base + "</probaColumn>");
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public static Object makeValue(String text, Vector values) throws NumberFormatException, DataFormatException {
    Object defaultValue = Double.valueOf(text);
    if ((((Double)defaultValue).doubleValue() > 1.0D) || (((Double)defaultValue).doubleValue() < 0.0D)) {
      defaultValue = null;
      throw new NumberFormatException();
    }
    return defaultValue;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
