/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid.table.factory;

import java.awt.Color;
import java.util.Vector;

import agscan.data.model.grid.table.TBooleanColumn;
import agscan.data.model.grid.table.TColumn;
import agscan.data.model.grid.table.TDoubleColumn;
import agscan.data.model.grid.table.TDoubleListColumn;
import agscan.data.model.grid.table.TIntegerColumn;
import agscan.data.model.grid.table.TIntegerListColumn;
import agscan.data.model.grid.table.TProbaColumn;
import agscan.data.model.grid.table.TStringColumn;
import agscan.data.model.grid.table.TStringListColumn;
import agscan.data.model.grid.table.TURLColumn;

public class TColumnFactory {
  public TColumnFactory() {
  }
  public static TColumn createColumn(int type, Vector params) {
    TColumn column = null;
    TColumnBuilder builder;
    switch (type) {
      case TColumn.TYPE_INTEGER:
        builder = new TIntegerColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TIntegerColumnBuilder)builder).setDefaultValue((Integer)params.elementAt(2));
        ((TIntegerColumnBuilder)builder).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TIntegerColumnBuilder)builder).setInfColor((Color)params.elementAt(4));
        ((TIntegerColumnBuilder)builder).setInfValue((Integer)params.elementAt(5));
        ((TIntegerColumnBuilder)builder).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TIntegerColumnBuilder)builder).setBetweenColor((Color)params.elementAt(7));
        ((TIntegerColumnBuilder)builder).setBetweenValue1((Integer)params.elementAt(8));
        ((TIntegerColumnBuilder)builder).setBetweenValue2((Integer)params.elementAt(9));
        ((TIntegerColumnBuilder)builder).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TIntegerColumnBuilder)builder).setSupColor((Color)params.elementAt(11));
        ((TIntegerColumnBuilder)builder).setSupValue((Integer)params.elementAt(12));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_REAL:
        builder = new TDoubleColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TDoubleColumnBuilder)builder).setDefaultValue((Double)params.elementAt(2));
        ((TDoubleColumnBuilder)builder).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TDoubleColumnBuilder)builder).setInfColor((Color)params.elementAt(4));
        ((TDoubleColumnBuilder)builder).setInfValue((Double)params.elementAt(5));
        ((TDoubleColumnBuilder)builder).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TDoubleColumnBuilder)builder).setBetweenColor((Color)params.elementAt(7));
        ((TDoubleColumnBuilder)builder).setBetweenValue1((Double)params.elementAt(8));
        ((TDoubleColumnBuilder)builder).setBetweenValue2((Double)params.elementAt(9));
        ((TDoubleColumnBuilder)builder).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TDoubleColumnBuilder)builder).setSupColor((Color)params.elementAt(11));
        ((TDoubleColumnBuilder)builder).setSupValue((Double)params.elementAt(12));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_TEXT:
        builder = new TStringColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TStringColumnBuilder)builder).setDefaultValue((String)params.elementAt(2));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_BOOLEAN:
        builder = new TBooleanColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TBooleanColumnBuilder)builder).setDefaultValue((Boolean)params.elementAt(2));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_PROBA:
        builder = new TProbaColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TProbaColumnBuilder)builder).setDefaultValue((Double)params.elementAt(2));
        ((TProbaColumnBuilder)builder).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TProbaColumnBuilder)builder).setInfColor((Color)params.elementAt(4));
        ((TProbaColumnBuilder)builder).setInfValue((Double)params.elementAt(5));
        ((TProbaColumnBuilder)builder).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TProbaColumnBuilder)builder).setBetweenColor((Color)params.elementAt(7));
        ((TProbaColumnBuilder)builder).setBetweenValue1((Double)params.elementAt(8));
        ((TProbaColumnBuilder)builder).setBetweenValue2((Double)params.elementAt(9));
        ((TProbaColumnBuilder)builder).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TProbaColumnBuilder)builder).setSupColor((Color)params.elementAt(11));
        ((TProbaColumnBuilder)builder).setSupValue((Double)params.elementAt(12));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_URL:
        builder = new TURLColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TURLColumnBuilder)builder).setDefaultValue((Vector)params.elementAt(2));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_INTEGER_LIST:
        builder = new TIntegerListColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TIntegerListColumnBuilder)builder).setValues((Vector)params.elementAt(3));
        ((TIntegerListColumnBuilder)builder).setDefaultValue((Integer)params.elementAt(2));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_REAL_LIST:
        builder = new TDoubleListColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TDoubleListColumnBuilder)builder).setValues((Vector)params.elementAt(3));
        ((TDoubleListColumnBuilder)builder).setDefaultValue((Double)params.elementAt(2));
        column = builder.getColumn();
        break;
      case TColumn.TYPE_TEXT_LIST:
        builder = new TStringListColumnBuilder();
        builder.createColumn((String)params.firstElement(), (String)params.elementAt(1));
        ((TStringListColumnBuilder)builder).setValues((Vector)params.elementAt(3));
        ((TStringListColumnBuilder)builder).setDefaultValue((String)params.elementAt(2));
        column = builder.getColumn();
        break;
    }
    column.setEditable(true);
    column.setRemovable(true);
    column.setVisible(true);
    return column;
  }
  public static void refreshColumn(TColumn column, Vector params) {
    int type = column.getType();
    switch (type) {
      case TColumn.TYPE_INTEGER:
        ((TIntegerColumn)column).setHeaderValue(params.firstElement());
        ((TIntegerColumn)column).setDefaultValue((Integer)params.elementAt(2));
        ((TIntegerColumn)column).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TIntegerColumn)column).setInfColor((Color)params.elementAt(4));
        ((TIntegerColumn)column).setInfValue((Integer)params.elementAt(5));
        ((TIntegerColumn)column).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TIntegerColumn)column).setBetweenColor((Color)params.elementAt(7));
        ((TIntegerColumn)column).setBetweenValue1((Integer)params.elementAt(8));
        ((TIntegerColumn)column).setBetweenValue2((Integer)params.elementAt(9));
        ((TIntegerColumn)column).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TIntegerColumn)column).setSupColor((Color)params.elementAt(11));
        ((TIntegerColumn)column).setSupValue((Integer)params.elementAt(12));

        break;
      case TColumn.TYPE_REAL:
        ((TDoubleColumn)column).setHeaderValue(params.firstElement());
        ((TDoubleColumn)column).setDefaultValue((Double)params.elementAt(2));
        ((TDoubleColumn)column).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TDoubleColumn)column).setInfColor((Color)params.elementAt(4));
        ((TDoubleColumn)column).setInfValue((Double)params.elementAt(5));
        ((TDoubleColumn)column).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TDoubleColumn)column).setBetweenColor((Color)params.elementAt(7));
        ((TDoubleColumn)column).setBetweenValue1((Double)params.elementAt(8));
        ((TDoubleColumn)column).setBetweenValue2((Double)params.elementAt(9));
        ((TDoubleColumn)column).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TDoubleColumn)column).setSupColor((Color)params.elementAt(11));
        ((TDoubleColumn)column).setSupValue((Double)params.elementAt(12));

        break;
      case TColumn.TYPE_TEXT:
        ((TStringColumn)column).setHeaderValue(params.firstElement());
        ((TStringColumn)column).setDefaultValue((String)params.elementAt(2));
        break;
      case TColumn.TYPE_BOOLEAN:
        ((TBooleanColumn)column).setHeaderValue(params.firstElement());
        ((TBooleanColumn)column).setDefaultValue((Boolean)params.elementAt(2));
        break;
      case TColumn.TYPE_PROBA:
        ((TProbaColumn)column).setHeaderValue(params.firstElement());
        ((TProbaColumn)column).setBooleanInfColor(((Boolean)params.elementAt(3)).booleanValue());
        ((TProbaColumn)column).setInfColor((Color)params.elementAt(4));
        ((TProbaColumn)column).setInfValue((Double)params.elementAt(5));
        ((TProbaColumn)column).setBooleanBetweenColor(((Boolean)params.elementAt(6)).booleanValue());
        ((TProbaColumn)column).setBetweenColor((Color)params.elementAt(7));
        ((TProbaColumn)column).setBetweenValue1((Double)params.elementAt(8));
        ((TProbaColumn)column).setBetweenValue2((Double)params.elementAt(9));
        ((TProbaColumn)column).setBooleanSupColor(((Boolean)params.elementAt(10)).booleanValue());
        ((TProbaColumn)column).setSupColor((Color)params.elementAt(11));
        ((TProbaColumn)column).setSupValue((Double)params.elementAt(12));
        break;
      case TColumn.TYPE_URL:
        ((TURLColumn)column).setHeaderValue(params.firstElement());
        ((TURLColumn)column).setDefaultValue((Vector)params.elementAt(2));
        break;
      case TColumn.TYPE_INTEGER_LIST:
        ((TIntegerListColumn)column).setHeaderValue(params.firstElement());
        ((TIntegerListColumn)column).setValues((Vector)params.elementAt(3));
        ((TIntegerListColumn)column).setDefaultValue((Integer)params.elementAt(2));
        break;
      case TColumn.TYPE_REAL_LIST:
        ((TDoubleListColumn)column).setHeaderValue(params.firstElement());
        ((TDoubleListColumn)column).setValues((Vector)params.elementAt(3));
        ((TDoubleListColumn)column).setDefaultValue((Double)params.elementAt(2));
        break;
      case TColumn.TYPE_TEXT_LIST:
        ((TStringListColumn)column).setHeaderValue(params.firstElement());
        ((TStringListColumn)column).setValues((Vector)params.elementAt(3));
        ((TStringListColumn)column).setDefaultValue((String)params.elementAt(2));
        break;
    }
    //column.setBooleanBetweenColor(params.);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
