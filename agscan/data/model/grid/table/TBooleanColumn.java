package agscan.data.model.grid.table;

import java.io.BufferedWriter;
import java.util.Vector;
import java.util.zip.DataFormatException;

import agscan.data.controler.memento.TColumnState;
import agscan.menu.columnmenubuilder.TBooleanColumnMenuBuilder;

public class TBooleanColumn extends TColumn {
  private Boolean def;
  public TBooleanColumn(int index, String name, String sk, boolean ed) {
    super(index);
    setHeaderValue(name);
    menuBuilder = TBooleanColumnMenuBuilder.getInstance();
    spotKey = sk;
    editable = ed;
    def = new Boolean(false);
    setCellEditor(new TBooleanEditor(this));
    setCellRenderer(new TBooleanRenderer());
  }
  public Object getDefaultValue() { return def; }
  public String getDefaultValueString() { return (def.booleanValue() ? "1" : "0"); }
  public void setDefaultValue(Boolean dv) { def = dv; }
  public int getType() { return TColumn.TYPE_BOOLEAN; }
  public boolean isCompatible(String[] tab) {
    for (int k = 0; k < tab.length; k++)
      if (!tab[k].equals("0") && !tab[k].equals("1") && !tab[k].equals("0.0") && !tab[k].equals("1.0")) return false;
    return true;
  }
  public Object[] getCompatibleValuesInBuffer(String[] tab) {
    Boolean[] values = new Boolean[tab.length];
    for (int k = 0; k < tab.length; k++) values[k] = new Boolean(tab[k].equals("1") || tab[k].equals("1.0"));
    return values;
  }
  public static Vector getParams(TColumnState cs) {
    Vector params = new Vector();
    params.addElement(cs.getName());
    params.addElement(cs.getSpotKey());
    params.addElement(cs.getDefaultValue());
    return params;
  }
  public void save(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    String ind = "  ";
    try {
      bw.write(base + "<booleanColumn>");
      bw.newLine();
      bw.write(base + ind + "<name>" + toString() + "</name>");
      bw.newLine();
      bw.write(base + ind + "<defaultValue>" + def + "</defaultValue>");
      bw.newLine();
      bw.write(base + ind + "<spotKey>" + spotKey + "</spotKey>");
      bw.newLine();
      bw.write(base + ind + "<editable>" + editable + "</editable>");
      bw.newLine();
      bw.write(base + ind + "<removable>" + removable + "</removable>");
      bw.newLine();
      bw.write(base + "</booleanColumn>");
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public static Object makeValue(String text, Vector values) throws NumberFormatException, DataFormatException {
    Object defaultValue = null;
    if (text.equals("0") || text.equalsIgnoreCase("false"))
      defaultValue = Boolean.FALSE;
    else if (text.equals("1") || text.equalsIgnoreCase("true"))
      defaultValue = Boolean.TRUE;
    else
      throw new NumberFormatException();
    return defaultValue;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
