package agscan.data.model.grid.table;

import java.io.BufferedWriter;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.DataFormatException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

import agscan.data.controler.memento.TColumnState;
import agscan.menu.columnmenubuilder.TStringListColumnMenuBuilder;

public class TStringListColumn extends TColumn {
  private static JComboBox cb;
  private int def;
  public TStringListColumn(int index, String name, String sk, boolean ed) {
    super(index);
    setHeaderValue(name);
    menuBuilder = TStringListColumnMenuBuilder.getInstance();
    spotKey = sk;
    editable = ed;
    cb = new JComboBox();
    DefaultCellEditor editor = new DefaultCellEditor(cb);
    editor.setClickCountToStart(2);
    setCellEditor(editor);
    setCellRenderer(new TStringRenderer());
  }
  public void setValues(Vector v) {
    cb.removeAllItems();
    for (Enumeration enume = v.elements(); enume.hasMoreElements(); )
      cb.addItem(enume.nextElement());
    if (v.size() > 0)
      def = 0;
    else
      def = -1;
    cb.setSelectedIndex(def);
  }
  public Object getDefaultValue() {
    if (def > -1)
      return (String)cb.getItemAt(def);
    return null;
  }
  public String getDefaultValueString() { return getDefaultValue().toString(); }
  public void setDefaultValue(String dv) {
    for (int i = 0; i < cb.getItemCount(); i++) {
      if (((String)cb.getItemAt(i)).equals(dv)) {
        def = i;
        break;
      }
    }
  }
  public int getType() { return TColumn.TYPE_TEXT_LIST; }
  public Vector getValues() {
    Vector v = new Vector();
    for (int i = 0; i < cb.getItemCount(); i++) v.addElement(cb.getItemAt(i));
    return v;
  }
  public boolean isCompatible(String[] tab) {
    boolean ok;
    int i;
    for (int k = 0; k < tab.length; k++) {
      ok = false;
      for (i = 0; i < cb.getItemCount(); i++)
        if (tab[k].equals(cb.getItemAt(i))) {
          ok = true;
          break;
        }
      if (!ok) return false;
    }
    return true;
  }
  public Object[] getCompatibleValuesInBuffer(String[] tab) {
    return tab;
  }
  public static Vector getParams(TColumnState cs) {
    Vector params = new Vector();
    params.addElement(cs.getName());
    params.addElement(cs.getSpotKey());
    params.addElement(cs.getDefaultValue());
    params.addElement(cs.getValues());
    return params;
  }
  public void save(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    String ind = "  ";
    try {
      bw.write(base + "<setOfTextsColumn>");
      bw.newLine();
      bw.write(base + ind + "<name>" + toString() + "</name>");
      bw.newLine();
      bw.write(base + ind + "<defaultValue>" + def + "</defaultValue>");
      bw.newLine();
      bw.write(base + ind + "<spotKey>" + spotKey + "</spotKey>");
      bw.newLine();
      bw.write(base + ind + "<editable>" + editable + "</editable>");
      bw.newLine();
      bw.write(base + ind + "<removable>" + removable + "</removable>");
      bw.newLine();
      bw.write(base + ind + "<values>");
      bw.newLine();
      for (Enumeration enume = getValues().elements(); enume.hasMoreElements(); ) {
        bw.write(base + ind + ind + "<value>" + (String)enume.nextElement() + "</value>");
        bw.newLine();
      }
      bw.write(base + ind + "</values>");
      bw.newLine();
      bw.write(base + "</setOfTextsColumn>");
      bw.newLine();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public static Object makeValue(String text, Vector values) throws NumberFormatException, DataFormatException {
    Object defaultValue = text;
    boolean ok = false;
    for (int i = 0; i < values.size(); i++)
      if (((String)values.elementAt(i)).equals((String)defaultValue)) {
        ok = true;
        break;
      }
    if (!ok) throw new DataFormatException();
    return defaultValue;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
