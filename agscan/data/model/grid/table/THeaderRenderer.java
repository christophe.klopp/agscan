/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import agscan.data.model.grid.TGridModel;

public class THeaderRenderer extends DefaultTableCellRenderer {
  private static Font font;
  private static ImageIcon iconAscending, iconDescending;
  public THeaderRenderer() {
    font = new Font("serif", Font.BOLD, 12);
    iconAscending = new ImageIcon(agscan.FenetrePrincipale.application.getClass().getResource("ressources/sort_ascending.gif"));
    iconDescending = new ImageIcon(agscan.FenetrePrincipale.application.getClass().getResource("ressources/sort_descending.gif"));
  }
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    JLabel lab = new JLabel((String)value);
    lab.setBorder(BorderFactory.createRaisedBevelBorder());
    lab.setHorizontalAlignment(CENTER);
    lab.setHorizontalTextPosition(JLabel.LEFT);
    lab.setFont(font);
    if ((table != null) && (column != 0)) {
      TGridModel gridModel = (TGridModel)table.getModel();
      int sortedColumn = 0;
      if (gridModel != null)
        sortedColumn = gridModel.getSortedColumn();
      if (sortedColumn == column)
        if (((TGridModel)table.getModel()).isSortedInAscendingOrder())
          lab.setIcon(iconAscending);
        else
          lab.setIcon(iconDescending);
      int selectedColumn = 0;
      if (gridModel != null)
        selectedColumn = gridModel.getSelectedColumn();
      if (selectedColumn == column) {
        lab.setBackground(Color.cyan);
        lab.setBorder(BorderFactory.createLoweredBevelBorder());
      }
      if (gridModel.getColorizedColumn() == column) {
        lab.setForeground(Color.red);
      }
    }
    return lab;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
