/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import agscan.TEventHandler;
import agscan.data.controler.TGridColumnsControler;
import agscan.dialog.TURLEditDialog;
import agscan.event.TEvent;

public class TURLEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
  private Vector v;
  private JButton editButton = new JButton("...");
  public static class URLButton extends JButton implements ActionListener {
    private String link;
    public URLButton(String href, String text) {
      super("<HTML><A href=\"" + href + "\">" + text + "</A></HTML>\n");
      link = href;
      setBackground(Color.white);
      setBorder(null);
      setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      addActionListener(this);
    }
    public void actionPerformed(ActionEvent event) {
      try {
        if (System.getProperty("os.name").toLowerCase().indexOf("windows") >= 0)
          Runtime.getRuntime().exec("explorer \"" + link + "\"");
        else if (System.getProperty("os.name").toLowerCase().indexOf("linux") >= 0)
          Runtime.getRuntime().exec("netscape \"" + link + "\"");
        else if (System.getProperty("os.name").toLowerCase().indexOf("unix") >= 0)
          Runtime.getRuntime().exec("netscape \"" + link + "\"");
      }
      catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }
  public TURLEditor(TURLColumn column) {
    super();
    v = (Vector)column.getDefaultValue();
    editButton.addActionListener(this);
  }
  public Object getCellEditorValue() {
    return v;
  }
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    v = (Vector)value;
    URLButton buttonURL = new URLButton((String)v.lastElement(), (String)v.firstElement());
    JPanel panel = new JPanel();
    editButton.setPreferredSize(new Dimension(20, 15));
    editButton.setMaximumSize(new Dimension(20, 15));
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.add(buttonURL);
    panel.add(editButton);
    panel.add(Box.createHorizontalStrut(5));
    if (isSelected) {
      panel.setBackground(table.getSelectionBackground());
      buttonURL.setBackground(table.getSelectionBackground());
    }
    else {
      panel.setBackground(Color.white);
      buttonURL.setBackground(Color.white);
    }
    return panel;
  }
  public void actionPerformed(ActionEvent event) {
    TURLEditDialog dial = new TURLEditDialog(v);
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    dial.setLocation((screenSize.width - dial.getWidth()) / 2, (screenSize.height - dial.getHeight()) / 2);
    dial.setVisible(true);
    if (dial.getResult() != null) {
      v = dial.getResult();
      fireEditingStopped();
      TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.REFRESH_COLUMNS_SIZES, null);
      TEventHandler.handleMessage(ev);
    }
  }
  public boolean isCellEditable(EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      return ((MouseEvent)anEvent).getClickCount() >= 1;
    }
    return true;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
