/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.model.grid.table;

import java.awt.Color;
import java.io.BufferedWriter;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.DataFormatException;

import javax.swing.JPopupMenu;
import javax.swing.table.TableColumn;

import agscan.data.controler.memento.TColumnState;
import agscan.menu.columnmenubuilder.TColumnMenuBuilder;
import agscan.data.element.grid.TGrid;

public abstract class TColumn extends TableColumn {
  public static final int TYPE_INTEGER = 0;
  public static final int TYPE_REAL = 1;
  public static final int TYPE_TEXT = 2;
  public static final int TYPE_BOOLEAN = 3;
  public static final int TYPE_PROBA = 4;
  public static final int TYPE_INTEGER_LIST = 5;
  public static final int TYPE_REAL_LIST = 6;
  public static final int TYPE_TEXT_LIST = 7;
  public static final int TYPE_URL = 8;
  public static final String[] columnType = {"Entier", "R�el", "Texte", "Bool�en", "Probabilit�", "Liste d'entiers",
                                             "Liste de r�els", "Liste de textes", "URL"};
  protected boolean visible, editable, removable, booleanSupColor, booleanBetweenColor, booleanInfColor, synchrone;

  protected String spotKey;
  protected TColumnMenuBuilder menuBuilder;

  public TColumn(int index) {
    super(index);
    visible = true;
    editable = true;
    removable = true;
    booleanSupColor = booleanBetweenColor = booleanInfColor = false;
    menuBuilder = null;
    synchrone = false;
    setHeaderRenderer(new THeaderRenderer());
  }
  public void setindex(int index) {
    super.setModelIndex(index);
  }
  public void setSynchrone(boolean b) {
    synchrone = b;
  }
  public void setVisible(boolean b) {
    visible = b;
  }
  public boolean isVisible() {
    return visible;
  }
  public void setEditable(boolean b) {
    editable = b;
  }
  public boolean isEditable() {
    return editable;
  }
  public boolean isSynchrone() {
    return synchrone;
  }
  public int getPreferredSize() {
    return 75;
  }
  public String getSpotKey() {
    return spotKey;
  }
  public void setRemovable(boolean b) {
    removable = b;
  }
  public boolean isRemovable() {
    return removable;
  }
  public abstract Object getDefaultValue();
  public String toString() {
    return (String)getHeaderValue();
  }
  public abstract int getType();
  public Vector getValues() { return null; }
  public Color getInfColor() { return Color.white; }
  public Color getBetweenColor() { return Color.white; }
  public Color getSupColor() { return Color.white; }
  public Object getSupValue() { return null; }
  public Object getBetweenValue1() { return null; }
  public Object getBetweenValue2() { return null; }
  public Object getInfValue() { return null; }
  public void setBooleanSupColor(boolean b) { booleanSupColor = b; }
  public void setBooleanBetweenColor(boolean b) { booleanBetweenColor = b; }
  public void setBooleanInfColor(boolean b) { booleanInfColor = b; }
  public boolean hasSupColor() { return booleanSupColor; }
  public boolean hasBetweenColor() { return booleanBetweenColor; }
  public boolean hasInfColor() { return booleanInfColor; }
  public static Object makeValue(int type, String text, Vector values) throws NumberFormatException, DataFormatException {
    Object defaultValue = null;
    switch (type) {
      case TYPE_BOOLEAN:
        defaultValue = TBooleanColumn.makeValue(text, values);
         break;
      case TYPE_INTEGER:
        defaultValue = TIntegerColumn.makeValue(text, values);
        break;
      case TYPE_PROBA:
        defaultValue = TProbaColumn.makeValue(text, values);
        break;
      case TYPE_REAL:
        defaultValue = TDoubleColumn.makeValue(text, values);
        break;
      case TYPE_TEXT:
        defaultValue = TStringColumn.makeValue(text, values);
        break;
      case TYPE_URL:
        defaultValue = TURLColumn.makeValue(text, values);
        break;
      case TYPE_INTEGER_LIST:
        defaultValue = TIntegerListColumn.makeValue(text, values);
        break;
      case TYPE_REAL_LIST:
        defaultValue = TDoubleListColumn.makeValue(text, values);
        break;
      case TYPE_TEXT_LIST:
        defaultValue = TStringListColumn.makeValue(text, values);
        break;
    }
    return defaultValue;
  }
  public static Vector makeList(int type, String text) {
    Vector v = new Vector();
    StringTokenizer tokenizer = new StringTokenizer(text, ";");
    String token;
    Object value;
    switch (type) {
      case TYPE_INTEGER_LIST:
        while (tokenizer.hasMoreTokens()) {
          token = tokenizer.nextToken();
          value = Integer.decode(token);
          v.addElement(value);
        }
        break;
      case TYPE_REAL_LIST:
        while (tokenizer.hasMoreTokens()) {
          token = tokenizer.nextToken();
          value = Double.valueOf(token);
          v.addElement(value);
        }
        break;
      case TYPE_TEXT_LIST:
        while (tokenizer.hasMoreTokens()) {
          token = tokenizer.nextToken();
          v.addElement(token);
        }
        break;
    }
    return v;
  }
  public abstract String getDefaultValueString();
  public JPopupMenu getPopupMenu(TGrid grid, boolean paste) {
    if (menuBuilder != null) {
      menuBuilder.createPopupMenu(this, grid.getSortButtonGroup(), (grid.getGridModel().getSortedColumn() == getModelIndex()),
                                  grid.getGridModel().isSortedInAscendingOrder(), grid.getColorizeButtonGroup(),
                                  (grid.getGridModel().getColorizedColumn() == getModelIndex()), paste);
      return menuBuilder.getPopupMenu();
    }
    return null;
  }
  public boolean isCompatible(String[] tab) { return false; }
  public Object[] getCompatibleValuesInBuffer(String[] tab) { return null; }
  public TColumnState createSnapshot() {
    TColumnState cs = new TColumnState(getModelIndex(), toString(), getType(), getDefaultValue(), getValues(), hasInfColor(),
                                       hasBetweenColor(), hasSupColor(), getInfColor(), getBetweenColor(), getSupColor(),
                                       getInfValue(), getBetweenValue1(), getBetweenValue2(), getSupValue(), visible,
                                       editable, removable, spotKey, synchrone);
    return cs;
  }
  public abstract void save(BufferedWriter bw, int indent);
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
