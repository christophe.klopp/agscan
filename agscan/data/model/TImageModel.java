 package agscan.data.model;
 
 import ij.ImagePlus;
import ij.ImageStack;
import ij.process.Blitter;
import ij.process.ImageProcessor;

import java.io.File;

import agscan.data.controler.TImageControler;
 
 public class TImageModel extends TModelImpl {
 	//TODO creer et appeler ici un Properties pour l'image
 	//	 creation d'un statut pour un modele d'image selon le nombre de canaux...
 	
 	/**
 	 * numberOfChannels gives the number of channels (= the number of images used) in the experiment
 	 * @author rcathelin
 	 * @since 2005/10/10
 	 */
 	
 	//private Vector initialImages;// vector of SImagePlus (for example a Cy3/Cy5 experiment is a 2 images vector)
 	private ImageStack stack;// stack of images 2005/10/14
 	private int  numberOfChannels = 0;// not very useful because it equals to the initialImages vector size
 	
 	private String unit;
 	
 	private String fileFormat;
 	private String ipType;//26/08/05 imaging plate size = taille de la membrane
 	
 	//public TImageModel(ImagePlus pImage, int[] data, double pixW, double pixH, int bpp, int type, String un, String ipt, double lat, String fFormat) {
 	public TImageModel(ImageStack stack, double pixW, double pixH, int bpp, String un, String ipt, double lat, String fFormat) {  	
 		super();
 		this.setNumberOfChannels(stack.getSize());//one image given in parameter = 1 channel experiment = radio experiment
 		//TODO virer latitude et autres proprietes de l'image ...
 		//for (int i=0;i<images.size();i++){
 		//	((SImagePlus)(images.get(i))).setLatitude(lat);// REMI ligne remise en place le 20/09/05 pour conversion QL=>PSL, lat valait toujours0!!!!!	
 		//}
 		ImagePlusTools.setLatitude(lat);//2005/10/14
 		this.stack = stack;
 		pixelHeight = pixH;
 		pixelWidth = pixW;
 		unit = un;
 		ipType = ipt;
 		fileFormat = fFormat;
 		
 	}
 	
 	public double getPixelWidth() {
 		return pixelWidth;
 		//TODO comprendre pixelWidth avec ij.Calibration 
 		//	return initialImage.getCalibration().pixelWidth;
 	}
 	
 	public double getPixelHeight() {
 		return pixelHeight;
 		//  TODO comprendre pixelHeight avec ij.Calibration
 		//return initialImage.getCalibration().pixelHeight;
 	}
 	
 	/*
 	 * images files of the model are the same => same size=> we use the first
 	 * @see agscan.data.model.TModelImpl#getElementWidth()
 	 */
 	public int getElementWidth() {
 		//return ((ImagePlus)(initialImages.get(0))).getWidth();
 		return stack.getWidth();
 	}
 	
 	public int getElementHeight() {
 		//return  ((ImagePlus)(initialImages.get(0))).getHeight();
 		return  stack.getHeight();
 	}
 	/**
 	 * Compute the compute image width in B5m thanks to the image width in pixels and the size of a pixel
 	 * @return pixel width * pixel width
 	 */ 	
 	public double getImageWidth() {
 		return getPixelWidth() * getElementWidth();
 	}
 	
 	/**
 	 * Compute the compute image height in B5m thanks to the image height in pixels and the size of a pixel
 	 * @return pixel height * pixel height
 	 */ 
 	public double getImageHeight() {
 		return getPixelHeight() * getElementHeight();
 	}
 	
 	public void notifyView() {
 	}
 	
 	public Object getValueAt(int rowIndex, int columnIndex) {
 		return null;
 	}
 	
 	public void save(File file) {
 	}
 	
 	public int getRowCount() {
 		return 0;
 	}
 	public int getColumnCount() {
 		return 0;
 	}
 	
 	/**
 	 * By default return the first image SImagePlus
 	 * equivalent to getImageChannel(1)
 	 * @return the image 
 	 */
 	public ImagePlus getInitialImage() {
 		//return ((ImagePlus)(initialImages.get(0)));
 		return (ImagePlus)(new ImagePlus(stack.getSliceLabel(1),stack.getProcessor(1)));//modif 2005/10/14
 	}
 	
// 	ajout 2005/10/14
 	public ImageProcessor getInitialProcessor() {
 		return stack.getProcessor(1);
 	}
 	
 	
 	/**
 	 * Ask for a selected image of initials image channels
 	 * @param i - index of the image to return
 	 * @return the image 
 	 */
 	public ImagePlus getChannelImage(int i) {
 		if (i>0 && i<=stack.getSize()) //return ((ImagePlus)(initialImages.get(i)));
 			return (ImagePlus)(new ImagePlus(stack.getSliceLabel(i),stack.getProcessor(i)));//modif 2005/10/14
 		else System.err.println("No image in the channel "+i); 
 		return null;
 	}
 	
 	public int getImageBitDepth() {
 	//	return ((ImagePlus)(initialImages.get(0))).getBitDepth();
 		//pas pratique: obligC) de crC)er une imagePlus avec le processor du stack pour connaitre la profondeur de l'image...
 		return ((ImagePlus)(new ImagePlus(null,stack.getProcessor(1)))).getBitDepth();
 	}
 	
 	
 	
 	public String getUnit() {
 		return unit;
 	}
 	
 	public String getIPType() {
 		return ipType;
 	}
 	
 	
 	//2005/10/14: Bof==>setInitialImage, remplacer directement le stack
 	
 	/**
 	 * @param pi -  the SImagePlus in the channel
 	 * @param channel - channel of the image. Warning first is channel 0
 	 */
 //	public void setInitialImage(ImagePlus pi, int channel) {
 		//initialImages.set(channel,pi);
 	//	stack.addSlice(pi.getTitle(),pi.getProcessor(),channel-1);//l'indice ici est l'indice apres lequel mettre l'image...
 //	}
 	/**
 	 * By default add in the first channel
 	 * @param pi
 	 */
 	//public void setInitialImage(ImagePlus pi) {
 		//initialImages.set(0,pi);
 	//	stack......
 	//}
 	
 	public String getFileFormat() {
 		return fileFormat;
 	}
 	
 	public void setFileFormat(String ff) {
 		fileFormat = ff;
 	}
 	
 	/**
 	 * @param numberOfChannels The numberOfChannels to set.
 	 * @author rcathelin
 	 * @since 2005/10/10
 	 */
 	private void setNumberOfChannels(int numberOfChannels) {
 		this.numberOfChannels = numberOfChannels;
 	}
 	
 	/**
 	 * @return Returns the numberOfChannels.
 	 * @author rcathelin
 	 * @since 2005/10/10
 	 */
 	public int getNumberOfChannels() {
 		return numberOfChannels;
 	}
 	
 	
 	/**
 	 * @return the max pixels tab of the maxImage
 	 * @author rcathelin
 	 * @version 05/11/10
 	 */
 	public int[] getMaxImageData(){
 			return ImagePlusTools.getImageData(getMaxImage(),ImagePlusTools.DEPTH_16_BITS); 
 	}
 	
 	/**
 	 * this method creates a max image with all the images of the experiment
  	 * In fact the max image is composed of the max pixel value between all images 
  	 * of the model for each pixel position
 	 * @author rcathelin
 	 * @version 05/11/10
 	 * @return the max SImagePlus
 	 */
 	/*public ImagePlus getMaxImage(){
 		//the current image is the first
 		ImagePlus maxImage = getInitialImage();
 		ImagePlus nextImage = null;
 		ImageProcessor maxProcessor = null;
 		// we invert the LUT for the max computing
 		maxImage =  (ImagePlus)TImageControler.setInvertedLUT(maxImage,true);
 		//TODO au lieu du cast au dessus ne faut-il pas changer le return dans setIInvertesLUT?a voir
 		
 		// if only one channel, we return the data for this image
 		// 		else several channels: we have to create the max image from them
 		if (numberOfChannels>1){ 		 		
 			for (int i=1;i<stack.getSize();i++){
 				nextImage = (ImagePlus)initialImages.get(i);
 				nextImage =  (ImagePlus)TImageControler.setInvertedLUT(nextImage,true);
 				maxProcessor = maxImage.getProcessor().duplicate();
 				maxProcessor.copyBits(nextImage.getProcessor(),0,0,Blitter.MAX);
 				maxImage = new ImagePlus("MAX",maxProcessor);
 			}
 		}
 		return maxImage; 	
 	}
 	*/
 	
	
 	/**
 	 * this method creates a max image with all the images of the experiment
  	 * In fact the max image is composed of the max pixel value between all images 
  	 * of the model for each pixel position
 	 * @author rcathelin
 	 * @version 05/11/10
 	 * @return the max SImagePlus
 	 */
 	public ImagePlus getMaxImage(){
 		//the current image is the first
 		ImageProcessor maxProcessor = getInitialProcessor().duplicate();
 		ImageProcessor nextProcessor = null;
 		ImagePlus maxImagePlus = getInitialImage();//init avec la 1ere image
 		// we invert the LUT for the max computing
 		maxProcessor = TImageControler.setInvertedLUT(maxProcessor,true);
 		//TODO au lieu du cast au dessus ne faut-il pas changer le return dans setIInvertesLUT?a voir
 		
 		// if only one channel, we return the data for this image
 		// 		else several channels: we have to create the max image from them
 		if (numberOfChannels>1){ 		 		
 			for (int i=2;i<=stack.getSize();i++){
 				nextProcessor = stack.getProcessor(i);
 				nextProcessor =  TImageControler.setInvertedLUT(nextProcessor,true);
 				//maxProcessor = maxImage.getProcessor().duplicate();
 				maxProcessor.copyBits(nextProcessor,0,0,Blitter.MAX);
 				maxImagePlus = new ImagePlus("MAX",maxProcessor);
 			}
 		}
 		return maxImagePlus; 	
 	}
 	
	public ImageStack getImages() {
		return stack;
	}
	public void setInitialImages(ImageStack initialImages) {
		this.stack = initialImages;
	}
 }
 
 /******************************************************
  * COPYRIGHT AND PERMISSION NOTICE
  *
  * Copyright (c) 2004 INSERM-ERM 206 - TAGC
  * Copyright (c) 2005 INRA - SIGENAE
  *
  * All rights reserved.
  *
  * Permission is hereby granted, free of charge, to any person obtaining
  * a copy of this software and associated documentation files (the
  * "Software"), to deal in the Software without restriction, including
  * without limitation the rights to use, copy, modify, merge, publish,
  * distribute, and/or sell copies of the Software, and to permit persons
  * to whom the Software is furnished to do so, provided that the above
  * copyright notice(s) and this permission notice appear in all copies of
  * the Software and that both the above copyright notice(s) and this
  * permission notice appear in supporting documentation.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
  * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
  * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
  * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
  * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  *
  * Except as contained in this notice, the name of a copyright holder
  * shall not be used in advertising or otherwise to promote the sale, use
  * or other dealings in this Software without prior written authorization
  * of the copyright holder.
  *******************************************************/
