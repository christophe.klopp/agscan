/*
 * Created on Oct 11, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.data.model;

import ij.ImagePlus;
import ij.process.ImageProcessor;
import agscan.data.controler.TImageControler;



/**
 * @author rcathelin
 *2005/10/14 changement de tout en static  et changement de nom pour changer faciulement les appels...
 *SImagePlus => ImagePlusTools 
 */

public abstract class ImagePlusTools {
	public static final int DEPTH_8_BITS = 8;
	public static final int DEPTH_16_BITS = 16;
	
	private static double latitude;//TODO mettre dans un ImageProperties plus tard
	
	//public ImagePlusTools.java() {
	//	super();
	//}		
	
	/**
	 * @param string
	 * @param maxProcessor
	 */
	//public SImagePlus(String string, ImageProcessor maxProcessor) {
//		super(string,maxProcessor);
		// TODO Auto-generated constructor stub
//	}
	
	
	/**
	 * @param absolutePath
	 */
//	public SImagePlus(String absolutePath) {
//		super(absolutePath);
		// TODO Auto-generated constructor stub
//	}
	
	
	// 02/09/05 modification: imageData is suppressed from the model
	// it's why we have to get data from image (short tab of pixels) and convert them into an int tab.
	// 
	/*
	 * Return pixels tab from the image
	 * 11/10/05 modification: addition of the param bits for getting data on 8 or 16 bits scale)
	 * @return an int pixel tab
	 * @since 11/10/05
	 * 
	 */
	public static int[] getImageData(ImagePlus ip, int bits) {  	
		int[] imageData = null;
		switch (bits) {
		case DEPTH_16_BITS:
			short[] imageDataShort= (short[]) ip.getProcessor().getPixels();
			imageData = new int[imageDataShort.length];
			
			for (int i=0;i< imageDataShort.length;i++)    {
				//imageData[i]=(imageDataShort[i]+65535)%65535;//modif 20/09/05 verif que en positif sinon conversion	
				//double test = imageDataShort[i] & 0xFFFF;
				imageData[i]=imageDataShort[i] & 0xFFFF;//-32768:32767=>0:65535
				// DEBUT	TEST REMI 06/01/17
				//	imageData[i]= 65536 - imageData[i];
				//					 a faire avant: ip.getProcessor().invert();// TEST REMI 06/01/17
				//				 FIN	TEST REMI 06/01/17
				//TODO verifier pourquoi difference de 1 entre & et modulo...
				//System.out.println(imageDataShort[i]+"==>"+imageData[i]+"==>"+test);
				//System.out.println();
			}
			
			break;
		case DEPTH_8_BITS:
			byte[] imageDataByte = (byte[]) ip.getProcessor().convertToByte(true).getPixels();
			imageData = new int[imageDataByte.length];
			for (int i=0;i< imageDataByte.length;i++)    {
				imageData[i]=imageDataByte[i] & 0xFF;// -128:127=>0:255 
			}
			break;
		}  		
		return imageData;
	}
	
	
	//TODO tout faire avec getImageData qui prends un processor au lieu d'une imagePlus...
	//2005/10/14 => modif (surcharge) pour tyravailler sur un imageProcessor
	// 02/09/05 modification: imageData is suppressed from the model
	// it's why we have to get data from image (short tab of pixels) and convert them into an int tab.
	// 
	/*
	 * Return pixels tab from the image
	 * 11/10/05 modification: addition of the param bits for getting data on 8 or 16 bits scale)
	 * @return an int pixel tab
	 * @since 11/10/05
	 * 
	 */
	public static int[] getImageData(ImageProcessor iproc, int bits) {  	
			int[] imageData = null;
		switch (bits) {
		case DEPTH_16_BITS:
			short[] imageDataShort= (short[]) iproc.getPixels();
			imageData = new int[imageDataShort.length];
			for (int i=0;i< imageDataShort.length;i++)    {
				//imageData[i]=(imageDataShort[i]+65535)%65535;//modif 20/09/05 verif que en positif sinon conversion	
				//double test = imageDataShort[i] & 0xFFFF;
				imageData[i]=imageDataShort[i] & 0xFFFF;//-32768:32767=>0:65535
				//TODO verifier pourquoi difference de 1 entre & et modulo...
				//System.out.println(imageDataShort[i]+"==>"+imageData[i]+"==>"+test);
				//System.out.println();
			}
			break;
		case DEPTH_8_BITS:
			byte[] imageDataByte = (byte[]) iproc.convertToByte(true).getPixels();
			imageData = new int[imageDataByte.length];
			for (int i=0;i< imageDataByte.length;i++)    {
				imageData[i]=imageDataByte[i] & 0xFF;// -128:127=>0:255 
			}
			break;
		}  		
		return imageData;
	}
	
	// ajout remi le 02/09/05, modifié le 22/09/05
	/*
	 * Returns the tab of pixels of the image converted into "PSL" or "QL" unit.
	 * PSL = Photo Stimulated Luminescence Unit
	 *  QL = Quantum Level Unit 
	 * @param ConversionUnit unit used for conversion: "PSL or "QL"
	 * @return the converted tab of pixels  
	 * @since 02/09/05   
	 */
	//retourne un tableau de doubles, avant c'etait des entiers...car la conversion est en décimales
	//TODO se fait sur la premiere image par defaut ->le faire pour toutes les images???
	//TODO et si on veut le faire sur une autre image???? donc faire que ce soit un outil commun
	public static double[] pixelsUnitConversion2(ImagePlus ip, String conversionUnit){
		double[] convertedPixels = null;// converted pixels tab
		int[] pixels = ImagePlusTools.getImageData(ip,16);// initial pixels tab => en entiers lui de 0 à 65535
		// if unit asked is the same that unit used by the image return the image pixels tab
		if (conversionUnit.compareToIgnoreCase(ip.unit)==0) {
			convertedPixels = new double[pixels.length];
			//creation du meme tableau mais en double
			for (int i=0;i< convertedPixels.length;i++){
				convertedPixels[i]=pixels[i];
			}
		}
		// else we have to convert pixels tab
		else {
			// two units = two cases
			if (conversionUnit=="PSL") {
				for (int i = 0; i < pixels.length; i++)
					convertedPixels = TImageControler.ql2pslData(pixels,ImagePlusTools.getLatitude());				
			}
			else {
				// case conversionUnit="QL"
				for (int i = 0; i < pixels.length; i++)
					convertedPixels = TImageControler.psl2qlData(pixels,ImagePlusTools.getLatitude());
			}
		}
		return convertedPixels;
	}
	
	public static void setLatitude(double latitude) {
		ImagePlusTools.latitude = latitude;
	}
	public static double getLatitude() {
		return latitude;
	}
	
}


/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2005 INRA - SIGENAE
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/