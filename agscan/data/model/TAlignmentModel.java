package agscan.data.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import agscan.data.element.image.TImage;
import agscan.data.model.grid.TGridModel;
import agscan.data.element.grid.TGrid;

public class TAlignmentModel extends TModelImpl {
  private TImageModel imageModel;
  private TGridModel gridModel;
  private int[] fitData;
  private boolean saveImage = false;//false by default - if true, image is stored into the alignment save

  public TAlignmentModel(TImageModel imageModel, TGridModel gridModel, boolean saveImage) {
    this.imageModel = imageModel;
    this.gridModel = gridModel;
    this.saveImage = saveImage;
  }
  public TGridModel getGridModel() {
    return gridModel;
  }
  public void setGridModel(TGridModel gm) {
    gridModel = gm;
  }
  public TImageModel getImageModel() {
    return imageModel;
  }
  public int getColumnCount() {
    return gridModel.getColumnCount();
  }
  public Object getValueAt(int rowIndex, int columnIndex) {
    return gridModel.getValueAt(rowIndex, columnIndex);
  }
  public int getElementWidth() {
    return imageModel.getElementWidth();
  }
  public int getRowCount() {
    return gridModel.getRowCount();
  }
  public void save(File file) {
    String s;
    int i;
    System.err.println("Enregistre l'alignment dans : " + file.getAbsolutePath());
    try {
      File tmpFile = File.createTempFile("zip", "agscan");//rename 2005/11/03
      tmpFile.deleteOnExit();
      BufferedWriter bw = new BufferedWriter(new FileWriter(tmpFile));
      Calendar rightNow = Calendar.getInstance();
      bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");//modif remi remis 2005/11/16 pb open XP
      //      bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");//modif remi le 22/08/05 pour le a
      bw.newLine();
      bw.write("<alignment>");
      bw.newLine();
      bw.write("  <infos>");
      bw.newLine();
      bw.write("    user=\"" + System.getProperty("user.name") + "\"");
      bw.newLine();
      bw.write("    os=\"" + System.getProperty("os.name") + "\"");
      bw.newLine();
      bw.write("    java=\"" + System.getProperty("java.runtime.version") + "\"");
      bw.newLine();
      DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRANCE);
      bw.write("    date=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      df = DateFormat.getTimeInstance(DateFormat.LONG, Locale.FRANCE);
      bw.write("    time=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      bw.write("  </infos>");
      bw.newLine();
      bw.write("  <image>");
      bw.newLine();
      ((TImage)imageModel.getReference()).saveFileInfo(bw, 4, saveImage);
      bw.write("  </image>");
      bw.newLine();
      bw.write("  <grid>");
      bw.newLine();
      ((TGrid)gridModel.getReference()).saveFileInfo(bw, 4);
      bw.write("  </grid>");
      bw.newLine();
      bw.write("  <features>");
      bw.newLine();
      ((TGrid)gridModel.getReference()).saveFeaturesInfo(bw, 4);
      bw.write("  </features>");
      bw.newLine();
      bw.write("</alignment>");
      bw.newLine();
      bw.close();

      FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
      ZipOutputStream zos = new ZipOutputStream(fos);
      ZipEntry ze = new ZipEntry("manifest");
      byte[] bytes = new byte[2048], b;
      zos.setMethod(ZipEntry.DEFLATED);
      zos.setLevel(9);
      zos.putNextEntry(ze);
      s = "image = " + imageModel.getReference().getName() + System.getProperty("line.separator");
      System.out.println("dans TAlignmentModel, image pour le xml s="+s);
      b = s.getBytes();
      zos.write(b, 0, b.length);
      i = file.getName().lastIndexOf(".zaf");
      if (i >= 0)
        s = "ali = " + file.getName().substring(0, i) + ".ali" + System.getProperty("line.separator");
      else
        s = "ali = " + file.getName() + ".ali" + System.getProperty("line.separator");
      b = s.getBytes();
      zos.write(b, 0, b.length);
      zos.closeEntry();
      ze = new ZipEntry(file.getName().substring(0, i) + ".ali");
      zos.putNextEntry(ze);
      FileInputStream fis = new FileInputStream(tmpFile);
      int lu;
      while(-1 != (lu = fis.read(bytes))) zos.write(bytes, 0, lu);
      zos.closeEntry();
      System.out.println("save image = "+saveImage);
      if (saveImage) {
      	//TODO boucle a voir dans le cas ou saveImage est a true
	    for(int j=0;j<imageModel.getNumberOfChannels();j++) 
	    {
    	  	s = imageModel.getChannelImage(j+1).getTitle();
	    
	        System.out.println("dans TAlignmentModel, demande du nom du data element s="+s);
	        ze = new ZipEntry(s);
	        zos.putNextEntry(ze);
	        fis = new FileInputStream(imageModel.getReference().getPath() + imageModel.getChannelImage(j+1).getTitle());
	        while(-1 != (lu = fis.read(bytes))) zos.write(bytes, 0, lu);
	        zos.closeEntry();
	        //TODO reflechir pour le pb du inf/img = 2 fichiers a sauver comment le deviner sachant que ca vient d'un plugin!!!!
	        if (s.endsWith(".inf")) {
	          ze = new ZipEntry(s.substring(0, s.lastIndexOf(".inf") + 1) + "img");
	          zos.putNextEntry(ze);
	          fis = new FileInputStream(imageModel.getReference().getPath() + s.substring(0, s.lastIndexOf(".inf") + 1) + "img");
	          while (-1 != (lu = fis.read(bytes))) zos.write(bytes, 0, lu);
	          zos.closeEntry();
	        }
        }
      }
      zos.close();
      fis.close();
      fos.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public void setSaveImage(boolean si) {
    saveImage = si;
  }
  public boolean getSaveImage() {
    return saveImage;
  }
  public void setImageModel(TImageModel im) {
    imageModel = im;
  }
  public void notifyView() {
    /**@todo Implementer cette methode image.data.model.TModelImpl abstract*/
  }
  public int getElementHeight() {
    return imageModel.getElementHeight();
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
