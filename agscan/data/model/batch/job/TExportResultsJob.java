package agscan.data.model.batch.job;

import java.io.BufferedWriter;
import java.io.File;

import agscan.TEventHandler;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.data.element.alignment.TAlignment;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TExportResultsJob extends TDecoJob {
  public static final int TEXT = 1;
  public static final int MAGE_ML = 2;
  private int type;

  public TExportResultsJob(int t, TAbstractJob job, int sp, int ep) {
    super(job, sp, ep);
    type = t;
  }
  public int getProgressionUnits() {
    return getLocalProgressionUnits() + job.getProgressionUnits();
  }
  public int getLocalProgressionUnits() {
    return 1;
  }
  public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
    return job.getGlobalAlignmentAlgorithm();
  }
  public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
    return job.getLocalAlignmentAlgorithm();
  }
  public String getStringID() {
    return getLocalStringID() + "|" + job.getStringID();
  }
  private String getLocalStringID() {
    return "EX" + type;
  }
  public void saveInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    String s = base + "<job_elem id=\"" + getLocalStringID() + "\" />";
    try {
      job.saveInfo(bw, indent);
      bw.write(s);
      bw.newLine();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  public void execute(TAlignment alignment) {
    String s = alignment.getName();
    if (type == TEXT)
      s = alignment.getPath() + File.separator  + s.substring(0, s.lastIndexOf(".")) + ".txt";
    else if (type == MAGE_ML)
      s = alignment.getPath() + File.separator + s.substring(0, s.lastIndexOf(".")) + ".xml";
    TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
    sdAlgo.initWithDefaults();
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY, alignment, sdAlgo));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.EXPORT_AS_TEXT, alignment, new File(s)));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress)));
    if (!worker.getSTOP()) super.execute(alignment);
  }
}
