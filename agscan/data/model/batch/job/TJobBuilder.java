package agscan.data.model.batch.job;

import java.util.Vector;

import agscan.algo.globalalignment.TSnifferGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentBlockAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TJobBuilder {
  private static TAbstractJob job;
  public TJobBuilder() {
  }
  public static TAbstractJob createDefaultJob() {
    
	/*Vector params_global = new Vector();
    params_global.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.searchAngle));
    params_global.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.contrastPercent));
    params_global.addElement(new Double(TSnifferGlobalAlignmentAlgorithm.searchAngleIncrement));
    params_global.addElement(new Integer(TSnifferGlobalAlignmentAlgorithm.snifferSize));
    Object[] o1 = new Object[11];
    o1[0] = new Integer(TSnifferGlobalAlignmentAlgorithm.locBootstrapSensitivity);
    o1[1] = new Integer(TSnifferGlobalAlignmentAlgorithm.locBootstrapIterations);
    o1[2] = new Integer(TSnifferGlobalAlignmentAlgorithm.locFirstRunSensitivity);
    o1[3] = new Integer(TSnifferGlobalAlignmentAlgorithm.locFirstRunIterations);
    o1[4] = new Integer(TSnifferGlobalAlignmentAlgorithm.locSecondRunSensitivity);
    o1[5] = new Integer(TSnifferGlobalAlignmentAlgorithm.locSecondRunIterations);
    o1[6] = new Integer(TSnifferGlobalAlignmentAlgorithm.locThirdRunSensitivity);
    o1[7] = new Integer(TSnifferGlobalAlignmentAlgorithm.locThirdRunIterations);
    o1[8] = new Double(TSnifferGlobalAlignmentAlgorithm.locSensitivityThreshold);
    o1[9] = new Boolean(TSnifferGlobalAlignmentAlgorithm.locGridCheck);
    Object[] o2 = new Object[4];
    o2[0] = new Double(TSnifferGlobalAlignmentAlgorithm.locT);
    o2[1] = new Double(TSnifferGlobalAlignmentAlgorithm.locC);
    o2[2] = TSnifferGlobalAlignmentAlgorithm.locSpotDetectionAlgorithmName;
    o2[3] = TSnifferGlobalAlignmentAlgorithm.locSpotDetectionAlgorithmParameters;
    o1[10] = o2;
    params_global.addElement(o1);
    Vector params_local = new Vector();
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.bootstrapSensitivity));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.bootstrapIterations));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.firstRunSensitivity));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.firstRunIterations));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.secondRunSensitivity));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.secondRunIterations));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.thirdRunSensitivity));
    params_local.addElement(new Integer(TLocalAlignmentBlockAlgorithm.thirdRunIterations));
    params_local.addElement(new Double(TLocalAlignmentBlockAlgorithm.sensitivityThreshold));
    params_local.addElement(new Boolean(TLocalAlignmentBlockAlgorithm.gridCheck));
    o1 = new Object[4];
    o1[0] = new Double(TLocalAlignmentBlockAlgorithm.locT);
    o1[1] = new Double(TLocalAlignmentBlockAlgorithm.locC);
    o1[2] = TLocalAlignmentBlockAlgorithm.locSpotDetectionAlgorithmName;
    o1[3] = TLocalAlignmentBlockAlgorithm.locSpotDetectionAlgorithmParameters;
    params_local.addElement(o1);
    job = new TGlobalAlignmentJob(TSnifferGlobalAlignmentAlgorithm.NAME, params_global,
        new TLocalAlignmentJob(TLocalAlignmentBlockAlgorithm.NAME, params_local,
        new TComputeJob(TQuantifImageConstantAlgorithm.NAME,
        new TComputeJob(TQuantifImageComputedAlgorithm.NAME,
        new TComputeJob(TQuantifFitConstantAlgorithm.NAME,
        new TComputeJob(TQuantifFitComputedAlgorithm.NAME,
        new TComputeJob(TComputeQMAlgorithm.NAME,
        new TComputeJob(TComputeFitCorrectionAlgorithm.NAME,
        new TComputeJob(TComputeOvershiningCorrectionAlgorithm.NAME,
        new TJob(), 95, 100), 89, 95), 84, 89), 79, 84), 74, 79), 68, 74), 63, 68), 21, 63), 0, 21);
    */
    return new TJob();
  }
  public static void createJob() {
    job = new TJob();
  }
  public static void addGlobalAlignmentJob(String al, Vector params) {
    System.err.println("addGlobalAlignmentJob");
    job = new TGlobalAlignmentJob(al, params, job, 0, 0);
  }
  public static void addLocalAlignmentJob(String al, Vector params) {
    System.err.println("addLocalAlignmentJob");
    job = new TLocalAlignmentJob(al, params, job, 0, 0);
  }
  public static void addTreatmentJob(int tr) {
    job = new TTreatmentJob(tr, job, 0, 0);
  }
  public static void addComputeJob(String al) {
    job = new TComputeJob(al, job, 0, 0);
  }
  public static void addExportResultsJob(int t) {
    job = new TExportResultsJob(t, job, 0, 0);
  }
  public static TAbstractJob getJob() {
    int n = job.getProgressionUnits(), cp = 0;
    TAbstractJob j = job;
    while (j != null) {
      if (n > 0)
        j.setProgressionRange(cp * 100 / n, (cp + j.getLocalProgressionUnits()) * 100 / n);
      else
        j.setProgressionRange(0, 0);
      System.err.println("*** " + j.startProgress + " - " + j.endProgress);
      cp += j.getLocalProgressionUnits();
      j = j.getJob();
    }
    return job;
  }
}
