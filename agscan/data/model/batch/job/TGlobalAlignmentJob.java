package agscan.data.model.batch.job;

import java.io.BufferedWriter;
import java.util.Vector;

import agscan.algo.factory.TGlobalAlignmentAlgorithmFactory;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TGlobalAlignmentJob extends TDecoJob {
  private String algo;
  private TGlobalAlignmentAlgorithm algorithm;

  public TGlobalAlignmentJob(String algo, Vector par, TAbstractJob job, int sp, int ep) {
    super(job, sp, ep);
    this.algo = algo;
    params = par;
    TGlobalAlignmentAlgorithmFactory factory = new TGlobalAlignmentAlgorithmFactory();
    algorithm = (TGlobalAlignmentAlgorithm)factory.createAlgorithm(algo);
    algorithm.init(params.toArray());
  }
  public int getProgressionUnits() {
    return getLocalProgressionUnits() + job.getProgressionUnits();
  }
  public int getLocalProgressionUnits() {
    return 4;
  }
  public void execute(TAlignment alignment) {
    TAlignmentModel alignmentModel = (TAlignmentModel)alignment.getModel();
    algorithm.initData(alignmentModel.getImageModel().getPixelWidth(), alignmentModel.getImageModel().getPixelHeight(),
                       alignmentModel.getImageModel().getMaxImageData(),  alignmentModel.getImageModel().getElementWidth(),
                       alignmentModel.getImageModel().getElementHeight(), alignmentModel,
                       ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMinUsed(),
                       ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMaxUsed());
    algorithm.setProgressRange(startProgress, endProgress);
    algorithm.execute(false);
    if (!worker.getSTOP()) super.execute(alignment);
  }
  public String getStringID() {
    return "AG" + TGlobalAlignmentAlgorithmFactory.getID(algo) + "|" + job.getStringID();
  }
  public void saveInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    TGlobalAlignmentAlgorithmFactory factory = new TGlobalAlignmentAlgorithmFactory();
    TGlobalAlignmentAlgorithm ga = (TGlobalAlignmentAlgorithm)factory.createAlgorithm(algo);
    ga.initWithLast();
    String s = base + "<job_elem id=\"AG" + TGlobalAlignmentAlgorithmFactory.getID(algo) + "\" params=\"" + ga.getStringParams() + "\" />";
    try {
      job.saveInfo(bw, indent);
      bw.write(s);
      bw.newLine();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
    return algorithm;
  }
  public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
    return job.getLocalAlignmentAlgorithm();
  }
}
