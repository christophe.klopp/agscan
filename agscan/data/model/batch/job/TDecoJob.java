package agscan.data.model.batch.job;

import java.util.Vector;

import agscan.data.controler.worker.TBatchWorker;
import agscan.data.controler.worker.TBatchWorkerNImages;
import agscan.data.element.alignment.TAlignment;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public abstract class TDecoJob extends TAbstractJob {
  protected TAbstractJob job;
  protected Vector params;

  public TDecoJob(TAbstractJob job, int sp, int ep) {
    super(sp, ep);
    this.job = job;
  }
  public void execute(TAlignment alignment) {
    job.execute(alignment);
  }
  public int getCurrentState() {
    return job.getCurrentState();
  }
  public TAbstractJob getJob() {
    return job;
  }
  public void setWorker(TBatchWorkerNImages worker) {
    this.worker = worker;
    if (job != null) job.setWorker(worker);
  }
}
