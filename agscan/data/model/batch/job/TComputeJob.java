package agscan.data.model.batch.job;

import java.io.BufferedWriter;

import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.TAlignmentModel;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPluginManager;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TComputeJob extends TDecoJob {
  private String algo;

  public TComputeJob(String algo, TAbstractJob job, int sp, int ep) {
    super(job, sp, ep);
    this.algo = algo;
  }
  public int getProgressionUnits() {
    return getLocalProgressionUnits() + job.getProgressionUnits();
  }
  public int getLocalProgressionUnits() {
    return 1;
  }
  public void execute(TAlignment alignment) {
    System.err.println("Calcul : " + algo + " - " + alignment.getName());
    String id = getLocalStringID();
    if (id.equals("QIC")) {
      TQuantifImageConstantAlgorithm qicAlgo = new TQuantifImageConstantAlgorithm((TAlignmentModel)alignment.getModel());
      qicAlgo.setProgressRange(startProgress, endProgress);
      qicAlgo.execute(false);
    }
    else if (id.equals("QIV")) {
      TQuantifImageComputedAlgorithm qivAlgo = new TQuantifImageComputedAlgorithm((TAlignmentModel)alignment.getModel());
      qivAlgo.setProgressRange(startProgress, endProgress);
      qivAlgo.execute(false);
    }
    else if (id.equals("QFC")) {
      TQuantifFitConstantAlgorithm qfcAlgo = new TQuantifFitConstantAlgorithm((TAlignmentModel)alignment.getModel());
      qfcAlgo.setProgressRange(startProgress, endProgress);
      qfcAlgo.execute(false);
    }
    else if (id.equals("QFV")) {
      TQuantifFitComputedAlgorithm qfvAlgo = new TQuantifFitComputedAlgorithm((TAlignmentModel)alignment.getModel());
      qfvAlgo.setProgressRange(startProgress, endProgress);
      qfvAlgo.execute(false);
    }
    else if (id.equals("QM")) {
      TComputeQMAlgorithm qmAlgo = new TComputeQMAlgorithm((TAlignmentModel)alignment.getModel());
      qmAlgo.setProgressRange(startProgress, endProgress);
      qmAlgo.execute(false);
    }
    else if (id.equals("CF")) {
      TComputeFitCorrectionAlgorithm fcAlgo = new TComputeFitCorrectionAlgorithm((TAlignmentModel)alignment.getModel());
      fcAlgo.setProgressRange(startProgress, endProgress);
      fcAlgo.execute(false);
    }
    else if (id.equals("CO")) {
      TComputeOvershiningCorrectionAlgorithm ocAlgo = new TComputeOvershiningCorrectionAlgorithm((TAlignmentModel)alignment.getModel());
      ocAlgo.setProgressRange(startProgress, endProgress);
      ocAlgo.execute(false);
    }
  
    /**** Lancements des plugins ****/
    else if (id.substring(0,2).equals("QP"))
    {
    	int i = Integer.parseInt(id.substring(2,4));
    	((SQuantifPlugin)TPluginManager.plugs[i]).run();
    }
    else if (id.substring(0,2).equals("AP"))
    {
    	int i = Integer.parseInt(id.substring(2,4));
    	((SAlignPlugin)TPluginManager.plugs[i]).run();
    }
    
    
    if (!worker.getSTOP()) super.execute(alignment);
  }
  public String getStringID() {
    return getLocalStringID() + "|" + job.getStringID();
  }
  private String getLocalStringID() {
    String s = "";
    if (algo.equals(TComputeFitCorrectionAlgorithm.NAME))
      s = "CF";
    else if (algo.equals(TComputeOvershiningCorrectionAlgorithm.NAME))
      s = "CO";
    else if (algo.equals(TComputeQMAlgorithm.NAME))
      s = "QM";
    else if (algo.equals(TQuantifFitComputedAlgorithm.NAME))
      s = "QFV";
    else if (algo.equals(TQuantifFitConstantAlgorithm.NAME))
      s = "QFC";
    else if (algo.equals(TQuantifImageComputedAlgorithm.NAME))
      s = "QIV";
    else if (algo.equals(TQuantifImageConstantAlgorithm.NAME))
      s = "QIC";
    // PLUGINS
    else if(algo.substring(0,1).equals("Q"))s = "QP"+algo.substring(1,3);
    else if(algo.substring(0,1).equals("A"))s = "AP"+algo.substring(1,3);
    return s;
  }
  public void saveInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    String s = base + "<job_elem id=\"" + getLocalStringID() + "\" />";
    try {
      job.saveInfo(bw, indent);
      bw.write(s);
      bw.newLine();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
    return job.getGlobalAlignmentAlgorithm();
  }
  public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
    return job.getLocalAlignmentAlgorithm();
  }
}
