package agscan.data.model.batch.job;

import java.io.BufferedWriter;

import agscan.TEventHandler;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.data.controler.TImageControler;
import agscan.data.controler.memento.TImageAction;
import agscan.data.controler.worker.TTransposeWorker;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TTreatmentJob extends TDecoJob {
	private int treatment;
	
	public TTreatmentJob(int tr, TAbstractJob job, int sp, int ep) {
		super(job, sp, ep);
		treatment = tr;
	}
	public int getProgressionUnits() {
		return getLocalProgressionUnits() + job.getProgressionUnits();
	}
	public int getLocalProgressionUnits() {
		return 1;
	}
	public void execute(TAlignment alignment) {
		TEvent event;
		switch (treatment) {
		case TImageControler.ROTATE_PLUS_90:
			//TTransposeWorker worker = new TTransposeWorker(alignment.getImage(), TransposeDescriptor.ROTATE_90, "Rotation +90 ...");//modif post integration 14/09/05
			TTransposeWorker worker = new TTransposeWorker(alignment.getImage(), "90_clockwise" , "Rotation +90 ...");
		worker.setPriority(Thread.MIN_PRIORITY);
		TImage image = (TImage)worker.construct(false);
		worker.finished();
		alignment.setImage(image);
		TImageAction ia = new TImageAction(90, false, false);
		alignment.getImage().getImageControler().addMemento(ia);
		alignment.getImage().getModel().addModif(1);
		event = new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress));
		TEventHandler.handleMessage(event);
		break;
		case TImageControler.ROTATE_MOINS_90:
			//worker = new TTransposeWorker(alignment.getImage(), TransposeDescriptor.ROTATE_270, "Rotation -90 ...");
			worker = new TTransposeWorker(alignment.getImage(), "90_counter_clockwise", "Rotation -90 ...");
		worker.setPriority(Thread.MIN_PRIORITY);
		image = (TImage)worker.construct(false);
		worker.finished();
		alignment.setImage(image);
		ia = new TImageAction(-90, false, false);
		alignment.getImage().getImageControler().addMemento(ia);
		alignment.getImage().getModel().addModif(1);
		event = new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress));
		TEventHandler.handleMessage(event);
		break;
		case TImageControler.ROTATE_180:
			//worker = new TTransposeWorker(alignment.getImage(), TransposeDescriptor.ROTATE_180, "Rotation 180 ...");
			worker = new TTransposeWorker(alignment.getImage(),"180", "Rotation 180 ...");
		worker.setPriority(Thread.MIN_PRIORITY);
		image = (TImage)worker.construct(false);
		worker.finished();
		alignment.setImage(image);
		ia = new TImageAction(180, false, false);
		alignment.getImage().getImageControler().addMemento(ia);
		alignment.getImage().getModel().addModif(1);
		event = new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress));
		TEventHandler.handleMessage(event);
		break;
		case TImageControler.HORIZONTAL_FLIP:
	    //worker = new TTransposeWorker(alignment.getImage(), TransposeDescriptor.FLIP_HORIZONTAL, "Retournement horizontal ...");
		worker = new TTransposeWorker(alignment.getImage(),"flip_horizontal", "Retournement horizontal ...");
		worker.setPriority(Thread.MIN_PRIORITY);
		image = (TImage)worker.construct(false);
		worker.finished();
		alignment.setImage(image);
		ia = new TImageAction(0, true, false);
		alignment.getImage().getImageControler().addMemento(ia);
		alignment.getImage().getModel().addModif(1);
		event = new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress));
		TEventHandler.handleMessage(event);
		break;
		case TImageControler.VERTICAL_FLIP:
			//worker = new TTransposeWorker(alignment.getImage(), TransposeDescriptor.FLIP_VERTICAL, "Retournement vertical ...");
			worker = new TTransposeWorker(alignment.getImage(), "flip_vertical", "Retournement vertical ...");
		worker.setPriority(Thread.MIN_PRIORITY);
		image = (TImage)worker.construct(false);
		worker.finished();
		alignment.setImage(image);
		ia = new TImageAction(0, false, true);
		alignment.getImage().getImageControler().addMemento(ia);
		alignment.getImage().getModel().addModif(1);
		event = new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(endProgress));
		TEventHandler.handleMessage(event);
		break;
		}
		if (!worker.getSTOP()) super.execute(alignment);
	}
	public String getStringID() {
		return getLocalStringID() + "|" + job.getStringID();
	}
	private String getLocalStringID() {
		String s = "";
		switch (treatment) {
		case TImageControler.ROTATE_PLUS_90:
			s = "R+90";
		break;
		case TImageControler.ROTATE_MOINS_90:
			s = "R-90";
		break;
		case TImageControler.ROTATE_180:
			s = "R180";
		break;
		case TImageControler.HORIZONTAL_FLIP:
			s = "FH";
		break;
		case TImageControler.VERTICAL_FLIP:
			s = "FV";
		break;
		}
		return s;
	}
	public void saveInfo(BufferedWriter bw, int indent) {
		String base = "";
		for (int i = 0; i < indent; i++) base += " ";
		String s = base + "<job_elem id=\"" + getLocalStringID() + "\" />";
		try {
			job.saveInfo(bw, indent);
			bw.write(s);
			bw.newLine();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
		return job.getGlobalAlignmentAlgorithm();
	}
	public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
		return job.getLocalAlignmentAlgorithm();
	}
}
