package agscan.data.model.batch.job;

import java.io.BufferedWriter;
import java.util.Vector;

import agscan.algo.factory.TLocalAlignmentAlgorithmFactory;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.view.TAlignmentView;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @author rcathelin
 * @since 2005/11/10
 * @version 2.0
 */
public class TLocalAlignmentJob extends TDecoJob {
  private String algo;
  private TLocalAlignmentAlgorithm algorithm;

  public TLocalAlignmentJob(String algo, Vector par, TAbstractJob job, int sp, int ep) {
    super(job, sp, ep);
    this.algo = algo;
    params = par;
    TLocalAlignmentAlgorithmFactory factory = new TLocalAlignmentAlgorithmFactory();
    algorithm = (TLocalAlignmentAlgorithm)factory.createAlgorithm(algo);
    //System.err.println(params.toArray().toString());
    algorithm.init(params.toArray());
  }
  public int getProgressionUnits() {
    return getLocalProgressionUnits() + job.getProgressionUnits();
  }
  public int getLocalProgressionUnits() {
    return 8;
  }
  public void execute(TAlignment alignment) {
    TAlignmentModel alignmentModel = (TAlignmentModel)alignment.getModel();
    algorithm.initData(alignmentModel.getImageModel().getPixelWidth(), alignmentModel.getImageModel().getPixelHeight(),
                       alignmentModel.getImageModel().getMaxImageData(),  alignmentModel.getImageModel().getElementWidth(),
                       alignmentModel.getImageModel().getElementHeight(), alignmentModel.getGridModel(),
                       (TAlignmentView)alignmentModel.getReference().getView(),
                       ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMinUsed(),
                       ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMaxUsed());
    algorithm.setProgressRange(startProgress, endProgress);
    algorithm.execute(false, Thread.MIN_PRIORITY);
    if (!worker.getSTOP()) super.execute(alignment);
  }
  public String getStringID() {
    return "AL" + TLocalAlignmentAlgorithmFactory.getID(algo) + "|" + job.getStringID();
  }
  public void saveInfo(BufferedWriter bw, int indent) {
    String base = "";
    for (int i = 0; i < indent; i++) base += " ";
    TLocalAlignmentAlgorithmFactory factory = new TLocalAlignmentAlgorithmFactory();
    TLocalAlignmentAlgorithm ga = (TLocalAlignmentAlgorithm)factory.createAlgorithm(algo);
    ga.initWithLast();
    String s = base + "<job_elem id=\"AL" + TLocalAlignmentAlgorithmFactory.getID(algo) + "\" params=\"" + ga.getStringParams() + "\" />";
    try {
      job.saveInfo(bw, indent);
      bw.write(s);
      bw.newLine();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
    return job.getGlobalAlignmentAlgorithm();
  }
  public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
    return algorithm;
  }
}
