package agscan.data.model.batch.job;

import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.grid.TGrid;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 *
 * @version 2.0
 */
public class TJob extends TAbstractJob {
  private TGrid grid;
  private String imageName, path;
  private TAlignment alignment;
  private boolean saveImage;

  public TJob() {
    super(0, 0);
    imageName = "";
    path = "";
    grid = null;
    saveImage = false;
  }
  public int getProgressionUnits() {
    return 0;
  }
  public TJob(String image, TGrid grid, boolean si) {
    super(0, 0);
    String pathSeparator = System.getProperty("file.separator");
    int ix = image.lastIndexOf(pathSeparator);
    imageName = image.substring(ix + 1);
    path = image.substring(0, ix);
    this.grid = grid;
    saveImage = si;
  }

  public String getImageName() {
    return imageName;
  }
  public String getPath() {
    return path;
  }
  public String getGridName() {
    return grid.getName();
  }
  public void setImageName(String s) {
    String pathSeparator = System.getProperty("file.separator");
    int ix = s.lastIndexOf(pathSeparator);
    imageName = s.substring(ix + 1);
    path = s.substring(0, ix);
  }
  public void setGrid(TGrid g) {
    grid = g;
  }
  public void execute(TAlignment alignment) {
    System.err.println("Execution : fin de la liste !");
  }
  public int getCurrentState() {
    return 0;
  }
  public TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm() {
    return null;
  }
  public TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm() {
    return null;
  }
}
