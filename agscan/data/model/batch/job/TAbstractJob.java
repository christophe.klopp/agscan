package agscan.data.model.batch.job;

import java.io.BufferedWriter;

import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.data.controler.worker.TBatchWorker;
import agscan.data.controler.worker.TBatchWorkerNImages;
import agscan.data.element.alignment.TAlignment;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public abstract class TAbstractJob {
  public static final int DONE = -1;
  public static final int QUEUED = -2;
  public static final int FAILED = -3;
  public static final int WARNING = -4;

  protected int startProgress, endProgress;
  protected TBatchWorkerNImages worker;
  //protected TBatchWorker worker;

  public TAbstractJob(int sp, int ep) {
    super();
    worker = null;
    startProgress = sp;
    endProgress = ep;
  }
  public abstract int getCurrentState();
  public abstract TGlobalAlignmentAlgorithm getGlobalAlignmentAlgorithm();
  public abstract TLocalAlignmentAlgorithm getLocalAlignmentAlgorithm();
  public abstract void execute(TAlignment alignment);
  public abstract int getProgressionUnits();
  public TAbstractJob getJob() {
    return null;
  }
  public int getLocalProgressionUnits() {
    return 0;
  }
  public void setProgressionRange(int sp, int ep) {
    startProgress = sp;
    endProgress = ep;
  }
  public String getStringID() {
    return "";
  }
  public void saveInfo(BufferedWriter bw, int indent) {

  }
  public void setWorker(TBatchWorkerNImages worker) {
    this.worker = worker;
  }
  
  /*public void setWorker(TBatchWorker worker) {
	    //this.worker = worker;
  }*/
  
  public TBatchWorkerNImages getWorker() {
    return worker;
  }
  
  /*public TBatchWorker getWorker() {
	    return worker;
  }*/
}
