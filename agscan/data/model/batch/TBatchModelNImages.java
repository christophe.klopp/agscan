package agscan.data.model.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import agscan.AGScan;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.model.TModelImpl;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.batch.job.TJobBuilder;
import agscan.data.model.batch.table.TImageNameColumn;
import agscan.data.model.batch.table.TProgressBarColumn;
import agscan.data.view.TBatchComPanel;
import agscan.data.view.table.TBatchTablePanel;
import agscan.data.controler.TBatchControler;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 *
 * @version 2.0
 */
public class TBatchModelNImages extends TModelImpl {
  private Vector progressValues;
  private Vector[] images;
  private TGrid grid;
  private TAbstractJob job;
  private DefaultTableColumnModel columns;
  private boolean saveImages;
  private int nbChannels;

  public TBatchModelNImages(TBatch ref) {
    super();
    nbChannels = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");//TBatchComPanel.getNbChan();
    images = new Vector[nbChannels];
    for(int i=0;i<nbChannels;i++)
    {
    	images[i] = new Vector();
    }
    progressValues = new Vector();
    grid = null;
    reference = ref;
    columns = new DefaultTableColumnModel();
    columns.addColumn(new TableColumn(0));
    for(int i=0;i<nbChannels;i++)
    {
    	columns.addColumn(new TImageNameColumn(columns.getColumnCount(),i+1));
    }
    columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));
    job = TJobBuilder.createDefaultJob();
    //TJobBuilder.createJob();
  }
  
  public TBatchModelNImages(TBatch ref, TGrid grid, Vector[] v_images) {
	this(ref);
	nbChannels = v_images.length;
    for(int i=0;i<nbChannels;i++)
    { 
    	for(int j=0;j<v_images[i].size();j++)
    	{
    		addImage(v_images[i].elementAt(j).toString(),i+1);
        }
    }
    this.grid = grid;
  }
  
  public TBatchModelNImages(TBatch ref, TGrid grid, Vector[] v_images, Enumeration enuStatus) {
    Object o;
    nbChannels =  v_images.length;
    images = new Vector[nbChannels];
    for(int i=0;i<nbChannels;i++)
    {
    	images[i] = new Vector();
    }
    progressValues = new Vector(); 
      for(int i=0;i<nbChannels;i++)
      {
    	  for(int j=0;j<v_images[i].size();j++)
      	  {
    		  o = v_images[i].elementAt(j);
		      addImage(o.toString(), ((Integer)enuStatus.nextElement()).intValue(),i+1);
          }
    		      
      }
    this.grid = grid;
    reference = ref;
    columns = new DefaultTableColumnModel();
    columns.addColumn(new TableColumn(0));
    for(int i=0;i<nbChannels;i++)
    {
    	columns.addColumn(new TImageNameColumn(columns.getColumnCount(),i+1));
    } 
    columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));
    job = TJobBuilder.createDefaultJob();
    //TJobBuilder.createJob();
  }
  
  
   //added 2005/11/04
  // allows to add columns to the batch table for multi channels experiment
  private void addChannelsColumns(int nb){
  	System.out.println("on add il faut nb="+nb);
  	for (int i=0;i<nb;i++){
  	  columns.addColumn(new TImageNameColumn(columns.getColumnCount(),i+1));
  	}
  	//if (nb==2)columns.addColumn(new TableColumn(columns.getColumnCount()));
  }
  //create a  new structure of columns....adapted with channels number
  public void setChannelsColumns(int nb){
	 columns = new DefaultTableColumnModel();
     columns.addColumn(new TableColumn(0));
     addChannelsColumns(nbChannels);
     columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));
     System.out.println("columns.getColumnCount()="+columns.getColumnCount());
 
  }
  
  public void setNbChan(int nb)
  {
	  nbChannels = nb;
	  images = new Vector[nbChannels];
	  for(int i=0;i<nbChannels;i++)
	  {
	    images[i] = new Vector();
	  }
	  setChannelsColumns(nbChannels);
  }
  
  
//modif 2005/11/04
  public void addImage(String image,int channel) {
    addImage(image, TAbstractJob.QUEUED,channel);
  }
//modif 2005/11/04
  public void addImage(String image, int st, int channel) {
    //TODO pas tres generique , a revoir....
	System.out.println("NBCHAN = "+nbChannels);
	images[channel-1].addElement(image);
  	if(channel==1)progressValues.addElement(new Integer(st));
  }
  public void removeJob(int i) {
    for(int j=0;j<nbChannels;j++)
    {
		images[j].removeElementAt(i);
    }
    progressValues.removeElementAt(i);
  }
  
  public void removeJob(Object o) {
    for(int c=0;c<nbChannels;c++)
    {
      if(images[c].contains(o))
    	  removeJob(images[c].indexOf(o));
    }
  }
  public void setGrid(TGrid grid) {
    this.grid = grid;
  }
  public TGrid getGrid() {
    return grid;
  }
  public void setJob(TAbstractJob j) {
    job = j;
  }
  public TAbstractJob getJob() {
    return job;
  }
  public Vector getImages(int i) {
    return images[i];
  }
  
  public Vector getProgressValues() {
    return progressValues;
  }
  public void setSaveImages(boolean b) {
    saveImages = b;
  }
  public boolean getSaveImages() {
    return saveImages;
  }
  public void notifyView() {

  }
  public void setProgressValue(int imageIndex, int val) {
    progressValues.setElementAt(new Integer(val), imageIndex);
  }
  
  
  // C'EST ICI L'affichage de la colonne 
  //This overide method allows to update columns of the table
  public Object getValueAt(int rowIndex, int columnIndex) {  	
    
	Object ret = null;
    String pathSeparator = System.getProperty("file.separator");
    String s;
    if(columnIndex==0)
    {
        ret = Integer.toString(rowIndex + 1);
    }
    else if (columnIndex == nbChannels+1)
    {
      	ret = progressValues.elementAt(rowIndex);
    }
    else
    {
      	if (!images[columnIndex-1].isEmpty()){
      		//System.out.println("bof...");
      		s = images[columnIndex-1].elementAt(rowIndex).toString();
      		ret = s.substring(s.lastIndexOf(pathSeparator) + 1);
      	}
    }
    return ret;
  }
  public int getRowCount() {
    return images[0].size();
  }
  public int getColumnCount() {
    return nbChannels;
  }
  public int getElementWidth() {
    return 0;
  }
  public int getElementHeight() {
    return 0;
  }
  public DefaultTableColumnModel getColumns() {
  	System.out.println("get COLUMNS!!!="+columns.getColumnCount());
    return columns;
  }
  public String getGridName() {
    if (grid != null)
      return grid.getName();
    return "";
  }
  public String getJobString() {
    return job.getStringID();
  }
  public void clearJobs() {
    
	for(int i=0;i<nbChannels;i++)
	{
		images[i].clear();
	}
    progressValues.clear();
  }
  public String getImageName(int i, int chan) {
    return (String)images[chan].elementAt(i);
  }
  
  public void setImageName(int i, int chan, String newname)
  {
	  images[chan].set(i,newname);
  }
 
  public String getImagePath(int i,int chan) {
    String pathSeparator = System.getProperty("file.separator");
    String s = (String)images[chan].elementAt(i);
    return s.substring(0, s.lastIndexOf(pathSeparator));
  }
  
  public void removeJobs(int[] rows) {
    Vector v = new Vector();
    for (int i = 0; i < rows.length; i++) 
    {
    	for(int c=0;c<nbChannels;c++)
    	{
    		v.addElement(images[c].elementAt(rows[i]));
    	}
    }
    Enumeration enu = v.elements();
    while (enu.hasMoreElements()) removeJob(enu.nextElement());
  }
  public void resetJobs(int[] rows) {
    for (int i = 0; i < rows.length; i++) progressValues.set(rows[i], new Integer(TAbstractJob.QUEUED));
  }
  public void save(File file) {
    String s;
    int i;
    System.err.println("Enregistre le batch dans : " + file.getAbsolutePath());
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      Calendar rightNow = Calendar.getInstance();
      bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
      bw.newLine();
      bw.write("<batch save_images=\"" + saveImages + "\" >");
      bw.newLine();
      bw.write("  <infos>");
      bw.newLine();
      bw.write("    user=\"" + System.getProperty("user.name") + "\"");
      bw.newLine();
      bw.write("    os=\"" + System.getProperty("os.name") + "\"");
      bw.newLine();
      bw.write("    java=\"" + System.getProperty("java.runtime.version") + "\"");
      bw.newLine();
      DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRANCE);
      bw.write("    date=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      df = DateFormat.getTimeInstance(DateFormat.LONG, Locale.FRANCE);
      bw.write("    time=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      bw.write("  </infos>");
      bw.newLine();
      bw.write("  <output dir=\"" + ((TBatch)reference).getOutputPath() + "\" />");
      bw.newLine();
      bw.write("  <channels nbchan=\"" + nbChannels + "\" />");
      bw.newLine();
      bw.write("  <saveImages value=\"" + ((TBatchModelNImages)reference.getModel()).getSaveImages() + "\" />");
      bw.newLine();
      bw.write("  <snapShot value=\"" +((TBatchTablePanel)reference.getView().getTablePanel()).getBCP().isSnapSelected()+ "\" />");
      bw.newLine();
      bw.write("  <tiffSave value=\"" +((TBatchTablePanel)reference.getView().getTablePanel()).getBCP().isTiffSelected()+ "\" />");
      bw.newLine();
      bw.write("  <grid>");
      bw.newLine();
      //2005/11/30 - if no grid is choosen, the batch isn't saved
      try{
      grid.saveFileInfo(bw, 4);
      }
      catch (NullPointerException ne){
      	System.out.println("no grid choosen, batch not saved");
      	TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
      			TDialogManager.MESSAGE_DIALOG,Messages.getString("TBatchModel.0"));//externalized since 2006/02/20 
      	TEventHandler.handleMessage(event);		
      	bw.close();
      	System.out.println("file deleted="+file.delete());//the empty file .bzb is deleted
      	return;
      }
      bw.write("  </grid>");
      bw.newLine();
      bw.write("  <job>");
      bw.newLine();
      job.saveInfo(bw, 4);
      bw.write("  </job>");
      bw.newLine();
      bw.write("  <images>");
      bw.newLine();
      String im, st;
      for(int c=0;c<nbChannels;c++)
      {
    	  i = 0;
    	  for (Enumeration enu = images[c].elements(); enu.hasMoreElements(); ) {
	        im = (String)enu.nextElement();
	        if (im.endsWith(".zaf"))
	          st = "alignment";
	        else
	          st = "image";
	        bw.write("    <image channel=\"" + c + "\" type=\"" + st + "\" name=\"" + im + "\" status=\"" + "-2" + "\" />");
	        i++;
	        bw.newLine();
	      }
      }
      bw.write("  </images>");
      bw.newLine();
      bw.write("</batch>");
      bw.newLine();
      bw.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
