package agscan.data.model.batch.table;

import javax.swing.table.TableColumn;

import agscan.data.view.table.TBatchTablePanel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TImageNameColumn extends TableColumn {
	public TImageNameColumn(int i) {
		super(i);
		setHeaderValue("Image");
		setHeaderRenderer(new TBatchTablePanel.BatchTableHeaderRenderer());
	}
	
	//added remi 2005/11/04
	public TImageNameColumn(int i,int channel) {
		super(i);
		setHeaderValue("Image " +channel);
		setHeaderRenderer(new TBatchTablePanel.BatchTableHeaderRenderer());
	}
	
}
