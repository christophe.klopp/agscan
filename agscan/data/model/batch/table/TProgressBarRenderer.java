package agscan.data.model.batch.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTable;

import agscan.data.model.grid.table.TRenderer;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TProgressBarRenderer extends TRenderer {
  public TProgressBarRenderer() {
  }
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    int val = ((Integer)value).intValue();
    JProgressBar sb;
    JLabel lab;
    switch (val) {
      case -1:
        lab = new JLabel("DONE", JLabel.CENTER);
        lab.setBackground(Color.green);
        lab.setOpaque(true);
        return lab;
      case -2:
        lab = new JLabel("WAITING", JLabel.CENTER);
        lab.setBackground(Color.cyan);
        lab.setOpaque(true);
        return lab;
      case -3:
        lab = new JLabel("FAILED", JLabel.CENTER);
        lab.setBackground(Color.red);
        lab.setOpaque(true);
        return lab;
      case -4:
        lab = new JLabel("WARNING", JLabel.CENTER);
        lab.setBackground(Color.magenta);
        lab.setOpaque(true);
        return lab;
      default:
        sb = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
        sb.setPreferredSize(new Dimension(200, 25));
        sb.setValue(((Integer)value).intValue());
        sb.setStringPainted(true);
        sb.setForeground(Color.blue);
        sb.setBackground(Color.white);
        return sb;
    }
  }
}
