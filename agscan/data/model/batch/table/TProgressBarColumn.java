package agscan.data.model.batch.table;

import javax.swing.table.TableColumn;

import agscan.data.view.table.TBatchTablePanel;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TProgressBarColumn extends TableColumn {
  public TProgressBarColumn(int i) {
    super(i);
    setHeaderValue("Status");
    setHeaderRenderer(new TBatchTablePanel.BatchTableHeaderRenderer());
    setCellRenderer(new TProgressBarRenderer());
    setPreferredWidth(150);
  }
}
