package agscan.data.model.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.model.TModelImpl;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.batch.job.TJobBuilder;
import agscan.data.model.batch.table.TImageNameColumn;
import agscan.data.model.batch.table.TProgressBarColumn;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 *
 * @version 2.0
 */
public class TBatchModel extends TModelImpl {
  private Vector images, progressValues, images2,images3;
  private TGrid grid;
  private TAbstractJob job;
  private DefaultTableColumnModel columns;
  private boolean saveImages;

  public TBatchModel(TBatch ref) {
    super();
    images = new Vector();
    images2 = new Vector();//2005/11/04
    images3 = new Vector();//2005/11/04
    progressValues = new Vector();
    grid = null;
    reference = ref;
    columns = new DefaultTableColumnModel();
    columns.addColumn(new TableColumn(0));
  columns.addColumn(new TImageNameColumn(columns.getColumnCount(),1));
    //columns.addColumn(new TImageNameColumn(columns.getColumnCount())); 
     columns.addColumn(new TImageNameColumn(columns.getColumnCount(),2));
     columns.addColumn(new TImageNameColumn(columns.getColumnCount(),3));
     columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));

    job = TJobBuilder.createDefaultJob();
  }
  public TBatchModel(TBatch ref, TGrid grid, Enumeration enuImages) {
    this(ref);
    while (enuImages.hasMoreElements()) {
      addImage(enuImages.nextElement().toString(),1);//modif pour coller avec l'ajout d'un param...
    }
    this.grid = grid;
  }
  public TBatchModel(TBatch ref, TGrid grid, Enumeration enuImages, Enumeration enuStatus) {
    Object o;
    images = new Vector();
    images2 = new Vector();//2005/11/29
    images3 = new Vector();//2005/11/29
    progressValues = new Vector();
    while (enuImages.hasMoreElements()) {
      o = enuImages.nextElement();
      addImage(o.toString(), ((Integer)enuStatus.nextElement()).intValue(),1);//ajout de l'arg 1 en test 2005/11/29
      //TODO modifier au dessus le "1" qui signifie qu'il n'y a un qu'un canal:l'ouverture d'un batch ne marche donc pas en multi-canaux 
    }
    this.grid = grid;
    reference = ref;
    columns = new DefaultTableColumnModel();
    columns.addColumn(new TableColumn(0));
    //columns.addColumn(new TImageNameColumn(columns.getColumnCount()));//modified 2005/11/29
    columns.addColumn(new TImageNameColumn(columns.getColumnCount(),1));//by this one
    columns.addColumn(new TImageNameColumn(columns.getColumnCount(),2));//added 2005/11/29
    columns.addColumn(new TImageNameColumn(columns.getColumnCount(),3));//added 2005/11/29
    columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));
    
    job = TJobBuilder.createDefaultJob();
  }
  
  /*
   //added 2005/11/04
  // allows to add columns to the batch table for multi channels experiment
  private void addChannelsColumns(int nb){
  	System.out.println("on add il faut nb="+nb);
  	for (int i=0;i<nb;i++){
  	  columns.addColumn(new TImageNameColumn(columns.getColumnCount()));
  	}
  	if (nb==2)columns.addColumn(new TableColumn(columns.getColumnCount()));
  }
  //create a  new structure of columns....adapted with channels number
  public void setChannelsColumns(int nb){
  	 columns = new DefaultTableColumnModel();
     columns.addColumn(new TableColumn(0));
     addChannelsColumns(nb);
     columns.addColumn(new TProgressBarColumn(columns.getColumnCount()));
     System.out.println("columns.getColumnCount()="+columns.getColumnCount());
 
  }
  */
  
  
  
//modif 2005/11/04
  public void addImage(String image,int channel) {
    addImage(image, TAbstractJob.QUEUED,channel);
  }
//modif 2005/11/04
  public void addImage(String image, int st, int channel) {
    //TODO pas tres generique , a revoir....
  	if (channel==1){
  		images.addElement(image);
  		progressValues.addElement(new Integer(st));
  	}
  	else if (channel==2) images2.addElement(image);//modif 2005/11/04
  	else if (channel==3) images3.addElement(image);//modif 2005/11/04
    
  }
  public void removeJob(int i) {
    images.removeElementAt(i);
    images2.removeElementAt(i);//modif 2005/11/04
    images3.removeElementAt(i);//modif 2005/11/04
    progressValues.removeElementAt(i);
  }
  public void removeJob(Object o) {
    int i = images.indexOf(o);
    removeJob(i);
  }
  public void setGrid(TGrid grid) {
    this.grid = grid;
  }
  public TGrid getGrid() {
    return grid;
  }
  public void setJob(TAbstractJob j) {
    job = j;
  }
  public TAbstractJob getJob() {
    return job;
  }
  public Vector getImages() {
    return images;
  }
//added 2005/11/04
  public Vector getImages2() {
    return images2;
  }
//added 2005/11/04
  public Vector getImages3() {
    return images3;
  }
  public Vector getProgressValues() {
    return progressValues;
  }
  public void setSaveImages(boolean b) {
    saveImages = b;
  }
  public boolean getSaveImages() {
    return saveImages;
  }
  public void notifyView() {

  }
  public void setProgressValue(int imageIndex, int val) {
    progressValues.setElementAt(new Integer(val), imageIndex);
  }
  
  
  // C'EST ICI L'affichage de la colonne 
  //This overide method allows to update columns of the table
  public Object getValueAt(int rowIndex, int columnIndex) {  	
    Object ret = null;
    String pathSeparator = System.getProperty("file.separator");
    String s;
    switch (columnIndex) {
      case 0:
        ret = Integer.toString(rowIndex + 1);
        break;
      case 1:
        s = images.elementAt(rowIndex).toString();
        ret = s.substring(s.lastIndexOf(pathSeparator) + 1);
        break;
      case 4:
      	ret = progressValues.elementAt(rowIndex);
      	break;
      case 2:
      	if (!images2.isEmpty()){
      		System.out.println("bof...");
      		s = images2.elementAt(rowIndex).toString();
      		ret = s.substring(s.lastIndexOf(pathSeparator) + 1);
      	}
      	break;
      case 3:
      	if (!images3.isEmpty()){ 
      		s = images3.elementAt(rowIndex).toString();
      		ret = s.substring(s.lastIndexOf(pathSeparator) + 1);
      	}
      	break;
    }
    return ret;
  }
  public int getRowCount() {
    return images.size();
  }
  public int getColumnCount() {
    return 3;
  }
  public int getElementWidth() {
    return 0;
  }
  public int getElementHeight() {
    return 0;
  }
  public DefaultTableColumnModel getColumns() {
  	System.out.println("get COLUMNS!!!="+columns.getColumnCount());
    return columns;
  }
  public String getGridName() {
    if (grid != null)
      return grid.getName();
    return "";
  }
  public String getJobString() {
    return job.getStringID();
  }
  public void clearJobs() {
    images.clear();
    images2.clear();//2005/11/04
    images3.clear();//2005/11/04
    progressValues.clear();
  }
  public String getImageName(int i) {
    return (String)images.elementAt(i);
  }
//2005/11/04
  public String getImage2Name(int i) {
    return (String)images2.elementAt(i);
  }
//2005/11/04
  public String getImage3Name(int i) {
    return (String)images3.elementAt(i);
  }

  public String getImagePath(int i) {
    String pathSeparator = System.getProperty("file.separator");
    String s = (String)images.elementAt(i);
    return s.substring(0, s.lastIndexOf(pathSeparator));
  }
//2005/11/04
  public String getImage2Path(int i) {
    String pathSeparator = System.getProperty("file.separator");
    String s = (String)images2.elementAt(i);
    return s.substring(0, s.lastIndexOf(pathSeparator));
  }
//2005/11/04
  public String getImage3Path(int i) {
    String pathSeparator = System.getProperty("file.separator");
    String s = (String)images3.elementAt(i);
    return s.substring(0, s.lastIndexOf(pathSeparator));
  }
  public void removeJobs(int[] rows) {
    Vector v = new Vector();
    for (int i = 0; i < rows.length; i++) v.addElement(images.elementAt(rows[i]));
    Enumeration enu = v.elements();
    while (enu.hasMoreElements()) removeJob(enu.nextElement());
  }
  public void resetJobs(int[] rows) {
    for (int i = 0; i < rows.length; i++) progressValues.set(rows[i], new Integer(TAbstractJob.QUEUED));
  }
  public void save(File file) {
    String s;
    int i;
    System.err.println("Enregistre le batch dans : " + file.getAbsolutePath());
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
      Calendar rightNow = Calendar.getInstance();
      bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
      bw.newLine();
      bw.write("<batch save_images=\"" + saveImages + "\" >");
      bw.newLine();
      bw.write("  <infos>");
      bw.newLine();
      bw.write("    user=\"" + System.getProperty("user.name") + "\"");
      bw.newLine();
      bw.write("    os=\"" + System.getProperty("os.name") + "\"");
      bw.newLine();
      bw.write("    java=\"" + System.getProperty("java.runtime.version") + "\"");
      bw.newLine();
      DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRANCE);
      bw.write("    date=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      df = DateFormat.getTimeInstance(DateFormat.LONG, Locale.FRANCE);
      bw.write("    time=\"" + df.format(new Date()) + "\"");
      bw.newLine();
      bw.write("  </infos>");
      bw.newLine();
      bw.write("  <output dir=\"" + ((TBatch)reference).getOutputPath() + "\" />");
      bw.newLine();
      bw.write("  <saveImages value=\"" + ((TBatchModel)reference.getModel()).getSaveImages() + "\" />");
      bw.newLine();
      bw.write("  <grid>");
      bw.newLine();
      //2005/11/30 - if no grid is choosen, the batch isn't saved
      try{
      grid.saveFileInfo(bw, 4);
      }
      catch (NullPointerException ne){
      	System.out.println("no grid choosen, batch not saved");
      	TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
      			TDialogManager.MESSAGE_DIALOG,Messages.getString("TBatchModel.0"));//externalized since 2006/02/20 
      	TEventHandler.handleMessage(event);		
      	bw.close();
      	System.out.println("file deleted="+file.delete());//the empty file .bzb is deleted
      	return;
      }
      bw.write("  </grid>");
      bw.newLine();
      bw.write("  <job>");
      bw.newLine();
      job.saveInfo(bw, 4);
      bw.write("  </job>");
      bw.newLine();
      bw.write("  <images>");
      bw.newLine();
      String im, st;
      i = 0;
      for (Enumeration enu = images.elements(); enu.hasMoreElements(); ) {
        im = (String)enu.nextElement();
        if (im.endsWith(".zaf"))
          st = "alignment";
        else
          st = "image";
        bw.write("    <image type=\"" + st + "\" name=\"" + im + "\" status=\"" + progressValues.elementAt(i).toString() + "\" />");
        i++;
        bw.newLine();
      }
      bw.write("  </images>");
      bw.newLine();
      bw.write("</batch>");
      bw.newLine();
      bw.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
