package agscan.data.model;

import java.io.File;

import javax.swing.table.AbstractTableModel;

import agscan.data.element.TDataElement;

public abstract class TModelImpl extends AbstractTableModel {
  protected TDataElement reference;
  protected int nbModif;
  protected double pixelWidth, pixelHeight;//TODO a garder?

  public TModelImpl() {
    nbModif = 0;
    reference = null;
    pixelWidth = pixelHeight = 25.0D;//TODO a garder ?
  }
  public abstract void save(File file);
  public abstract void notifyView();
  public abstract Object getValueAt(int rowIndex, int columnIndex);
  public abstract int getRowCount();
  public abstract int getColumnCount();
  public TDataElement getReference() {
    return reference;
  }
  public void setReference(TDataElement e) {
    reference = e;
  }
  public void addModif(int n) {
    nbModif += n;
	//System.out.println("ajout de 1 modif!");
  }
  public int getModif() {
    return nbModif;
  }
  public void clearModifs() {
    nbModif = 0;
  }
  public abstract int getElementWidth();
  public abstract int getElementHeight();
  
  public double getPixelWidth() {
    return pixelWidth;// dans TImageModel, on appelle plutot la valeur de l'imagePlus associée
  }
  public double getPixelHeight() {
    return pixelHeight;// dans TImageModel, on appelle plutot la valeur de l'imagePlus associée
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
