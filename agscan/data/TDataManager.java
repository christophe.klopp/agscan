package agscan.data;

import java.awt.Color;
import java.awt.Cursor;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TControlerImpl;
import agscan.data.element.TDataElement;
import agscan.data.element.image.TImage;
import agscan.event.TBZMessageListener;
import agscan.event.TEvent;
import agscan.ioxml.TBatchReader;
import agscan.ioxml.TLoadGridWorker;
import agscan.ioxml.TOpenAlignmentWorker;
import agscan.ioxml.TOpenFileWorker;
import agscan.ioxml.TOpenGridWorker;
import agscan.statusbar.TStatusBar;
import agscan.data.element.batch.TBatch;

public class TDataManager implements TBZMessageListener {
	public static final int OPEN_IMAGE = -1;
	public static final int OPEN_GRID = -2;
	public static final int REMOVE_CURRENT_DATA = -3;
	public static final int GET_CURRENT_DATA = -4;
	public static final int ADD_DATA_ELEMENT = -5;
	public static final int CLOSE_ALL = -6;
	public static final int SET_CURRENT_DATA = -7;
	public static final int IS_DATA_OPENED = -8;
	public static final int LOAD_GRID = -9;
	public static final int CHANGE_CURRENT_DATA = -10;
	public static final int OPEN_ALIGNMENT = -11;
	// ajouts integration 14/09/05
	public static final int OPEN_BATCH = -12;
	public static final int NEW_BATCH = -13;
//	public static final int OPEN_FILE = -14;//never used ? =>remi 07/10/05
	public static final int CHANGE_BATCH = -15;	
	
	
	private TDataElement currentDataElement;
	private Vector data;
	
	public TDataManager() {
		data = new Vector();
		currentDataElement = null;
	}
	public Object[] BZMessageResponse(TEvent event) {
		int action = event.getAction();
	//	System.out.println("action dans TDataManager= "+action);//test remi 16/05/05
		Object[] ret = null;
		switch (action) {
		case OPEN_IMAGE:
			System.out.println("action OPEN_IMAGE= "+action); //$NON-NLS-1$
			//openImage((File)event.getParam(1));//avt
			//modif fluo
			//TODO pourquoi qd un fichier image, event prend 2 param (le 1er etant a null)?

			openImage((File[])event.getParam(1));

			//
			break;
		case OPEN_GRID:
			openGrid((File)event.getParam(1));
			break;
		case OPEN_ALIGNMENT :
			openAlignment((File)event.getParam(1));
			break;
		case OPEN_BATCH:
			openBatch((File)event.getParam(1));
			break;
		case REMOVE_CURRENT_DATA :
			if (currentDataElement != null) removeDataElement(currentDataElement);
			//currentDataElement = null;//remi test
			//System.gc();//remi test
			break;
		case GET_CURRENT_DATA :
			ret = new Object[1];
			ret[0] = currentDataElement;
			break;
		case ADD_DATA_ELEMENT :
			addDataElement((TDataElement)event.getParam(1));
			break;
		case CLOSE_ALL :
			removeAll();
			break;
		case SET_CURRENT_DATA :
			currentDataElement = (TDataElement)event.getParam(1);
			break;
		case IS_DATA_OPENED :
			ret = new Object[1];
			ret[0] = new Boolean(isDataOpened(((File)event.getParam(1)).getAbsolutePath()));
			break;
		case LOAD_GRID :
			loadGrid((File)event.getParam(1), (TImage)event.getParam(2));
			break;
		case CHANGE_CURRENT_DATA:
			changeCurrentData((TDataElement)event.getParam(1));
			break;
		case NEW_BATCH:
			newBatch();
			break;
		case CHANGE_BATCH:
			//changeBatch();	
		default :
			//TODO faire qqchose ici...
			//if (action>0) 
			// 	System.out.println("action dans TDataManager= "+action);//test remi 30/05/05    
			// ajout REMI plugins
			//	if (action<0){
			//System.out.println("dans le DEFAULT");
			if (event.getParam(0) == null)
				event.setParam(0, currentDataElement);
		if (event.getParam(0) != null)
			ret = ((TDataElement)event.getParam(0)).getControler().processEvent(event);
		else if (currentDataElement != null)
			ret = currentDataElement.getControler().processEvent(event);
		//}
		//else //ajout cas plugin => si positif ET different des valeurs traités auparavant
		// alors le traitement sera dans le plugin . on peut donner comme param[0] le plugin
		// et le action Performed du plugin est appelé
		
		//((TPlugin)event.getParam(0)).actionPerformed(null);
		break;
		}
		return ret;
	}
	private boolean isDataOpened(String s) {
		TDataElement elem;
		for (Enumeration enume = data.elements(); enume.hasMoreElements(); ) {
			elem = (TDataElement)enume.nextElement();
			if (s.equals(elem.getPath() + elem.getName())) return true;
		}
		return false;
	}
	

	public void openImage(File[] f) {
		System.out.println("=> openImage de TDataManager"); //$NON-NLS-1$
		String s = "";
		for (int i = 0; i < f.length; i++){
			s = s + f[i].getAbsolutePath();
		}
	
		TDataElement element;
		for (Enumeration enume = data.elements(); enume.hasMoreElements(); ) {
			element = (TDataElement)enume.nextElement();
			if (s.equals(element.getPath() + element.getName())) {
				TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.SELECT_VIEW, element.getView());
				TEventHandler.handleMessage(event);
				return;
			}
		}
		TOpenFileWorker worker = new TOpenFileWorker(f);
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	
	public void openGrid(File file) {
		String s = file.getAbsolutePath();
		TDataElement element;
		for (Enumeration enume = data.elements(); enume.hasMoreElements(); ) {
			element = (TDataElement)enume.nextElement();
			if (s.equals(element.getPath() + element.getName())) {
				TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.SELECT_VIEW, element.getView());
				TEventHandler.handleMessage(event);
				return;
			}
		}
		TOpenGridWorker worker = new TOpenGridWorker(file);
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	public void openAlignment(File file) {
		String s = file.getAbsolutePath();
		TDataElement element;
		for (Enumeration enume = data.elements(); enume.hasMoreElements(); ) {
			element = (TDataElement)enume.nextElement();
			if (s.equals(element.getPath() + element.getName())) {
				TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.SELECT_VIEW, element.getView());
				TEventHandler.handleMessage(event);
				return;
			}
		}
		TOpenAlignmentWorker worker = new TOpenAlignmentWorker(file);
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	//ajout integration 14/09/05
	public void openBatch(File file) {
		String s = file.getAbsolutePath();
		TDataElement element;
		for (Enumeration enume = data.elements(); enume.hasMoreElements(); ) {
			element = (TDataElement)enume.nextElement();
			if (s.equals(element.getPath().concat(element.getName()))) {
				TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.SELECT_VIEW, element.getView());
				TEventHandler.handleMessage(event);
				return;
			}
		}
		//System.err.println("Open batch : ".concat(s).concat(" !!!"));
		TBatchReader batchReader = new TBatchReader(file.getAbsolutePath());
		SAXParserFactory factory = SAXParserFactory.newInstance();
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, "Ouverture du batch ...", Color.red);
		TEventHandler.handleMessage(event);
		FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		TBatch batch = null;
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(file, (DefaultHandler)batchReader);
			batch = batchReader.getBatch();
			String pathSeparator = System.getProperty("file.separator");
			String p = pathSeparator;
			String filename = file.getAbsolutePath();
			int ix = filename.lastIndexOf(pathSeparator);
			if (ix > -1) p = filename.substring(0, ix + 1);
			batch.setPath(p);
			batch.setName(filename.substring(ix + 1));
			batch.getModel().clearModifs();
		}
		catch (Throwable t) {
			t.printStackTrace();
		}
		event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, batch);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, batch.getView());
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TDataManager.0"), Color.blue);//externalized since 2006/02/20
		TEventHandler.handleMessage(event);
		FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
	}
	//ajout integration 14/09/05
	public void newBatch() {
		TBatch batch = new TBatch();
		TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.ADD_DATA_ELEMENT, null, batch);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, batch.getView());
		TEventHandler.handleMessage(event);
	}
	public void loadGrid(File gridFile, TImage image) {
		TLoadGridWorker worker = new TLoadGridWorker(gridFile, image);
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.construct(false);
		worker.finished();
	}
	public void addDataElement(TDataElement elem) {
		data.addElement(elem);
		currentDataElement = elem;
	}
	public void changeCurrentData(TDataElement newDataElement) {
		int index = data.indexOf(currentDataElement);
		data.removeElement(currentDataElement);
		data.insertElementAt(newDataElement, index);
		currentDataElement = newDataElement;
	}
	public boolean removeDataElement(TDataElement elem) {
		int index = data.indexOf(elem);
		TEvent event;
		if (index >= 0) {
			
			if (elem.getModel().getModif() != 0) {				
				int ans = JOptionPane.showConfirmDialog(null, Messages.getString("TDataManager.6") + elem.getName() + //$NON-NLS-1$
						Messages.getString("TDataManager.7"), Messages.getString("TDataManager.8"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				if (ans == JOptionPane.YES_OPTION) {
					event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.SAVE, null, elem);
					TEventHandler.handleMessage(event);
					//elem.getControler().processEvent(event);
					
				}
				else if (ans == JOptionPane.CANCEL_OPTION)
					return false;
							}
			data.removeElement(elem);
			if (data.size() > 0) {
				if (index > 0)
					currentDataElement = (TDataElement)data.elementAt(index - 1);
				else
					currentDataElement = (TDataElement)data.elementAt(0);
			}
			else
				currentDataElement = null;
			event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.REMOVE_VIEW, elem.getView());
			TEventHandler.handleMessage(event);
		}
		return true;
	}
	public void removeAll() {
		Object[] o = data.toArray();
		boolean ok;
		for (int i = 0; i < o.length; i++) {
			ok = removeDataElement((TDataElement)o[i]);
			if (!ok) break;
		}
	}
	public TDataElement getCurrentDataElement() {
		return currentDataElement;
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
