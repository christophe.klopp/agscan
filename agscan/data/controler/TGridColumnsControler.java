package agscan.data.controler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.controler.memento.TColumnModifyAction;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TDataElementMementos;
import agscan.data.controler.memento.TGridAction;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.dialog.TImportColumnsDialog;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;
import agscan.data.model.grid.table.factory.TColumnFactory;

public class TGridColumnsControler {
  public static final int IMPORT_COLUMNS = 301;
  public static final int ADD_COLUMN = 302;
  public static final int REFRESH_COLUMN = 303;
  public static final int HIDE_COLUMN = 304;
  public static final int SHOW_COLUMN = 305;
  public static final int REMOVE_COLUMN = 306;
  public static final int REFRESH_COLUMNS_SIZES = 307;

  private boolean lastProcessOk;

  public TGridColumnsControler() {
  }
  public Object[] processEvent(TEvent event, TDataElementMementos mementos) {
    int action = event.getAction();
    int[] ix;
    Object[] ret = null;
    lastProcessOk = true;
    TEvent ev;// modif integration 14/09/05
    TGriddedElement grid = (TGriddedElement)event.getParam(0);
    switch (action) {
      case ADD_COLUMN:
        ret = new Object[1];
        ret[0] = addColumn(((Integer)event.getParam(1)).intValue(), (Vector)event.getParam(2), grid);
        ix = new int[1];
        ix[0] = grid.getGridModel().getConfig().getColumns().getColumnCount() - 1;
        TColumnState[] cs = new TColumnState[1];
        cs[0] = ((TColumn)ret[0]).createSnapshot();
        /*
         * remplacé/allégé integration 14/09/05
         * TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
                              new TGridAction(true, false, false, false, false, false, ix, cs, null, null, null));
        TEventHandler.handleMessage(ev);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, grid);
        TEventHandler.handleMessage(event);
        */
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid));
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, grid);
        TEventHandler.handleMessage(ev);
        break;
      case REFRESH_COLUMN:
        TColumnState previousState = ((TColumn)event.getParam(1)).createSnapshot();
        refreshColumn((TColumn)event.getParam(1), (Vector)event.getParam(2), grid);
        TColumnState state = ((TColumn)event.getParam(1)).createSnapshot();
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
                              new TColumnModifyAction(previousState, state));
        TEventHandler.handleMessage(ev);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, grid);
        TEventHandler.handleMessage(event);
        break;
      case HIDE_COLUMN:
        grid.getGridModel().getConfig().hideColumn((TColumn)event.getParam(1));
        grid.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        break;
      case SHOW_COLUMN:
        grid.getGridModel().getConfig().showColumn((TColumn)event.getParam(1));
        grid.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        break;
      case IMPORT_COLUMNS:
        importColumns((File)event.getParam(1), grid, mementos);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, grid));
        break;
      case REMOVE_COLUMN:
        StringSelection selection = new StringSelection(getColumnValues((TColumn)event.getParam(1), grid));
        removeColumn((TColumn)event.getParam(1), grid);
        ix = new int[1];
        ix[0] = ((TColumn)event.getParam(1)).getModelIndex();
        cs = new TColumnState[1];
        cs[0] = ((TColumn)event.getParam(1)).createSnapshot();
        StringSelection[] ss = new StringSelection[1];
        ss[0] = selection;
        //ev modifié integration 14/09/05
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
                              new TGridAction(false, true, false, false, false, false, false, false, ix, cs, ss, null, null));
        TEventHandler.handleMessage(ev);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, grid);
        TEventHandler.handleMessage(event);
        break;
      case REFRESH_COLUMNS_SIZES:
        grid.getView().getTablePanel().initColumnSizes(false);
        grid.getView().getTablePanel().refresh();
        break;
      default:
        lastProcessOk = false;
        break;
    }
    return ret;
  }
  public boolean isLastProcessOk() {
    return lastProcessOk;
  }
  public TColumn addColumn(int index, int type, Vector params, TGriddedElement grid) {
    TColumn column = TColumnFactory.createColumn(type, params);
    if (column != null) {
      column.setModelIndex(index);
      column.setindex(index);
      grid.getGridModel().getConfig().addColumn(column, index);
      grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      grid.getView().getTablePanel().initColumnSizes(column);
    }
    return column;
  }
  public TColumn addColumn(int type, Vector params, TGriddedElement grid) {
    TColumn column = TColumnFactory.createColumn(type, params);
    if (column != null) {
      grid.getGridModel().getConfig().addColumn(column);
      grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
      grid.getView().getTablePanel().initColumnSizes(column);
    }
    return column;
  }
  public void refreshColumn(TColumn column, Vector params, TGriddedElement grid) {
    TColumnFactory.refreshColumn(column, params);
    grid.getView().getTablePanel().initColumnSizes(column);
    grid.getView().getTablePanel().refresh();
    grid.getView().getGraphicScrollPane().repaint();
  }
  public void removeColumn(TColumn column, TGriddedElement grid) {
    if (column.getModelIndex() == grid.getGridModel().getColorizedColumn())
      grid.getGridModel().decolorize();
    if (column.getModelIndex() == grid.getGridModel().getSelectedColumn())
      grid.getGridModel().unsort();
    grid.getGridModel().getConfig().removeColumn(column);
    grid.getGridModel().removeParameter(column.getSpotKey());
  }
  public String getColumnValues(TColumn column, TGriddedElement grid) {
    Object[] spots = grid.getGridModel().getSpots().toArray();
    StringBuffer s = new StringBuffer(spots.length * 10);
    for (int i = 0; i < spots.length; i++)
      s.append(((TSpot)spots[i]).getParameterString(column.getSpotKey()) + "\n"); //$NON-NLS-1$
    return s.toString();
  }
  private void importColumns(File file, TGriddedElement grid, TDataElementMementos mementos) {
    TGridModel model = grid.getGridModel();
    String line = "", token; //$NON-NLS-1$
    StringTokenizer strtok;
    boolean empty = true;
    try {
      BufferedReader in = new BufferedReader(new FileReader(file));
      int nameIndex = -1, i = 0, col = 0;
      Hashtable columnIndex = new Hashtable();
      while ((line.equals("") || line.startsWith("#"))) line = in.readLine(); //$NON-NLS-1$ //$NON-NLS-2$
      strtok = new StringTokenizer(line, ","); //$NON-NLS-1$
      while (strtok.hasMoreTokens()) {
        token = strtok.nextToken().trim();
        if (token.equalsIgnoreCase("nom")) //$NON-NLS-1$
          nameIndex = i;
        else
          columnIndex.put(token, new Integer(col++));
        i++;
      }
      if (!columnIndex.isEmpty()) {
        i = 0;
        String[][] value;
        String[] tmp;
        Hashtable name = null;
        value = new String[columnIndex.size()][model.getRowCount()];
        if (nameIndex != -1) name = new Hashtable();
        int i2;
        boolean ok = true;
        while (in.ready()) {
          line = in.readLine();
          if (!line.equals("") && !line.startsWith("#")) { //$NON-NLS-1$ //$NON-NLS-2$
            tmp = line.split(","); //$NON-NLS-1$
            if (((nameIndex != -1) && (tmp.length != (columnIndex.size() + 1))) ||
                ((nameIndex == -1) && (tmp.length != columnIndex.size()))){
              ok = false;
              break;
            }
            i2 = 0;
            if (nameIndex != -1)
              for (int k = 0; k <= columnIndex.size(); k++)
                if (nameIndex == k)
                  name.put(tmp[nameIndex].trim(), new Integer(i));
                else
                  value[i2++][i] = tmp[k];
            else
              for (int k = 0; k < columnIndex.size(); k++)
                if (nameIndex == k)
                  name.put(tmp[nameIndex].trim(), new Integer(i));
                else
                  value[i2++][i] = tmp[k];
            i++;
          }
        }
        in.close();
        if (!ok) {
          JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.9") + line + //$NON-NLS-1$
                                        Messages.getString("TGridColumnsControler.10"), Messages.getString("TGridColumnsControler.11"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
        }
        else {
          TImportColumnsDialog dial = new TImportColumnsDialog(columnIndex, nameIndex != -1);
          dial.pack();
          dial.setModal(true);
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          dial.setLocation((screenSize.width - dial.getWidth()) / 2, (screenSize.height - dial.getHeight()) / 2);
          dial.setVisible(true);
          if (!dial.isCanceled()) {
            Object[] selectedColumns = dial.getSelectedColumns();
            Object[] selectedTypes = dial.getSelectedTypes();
            int[] ix = new int[selectedColumns.length];
            TColumnState[] cs = new TColumnState[selectedColumns.length];
            StringSelection[] ss = new StringSelection[selectedColumns.length];
            boolean synchro = dial.isSynchro(), existColumn;
            TColumn column;
            Vector params;
            int type;
            String columnName;
            for (i = 0; i < selectedColumns.length; i++) {
              params = new Vector();
              type = ((Integer)selectedTypes[i]).intValue();
              columnName = (String)selectedColumns[i];
              column = null;
              Enumeration enume = model.getConfig().getColumns().getColumns();
              existColumn = false;
              while (enume.hasMoreElements()) {
                column = (TColumn)enume.nextElement();
                if (column.toString().equals(columnName)) {
                  existColumn = true;
                  break;
                }
              }
              if (!existColumn) {
                params.addElement(columnName);
                switch (type) {
                  case TColumn.TYPE_INTEGER:
                    params.addElement(new Integer(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Integer(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Integer(0));
                    params.addElement(new Integer(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Integer(0));
                  case TColumn.TYPE_PROBA:
                  case TColumn.TYPE_REAL:
                    params.addElement(new Double(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Double(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Double(0));
                    params.addElement(new Double(0));
                    params.addElement(new Boolean(false));
                    params.addElement(Color.white);
                    params.addElement(new Double(0));
                    break;
                  case TColumn.TYPE_TEXT:
                    params.addElement(""); //$NON-NLS-1$
                  case TColumn.TYPE_BOOLEAN:
                    params.addElement(new Boolean(false));
                  case TColumn.TYPE_URL + 3:
                    Vector v = new Vector();
                    v.addElement("Lien"); //$NON-NLS-1$
                    v.addElement("http://localhost"); //$NON-NLS-1$
                    params.addElement(v);
                    break;
                }
                column = TColumnFactory.createColumn(type, params);
                if (column.isCompatible(value[((Integer)columnIndex.get(columnName)).intValue()])) {
                  Object[] objectValues = column.getCompatibleValuesInBuffer(value[((Integer)columnIndex.get(columnName)).intValue()]);
                  if (synchro) {
                    Vector v = model.getSpots();
                    TSpot spot;
                    for (int k = 0; k < v.size(); k++) {
                      spot = (TSpot)v.elementAt(k);
                      spot.addParameter(column.getSpotKey(), objectValues[((Integer)name.get(spot.getName())).intValue()], true);
                    }
                  }
                  else {
                    Vector v = model.getSpots();
                    TSpot spot;
                    for (int k = 0; k < v.size(); k++) {
                      spot = (TSpot)v.elementAt(k);
                      spot.addParameter(column.getSpotKey(), objectValues[model.getSpotRow(k)], true);
                    }
                  }
                  grid.getGridModel().getConfig().addColumn(column);
                  ix[i] = column.getModelIndex();
                  cs[i] = column.createSnapshot();
                  ss[i] = new StringSelection(getColumnValues(column, grid));
                  empty = false;
                  grid.getView().getTablePanel().initColumnSizes(column);
                }
                else {
                  JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.15") + column.toString() + //$NON-NLS-1$
                                                Messages.getString("TGridColumnsControler.16"), Messages.getString("TGridColumnsControler.17"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
                  ix[i] = -1;
                  cs[i] = null;
                  ss[i] = null;
                }
              }
              else {
                JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.18") + column.toString() + //$NON-NLS-1$
                                              Messages.getString("TGridColumnsControler.19"), //$NON-NLS-1$
                                              Messages.getString("TGridColumnsControler.20"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
                ix[i] = -1;
                cs[i] = null;
                ss[i] = null;
              }
            }
            if (!empty) {
              //mementos.pokeMemento(new TGridAction(false, false, true, false, false, false, ix, cs, ss, file, null));//remplacé integration 14/09/05
              mementos.pokeMemento(new TGridAction(false, false, true, false, false, false, false, false, ix, cs, ss, file, null));
              grid.getModel().addModif(1);
              TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid));
            }
          }
        }
      }
      else
        JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.21") + file.getName() + Messages.getString("TGridColumnsControler.22"), Messages.getString("TGridColumnsControler.23"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace();
    }
    catch (IOException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.24"), Messages.getString("TGridColumnsControler.25"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.26") + line + Messages.getString("TGridColumnsControler.27"), Messages.getString("TGridColumnsControler.28"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    catch (NullPointerException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridColumnsControler.29") + file.getName() + //$NON-NLS-1$
                                    Messages.getString("TGridColumnsControler.30"), Messages.getString("TGridColumnsControler.31"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
