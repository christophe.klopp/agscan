/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Societe : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.algo.globalalignment.TGlobalAlignmentAlgorithm;
import agscan.algo.localalignment.TLocalAlignmentAlgorithm;
import agscan.algo.quantif.TComputeFitCorrectionAlgorithm;
import agscan.algo.quantif.TComputeOvershiningCorrectionAlgorithm;
import agscan.algo.quantif.TComputeQMAlgorithm;
import agscan.algo.quantif.TComputeSelectionAlgorithm;
import agscan.algo.quantif.TComputeSpotDiametersWithFitAlgorithm;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.algo.spotdetection.TSpotDetectionAlgorithm;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TSaveAsAction;
import agscan.statusbar.TStatusBar;
import agscan.data.view.TAlignmentView;

public class TAlignmentControler extends TControlerImpl {
  public static final int IMAGE = 1;
  public static final int GRID= 2;
  public static final int ALIGNMENT = 3;
  public static final int COMPUTE_SPOT_QUALITY = 601;
  public static final int GLOBAL_ALIGNMENT = 602;
  public static final int LOCAL_ALIGNMENT = 603;
  public static final int COMPUTE_QUANTIF_IMAGE_CONST = 604;
  public static final int COMPUTE_QUANTIF_FIT_CONST = 605;
  public static final int COMPUTE_QUANTIF_IMAGE_COMP = 606;
  public static final int COMPUTE_QUANTIF_FIT_COMP = 607;
  public static final int SHOW_COMPUTED_DIAMETERS = 608;
  public static final int SHOW_FIT_IMAGE = 609;
  public static final int SHOW_DIFF_IMAGE = 610;
  public static final int COMPUTE_QM = 611;
  public static final int COMPUTE_FIT_CORRECTION = 612;
  public static final int COMPUTE_OVERSHINING_CORRECTION = 613;
  public static final int COMPUTE_SELECTION = 614;
  public static final int EXPORT_AS_TEXT = 615;

  private TGridControler gridControler;
  private TGridPositionControler gridPositionControler;
  private TImageControler imageControler;
  private Vector undoList;
  private int index;

  public TAlignmentControler(TAlignment ref, TImageControler imageControler, TGridControler gridControler) {
    super(ref);
    gridPositionControler = new TGridPositionControler();
    this.imageControler = imageControler;
    this.gridControler = gridControler;
    gridControler.setReference(ref);
    undoList = new Vector();
    index = -1;
  }
  public void setGridControler(TGridControler gc) {
    this.gridControler = gc;
    gc.setReference(reference);
  }
  public Object[] processEvent(TEvent event) {
  	 Object[] ret = null;
     int undoType;
     TAlignmentModel alignmentModel = (TAlignmentModel)((TGriddedElement)reference).getModel();
     TAlignment alignment = (TAlignment)alignmentModel.getReference();
     TEvent ev;
     Vector spots, calculs;
     TDataElement elem;
     final BufferedImage bi;
     JPanel panel;
     JFrame frame;

    switch (event.getAction()) {
      case COMPUTE_SPOT_QUALITY:
        TSpotDetectionAlgorithm sdAlgo = (TSpotDetectionAlgorithm)event.getParam(1);
        TAlignmentModel am = ((TAlignmentModel)((TAlignment)event.getParam(0)).getModel());
        spots = alignmentModel.getGridModel().getSelectionModel().getRecursivelySelectedSpots();
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.HISTOGRAM_MAX_USED,
                              new Integer(((TImage)alignmentModel.getImageModel().getReference()).getHistogramMaxUsed()));
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.HISTOGRAM_MIN_USED,
                              new Integer(((TImage)alignmentModel.getImageModel().getReference()).getHistogramMinUsed()));
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.IMAGE_DATA, alignmentModel.getImageModel().getMaxImageData());//remi modif 2005/11/10
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.IMAGE_WIDTH_IN_PIXELS, new Integer(alignmentModel.getImageModel().getElementWidth()));
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.IMAGE_HEIGHT_IN_PIXELS, new Integer(alignmentModel.getImageModel().getElementHeight()));
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.XRES, new Double(alignmentModel.getImageModel().getPixelWidth()));
        sdAlgo.setWorkingData(TSpotDetectionAlgorithm.YRES, new Double(alignmentModel.getImageModel().getPixelHeight()));
        if (spots.size() > 0) {
        	sdAlgo.setWorkingData(TSpotDetectionAlgorithm.SPOTS, spots);
        	sdAlgo.execute(true);
        	reference.getView().getGraphicPanel().repaint();
        	TEventHandler.handleMessage(
        			new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, event.getParam(0),
        					Double.toString((double)((int)(((Double)sdAlgo.getResult(TSpotDetectionAlgorithm.RESULT_PC_GOOD)).doubleValue() * 10000)) / 100.0D) + "%")); //$NON-NLS-1$        	
        	//23/08/05 -  on ajoute la signification dans la statusbar du pourcentage ecrit en fin de detection
        	event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, event.getParam(0),Messages.getString("TAlignmentControler.1"),Color.blue); //$NON-NLS-1$
    		TEventHandler.handleMessage(event);        	
        }
        break;
      case LOCAL_ALIGNMENT:
        TLocalAlignmentAlgorithm laAlgo = (TLocalAlignmentAlgorithm)event.getParam(1);
        am = (TAlignmentModel)reference.getModel();
        laAlgo.initData(am.getImageModel().getPixelWidth(), am.getImageModel().getPixelHeight(),
                        am.getImageModel().getMaxImageData(),  am.getImageModel().getElementWidth(),//remi modif 2005/11/10
                        am.getImageModel().getElementHeight(), am.getGridModel(),
                        (TAlignmentView)am.getReference().getView(),
                        ((TImage)am.getImageModel().getReference()).getHistogramMinUsed(),
                        ((TImage)am.getImageModel().getReference()).getHistogramMaxUsed());
        laAlgo.execute(((Boolean)event.getParam(2)).booleanValue(), Thread.MIN_PRIORITY);
        break;
      case GLOBAL_ALIGNMENT:
        TGlobalAlignmentAlgorithm gaAlgo = (TGlobalAlignmentAlgorithm)event.getParam(1);
        if (event.getParam(0) == null)
          alignmentModel = (TAlignmentModel)reference.getModel();
        else
          alignmentModel = (TAlignmentModel)((TAlignment)event.getParam(0)).getModel();
        gaAlgo.initData(alignmentModel.getImageModel().getPixelWidth(), alignmentModel.getImageModel().getPixelHeight(),
                        alignmentModel.getImageModel().getMaxImageData(),  alignmentModel.getImageModel().getElementWidth(),//remi modif 2005/11/10
                        alignmentModel.getImageModel().getElementHeight(), alignmentModel,
                        ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMinUsed(),
                        ((TImage)alignmentModel.getImageModel().getReference()).getHistogramMaxUsed());
        gaAlgo.execute(true);
        break;
      case SAVE:
        String pathSeparator = System.getProperty("file.separator");
        //String path = reference.getPath();
        String path = reference.getPath();
        if (!path.endsWith(pathSeparator)) path = path + pathSeparator;
        String name = reference.getName();
        //String name = imageControler.reference.getName();
        System.err.println("PATH = " + path);
        System.err.println("NAME = " + name);
        if (!path.equals("")) {
          File file = new File(path + name);
          save(file);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        }
        else {
        	 ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.FIRE_ACTION, new Integer(TSaveAsAction.getID()));//TMenuManager.SAVE_AS
             TEventHandler.handleMessage(ev);
        }
        break;
      case SAVE_AS:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.IS_DATA_OPENED, event.getParam(0), (File)event.getParam(1));
        boolean b = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
        if (b)
          messageFichierOuvert(((File)ev.getParam(1)).getAbsolutePath());
        else {
          b = TImageControler.okToWrite(((File)event.getParam(1)).getAbsolutePath());
          if (b) {
            save((File)event.getParam(1), ((Boolean)event.getParam(2)).booleanValue());
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
          }
        }
        break;
      case UNDO:
        undoType = ((Integer)undoList.elementAt(index--)).intValue();
        if (undoType == IMAGE)
          imageControler.processEvent(event);
        else if (undoType == GRID)
          gridControler.processEvent(event);
        alignmentModel.addModif(-1);
        reference.getView().getGraphicPanel().repaint();
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case REDO:
        undoType = ((Integer)undoList.elementAt(++index)).intValue();
        if (undoType == IMAGE)
          imageControler.processEvent(event);
        else if (undoType == GRID)
          gridControler.processEvent(event);
        alignmentModel.addModif(1);
        reference.getView().getGraphicPanel().repaint();
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case TImageControler.ADD_MEMENTO:
        imageControler.processEvent(event);
        undoList.insertElementAt(new Integer(IMAGE), ++index);
        alignmentModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        while (undoList.size() > (index + 1)) undoList.remove(undoList.size() - 1);
        break;
      case TGridControler.ADD_ACTION_MEMENTO:
        gridControler.processEvent(event);
        alignmentModel.addModif(1);
        undoList.insertElementAt(new Integer(GRID), ++index);
        while (undoList.size() > (index + 1)) undoList.remove(undoList.size() - 1);
        break;
      case TGridControler.ADD_MEMENTO:
        gridControler.processEvent(event);
        alignmentModel.addModif(1);
        undoList.insertElementAt(new Integer(GRID), ++index);
        while (undoList.size() > (index + 1)) undoList.remove(undoList.size() - 1);
        break;
      case COMPUTE_QUANTIF_IMAGE_CONST:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TQuantifImageConstantAlgorithm qicAlgo = new TQuantifImageConstantAlgorithm((TAlignmentModel)elem.getModel());
        qicAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_QUANTIF_FIT_CONST:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TQuantifFitConstantAlgorithm qfcAlgo = new TQuantifFitConstantAlgorithm((TAlignmentModel)elem.getModel());
        qfcAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_QUANTIF_IMAGE_COMP:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TQuantifImageComputedAlgorithm qicpAlgo = new TQuantifImageComputedAlgorithm((TAlignmentModel)elem.getModel());
        qicpAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_QUANTIF_FIT_COMP:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TQuantifFitComputedAlgorithm qfcpAlgo = new TQuantifFitComputedAlgorithm((TAlignmentModel)elem.getModel());
        qfcpAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case SHOW_COMPUTED_DIAMETERS:
        if (((Boolean)event.getParam(1)).booleanValue() == true) {
          TComputeSpotDiametersWithFitAlgorithm csdawfAlgo = new TComputeSpotDiametersWithFitAlgorithm(alignmentModel, true);
          csdawfAlgo.execute(true);
        }
        else {
          ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignment);
          spots = (Vector)TEventHandler.handleMessage(ev)[0];
          TSpot spot;
          for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements(); ) {
            spot = (TSpot)enu_spots.nextElement();
            spot.updateDiameterWithUser();
          }
        }
        ((TAlignmentView)alignment.getView()).setComputedDiameters(((Boolean)event.getParam(1)).booleanValue());
        break;
      case SHOW_FIT_IMAGE:
        bi = makeFitImage(alignmentModel);
        panel = new JPanel() {
          public void paint(Graphics g) {
            g.drawImage(bi, 0, 0, this);
          }
        };
        panel.setPreferredSize(new Dimension(bi.getWidth(), bi.getHeight()));
        frame = new JFrame("Image fit");
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new JScrollPane(panel), BorderLayout.CENTER);
        frame.setVisible(true);
        break;
      case SHOW_DIFF_IMAGE:
        bi = makeDiffImage(alignmentModel);
        panel = new JPanel() {
          public void paint(Graphics g) {
            g.drawImage(bi, 0, 0, this);
          }
        };
        panel.setPreferredSize(new Dimension(bi.getWidth(), bi.getHeight()));
        frame = new JFrame("Image difference");
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new JScrollPane(panel), BorderLayout.CENTER);
        frame.setVisible(true);
        break;
      case COMPUTE_QM:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TComputeQMAlgorithm cqmAlgo = new TComputeQMAlgorithm((TAlignmentModel)elem.getModel());
        cqmAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_FIT_CORRECTION:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TComputeFitCorrectionAlgorithm cfcAlgo = new TComputeFitCorrectionAlgorithm((TAlignmentModel)elem.getModel());
        cfcAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_OVERSHINING_CORRECTION:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TComputeOvershiningCorrectionAlgorithm cocAlgo = new TComputeOvershiningCorrectionAlgorithm((TAlignmentModel)elem.getModel());
        cocAlgo.execute(((Boolean)event.getParam(1)).booleanValue());
        break;
      case COMPUTE_SELECTION:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
        elem = (TDataElement) TEventHandler.handleMessage(ev)[0];
        TComputeSelectionAlgorithm csAlgo = new TComputeSelectionAlgorithm((TAlignmentModel)elem.getModel(), (Vector)event.getParam(1));
        csAlgo.execute(true);
        break;
      case EXPORT_AS_TEXT:
        if (TImageControler.okToWrite(((File)event.getParam(1)).getAbsolutePath()))
          ((TAlignment)event.getParam(0)).getGridModel().exportAsText((File)event.getParam(1));
        break;
      case TStatusBar.ALGO_PROGRESS:
        if (((TAlignment)reference).isInBatch())
          TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, ((TAlignment)reference).getBatch(), event.getParam(1)));
        else
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, event.getParam(0), event.getParam(1)));
        break;
      default:
        if (event.getParam(0) == null)
          event.setParam(0, alignmentModel.getReference());
        ret = gridControler.processEvent(event);
        if (!gridControler.isLastProcessOk()) {
          ret = gridPositionControler.processEvent(event);
          if (!gridPositionControler.isLastProcessOk()) {
            ret = imageControler.processEvent(event);
            if (imageControler.isLastProcessOk()) reference.getView().getGraphicPanel().repaint();
          }
        }
        break;
    }
    return ret;
  }
  public boolean hasUndoMementos() {
    return (index > -1);
  }
  public boolean hasRedoMementos() {
    return ((undoList.size() - index) >= 2);
  }
  private void messageFichierOuvert(String filename) {
    JOptionPane.showMessageDialog(null, Messages.getString("TAlignmentControler.0") + filename + Messages.getString("TAlignmentControler.8"), //$NON-NLS-1$ //$NON-NLS-2$
                                  Messages.getString("TAlignmentControler.9"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
  }
  private void save(File file, boolean saveImage) {
    ((TAlignmentModel)reference.getModel()).setSaveImage(saveImage);
    save(file);
  }
  private void save(File file) {
    ((TAlignmentModel)reference.getModel()).save(file);
    String pathSeparator = System.getProperty("file.separator");
    String p = pathSeparator;
    String filename = file.getAbsolutePath();
    int ix = filename.lastIndexOf(pathSeparator);
    if (ix > -1) p = filename.substring(0, ix + 1);
    ((TAlignment)reference).setPath(p);
    System.err.println("path de sauvegarde : "+ p);
    if(((TAlignment)reference).getBatch()!=null)((TBatchModelNImages)((TAlignment)reference).getBatch().getModel()).setImageName(((TAlignment)reference).getBatch().getWorker().getCurrentRunningJob(), 0, filename);
    ((TAlignment)reference).setName(filename.substring(ix + 1));
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
  }
  public TGridPositionControler getGridPositionControler() {
    return gridPositionControler;
  }
  public TGridSelectionControler getGridSelectionControler() {
    return gridControler.getGridSelectionControler();
  }
  public TGridColumnsControler getGridColumnsControler() {
    return gridControler.getGridColumnsControler();
  }
  private BufferedImage makeFitImage(TAlignmentModel alignmentModel) {
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
    int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    TFitAlgorithm fitAlgo = null;
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData(alignmentModel);
    ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignmentModel.getReference());
    Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];
    double[] fitData;
    TSpot spot;
    Enumeration enume;
    double xRes = alignmentModel.getPixelWidth();
    double yRes = alignmentModel.getPixelWidth();
    Point2D.Double point;
    double fitQL;
    int bbb;
    int imageWidth = alignmentModel.getElementWidth(), imageHeight = alignmentModel.getElementHeight();
   // double lat = alignmentModel.getImageModel().getInitialImage().getLatitude();
    double lat = ImagePlusTools.getLatitude();
    short[] thData = new short[imageWidth * imageHeight];
    short fond = 0;
    for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements(); ) {
      spot = (TSpot)enu_spots.nextElement();
      fitData = spot.getFitData();
      if (fitData == null) {
        fitAlgo.computeFit(spot);
        fitData = spot.getFitData();
      }
      enume = spot.getPoints2D(xRes, yRes).elements();
      int n = 0;
      while (enume.hasMoreElements()) {
        point = (Point2D.Double)enume.nextElement();
        bbb = (int)point.getX()+ (int)point.getY() * imageWidth;
        fitQL = 65535.0D * (Math.log(fitData[n++]) / Math.log(10) / lat + 0.5D);
        if (fitQL > 65535) fitQL = 65535;
        if (thData[bbb] == fond)
          thData[bbb] = (short)(~(int)fitQL & 0xFFFF);
        else
          thData[bbb] = (short)Math.max((int)thData[bbb], ~((int)fitQL & 0xFFFF));
      }
    }
    Raster raster = Raster.createPackedRaster(new DataBufferUShort(thData, imageWidth * imageHeight),
                                     imageWidth, imageHeight, 16, new Point(0, 0));
    BufferedImage bi = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_USHORT_GRAY);
    bi.setData(raster);
    return bi;
  }
  private BufferedImage makeDiffImage(TAlignmentModel alignmentModel) {
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
    int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
    TFitAlgorithm fitAlgo = null;
    if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
      fitAlgo = new TGaussNewtonFitAlgorithm();
    else if (fitAlg == TMenuManager.FIT_NORMAL)
      fitAlgo = new TNormalFitAlgorithm();
    fitAlgo.initData(alignmentModel);
    ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, alignmentModel.getReference());
    Vector spots = (Vector)TEventHandler.handleMessage(ev)[0];
    double[] fitData;
    TSpot spot;
    Enumeration enume;
    double xRes = alignmentModel.getPixelWidth();
    double yRes = alignmentModel.getPixelHeight();
    // double yRes = alignmentModel.getPixelWidth();//corrige le 03/10/05
    Point2D.Double point;
    double fitQL, Yi;
    int bbb;
    int imageWidth = alignmentModel.getElementWidth(), imageHeight = alignmentModel.getElementHeight();
    //double lat = alignmentModel.getImageModel().getInitialImage().getLatitude();
    double lat =ImagePlusTools.getLatitude();
    short[] thData = new short[imageWidth * imageHeight];
    short fond = 0;
    TImageModel imageModel = alignmentModel.getImageModel();
    int[] imageData = ImagePlusTools.getImageData(imageModel.getInitialImage(),ImagePlusTools.DEPTH_16_BITS);
    for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements(); ) {
      spot = (TSpot)enu_spots.nextElement();
      fitData = spot.getFitData();
      if (fitData == null) {
        fitAlgo.computeFit(spot);
        fitData = spot.getFitData();
      }
      enume = spot.getPoints2D(xRes, yRes).elements();
      int n = 0;
      while (enume.hasMoreElements()) {
        point = (Point2D.Double)enume.nextElement();
        bbb = (int)point.getX()+ (int)point.getY() * imageWidth;
        fitQL = 65535.0D * (Math.log(fitData[n++]) / Math.log(10) / lat + 0.5D);
        try {
          Yi = (imageData[bbb] & 0xFFFF);
        }
        catch(ArrayIndexOutOfBoundsException ex) {
          Yi = 0;
        }
        if (thData[bbb] == fond)
          thData[bbb] = (short)(~(int)Math.abs(fitQL - Yi) & 0xFFFF);
        else
          thData[bbb] = (short)Math.max((int)thData[bbb], ~((int)Math.abs(fitQL - Yi) & 0xFFFF));
      }
    }
    Raster raster = Raster.createPackedRaster(new DataBufferUShort(thData, imageWidth * imageHeight),
                                     imageWidth, imageHeight, 16, new Point(0, 0));
    BufferedImage bi = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_USHORT_GRAY);
    bi.setData(raster);
    return bi;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
