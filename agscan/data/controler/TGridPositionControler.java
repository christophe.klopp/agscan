/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.Enumeration;
import java.util.Vector;

import agscan.TEventHandler;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TGridPositionControler {
  public static final int TRANSLATE = 401;
  public static final int ROTATE = 402;
  public static final int RESIZE_NORTH = 403;
  public static final int RESIZE_SOUTH = 404;
  public static final int RESIZE_EAST = 405;
  public static final int RESIZE_WEST = 406;
  public static final int RESIZE_DIAMETER = 407;
  public static final int IS_ON_NORTH_BORDER = 408;
  public static final int IS_ON_SOUTH_BORDER = 409;
  public static final int IS_ON_WEST_BORDER = 410;
  public static final int IS_ON_EAST_BORDER = 411;
  public static final int IS_ON_SPOT_BORDER = 412;
  public static final int SET_POSITION = 413;
  public static final int SET_ANGLE = 414;
  public static final int ORTHO_TRANSLATE = 415;
  public static final int SET_WIDTH = 416;
  public static final int SET_HEIGHT = 417;
  public static final int TRANSLATE_BLOCK = 418;//ajout integration 13/09/05
  
  private boolean lastProcessOk;

  public TGridPositionControler() {
  }
  public Object[] processEvent(TEvent event) {
  	int action = event.getAction(), sl;
    Object[] ret = null;
    TGridBlock block;
    TSpot spot;
    double dx, dy, yRap, corrDy, xRap, corrDx;
    Enumeration enume;
    boolean bool;
    float[] point1 = new float[2], point2 = new float[2];
    TGridSelectionControler gsc;
    lastProcessOk = true;
    TGriddedElement grid = (TGriddedElement)event.getParam(0);

    switch (action) {
      case TRANSLATE:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0), grid))[0]).elements();
        System.err.println("translate");
        TGridElement ge;
        double a;
        while (enume.hasMoreElements()) {
          ge = (TGridElement)enume.nextElement();
          a = ge.getAngleTotal();
          dx = ((Double)event.getParam(1)).doubleValue();
          dy = ((Double)event.getParam(2)).doubleValue();
          ge.addPosition(dx * Math.cos(a), dx * Math.sin(a));
          ge.addPosition(-dy * Math.sin(a), dy * Math.cos(a));
        }
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_SPOT_SELECTION,
                                               event.getParam(0), grid))[0]).elements();
        while (enume.hasMoreElements()) {        	
          ge = (TGridElement)enume.nextElement();
          a = ge.getAngleTotal();
          dx = ((Double)event.getParam(1)).doubleValue();
          dy = ((Double)event.getParam(2)).doubleValue();
          ge.addPosition(dx * Math.cos(a), dx * Math.sin(a));
          ge.addPosition(-dy * Math.sin(a), dy * Math.cos(a));
        }
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
        	ge = (TGridElement)enume.nextElement();
        	((TSpot)ge).setQuality( -1);
        	((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
        	ge.setFitAlgorithm(null);
        }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case TRANSLATE_BLOCK://ajout integration 13/09/05
        TGridBlock gb = (TGridBlock)event.getParam(1);
        a = gb.getAngleTotal();
        dx = ((Double)event.getParam(2)).doubleValue();
        dy = ((Double)event.getParam(3)).doubleValue();
        gb.addPosition(dx * Math.cos(a), dx * Math.sin(a));
        gb.addPosition(-dy * Math.sin(a), dy * Math.cos(a));
        break;
      case ORTHO_TRANSLATE:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0), grid))[0]).elements();
        while (enume.hasMoreElements()) {
          ge = (TGridElement)enume.nextElement();
          a = ge.getAngleTotal();
          dx = ((Double)event.getParam(1)).doubleValue();
          dy = ((Double)event.getParam(2)).doubleValue();
          ge.addPosition(dx, dy);
        }
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_SPOT_SELECTION,
                                               event.getParam(0), grid))[0]).elements();
        while (enume.hasMoreElements()) {
          ge = (TGridElement)enume.nextElement();
          a = ge.getAngleTotal();
          dx = ((Double)event.getParam(1)).doubleValue();
          dy = ((Double)event.getParam(2)).doubleValue();
          ge.addPosition(dx, dy);
          ge.setFitAlgorithm(null);//ajout integration 13/09/05
        }
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
            ge = (TGridElement)enume.nextElement();
            ((TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case SET_POSITION:
        gsc = ((TAlignmentControler)grid.getControler()).getGridSelectionControler();
        TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_SELECTION, event.getParam(0));
        Vector selection = (Vector)TEventHandler.handleMessage(ev)[0];
        //Vector selection = (Vector)gsc.processEvent(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ge.setPosition( ( (Double) event.getParam(1)).doubleValue(), ( (Double) event.getParam(2)).doubleValue());
            ge.setFitAlgorithm(null);
          }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        //selection = (Vector)gsc.processEvent(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case ROTATE:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        System.err.println("rotate");
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          block.addAngle(Math.atan(((Double)event.getParam(1)).doubleValue()));
          block.updateSpotsPosition();
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case SET_ANGLE:
        gsc = ((TAlignmentControler)grid.getControler()).getGridSelectionControler();
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        //enume = ((Vector)gsc.processEvent(ev)[0]).elements();
        System.err.println("set angle= "+Math.atan(((Double)event.getParam(1)).doubleValue()));
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          block.setAngle(Math.atan(((Double)event.getParam(1)).doubleValue()));
          block.updateSpotsPosition();
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        //selection = (Vector)gsc.processEvent(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case SET_WIDTH:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDx = ((Double)event.getParam(1)).doubleValue();
          xRap = corrDx / block.getWidth();
          block.resizeByRap(xRap, 1.0D, false);
          block.updateSpotsPosition();
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));

        break;
      case SET_HEIGHT:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDy = ((Double)event.getParam(1)).doubleValue();
          yRap = corrDy / block.getHeight();
          block.resizeByRap(1.0D, yRap, false);
          block.updateSpotsPosition();
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case RESIZE_NORTH:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDy = ((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(3)).doubleValue() * Math.cos(block.getAngle());
          if ((block.getHeight() + corrDy) >= 0) {
            yRap = (block.getHeight() + corrDy) / block.getHeight();
            block.resizeByRap(1.0D, yRap, false);
            block.addPosition(((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(2)).doubleValue() * Math.sin(block.getAngle()), -corrDy);
          }
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case RESIZE_SOUTH:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDy = ((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(3)).doubleValue() * Math.cos(block.getAngle());
          if ((block.getHeight() + corrDy) >= 0) {
            yRap = (block.getHeight() + corrDy) / block.getHeight();
            block.resizeByRap(1.0D, yRap, false);
            block.updateSpotsPosition();
          }
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case RESIZE_WEST:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDx = ((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(2)).doubleValue() * Math.cos(block.getAngle());
          if ((block.getWidth() + corrDx) >= 0) {
            xRap = (block.getWidth() + corrDx) / block.getWidth();
            block.resizeByRap(xRap, 1.0D, false);
            block.addPosition(-corrDx, ((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(2)).doubleValue() * Math.sin(block.getAngle()));
          }
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case RESIZE_EAST:
        enume = ((Vector)TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION,
                                               event.getParam(0)))[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          corrDx = ((Double)event.getParam(1)).doubleValue() * ((Double)event.getParam(2)).doubleValue() * Math.cos(block.getAngle());
          if ((block.getWidth() + corrDx) >= 0) {
            xRap = (block.getWidth() + corrDx) / block.getWidth();
            block.resizeByRap(xRap, 1.0D, false);
            block.updateSpotsPosition();
          }
        }
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        selection = (Vector)TEventHandler.handleMessage(ev)[0];
        for (enume = selection.elements(); enume.hasMoreElements(); ) {
            ge = (TGridElement) enume.nextElement();
            ( (TSpot) ge).setQuality( -1);
            ((TSpot)ge).setSynchro(false, grid.getView().getTablePanel());
            ge.setFitAlgorithm(null);
          }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      case IS_ON_NORTH_BORDER:
        ret = new Object[1];
        ret[0] = new Boolean(false);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        while (enume.hasMoreElements()) {
          block = null;
          try {
            block = (TGridBlock)enume.nextElement();
          }
          catch (Exception ex) {
            System.err.println("**** " + block);
            ex.printStackTrace();
          }
          Shape s = block.getAlignedShape();
          PathIterator it = s.getPathIterator(AffineTransform.getScaleInstance(1.0, 1.0));
          it.currentSegment(point1);
          it.next();
          it.currentSegment(point2);
          bool = isOnSegment(point1[0], point1[1], point2[0], point2[1], ((Double)event.getParam(1)).doubleValue(),
                             ((Double)event.getParam(2)).doubleValue(), ((Double)event.getParam(3)).doubleValue());
          if (bool) {
            ret[0] = new Boolean(true);
            break;
          }
        }
        break;
      case IS_ON_SOUTH_BORDER:
        ret = new Object[1];
        ret[0] = new Boolean(false);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          Shape s = block.getAlignedShape();
          PathIterator it = s.getPathIterator(AffineTransform.getScaleInstance(1.0, 1.0));
          it.next();
          it.next();
          it.currentSegment(point1);
          it.next();
          it.currentSegment(point2);
          bool = isOnSegment(point1[0], point1[1], point2[0], point2[1], ((Double)event.getParam(1)).doubleValue(),
                             ((Double)event.getParam(2)).doubleValue(), ((Double)event.getParam(3)).doubleValue());
          if (bool) {
            ret[0] = new Boolean(true);
            break;
          }
        }
        break;
      case IS_ON_WEST_BORDER:
        ret = new Object[1];
        ret[0] = new Boolean(false);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          Shape s = block.getAlignedShape();
          PathIterator it = s.getPathIterator(AffineTransform.getScaleInstance(1.0, 1.0));
          it.currentSegment(point1);
          it.next();
          it.next();
          it.next();
          it.currentSegment(point2);
          bool = isOnSegment(point1[0], point1[1], point2[0], point2[1], ((Double)event.getParam(1)).doubleValue(),
                             ((Double)event.getParam(2)).doubleValue(), ((Double)event.getParam(3)).doubleValue());
          if (bool) {
            ret[0] = new Boolean(true);
            break;
          }
        }
        break;
      case IS_ON_EAST_BORDER:
        ret = new Object[1];
        ret[0] = new Boolean(false);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        while (enume.hasMoreElements()) {
          block = (TGridBlock)enume.nextElement();
          Shape s = block.getAlignedShape();
          PathIterator it = s.getPathIterator(AffineTransform.getScaleInstance(1.0, 1.0));
          it.next();
          it.currentSegment(point1);
          it.next();
          it.currentSegment(point2);
          bool = isOnSegment(point1[0], point1[1], point2[0], point2[1], ((Double)event.getParam(1)).doubleValue(),
                             ((Double)event.getParam(2)).doubleValue(), ((Double)event.getParam(3)).doubleValue());
          if (bool) {
            ret[0] = new Boolean(true);
            break;
          }
        }
        break;
      case IS_ON_SPOT_BORDER:
        ret = new Object[1];
        ret[0] = new Integer(0);
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        while (enume.hasMoreElements()) {
          spot = (TSpot)enume.nextElement();
          sl = spot.isOnShape(((Double)event.getParam(1)).doubleValue(), ((Double)event.getParam(2)).doubleValue());
          if (sl != 0) {
            ret[0] = new Integer(sl);
            break;
          }
        }
        break;
      case RESIZE_DIAMETER:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, event.getParam(0));
        enume = ((Vector)TEventHandler.handleMessage(ev)[0]).elements();
        dx = ((Double)event.getParam(1)).doubleValue();
        dy = ((Double)event.getParam(2)).doubleValue();
        sl = ((Integer)event.getParam(3)).intValue();
        double d = Math.sqrt(dx * dx + dy * dy) * 2;
        double dep;
        while (enume.hasMoreElements()) {
          spot = (TSpot)enume.nextElement();
          dep = d;
          switch (sl) {
            case TSpot.NE:
              if ((dx <= 0) && (dy >= 0)) dep = -d;
              break;
            case TSpot.SE:
              if ((dx <= 0) && (dy <= 0)) dep = -d;
              break;
            case TSpot.SW:
              if ((dx >= 0) && (dy <= 0)) dep = -d;
              break;
            case TSpot.NW:
              if ((dx >= 0) && (dy >= 0)) dep = -d;
              break;
          }
          spot.addToDiameter(dep);
          spot.setQuality(-1);
          spot.setSynchro(false, grid.getView().getTablePanel());//ajout integration 13/09/05
          spot.setFitAlgorithm(null);//ajout integration 13/09/05
        }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, ""));
        break;
      default:
        lastProcessOk = false;
        break;
    }
    return ret;
  }
  public boolean isLastProcessOk() {
    return lastProcessOk;
  }
  private boolean isOnSegment(double x1, double y1, double x2, double y2, double x, double y, double res) {
    double a;
    if (x2 != x1)
      a = (y2 - y1) / (x2 - x1);
    else
      a = 1000000000000.0D;
    double b = y1 - a * x1;
    double Y = ((x + a * y) * (y1 - y2) + x1 * y2 - x2 * y1) / (a * (y1 - y2) + x1 - x2);
    double X = x - a * (Y - y);
    double d = Math.sqrt((x - X) * (x - X) + (y - Y) * (y - Y));
    return ((d <= (3.0 * res)) && (((x >= x1) && (x <= x2)) || ((x >= x2) && (x <= x1)) || (Math.abs(x - x1) < (3 * res)) || (Math.abs(x - x2) < (3 * res))));
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
