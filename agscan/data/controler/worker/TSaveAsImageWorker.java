/*
 * Created on Aug 24, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package agscan.data.controler.worker;

import ij.io.FileSaver;

import java.awt.Color;
import java.io.File;

import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TImageControler;
import agscan.data.element.image.TImage;
import agscan.event.TEvent;
import agscan.plugins.SFormatPlugin;
import agscan.plugins.TPluginManager;
import agscan.statusbar.TStatusBar;

/**
 * @author rcathelin
 * 24/08/05
 * remplace tous les autres workers "save as" et permet comme ca de traiter aussi les formats 
 * connus par les plugins
 TODO en virant les autres workers, penser a mettre ajour les fichiers de 
 * langues (supprimer les lignes devenues inutiles) 
 * 2005/11/02 - ajout du channel a sauver en param
  */

public class TSaveAsImageWorker extends SwingWorker {
	private File file;
	private TImage image;
	private String extension;
	private int channel;
	public TSaveAsImageWorker(File file, TImage image, String extension,int channel) {
		super();
		this.file = file;
		this.image = image;
		this.extension = extension;
		this.channel = channel;
	}
	public Object construct(boolean thread) {
		System.out.print("Save as ..."); 
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, image, new Boolean(true));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, image,"Enregistrement de l'image ...", Color.red); 
		TEventHandler.handleMessage(event);
		
		String filename = file.getAbsolutePath();
		
		// test REMI
		//		 23/08/05: sauvegarde via IJ
		//		the user give (or not) the extension
		// if not we add it to the filename
		int i = filename.lastIndexOf(extension);
		if (i==-1) 	filename += "."+extension;
		else filename = filename.substring(0, i) + extension;
		// si le controller est ok
		if (TImageControler.okToWrite(filename)) {
			if (extension=="tif"){
			//FileSaver saver = new FileSaver(this.image.getImageModel().getInitialImage());//ij
				//modification 2005/11/02 - save of a specific channel (before it saves only the first!)
				FileSaver saver = new FileSaver(this.image.getImageModel().getChannelImage(channel));
			System.out.println("PATH du FILE="+filename);
			saver.saveAsTiff(filename);
			image.getImageModel().setFileFormat("tif"); //$NON-NLS-1$
			}
			else {
				//TODO le save a partir du plugin			

				if (TPluginManager.isKnownExtension(extension))	{
					System.out.println(extension +" = known extension !");	 //$NON-NLS-1$
//					modification 2005/11/02 - save of a specific channel (before it saves only the first!)
					//SFormatPlugin.callSaveImagePlus(this.image.getImageModel().getInitialImage(),filename);// call of saveImagePlus of the plugin thanks to the pluginManager
					SFormatPlugin.callSaveImagePlus(this.image.getImageModel().getChannelImage(channel),filename);// call of saveImagePlus of the plugin thanks to the pluginManager
				}
				System.out.println("PATH du FILE="+filename);
				image.getImageModel().setFileFormat(extension);
			}
			String pathSeparator = System.getProperty("file.separator"); //$NON-NLS-1$
			String p = pathSeparator;
			int ix = filename.lastIndexOf(pathSeparator);
			if (ix > -1) p = filename.substring(0, ix + 1);
			image.setPath(p);
			image.setName(filename.substring(ix + 1));
			//event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME, image.getName(), image.getPath());
			//TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, null);
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, image);
			TEventHandler.handleMessage(event);
		}
		//fin test
		
		
		
		return null;
	}
	public void finished() {
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, image, new Integer(0));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, image, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, image, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, image, Messages.getString("TSaveAsImageWorker.0"), Color.blue); //externalized since 2006/02/20
		TEventHandler.handleMessage(event);
	}
}