package agscan.data.controler.worker;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JEditorPane;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.controler.TBatchControler;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.view.graphic.TPlotsPanel;
import agscan.data.view.table.TBatchTablePanel;
import agscan.event.TEvent;
import agscan.ioxml.TReadQuantifResultsWorker;
import agscan.ioxml.TResultArray;
import agscan.statusbar.TStatusBar;
import agscan.data.element.batch.TBatch;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TPlotArrayWorker extends SwingWorker {
  private TBatch batch;
  private int[] rows;
  private String dirName;
  private boolean linear, log;

  public TPlotArrayWorker(int[] r, TBatch b, String dn, boolean linear, boolean log) {
    batch = b;
    rows = r;
    dirName = dn;
    this.linear = linear;
    this.log = log;
  }
  public Object construct(boolean thread) {
    if (linear | log) {
      batch.setBatchRunning(true);
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(true)));
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_LABEL, batch, "Calcul des plots ...", Color.red));
      ((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
      JFreeChart jfc;
      TBatchModelNImages model = (TBatchModelNImages) batch.getModel();
      TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
      sdAlgo.initWithDefaults();

      TReadQuantifResultsWorker worker;
      TResultArray results;
      File dir = new File(dirName);
      dir.mkdir();
      Object[] params = new Object[7];
      params[6] = TQuantifImageConstantAlgorithm.COLUMN_NAME;
      int[][] data = new int[rows.length][];
      int[][] q = new int[rows.length][];
      String[] n = new String[rows.length];
      String[] sks = {TQuantifImageConstantAlgorithm.COLUMN_NAME};
      for (int i = 0; i < rows.length; i++) {
        worker = new TReadQuantifResultsWorker(new File(model.getImageName(rows[i],0)), batch, sks);
        worker.setPriority(Thread.MAX_PRIORITY);
        results = (TResultArray) worker.construct(false);
        data[i] = results.getResults()[0];
        q[i] = results.getQuality();
        n[i] = model.getImageName(rows[i],0).substring(model.getImageName(rows[i],0).lastIndexOf(File.separator) + 1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer( (i + 1) * 100 / rows.length)));
      }
      TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer(0)));
      LogarithmicAxis v, h;
      int k = 0, ntot = (rows.length - 1) * rows.length / 2, prev_pc = 0, pc;
      for (int i = 0; i < rows.length; i++) {
        params[0] = data[i];
        params[2] = q[i];
        params[4] = n[i];
        for (int j = i + 1; j < rows.length; j++) {
          params[1] = data[j];
          params[3] = q[j];
          params[5] = n[j];
          jfc = (JFreeChart) TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TBatchControler.MAKE_PLOT, batch, params))[0];
          jfc.setTitle("");
          jfc.getXYPlot().getDomainAxis().setTickLabelsVisible(false);
          jfc.getXYPlot().getDomainAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
          jfc.getXYPlot().getRangeAxis().setTickLabelsVisible(false);
          jfc.getXYPlot().getRangeAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
          try {
            if (linear) ChartUtilities.saveChartAsPNG(new File(dirName + File.separator + n[i] + "-" + n[j] + ".png"), jfc, 150, 100);
            if (log) {
              v = new LogarithmicAxis("L " + jfc.getXYPlot().getRangeAxis().getLabel().replaceAll("L ", ""));
              v.setAllowNegativesFlag(true);
              h = new LogarithmicAxis("L " + jfc.getXYPlot().getDomainAxis().getLabel().replaceAll("L ", ""));
              h.setAllowNegativesFlag(true);
              jfc.getXYPlot().setRangeAxis(v);
              jfc.getXYPlot().setDomainAxis(h);
              jfc.getXYPlot().getDomainAxis().setTickLabelsVisible(false);
              jfc.getXYPlot().getDomainAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
              jfc.getXYPlot().getRangeAxis().setTickLabelsVisible(false);
              jfc.getXYPlot().getRangeAxis().setLabelFont(new Font("courier", Font.BOLD, 10));
              ChartUtilities.saveChartAsPNG(new File(dirName + File.separator + n[i] + "-" + n[j] + "_LOG.png"), jfc, 150, 100);
            }
            k++;
            pc = k * 100 / ntot;
            if (pc > prev_pc) {
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, batch, new Integer(pc)));
              prev_pc = pc;
            }
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        }
      }
      File htmlFile = new File(dirName + File.separator + "plots.html");
      try {
        BufferedWriter bw = new BufferedWriter(new FileWriter(htmlFile));
        bw.write("<HTML>");
        bw.newLine();
        bw.write("<table>");
        bw.newLine();
        for (int i = 0; i < rows.length; i++) {
          bw.write("<tr>");
          bw.newLine();
          for (int j = 0; j < rows.length; j++) {
            bw.write("<td>");
            if (i < j) {
              if (linear) bw.write("<img src=\"" + n[i] + "-" + n[j] + ".png\" >");
            }
            else if (i > j) {
              if (log) bw.write("<img src=\"" + n[j] + "-" + n[i] + "_LOG.png\" >");
            }
            bw.write("</td>");
            bw.newLine();
          }
          bw.write("</tr>");
          bw.newLine();
        }
        bw.write("</table>");
        bw.newLine();
        bw.write("</HTML>");
        bw.newLine();
        bw.close();
        JEditorPane jep = new JEditorPane("file:" + htmlFile.getAbsolutePath());
        jep.setEditable(false);
        TPlotsPanel pp = new TPlotsPanel(jep);
        batch.setShowingElement(pp);
        batch.setShowingImage(true);
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return null;
  }
  public void finished() {
    batch.setBatchRunning(false);
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, batch, Messages.getString("TPlotArrayWorker.0"), Color.blue));
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, batch, new Boolean(false)));
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
    FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
}
