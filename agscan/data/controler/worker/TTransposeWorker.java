/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Socio?=to?= : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler.worker;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

import java.awt.Color;

import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;


// modif du 2005/10/12 => action sur le vecteur d'images au lieu d'une image
public class TTransposeWorker extends SwingWorker {
	private TImage image;
	//private TransposeType transposeType;
	private String transposeType;// +90 = "90_clockwise", -90 = "90_counter_clockwise", 180 = "180"
	private String message;
	
	public TTransposeWorker(TImage image, String tt, String mess) {
		super();
		this.image = image;
		transposeType = tt;
		message = mess;
	}
	
	public Object construct(boolean thread) {
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_LABEL, image, message, Color.red);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, image, new Boolean(true));
		TEventHandler.handleMessage(event);
		//TODO voir si on ne peut pas travailler direct avec les Processor pour ip8 et ip8modified
		ImagePlus ip8 = image.getImageView().getDisplayImage();
		ImagePlus ip8modified = ip8;//initialisC) avec l'image de dC)part 8 bits
		//2005/10/14 modif de tout : descente d'un niveau: ImagePlus=>ImageProcessor
		//Vector imagesVector = ((TImageModel)image.getModel()).getInitialImages();
		ImageStack stack = ((TImageModel)image.getModel()).getImages();
		//Vector imagesModifiedVector = new Vector();//2005/10/14 on met des ImageProcessor au lieu d'imagePlus dedans
		
		ImageStack imagesModifiedStack = null;
		
		// CK Janurary 30th 2007 
		if ((transposeType == "90_clockwise") || (transposeType == "90_counter_clockwise")){
			imagesModifiedStack = new ImageStack(stack.getHeight(),stack.getWidth());//2005/10/14 Vector=>Stack
		}else{
			imagesModifiedStack = new ImageStack(stack.getWidth(),stack.getHeight());//2005/10/14 Vector=>Stack
		}
		
		//ImagePlus currentImage = null;//2005/10/14 modif
		//ImagePlus modifiedImage = null;//2005/10/14 modif
		 ImageProcessor currentProc = null;//2005/10/14
		 ImageProcessor modifiedProc = null;//2005/10/14
		if (transposeType == "90_clockwise") {
			// on separe les modifs sur l'image 8 bits de celles sur le vecteur sinon les actions sont reproduites plusieurs fois sur la 8 bits...
			//    1/modif de l'image affichC)e selon la rotation
			ip8modified = new ImagePlus(null,ip8.getProcessor().rotateRight());
		}
		else if (transposeType == "90_counter_clockwise") {
			//    1/modif de l'image affichC)e selon la rotation
			ip8modified = new ImagePlus(null,ip8.getProcessor().rotateLeft());
		}
		else if (transposeType == "180") {
			//    1/modif de l'image affichC)e selon la rotation
			ip8modified = new ImagePlus(null,ip8.getProcessor().rotateLeft());// une fois 90
			ip8modified = new ImagePlus(null,ip8modified.getProcessor().rotateLeft());// deux fois 90
		}
		else if (transposeType == "flip_vertical"){
			ip8modified.getProcessor().flipVertical();
		}
		else if (transposeType == "flip_horizontal"){
			ip8modified.getProcessor().flipHorizontal();
		}
		
		// pour le vecteur de 16 bits
		for (int index=1;index<=stack.getSize();index++){
			currentProc = modifiedProc = stack.getProcessor(index); // (ImagePlus)imagesVector.get(index); 
			
			//SImagePlus ip16 = ((TImageModel)image.getModel()).getInitialImage();
			//SImagePlus ip16modified = ip16;//initialisC) avec l'image de dC)part 16 bits
			//System.out.println("Dans TTranspose,l'image recup fait (en bits):"+ip8.getBitDepth());//test remi 12/07/05
			// REMARQUE : AU LIEU DE MODIFIER LES 2 :MODIFIER LA 16 bits et la transformer en 8 bits....
			//TODO comprendre a quoi sert le data ci-dessous et que lui arrive t-il durant une rotation par exemple
			// remplacement par la permiere image par defaut...peut etre faux attention!!!
			int[] data = ImagePlusTools.getImageData(currentProc,16);
			int[] _data = new int[data.length];
			int ix = 0;
			int n = image.getModel().getElementHeight() * image.getModel().getElementWidth();
			int len = n / 100;
			int i = 0;
			// les rotations sont traitC)es ici "C  la main" => il faut modifier le tableau de data
			
			if (transposeType == "90_clockwise") {
				for (int x = 0; x < image.getModel().getElementWidth(); x++)
					for (int y = image.getModel().getElementHeight() - 1; y >= 0; y--) {
						_data[ix++] = data[y * image.getModel().getElementWidth() + x];
						if (((y * image.getModel().getElementWidth() + x) % len) == 0) {
							event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR, image, new Integer(i++));
							TEventHandler.handleMessage(event);
						}
					}
				// 2/ modif de l'image initiale du model
				//modifiedImage = new ImagePlus(null,currentProc.rotateRight());
				modifiedProc = currentProc.rotateRight();//2005/10/14
			}
			else if (transposeType == "90_counter_clockwise") {
				for (int x = image.getModel().getElementWidth() - 1; x >= 0; x--)
					for (int y = 0; y < image.getModel().getElementHeight(); y++) {
						_data[ix++] = data[y * image.getModel().getElementWidth() + x];
						if (((y * image.getModel().getElementWidth() + x) % len) == 0) {
							event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR, image, new Integer(i++));
							TEventHandler.handleMessage(event);
						}
					}
				
				//    2/ modif de l'image initiale du model
				//modifiedImage = new ImagePlus(null,currentImage.getProcessor().rotateLeft());
				modifiedProc = currentProc.rotateLeft();//2005/10/14
			}
			else if (transposeType == "180") {
				for (int y = image.getModel().getElementHeight() - 1; y >= 0; y--)
					for (int x = image.getModel().getElementWidth() - 1; x >= 0; x--) {
						_data[ix++] = data[y * image.getModel().getElementWidth() + x];
						if (((y * image.getModel().getElementWidth() + x) % len) == 0) {
							event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR, image, new Integer(i++));
							TEventHandler.handleMessage(event);
						}
					}
				
				//    2/ modif de l'image initiale du model
				//modifiedImage = new ImagePlus(null,currentImage.getProcessor().rotateLeft());// une fois 90
				//modifiedImage = new ImagePlus(null,modifiedImage.getProcessor().rotateLeft());// deux fois 90
				//2005/10/14
				modifiedProc = currentProc.rotateLeft();// une fois 90
				modifiedProc = modifiedProc.rotateLeft();// deux fois 90
			}
			
			//autres cas : les flips: pas de modifs des data?
			else if (transposeType == "flip_vertical"){				
				//modifiedImage.getProcessor().flipVertical();//2005/10/14
				modifiedProc.flipVertical();
			}
			else if (transposeType == "flip_horizontal"){			
				//modifiedImage.getProcessor().flipHorizontal();//2005/10/14
				modifiedProc.flipHorizontal();
			}
			//imagesModifiedVector.add(modifiedProc);//ajout au nouveau vecteur de l'image modifiC)e
			imagesModifiedStack.addSlice(image.getImageModel().getChannelImage(index).getTitle(),modifiedProc);//modif Vector => Stack 2005/10/14
			//			2005/10/14
		}
		
		//1/modif de l'image affichC)e selon la rotation
		//image.getImageView().setDisplayImage(PlanarImage.wrapRenderedImage(TransposeDescriptor.create(pi, transposeType, null).getAsBufferedImage()));
		image.getImageView().setDisplayImage(ip8modified);
		//2/ modif de l'image initiale du model
		//    pi = ((TImageModel)image.getModel()).getInitialImage();
		//((TImageModel)image.getModel()).setInitialImage(PlanarImage.wrapRenderedImage(TransposeDescriptor.create(pi, transposeType, null).getAsBufferedImage()));
		
		//((TImageModel)image.getModel()).setInitialImages(imagesModifiedVector);//2005/10/14
//		2005/10/14 modif on remplace le vecteur par un stack...
		//((TImageModel)image.getModel()).setInitialImages(imagesModifiedVector);
		((TImageModel)image.getModel()).setInitialImages(imagesModifiedStack);//2005/10/14
		
		image.getView().getGraphicPanel().revalidate();
		System.err.println("setImageData!!!!!!!!!!!!!!!!!! (dans TTransposeWorker)");
		//image.getImageModel().setImageData(_data);// remove the 02/09/05 remi
		
		return null;
	}
	public void finished() {
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR, image, new Integer(0));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_PROGRESS_BAR_SHOW_PERCENT, image, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.IMAGE_ACTION_LABEL, image, "", Color.blue);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, image);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, image);
		TEventHandler.handleMessage(event);
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 * Copyright (c) 2005 INRA - SIGENAE
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
