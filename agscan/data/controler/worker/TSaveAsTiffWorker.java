/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler.worker;

import ij.io.FileSaver;

import java.awt.Color;
import java.io.File;

import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.controler.TControlerImpl;
import agscan.data.controler.TImageControler;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TSaveAsTiffWorker extends SwingWorker {
	private File file;
	private TImage image;
	public TSaveAsTiffWorker(File file, TImage image) {
		super();
		this.file = file;
		this.image = image;
	}
	public Object construct(boolean thread) {
		System.err.print("Save as TIFF ..."); //$NON-NLS-1$
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, image, new Boolean(true));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, image, Messages.getString("TSaveAsTiffWorker.1"), Color.red); //$NON-NLS-1$
		TEventHandler.handleMessage(event);
			
		int[] data = ImagePlusTools.getImageData(image.getImageModel().getInitialImage(),16);
		//if (image.getImageModel().getUnit().equalsIgnoreCase("PSL")) data = TImageControler.ql2pslData(data, image.getImageModel().getLatitude());//retiré le 22/09/05
				 
		String filename = file.getAbsolutePath();
		int iw = (int)image.getModel().getElementWidth();
		int ih = (int)image.getModel().getElementHeight();
		
		/*
		 BufferedImage bi = null;
		 WritableRaster raster = null;
		 if (image.getImageModel().getNBits() == 16) {
		 short[] tab_b = new short[iw * ih];
		 for (int i = 0; i < data.length; i++) tab_b[i] = (short)~data[i];
		 DataBufferUShort dataBuffer = new DataBufferUShort(tab_b, tab_b.length);
		 bi = new BufferedImage(iw, ih, BufferedImage.TYPE_USHORT_GRAY);
		 raster = WritableRaster.createPackedRaster(dataBuffer, iw, ih, 16, null);
		 }
		 else if ((image.getImageModel().getNBits() == 8) || (image.getImageModel().getNBits() == 24)) {
		 byte[] tab_b = new byte[iw * ih];
		 for (int i = 0; i < data.length; i++) tab_b[i] = (byte)((~data[i] >> 8) & 0xFF);
		 DataBufferByte dataBuffer = new DataBufferByte(tab_b, tab_b.length);
		 bi = new BufferedImage(iw, ih, BufferedImage.TYPE_BYTE_GRAY);
		 raster = WritableRaster.createPackedRaster(dataBuffer, iw, ih, 8, null);
		 }
		 */
		/*
		 if ((bi != null) && (raster != null)) {
		 bi.setData(raster);
		 if (filename.length() < 4)
		 filename += ".tif"; //$NON-NLS-1$
		 else if (!filename.substring(filename.length() - 4).equalsIgnoreCase(".tif")) //$NON-NLS-1$
		 filename += ".tif"; //$NON-NLS-1$
		 if (TImageControler.okToWrite(filename)) {
		 ParameterBlock pb = new ParameterBlock();
		 pb.addSource(bi);
		 pb.add(filename).add("tiff"); //$NON-NLS-1$
		 JAI.create("filestore", pb); //$NON-NLS-1$
		 System.err.println(" done."); //$NON-NLS-1$
		 image.getImageModel().setFileFormat("tiff"); //$NON-NLS-1$
		 String pathSeparator = System.getProperty("file.separator"); //$NON-NLS-1$
		 String p = pathSeparator;
		 int ix = filename.lastIndexOf(pathSeparator);
		 if (ix > -1) p = filename.substring(0, ix + 1);
		 image.setPath(p);
		 image.setName(filename.substring(ix + 1));
		 event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME, image.getName(), image.getPath());
		 TEventHandler.handleMessage(event);
		 event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, null);
		 TEventHandler.handleMessage(event);
		 event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, image);
		 TEventHandler.handleMessage(event);
		 }
		 
		 else
		 System.err.println(" canceled."); //$NON-NLS-1$
		 }
		 */
		
		// test REMI
		//		 23/08/05: sauvegarde via IJ
		//		the user give (or not) the extension 
		int i = filename.lastIndexOf(".tif");
		if (i==-1) 	filename += ".tif";
		else filename = filename.substring(0, i) + ".tif";
		// si le controller est ok
		if (TImageControler.okToWrite(filename)) {
			FileSaver saver = new FileSaver(this.image.getImageModel().getInitialImage());//ij  
			System.out.println("PATH du FILE="+filename);
			saver.saveAsTiff(filename);
			image.getImageModel().setFileFormat("tif"); //$NON-NLS-1$
			String pathSeparator = System.getProperty("file.separator"); //$NON-NLS-1$
			String p = pathSeparator;
			int ix = filename.lastIndexOf(pathSeparator);
			if (ix > -1) p = filename.substring(0, ix + 1);
			image.setPath(p);
			image.setName(filename.substring(ix + 1));
			event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME, image.getName(), image.getPath());
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, null);
			TEventHandler.handleMessage(event);
			event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, image);
			TEventHandler.handleMessage(event);
		}
		//fin test
		
		
		
		return null;
	}
	public void finished() {
		TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, image, new Integer(0));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_INDETERMINATE, image, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, image, new Boolean(false));
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, image, Messages.getString("TSaveAsTiffWorker.12"), Color.blue); //$NON-NLS-1$
		TEventHandler.handleMessage(event);
	}
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
