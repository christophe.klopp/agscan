package agscan.data.controler.worker;

import ij.ImagePlus;
import ij.io.FileSaver;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.batch.TBatchModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.TAlignmentView;
import agscan.data.view.table.TBatchTablePanel;
import agscan.event.TEvent;
import agscan.ioxml.TOpenAlignmentWorker;
import agscan.ioxml.TOpenFileWorker;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @rcathelin modified 2005/11/07
 * @version 2.0
 */
public class TBatchWorker extends SwingWorker implements ActionListener, ChangeListener {
  private TBatch batch;
  private int currentRunningJob, nbJobsToDo;
  private TAlignment alignment = null;
  private int nbChannels = 1;//default - added 2005/11/07 

  public TBatchWorker(TBatch b) {
    super();
    batch = b;
    currentRunningJob = -10;
  }
  public Object construct(boolean thread) {
    TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
    sdAlgo.initWithDefaults();
    String pathSeparator = System.getProperty("file.separator");
    TOpenFileWorker openFileWorker;
    TOpenAlignmentWorker openAlignmentWorker;
    //batch.setWorker(this);
    batch.setBatchRunning(true);
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
    ((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
    TAbstractJob job = ((TBatchModel)batch.getModel()).getJob();
    //job.setWorker(this);
    //recuperation des images ici
    Enumeration enu_images = ((TBatchModel)batch.getModel()).getImages().elements();
    //2005/11/07 images added
    Enumeration enu_images2 = null;
    	Enumeration enu_images3  = null;
    if (!((TBatchModel)batch.getModel()).getImages2().isEmpty()){
        enu_images2 = ((TBatchModel)batch.getModel()).getImages2().elements();
        nbChannels = 2;
    }
    if (!((TBatchModel)batch.getModel()).getImages3().isEmpty()){
    	enu_images3 = ((TBatchModel)batch.getModel()).getImages3().elements();
    	nbChannels = 3;
    }    
    Enumeration enu_status = ((TBatchModel)batch.getModel()).getProgressValues().elements();
    nbJobsToDo = 0;
    while (enu_status.hasMoreElements()) {
      if (((Integer)enu_status.nextElement()).intValue() == TAbstractJob.QUEUED) nbJobsToDo++;
    }
    enu_status = ((TBatchModel)batch.getModel()).getProgressValues().elements();
    if (job != null) {
      String imageName = null, imageName2 = null, imageName3 = null;//2 & 3 added 2005/11/07
      TGrid grid;
      TImage image;
      boolean saveImage = ((TBatchModel)batch.getModel()).getSaveImages();
      grid = ((TBatchModel)batch.getModel()).getGrid();
      Vector params = new Vector();
      params.addElement("Computed Diameter");//"Diametre calcule");//modified 2005/11/17
      params.addElement(new Double(-1));
      params.addElement(new Boolean(false));
      params.addElement(Color.lightGray);
      params.addElement(null);
      params.addElement(new Boolean(false));
      params.addElement(Color.lightGray);
      params.addElement(null);
      params.addElement(null);
      params.addElement(new Boolean(false));
      params.addElement(Color.lightGray);
      params.addElement(null);
      TColumn col = grid.getGridControler().getGridColumnsControler().addColumn(TColumn.TYPE_REAL, params, grid);
      grid.getGridModel().addParameter(col.getSpotKey(), col.getDefaultValue(), false);
      col.setEditable(false);
      col.setRemovable(false);
      col.setSynchrone(true);
      Enumeration enume = grid.getGridModel().getSpots().elements();
      TSpot spot;
      while (enume.hasMoreElements()) {
        spot = (TSpot)enume.nextElement();
        spot.setLeft(grid.getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent()));
        spot.setRight(grid.getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent()));
        spot.setTop(grid.getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent()));
        spot.setBottom(grid.getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent()));
        spot.setGlobalLeft(grid.getGridModel().getLeftSpot(spot, grid.getGridModel().getRootBlock()));
        spot.setGlobalRight(grid.getGridModel().getRightSpot(spot, grid.getGridModel().getRootBlock()));
        spot.setGlobalTop(grid.getGridModel().getTopSpot(spot, grid.getGridModel().getRootBlock()));
        spot.setGlobalBottom(grid.getGridModel().getBottomSpot(spot, grid.getGridModel().getRootBlock()));
      }
      currentRunningJob = -1;
      while (enu_status.hasMoreElements() && !STOP) {
      	double total = Runtime.getRuntime().totalMemory()/(1024*1024d);//REMI
      	double free = Runtime.getRuntime().freeMemory()/(1024*1024d);//REMI
      	DecimalFormat df = new DecimalFormat("#######0.000");//REMI
      	System.out.print("################### Memory used= " );//REMI  	
      	System.out.println(df.format((total-free)));//REMI
      	
        currentRunningJob++;
        
        imageName = (String) enu_images.nextElement();
        //added - 2005/11/07
        if (nbChannels>1)  	imageName2 = (String) enu_images2.nextElement();
        if (nbChannels>2)  	imageName3 = (String) enu_images3.nextElement();
              
        if (((Integer)enu_status.nextElement()).intValue() == TAbstractJob.QUEUED) {
          alignment = null;
          image = null;
          System.gc();
          try {
          	if (imageName.endsWith(".zaf")) {
          		//TODO voir ici si open Alignement doit etre aussi adapte
          		openAlignmentWorker = new TOpenAlignmentWorker(new File(imageName));
          		
              openAlignmentWorker.setPriority(Thread.MIN_PRIORITY);
              alignment = (TAlignment)openAlignmentWorker.construct(false);
              alignment.setBatch(batch);
              alignment.setPath(batch.getOutputDirectory());
              alignment.setName(imageName.substring(imageName.lastIndexOf(pathSeparator) + 1, imageName.lastIndexOf(".")) + ".zaf");
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, alignment, new Integer(0)));
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false)));
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment, Messages.getString("TBatchWorker.0"), Color.blue));
              FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
              enume = alignment.getGridModel().getSpots().elements();
              while (enume.hasMoreElements()) {
                spot = (TSpot)enume.nextElement();
                spot.setLeft(alignment.getGridModel().getLeftSpot(spot, (TGridBlock)spot.getParent()));
                spot.setRight(alignment.getGridModel().getRightSpot(spot, (TGridBlock)spot.getParent()));
                spot.setTop(alignment.getGridModel().getTopSpot(spot, (TGridBlock)spot.getParent()));
                spot.setBottom(alignment.getGridModel().getBottomSpot(spot, (TGridBlock)spot.getParent()));
                spot.setGlobalLeft(alignment.getGridModel().getLeftSpot(spot, alignment.getGridModel().getRootBlock()));
                spot.setGlobalRight(alignment.getGridModel().getRightSpot(spot, alignment.getGridModel().getRootBlock()));
                spot.setGlobalTop(alignment.getGridModel().getTopSpot(spot, alignment.getGridModel().getRootBlock()));
                spot.setGlobalBottom(alignment.getGridModel().getBottomSpot(spot, alignment.getGridModel().getRootBlock()));
              }
              TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
              job.execute(alignment);
              TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY, alignment, sdAlgo));
              TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));
              ( (TBatchModel) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.DONE), currentRunningJob);
            }
            else {
            	openFileWorker = null;//init added 2005/11/07
              	if  (nbChannels==1) openFileWorker = new TOpenFileWorker(new File(imageName));
            	else if (nbChannels==2) openFileWorker = new TOpenFileWorker(new File(imageName),new File(imageName2));
            	else if (nbChannels==3) openFileWorker = new TOpenFileWorker(new File(imageName),new File(imageName2),new File(imageName3));
            	
             // openFileWorker = new TOpenFileWorker(new File(imageName));
              openFileWorker.setPriority(Thread.MIN_PRIORITY);
              image = (TImage) openFileWorker.construct(false);
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0)));
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false)));
              TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchWorker.0"), Color.blue));
              System.out.println("image a pour nom: "+image.getName());
              System.out.println("image a pour path: "+image.getPath());
              if (image != null) {
                image.getModel().setReference(image);
                grid.getGridModel().initialize();
                grid.getGridControler().clearMementos();
                alignment = new TAlignment(new TAlignmentModel(image.getImageModel(), grid.getGridModel(), saveImage), image, grid, batch);
                alignment.setPath(batch.getOutputDirectory());
                System.out.println("TBatchWorker nom1 =>>> "+imageName.substring(imageName.lastIndexOf(pathSeparator) + 1, imageName.lastIndexOf(".")) + ".zaf");
                System.out.println("TBatchWorker nom2 =>>> "+image.getName().substring(0, image.getName().lastIndexOf(".")) + ".zaf");
                //alignment.setName(imageName.substring(imageName.lastIndexOf(pathSeparator) + 1, imageName.lastIndexOf(".")) + ".zaf");//test
                alignment.setName(image.getName().substring(0, image.getName().lastIndexOf(".")) + "_batchMode.zaf");
                TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
                ( (TBatchModel) batch.getModel()).getImages().setElementAt(alignment.getPath() + pathSeparator + alignment.getName(), currentRunningJob);
                batch.getView().getTablePanel().repaint();
                TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));
                TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
                job.execute(alignment);
                
                //ici modif snapshot 2005/12/01: le batch fait automatiquement un tif de l'alignement....
                //TODO modifier le menu du batch pour donner le choix de faire ce snapshot ou pas.
                //recuperation de la vue de l'alignement 
                TAlignmentView tav = (TAlignmentView)(alignment.getView());
                //obrtention depuis le graphicPanel de la vue de l'image de l'alignement!
                BufferedImage buffImage = tav.getGraphicPanel().getSnapshot(10000);//10000 is the dimMax for the image (if the w or h is higher max is used)        		
                String imagePath=alignment.getPath()+File.separator+image.getName().substring(0, image.getName().lastIndexOf("."))+"_snapshot.tif";
                //creation of the image
                Image im = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
                //save of the image converted into imagePlus 
                FileSaver saver = new FileSaver(new ImagePlus("snapshot",im));//ij  
                System.out.println("Image Path ="+imagePath);
                saver.saveAsTiff(imagePath);
                //ici fin modif snapshot 2005/12/01
                TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));
                ( (TBatchModel) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.DONE), currentRunningJob);
              }
              else {
              	TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(100)));
              	( (TBatchModel) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.FAILED), currentRunningJob);
              }
            }
          }
          catch (Exception ex) {
            TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(100)));
            ( (TBatchModel) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.FAILED), currentRunningJob);
            ex.printStackTrace();
          }
          batch.getView().getTablePanel().repaint();
        }
      }
      if (STOP) ( (TBatchModel) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.QUEUED), currentRunningJob);
      if (grid.getGridModel() != null) grid.getGridModel().initialize();
    }
    return null;
  }
  public int getCurrentRunningJob() {
    return currentRunningJob;
  }
  public int getNbJobsToDo() {
    return nbJobsToDo;
  }
  public void finished() {
    //batch.setWorker(null);
    batch.setBatchRunning(false);
    TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
    TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchWorker.0"), Color.blue);
    TEventHandler.handleMessage(ev);
    ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null);
    TEventHandler.handleMessage(ev);
    currentRunningJob = -10;
    nbJobsToDo = 0;
    alignment = null;
    //((TBatchTablePanel)batch.getView().getTablePanel()).setShowCurrentAlignmentCheckBoxSelected(false);
    ((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
  }
  public void actionPerformed(ActionEvent event) {
    int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TBatchWorker.1"),
    		 Messages.getString("TBatchWorker.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    if (rep == JOptionPane.YES_OPTION) {
      stop();
    }
  }
  public TAlignment getCurrentAlignment() {
    return alignment;
  }
  public void stateChanged(ChangeEvent e) {
    int val = ((JSlider)e.getSource()).getValue();
    switch (val) {
      case 0 :
        setPriority(Thread.MIN_PRIORITY);
        break;
      case 1 :
        setPriority(Thread.NORM_PRIORITY);
        break;
      case 2 :
        setPriority(Thread.MAX_PRIORITY);
        break;
    }
  }
}
