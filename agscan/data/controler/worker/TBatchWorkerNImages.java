package agscan.data.controler.worker;

import ij.ImagePlus;
import ij.io.FileSaver;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.SwingWorker;
import agscan.TEventHandler;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.controler.TAlignmentControler;
import agscan.data.controler.TBatchControler;
import agscan.data.controler.TBatchControlerNImages;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.data.element.image.TImage;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.batch.TBatchModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.TAlignmentView;
import agscan.data.view.table.TBatchTablePanel;
import agscan.event.TEvent;
import agscan.ioxml.TOpenAlignmentWorker;
import agscan.ioxml.TOpenFileWorker;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @rcathelin modified 2005/11/07
 * @version 2.0
 */
public class TBatchWorkerNImages extends SwingWorker implements ActionListener, ChangeListener {
	private TBatch batch;
	private int currentRunningJob, nbJobsToDo;
	private TAlignment alignment = null;
	private int nbChan; 

	public TBatchWorkerNImages(TBatch b) {
		super();
		batch = b;
		currentRunningJob = -10;
		nbChan = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");//default - added 2005/11/07 
		System.out.println("IL Y A "+nbChan+" IMAGES");
	}
	public Object construct(boolean thread) {

		String pathSeparator = System.getProperty("file.separator");
		TOpenFileWorker openFileWorker;
		TOpenAlignmentWorker openAlignmentWorker;
		batch.setWorker(this);
		batch.setBatchRunning(true);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
		((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
		TAbstractJob job = ((TBatchModelNImages)batch.getModel()).getJob();
		job.setWorker(this);

		Enumeration enu_status = ((TBatchModelNImages)batch.getModel()).getProgressValues().elements();
		nbJobsToDo = 0;

		// comptage du nombre de groupes d'images de la pile
		while (enu_status.hasMoreElements()) {
			if (((Integer)enu_status.nextElement()).intValue() == TAbstractJob.QUEUED) nbJobsToDo++;
		}
		System.out.println("Nombre de taches : "+nbJobsToDo);
		enu_status = ((TBatchModelNImages)batch.getModel()).getProgressValues().elements();

		// si la pile n'est pas vide 
		if (job != null && nbChan>0) {

			TImage image;
			currentRunningJob = -1;


			// init de la liste des noms des images
			String[] imageName= new String[nbChan];
			for(int i=0;i<nbChan;i++)
			{
				imageName[i]=null;
			}

			int n=0;
			while (enu_status.hasMoreElements() && !STOP) {
				n++;

				currentRunningJob++;

				System.gc();


				// si la tache est en attente
				if (((Integer)enu_status.nextElement()).intValue() == TAbstractJob.QUEUED) {
					alignment = null;
					image = null;
					System.gc();
					try {

						TGrid grid;

						imageName[0] = (String)((TBatchModelNImages)batch.getModel()).getImages(0).get(currentRunningJob);

						if (imageName[0].endsWith(".zaf")) {   		

							//TODO voir ici si open Alignement doit etre aussi adapte
							openAlignmentWorker = new TOpenAlignmentWorker(new File(imageName[0]));
							openAlignmentWorker.setPriority(Thread.MIN_PRIORITY);

							alignment = (TAlignment)openAlignmentWorker.construct(false);

							System.err.println(alignment==null);

							alignment.setBatch(batch);

							//grid = ((TBatchModelNImages)batch.getModel()).getGrid();
							grid = (TGrid)((TAlignmentModel)alignment.getModel()).getGridModel().getReference();
							//TAlignmentModel.this.
							//grid = alignment.getGridModel().
							//  recuperation de la grille et de l'etat de la checkbox saveImages

							boolean saveImage = ((TBatchModelNImages)batch.getModel()).getSaveImages();

							((TAlignmentModel)alignment.getModel()).setSaveImage(saveImage);
							alignment.setPath(batch.getOutputPath());
							alignment.setName(imageName[0].substring(imageName[0].lastIndexOf(pathSeparator) + 1, imageName[0].lastIndexOf(".")) + ".zaf");
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, alignment, new Integer(0)));
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false)));
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment, Messages.getString("TBatchWorker.0"), Color.blue));
							FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());

							TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
							job.execute(alignment);
							TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));
							( (TBatchModelNImages) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.DONE), currentRunningJob);
						}
						else
						{
							grid = ((TBatchModelNImages)batch.getModel()).getGrid();
							//  recuperation de la grille et de l'etat de la checkbox saveImages

							boolean saveImage = ((TBatchModelNImages)batch.getModel()).getSaveImages();
							Vector params = new Vector();
							params.addElement("Computed Diameter");//"Diametre calcule");//modified 2005/11/17
							params.addElement("Computed Diameter");
							params.addElement(new Double(-1.0));
							params.addElement(new Boolean(false));
							params.addElement(Color.lightGray);
							params.addElement(null);
							params.addElement(new Boolean(false));
							params.addElement(Color.lightGray);
							params.addElement(null);
							params.addElement(null);
							params.addElement(new Boolean(false));
							params.addElement(Color.lightGray);
							params.addElement(null);
							System.err.println(grid==null);
							TColumn col = grid.getGridControler().getGridColumnsControler().addColumn(TColumn.TYPE_REAL, params, grid);
							grid.getGridModel().addParameter(col.getSpotKey(), col.getDefaultValue(), false);
							col.setEditable(false);
							col.setRemovable(false);

							for(int i=0;i<nbChan;i++)
							{
								imageName[i] = (String)((TBatchModelNImages)batch.getModel()).getImages(i).get(currentRunningJob);
							} 

							// on ouvre les 1,2 ou 3 premiers canaux pour l'affichage
							openFileWorker = null;
							File[] f = new File[nbChan];
							for (int i=0;i<nbChan;i++){
									f[i]=new File(imageName[i]);
								}
							openFileWorker = new TOpenFileWorker(f);



							System.out.println("Groupe d'images :");
							for (int i=0;i<imageName.length;i++)
							{
								System.out.println(imageName[i]);
							}		

							// petite priorite : TODO NMARY : voir pourquoi
							openFileWorker.setPriority(Thread.MIN_PRIORITY);

							// construction de l'image RGB
							image = (TImage) openFileWorker.construct(false);

							// init des barres de statut
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0)));
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false)));
							TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchWorker.0"), Color.blue));

							// si l'image max a bien ete construite
							if (image != null) {
								// cette image max devient la reference du modele
								image.getModel().setReference(image);
								// reinit du modele de grille
								grid.getGridModel().initialize();
								grid.getGridControler().clearMementos();
								// creation d'un nouvel alignement 
								alignment = new TAlignment(new TAlignmentModel(image.getImageModel(), grid.getGridModel(), saveImage), image, grid, batch);
								alignment.setPath(batch.getOutputPath());
								alignment.setName(image.getName().substring(0, image.getName().lastIndexOf(".")) + "_batchMode.zaf");
								alignment.getModel().setReference(alignment);
								TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.UNSELECT_ALL, alignment));
								TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
								((TBatchModelNImages) batch.getModel()).getImages(0).setElementAt(alignment.getPath() + pathSeparator + alignment.getName(), currentRunningJob);
								batch.getView().getTablePanel().repaint();
								TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment));
								System.out.println(n+"e passage");
								TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));
								// blocage de tout le panel


								// lancement des traitements sur le groupe d'images
								job.execute(alignment);

								// deblocage


								// snapshot !
								if (AGScan.prop.getIntProperty("TProcessChooserAction.snap")==1)
								{
									// TODO modifier le menu du batch pour donner le choix de faire ce snapshot ou pas.
									TAlignmentView tav = (TAlignmentView)(alignment.getView());
									tav.refresh();
									tav.getGraphicPanel().repaint();
									BufferedImage buffImage = tav.getGraphicPanel().getSnapshot(10000);    		
									String imagePath=alignment.getPath()+File.separator+image.getName().substring(0, image.getName().lastIndexOf("."))+"_snapshot.tif";
									Image im = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
									FileSaver saver = new FileSaver(new ImagePlus("snapshot",im));//ij  
									// sauvegarde du snapshot
									saver.saveAsTiff(imagePath);

								}

								if (AGScan.prop.getIntProperty("TProcessChooserAction.tiff")==1)
								{
									// TODO modifier le menu du batch pour donner le choix de faire ce snapshot ou pas.
									TAlignment ali = (TAlignment)(getCurrentAlignment());
									TImage image2 = ali.getImage();
									String filename=alignment.getPath()+File.separator+image.getName().substring(0, image.getName().lastIndexOf("."))+".tif";
									File file = new File(filename);
									TSaveAsTiffWorker worker = new TSaveAsTiffWorker(file, image2);
									worker.setPriority(Thread.MAX_PRIORITY);
									worker.construct(false);
									worker.finished();

								}

								// sauvegarde de l'alignement
								TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, alignment));

								// fin de tache
								( (TBatchModelNImages) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.DONE), currentRunningJob);
							}

							else {
								// echec dans la tache
								TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(100)));
								( (TBatchModelNImages) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.FAILED), currentRunningJob);
							}
						}
					}
					catch (Exception ex) {
						// echec dans la tache
						TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment, new Integer(100)));
						( (TBatchModelNImages) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.FAILED), currentRunningJob);
						ex.printStackTrace();
					}
					batch.getView().getTablePanel().repaint();
					System.out.println("ENUMERATION : "+enu_status.hasMoreElements());
				}

			}
			// tache stoppee
			if (STOP) ( (TBatchModelNImages) batch.getModel()).getProgressValues().setElementAt(new Integer(TAbstractJob.QUEUED), currentRunningJob);
		}
		return null;
	}
	public int getCurrentRunningJob() {
		return currentRunningJob;
	}
	public int getNbJobsToDo() {
		return nbJobsToDo;
	}
	public void finished() {
		batch.setWorker(null);
		batch.setBatchRunning(false);
		TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, batch));
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchWorker.0"), Color.blue);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null);
		TEventHandler.handleMessage(ev);
		currentRunningJob = -10;
		nbJobsToDo = 0;
		alignment = null;
		((TBatchTablePanel)batch.getView().getTablePanel()).refresh();
	}
	public void actionPerformed(ActionEvent event) {
		int rep = JOptionPane.showConfirmDialog(null, Messages.getString("TBatchWorker.1"),
				Messages.getString("TBatchWorker.2"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (rep == JOptionPane.YES_OPTION) {
			stop();
		}
	}
	public TAlignment getCurrentAlignment() {
		return alignment;
	}
	public void stateChanged(ChangeEvent e) {
		int val = ((JSlider)e.getSource()).getValue();
		switch (val) {
		case 0 :
			setPriority(Thread.MIN_PRIORITY);
			break;
		case 1 :
			setPriority(Thread.NORM_PRIORITY);
			break;
		case 2 :
			setPriority(Thread.MAX_PRIORITY);
			break;
		}
	}

	public TBatch getBatch()
	{
		return batch;
	}
}
