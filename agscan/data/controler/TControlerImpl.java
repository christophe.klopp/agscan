package agscan.data.controler;

import agscan.data.controler.memento.TDataElementMementos;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.element.TDataElement;
import agscan.event.TEvent;

public abstract class TControlerImpl {
  protected TDataElement reference;
  protected TDataElementMementos mementos;
  public static final int SAVE = 1001;
  public static final int SAVE_AS = 1002;
  
  public static final int SAVE_AS_IMAGE = 1006;//remi 24/08/05
  /*
  public static final int SAVE_AS_FUJI = 1003;
  public static final int SAVE_AS_TINA = 1004;
  public static final int SAVE_AS_JPEG = 1005;
  public static final int SAVE_AS_TIFF = 1006;
  public static final int SAVE_AS_PNG = 1007;
  public static final int SAVE_AS_BMP = 1008;
  */
  public static final int UNDO = 1009;
  public static final int REDO = 1010;
  public static final int CLEAR_MODIFS = 1012;

  public TControlerImpl() {
    reference = null;
    mementos = new TDataElementMementos();
  }
  public TControlerImpl(TDataElement ref) {
    this();
    reference = ref;
  }
  public TUndoElement peekMemento() {
    if (mementos != null) return mementos.peekMemento();
    return null;
  }
  public TUndoElement getNextMemento() {
    if (mementos != null) return mementos.getNextMemento();
    return null;
  }
  public TDataElementMementos getMementos() {
    return mementos;
  }
  public void clearMementos() {
    mementos.clear();
  }
  public abstract Object[] processEvent(TEvent event);
  
  public boolean hasUndoMementos() {
    return mementos.hasUndo();
  }
  public boolean hasRedoMementos() {
    return mementos.hasRedo();
  }
  public void setReference(TDataElement ref) {
    reference = ref;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
