package agscan.data.controler;

import ij.ImagePlus;
import ij.process.ImageProcessor;

import java.io.File;

import javax.swing.JOptionPane;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.memento.TImageAction;
import agscan.data.controler.memento.TImageState;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.controler.worker.TSaveAsImageWorker;
import agscan.data.controler.worker.TTransposeWorker;
import agscan.data.element.image.TImage;
import agscan.data.model.TImageModel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.dialog.TDialogManager;
//import agscan.dialog.histogram.transfercurve.TTransferCurve;//2005/11/16 transfer curves removed
import agscan.event.TEvent;
import agscan.factory.TImageModelFactory;
import agscan.menu.TMenuManager;
import agscan.menu.action.TSaveAsAction;

public class TImageControler extends TControlerImpl {
	public static final int MIN_AND_MAX = 501;
	public static final int MIN = 502;
	public static final int MAX = 503;
	public static final int VERTICAL_FLIP = 504;
	public static final int HORIZONTAL_FLIP = 505;
//	public static final int TRANSFER_CURVE = 506;//2005/11/16 transfer curves removed
	public static final int ROTATE_PLUS_90 = 507;
	public static final int ROTATE_MOINS_90 = 508;
	public static final int ROTATE_180 = 509;
	public static final int ADD_MEMENTO = 512;
	public static final int INVERT_LUT = 513;
	private boolean lastProcessOk;
	
	public TImageControler(TImage ref) {
		super(ref);
//		2005/11/16 transfer curves removed
		//mementos.pokeMemento(new TImageState(0, 255, ref.getImageView().getImageGraphicPanel().getTransferCurve(),
		//		ref.getImageView().getImageGraphicPanel().getTransferCurve().getParam()));
		mementos.pokeMemento(new TImageState(0, 255));
	}
	public Object[] processEvent(TEvent event) {
		int action = event.getAction();
		double min, max;
		String sameThread;//ajout 12/09/05
		TImage image = (TImage)reference;
		TUndoElement undoElement;
		TImageAction ia;
		TEvent ev;
		boolean b;
		lastProcessOk = true;
		
		switch (action) {
		case MIN_AND_MAX:
			min = ((Double)event.getParam(1)).doubleValue();
			max = ((Double)event.getParam(2)).doubleValue();
			System.out.println("TImageControler=> min ="+min); //$NON-NLS-1$
			System.out.println("TImageControler=> max ="+max); //$NON-NLS-1$
			
			setMinAndMax(min, max);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			break;
		case MIN:
			min = ((Double)event.getParam(1)).doubleValue();
			max = image.getImageView().getMax();
			setMinAndMax(min, max);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			break;
		case MAX:
			max = ((Double)event.getParam(1)).doubleValue();
			min = image.getImageView().getMin();
			setMinAndMax(min, max);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			break;
//			2005/11/16 transfer curves remove
			/*
		case TRANSFER_CURVE:
			image.getImageView().getImageGraphicPanel().setTransferCurve((TTransferCurve)event.getParam(1));
			setMinAndMax();
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			break;
			*/
		case VERTICAL_FLIP:
		 sameThread = ((String)event.getParam(1));
		// sameThread.toString();//test pour cracher une error
		 System.out.println("samethread="+sameThread);
			flipVertical(Messages.getString("TImageControler.2"),sameThread); //$NON-NLS-1$
			ia = new TImageAction(0, false, true);
			mementos.pokeMemento(ia);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			image.getModel().addModif(1);
			break;
		case HORIZONTAL_FLIP:
			sameThread = ((String)event.getParam(1));
			flipHorizontal(Messages.getString("TImageControler.3"),sameThread); //$NON-NLS-1$
			ia = new TImageAction(0, true, false);
			mementos.pokeMemento(ia);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			image.getModel().addModif(1);
			break;
		case INVERT_LUT:
			//TODO le undo/redo de cette action soit a mettre avec TImagAction (pas prvu au depart...sinon?)
			//TODO le invertLut devrait meme etre fait ailleurs dans un worker ? non?
			ImageProcessor  p = image.getImageView().getDisplayImage().getProcessor();//recup de l'imageProcessor
			p.invertLut();
			ImagePlus invertImp = new ImagePlus(null,p);
			image.getImageView().setDisplayImage(invertImp);// creation d'une imagePlus avec la modif
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));//utile?
			break;
		case ROTATE_PLUS_90:
			 sameThread = ((String)event.getParam(1));
			rotatePlus90(Messages.getString("TImageControler.4"),sameThread); //$NON-NLS-1$
			ia = new TImageAction(90, false, false);
			mementos.pokeMemento(ia);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			image.getModel().addModif(1);
			break;
		case ROTATE_MOINS_90:
			 sameThread = ((String)event.getParam(1));
			rotateMoins90(Messages.getString("TImageControler.5"),sameThread); //$NON-NLS-1$
			ia = new TImageAction(-90, false, false);
			mementos.pokeMemento(ia);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			image.getModel().addModif(1);
			break;
		case ROTATE_180:
			 sameThread = ((String)event.getParam(1));
			rotate180(Messages.getString("TImageControler.6"),sameThread); //$NON-NLS-1$
			ia = new TImageAction(180, false, false);
			mementos.pokeMemento(ia);
			TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
			image.getModel().addModif(1);
			break;
		case SAVE:
			saveCurrentData();
			break;
			//remi modif 24/08/05 remplac par:
		case SAVE_AS_IMAGE:
			ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.IS_DATA_OPENED, event.getParam(0), (File)event.getParam(1));
			b = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();//test if file already exists
			if (!b)
				saveDataAsImage((File)event.getParam(1),(String)event.getParam(2),((Integer)event.getParam(3)).intValue());//remi 24/08/05 - modified 2005/11/02, add of the third param
			else
				messageFichierOuvert(((File)event.getParam(1)).getAbsolutePath());//message "file exists"
			break;
		case UNDO:
			System.out.println("TImageControler ==> case UNDO"); //$NON-NLS-1$
			//TODO comprendre et faire fonctionner les undo/redo
			undoElement = peekMemento();
			undo(undoElement);
			if (undoElement instanceof TImageAction) image.getModel().addModif(-1);
			break;
		case REDO:
			undoElement = getNextMemento();
			redo(undoElement);
			if (undoElement instanceof TImageAction) image.getModel().addModif(1);
			break;
		case ADD_MEMENTO:
			mementos.pokeMemento(image.createStateSnapshot());
			break;
		case CLEAR_MODIFS:
			reference.getModel().clearModifs();
			break;
		default:
			lastProcessOk = false;
		break;
		}
		return null;
	}
	/**
	 * remi
	 * permet d'appeler le worker qui gere la sauvegarde d'un fichier image
	 * modification - 2005/11/02 => add of a param: the channel image to save
	 * @param file - the new File to create = new image
	 * @param string - the extension to use to save this image
	 * @param channel - the channel to save 
	 * @version 2005/08/24
	 * @see TSaveAsImageWorker
	 */
	private void saveDataAsImage(File file, String extension, int channel) {
		TSaveAsImageWorker worker = new TSaveAsImageWorker(file, (TImage)reference, extension, channel);
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.construct(false);
		worker.finished();
	}
	public boolean isLastProcessOk() {
		return lastProcessOk;
	}
	private void setMinAndMax() {
		TImageGraphicPanel tigp = ((TImage)reference).getImageView().getImageGraphicPanel();
		setMinAndMax(tigp.getMin(), tigp.getMax());
	}
	
	
	private void setMinAndMax(double min, double max) {
		System.out.println("TImageControler.setMinAndMax(), min ="+min); //$NON-NLS-1$
		System.out.println("TImageControler.setMinAndMax(), max ="+max); //$NON-NLS-1$
		System.out.println("Dans la methode TImageControler.setMinAndMax() RENDU INOPERANTE: A QUOI SERVAIT-ELLE?"); //$NON-NLS-1$
		TImage image = (TImage)reference;
		ImagePlus pi = ((TImageModel)image.getModel()).getInitialImage();
		int start, end;
		//LookupTableJAI lookupTable;
		
		image.getImageView().setMax((int)max);
		image.getImageView().setMin((int)min);
		start = (int)(255 * min / (min - max));
		if (start < 0) start = 0;
		end = (int)((255 - min) * 255 / (max - min));
		if (end > 255) end = 255;
		
		//lookupTable = image.getImageView().getImageGraphicPanel().getTransferCurve().createLookupTable(min, max, start, end);
		//image.getImageView().setDisplayImage(LookupDescriptor.create(pi, lookupTable, null));
	}
	
	//remi le 22/08/05
	//ajout de cette methode qui permet d'inverser ou non le LUT
	// selon ce que l'on veut sur une image
	// a utiliser pour donner le LUT comme on veut (invers ou non)
	/// LUT = Look-up table => table de correpondance pour gere la palettre des couleurs  l'affichage
	// true alors le LUT de l'image en sortie sera invers
	// false alors le LUT de l'image en sortie ne sera pas invers
	public static ImagePlus setInvertedLUT(ImagePlus imp,boolean val){
		// si c'est pas comme on veut, on inverse, sinon on laisse
		ImagePlus invertImp = new ImagePlus();
		try {
			if (!imp.getProcessor().isInvertedLut()==val){
				ImageProcessor p = imp.getProcessor();
				p.invertLut();
				invertImp = new ImagePlus(null,p);
			}
			else return imp;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Is there an .img file available", "Problem with the input file", JOptionPane.ERROR_MESSAGE);
			// TODO: handle exception
		}
		return invertImp;

	}
	
	
	
	
	//remi 2005/10/14 modif ajout de setInvertedLUT aui agit directement sur ImageProcessor
	// c'est plus facile pour l'acces a partir d'un stack que de creer une ImagePlus
	
	//ajout de cette methode qui permet d'inverser ou non le LUT
	// selon ce que l'on veut sur une image
	// a utiliser pour donner le LUT comme on veut (invers ou non)
	/// LUT = Look-up table => table de correpondance pour gere la palettre des couleurs  l'affichage
	// true alors le LUT de l'image en sortie sera invers
	// false alors le LUT de l'image en sortie ne sera pas invers
	public static ImageProcessor setInvertedLUT(ImageProcessor proc,boolean val){
		// si c'est pas comme on veut, on inverse, sinon on laisse
		if (!proc.isInvertedLut()==val){
			proc.invertLut();
			return proc;
		}
		else return proc;
	}
	
	
	
	
	/*
	 * mthodes flip remplaces le 12/09/05 par des methodes avec un param supplmentaire 
	private void flipVertical(String s) {
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "flip_vertical", s); //$NON-NLS-1$
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	private void flipHorizontal(String s) {
		//TTransposeWorker worker = new TTransposeWorker((TImage)reference, TransposeDescriptor.FLIP_HORIZONTAL, s);
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "flip_horizontal", s); //$NON-NLS-1$
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	*/

	/**
	 * flipVertical manages the worker that will make vertical flip to the image
	 * @param s message to display during the flip
	 * @param sameThread says if the operation is done in the same thread or not 
	 * (if true : no new  thread, false = new thread created
	 * note: to do action in the same thread make wait for the end of the action)
	 */
	private void flipVertical(String s, String sameThread) {
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "flip_vertical", s); //$NON-NLS-1$
		// an other thread is called to do the rotation 
		if (sameThread=="false"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.start();
		}
		// the same thread does action
		else if (sameThread=="true"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.construct(false);
			worker.finished();
		}
		else System.err.println("param sameThread must be \"true\" or \"false\"!");
	}

	/**
	 * flipHorizontal manages the worker that will make horizontal flip to the image
	 * @param s message to display during the flip
	 * @param sameThread says if the operation is done in the same thread or not 
	 * (if true : no new  thread, false = new thread created
	 * note: to do action in the same thread make wait for the end of the action)
	 */
	private void flipHorizontal(String s, String sameThread) {
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "flip_horizontal", s); //$NON-NLS-1$
		// an other thread is called to do the rotation 
		if (sameThread=="false"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.start();
		}
		// the same thread does action
		else if (sameThread=="true"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.construct(false);
			worker.finished();
		}
		else System.err.println("param sameThread must be \"true\" or \"false\"!");
	}

	
	/**
	 * rotatePlus90 manages the worker that will make a 90 clockwise rotation to the image
	 * @param s message to display during the rotation
	 * @param sameThread says if the operation is done in the same thread or not 
	 * (if true : no new  thread, false = new thread created
	 * note: to do action in the same thread make wait for the end of the action)
	 */
	private void rotatePlus90(String s, String sameThread) {
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "90_clockwise", s); //$NON-NLS-1$
		// an other thread is called to do the rotation 
		if (sameThread=="false"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.start();
		}
		// the same thread does action
		else if (sameThread=="true"){
			worker.setPriority(Thread.MIN_PRIORITY);
			worker.construct(false);
			worker.finished();
		}
		else System.err.println("param sameThread must be \"true\" or \"false\"!");
	}

	/**
	 * rotateMoins90 manages the worker that will make a 90 counter-clockwise rotation to the image
	 * @param s message to display during the rotation
	 * @param sameThread says if the operation is done in the same thread or not 
	 * (if true : no new  thread, false = new thread created
	 * note: to do action in the same thread make wait for the end of the action)
	 */
	 private void rotateMoins90(String s, String sameThread) {
		 TTransposeWorker worker = new TTransposeWorker((TImage)reference, "90_counter_clockwise", s); //$NON-NLS-1$
			// an other thread is called to do the rotation  
			if (sameThread=="false"){
				worker.setPriority(Thread.MIN_PRIORITY);
				worker.start();
			}
			// the same thread does action
			else if (sameThread=="true"){
				worker.setPriority(Thread.MIN_PRIORITY);
				worker.construct(false);
				worker.finished();
			}
			else System.err.println("param sameThread must be \"true\" or \"false\"!");
		}
		
	/*
	 * remplaces le 12/09/05
	 private void rotateMoins90(String s) {
	 TTransposeWorker worker = new TTransposeWorker((TImage)reference, "90_counter_clockwise", s); //$NON-NLS-1$
	 worker.setPriority(Thread.MIN_PRIORITY);
	 worker.start();
	 }
	
	private void rotate180(String s) {
		TTransposeWorker worker = new TTransposeWorker((TImage)reference, "180", s); //$NON-NLS-1$
		worker.setPriority(Thread.MIN_PRIORITY);
		worker.start();
	}
	*/
	/**
	 * rotate180 manages the worker that will make a 180 degrees rotation to the image
	 * @param s message to display during the rotation
	 * @param sameThread says if the operation is done in the same thread or not 
	 * (if true : no new  thread, false = new thread created
	 * note: to do action in the same thread make wait for the end of the action)
	 */
	 private void rotate180(String s, String sameThread) {
		 TTransposeWorker worker = new TTransposeWorker((TImage)reference, "180", s); //$NON-NLS-1$
			// an other thread is called to do the rotation  
			if (sameThread=="false"){
				worker.setPriority(Thread.MIN_PRIORITY);
				worker.start();
			}
			// the same thread does action
			else if (sameThread=="true"){
				worker.setPriority(Thread.MIN_PRIORITY);
				worker.construct(false);
				worker.finished();
			}
			else System.err.println("param sameThread must be \"true\" or \"false\"!");
		}
			
	
	
	//ajout le 02/09/05
	/*
	 * convert a QL tab of intensity values into a PSL tab of intensity values
	 * @return the tab converted into PSL unit
	 * @param data tab of intensity values (in QL unit) 
	 * @param latitude latitude (dynamic range of the scanner used for this image)
	 */
		//les donnes sont donnes comme intensits donc valeurs entieres. La conversion peut engendrer des valeurs non entieres
	 //TODO attention concversions PSL en double et QL double ou int???
	public static double[] ql2pslData(int[] data, double latitude) {
		double[] _data = new double[data.length];
		//System.err.print(" Converting QL to PSL ...");
		for (int i = 0; i < data.length; i++) _data[i] = (double)TImageModelFactory.ql2psl((double)data[i], 65535.0D, latitude);
		return _data;
	}
	
	
	//ajout le 02/09/05
	/*
	 * convert a PSL tab of intensity values into a QL tab of intensity values
	 * @return the tab converted into QL unit
	 * @param data tab of intensity values (in PSL unit) 
	 * @param latitude latitude (dynamic range of the scanner used for this image)
	 */
	//les donnes sont donnes comme intensits donc valeurs entieres. La conversion peut engendrer des valeursd non entieres
	public static double[] psl2qlData(int[] data, double latitude) {
		double[] _data = new double[data.length];
		//System.err.print(" Converting QL to PSL ..."); 
		for (int i = 0; i < data.length; i++) _data[i] = (int)TImageModelFactory.psl2ql((double)data[i], 65535.0D, latitude);
		return _data;
	}
	
	public static boolean okToWrite(String filename) {
		int result = JOptionPane.YES_OPTION;
		if (new File(filename).exists())
			result = JOptionPane.showConfirmDialog(null, Messages.getString("TImageControler.18") + filename + //$NON-NLS-1$
					Messages.getString("TImageControler.19"), //$NON-NLS-1$
					Messages.getString("TImageControler.20"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$
		return (result == JOptionPane.YES_OPTION);
	}
	private void saveCurrentData() {
		//2005/11/02 - adaptation to multi channels image = save each image one by one 
		TImage image = (TImage)reference;
		String ff = image.getImageModel().getFileFormat();
		//String name = image.getName();
		TImageModel imageModel =  (TImageModel)image.getModel();
		for (int i=1;i<=imageModel.getNumberOfChannels();i++){
		//String path = imageModel.getChannelImage(i).getTitle();
			String path = image.getPath();
			String name = imageModel.getChannelImage(i).getTitle();
			//String name2 = image.getName();
			//System.out.println("nom : "+ name2);
			System.out.println( "ip title = "+imageModel.getChannelImage(i).getTitle());	
	
		//TODO etre sur que le getTitle contient le path entier...
		//String path = image.getPath();
		if (!path.equals("")) { //$NON-NLS-1$
			File file = new File(path + name);
			System.out.println("path+name= "+path+name);
			System.out.println("ext format="+ ff);
	
			saveDataAsImage(file,ff,i);
		
		}
		else {
			// si path courant non identifi, on lance le "save as"
			// qui demandera nom et extension
			TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.FIRE_ACTION, new Integer(TSaveAsAction.getID()));//TMenuManager.SAVE_AS avant
			TEventHandler.handleMessage(event);
		}
		}
	}
	private void messageFichierOuvert(String filename) {
		JOptionPane.showMessageDialog(null, Messages.getString("TImageControler.28") + filename + Messages.getString("TImageControler.29"), //$NON-NLS-1$ //$NON-NLS-2$
				Messages.getString("TImageControler.30"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
	}
	public void undo(TUndoElement undoElement) {
		System.out.println("undo= "+undoElement.toString());//test remi //$NON-NLS-1$
		if (undoElement instanceof TImageAction) {
			if (((TImageAction)undoElement).getHorizontalFlip())
				flipHorizontal(Messages.getString("TImageControler.32"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getVerticalFlip())
				flipVertical(Messages.getString("TImageControler.33"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == 90)
				rotateMoins90(Messages.getString("TImageControler.34"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == -90)
				rotatePlus90(Messages.getString("TImageControler.35"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == 180)
				rotate180(Messages.getString("TImageControler.36"),"false"); //$NON-NLS-1$
		}
		else {
			TImageState is = (TImageState)mementos.getPreviousStateMemento();
//			2005/11/16 transfer curves removed	TTransferCurve tc = is.getTransferCurve();
			double param = is.getParam();
			TImage image = (TImage)reference;
			//tc.setParam(param);//		2005/11/16 transfer curves removed
			//image.getImageView().getImageGraphicPanel().setTransferCurve(tc);//		2005/11/16 transfer curves removed
			int min = is.getMin();
			int max = is.getMax();
			setMinAndMax(min, max);
		}
		TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, reference);
		TEventHandler.handleMessage(event);
	}
	public void redo(TUndoElement undoElement) {
		if (undoElement instanceof TImageAction){
			if (((TImageAction)undoElement).getHorizontalFlip())
				flipHorizontal(Messages.getString("TImageControler.37"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getVerticalFlip())
				flipVertical(Messages.getString("TImageControler.38"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == 90)
				rotatePlus90(Messages.getString("TImageControler.39"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == -90)
				rotateMoins90(Messages.getString("TImageControler.40"),"false"); //$NON-NLS-1$
			else if (((TImageAction)undoElement).getRotation() == 180)
				rotate180(Messages.getString("TImageControler.41"),"false"); //$NON-NLS-1$
		}
		else {
		//	TTransferCurve tc = ((TImageState)undoElement).getTransferCurve();//		2005/11/16 transfer curves removed
			double param = ((TImageState)undoElement).getParam();
		//	tc.setParam(param);//		2005/11/16 transfer curves removed
			TImage image = (TImage)reference;
			//image.getImageView().getImageGraphicPanel().setTransferCurve(tc);//		2005/11/16 transfer curves removed
			int min = ((TImageState)undoElement).getMin();
			int max = ((TImageState)undoElement).getMax();
			setMinAndMax(min, max);
		}
		TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
		TEventHandler.handleMessage(event);
		event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, reference);
		TEventHandler.handleMessage(event);
	}
	//ajout integration 14/09/05
	 public void addMemento(TImageAction mem) {
	    mementos.pokeMemento(mem);
	  }
}
/******************************************************
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 2004 INSERM-ERM 206 - TAGC
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 *******************************************************/
