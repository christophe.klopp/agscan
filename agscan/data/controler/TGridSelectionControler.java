package agscan.data.controler;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Vector;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.element.TDataElement;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TGridSelectionModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.table.TGridTablePanel;
import agscan.event.TEvent;
import agscan.statusbar.TStatusBar;

public class TGridSelectionControler {
  public static final int TOGGLE_SELECT_POINT = 201;
  public static final int UNSELECT_ALL = 202;
  public static final int CLEAR_PRESELECTION = 203;
  public static final int PRESELECT = 204;
  public static final int TOGGLE_SELECT_RECT = 205;
  public static final int SELECT = 206;
  public static final int UNSELECT = 207;
  public static final int GET_PRESELECTION = 208;
  public static final int GET_SELECTION = 209;
  public static final int IS_SELECTED = 210;
  public static final int GET_BLOCK_SELECTION = 211;
  public static final int GET_ALL_SELECTED_SPOTS = 212;
  public static final int GET_SPOT_SELECTION = 213;
  public static final int SELECT_ALL = 214;
  public static final int GET_BLOCK1_SELECTION = 215;

  private boolean lastProcessOk;

  public TGridSelectionControler() {
  }
  public Object[] processEvent(TEvent event) {
    int action = event.getAction();
    Object[] ret = null;
    lastProcessOk = true;
    TGriddedElement grid = (TGriddedElement)event.getParam(0);
    switch (action) {
      case TOGGLE_SELECT_POINT:
        toggleSelect((Point)event.getParam(1), ((Integer)event.getParam(2)).intValue(),
                     ((Boolean)event.getParam(3)).booleanValue(), grid);
        break;
      case UNSELECT_ALL:
        unselectAll(grid);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.SPOT_PERCENT_LABEL, grid, Messages.getString("TGridSelectionControler.0"))); //$NON-NLS-1$
        break;
      case CLEAR_PRESELECTION:
        clearPreselection((TDataElement)event.getParam(1), grid);
        break;
      case PRESELECT:
        preSelect((Rectangle)event.getParam(1), (TDataElement)event.getParam(2), grid);
        break;
      case TOGGLE_SELECT_RECT:
        toggleSelect((Rectangle)event.getParam(1), ((Integer)event.getParam(2)).intValue(), grid);
        break;
      case SELECT:
        select(((Integer)event.getParam(1)).intValue(), grid, ((Boolean)event.getParam(2)).booleanValue());
        break;
      case UNSELECT:
        unselect(((Integer)event.getParam(1)).intValue(), grid);
        break;
      case GET_PRESELECTION:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getPreSelection();
        break;
      case GET_SELECTION:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getSelection();
        break;
      case GET_ALL_SELECTED_SPOTS:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getRecursivelySelectedSpots();
        break;
      case GET_SPOT_SELECTION:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getSelectedSpots();
        break;
      case IS_SELECTED:
        ret = new Object[1];
        TGridElement elem = (TGridElement)event.getParam(1);
        if (elem instanceof TGridBlock)
          ret[0] = new Boolean(grid.getGridModel().getSelectionModel().isSelected((TGridBlock)elem));
        else if (elem instanceof TSpot)
          ret[0] = new Boolean(grid.getGridModel().getSelectionModel().isRecursivelySelected((TSpot)elem));
        break;
      case GET_BLOCK_SELECTION:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getBlockSelection();
        break;
      case GET_BLOCK1_SELECTION:
        ret = new Object[1];
        ret[0] = grid.getGridModel().getSelectionModel().getBlock1Selection();
        break;
      case SELECT_ALL:
        selectAll(grid);
        break;
      default:
        lastProcessOk = false;
        break;
    }
    return ret;
  }
  public boolean isLastProcessOk() {
    return lastProcessOk;
  }
  private void toggleSelect(Point p, int mode, boolean ctrl, TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    TGridElement element = model.toggleSelect(p, mode, ctrl);
    ((TGridTablePanel)grid.getView().getTablePanel()).updateSelection(model.getSelectionModel());
    if (element != null)
      ((TGridTablePanel)grid.getView().getTablePanel()).showSpot((TSpot)element.getSpots().lastElement());
  }
  private Vector toggleSelect(Rectangle r, int mode, TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    if (r.width == 0) r.width = 1;
    if (r.height == 0) r.height = 1;
    Vector v = model.toggleSelect(r, mode);
    ((TGridTablePanel)grid.getView().getTablePanel()).updateSelection(model.getSelectionModel());
    return v;
  }
  private void unselectAll(TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    Vector v = new Vector(model.getSelectionModel().getRecursivelySelectedSpots());
    v.addAll(model.getSelectionModel().getPreSelection());
    model.unselectAll();
    grid.getView().getGraphicPanel().repaint(model.getSelectionModel(), v);
    ((TGridTablePanel)grid.getView().getTablePanel()).updateSelection(model.getSelectionModel());
  }
  private void selectAll(TGriddedElement grid) {
    TGridSelectionModel model = grid.getGridModel().getSelectionModel();
    model.addBlock(grid.getGridModel().getRootBlock());
    grid.getView().getGraphicPanel().repaint();
  }

  private void clearPreselection(TDataElement element, TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    Vector v = new Vector(model.getSelectionModel().getPreSelection());
    model.clearPreSelection();
    element.getView().getGraphicPanel().repaint(model.getSelectionModel(), v);
    ((TGridTablePanel)element.getView().getTablePanel()).updateSelection(model.getSelectionModel());
  }
  private void preSelect(Rectangle rect, TDataElement element, TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    if (rect.width == 0) rect.width = 1;
    if (rect.height == 0) rect.height = 1;
    model.preselect(rect);
    element.getView().getGraphicPanel().repaint(model.getSelectionModel(), model.getSelectionModel().getPreSelection());
    ((TGridTablePanel)element.getView().getTablePanel()).updateSelection(model.getSelectionModel());
    if (model.getSelectionModel().getPreSelection().size() > 0)
      ((TGridTablePanel)element.getView().getTablePanel()).showSpot((TSpot)model.getSelectionModel().getPreSelection().lastElement());
  }
  private void select(int i, TGriddedElement grid, boolean scrollGraphic) {
    TGridModel model = grid.getGridModel();
    model.select(i);
    TSpot spot = model.getSpot(i);
    Vector v = new Vector();
    v.addElement(spot);
    grid.getView().getGraphicPanel().repaint(model.getSelectionModel(), v);
    if (scrollGraphic) {
      double z = grid.getView().getGraphicPanel().getZoom();
      if (z < 0) z = -1.0D / z;
      int x1 = (int) (spot.getX() / ( (TGriddedElement) grid).getGridModel().getPixelWidth() * z);
      int y1 = (int) (spot.getY() / ( (TGriddedElement) grid).getGridModel().getPixelHeight() * z);
      int x2 = (int) ( (spot.getX() + spot.getWidth()) / ( (TGriddedElement) grid).getGridModel().getPixelWidth() * z);
      int y2 = (int) ( (spot.getY() + spot.getHeight()) / ( (TGriddedElement) grid).getGridModel().getPixelHeight() * z);
      int x = x1 - grid.getView().getGraphicScrollPane().getViewport().getViewRect().width / 2;
      if (x < 0)
        x = 0;
      int y = y1 - grid.getView().getGraphicScrollPane().getViewport().getViewRect().height / 2;
      if (y < 0)
        y = 0;
      if (!grid.getView().getGraphicScrollPane().getViewport().getViewRect().contains(x1, y1) ||
          !grid.getView().getGraphicScrollPane().getViewport().getViewRect().contains(x2, y2)) {
        grid.getView().getGraphicScrollPane().getViewport().setViewPosition(new Point(x, y));
        grid.getView().getGraphicScrollPane().revalidate();
      }
    }
  }
  private void unselect(int i, TGriddedElement grid) {
    TGridModel model = grid.getGridModel();
    model.unselect(i);
    Vector v = new Vector();
    v.addElement(model.getSpot(i));
    grid.getView().getGraphicPanel().repaint(model.getSelectionModel(), v);
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
