package agscan.data.controler;

import java.awt.Color;
import java.awt.Cursor;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.data.controler.memento.TDataElementMementos;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.table.TColumn;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.statusbar.TStatusBar;

public class TGridStructureControler {
  public static final int SPOT_WIDTH = 101;
  public static final int SPOT_HEIGHT = 102;
  public static final int LEVEL1_NB_COLUMNS = 103;
  public static final int LEVEL1_NB_ROWS = 104;
  public static final int LEVEL1_SPOT_DIAMETER = 105;
  public static final int LEVEL1_START = 106;
  public static final int LEVEL1_ID_NUMBER = 107;
  public static final int NB_LEVELS = 108;
  public static final int LEVEL2_NB_COLUMNS = 109;
  public static final int LEVEL2_NB_ROWS = 110;
  public static final int LEVEL2_COLUMN_SPACING = 111;
  public static final int LEVEL2_ROW_SPACING = 112;
  public static final int LEVEL2_START = 113;
  public static final int LEVEL2_ID_NUMBER = 114;
//level3 removed - 20005/10/28
 /* public static final int LEVEL3_NB_COLUMNS = 115;
  public static final int LEVEL3_NB_ROWS = 116;
  public static final int LEVEL3_COLUMN_SPACING = 117;
  public static final int LEVEL3_ROW_SPACING = 118;
  public static final int LEVEL3_START = 119;
  public static final int LEVEL3_ID_NUMBER = 120;
*/
  private boolean lastProcessOk;

  public TGridStructureControler() {
  }

  public Object[] processEvent(TEvent event, TDataElementMementos mementos) {
    int action = event.getAction();
    Object[] ret = null;
    lastProcessOk = true;
    TGriddedElement griddedElement = (TGriddedElement)event.getParam(0);
    switch (action) {
      case SPOT_WIDTH:
        setWidth(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case SPOT_HEIGHT:
        setHeight(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL1_NB_COLUMNS:
        setLev1NbColumns(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL1_NB_ROWS:
        setLev1NbRows(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL1_SPOT_DIAMETER:
        setSpotDiameter(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL1_START:
        setLev1Start((String)event.getParam(1), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL1_ID_NUMBER:
        setLev1IDNumber(((Boolean)event.getParam(1)).booleanValue(), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case NB_LEVELS:
        setNbLevels(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_NB_COLUMNS:
        setLev2NbColumns(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_NB_ROWS:
        setLev2NbRows(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_COLUMN_SPACING:      	
        setLev2ColumnSpacing(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_ROW_SPACING:    
        setLev2RowSpacing(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_START:
        setLev2Start((String)event.getParam(1), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL2_ID_NUMBER:
        setLev2IDNumber(((Boolean)event.getParam(1)).booleanValue(), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
        /*//  level3 removed - 20005/10/28
      case LEVEL3_NB_COLUMNS:
        setLev3NbColumns(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL3_NB_ROWS:
        setLev3NbRows(((Integer)event.getParam(1)).intValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL3_COLUMN_SPACING:
        setLev3ColumnSpacing(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL3_ROW_SPACING:
        setLev3RowSpacing(((Double)event.getParam(1)).doubleValue(), griddedElement);
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL3_START:
        setLev3Start((String)event.getParam(1), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
      case LEVEL3_ID_NUMBER:
        setLev3IDNumber(((Boolean)event.getParam(1)).booleanValue(), griddedElement);
        mementos.pokeMemento(griddedElement.createStateSnapshot());
        griddedElement.getModel().addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
        event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, griddedElement);
        TEventHandler.handleMessage(event);
        break;
        */
      default:
        lastProcessOk = false;
        break;
    }
    return ret;
  }
  public boolean isLastProcessOk() {
    return lastProcessOk;
  }
  private void setWidth(double w, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.0"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel1Params(-1, -1, w, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setHeight(double h, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.2"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel1Params(-1, -1, -1, h, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev1NbColumns(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.4"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel1Params(-1, n, -1, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev1NbRows(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.6"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel1Params(n, -1, -1, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setSpotDiameter(double d, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.8"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel1Params(-1, -1, -1, -1, d, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev1Start(String start, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.10"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    grid.getGridModel().getConfig().setLevel1Params(-1, -1, -1, -1, -1, TGridConfig.getStartValue(start), -1);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev1IDNumber(boolean b, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.12"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    grid.getGridModel().getConfig().setLevel1Params(-1, -1, -1, -1, -1, -1,
                                                    (b == true) ? TGridConfig.ID_NUMBER : TGridConfig.ID_ROW_COLUMN);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setNbLevels(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.14"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setNbLevels(n);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    grid.getView().getTablePanel().initColumnSizes((TColumn)grid.getGridModel().getConfig().getColumns().getColumn(1));
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  
  private void setLev2NbColumns(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.16"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel2Params(-1, n, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev2NbRows(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.18"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel2Params(n, -1, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
 
  
  /**
   * Updates the level2 column spacing value of the grid model and acts in consequence (dialog and grid display)
   * @param cs column spacing value (in micrometer)
   * @param grid the current grid  
   * @return void
   */
  private void setLev2ColumnSpacing(double cs, TGriddedElement grid) {
  	agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.20"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel2Params(-1, -1, cs , -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  
  /**
   * Updates the level2 row spacing value of the grid model and acts in consequence (dialog and grid display)
   * @param rs row spacing value (in micrometer)
   * @param grid the current grid  
   * @return void
   */
  private void setLev2RowSpacing(double rs, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.22"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel2Params(-1, -1, -1, rs, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev2Start(String start, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.24"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    grid.getGridModel().getConfig().setLevel2Params(-1, -1, -1, -1, TGridConfig.getStartValue(start), -1);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev2IDNumber(boolean b, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.26"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    grid.getGridModel().getConfig().setLevel2Params(-1, -1, -1, -1, -1,
                                                    (b == true) ? TGridConfig.ID_NUMBER : TGridConfig.ID_ROW_COLUMN);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  
  /*//  level3 removed - 20005/10/28
  private void setLev3NbColumns(int n, TGriddedElement grid) {
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.28"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel3Params(-1, n, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev3NbRows(int n, TGriddedElement grid) {
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.30"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel3Params(n, -1, -1, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev3ColumnSpacing(double cs, TGriddedElement grid) {
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
  TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.32"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel3Params(-1, -1, cs, -1, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev3RowSpacing(double rs, TGriddedElement grid) {
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.34"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    grid.getGridModel().getConfig().setLevel3Params(-1, -1, -1, rs, -1, -1);
    grid.getGridModel().update();
    grid.getView().getGraphicPanel().revalidate();
    grid.getView().getViewPanel().repaint();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev3Start(String start, TGriddedElement grid) {
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.36"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    grid.getGridModel().getConfig().setLevel3Params(-1, -1, -1, -1, TGridConfig.getStartValue(start), -1);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void setLev3IDNumber(boolean b, TGriddedElement grid) {
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, Messages.getString("TGridStructureControler.38"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    grid.getGridModel().getConfig().setLevel3Params(-1, -1, -1, -1, -1,
                                                    (b == true) ? TGridConfig.ID_NUMBER : TGridConfig.ID_ROW_COLUMN);
    grid.getGridModel().update();
    grid.getView().refresh();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, grid, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, grid);
    TEventHandler.handleMessage(event);
    bzscan2.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  */
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
