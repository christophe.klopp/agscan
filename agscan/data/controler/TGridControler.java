/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.memento.TColumnModifyAction;
import agscan.data.controler.memento.TColumnState;
import agscan.data.controler.memento.TDataElementMementos;
import agscan.data.controler.memento.TGridAction;
import agscan.data.controler.memento.TGridCellAction;
import agscan.data.controler.memento.TGridState;
import agscan.data.controler.memento.TUndoEdgesFinderGlobalAlignment;
import agscan.data.controler.memento.TUndoElement;
import agscan.data.controler.memento.TUndoGridPosition;
import agscan.data.controler.memento.TUndoGridResizing;
import agscan.data.controler.memento.TUndoGridRotation;
import agscan.data.controler.memento.TUndoGridSpotResizing;
import agscan.data.controler.memento.TUndoLocalAlignmentBlock;
import agscan.data.controler.memento.TUndoLocalAlignmentGrid;
import agscan.data.controler.memento.TUndoSnifferGlobalAlignment;
import agscan.data.element.TGriddedElement;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.TGridGraphicPanel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;
import agscan.menu.action.TSaveAsAction;
import agscan.statusbar.TStatusBar;
import agscan.data.element.grid.TGrid;

public class TGridControler extends TControlerImpl {
  public static final int REFRESH_GRAPHIC = 1;
  public static final int INIT_SELECTION_WITH_DEFAULT_VALUE = 2;
  public static final int SORT_COLUMN_DESCENDING = 3;
  public static final int SORT_COLUMN_ASCENDING = 4;
  public static final int UNSORT = 5;
  public static final int GET_SPOT_INDEX = 6;
  public static final int COLORIZE = 7;
  public static final int DECOLORIZE = 8;
  public static final int INIT_SELECTION_WITH_0 = 9;
  public static final int INIT_SELECTION_WITH_1 = 10;
  public static final int INIT_SELECTION_WITH_VALUE = 11;
  public static final int GET_SELECTION_STRING = 12;
  public static final int GET_ALL_SPOTS = 13;
  public static final int GET_SELECTED_SPOTS_OR_ALL = 14;
  public static final int ADD_ACTION_MEMENTO = 15;
  public static final int PASTE = 16;
  public static final int INIT_COLUMN_WITH_FILE = 17;
  public static final int GET_SPOT = 18;
  public static final int GET_BLOCK_1 = 19;
  public static final int GET_BLOCK_2 = 20;
  public static final int GET_GRID = 21;
  public static final int ADD_MEMENTO = 24;

  /**
   * @link aggregationByValue
   * @supplierCardinality 1
   */
  private TGridStructureControler gridStructureControler = new TGridStructureControler();

  /**
   * @link aggregationByValue
   * @supplierCardinality 1
   */
  private TGridSelectionControler gridSelectionControler = new TGridSelectionControler();

  /**
   * @link aggregationByValue
   * @supplierCardinality 1
   */
  private TGridColumnsControler gridColumnsControler = new TGridColumnsControler();

  private boolean lastProcessOk;

  public TGridControler(TGriddedElement ref) {
    super(ref);
    mementos.pokeMemento(ref.createStateSnapshot());
  }
  public Object[] processEvent(TEvent event) {
    int action = event.getAction();
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    Object[] ret = null;
    TUndoElement undoElement;
    TEvent ev;

    lastProcessOk = true;
    switch (action) {
      case REFRESH_GRAPHIC:
        ((TGridGraphicPanel)reference.getView().getGraphicPanel()).repaint(gridModel.getSelectionModel(),
            (Vector)event.getParam(1));
        break;
      case INIT_SELECTION_WITH_DEFAULT_VALUE:
        initSelectionWithDefaultValue(event);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        reference.getView().getTablePanel().repaint();
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference);
        TEventHandler.handleMessage(ev);
        break;
      case SORT_COLUMN_ASCENDING:
        gridModel.sortAscending((TColumn)event.getParam(1));
        reference.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        reference.getView().getTablePanel().refresh();
        break;
      case SORT_COLUMN_DESCENDING:
        gridModel.sortDescending((TColumn)event.getParam(1));
        reference.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        reference.getView().getTablePanel().refresh();
        break;
      case UNSORT:
        gridModel.unsort();
        reference.getView().getTablePanel().initColumnSizes(false);
        reference.getView().getTablePanel().refresh();
        break;
      case GET_SPOT_INDEX:
        ret = new Object[1];
        ret[0] = new Integer(gridModel.getSpotIndex(((Integer)event.getParam(1)).intValue()));
        break;
      case COLORIZE:
        gridModel.colorizeColumn((TColumn)event.getParam(1));
        break;
      case DECOLORIZE:
        gridModel.decolorize();
        break;
      case INIT_COLUMN_WITH_FILE:
        initColumnWithFile((TColumn)event.getParam(1), (File)event.getParam(2), (TGriddedElement)reference, mementos);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case INIT_SELECTION_WITH_0:
        initSelectionWith0(event);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        reference.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        reference.getView().getTablePanel().repaint();
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case INIT_SELECTION_WITH_1:
        initSelectionWith1(event);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        reference.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        reference.getView().getTablePanel().repaint();
        break;
      case INIT_SELECTION_WITH_VALUE:
        initSelectionWithValue(event);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        reference.getView().getTablePanel().initColumnSizes((TColumn)event.getParam(1));
        reference.getView().getTablePanel().repaint();
        break;
      case PASTE:
        paste(event);
        int i = gridModel.getSelectedColumn();
        TColumn column = (TColumn)gridModel.getConfig().getColumns().getColumn(i);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        reference.getView().getTablePanel().initColumnSizes(column);
        reference.getView().getTablePanel().repaint();
        break;
      case GET_SELECTION_STRING:
        ret = new Object[1];
        ret[0] = getSelectionString((TColumn)event.getParam(1));
        break;
      case GET_ALL_SPOTS:
        ret = new Object[1];
        ret[0] = gridModel.getSpots();
        break;
      case GET_SELECTED_SPOTS_OR_ALL:
        ret = new Object[1];
        ret[0] = getSelectedSpotsOrAll();
        break;
      case UNDO:
        undoElement = peekMemento();
        undo(undoElement);
        gridModel.addModif(-1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case REDO:
        undoElement = getNextMemento();
        redo(undoElement);
        gridModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case ADD_MEMENTO:
        mementos.pokeMemento(reference.createStateSnapshot());
        gridModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case CLEAR_MODIFS:
        reference.getModel().clearModifs();
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case ADD_ACTION_MEMENTO:
        mementos.pokeMemento((TUndoElement)event.getParam(1));
        gridModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case SAVE_AS:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.IS_DATA_OPENED, event.getParam(1), (File)event.getParam(1));
        boolean b = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
        if (b)
          messageFichierOuvert(((File)event.getParam(1)).getAbsolutePath());
        else {
          b = TImageControler.okToWrite(((File)event.getParam(1)).getAbsolutePath());
          if (b) {
            save((File)event.getParam(1));
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
          }
        }
        break;
      case SAVE:
        String name = reference.getName();
        String path = reference.getPath();
        if (!path.equals("")) { //$NON-NLS-1$
          File file = new File(path + name);
          save(file);
        }
        else {
          ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.FIRE_ACTION, new Integer(TSaveAsAction.getID()));//TMenuManager.SAVE_AS avant
          TEventHandler.handleMessage(ev);
        }
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        break;
      case GET_SPOT:
        ret = new Object[1];
        ret[0] = ((TGriddedElement)reference).getGridModel().getSpot(((Double)event.getParam(1)).doubleValue(),
                                                                     ((Double)event.getParam(2)).doubleValue());
        break;
      case GET_BLOCK_1:
        ret = new Object[1];
        ret[0] = ((TGriddedElement)reference).getGridModel().getLevel1Element(((Double)event.getParam(1)).doubleValue(),
                                                                              ((Double)event.getParam(2)).doubleValue());
        break;
      case GET_BLOCK_2:
        ret = new Object[1];
        ret[0] = ((TGriddedElement)reference).getGridModel().getLevel2Element(((Double)event.getParam(1)).doubleValue(),
                                                                              ((Double)event.getParam(2)).doubleValue());
        break;
      case GET_GRID:
        ret = new Object[1];
        if ((event.getParam(0) != null) && (event.getParam(1) != null) && (event.getParam(2) != null))
          ret[0] = ((TGriddedElement)reference).getGridModel().getGrid(((Double)event.getParam(1)).doubleValue(),
                                                                       ((Double)event.getParam(2)).doubleValue());
        else
          ret[0] = gridModel.getRootBlock();
        break;
      default :
        ret = gridStructureControler.processEvent(event, mementos);
        if (!gridStructureControler.isLastProcessOk()) {
          ret = gridSelectionControler.processEvent(event);
          if (!gridSelectionControler.isLastProcessOk()) {
            ret = gridColumnsControler.processEvent(event, mementos);
            if (!gridColumnsControler.isLastProcessOk())
              lastProcessOk = false;
          }
        }
        break;
    }
    return ret;
  }
  public boolean isLastProcessOk() {
    return lastProcessOk;
  }
  private void initColumnWithFile(TColumn column, File file, TGriddedElement griddedElement, TDataElementMementos mementos) {
    TGridModel model = (TGridModel)griddedElement.getModel();
    String line = "", token; //$NON-NLS-1$
    StringTokenizer strtok;
    try {
      BufferedReader in = new BufferedReader(new FileReader(file));
      int nameIndex = -1, columnIndex = -1, i = 0;
      while ((line.equals("") || line.startsWith("#"))) line = in.readLine(); //$NON-NLS-1$ //$NON-NLS-2$
      strtok = new StringTokenizer(line, ","); //$NON-NLS-1$
      while (strtok.hasMoreTokens()) {
        token = strtok.nextToken().trim();
        if (token.equalsIgnoreCase("nom")) //$NON-NLS-1$
          nameIndex = i;
        else if (token.equalsIgnoreCase(column.toString()))
          columnIndex = i;
        i++;
      }
      if (columnIndex != -1) {
        i = 0;
        String[] value;
        Hashtable name = null;
        value = new String[model.getRowCount()];
        if (nameIndex != -1) name = new Hashtable();
        while (in.ready()) {
          line = in.readLine();
          if (!line.equals("") && !line.startsWith("#")) { //$NON-NLS-1$ //$NON-NLS-2$
            value[i] = line.split(",")[columnIndex].trim(); //$NON-NLS-1$
            if (nameIndex != -1) name.put(line.split(",")[nameIndex].trim(), new Integer(i)); //$NON-NLS-1$
            i++;
          }
        }
        in.close();
        if (i == model.getRowCount()) {
          if (column.isCompatible(value)) {
            int[] ix = new int[2];
            ix[0] = column.getModelIndex();
            TColumnState[] cs = new TColumnState[2];
            cs[0] = column.createSnapshot();
            StringSelection[] ss = new StringSelection[2];
            ss[0] = new StringSelection(gridColumnsControler.getColumnValues(column, griddedElement));
            Object[] objectValues = column.getCompatibleValuesInBuffer(value);
            if (nameIndex != -1) {
              Vector v = model.getSpots();
              TSpot spot;
              for (i = 0; i < v.size(); i++) {
                spot = (TSpot)v.elementAt(i);
                spot.addParameter(column.getSpotKey(), objectValues[((Integer)name.get(spot.getName())).intValue()], true);
              }
            }
            else {
              Vector v = model.getSpots();
              TSpot spot;
              for (i = 0; i < v.size(); i++) {
                spot = (TSpot)v.elementAt(i);
                spot.addParameter(column.getSpotKey(), objectValues[model.getSpotRow(i)], true);
              }
            }
            ix[1] = column.getModelIndex();
            cs[1] = column.createSnapshot();
            ss[1] = new StringSelection(gridColumnsControler.getColumnValues(column, griddedElement));
          //  mementos.pokeMemento(new TGridAction(false, false, false, true, false, false, ix, cs, ss, file, null));//remplacement integration 14/09/05
            mementos.pokeMemento(new TGridAction(false, false, false, true, false, false, false, false, ix, cs, ss, file, null));
            model.addModif(1);
            TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, griddedElement));
          }
          else {
            JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.10"), Messages.getString("TGridControler.11"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
          }
        }
        else {
          JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.12"), Messages.getString("TGridControler.13"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
      else {
        JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.14") + column.toString() + //$NON-NLS-1$
                                      Messages.getString("TGridControler.15") + //$NON-NLS-1$
                                      file.getName() + Messages.getString("TGridControler.16"), Messages.getString("TGridControler.17"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace();
    }
    catch (IOException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.18"), Messages.getString("TGridControler.19"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
    }
    catch(ArrayIndexOutOfBoundsException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.20") + line + Messages.getString("TGridControler.21"), Messages.getString("TGridControler.22"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }
    catch (NullPointerException ex) {
      JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.23") + file.getName() + //$NON-NLS-1$
                                    Messages.getString("TGridControler.24"), Messages.getString("TGridControler.25"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }
  private Object[] getSelectedSpotsOrAll() {
    TGriddedElement grid = (TGriddedElement)reference;
    final TGridModel model = grid.getGridModel();
    Vector spots;
    if (grid.getGridModel().getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = model.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = model.getSpots();
    TreeSet tm = new TreeSet(new Comparator() {
      public int compare(Object o1, Object o2) {
        TSpot spot1 = (TSpot)o1;
        TSpot spot2 = (TSpot)o2;
        int row1 = model.getSpotRow(spot1.getIndex());
        int row2 = model.getSpotRow(spot2.getIndex());
        if (row1 > row2)
          return 1;
        else if (row1 < row2)
          return -1;
        return 0;
      }
    });
    tm.addAll(spots);
    return tm.toArray();
  }
  private String getSelectionString(TColumn column) {
    Object[] spots = getSelectedSpotsOrAll();
    StringBuffer s = new StringBuffer(spots.length * 10);
    for (int i = 0; i < spots.length; i++)
      s.append(((TSpot)spots[i]).getParameterString(column.getSpotKey()) + "\n"); //$NON-NLS-1$
    return s.toString();
  }
  public void undo(TUndoElement undoElement) {
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, reference, Messages.getString("TGridControler.1"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    TGriddedElement grid = (TGriddedElement)reference;
    TGridConfig conf = grid.getGridModel().getConfig();
    TColumnState cs;
    TColumn column;
    Vector v;
    Hashtable hash;
    int r, c, l;
    TGridElement elem;
    double x, y, d, a;
    String[] tab;
    Object[] values, spots;
    int[] sel;
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    if (undoElement instanceof TGridAction) {
      TGridAction ga = (TGridAction)undoElement;
      if (ga.getAddColumn())
        gridColumnsControler.removeColumn((TColumn)conf.getColumns().getColumn(ga.getColumnIndex(0)), grid);
      else if (ga.getRemoveColumn()) {
        cs = ga.getColumnState(0);
        column = gridColumnsControler.addColumn(cs.getIndex(), cs.getType(), cs.getParams(), grid);
        tab = null;
        Transferable text = ga.getColumnContents(0);
        try {
          String buffer = (String)text.getTransferData(DataFlavor.stringFlavor);
          tab = buffer.split("\n"); //$NON-NLS-1$
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        values = column.getCompatibleValuesInBuffer(tab);
        spots = grid.getGridModel().getSpots().toArray();
        for (int k = 0; k < spots.length; k++)
          ((TSpot)spots[k]).addParameter(column.getSpotKey(), values[k], true);
        grid.getView().getTablePanel().initColumnSizes(column);
      }
      else if (((TGridAction)undoElement).getImportColumns()) {
        for (int i = 0; i < ga.getNbColumns(); i++)
          if (ga.getColumnIndex(i) != -1)
            gridColumnsControler.removeColumn( (TColumn) conf.getColumns().getColumn(conf.getColumns().getColumnCount() - 1), grid);
      }
      else if (((TGridAction)undoElement).getInitColumnWithFile()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0));
        try {
          tab = ((String)ga.getColumnContents(0).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
          values = column.getCompatibleValuesInBuffer(tab);
          spots = grid.getGridModel().getSpots().toArray();
          for (int k = 0; k < spots.length; k++)
            ((TSpot)spots[k]).addParameter(column.getSpotKey(), values[k], true);
          grid.getView().getTablePanel().initColumnSizes(column);
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      else if (((TGridAction)undoElement).getInitColumnWithDefault()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0));
        try {
          tab = ((String)ga.getColumnContents(0).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
          values = column.getCompatibleValuesInBuffer(tab);
          if (ga.getSelection() == null) {
            spots = grid.getGridModel().getSpots().toArray();
            for (int k = 0; k < spots.length; k++)
              ((TSpot)spots[k]).addParameter(column.getSpotKey(), values[k], true);
          }
          else {
            sel = ga.getSelection();
            for (int k = 0; k < sel.length; k++)
              grid.getGridModel().getSpot(sel[k]).addParameter(column.getSpotKey(), values[k], true);
          }
          grid.getView().getTablePanel().initColumnSizes(column);
          grid.getView().getTablePanel().repaint();
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      else if (((TGridAction)undoElement).getInitColumnWithValue()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0));
        try {
          tab = ((String)ga.getColumnContents(0).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
          values = column.getCompatibleValuesInBuffer(tab);
          if (ga.getSelection() == null) {
            grid.getGridModel().addParameter(column.getSpotKey(), values[0], true);
          }
          else {
            sel = ga.getSelection();
            for (int k = 0; k < sel.length; k++)
              grid.getGridModel().getSpot(sel[k]).addParameter(column.getSpotKey(), values[k], true);
          }
          grid.getView().getTablePanel().initColumnSizes(column);
          //grid.getView().getTablePanel().repaint();
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    else if (undoElement instanceof TGridState) {
      TGridState gs = (TGridState)mementos.getPreviousStateMemento();
      conf.setNbLevels(gs.getNbLevels());
      conf.setLevel1Params(gs.getLev1NbRows(), gs.getLev1NbCols(), gs.getLev1ColWidth(), gs.getLev1RowHeight(), gs.getLev1Diameter(), gs.getLev1Start(), gs.getLev1SpotRC());
      conf.setLevel2Params(gs.getLev2NbRows(), gs.getLev2NbCols(), gs.getLev2ColSpacing(), gs.getLev2RowSpacing(),
                           gs.getLev2Start(), gs.getLev2BlockRC());
   //   conf.setLevel3Params(gs.getLev3NbRows(), gs.getLev3NbCols(), gs.getLev3ColSpacing(), gs.getLev3RowSpacing(),
     //                      gs.getLev3Start(), gs.getLev3BlockRC());//  level3 removed - 20005/10/28
      grid.getGridModel().update();
      grid.getView().getGraphicPanel().revalidate();
      grid.getView().getViewPanel().repaint();
      reference.getView().refresh();
      grid.getView().getTablePanel().initColumnSizes((TColumn)grid.getGridModel().getConfig().getColumns().getColumn(1));
    }
    else if (undoElement instanceof TGridCellAction) {
      TGridCellAction gca = (TGridCellAction)undoElement;
      column = (TColumn)grid.getGridModel().getConfig().getColumns().getColumn(gca.getColumnIndex());
      grid.getGridModel().getSpot(gca.getRow()).addParameter(column.getSpotKey(), gca.getPreviousValue(), true);
      grid.getView().getTablePanel().repaint();
    }
    else if (undoElement instanceof TColumnModifyAction) {
      TColumnModifyAction cma = (TColumnModifyAction)undoElement;
      cs = cma.getPreviousState();
      column = (TColumn)grid.getGridModel().getConfig().getColumns().getColumn(cs.getIndex());
      gridColumnsControler.refreshColumn((TColumn)conf.getColumns().getColumn(column.getModelIndex()), cs.getParams(), grid);
    }
    else if (undoElement instanceof TUndoGridRotation) {
      v = ((TUndoGridRotation)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        a = ((Double)hash.get("A0")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setAngle(a);
      }
    }
    else if (undoElement instanceof TUndoGridResizing) {
      v = ((TUndoGridResizing)undoElement).getList();
      double w0, h0, w1, h1;
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get("X0")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y0")).doubleValue(); //$NON-NLS-1$
        w0 = ((Double)hash.get("W0")).doubleValue(); //$NON-NLS-1$
        h0 = ((Double)hash.get("H0")).doubleValue(); //$NON-NLS-1$
        w1 = ((Double)hash.get("W1")).doubleValue(); //$NON-NLS-1$
        h1 = ((Double)hash.get("H1")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) {
          elem.resizeByRap(w0 / w1, h0 / h1, false);
          elem.setPosition(x, y);
        }
      }
    }
    else if (undoElement instanceof TUndoGridPosition) {
      v = ((TUndoGridPosition)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get("X0")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y0")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setPosition(x, y);
      }
    }
    else if ((undoElement instanceof TUndoLocalAlignmentBlock) || (undoElement instanceof TUndoLocalAlignmentGrid)) {
      if (undoElement instanceof TUndoLocalAlignmentBlock)
        v = ((TUndoLocalAlignmentBlock)undoElement).getList();
      else
        v = ((TUndoLocalAlignmentGrid)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get("X0")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y0")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setPosition(x, y);
      }
    }
    else if ((undoElement instanceof TUndoEdgesFinderGlobalAlignment) || (undoElement instanceof TUndoSnifferGlobalAlignment)) {
      v = null;
      if (undoElement instanceof TUndoEdgesFinderGlobalAlignment)
        v = ((TUndoEdgesFinderGlobalAlignment)undoElement).getList();
      else
        v = ((TUndoSnifferGlobalAlignment)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        x = ((Double)hash.get("X0")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y0")).doubleValue(); //$NON-NLS-1$
        a = ((Double)hash.get("A0")).doubleValue(); //$NON-NLS-1$
        elem = grid.getGridModel().getRootBlock();
        if (elem != null) {
          elem.setPosition(x, y);
          elem.setAngle(a);
        }
      }
    }
    else if (undoElement instanceof TUndoGridSpotResizing) {
      v = ((TUndoGridSpotResizing)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        d = ((Double)hash.get("D0")).doubleValue(); //$NON-NLS-1$
        elem = grid.getGridModel().getSpot(r, c);
        if (elem != null) ((TSpot)elem).setUserDiameter(d);
      }
    }
    grid.getView().getTablePanel().repaint();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, reference, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  protected void redo(TUndoElement undoElement) {
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, reference, Messages.getString("TGridControler.0"), Color.red); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    TGriddedElement grid = (TGriddedElement)reference;
    TGridConfig conf = grid.getGridModel().getConfig();
    TColumnState cs;
    TColumn column;
    Vector v;
    Hashtable hash;
    int r, c, l;
    TGridElement elem;
    double x, y, d, a;
    String[] tab;
    Object[] values, spots;
    int[] sel;

    if (undoElement instanceof TGridAction){
      TGridAction ga = (TGridAction)undoElement;
      if (ga.getAddColumn()) {
        cs = ga.getColumnState(0);
        gridColumnsControler.addColumn(cs.getIndex(), cs.getType(), cs.getParams(), grid);
      }
      else if (ga.getRemoveColumn())
        gridColumnsControler.removeColumn((TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0)), grid);
      else if (ga.getImportColumns()) {
        for (int i = 0; i < ga.getNbColumns(); i++)
          if (ga.getColumnIndex(i) != -1) {
            cs = ga.getColumnState(i);
            column = gridColumnsControler.addColumn(cs.getType(), cs.getParams(), grid);
            try {
              tab = ((String)ga.getColumnContents(i).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
              values = column.getCompatibleValuesInBuffer(tab);
              spots = grid.getGridModel().getSpots().toArray();
              for (int k = 0; k < spots.length; k++)
                ((TSpot)spots[k]).addParameter(column.getSpotKey(), values[k], true);
              grid.getView().getTablePanel().initColumnSizes(column);
            }
            catch (Exception ex) {
              ex.printStackTrace();
            }
          }
      }
      else if (((TGridAction)undoElement).getInitColumnWithFile()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(1));
        try {
          tab = ((String)ga.getColumnContents(1).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
          values = column.getCompatibleValuesInBuffer(tab);
          spots = grid.getGridModel().getSpots().toArray();
          for (int k = 0; k < spots.length; k++)
            ((TSpot)spots[k]).addParameter(column.getSpotKey(), values[k], true);
          grid.getView().getTablePanel().initColumnSizes(column);
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      else if (((TGridAction)undoElement).getInitColumnWithDefault()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0));
        if (ga.getSelection() == null)
          grid.getGridModel().addParameter(column.getSpotKey(), column.getDefaultValue(), true);
        else {
          sel = ga.getSelection();
          for (int k = 0; k < sel.length; k++)
            grid.getGridModel().getSpot(sel[k]).addParameter(column.getSpotKey(), column.getDefaultValue(), true);
        }
        grid.getView().getTablePanel().initColumnSizes(column);
      }
      else if (((TGridAction)undoElement).getInitColumnWithValue()) {
        column = (TColumn)conf.getColumns().getColumn(((TGridAction)undoElement).getColumnIndex(0));
        try {
          tab = ((String)ga.getColumnContents(1).getTransferData(DataFlavor.stringFlavor)).split("\n"); //$NON-NLS-1$
          values = column.getCompatibleValuesInBuffer(tab);
          if (ga.getSelection() == null)
            grid.getGridModel().addParameter(column.getSpotKey(), values[0], true);
          else {
            sel = ga.getSelection();
            for (int k = 0; k < sel.length; k++)
              grid.getGridModel().getSpot(sel[k]).addParameter(column.getSpotKey(), values[k], true);
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        grid.getView().getTablePanel().initColumnSizes(column);
      }
    }
    else if (undoElement instanceof TGridState) {
      TGridState gs = (TGridState)undoElement;
      conf.setNbLevels(gs.getNbLevels());
      conf.setLevel1Params(gs.getLev1NbRows(), gs.getLev1NbCols(), gs.getLev1ColWidth(), gs.getLev1RowHeight(), gs.getLev1Diameter(), gs.getLev1Start(), gs.getLev1SpotRC());
      conf.setLevel2Params(gs.getLev2NbRows(), gs.getLev2NbCols(), gs.getLev2ColSpacing(), gs.getLev2RowSpacing(),
                           gs.getLev2Start(), gs.getLev2BlockRC());
    //  conf.setLevel3Params(gs.getLev3NbRows(), gs.getLev3NbCols(), gs.getLev3ColSpacing(), gs.getLev3RowSpacing(),
      //                     gs.getLev3Start(), gs.getLev3BlockRC());//  level3 removed - 20005/10/28
      grid.getGridModel().update();
      grid.getView().getGraphicPanel().revalidate();
      grid.getView().getViewPanel().repaint();
      reference.getView().refresh();
      grid.getView().getTablePanel().initColumnSizes((TColumn)grid.getGridModel().getConfig().getColumns().getColumn(1));
    }
    else if (undoElement instanceof TGridCellAction) {
      TGridCellAction gca = (TGridCellAction)undoElement;
      TColumn col = (TColumn)grid.getGridModel().getConfig().getColumns().getColumn(gca.getColumnIndex());
      grid.getGridModel().getSpot(gca.getRow()).addParameter(col.getSpotKey(), gca.getValue(), true);
    }
    else if (undoElement instanceof TColumnModifyAction) {
      TColumnModifyAction cma = (TColumnModifyAction)undoElement;
      cs = cma.getState();
      column = (TColumn)grid.getGridModel().getConfig().getColumns().getColumn(cs.getIndex());
      gridColumnsControler.refreshColumn((TColumn)conf.getColumns().getColumn(column.getModelIndex()), cs.getParams(), grid);
    }
    else if (undoElement instanceof TUndoGridRotation) {
      v = ((TUndoGridRotation)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        a = ((Double)hash.get("A1")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setAngle(a);
      }
    }
    else if (undoElement instanceof TUndoGridResizing) {
      v = ((TUndoGridResizing)undoElement).getList();
      double w0, h0, w1, h1;
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get(Messages.getString("TGridControler.73"))).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y1")).doubleValue(); //$NON-NLS-1$
        w0 = ((Double)hash.get("W0")).doubleValue(); //$NON-NLS-1$
        h0 = ((Double)hash.get("H0")).doubleValue(); //$NON-NLS-1$
        w1 = ((Double)hash.get("W1")).doubleValue(); //$NON-NLS-1$
        h1 = ((Double)hash.get("H1")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) {
          elem.resizeByRap(w1 / w0, h1 / h0, false);
          elem.setPosition(x, y);
        }
      }
    }
    else if (undoElement instanceof TUndoGridPosition) {
      v = ((TUndoGridPosition)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get("X1")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y1")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setPosition(x, y);
      }
    }
    else if ((undoElement instanceof TUndoLocalAlignmentBlock) || (undoElement instanceof TUndoLocalAlignmentGrid)){
      if (undoElement instanceof TUndoLocalAlignmentBlock)
        v = ((TUndoLocalAlignmentBlock)undoElement).getList();
      else
        v = ((TUndoLocalAlignmentGrid)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        l = ((Integer)hash.get("L")).intValue(); //$NON-NLS-1$
        x = ((Double)hash.get("X1")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y1")).doubleValue(); //$NON-NLS-1$
        elem = null;
        switch (l) {
          case 0:
            elem = grid.getGridModel().getSpot(r, c);
            break;
          case 1:
            elem = grid.getGridModel().getLevel1Element(r, c);
            break;
          case 2:
            elem = grid.getGridModel().getLevel2Element(r, c);
            break;
          case 3:
            elem = grid.getGridModel().getRootBlock();
            break;
        }
        if (elem != null) elem.setPosition(x, y);
      }
    }
    else if ((undoElement instanceof TUndoEdgesFinderGlobalAlignment) || (undoElement instanceof TUndoSnifferGlobalAlignment)){
      v = null;
      if (undoElement instanceof TUndoEdgesFinderGlobalAlignment)
        v = ((TUndoEdgesFinderGlobalAlignment)undoElement).getList();
      else
        v = ((TUndoSnifferGlobalAlignment)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        x = ((Double)hash.get("X1")).doubleValue(); //$NON-NLS-1$
        y = ((Double)hash.get("Y1")).doubleValue(); //$NON-NLS-1$
        a = ((Double)hash.get("A1")).doubleValue(); //$NON-NLS-1$
        elem = grid.getGridModel().getRootBlock();
        if (elem != null) {
          elem.setPosition(x, y);
          elem.setAngle(a);
        }
      }
    }
    else if (undoElement instanceof TUndoGridSpotResizing) {
      v = ((TUndoGridSpotResizing)undoElement).getList();
      for (Enumeration enume = v.elements(); enume.hasMoreElements(); ) {
        hash = (Hashtable)enume.nextElement();
        r = ((Integer)hash.get("R")).intValue(); //$NON-NLS-1$
        c = ((Integer)hash.get("C")).intValue(); //$NON-NLS-1$
        d = ((Double)hash.get("D1")).doubleValue(); //$NON-NLS-1$
        elem = grid.getGridModel().getSpot(r, c);
        if (elem != null) ((TSpot)elem).setUserDiameter(d);
      }
    }
    grid.getView().getTablePanel().repaint();
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GRID_ACTION_LABEL, reference, "", Color.blue); //$NON-NLS-1$
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
    agscan.FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void save(File file) {
    ((TGrid)reference).getGridModel().save(file);
    String pathSeparator = System.getProperty("file.separator"); //$NON-NLS-1$
    String p = pathSeparator;
    String filename = file.getAbsolutePath();
    int ix = filename.lastIndexOf(pathSeparator);
    if (ix > -1) p = filename.substring(0, ix + 1);
    ((TGrid)reference).setPath(p);
    ((TGrid)reference).setName(filename.substring(ix + 1));
    TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME, ((TGrid)reference).getName(), ((TGrid)reference).getPath());
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, null);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
  }
  private void messageFichierOuvert(String filename) {
  JOptionPane.showMessageDialog(null, Messages.getString("TGridControler.97") + filename + Messages.getString("TGridControler.98"), //$NON-NLS-1$ //$NON-NLS-2$
                                Messages.getString("TGridControler.2"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
  }
  public TGridSelectionControler getGridSelectionControler() {
    return gridSelectionControler;
  }
  public TGridColumnsControler getGridColumnsControler() {
    return gridColumnsControler;
  }
  private void initSelectionWithDefaultValue(TEvent event) {
    TColumn column = (TColumn)event.getParam(1);
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    column.getCellEditor().cancelCellEditing();
    Vector spots;
    if (gridModel.getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = gridModel.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = gridModel.getSpots();
    int[] ix = new int[2];
    TColumnState[] cs = new TColumnState[2];
    StringSelection[] ss = new StringSelection[2];
    ix[0] = column.getModelIndex();
    cs[0] = column.createSnapshot();
    ss[0] = new StringSelection(getSelectionString(column));
    int[] sel = new int[spots.size()];
    for (int i = 0; i < spots.size(); i++) sel[i] = ((TSpot)spots.elementAt(i)).getIndex();
    for (Enumeration enume = spots.elements(); enume.hasMoreElements(); )
      ((TSpot)enume.nextElement()).addParameter(column.getSpotKey(),
          column.getDefaultValue(), true);
    ix[1] = column.getModelIndex();
    cs[1] = column.createSnapshot();
    ss[1] = new StringSelection(getSelectionString(column));
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
                              new TGridAction(false, false, false, false, true, false, false, false, ix, cs, ss, null, sel));
    //new TGridAction(false, false, false, false, true, false, ix, cs, ss, null, sel)); remplacé integration 14/09/05
    TEventHandler.handleMessage(ev);
  }
  public void initSelectionWith0(TEvent event) {
    TColumn column = (TColumn)event.getParam(1);
    Vector spots;
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    column.getCellEditor().cancelCellEditing();
    if (gridModel.getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = gridModel.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = gridModel.getSpots();
    int[] ix = new int[2];
    TColumnState[] cs = new TColumnState[2];
    StringSelection[] ss = new StringSelection[2];
    ix[0] = column.getModelIndex();
    cs[0] = column.createSnapshot();
    ss[0] = new StringSelection(getSelectionString(column));
    int[] sel = new int[spots.size()];
    for (int i = 0; i < spots.size(); i++) sel[i] = ((TSpot)spots.elementAt(i)).getIndex();
    for (Enumeration enume = spots.elements(); enume.hasMoreElements(); )
      ((TSpot)enume.nextElement()).addParameter(column.getSpotKey(), new Boolean(false), true);
    ix[1] = column.getModelIndex();
    cs[1] = column.createSnapshot();
    ss[1] = new StringSelection(getSelectionString(column));
    //remplacé integration 14/09/05
  //  TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
    //                          new TGridAction(false, false, false, false, false, true, ix, cs, ss, null, sel));
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
            new TGridAction(false, false, false, false, false, true, false, false, ix, cs, ss, null, sel));
    TEventHandler.handleMessage(ev);
  }
  public void initSelectionWith1(TEvent event) {
    TColumn column = (TColumn)event.getParam(1);
    Vector spots;
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    column.getCellEditor().cancelCellEditing();
    if (gridModel.getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = gridModel.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = gridModel.getSpots();
    int[] ix = new int[2];
    TColumnState[] cs = new TColumnState[2];
    StringSelection[] ss = new StringSelection[2];
    ix[0] = column.getModelIndex();
    cs[0] = column.createSnapshot();
    ss[0] = new StringSelection(getSelectionString(column));
    int[] sel = new int[spots.size()];
    for (int i = 0; i < spots.size(); i++) sel[i] = ((TSpot)spots.elementAt(i)).getIndex();
    for (Enumeration enume = spots.elements(); enume.hasMoreElements(); )
      ((TSpot)enume.nextElement()).addParameter(column.getSpotKey(), new Boolean(true), true);
    ix[1] = column.getModelIndex();
    cs[1] = column.createSnapshot();
    ss[1] = new StringSelection(getSelectionString(column));
    //remplacé integration 14/09/05
       // TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
     //                         new TGridAction(false, false, false, false, false, true, ix, cs, ss, null, sel));
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
            new TGridAction(false, false, false, false, false, true, false, false, ix, cs, ss, null, sel));
    TEventHandler.handleMessage(ev);
  }
  public void initSelectionWithValue(TEvent event) {
    TColumn column = (TColumn)event.getParam(1);
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    Vector spots;
    column.getCellEditor().cancelCellEditing();
    if (gridModel.getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = gridModel.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = gridModel.getSpots();
    int[] ix = new int[2];
    TColumnState[] cs = new TColumnState[2];
    StringSelection[] ss = new StringSelection[2];
    ix[0] = column.getModelIndex();
    cs[0] = column.createSnapshot();
    ss[0] = new StringSelection(getSelectionString(column));
    int[] sel = new int[spots.size()];
    for (int i = 0; i < spots.size(); i++) sel[i] = ((TSpot)spots.elementAt(i)).getIndex();
    for (Enumeration enume = spots.elements(); enume.hasMoreElements(); )
      ((TSpot)enume.nextElement()).addParameter(column.getSpotKey(), event.getParam(2), true);
    ix[1] = column.getModelIndex();
    cs[1] = column.createSnapshot();
    ss[1] = new StringSelection(getSelectionString(column));
   // remplacé integration 14/09/05
    //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
     //                         new TGridAction(false, false, false, false, false, true, ix, cs, ss, null, sel));
    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
            new TGridAction(false, false, false, false, false, true, false, false, ix, cs, ss, null, sel));
    TEventHandler.handleMessage(ev);
  }
  public void paste(TEvent event) {
    TGridModel gridModel = ((TGriddedElement)reference).getGridModel();
    Vector spots;
    Object[] sp = getSelectedSpotsOrAll();
    int i = gridModel.getSelectedColumn();
    TColumn column = (TColumn)gridModel.getConfig().getColumns().getColumn(i);
    Transferable text = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this);
    if (gridModel.getSelectionModel().getRecursivelySelectedSpots().size() > 0)
      spots = gridModel.getSelectionModel().getRecursivelySelectedSpots();
    else
      spots = gridModel.getSpots();
    int[] ix = new int[2];
    TColumnState[] cs = new TColumnState[2];
    StringSelection[] ss = new StringSelection[2];
    ix[0] = column.getModelIndex();
    cs[0] = column.createSnapshot();
    ss[0] = new StringSelection(getSelectionString(column));
    int[] sel = new int[spots.size()];
    for (int k = 0; k < spots.size(); k++) sel[k] = ((TSpot)spots.elementAt(k)).getIndex();
    try {
      String buffer = (String)text.getTransferData(DataFlavor.stringFlavor);
      String[] tab = buffer.split("\n"); //$NON-NLS-1$
      Object[] values = column.getCompatibleValuesInBuffer(tab);
      for (int k = 0; k < sp.length; k++) ((TSpot)sp[k]).addParameter(column.getSpotKey(), values[k], true);
      ix[1] = column.getModelIndex();
      cs[1] = column.createSnapshot();
      ss[1] = new StringSelection(getSelectionString(column));
      // remplacé integration 14/09/05
      //  TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
      //                        new TGridAction(false, false, false, false, false, true, ix, cs, ss, null, sel));
      TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TGridControler.ADD_ACTION_MEMENTO, event.getParam(0),
            new TGridAction(false, false, false, false, false, true, false, false, ix, cs, ss, null, sel));
      TEventHandler.handleMessage(ev);
      TEventHandler.handleMessage(new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
