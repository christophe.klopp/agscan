package agscan.data.controler.memento;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import agscan.TEventHandler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;

/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

public class TUndoGridSpotResizing extends TUndoState {
  private Vector list;
  private File tmpFile;

  public TUndoGridSpotResizing() {
    super();
    Hashtable elementHashtable;
    /*try {
      tmpFile = File.createTempFile("bzscanundo", "");
      tmpFile.deleteOnExit();
    }
    catch (Exception ex) {
      tmpFile = null;
      ex.printStackTrace();
    }*/
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, null);
    Vector selection = (Vector)TEventHandler.handleMessage(event)[0];
    list = new Vector();
    TSpot spot;
    for (Enumeration enume = selection.elements(); enume.hasMoreElements(); ) {
      spot = (TSpot)enume.nextElement();
      elementHashtable = new Hashtable();
      elementHashtable.put("R", new Integer(spot.getRow()));
      elementHashtable.put("C", new Integer(spot.getCol()));
      elementHashtable.put("D0", new Double(spot.getUserDiameter()));
      list.addElement(elementHashtable);
    }
  }
  public String toString() {
    return " ";
  }
  public void addFinalState() {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_ALL_SELECTED_SPOTS, null);
    Vector selection = (Vector)TEventHandler.handleMessage(event)[0];
    TSpot spot;
    Hashtable elementHashtable = null;
    int i = 0;
    for (Enumeration enume = selection.elements(); enume.hasMoreElements(); ) {
      spot = (TSpot)enume.nextElement();
      elementHashtable = (Hashtable)list.elementAt(i++);
      elementHashtable.put("D1", new Double(spot.getUserDiameter()));
    }
    /*try {
      XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(tmpFile)));
      enc.writeObject(list);
      enc.close();
      list = null;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }*/
  }
  public Vector getList() {
    /*Vector list = null;
    try {
      XMLDecoder enc = new XMLDecoder(new BufferedInputStream(new FileInputStream(tmpFile)));
      list = (Vector)enc.readObject();
      enc.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }*/
    return list;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
