/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler.memento;

import java.awt.Color;
import java.util.Vector;

import agscan.data.model.grid.table.TBooleanColumn;
import agscan.data.model.grid.table.TColumn;
import agscan.data.model.grid.table.TDoubleColumn;
import agscan.data.model.grid.table.TDoubleListColumn;
import agscan.data.model.grid.table.TIntegerColumn;
import agscan.data.model.grid.table.TIntegerListColumn;
import agscan.data.model.grid.table.TProbaColumn;
import agscan.data.model.grid.table.TStringColumn;
import agscan.data.model.grid.table.TStringListColumn;
import agscan.data.model.grid.table.TURLColumn;

public class TColumnState {
  private String name, spotKey;
  private int index, type;
  private Object defaultValue;
  private Vector values;
  private Color infColor, betweenColor, supColor;
  private boolean sup, between, inf, visible, editable, removable, synchrone;
  private Object infValue, betweenValue1, betweenValue2, supValue;

  public TColumnState(int i, String name, int type, Object defVal, Vector values, boolean inf, boolean between, boolean sup,
                      Color infCol, Color betweenCol, Color supCol, Object infVal, Object betweenVal1, Object betweenVal2,
                      Object supVal, boolean vis, boolean edit, boolean rem, String sk, boolean syn) {
    index = i;
    this.name = name;
    this.type = type;
    defaultValue = defVal;
    this.values = values;
    this.inf = inf;
    this.between = between;
    this.sup = sup;
    infColor = infCol;
    betweenColor = betweenCol;
    supColor = supCol;
    infValue = infVal;
    betweenValue1 = betweenVal1;
    betweenValue2 = betweenVal2;
    supValue = supVal;
    visible = vis;
    editable = edit;
    removable = rem;
    spotKey = sk;
    synchrone = syn;
  }
  public String getName() { return name; }
  public int getIndex() { return index; }
  public int getType() { return type; }
  public Object getDefaultValue() { return defaultValue; }
  public Vector getValues() { return values; }
  public boolean getInf() { return inf; }
  public boolean getBetween() { return between; }
  public boolean getSup() { return sup; }
  public Color getInfColor() { return infColor; }
  public Color getBetweenColor() { return betweenColor; }
  public Color getSupColor() { return supColor; }
  public Object getInfValue() { return infValue; }
  public Object getBetweenValue1() { return betweenValue1; }
  public Object getBetweenValue2() { return betweenValue2; }
  public Object getSupValue() { return supValue; }
  public boolean getVisible() { return visible; }
  public boolean getEditable() { return editable; }
  public boolean getRemovable() { return removable; }
  public String getSpotKey() { return spotKey; }
  public boolean getSynchrone() { return synchrone; }
  public Vector getParams() {
    Vector v = null;
    switch (type) {
      case TColumn.TYPE_BOOLEAN:
        v = TBooleanColumn.getParams(this);
        break;
      case TColumn.TYPE_INTEGER:
        v = TIntegerColumn.getParams(this);
        break;
      case TColumn.TYPE_INTEGER_LIST:
        v = TIntegerListColumn.getParams(this);
        break;
      case TColumn.TYPE_PROBA:
        v = TProbaColumn.getParams(this);
        break;
      case TColumn.TYPE_REAL:
        v = TDoubleColumn.getParams(this);
        break;
      case TColumn.TYPE_REAL_LIST:
        v = TDoubleListColumn.getParams(this);
        break;
      case TColumn.TYPE_TEXT:
        v = TStringColumn.getParams(this);
        break;
      case TColumn.TYPE_TEXT_LIST:
        v = TStringListColumn.getParams(this);
        break;
      case TColumn.TYPE_URL:
        v = TURLColumn.getParams(this);
        break;
    }
    return v;
  }
  public String toString() { return ""; }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
