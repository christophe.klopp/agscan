/**
 * <p>Titre : BZScan</p> <p>Description : DNA chips scan analysis</p>
 * <p>Copyright : Copyright (c) 2003</p> <p>Soci�t� : TAGC</p>
 * @author Fabrice Lopez
 * @version 2.0
 */

package agscan.data.controler.memento;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import agscan.TEventHandler;
import agscan.data.controler.TGridSelectionControler;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridElement;
import agscan.data.model.grid.TSpot;
import agscan.event.TEvent;

public class TUndoGridRotation extends TUndoState {
  private File tmpFile;
  private Vector list;

  public TUndoGridRotation() {
    super();
    Hashtable elementHashtable;
    /*try {
      tmpFile = File.createTempFile("bzscanundo", "");
      tmpFile.deleteOnExit();
    }
    catch (Exception ex) {
      tmpFile = null;
      ex.printStackTrace();
    }*/
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, null);
    Vector selection = (Vector)TEventHandler.handleMessage(event)[0];
    list = new Vector();
    TGridElement element;
    int level = 0;
    for (Enumeration enume = selection.elements(); enume.hasMoreElements(); ) {
      element = (TGridElement)enume.nextElement();
      if (element.getParent() == null)
        level = 3;
      else if (! (element instanceof TSpot)) {
        if (((TGridBlock)element).getFirstElement() instanceof TSpot)
          level = 1;
        else
          level = 2;
      }
      elementHashtable = new Hashtable();
      elementHashtable.put("L", new Integer(level));
      elementHashtable.put("R", new Integer(element.getRow()));
      elementHashtable.put("C", new Integer(element.getCol()));
      elementHashtable.put("A0", new Double(element.getAngle()));
      list.addElement(elementHashtable);
    }
  }
  public void addFinalPosition() {
    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.GET_BLOCK_SELECTION, null);
    Vector selection = (Vector)TEventHandler.handleMessage(event)[0];
    TGridElement element;
    Hashtable elementHashtable = null;
    int i = 0;
    for (Enumeration enume = selection.elements(); enume.hasMoreElements(); ) {
      element = (TGridElement)enume.nextElement();
      elementHashtable = (Hashtable)list.elementAt(i++);
      elementHashtable.put("A1", new Double(element.getAngle()));
    }
    /*try {
      XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(tmpFile)));
      enc.writeObject(list);
      enc.close();
      list = null;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }*/
  }
  public String toString() {
    return "ROTATION";
  }
  public Vector getList() {
    /*Vector list = null;
    try {
      XMLDecoder enc = new XMLDecoder(new BufferedInputStream(new FileInputStream(tmpFile)));
      list = (Vector)enc.readObject();
      enc.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }*/
    return list;
  }
}
/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
