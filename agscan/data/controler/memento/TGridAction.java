/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler.memento;

import java.awt.datatransfer.StringSelection;
import java.io.File;

public class TGridAction extends TUndoAction {
  private boolean addColumn, removeColumn, importColumns, initColumnWithFile, initColumnWithDefault, initColumnWithValue, addFilledColumn,
      initSelectionWithValues;
  private int[] columnIndex, selection;
  private TColumnState[] columnState;
  private StringSelection[] columnContents;
  private File file;
  public TGridAction(boolean addCol, boolean removeCol, boolean importCol, boolean initColWithFile, boolean initColWithDef,
                     boolean initColWithVal, boolean addFilledCol, boolean initSelWithVal, int[] colIx, TColumnState[] cs,
                     StringSelection[] columnCont, File file, int[] sel) {
    addColumn = addCol;
    removeColumn = removeCol;
    columnIndex = colIx;
    columnState = cs;
    columnContents = columnCont;
    importColumns = importCol;
    this.file = file;
    initColumnWithFile = initColWithFile;
    initColumnWithDefault = initColWithDef;
    initColumnWithValue = initColWithVal;
    addFilledColumn = addFilledCol;
    initSelectionWithValues = initSelWithVal;
    selection = sel;
  }
  public String toString() { return ""; }
  public boolean getAddColumn() {
    return addColumn;
  }
  public boolean getRemoveColumn() {
    return removeColumn;
  }
  public boolean getImportColumns() {
    return importColumns;
  }
  public boolean getInitColumnWithFile() {
    return initColumnWithFile;
  }
  public boolean getInitColumnWithDefault() {
    return initColumnWithDefault;
  }
  public boolean getInitColumnWithValue() {
    return initColumnWithValue;
  }
  public boolean getAddFilledColumn() {
    return addFilledColumn;
  }
  public boolean getInitSelectionWithValues() {
    return initSelectionWithValues;
  }
  public int getColumnIndex(int i) {
    return columnIndex[i];
  }
  public TColumnState getColumnState(int i) {
    return columnState[i];
  }
  public StringSelection getColumnContents(int i) {
    return columnContents[i];
  }
  public int getNbColumns() { return columnIndex.length; }
  public File getFile() { return file; }
  public int[] getSelection() { return selection; }
}

/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
