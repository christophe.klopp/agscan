package agscan.data.controler.memento;

import java.util.Vector;

public class TDataElementMementos {
  private Vector mementos;
  private int index;

  public TDataElementMementos() {
    mementos = new Vector();
    index = 0;
  }
  public void pokeMemento(TUndoElement state) {
    removeMementosAboveIndex();
    mementos.addElement(new TDataElementMemento(state));
    index++;
  }
  public void removeMemento() {
    if (index > 1) {
      mementos.remove(index - 1);
      index--;
    }
  }
  public TUndoElement peekMemento() {
    if (index > 1) {
      TUndoElement undoElement = ((TDataElementMemento)mementos.elementAt(--index)).getUndoElement();
      return undoElement;
    }
    return null;
  }
  public TUndoElement getPreviousStateMemento() {
    if (index >= 1) {
      int i = index - 1;
      while (((TDataElementMemento)mementos.elementAt(i)).getUndoElement() instanceof TUndoAction) i--;
      return ((TDataElementMemento)mementos.elementAt(i)).getUndoElement();
    }
    return null;
  }
  public TUndoElement getNextMemento() {
    if (index < mementos.size()) {
      TUndoElement undoElement = ((TDataElementMemento)mementos.elementAt(index++)).getUndoElement();
      return undoElement;
    }
    return null;
  }
  public void removeMementosAboveIndex() {
    while (mementos.size() > index) mementos.remove(mementos.size() - 1);
  }
  public boolean isEmpty() {
    return (mementos.size() == 1);
  }
  public boolean hasUndo() {
    return (index > 1);
  }
  public boolean hasRedo() {
    return (index < mementos.size());
  }
  public void clear() {
    index = 1;
    removeMementosAboveIndex();
  }
  public Vector getUsedMementos() {
    Vector v = new Vector();
    for (int i = 0; i < index; i++) v.addElement(mementos.elementAt(i));
    return v;
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
