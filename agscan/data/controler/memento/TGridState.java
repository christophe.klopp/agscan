/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2002</p>
 * <p>Soci�t� : </p>
 * @author non attribuable
 * @version 1.0
 */

package agscan.data.controler.memento;

public class TGridState extends TUndoState {
  private int nbLevels, lev1NbCols, lev1NbRows, lev2NbCols, lev2NbRows, lev3NbCols, lev3NbRows, lev1Start, lev2Start, lev3Start;
  private double lev1ColWidth, lev1RowHeight, lev1Diameter, lev2ColSpacing, lev2RowSpacing, lev3ColSpacing, lev3RowSpacing;
  private int lev1SpotRC, lev2BlockRC, lev3BlockRC;
//level3 removed - 20005/10/28
  public TGridState(int nbLev, int L1nbCols, double L1ColWidth, int L1NbRows, double L1RowHeight, double diameter,
                    int L1Start, int L1SpotRC, int L2NbCols, double L2ColSpacing, int L2NbRows, double L2RowSpacing,
                    int L2Start, int L2BlockRC/*, int L3NbCols, double L3ColSpacing, int L3NbRows, double L3RowSpacing,
                    int L3Start, int L3BlockRC*/) {
    nbLevels = nbLev;
    lev1NbCols = L1nbCols;
    lev1ColWidth = L1ColWidth;
    lev1NbRows = L1NbRows;
    lev1RowHeight = L1RowHeight;
    lev1Diameter = diameter;
    lev1Start = L1Start;
    lev1SpotRC = L1SpotRC;
    lev2NbCols = L2NbCols;
    lev2ColSpacing = L2ColSpacing;
    lev2NbRows = L2NbRows;
    lev2RowSpacing = L2RowSpacing;
    lev2Start = L2Start;
    lev2BlockRC = L2BlockRC;
//  level3 removed - 20005/10/28
   /* lev3NbCols = L3NbCols;
    lev3ColSpacing = L3ColSpacing;
    lev3NbRows = L3NbRows;
    lev3RowSpacing = L3RowSpacing;
    lev3Start = L3Start;
    lev3BlockRC = L3BlockRC;
    */
  }
  public int getNbLevels() {
    return nbLevels;
  }
  public int getLev1NbCols() {
    return lev1NbCols;
  }
  public int getLev1NbRows() {
    return lev1NbRows;
  }
  public int getLev2NbCols() {
    return lev2NbCols;
  }
  public int getLev2NbRows() {
    return lev2NbRows;
  }
  public int getLev3NbCols() {
    return lev3NbCols;
  }
  public int getLev3NbRows() {
    return lev3NbRows;
  }
  public double getLev1ColWidth() {
    return lev1ColWidth;
  }
  public double getLev1RowHeight() {
    return lev1RowHeight;
  }
  public double getLev1Diameter() {
    return lev1Diameter;
  }
  public double getLev2ColSpacing() {
    return lev2ColSpacing;
  }
  public double getLev2RowSpacing() {
    return lev2RowSpacing;
  }
//level3 removed - 20005/10/28
  /*
  public double getLev3ColSpacing() {
    return lev3ColSpacing;
  }
  public double getLev3RowSpacing() {
    return lev3RowSpacing;
  }*/
  public int getLev1Start() {
    return lev1Start;
  }
  public int getLev2Start() {
    return lev2Start;
  }
//level3 removed - 20005/10/28
  /*public int getLev3Start() {
    return lev3Start;
  }*/
  public int getLev1SpotRC() {
    return lev1SpotRC;
  }
  public int getLev2BlockRC() {
    return lev2BlockRC;
  }
//level3 removed - 20005/10/28
 /* public int getLev3BlockRC() {
    return lev3BlockRC;
  }*/
  public String toString() {
    return "";
  }
}/******************************************************
* COPYRIGHT AND PERMISSION NOTICE
*
* Copyright (c) 2004 INSERM-ERM 206 - TAGC
*
* All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and/or sell copies of the Software, and to permit persons
* to whom the Software is furnished to do so, provided that the above
* copyright notice(s) and this permission notice appear in all copies of
* the Software and that both the above copyright notice(s) and this
* permission notice appear in supporting documentation.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
* OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
* HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
* SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Except as contained in this notice, the name of a copyright holder
* shall not be used in advertising or otherwise to promote the sale, use
* or other dealings in this Software without prior written authorization
* of the copyright holder.
*******************************************************/
