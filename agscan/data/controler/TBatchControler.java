package agscan.data.controler;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.Enumeration;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.xml.sax.helpers.DefaultHandler;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.Messages;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.algo.spotdetection.TFourProfilesSpotDetectionAlgorithm;
import agscan.data.TDataManager;
import agscan.data.controler.worker.TBatchWorker;
import agscan.data.controler.worker.TBatchWorkerNImages;
import agscan.data.controler.worker.TPlotArrayWorker;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.TImageModel;
import agscan.data.model.batch.TBatchModelNImages;
import agscan.data.model.batch.job.TAbstractJob;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.TBatchComPanel;
import agscan.data.view.TBatchView;
import agscan.data.view.TViewImpl;
import agscan.data.view.graphic.TChartsPanel;
import agscan.data.view.table.TBatchTablePanel;
import agscan.dialog.TDialogManager;
import agscan.dialog.TRBCPlotsDialog;
import agscan.event.TEvent;
import agscan.ioxml.TBZGridReader;
import agscan.ioxml.TGridReader;
import agscan.ioxml.TMageMlGridReader;
import agscan.ioxml.TOpenAlignmentWorker;
import agscan.ioxml.TOpenFileWorker;
import agscan.ioxml.TReadQuantifResultsWorker;
import agscan.ioxml.TResultArray;
import agscan.menu.TMenuManager;
import agscan.menu.action.TSaveAsAction;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPluginManager;
import agscan.statusbar.TStatusBar;
import agscan.data.element.batch.TBatch;
import agscan.data.element.grid.TGrid;
import agscan.plugins.*;
/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Societe : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TBatchControler extends TControlerImpl {
  public TBatchControler() {
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public static final int ADD_JOB = 701;
  public static final int REMOVE_JOB = 702;
  public static final int ADD_JOBS = 703;
  public static final int REMOVE_SELECTED_JOBS = 704;
  public static final int CLEAR_JOBS = 705;
  public static final int SET_GRID = 706;
  public static final int GET_IMAGE_PATH = 707;
  public static final int GET_IMAGE_NAME = 708;
  public static final int RUN = 709;
  public static final int SHOW = 710;
  public static final int RAZ_SHOW = 711;
  public static final int PROGRESS = 712;
  public static final int REFRESH_VIEW = 713;
  public static final int SHOW_CURRENT_ALIGNMENT = 714;
  public static final int GET_NB_SELECTED_JOBS = 715;
  public static final int GET_NB_JOBS = 716;
  public static final int RESET_SELECTED_JOBS = 717;
  public static final int SET_OUTPUT_PATH = 718;
  public static final int SELECT_ALL = 719;
  public static final int SAVE_IMAGES = 720;
  public static final int OPEN = 721;
  public static final int MAKE_PLOT = 722;
  public static final int SHOW_PLOTS = 723;
  public static final int MAKE_PLOTS = 724;
  public static final int SAVE_ALIGNMENT = 725;
  public static final int CHANGE_TABLE =  726;
  public static final int TREATMENT_PROGRESSION_UNITS = 1;
  public static final int GLOBAL_ALIGNMENT_PROGRESSION_UNITS = 4;
  public static final int LOCAL_ALIGNMENT_PROGRESSION_UNITS = 8;
  public static final int QUANTIF_PROGRESSION_UNITS = 7;

  private TBatchWorkerNImages batchWorker;
  //private TBatchWorker batchWorker;
  private int pc0 = 0, pc1 = 0, pc2 = 0;
  private boolean showCA;

  public TBatchControler(TBatch ref) {
    reference = ref;
    showCA = false;
  }
  public Object[] processEvent(TEvent event) {
    Object[] ret = null;
    TBatchModelNImages batchModel = (TBatchModelNImages)reference.getModel();
    String s;
    boolean cent = false;
    TDataElement element;
    switch (event.getAction()) {
      case ADD_JOB:
      	break;
      case REMOVE_JOB:
      	break;
      case ADD_JOBS:
      	File[] files = (File[])event.getParam(1);
      	//modified 2005/11/04
      	int nbChannels = ((Integer)event.getParam(2)).intValue();
      	System.out.println("dans Batch controler nbChannels="+nbChannels);
      	if (nbChannels==1){
      		//si un seul canal une selection multiple = plusieurs experiences
      		for (int i = 0; i < files.length; i++)
      			batchModel.addImage(files[i].getAbsolutePath(),1);
      	}
      	else{
      		//sinon il faut prendre les images 1,2 ou 3 par 3...
      		if (files.length!=nbChannels){
      			JOptionPane.showMessageDialog(null,"Please select "+nbChannels+ "images", "Bad selection!", JOptionPane.ERROR_MESSAGE);
      		}
      		else for (int i = 0; i < files.length; i++){
      			batchModel.addImage(files[i].getAbsolutePath(),i+1);
      		}
      	}
      		
      	reference.getView().refresh();
      	break;
      	//test 2005/11/04
      	case CHANGE_TABLE:
      	//batchModel.getColumns();      	
      	int nbChan = ((Integer)event.getParam(1)).intValue();
      	//TBatchModelNImages model =(TBatchModelNImages)batch.getModel();
      	batchModel.setNbChan(nbChan);//the number of channels to add - added 2005/11/04      	
      	System.out.println("MODELE MIS A JOUR : "+nbChan+" COLONNES");
      	DefaultTableColumnModel columns = ((TBatchModelNImages)reference.getModel()).getColumns();
      	JTable table = reference.getView().getTablePanel().getTable();
      	table.setColumnModel(columns);
      	
      	//System.out.println("dans change table !!! nb="+nbChan);
    	//event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, v);
		//TEventHandler.handleMessage(event);
      	//batch.setModel(model);
      	break;
      	
      	//end test
      	
      	
      	
      case REMOVE_SELECTED_JOBS:
        int[] rows = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows();
        batchModel.removeJobs(rows);
        reference.getView().refresh();
        batchModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
        break;
      case GET_NB_SELECTED_JOBS:
        ret = new Object[1];
        ret[0] = new Integer(((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows().length);
        break;
      case GET_NB_JOBS:
        ret = new Object[1];
        ret[0] = new Integer(batchModel.getRowCount());
        break;
      case CLEAR_JOBS:
        batchModel.clearJobs();
        reference.getView().refresh();
        break;
      case SET_OUTPUT_PATH:
        ((TBatch)reference).setOutputPath((String)event.getParam(1));
        break;
      case SAVE_IMAGES:
        ((TBatchModelNImages)reference.getModel()).setSaveImages(((Boolean)event.getParam(1)).booleanValue());
        //System.err.println(((TBatchModelNImages)reference.getModel()).getSaveImages());
        break;
      case SET_GRID:
        TGridReader gridReader = null;
        File gridFile = (File)event.getParam(1);
        if (gridFile.getAbsolutePath().endsWith(".grd"))
          gridReader = new TBZGridReader(gridFile.getAbsolutePath());
        else if (gridFile.getAbsolutePath().endsWith(".xml"))
          gridReader = new TMageMlGridReader(gridFile.getAbsolutePath());
        SAXParserFactory factory = SAXParserFactory.newInstance();
        TGrid grid = null;
        TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchControler.1"), Color.red);
        TEventHandler.handleMessage(ev);
        try
        {
          SAXParser saxParser = factory.newSAXParser();
          saxParser.parse(gridFile, (DefaultHandler)gridReader);
          grid = gridReader.getGrid();
          String pathSeparator = System.getProperty("file.separator");
          String p = pathSeparator;
          String filename = gridFile.getAbsolutePath();
          int ix = filename.lastIndexOf(pathSeparator);
          if (ix > -1) p = filename.substring(0, ix + 1);
          grid.setPath(p);
          grid.setName(filename.substring(ix + 1));
        }
        catch (Throwable t) {
          t.printStackTrace();
        }
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
        TEventHandler.handleMessage(ev);
        ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchControler.0"), Color.blue);
        TEventHandler.handleMessage(ev);
        ((TBatchModelNImages)reference.getModel()).setGrid(grid);
        reference.getView().refresh();
        break;
      case TGridControler.GET_GRID:
        ret = new Object[1];
        ret[0] = ((TBatchModelNImages)reference.getModel()).getGrid().getGridModel().getRootBlock();
        break;
      case TGridControler.GET_ALL_SPOTS:
        ret = new Object[1];
        ret[0] = ((TBatchModelNImages)reference.getModel()).getGrid().getGridModel().getSpots();
        break;
      case GET_IMAGE_PATH:
        ret = new Object[1];
        int nbChan4path = ((Integer)event.getParam(2)).intValue(); 
        ret[0] = batchModel.getImagePath(((Integer)event.getParam(1)).intValue(),1);
        break;
      case GET_IMAGE_NAME:
        ret = new Object[1];
        int nbChan4name = ((Integer)event.getParam(2)).intValue(); 
        ret[0] = batchModel.getImageName(((Integer)event.getParam(1)).intValue(),1);
        break;
      case RUN:
        pc0 = pc1 = pc2 = 0;
        cent = false;
        batchWorker = new TBatchWorkerNImages((TBatch)reference);
        batchWorker.start();
        
        break;
      case SAVE_AS:
        ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.IS_DATA_OPENED, event.getParam(0), (File)event.getParam(1));
        boolean b = ((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue();
        if (b)
          messageFichierOuvert(((File)ev.getParam(1)).getAbsolutePath());
        else {
          b = TImageControler.okToWrite(((File)event.getParam(1)).getAbsolutePath());
          if (b) {
            save((File)event.getParam(1), ((Boolean)event.getParam(2)).booleanValue());
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
          }
        }
        break;
      case SAVE:
        String path = reference.getPath();
        String name = reference.getName();
        if (!path.equals("")) {
          File file = new File(path + name);
          save(file);
          TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference));
        }
        else {
          ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.FIRE_ACTION, new Integer(TSaveAsAction.getID()));//TMenuManager.SAVE_AS avant
          TEventHandler.handleMessage(ev);
        }
        break;
      case SAVE_ALIGNMENT:
        element = ((TBatch)reference).getShowingElement();
        if (element instanceof TAlignment) {
          TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.SAVE, element));
        }
        break;
        
      /*** Affichage apres batch ***/
      case SHOW:
        //((TBatchTablePanel)reference.getView().getTablePanel()).setShowCurrentAlignmentCheckBoxSelected(false);
    	int row = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows()[0];
        int pv = ((Integer)batchModel.getProgressValues().elementAt(row)).intValue();
        if (batchModel.getImageName(row,0).endsWith(".zaf")) {
          if ((pv == TAbstractJob.DONE) || (pv == TAbstractJob.FAILED) || (pv == TAbstractJob.QUEUED)) {
            System.err.println(batchModel.getImageName(row,0));
            TOpenAlignmentWorker worker = new TOpenAlignmentWorker(new File(batchModel.getImageName(row,0)));
            worker.setPriority(Thread.MAX_PRIORITY);
            TAlignment alignment = (TAlignment)worker.construct(false);
            if (alignment != null) {
              Enumeration enume = alignment.getGridModel().getSpots().elements();
              TSpot spot;
              
              TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
  
              alignment.getModel().setReference(alignment);
              event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
              alignment.getView().setZoom(((Integer)TEventHandler.handleMessage(event)[0]).intValue());
              ((TBatch)reference).setShowingElement(alignment);
              ((TBatch)reference).setShowingImage(true);
            }
            event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, alignment, new Integer(0));
            TEventHandler.handleMessage(event);
            event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, alignment, new Boolean(false));
            TEventHandler.handleMessage(event);
            event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment, Messages.getString("TBatchControler.0"), Color.blue);
            TEventHandler.handleMessage(event);
            FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
          }
          else if ((pv >= 0) && (pv <= 100)) {
            TAlignment alignment = batchWorker.getCurrentAlignment();
            if (alignment != null) {
              TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment));
              alignment.getModel().setReference(alignment);
              event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
              alignment.getView().setZoom(((Integer)TEventHandler.handleMessage(event)[0]).intValue());
              ((TBatch)reference).setShowingElement(alignment);
              ((TBatch)reference).setShowingImage(true);
            }
          }
        }
        else {
          File[] f = new File[1];
          f[0] = new File(batchModel.getImageName(row,1));
          TOpenFileWorker worker = new TOpenFileWorker(f);
          worker.setPriority(Thread.MAX_PRIORITY);
          TImage image = (TImage) worker.construct(false);
          if (image != null) {
            event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
            image.getView().setZoom(((Integer)TEventHandler.handleMessage(event)[0]).intValue());
            image.getModel().setReference(image);
            ((TBatch)reference).setShowingElement(image);
            ((TBatch)reference).setShowingImage(true);
          }
          event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
          TEventHandler.handleMessage(event);
          event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
          TEventHandler.handleMessage(event);
          event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchControler.0"), Color.blue);
          TEventHandler.handleMessage(event);
        }
        event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
        TEventHandler.handleMessage(event);
        ((TBatchTablePanel)reference.getView().getTablePanel()).refresh();
        break;
      case OPEN:
        row = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows()[0];
        pv = ((Integer)batchModel.getProgressValues().elementAt(row)).intValue();
        if (batchModel.getImageName(row,0).endsWith(".zaf")) {
        	if ((pv == TAbstractJob.DONE) || (pv == TAbstractJob.FAILED) || (pv == TAbstractJob.QUEUED)) {
        	String pathSeparator = System.getProperty("file.separator");
        	String filename = batchModel.getImageName(row,0);
        	int ix = filename.lastIndexOf(pathSeparator);
        	if (ix > -1) filename = filename.substring(ix+1,filename.length());
        	String fil = ((TBatch)reference).getOutputPath()+pathSeparator+filename;
        	System.err.println(fil);
        	event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_ALIGNMENT, null, new File(fil));
            TEventHandler.handleMessage(event);
          }
        }
        else {
          event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, new File(batchModel.getImageName(row,0)));
          TEventHandler.handleMessage(event);
        }
        break;
      case RAZ_SHOW:
        RAZShow();
        break;
      case TStatusBar.ALGO_PROGRESS:
        int p = ((Integer)event.getParam(1)).intValue();
        if ((p == 100) && !cent) {
          pc0 += 100;
          cent = true;
        }
        else {
          pc1 = pc0 + p;
          cent = false;
        }
        if (pc1 != pc2) {
          if (batchWorker.getNbJobsToDo() > 0)
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, reference,
                new Integer(pc1 / batchWorker.getNbJobsToDo())));
          else
            TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.ALGO_PROGRESS, reference,
                new Integer(0)));
          pc2 = pc1;
        }
        ((TBatchModelNImages)reference.getModel()).getProgressValues().setElementAt(event.getParam(1), batchWorker.getCurrentRunningJob());
        reference.getView().getTablePanel().repaint();
        break;
      case REFRESH_VIEW:
        refreshView();
        break;
      case SHOW_CURRENT_ALIGNMENT:
        showCA = ((Boolean)event.getParam(1)).booleanValue();
        if (showCA)
          refreshView();
        else
          RAZShow();
        break;
      case RESET_SELECTED_JOBS:
        rows = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows();
        batchModel.resetJobs(rows);
        reference.getView().refresh();
        batchModel.addModif(1);
        TEventHandler.handleMessage(new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, null));
        break;
      case SELECT_ALL:
        ((TBatchTablePanel)reference.getView().getTablePanel()).selectAll();
        break;
      case MAKE_PLOT:
        rows = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows();
        ret = new Object[1];
        Object[] params = (Object[])event.getParam(1);
        ret[0] = makePlot((int[])params[0], (int[])params[1], (int[])params[2], (int[])params[3], (String)params[4], (String)params[5], (String)params[6]);
        break;
      case SHOW_PLOTS:
        s = ((TBatch)reference).getName();
        s = s.substring(0, s.lastIndexOf("."));
        TRBCPlotsDialog dialog = new TRBCPlotsDialog(((TBatch)reference).getOutputPath(), s);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        dialog.pack();
        dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
        dialog.setVisible(true);
        if (dialog.getOkOut()) {
          rows = ( (TBatchTablePanel) reference.getView().getTablePanel()).getSelectedRows();
          TPlotArrayWorker paw = new TPlotArrayWorker(rows, (TBatch) reference, dialog.getDir() + File.separator + dialog.getName(), dialog.getLinear(), dialog.getLog());
          paw.start();
        }
        break;
      case MAKE_PLOTS:
        rows = ((TBatchTablePanel)reference.getView().getTablePanel()).getSelectedRows();
        makePlots(rows);
        break;
      default:
        if (((TBatch)reference).isShowingImage()) {
          TDataElement showingElement = ((TBatch)reference).getShowingElement();
          if (showingElement != null) {
            event.setParam(0, showingElement);
            ret = showingElement.getControler().processEvent(event);
            reference.getView().getGraphicPanel().repaint();
          }
        }
        break;
    }
    return ret;
  }
  private void messageFichierOuvert(String filename) {
    JOptionPane.showMessageDialog(null, "<html><P>Le fichier <font color=red><b>" + filename + "</b></font> est deje ouvert dans BZScan.</P></html>",
                                  "Erreur", JOptionPane.WARNING_MESSAGE);
  }

  private JFreeChart makePlot(int p1, int p2, String sk) {
    TBatchModelNImages model = (TBatchModelNImages)reference.getModel();
    TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
    sdAlgo.initWithDefaults();
    TOpenAlignmentWorker worker = new TOpenAlignmentWorker(new File(model.getImageName(p1,0)));
    worker.setPriority(Thread.MAX_PRIORITY);
    TAlignment alignment1 = (TAlignment)worker.construct(false);
    worker = new TOpenAlignmentWorker(new File(model.getImageName(p2,0)));
    worker.setPriority(Thread.MAX_PRIORITY);
    TAlignment alignment2 = (TAlignment)worker.construct(false);
    return makePlot(alignment1, alignment2, sk);
  }
  private JFreeChart makePlot(int[] v1, int[] v2, int[] vb1, int[] vb2, String n1, String n2, String sk) {
    XYSeries series_1, series_0, series_mixed;
    series_1 = new XYSeries(new Integer(0));
    series_0 = new XYSeries(new Integer(0));
    series_mixed = new XYSeries(new Integer(0));
    int q1, q2;
    for (int i = 0; i < v1.length; i++) {
      q1 = vb1[i];
      q2 = vb2[i];
      if (q1 != q2)
        series_mixed.add(v1[i], v2[i]);
      else if (q1 == 1)
        series_1.add(v1[i], v2[i]);
      else if (q1 == 0)
        series_0.add(v1[i], v2[i]);
    }
    XYSeriesCollection coll = new XYSeriesCollection(series_1);
    coll.addSeries(series_0);
    coll.addSeries(series_mixed);
    JFreeChart plot = ChartFactory.createScatterPlot(sk, n1, n2, coll, PlotOrientation.HORIZONTAL, false, false, false);
    XYDotRenderer dotRenderer = new XYDotRenderer();
    dotRenderer.setSeriesPaint(0, Color.red);
    dotRenderer.setSeriesPaint(1, Color.green);
    dotRenderer.setSeriesPaint(2, Color.orange);
    plot.getXYPlot().setRenderer(dotRenderer);
    return plot;
  }
  private JFreeChart makePlot(TAlignment alignment1, TAlignment alignment2, String sk) {
    FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    XYSeries series_1, series_0, series_mixed;
    XYSeriesCollection coll;
    JFreeChart plot;
    TFourProfilesSpotDetectionAlgorithm sdAlgo = new TFourProfilesSpotDetectionAlgorithm(true);
    sdAlgo.initWithDefaults();
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment1));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY, alignment1, sdAlgo));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TGridSelectionControler.SELECT_ALL, alignment2));
    TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TAlignmentControler.COMPUTE_SPOT_QUALITY, alignment2, sdAlgo));
    int[] v1, v2, vb1, vb2;
    vb1 = alignment1.getGridModel().getIntSerie("RG");
    vb2 = alignment2.getGridModel().getIntSerie("RG");
    XYDotRenderer dotRenderer = new XYDotRenderer();
    dotRenderer.setSeriesPaint(0, Color.red);
    dotRenderer.setSeriesPaint(1, Color.green);
    dotRenderer.setSeriesPaint(2, Color.orange);
    v1 = alignment1.getGridModel().getIntSerie(sk);
    v2 = alignment2.getGridModel().getIntSerie(sk);
    series_1 = new XYSeries(new Integer(0));
    series_0 = new XYSeries(new Integer(0));
    series_mixed = new XYSeries(new Integer(0));
    for (int i = 0; i < v1.length; i++) {
      if (vb1[i] != vb2[i])
        series_mixed.add(v1[i], v2[i]);
      else if (vb1[i] == 1)
        series_1.add(v1[i], v2[i]);
      else if (vb1[i] == 0)
        series_0.add(v1[i], v2[i]);
    }
    coll = new XYSeriesCollection(series_1);
    coll.addSeries(series_0);
    coll.addSeries(series_mixed);
    plot = ChartFactory.createScatterPlot(sk, alignment1.getName(), alignment2.getName(), coll, PlotOrientation.HORIZONTAL, false, false, false);
    plot.getXYPlot().setRenderer(dotRenderer);
    FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
    return plot;
  }

  private void makePlots(int[] rows) {
    TBatchModelNImages model = (TBatchModelNImages)reference.getModel();
    JFreeChart plot;
    ChartPanel cp;
    
    
    	/*****************************/
    
    
    
    	// Il faut ici recuperer les colonnes de la quantification que l'on vient d'effectuer
    	//  recuperation de l'alignement 
		/*TAlignment alignment = null;
		TEvent ev = null;
		ev = new TEvent(TEventHandler.DATA_MANAGER,TDataManager.GET_CURRENT_DATA, null); 
		TDataElement currentElement = (TDataElement) TEventHandler.handleMessage(ev)[0]; 
		TControlerImpl c = currentElement.getControler();
		if (currentElement instanceof TAlignment) alignment = (TAlignment)currentElement;
		else if (currentElement instanceof TBatch) 
		 {
			if (((TBatch)currentElement).getWorker()==null)System.out.println("PROBLEM : worker == null!!!");
			alignment = ((TBatch)currentElement).getWorker().getCurrentAlignment();
			if (alignment == null)System.out.println("PROBLEM : alignment == null!!!");
		 }
		else System.out.println("PROBLEM : currentElement == null!!!");
	    DefaultTableColumnModel columns = alignment.getGridModel().getConfig().getColumns();
		TColumn col = null;
		Enumeration enume = columns.getColumns();//l'enumeration contient toutes les colonnes de la table
		boolean nameExist = false;	
		String[] sks = new String[columns.getColumnCount()];
		for(int i=0;i<columns.getColumnCount();i++) {
			sks[i]=((TColumn)enume.nextElement()).getDefaultValueString();
			i++;
		}*/
    
		/*****************************/
    int indiceplug=-1;
    TBatch batch = batchWorker.getBatch();
    TBatchView view = (TBatchView)batch.getView();
    TBatchTablePanel tabpan = (TBatchTablePanel)view.getTablePanel();
    TBatchComPanel bcp = tabpan.getBCP();
    int nbc = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");
    int nbcol=3+nbc;
    System.out.println("NBCOL="+nbcol);
    
    for (int i=0;i<TPluginManager.plugs.length;i++)
    {
    	if (bcp.configContains(TPluginManager.plugs[i].getName())&&(TPluginManager.plugs[i] instanceof SQuantifPlugin))
    	{
    		indiceplug = i;
    		nbcol = ((SQuantifPlugin)TPluginManager.plugs[i]).columnsNames.length;
    	}
    }
    String[] sks  = new String[nbcol];
    if (indiceplug!=-1)
    {
    	
    	for (int i=0;i<nbcol;i++)
	    {
	    	sks[i] = ((SQuantifPlugin)TPluginManager.plugs[indiceplug]).columnsNames[i];
	    	System.err.println(sks[i]+ "...........");
	    }
    }
    else 
    {
    	sks[0] = TQuantifImageComputedAlgorithm.COLUMN_NAME ;
    	sks[1] = TQuantifFitConstantAlgorithm.COLUMN_NAME ;
    	sks[2] = TQuantifFitComputedAlgorithm.COLUMN_NAME ;
    	sks[3] = TQuantifImageConstantAlgorithm.COLUMN_NAME ;
    	for(int i=4;i<nbcol;i++)
    	{
    		sks[i]= TQuantifImageConstantAlgorithm.COLUMN_NAME+"("+(i-2)+")" ;
    	}
    }
    String n1 = model.getImageName(rows[0],0).substring(model.getImageName(rows[0],0).lastIndexOf(File.separator) + 1);
    String n2 = model.getImageName(rows[1],0).substring(model.getImageName(rows[1],0).lastIndexOf(File.separator) + 1);
    TReadQuantifResultsWorker worker = new TReadQuantifResultsWorker(new File(model.getImageName(rows[0],0)), reference, sks);
    worker.setPriority(Thread.MAX_PRIORITY);
    TResultArray results1 = (TResultArray)worker.construct(false);
    worker = new TReadQuantifResultsWorker(new File(model.getImageName(rows[1],0)), reference, sks);
    worker.setPriority(Thread.MAX_PRIORITY);
    TResultArray results2 = (TResultArray)worker.construct(false);

    TChartsPanel chartsPanel = new TChartsPanel();
    XYDotRenderer dotRenderer = new XYDotRenderer();
    dotRenderer.setSeriesPaint(0, Color.red);
    dotRenderer.setSeriesPaint(1, Color.green);
    dotRenderer.setSeriesPaint(2, Color.orange);
    dotRenderer.setSeriesPaint(3, Color.blue);

    for (int i = 0; i < nbcol; i++) {
      plot = makePlot(results1.getResults()[i], results2.getResults()[i], results1.getQuality(), results2.getQuality(), n1, n2, sks[i]);
      cp = new ChartPanel(plot);
      cp.setPreferredSize(new Dimension(500, 400));
      chartsPanel.addChartPanel(cp, sks[i]);
    }
    ((TBatch)reference).setShowingElement(chartsPanel);
    ((TBatch)reference).setShowingImage(true);
    TEvent event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR, null, new Integer(0));
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_PROGRESS_BAR_SHOW_PERCENT, null, new Boolean(false));
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, null, Messages.getString("TBatchControler.0"), Color.blue);
    TEventHandler.handleMessage(event);
    FenetrePrincipale.application.setCursor(Cursor.getDefaultCursor());
  }
  private void RAZShow() {
    ((TBatch)reference).setShowingImage(false);
    ((TBatch)reference).setShowingElement((TDataElement)null);
    TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, null);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DIALOG_MANAGER, TDialogManager.UPDATE, null);
    TEventHandler.handleMessage(event);
  }
  public void refreshView() {
    if (batchWorker != null) {
      TAlignment alignment = batchWorker.getCurrentAlignment();
      ( (TBatch) reference).setShowingImage(true);
      if (alignment != null) {
        TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.GET_ZOOM, null);
        alignment.getView().setZoom( ( (Integer) TEventHandler.handleMessage(event)[0]).intValue());
        ( (TBatch) reference).setShowingElement(alignment);
      }
    }
  }
  private void save(File file, boolean saveImages) {
    ((TBatchModelNImages)reference.getModel()).setSaveImages(saveImages);
    save(file);
  }
  private void save(File file) {
    ((TBatchTablePanel)reference.getView().getTablePanel()).update();
    ((TBatchModelNImages)reference.getModel()).save(file);
    String pathSeparator = System.getProperty("file.separator");
    String p = pathSeparator;
    String filename = file.getAbsolutePath();
    int ix = filename.lastIndexOf(pathSeparator);
    if (ix > -1) p = filename.substring(0, ix + 1);
    ((TBatch)reference).setPath(p);
    ((TBatch)reference).setName(filename.substring(ix + 1));
    TEvent event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.UPDATE_VIEW_NAME, ((TBatch)reference).getName(), ((TBatch)reference).getPath());
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CLEAR_MODIFS, null);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, reference);
    TEventHandler.handleMessage(event);
    event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.UPDATE, reference);
    TEventHandler.handleMessage(event);
  }
  public boolean showCurrentAlignment() {
    return showCA;
  }

  private void jbInit() throws Exception {
  }
}
