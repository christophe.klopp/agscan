/*
 * Created on Aug 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 * 
 * 
 * classe outil intermédiaire pour caster les properties de String en int, double...
 * cela evite de caster apres l'appel getProperty
 * 
 * 
 */
package agscan;

import java.io.File;
import java.util.Properties;


public class MyProperties extends Properties {
	public MyProperties(){
		super();
	}	
	
	public MyProperties(Properties defaults) {
		super(defaults);
	}
	
	// INUTILE CAR ON CREE UN FICHIER PROPERTIES ET DONC CETTE FONCTION N'EXISTERA PAS:
	//CF dans TBZScan2.java 	public static Properties prop = new Properties(new DefaultProperties());
	// on crée un Properties qu iaura pour default DefaultProperties
	// 2 possibilités:
	//    1/surcharger le type PROPERTIES de base pour permettreles getInt, getDouble...
	//    2/garder le type PROPERTIES mais faire les appels en parsant le resultat qui sera un string...
		
	// CREER une classe intermediaire MyProperties qui connait les int et les doubles
	// et c'est de cette classe que va heriter DefaultProperties
	
	
	//retourne l'int (caste la valeur string en int) correspondant à la clef 
	public  int getIntProperty(String key) {
		return Integer.parseInt(this.getProperty(key));		
	}
	
	//	retourne le double (caste la valeur string en double) correspondant à la clef
	public  double getDoubleProperty(String key) {
		return Double.parseDouble(this.getProperty(key));		
	}
		
	// surcharge de setProperty pour transformer un entier en String dans la hashtable
	public void setProperty(String key, int value){
		setProperty(key,String.valueOf(value));
	}
	
	//surcharge de setProperty pour transformer un double en String dans la hashtable
	public void setProperty(String key, double value){
		setProperty(key,String.valueOf(value));
	}
	
	//2005/12/01 - this method allows to verify that paths saved exist
	// if paths are not valid for this session, there are replaced with the current directory
	public void chekPaths(){
		File testPath = null;
		if (!new File(this.getProperty("imagesPath")).exists()) this.setProperty("imagePath","."+File.separator);
		if (!new File(this.getProperty("gridsPath")).exists()) this.setProperty("gridsPath","."+File.separator);
		if (!new File(this.getProperty("batchPath")).exists()) this.setProperty("batchPath","."+File.separator);
		if (!new File(this.getProperty("alignmentsPath")).exists()) this.setProperty("alignmentsPath","."+File.separator);
		if (!new File(this.getProperty("columnsPath")).exists()) this.setProperty("columnsPath","."+File.separator);
		if (!new File(this.getProperty("exportsPath")).exists()) this.setProperty("exportsPath","."+File.separator);
	}			
}