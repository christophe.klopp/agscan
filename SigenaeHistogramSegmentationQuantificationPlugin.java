/* Created on Oct 19, 2005
 * 
 * 
 * This class is an example of quantification plugin 

 * Note: the idea here is to indicate the places where the code has to be.
 * updated or modified in order to adapt it to the need.	
 * 
 * @version 2006/10/10
 * @author cklopp
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * 
 */

import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.HistogramWindow;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import plugins.ImageTools;

import agscan.AGScan;
import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.dialog.TOpenNImagesDialog;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SigenaeHistogramSegmentationQuantificationPlugin extends SQuantifPlugin {
	// *** size to be adjusted to the need
	int size;
	String[] colors;
	int KERNEL_SIZE =5;
	int TEST = 0;

	//public static int id = -1;

	public SigenaeHistogramSegmentationQuantificationPlugin() {

		super("Sigenae Histogram Segmentation Quantification plugin", null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		imagesMin = 2;
		imagesMax = 5;
		//declaration of columns of the plugin

	}

	public String getName() {
		return (String) this.getValue(NAME);
	}

	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	 */
	public void actionPerformed(ActionEvent e) {

		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);

	}

	/**
	 * run of the quantification 
	 */
	public void run() {				
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		size = model.getNumberOfChannels();
		colors = new String[size];
		columnsNames = new String[6*size];//array of columns created by the quantification
		columnsTypes = new int[6*size];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
		
		colors = TOpenNImagesDialog.findColors();
		for (int i=0;i<size;i++)
		{
			columnsNames[i]= "FG "+colors[i]+" Total";
		}
		for (int i=0;i<size;i++)
		{
			columnsNames[size+i]= "BG "+colors[i]+" Total";
		}
		for (int i=0;i<size;i++)
		{
			columnsNames[size*2+i]= "FG "+colors[i]+" Mean";
		}
		for (int i=0;i<size;i++)
		{
			columnsNames[size*3+i]= "BG "+colors[i]+" Mean";
		}
		for (int i=0;i<size;i++)
		{
			columnsNames[size*4+i]= "FG "+colors[i]+" Median";
		}
		for (int i=0;i<size;i++)
		{
			columnsNames[size*5+i]= "BG "+colors[i]+" Median";
		}

		// *** to be adjusted to the neednt[] 
		//type of quantification results
		for (int i=0;i<size*6;i++)
		{
			columnsTypes[i] = TColumn.TYPE_REAL;
		}
		ImageStack stack = model.getImages(); // in the stack you have the different images 
		int[] imageMax = model.getMaxImageData();
		int imageWidth = (int)(model.getImageWidth()/model.getPixelWidth());

		// get the selected spots 
		Vector spots = getSelectedSpots();

		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}

		// addition of spots parameters to each spot
		for (int i=0;i<columnsNames.length;i++)		{
			//System.out.println("Colonne "+i+" Type/Nom : "+columnsTypes[i]+"/"+columnsNames[i]);
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}

		// ------ memory and processing time -----------
		// some parameter can be set for once and used after in order to be more efficient
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight(); //height of one pixel in micrometers( for example 25)
		
		float[] fgTotal = new float[size];
		float[] bgTotal = new float[size];
		float[] fgMean = new float[size];
		float[] bgMean = new float[size];
		float[] fgMedian = new float[size];
		float[] bgMedian = new float[size];
		
		int nbFgPixels;
		int nbBgPixels;
		float distanceToCenter;
		float x;
		float y;

		// or in order not to recreate new object in the loop the object are created once and reinitialized for
		// each spot
		TSpot currentSpot;
		currentSpot =(TSpot) spots.elementAt(0);
		int spotWidth = (int)(currentSpot.getSpotWidth()/pixelWidth);
		int spotHeight = (int)(currentSpot.getSpotHeight()/pixelHeight);
		//System.out.println("spotWidth = "+spotWidth+" spotHeight = "+spotWidth);
		
		ImageProcessor ipSpot_16[] = new ImageProcessor[size];
		ImagePlus impSpot_16[] = new ImagePlus[size];
		for (int i=0;i<size;i++)
		{
			ipSpot_16[i] = new FloatProcessor(spotWidth, spotHeight, null, null);
			impSpot_16[i] = new ImagePlus("Signed 16 bit", ipSpot_16[i]);
		}
	
		
		float[][] colorSpot = new float [size][spotWidth*spotHeight];


		// This part in handling the GUI before the job 
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part

		/*
		 * defining kernel
		 */

		float[][] kernel;
		int kernelsize = (int)(currentSpot.getWidth()/pixelWidth);
		//System.out.println("kernel size before "+kernelsize);
		kernel = ImageTools.calcCircKernel(kernelsize/KERNEL_SIZE);
		kernelsize = kernel.length;

		// to retrieve the data of the images (colors in 16 bits and max image in 8 bits)
		ImageProcessor[] ip_16 = new ImageProcessor[size];
		for (int i=0;i<size;i++)
		{
			ip_16[i] = stack.getProcessor(i+1);
		}
		//System.out.println("Stack = "+stack.getSize());

		// determining the spot radius

		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i); //to get current spot 

			// to fill the spot image processors
			for (int j = 0; j < spotWidth; j++){
				for (int k = 0; k < spotHeight; k++){
					for (int l=0; l < size ;l++){

						colorSpot[l][k*spotWidth+j]= ip_16[l].getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
							
						//if ((ip_16green.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k)) > ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k)))){
						//	maxSpot[k*spotWidth+ j] = (int)ip_16green.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
						//}else{
						//	maxSpot[k*spotWidth+ j] = (int)ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
						//}
						//System.out.println("j = "+j+" k = "+k+" valueX "+ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight + k)));
					}
				}
			}

			// to create the imageProcessors and imagePlus
			for(int j=0;j<size;j++)
			{
				ipSpot_16[j]= new FloatProcessor(spotWidth, spotHeight, colorSpot[j], null);
				impSpot_16[j] = new ImagePlus(colors[j]+" spot", ipSpot_16[j]);
			}

			/*
			 * Retrieving the segmented image and largest particle features 
			 */
			double[] coords = computeSpotCenter(kernelsize, kernel, imageMax, imageWidth, currentSpot );
			ImagePlus  segementedImage = computeSegementedImage(kernelsize, kernel, imageMax, imageWidth, currentSpot );
			ImageProcessor segementedImageProcessor = segementedImage.getProcessor();
			//segementedImage.show();
			/*
			 * narrowing the diameter circle
			 */
			double diameter = pixelWidth;
			currentSpot.setComputedDiameter(diameter);
			currentSpot.updateDiameterWithComputed();

			/*
			 * displaying the outline of the particle
			 */

			if (coords[3] > 0.70){
				int value; 
				int valueNorth;
				int valueSouth;
				int valueEast;
				int valueWest;

				for (int j = 1; j < segementedImageProcessor.getWidth()-1; j++){
					for (int k = 1; k < segementedImageProcessor.getHeight()-1; k++){
						value = segementedImageProcessor.getPixel(j, k);
						valueNorth = segementedImageProcessor.getPixel(j, k-1);
						valueSouth = segementedImageProcessor.getPixel(j, k+1);
						valueEast = segementedImageProcessor.getPixel(j-1, k);
						valueWest = segementedImageProcessor.getPixel(j+1, k);
						if ((value != valueNorth) || (value != valueSouth) || 
								(value != valueEast) || (value != valueWest)){
							//System.out.println(currentSpot.getX()+" "+currentSpot.getY()+" "+j+" :"+k);
							((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotSpot(
									new TImageGraphicPanel.HotSpot(Color.white, (int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight)+k));
						}

					}
				}

			}

			// get the quantification values for the spots
			for (int j=0;j<size;j++)
			{
				fgTotal[j] = 0;
				bgTotal[j] = 0;
				fgMean[j] = 0;
				bgMean[j] = 0;
				fgMedian[j] = 0;
				bgMedian[j] = 0;
			}

			nbFgPixels = 0;
			nbBgPixels = 0;
			int inside = 0;

			if (coords[3] > 0.7){

				for (int j = 0; j < spotWidth; j++){
					for (int k = 0; k < spotHeight; k++){
						//System.out.println(" j "+j+" k "+k+" : "+segementedImageProcessor.getPixel(j, k));
						if (segementedImageProcessor.getPixel(j, k) == 0){
							inside++;
							for(int l=0;l<size;l++){
								fgTotal[l] = fgTotal[l] + ipSpot_16[l].getPixelValue(j, k);
							}
							nbFgPixels++;
						}else{
							for(int l=0;l<size;l++){
								bgTotal[l] = bgTotal[l] + ipSpot_16[l].getPixelValue(j, k);
							}
							nbBgPixels++;
						}
					}
				}

				/*
				 * Calculate Medians 
				 */

				float[][] fgPixels =  new float[size][nbFgPixels];
				float[][] bgPixels =  new float[size][nbBgPixels];

				nbFgPixels =0;
				nbBgPixels = 0;

				for (int j = 0; j < spotWidth; j++){
					for (int k = 0; k < spotHeight; k++){
						if (segementedImageProcessor.getPixel(j, k) == 0){
							for (int l=0;l<size;l++)
							{
								fgPixels[l][nbFgPixels] = ipSpot_16[l].getPixelValue(j, k);
							}
							nbFgPixels++;
						}else{
							for (int l=0;l<size;l++)
							{
								bgPixels[l][nbBgPixels] = ipSpot_16[l].getPixelValue(j, k);
							}
							nbBgPixels++;
						}
					}
				}

				for(int j=0;j<size;j++){
					fgMean[j] = fgTotal[j]/nbFgPixels;
					bgMean[j] = bgTotal[j]/nbBgPixels;
				}

				/*
				 * Median calculation 
				 */

				for(int j=0;j<size;j++)
				{
					fgMedian[j] = getMedian(fgPixels[j]);
					bgMedian[j] = getMedian(bgPixels[j]);;
				}
			}

			// just to have columns !!
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[j], new Double(fgTotal[j]), true);
			}
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[size+j], new Double(bgTotal[j]), true);
			}
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[size*2+j], new Double(fgMean[j]), true);
			}
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[size*3+j], new Double(bgMean[j]), true);
			}
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[size*4+j], new Double(fgMedian[j]), true);
			}
			for (int j=0;j<size;j++)
			{
				currentSpot.addParameter(columnsNames[size*5+j], new Double(bgMedian[j]), true);
			}

			// This part in handling the GUI during the job
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
			}
			// ---- end of the GUI part
		}

		// This part in handling the GUI after the job 	
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part 
	}

	public float getMedian(float pixels[]) {
		float median = 0;
		//sort the array in ascending order of the values provided in the distribution
		Arrays.sort(pixels);
		//for(int l=0; l<bgRedPixels.length; l++)
		//System.out.println(l+" l "+bgRedPixels[l]);
		if((pixels.length%2) == 0)  //sample length is even
			median = (pixels[pixels.length/2-1] + pixels[pixels.length/2]) / 2;
		else  //sample length is odd
			median = pixels[pixels.length/2];
		//System.out.println("median"+median);	
		return median;
	}

	/**
	 * Returns the segmented spot image 
	 * @param kernelsize
	 * @param kernel
	 * @param imageMax
	 * @param imageWidth
	 * @param spot
	 * @return
	 */
	public ImagePlus computeSegementedImage(int kernelsize, float[][] kernel , int[] imageMax, double imageWidth,  TSpot spot) {
		TGridModel model = spot.getModel();
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight();
		int spotWidth = (int)((spot.getSpotWidth()/pixelWidth)+kernelsize);
		int spotHeight = (int)((spot.getSpotHeight()/pixelHeight)+kernelsize);
		int xSpotPosition = (int)((spot.getX()/pixelWidth)-kernelsize/2);
		int ySpotPosition = (int)((spot.getY()/pixelHeight)-kernelsize/2);

		float[][] origimg = new float[spotWidth][spotHeight];
		for (int j = 0; j < spotWidth; j++){
			for (int k = 0; k < spotHeight; k++){
				//spotData[k*spotWidth+ j] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];
				origimg[j][k] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];	
			}
		}

		/**
		 * Template matching  
		 */  	

		float[][] corrimg = null;
		corrimg = new float[spotWidth][spotHeight];
		corrimg = ImageTools.statsCorrelation(origimg, kernel);
		float[] outData = new float[corrimg.length*corrimg[0].length];
		for (int k=0; k < corrimg.length; k++){
			for (int l=0; l < corrimg[0].length; l++){
				outData[l*corrimg.length+k] = (corrimg[k][l]*32167)+32167;
			}
		}
		ImageProcessor outSpot = new FloatProcessor(corrimg.length, corrimg[0].length, outData, null);
		ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);

		outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();
		outSpot.autoThreshold();
		outimpSpot = new ImagePlus("correlation Image", outSpot);
		/**
		 * Thresholding  
		 */  
		
		outSpot.autoThreshold();
		ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
		ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,512+8192+255, myrt, 0, pixelWidth*pixelHeight);
		mypart.analyze(outimpSpot);
		String myhead = myrt.getColumnHeadings();

		double[] coords = new double[5];
		coords[0] = myrt.getCounter();

		if (myrt.getCounter() > 0){  
			/*
			 * Retreiving the index of the largest particle
			 */
			int maxAreaNum = 0;
			double maxArea = 0;  
			int nbParticles = myrt.getCounter();
			for (int m = 0; m < nbParticles; m++){
				if (myrt.getColumn(ij.measure.ResultsTable.AREA)[m]> maxArea){
					maxArea = myrt.getColumn(ij.measure.ResultsTable.AREA)[m];
					maxAreaNum = m;
				}
			}

			//System.out.println(" largest particle = "+maxAreaNum+"nb particles = "+nbParticles);

			for (int m = 0; m < nbParticles; m++){
				if (m != maxAreaNum){
					//System.out.println(" "+myrt.getColumn(ij.measure.ResultsTable.ROI_X)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_Y)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_WIDTH)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_HEIGHT)[m]);
					outimpSpot = particleCleaning(outimpSpot, (int)myrt.getColumn(ij.measure.ResultsTable.ROI_X)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_Y)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_WIDTH)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_HEIGHT)[m]);
				}
			}
			/*
			 * Retreiving the values corresponding the largest particle
			 */
			if (myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[maxAreaNum] > 0.7){
				//System.out.println(" Y = "+myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0]);
				coords[1] = (spot.getSpotWidth()/pixelWidth/2)- myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[maxAreaNum];
				coords[2] = (spot.getSpotHeight()/pixelHeight/2) - myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[maxAreaNum];
				coords[3] = myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[maxAreaNum]; 
				coords[4] = myrt.getColumn(ij.measure.ResultsTable.AREA)[maxAreaNum];
			}else{
				coords[1] = coords[2] = coords[3] = coords[4] = 0;
			}
		}else{
			coords[1] = coords[2] = coords[3] = coords[4] = 0;
		}

		return outimpSpot; 

	}
	/**
	 * Returns the number of particles and the X,Y coordinates, the area and the circularity of the 
	 * largest particle
	 * @param kernelsize
	 * @param kernel
	 * @param imageMax
	 * @param imageWidth
	 * @param spot
	 * @return
	 */
	public double[] computeSpotCenter(int kernelsize, float[][] kernel , int[] imageMax, double imageWidth,  TSpot spot) {
		TGridModel model = spot.getModel();
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight();
		//int spotWidth = (int)((spot.getSpotWidth()+spot.getInitDiameter())/pixelWidth);
		//int spotHeight = (int)((spot.getSpotHeight()+spot.getInitDiameter())/pixelHeight);
		int spotWidth = (int)((spot.getSpotWidth()/pixelWidth)+kernelsize);
		int spotHeight = (int)((spot.getSpotHeight()/pixelHeight)+kernelsize);
		int xSpotPosition = (int)((spot.getX()/pixelWidth)-kernelsize/2);
		int ySpotPosition = (int)((spot.getY()/pixelHeight)-kernelsize/2);

		//System.out.println("xSpotPosition "+xSpotPosition+" ySpotPosition "+ySpotPosition);
		//System.out.println("spotWidth "+spotWidth+" spotHeight "+spotHeight);
		//System.out.println("kernel length "+(int)(kernelsize/2));
		//System.out.println("kernel info "+kernel.length+" - "+kernel[0].length);

		//float[] spotData = new float[spotWidth*spotHeight];
		float[][] origimg = new float[spotWidth][spotHeight];
		for (int j = 0; j < spotWidth; j++){
			for (int k = 0; k < spotHeight; k++){
				//spotData[k*spotWidth+ j] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];
				origimg[j][k] = imageMax[(int)((ySpotPosition+k)*imageWidth+j+xSpotPosition)];	
			}
		}

		//FloatProcessor ipSpot = new FloatProcessor(spotWidth, spotHeight, spotData, null);
		//ImagePlus impSpot = new ImagePlus("extracted Spot Image", ipSpot);

		//impSpot.show();

		/**
		 * Template matching  
		 */  	

		float[][] corrimg = null;
		corrimg = new float[spotWidth][spotHeight];
		//System.out.println("corrimg before "+corrimg.length+" - "+corrimg[0].length);
		corrimg = ImageTools.statsCorrelation(origimg, kernel);
		//ImagePlus imp = new ImagePlus();
		//ij.process.ImageProcessor ip2 = imp.getProcessor().createProcessor(corrimg.length,corrimg[0].length);

		//System.out.println("corrimg after "+ corrimg.length+" "+corrimg[0].length);
		float[] outData = new float[corrimg.length*corrimg[0].length];
		for (int k=0; k < corrimg.length; k++){
			for (int l=0; l < corrimg[0].length; l++){
				outData[l*corrimg.length+k] = (corrimg[k][l]*32167)+32167;
			}
		}
		ImageProcessor outSpot = new FloatProcessor(corrimg.length, corrimg[0].length, outData, null);
		ImagePlus outimpSpot = new ImagePlus("correlation Image", outSpot);

		//outimpSpot.show();

		outSpot.invert();
		new ij.process.ImageConverter(outimpSpot).convertToGray8();
		outSpot = outimpSpot.getProcessor();

		/**
		 * Thresholding  
		 */  

		outSpot.autoThreshold();
		ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
		ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,512+8192+255, myrt, 0, pixelWidth*pixelHeight);
		mypart.analyze(outimpSpot);
		String myhead = myrt.getColumnHeadings();

		double[] coords = new double[5];
		coords[0] = myrt.getCounter();

		if (myrt.getCounter() > 0){  
			/*
			 * Retreiving the index of the largest particle
			 */
			int maxAreaNum = 0;
			double maxArea = 0;  
			int nbParticles = myrt.getCounter();
			for (int m = 0; m < nbParticles; m++){
				if (myrt.getColumn(ij.measure.ResultsTable.AREA)[m]> maxArea){
					maxArea = myrt.getColumn(ij.measure.ResultsTable.AREA)[m];
					maxAreaNum = m;
				}
			}

			//System.out.println(" largest particle = "+maxAreaNum+"nb particles = "+nbParticles);

			for (int m = 0; m < nbParticles; m++){
				if (m != maxAreaNum){
					//System.out.println(" "+myrt.getColumn(ij.measure.ResultsTable.ROI_X)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_Y)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_WIDTH)[m]+" "+myrt.getColumn(ij.measure.ResultsTable.ROI_HEIGHT)[m]);
					outimpSpot = particleCleaning(outimpSpot, (int)myrt.getColumn(ij.measure.ResultsTable.ROI_X)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_Y)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_WIDTH)[m], (int)myrt.getColumn(ij.measure.ResultsTable.ROI_HEIGHT)[m]);
				}
			}
			/*
			 * Retreiving the values corresponding the largest particle
			 */
			if (myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[maxAreaNum] > 0.7){
				//System.out.println(" Y = "+myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0]);
				coords[1] = (spot.getSpotWidth()/pixelWidth/2)- myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[maxAreaNum];
				coords[2] = (spot.getSpotHeight()/pixelHeight/2) - myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[maxAreaNum];
				coords[3] = myrt.getColumn(ij.measure.ResultsTable.CIRCULARITY)[maxAreaNum]; 
				coords[4] = myrt.getColumn(ij.measure.ResultsTable.AREA)[maxAreaNum];
			}else{
				coords[1] = coords[2] = coords[3] = coords[4] = 0;
			}
		}else{
			coords[1] = coords[2] = coords[3] = coords[4] = 0;
		}

		return coords; 

	}
	ImagePlus particleCleaning( ImagePlus IPlus, int StartX, int StartY, int X, int Y  )	{

		// Declaration
		int DX,DY;
		ImageProcessor IP;
		IP = IPlus.getProcessor();

		// On parcourt toute l'image
		for(DX = StartX; DX < StartX+X; DX++)
		{
			for(DY = StartY; DY < StartY+Y; DY++)
			{							
				//	cleans the pixels ( en mettant sa valeur ` 255 )
				IP.putPixel(DX, DY, 255);

			}// end for
		}// end for

		IPlus.setProcessor(IPlus.getTitle(), IP);		

		//ImageProcessor IP2 = IPlus.getProcessor().duplicate();
		//ImagePlus IMP2 = new ImagePlus( "toto",IP2);
		//IMP2.show();

		return IPlus;

	}
}