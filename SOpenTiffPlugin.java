/*
 * Created on 8th April 2008
 * The SOpenTiffPlugin allow to open a tiff compress or not
 * LZW compression only
 * 
 * @author Alexis JULIN
 * @see Tplugin
 * @see Plugin ImageJ=>package net.*;
 */
import java.awt.event.ActionEvent;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Vector;

import javax.swing.JFileChooser;


import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.plugins.TPlugin;
import agscan.menu.action.TAction;
import agscan.data.TDataManager;
import agscan.dialog.TFileImageChooser;
import agscan.event.TEvent;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import ij.io.FileInfo;
import ij.io.TiffDecoder;

import net.sf.ij.jaiio.*;
import net.sourceforge.jiu.codecs.*;
import net.sourceforge.jiu.codecs.tiff.*;
import net.sourceforge.jiu.ops.*;


public class SOpenTiffPlugin extends TPlugin {

	
	public SOpenTiffPlugin() {
		//TODO externalize
		super("Open a multi tif",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets when the menu is active 		
	}
	
	public void exec(){
		
		
		String adresse = askTifAdresse();
		System.out.println(adresse);


		//chargement du multi tif
		ImagePlus tab[]=loadTiff(adresse);

		if(tab.length==0){
			System.out.println("Probleme venant du Plugin ImageIO");
		}
		else{
			for(int i=0;i<tab.length;i++){
				System.out.println(tab[i].getTitle());

			}


			if(tab.length > 1){
				//determination des longueurs d'ondes methode 1
				TiffDecoder td;
				FileInfo ti[];
				String nom = null;
				String dir = null;
				dir = adresse.substring(0, adresse.lastIndexOf('/')+1);
				nom=adresse.substring(adresse.lastIndexOf('/')+1, adresse.length());


				td=new TiffDecoder(dir,nom);
				try{
					ti = td.getTiffInfo();
					
					System.out.println("Recuperation des longueurs d'ondes\n");
					for(int i=0;i<ti.length;i++)
						System.out.println("Longueur d'onde "+i+"  = "+ti[i].description);
					System.out.println("\n");
				}
				catch(IOException ioe){
					System.out.println("Impossible de lire les informations dans le fichier tif "+adresse);
				}
//				
				//determination des longueurs d'ondes methode 2
//				TIFFCodec tc=new TIFFCodec();
//				try{
//					//System.out.println("etape 1 reussie");
//					tc.setFile(adresse, CodecMode.LOAD);
//					//System.out.println("etape 2 reussie");
//					tc.process();
//					//System.out.println("etape 3 reussie");
//					tc.readImageFileDirectory();
//					
//				}
//
//				catch(IOException e){
//					System.out.println("Erreur IO");
//				}
//				catch(UnsupportedCodecModeException e){
//					System.out.println("Erreur CodecMode");
//				}
//				catch(MissingParameterException msp){
//					System.out.println("Erreur il manque un param�tre");
//				}
//				catch(OperationFailedException ofe){
//					System.out.println("Erreur operation echou�e");
//				}


			}


			//sauvegarde des images
			saveTiffs(tab);



			//affichage
			switch(tab.length){
			case 1 :{
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null,new File(tab[0].getTitle()));
				TEventHandler.handleMessage(event);
				break;
			}

			case 2 :{
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, new File(tab[0].getTitle()),new File(tab[1].getTitle()));
				TEventHandler.handleMessage(event);
				break;				

			}
			case 3 :{
				TEvent event = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.OPEN_IMAGE, null, new File(tab[0].getTitle()),new File(tab[1].getTitle()), new File(tab[2].getTitle()));
				TEventHandler.handleMessage(event);
				break;
			}
			default : System.out.println("Impossible d'ouvrir plus de 3 canaux");

			}
		}
		
		
	}
	
	
	private static void saveTiffs ( ImagePlus [] tab ){
		
		//sauvegarde des images decompress�es et d�compos�es
		JAIWriter jw=new JAIWriter();
		try{
			//ecriture du fichier
			for(int i=0;i<tab.length;i++){
				jw.write(tab[i].getTitle(),tab[i] );
				System.out.println("Ecriture reussie");
			}
			System.out.println("Sauvegarde des images termin�es");
		}
		catch(FileNotFoundException fnfe){
			System.out.println("Probleme fichier non trouv� ");
		}
		catch(IOException ioe){
			System.out.println("Impossible d'ecrire dans le fichier");
		}
		
	}
	
	private static ImagePlus[] loadTiff(String adress){
		int w=0;
		int h=0;
		FileInfo info;
		
		
		//chargement du tiff
		ImagePlus[] tab=null;
		Vector buff;
		ImagePlus im;
		try{
			tab = JAIReader.read(new File(adress) ,null);

			//cas ou read renvoie une stack =>plusieurs images au caracteristiques identiques
			if(tab.length==1){
				im=tab[0];
				
				if(tab[0].getStackSize() != 1){
					System.out.println("Cas d'un multi tif");
					tab = stackToImages(im,adress);
				}
			}
			//cas ou ya un pb de dimension
			else{
				Vector v=new Vector();
				//recherche des dimensions max qui correspondent aux bonnes images
				for(int i=0;i<tab.length;i++){
					info=tab[i].getFileInfo();
					if(info.width>w)
						w=info.width;
					if(info.height>h)
						h=info.height;
				}
				//on stocke les bonnes images
				for(int i=0;i<tab.length;i++){
					info=tab[i].getFileInfo();
					if((info.width == w) && (info.height == h))
						v.add(tab[i]);
				}
				//on repasse sous la forme de tableau
				tab=null;
				tab=new ImagePlus[v.size()];
				int no=65;
				for(int i=0;i<v.size();i++){
					tab[i]=(ImagePlus)v.elementAt(i);
					String s=tab[i].getTitle();
					System.out.println(s);
					tab[i].setTitle(createName(adress,s,no+i,true));
				}

			}			
			//A ce niveau dans tous les cas on poss�de un tableau d'ImagePlus contenant des images
			//correctes
			//Il faut maintenant les afficher
			for(int i=0;i<tab.length;i++)
				System.out.println(tab[i].getFileInfo().toString());
		}
		catch(Exception e){
			System.out.println("Excpetion non g�r� dans le PlugIn ImageJ");
		}
		
		return tab;
		
		
	}
	
	
	//renvoi l'adresse absolue du fichier tif selectionn� 
	private static String askTifAdresse() {
			String adresse="";
			//TODO externalize
			TFileImageChooser chooser = new TFileImageChooser();
			chooser.addChoosableFileFilter(new ImageFilter("tif", "TIF File (*.tif)"));
			int returnVal = chooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) 
				adresse = chooser.getSelectedFile().getAbsolutePath();
			return adresse;
	}
	
	
	
	
	//Split une stack en un tableau d'ImagePlus => Les dim sont bonnes
	//mais images obtenues non v�rifi�es
	private static ImagePlus[] stackToImages(ImagePlus imp,String adress){
		
		ImagePlus [] tab=null;

		String sLabel = imp.getTitle();
        String sImLabel = "";
        ImageProcessor ip,newip;

        
        ImageStack stack = imp.getStack();

        int sz = stack.getSize();
        tab=new ImagePlus[sz];
        

        int currentSlice = imp.getCurrentSlice();  // to reset ***
        
        int m=64+sz;//title
        
        


        for(int n=1;n<=sz;++n) {
        	imp.setSlice(n);   // activate next slice ***

            // Get current image processor from stack.  What ever is
            // used here should do a COPY pixels from old processor to
            // new. For instance, ImageProcessor.crop() returns copy.
            ip = imp.getProcessor(); // ***
            newip = ip.createProcessor(ip.getWidth(),ip.getHeight());
            newip.setPixels(ip.getPixelsCopy());
            newip.invert();
            // Create a suitable label, using the slice label if possible
            sImLabel = imp.getStack().getSliceLabel(n);
            if (sImLabel == null || sImLabel.length() < 1) {
            	
            	sImLabel = createName(adress,sLabel,m,false);
            }

            // Create new image corresponding to this slice.
            ImagePlus im = new ImagePlus(sImLabel, newip);
            im.setCalibration(imp.getCalibration());

            // Stocke l'image
            tab[n-1]=im;
            m--;
        }

        // Reset original stack state.
        imp.setSlice(currentSlice); // ***
        if(imp.isProcessor()) {
            ip = imp.getProcessor();
            ip.setPixels(ip.getPixels()); //***
        }
        imp.setSlice(currentSlice);

		return tab;
	}
	
	//cree un nom utilisable pour chaque image
	private static String createName(String adresse,String origine,int no,boolean compress){
		//origine de la forme XXXXXX.tif
		String nom=null;
		String part0=null;
		String part1=null;
		String part2=null;
		if(!compress){
			part0=adresse.substring(0, adresse.length()-origine.length());
			part1=origine.substring(0,origine.length()-4);
			part2=origine.substring(origine.length()-4,origine.length());
		}
		else{
			part0=adresse.substring(0, adresse.length()-origine.length()+6);
			part1=origine.substring(0,origine.length()-10);
			part2=origine.substring(origine.length()-10,origine.length()-6);
		}
			
		nom=part0+part1+"_"+(char)no+part2;
		
		return nom;
		
	}
	

	
	
	public void actionPerformed(ActionEvent ae) {
		
			this.exec();

	}
	
	
}
