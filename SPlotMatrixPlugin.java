
/*
 * Created on 8th December 2005
 * The SPlotMatrixPlugin makes cross plots of several experiments (minimum two)
 * It was developped by Fabrice Lopez for BZScan2
 * and adapted by Remi Cathelin in order to become an AGScan Plugin 
 * @version 2005/12/08
 * @author Fabrice Lopez
 * @author Remi Cathelin
 * @see TPlugin
 */

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.ioxml.TDefaultFileChooser;
import agscan.menu.action.TAction;
import agscan.plugins.TPlugin;

public class SPlotMatrixPlugin extends TPlugin{
////	public static int id = -1;
	
	public SPlotMatrixPlugin() {
		super("Plots n BZScan files",null);// TPlugin => TAction
		id = id0;//creation of the next id, using tjhe current value of id0
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets the menu always active 
	}
	public void exec() {
		File[] files = askQtfFiles();
		if (files != null) {
			String name = files[0].getPath();
			String path = name.substring(0, name.lastIndexOf(File.separator));
			if (name.lastIndexOf(File.separator) != -1)
				name = name.substring(0, name.lastIndexOf(File.separator));
			name = name.substring(name.lastIndexOf(File.separator) + 1) + "_plots";
			TRBCPlotsDialog dialog = new TRBCPlotsDialog(path, name);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			dialog.pack();
			dialog.setLocation((screenSize.width - dialog.getWidth()) / 2, (screenSize.height - dialog.getHeight()) / 2);
			dialog.setVisible(true);
			if (dialog.getOkOut()) {
				TPlotArrayWorker paw = new TPlotArrayWorker(files, dialog.getDir() + File.separator + dialog.getName(), dialog.getLinear(), dialog.getLog());
				paw.start();
			}
		}
	}
	
	private File[] askQtfFiles() {
		File[] files = null;
		boolean ok = false;
		TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Ouvrir des alignements");
		chooser.setMultiSelectionEnabled(true);
		chooser.setCurrentDirectory(new File(AGScan.prop.getProperty("alignmentsPath")));
		chooser.addChoosableFileFilter(new ImageFilter("zaf", "Alignements (*.zaf)"));
		while (!ok) {
			int returnVal = chooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				files = chooser.getSelectedFiles();
				AGScan.prop.setProperty("alignmentsPath",chooser.getCurrentDirectory().getAbsolutePath());
				if (files.length < 2) {
					JOptionPane.showMessageDialog(null, "Vous devez choisir plusieurs fichiers !!!");
					files = null;
				}
				else
					
					ok = true;
			}
			else
				ok = true;
		}
		return files;
	}
	
	public void close() {
		
	}
	public void update() {
		
	}
	
	public void actionPerformed(ActionEvent arg0) {
		exec();	
	}
	/* (non-Javadoc)
	 * @see agscan.plugins.TPlugin#getMenuId()
	 */
	////public int getMenuId() {
		// TODO Auto-generated method stub
	////	return id;
	////}
}
