/* Created on Oct 19, 2005
 * 
 * 
 * This class is a quantification plugin based on Magic Tool software.
 * http://www.bio.davidson.edu/projects/magic/magic.html
 * This quantification plugin is working only for two channels images.
 * This quantification is based on a 6 pixels radius fixed circle for each rectangular spot region.
 * Results displayed into the application are : "FG Total Red" for the foreground red signal
 * "FG Total Green" for the foreground green signal and "Green / Red", the ratio green/red.
 * Notice that in this plugin other results are computed and available (but not displayed)(see GeneData class).
 * We can easily modify parameters of the radius.
 * We can also choose an other "Magic Tool quantification" (adaptative circle or seeded region).
 * An improvement could be the integration of the "Magic Tool" display panel which give users the choice of parameters.
 *   
 * Note: this plugin needs classes from package magictool.image.
 * You need to put this package into the plugins directory.	
 * 
 * @version 2005/10/20
 * @author rcathelin
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * @see magictool.image.GeneData;
 * 
 */

import ij.*;
import ij.plugin.filter.GaussianBlur;
import ij.process.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;
import java.awt.Toolkit;

import javax.swing.JOptionPane;

import plugins.ArrayDisplay;
import plugins.ImageTools;

import magictool.image.GeneData;
import magictool.image.SingleGeneImage;
import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SSampleQuantification extends SQuantifPlugin {
	
	String[] columnsNames = new String[12];//array of columns created by the quantification
	int[] columnsTypes = new int[12];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	ArrayDisplay kernelAd;
	
	//public static int id = -1;
	
	public SSampleQuantification() {
		
		super("Fluo image analysis", null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		setImagesMin(2);
		setImagesMax(2);
		//declaration of columns of the plugin
		//name of quantification columns
		columnsNames[0]= "Area";
		columnsNames[1]= "Mean";
		columnsNames[2]= "StdDev";
		columnsNames[3]= "Mode";
		columnsNames[4]= "Min";
		columnsNames[5]= "Max";
		columnsNames[6]= "local X";
		columnsNames[7]= "local Y";
		columnsNames[8]= "XM";
		columnsNames[9]= "YM";
		columnsNames[10]= "Perim";
		columnsNames[11]= "Nb spots";
		//type of quantification results
		columnsTypes[0] = TColumn.TYPE_REAL;
		columnsTypes[1] = TColumn.TYPE_REAL;
		columnsTypes[2] = TColumn.TYPE_REAL;
		columnsTypes[3] = TColumn.TYPE_REAL;
		columnsTypes[4] = TColumn.TYPE_REAL;
		columnsTypes[5] = TColumn.TYPE_REAL;
		columnsTypes[6] = TColumn.TYPE_REAL;
		columnsTypes[7] = TColumn.TYPE_REAL;
		columnsTypes[8] = TColumn.TYPE_REAL;
		columnsTypes[9] = TColumn.TYPE_REAL;
		columnsTypes[10] = TColumn.TYPE_REAL;
		columnsTypes[11] = TColumn.TYPE_REAL;
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * run of the quantification 
	 */
	public void run() {				
		
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages();
		int[] histogrameData = alignment.getImage().getHistogramData();
		Vector spots = getSelectedSpots();
		//Vector spots = (Vector) alignment.getGridModel().getSpots();
		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		// histogram info
		double histogramTotal = 0;
		for (int i=0;i<histogrameData.length;i++){
			histogramTotal =histogramTotal + i*histogrameData[i];
			//System.out.println("histogram "+i+" "+histogrameData[i]);
		}

		//addition of spots parameters
		for (int i=0;i<columnsNames.length;i++)		{
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}
		
		// object and variable definition
		int imageWidth =  (int) model.getImageWidth();// width in micrometers of the image
		int pixelWidth = (int) model.getPixelWidth();//width 
		int pixelHeight = (int) model.getPixelHeight();//height of one pixel in micrometers( for example 25)
		int imageWidthInPix = imageWidth/pixelWidth;//height in pixels of the image 
		
		double imagePixelSize = (model.getImageWidth()*model.getImageHeight())/(pixelWidth*pixelHeight);
		double imageMean = 	histogramTotal/imagePixelSize;
		System.out.println("image mean "+imageMean);
		
		TSpot currentSpot;
		double val = 0;
		double val2 = 0;
		Point2D.Double point;		
		
		double rectangleWidth = 0;
		double rectangleHeight = 0;
		int nbPixelsW = 0;
		int nbPixelsH = 0;
		Vector vec = null;
		int[] spotPixelsRed = null;
		short[] spotPixelsMax = null;
		int[] spotPixelsGreen = null;
		Enumeration enu_pixels = null;
		int j=0;
		int m =0;
		int n = 0;
		int val_int = 0;
		float[] xm = new float[100];
		float[] ym = new float[100];
		float[][] origimg = null;
		float[][] corrimg = null;
		float[] showimg = null;
		int kernelSize;
		float[][] kernel;
		int kernelPart = 6; // used to split the spot zone to determine the kernel size
		float spotMean = 0;
		
		
		Object params_fixed[]=null;
		params_fixed = new Object[1];
		Object params_adaptive[]=null;
		params_adaptive = new Object[3];
		Object params_seeded[]=null;
		params_seeded = new Object[1];
		
		GeneData gd = null;
		
		// UI management
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		
		int[] dataRed_8bits = ImagePlusTools.getImageData(model.getChannelImage(1),16);
		int[] dataGreen_8bits = ImagePlusTools.getImageData(model.getChannelImage(2),16);
		
		currentSpot =(TSpot) spots.firstElement();
		System.out.println("spot info "+currentSpot.getRectangle().getWidth()+" "+currentSpot.getRectangle().getHeight());
		// Template matching : kernel generation 
		kernelSize = (int) ((currentSpot.getRectangle().getWidth()/pixelWidth)/kernelPart)-1;
		//kernel = ImageTools.gaussianKernel(2* kernelSize + 1, 2* kernelSize + 1);	
		kernel = ImageTools.calcCircKernel(kernelSize);
		//displayKernel(kernel);
		
		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i);//current spot 
			//we search the number of pixels into height and width of the rectangle representing the spot
			rectangleWidth =  currentSpot.getRectangle().getWidth();
			rectangleHeight =  currentSpot.getRectangle().getHeight();
			nbPixelsW = (int) ((rectangleWidth/pixelWidth)+1);
			nbPixelsH = (int) ((rectangleHeight/pixelHeight)+1);
			
			//get pixels contained into the rectangle of the spots (we give pixel h and w = 25 for example)
			//vec = currentSpot.getRectanglePixels(currentSpot.getModel().getPixelWidth(),currentSpot.getModel().getPixelHeight());
			vec = currentSpot.getRectanglePixels(pixelWidth,pixelHeight);
			spotPixelsRed = new int[vec.size()];	//pixels of the spot= the rectangle
			spotPixelsGreen = new int[vec.size()];	//pixels of the spot= the rectangle	
			spotPixelsMax = new short[vec.size()]; //pixels of the image used for spot quality
			enu_pixels = vec.elements();
			
			j=0;
			origimg = new float[nbPixelsW+kernelSize*2][nbPixelsH+kernelSize*2];
			showimg = new float[origimg.length*origimg[0].length];
			
			for (int k=0; k < origimg.length; k++){
				for (int l=0; l < origimg[0].length; l++){
					origimg[k][l] = (float) imageMean;
				}
			}
			
			while (enu_pixels.hasMoreElements()) {
				point = (Point2D.Double)enu_pixels.nextElement();				
				try {
					//RED IMAGE
					m = j / nbPixelsW;
					n = j % nbPixelsW;
					val = dataRed_8bits[(int)point.getY() * imageWidthInPix + (int)point.getX()];
					//val_int 
					//val_int = (int)val  & 0xFFFF ;// modulo  for range -128..127 ==> 0..255
					//spotPixelsRed[j]=val_int;
					//GREEN IMAGE
					val2 = dataGreen_8bits[(int)point.getY() * imageWidthInPix + (int)point.getX()];
					/*val_int = (int)val  & 0xFFFF ;// modulo  for range -128..127 ==> 0..255
					spotPixelsGreen[j]=val_int;*/
					if (val > val2 ){
						spotPixelsMax[j] = (short)val;
					} else {
						spotPixelsMax[j] = (short)val2;
					}
					if ((float)spotPixelsMax[j] > 0){
						origimg[m+kernelSize][n+kernelSize] = (float)spotPixelsMax[j];
					}else{
						origimg[m+kernelSize][n+kernelSize] = 65536 + (float)spotPixelsMax[j];
					}
					//origimg[m][n] = (float)spotPixelsMax[j];
					//showimg[j] = (float)spotPixelsMax[j];
					//System.out.println("m n val = "+m+" "+n+" ** "+(float)spotPixelsMax[j]);
					j++;
				}
				catch (Exception ex) {
					val = -1;
				}			
			}
			
			j = 0;
			for (int k=0; k < origimg.length; k++){
				for (int l=0; l < origimg[0].length; l++){
					showimg[j] =  origimg[k][l];
					j++;
				}
			}	
			
			// data quantification 
			ImageProcessor ip = new FloatProcessor(origimg.length, origimg[0].length, showimg, null);
			ImagePlus imp = new ImagePlus("Signed 16 bit", ip);
			
			//String fileName = "/tmp/file1.tiff";		
			//new ij.io.FileSaver(imp).saveAsTiff(fileName); 
			

			//imp.show();
			//imp2.show();
			corrimg = new float[pixelWidth][pixelHeight];
			corrimg = ImageTools.statsCorrelation(origimg, kernel);
			ij.process.ImageProcessor ip2 = imp.getProcessor().createProcessor(corrimg.length,corrimg[0].length);
		
			
			System.out.println("corre image = "+ corrimg.length+" "+corrimg[0].length);
			
			for (int k=0; k < corrimg.length; k++){
				for (int l=0; l < corrimg[0].length; l++){
					int value = (int)(corrimg[k][l]*32167)+32167;
					//int value = (int)(corrimg[k][l]);
					//System.out.println("k l ori  cor ("+k+" -"+l+" - "+corrimg[k][l]+" "+value+")");
					//System.out.println("orimage i j value ("+k+" -"+l+" - "+);
					ip2.putPixelValue(k,l,value);
				}
			}

			//fileName = "/tmp/file2.tiff";	
			ij.ImagePlus imp2 = new ij.ImagePlus("Before template matching", ip2);
			//new ij.io.FileSaver(imp2).saveAsTiff(fileName);		

			//ImageProcessor ip2 = imp2.getProcessor();
			ip2.invert();
			//ij.plugin.filter.GaussianBlur myblur = new ij.plugin.filter.GaussianBlur();
			//ij.ImagePlus imp2 = new ij.ImagePlus("Template matching+8bits", ip2);
			//ij.ImagePlus imp3 = new ij.ImagePlus("8bits Image"+i, ip2);
			new ij.process.ImageConverter(imp2).convertToGray8();


			
			//myblur.blur(ip2, (double)nbPixelsW/5);
			//myblur.blur(ip2, (double)nbPixelsW/5);
			ip2 = imp2.getProcessor();
			ip2.autoThreshold();
			ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
			ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(0,255, myrt, (pixelWidth*pixelHeight/20), (pixelWidth*pixelHeight/1.8));
			mypart.analyze(imp2);
			String myhead = myrt.getColumnHeadings();

			//fileName = "/tmp/file3.tiff";		
			//new ij.io.FileSaver(imp2).saveAsTiff(fileName);
			
			//System.out.println("heading "+myhead);
			//xm = myrt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS);
			//ym = myrt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS);
			
			//here spotPixels contains all 8-bits values of the spot for the 2 channels
			//For this spot, we compute a quantification with its tab of pixels values 
			

			// results output ----------------------------------
			if (myrt.getCounter() == 0){
				for (int k = 0; k < columnsNames.length; k++){
					currentSpot.addParameter(columnsNames[k], new Double(0), true);
				}
			}else{
				if (myrt.getCounter() > 1){
					for (int k = 0; k < columnsNames.length-1; k++){
						currentSpot.addParameter(columnsNames[k], new Double(999999), true);
					}	
					currentSpot.addParameter(columnsNames[columnsNames.length-1], new Double(myrt.getCounter()), true);
				}else {
				currentSpot.addParameter(columnsNames[0], new Double(myrt.getColumn(ij.measure.ResultsTable.AREA)[0]), true);
				currentSpot.addParameter(columnsNames[1], new Double(myrt.getColumn(ij.measure.ResultsTable.MEAN)[0]), true);
				currentSpot.addParameter(columnsNames[2], new Double(myrt.getColumn(ij.measure.ResultsTable.STD_DEV)[0]), true);
				currentSpot.addParameter(columnsNames[3], new Double(myrt.getColumn(ij.measure.ResultsTable.MODE)[0]), true);
				currentSpot.addParameter(columnsNames[4], new Double(myrt.getColumn(ij.measure.ResultsTable.MIN)[0]), true);
				currentSpot.addParameter(columnsNames[5], new Double(myrt.getColumn(ij.measure.ResultsTable.MAX)[0]), true);
				currentSpot.addParameter(columnsNames[6], new Double(myrt.getColumn(ij.measure.ResultsTable.X_CENTROID)[0]), true);
				currentSpot.addParameter(columnsNames[7], new Double(myrt.getColumn(ij.measure.ResultsTable.Y_CENTROID)[0]), true);
				currentSpot.addParameter(columnsNames[8], new Double(myrt.getColumn(ij.measure.ResultsTable.X_CENTER_OF_MASS)[0]), true);
				currentSpot.addParameter(columnsNames[9], new Double(myrt.getColumn(ij.measure.ResultsTable.Y_CENTER_OF_MASS)[0]), true);
				currentSpot.addParameter(columnsNames[10], new Double(myrt.getColumn(ij.measure.ResultsTable.PERIMETER)[0]), true);
				currentSpot.addParameter(columnsNames[11], new Double(myrt.getCounter()), true);
				}
			}
			

			
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
				//	System.out.print("number of spots finished="+i);
				//	System.out.println("==> "+(100*i)/spots.size()+"%");
			}
			
		}
		
		System.out.println("quantificaton done!");
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
	}

	/* (non-Javadoc)
	 * @see agscan.plugins.TPlugin#getMenuId()
	 */
	////public int getMenuId() {
		// TODO Auto-generated method stub
	////	return id;
	////}
	  private void displayKernel(float[][] kernel) {
		     // display kernel 
		     if (kernelAd != null) kernelAd.destroy(); // clear old window
		     kernelAd = new ArrayDisplay(kernel,"Kernel");
		     // arrange new window nicely on screen to right of main ImageJ window
		     //Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); 
		     Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		     kernelAd.setScreenLocation(
		             screen.width/4 + 10, 2);
		     ImageTools.autoSetWindowLevel();
		  }
	  
	
}