/* Created on Oct 19, 2005
 * 
 * 
 * This class is an example of quantification plugin 
 
 * Note: the idea here is to indicate the places where the code has to be.
 * updated or modified in order to adapt it to the need.	
 * 
 * @version 2006/10/10
 * @author cklopp
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * 
 */

import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.ImageWindow;
import ij.measure.ResultsTable;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ByteProcessor;
import ij.plugin.Thresholder;
import ij.plugin.filter.Binary;

import ij.plugin.MeasurementsWriter;
import ij.plugin.filter.Analyzer;
import ij.plugin.filter.ParticleAnalyzer;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SExampleQuantificationPlugin2pegourie extends SQuantifPlugin {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// *** size to be adjusted to the need
	String[] columnsNames = new String[8];//array of columns created by the quantification
	int[] columnsTypes = new int[8];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	//public static int id = -1;
	
	public SExampleQuantificationPlugin2pegourie() {
		
		super("Simple Quantification Pegourie initial",null); //null because no icon asociated		
		id = id0; 						 // next id in the row 
		setMenuPlugin(true); 			 //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);	 //menu is enabled only on an alignment
		imagesMin = 2;
		imagesMax = 2;
		//declaration of columns of the plugin
		
		// *** to be adjusted to the need
		//name of quantification columns
		columnsNames[0]= "Dominance";
		columnsNames[1]= "Vert (%)";
		columnsNames[2]= "Rouge (%)";
		columnsNames[3]= "Area";
		columnsNames[4]= "Mean";
		columnsNames[5]= "Perimetre";
		columnsNames[6]= "Circulariti";
		columnsNames[7]= "Fiabiliti";
		


		// *** to be adjusted to the need
		//type of quantification results
		columnsTypes[0] = TColumn.TYPE_TEXT;
		columnsTypes[1] = TColumn.TYPE_REAL;
		columnsTypes[2] = TColumn.TYPE_REAL;
		columnsTypes[3] = TColumn.TYPE_REAL;
		columnsTypes[4] = TColumn.TYPE_REAL;
		columnsTypes[5] = TColumn.TYPE_REAL;
		columnsTypes[6] = TColumn.TYPE_REAL;
		columnsTypes[7] = TColumn.TYPE_TEXT;
		

	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * run of the quantification 
	 */
	//@SuppressWarnings("deprecation")
	public void run() {				
		// information about the alignment
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages(); // in the stack you have the different images 
		
		// get the selected spots 
		Vector spots = getSelectedSpots();

		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		// addition of spots parameters to each spot
		System.out.println("columnsNames.length = "+columnsNames.length);
		for (int i=0;i<columnsNames.length;i++)	{
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}
		
		// ------ memory and processing time -----------
		// some parameter can be set for once and used after in order to be more efficient
		int pixelWidth = (int) model.getPixelWidth(); //width of one pixel in micrometers( for example 25)
		int pixelHeight = (int) model.getPixelHeight(); //height of one pixel in micrometers( for example 25)
		
		// or in order not to recreate new object in the loop the object are created once and reinitialized for
		// each spot
		TSpot currentSpot;
		currentSpot =(TSpot) spots.elementAt(0);
		int spotWidth = (int)(currentSpot.getSpotWidth()/pixelWidth);
		int spotHeight = (int)(currentSpot.getSpotHeight()/pixelHeight);
		//System.out.println("spotWidth = "+spotWidth+" spotHeight = "+spotWidth);
		ImageProcessor ipSpot_16green = new FloatProcessor(spotWidth, spotHeight, null, null);
		//ImagePlus impSpot_16green = new ImagePlus("Signed 16 bit", ipSpot_16green);
		ImageProcessor ipSpot_16red = new FloatProcessor(spotWidth, spotHeight, null, null);
		//ImagePlus impSpot_16red = new ImagePlus("Signed 16 bit", ipSpot_16red);
		
		float[] greenSpot = new float[spotWidth*spotHeight];
		float[] redSpot = new float[spotWidth*spotHeight];
						
		// This part in handling the GUI before the job 
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part
		
		// to retrieve the data of the images (colors in 16 bits and max image in 8 bits)
		ImageProcessor ip_16red = stack.getProcessor(1);
		ImageProcessor ip_16green = stack.getProcessor(2);
		// On realise une auto binarisation de l'image sur 8 bits afin					Analyzer.getResultsTable().deleteRow(0);
		Analyzer.getResultsTable().deleteRow(1);
		// Creation de la table de resultats
		ResultsTable rt = Analyzer.getResultsTable();
		ParticleAnalyzer Pan = new ParticleAnalyzer();
		
		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i); //to get current spot 
			
			// to fill the spot image processors
			for (int j = 0; j < spotWidth; j++){
				for (int k = 0; k < spotHeight; k++){
					greenSpot[j*spotWidth+ k] = ip_16green.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
					redSpot[j*spotWidth+ k] = ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight+k));
					//System.out.println("j = "+j+" k = "+k+" valueX "+ip_16red.getPixelValue((int)(currentSpot.getX()/pixelWidth+j), (int)(currentSpot.getY()/pixelHeight + k)));
				}
			}
			
			// to create the imageProcessors and imagePlus
			ipSpot_16green = new FloatProcessor(spotWidth, spotHeight, greenSpot, null);			
			ipSpot_16red = new FloatProcessor(spotWidth, spotHeight, redSpot, null);
			
			// Convertion 8 bits des ImageProcessor		
			ipSpot_16green.convertToByte(true);			
			ipSpot_16red.convertToByte(true);
			
			// Creation des ImagePlus
			ImagePlus impSpot_16green,impSpot_16red;			
			impSpot_16green = new ImagePlus("green spot", ipSpot_16green);
			impSpot_16red = new ImagePlus("red spot", ipSpot_16red);
			
		
			/** Preparation des images */
			

			Thresholder thresh = new Thresholder();
			
			ImageWindow winG,winR;
			// On cree une fenetre, sans l'afficher, contenant impSpot_16green car Thresholder va chercher directement
			// l'image dans la fentre active
			winG = new ImageWindow(impSpot_16green);			
			thresh.run("skip");
			// On cree une nouvelle fenetre, sans l'afficher, avec cette fois ci impSpot_16red
			winR = new ImageWindow(impSpot_16red);			
			thresh.run("skip");			
			
			// Recuperation des images 8 bit contenu dans les deux fenetres
			ImagePlus impSpot_8green = winG.getImagePlus();
			ImagePlus impSpot_8red = winR.getImagePlus();	
			
			
			// Apres convertion des images sur 8-bits, on peut utiliser les plugins
			// d'ImageJ sur celles ci
			
			// On realise une ouverture sur l'image afin de supprimer les parasites eventuels
			Binary B = new Binary();
				// Initialisation de l'ouverture
					B.setup("open", impSpot_8green);
					B.setup("open", impSpot_8red);	
				// Ouverture sur l'image
					B.run(impSpot_8green.getProcessor());
					B.run(impSpot_8red.getProcessor());
			
			
			/** Analise de l'image verte */

					
					
					// Creation du Particule analyzer
					// CLEAR_WORKSHEET = 64 efface les resultats precedents
					// 16383 = 11111111111111 pour afficher tout ce dont j'ai besoin
					// rt : table de resultats
					// 1 pour la taille mini du spot en pixel
					// 100 pour la largeur maxi du spot en pixel
					Pan = new ParticleAnalyzer(576, 16383, rt, 16, 250);
					
					
					
					
					//Analyzer an = new Analyzer(impSpot_8green,16383,rt);
					// Affichage du filtre de calcul set measurement
					// an.setup("set", impSpot_8green);
			
					// On lance une analyse sur l'image verte
					
					//an.run(impSpot_8green.getProcessor());
					//Pan.showDialog();
					//Pan.setup("", impSpot_8green);
					Pan.analyze(impSpot_8green);
					

					
					// Pour recuperer des donnies precise
					float AreaG = (float) rt.getValue("Circ.", 0);
					float MeanG = (float) rt.getValue("Circ.", 0);					
					float PerimG = (float) rt.getValue("Circ.", 0);
					float CircG = (float) rt.getValue("Circ.", 0);					
					
					System.out.println("AreaG : " + AreaG);
					System.out.println("MeanG : " + MeanG);					
					System.out.println("PerimG : " + PerimG);
					System.out.println("CircG : " + CircG);	
					
					//an = new Analyzer(impSpot_8red,16383,rt);
					// Affichage du filtre de calcul set measurement
					// an.setup("set", impSpot_8red);
					
					// On lance une analyse sur l'image rouge
					//an.run(impSpot_8red.getProcessor());
					Pan.analyze(impSpot_8red);
					
					// Pour recuperer des donnies precise
					float AreaR = Analyzer.getResultsTable().AREA;
					float MeanR = Analyzer.getResultsTable().MEAN;					
					float PerimR = Analyzer.getResultsTable().PERIMETER;
					float CircR = Analyzer.getResultsTable().CIRCULARITY;
					
					System.out.println("AreaR : " + AreaR);
					System.out.println("MeanR : " + MeanR);					
					System.out.println("PerimR : " + PerimR);
					System.out.println("CircR : " + CircR);	
			
					// Affichage de la tble de resultats
					// Analyzer.getResultsTable().show("resultats");
			
					
			
					// show the imagePlus
					//impSpot_8green.show();
					//impSpot_8red.show();
					
					// just to have columns !!
					
					// On s'impose certains criteres de selection
					
					float total = AreaG + AreaR;
					float PourcentG = (AreaG*100)/total;
					float PourcentR = 100 - PourcentG;
					
					
					if(AreaG>AreaR)//  = OK


					{
						if (CircG > 0.85)
						{
							if (PourcentR-PourcentG <= 20)
							{
								currentSpot.addParameter(columnsNames[0], "VERT", true);
							}
							else
							{
								currentSpot.addParameter(columnsNames[0], "JAUNE", true);	
							}
							currentSpot.addParameter(columnsNames[1], new Double(PourcentG), true);
							currentSpot.addParameter(columnsNames[2], new Double(PourcentR), true);
							currentSpot.addParameter(columnsNames[3], new Double(AreaG), true);
							currentSpot.addParameter(columnsNames[4], new Double(MeanG), true);
							currentSpot.addParameter(columnsNames[5], new Double(PerimG), true);
							currentSpot.addParameter(columnsNames[6], new Double(CircG), true);
						
							currentSpot.addParameter(columnsNames[7], "YES", true);
						}
						else
						{
						currentSpot.addParameter(columnsNames[0], "---", true);
						currentSpot.addParameter(columnsNames[1], new Double(0), true);
						currentSpot.addParameter(columnsNames[2], new Double(0), true);
						currentSpot.addParameter(columnsNames[3], new Double(0), true);
						currentSpot.addParameter(columnsNames[4], new Double(0), true);
						currentSpot.addParameter(columnsNames[5], new Double(0), true);
						currentSpot.addParameter(columnsNames[6], new Double(0), true);
						currentSpot.addParameter(columnsNames[7], "NO", true);	
						}
					}
					else
					{
						if (CircG > 0.85)
						{
							if (PourcentR-PourcentG <= 10)
							{
								currentSpot.addParameter(columnsNames[0], "ROUGE", true);
							}
							else
							{
								currentSpot.addParameter(columnsNames[0], "JAUNE", true);	
							}
							currentSpot.addParameter(columnsNames[1], new Double(PourcentG), true);
							currentSpot.addParameter(columnsNames[2], new Double(PourcentR), true);
							currentSpot.addParameter(columnsNames[3], new Double(AreaR), true);
							currentSpot.addParameter(columnsNames[4], new Double(MeanR), true);
							currentSpot.addParameter(columnsNames[5], new Double(PerimR), true);
							currentSpot.addParameter(columnsNames[6], new Double(CircR), true);
						
							currentSpot.addParameter(columnsNames[7], "YES", true);
						}
						else
						{
						currentSpot.addParameter(columnsNames[0], "---", true);
						currentSpot.addParameter(columnsNames[1], new Double(0), true);
						currentSpot.addParameter(columnsNames[2], new Double(0), true);
						currentSpot.addParameter(columnsNames[3], new Double(0), true);
						currentSpot.addParameter(columnsNames[4], new Double(0), true);
						currentSpot.addParameter(columnsNames[5], new Double(0), true);
						currentSpot.addParameter(columnsNames[6], new Double(0), true);
						currentSpot.addParameter(columnsNames[7], "NO", true);	
						}
					}
										
					
/*			//nombre de colonne
			int sizeRt = rt.getCounter();
			
			for (int k = 0; k < spots.size(); k++){
				currentSpot =(TSpot) spots.elementAt(k);//current spot
				for ( int l = 0; l<sizeRt ; l++ ) {
					//we search the number of pixels into height and width of the rectangle representing the spot
					//currentSpot.addParameter(columnsNames[l], new Double(rt.getRowAsString(l)), true);
					//System.out.println(rt.getRowAsString(l));
				}
			}
*/			
			
			
			
			// This part in handling the GUI during the job
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
			}
			// ---- end of the GUI part
		}
		
		// This part in handling the GUI after the job 	
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		// ---- end of the GUI part 
	}

}