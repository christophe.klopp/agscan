import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.event.TEvent;
import agscan.ioxml.TDefaultFileChooser;
import agscan.menu.TMenuManager;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class TView3DDialog extends JDialog {
  private JPanel centerPanel = new JPanel();
  private JButton closeButton = new JButton();
  private JButton saveButton = new JButton();
  private JPanel southPanel = new JPanel();
  private JCheckBox imageCheckBox = new JCheckBox();
  private JPanel imageChoicesPanel = new JPanel();
  private JCheckBox imageSurfaceCheckBox = new JCheckBox();
  private JCheckBox imageColorCheckBox = new JCheckBox();
  private JLabel imageTranspLabel = new JLabel();
  private JSlider imageTranspSlider = new JSlider();
  private JCheckBox fitCheckBox = new JCheckBox();
  private JPanel fitChoicesPanel = new JPanel();
  private JCheckBox fitSurfaceCheckBox = new JCheckBox();
  private JCheckBox fitColorCheckBox = new JCheckBox();
  private JLabel fitTranspLabel = new JLabel();
  private JSlider fitTranspSlider = new JSlider();
  private JCheckBox backgroundCheckBox = new JCheckBox();
  private JPanel backgroundChoicesPanel = new JPanel();
  private JCheckBox backgroundSurfaceCheckBox = new JCheckBox();
  private JLabel backgroundTranspLabel = new JLabel();
  private JSlider backgroundTranspSlider = new JSlider();
  private JPanel otherPanel = new JPanel();
  private JLabel zoomZLabel = new JLabel();
  private JSlider zoomZSlider = new JSlider();
  private JCheckBox zone3DCheckBox = new JCheckBox();
  private JLabel zoomXYLabel = new JLabel();
  private JSlider zoomXYSlider = new JSlider();
  private JCheckBox showSpotsCheckBox = new JCheckBox();

   public TView3DDialog(TDataElement element) {
    super(FenetrePrincipale.application);
    elem = element;
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    init(element);
    initListeners();
    pack();
  }

  private static TView3DDialog instance;
  private TView3DPanel view3DPanel;
  private TDataElement elem;

  public void init(TDataElement element) {
    if (element instanceof TAlignment || element instanceof TImage) {
      Object[] params3D = T3DTool.get3DParams();
      elem = element;
      if (element instanceof TImage) {
        imageCheckBox.setEnabled(true);
        fitCheckBox.setEnabled(false);
        backgroundCheckBox.setEnabled(false);
        zoomZLabel.setEnabled(true);
        zoomXYLabel.setEnabled(true);
        zoomZSlider.setEnabled(true);
        zoomXYSlider.setEnabled(true);
      }
      else if (element instanceof TAlignment) {
        imageCheckBox.setEnabled(true);
        fitCheckBox.setEnabled(true);
        backgroundCheckBox.setEnabled(true);
        zoomZLabel.setEnabled(true);
        zoomXYLabel.setEnabled(true);
        zoomZSlider.setEnabled(true);
        zoomXYSlider.setEnabled(true);
      }
      else {
      	//nothing!
      }
      imageCheckBox.setSelected(((Boolean)params3D[4]).booleanValue());
      imageSurfaceCheckBox.setEnabled(((Boolean)params3D[4]).booleanValue());
      imageColorCheckBox.setEnabled(((Boolean)params3D[4]).booleanValue());
      imageTranspSlider.setEnabled(((Boolean)params3D[4]).booleanValue() && ((Boolean)params3D[7]).booleanValue());
      imageTranspLabel.setEnabled(((Boolean)params3D[4]).booleanValue() && ((Boolean)params3D[7]).booleanValue());
      ItemListener[] ils = fitCheckBox.getItemListeners();
      ItemListener il = null;
      if (ils.length > 0) {
        il = ils[0];
        fitCheckBox.removeItemListener(il);
      }
      fitCheckBox.setSelected(((Boolean)params3D[5]).booleanValue());
      if (il != null) fitCheckBox.addItemListener(il);
      fitSurfaceCheckBox.setEnabled(((Boolean)params3D[5]).booleanValue());
      fitColorCheckBox.setEnabled(((Boolean)params3D[5]).booleanValue());
      fitTranspSlider.setEnabled(((Boolean)params3D[5]).booleanValue() && ((Boolean)params3D[8]).booleanValue());
      fitTranspLabel.setEnabled(((Boolean)params3D[5]).booleanValue() && ((Boolean)params3D[8]).booleanValue());
      backgroundCheckBox.setSelected(((Boolean)params3D[6]).booleanValue());
      backgroundSurfaceCheckBox.setEnabled(((Boolean)params3D[6]).booleanValue());
      backgroundTranspSlider.setEnabled(((Boolean)params3D[6]).booleanValue() && ((Boolean)params3D[9]).booleanValue());
      backgroundTranspLabel.setEnabled(((Boolean)params3D[6]).booleanValue() && ((Boolean)params3D[9]).booleanValue());
      imageSurfaceCheckBox.setSelected(((Boolean)params3D[7]).booleanValue());
      fitSurfaceCheckBox.setSelected(((Boolean)params3D[8]).booleanValue());
      backgroundSurfaceCheckBox.setSelected(((Boolean)params3D[9]).booleanValue());
      imageColorCheckBox.setSelected(((Boolean)params3D[10]).booleanValue());
      fitColorCheckBox.setSelected(((Boolean)params3D[11]).booleanValue());
      imageTranspSlider.setValue(((Integer)params3D[12]).intValue());
      fitTranspSlider.setValue(((Integer)params3D[13]).intValue());
      backgroundTranspSlider.setValue(((Integer)params3D[14]).intValue());
      zoomZSlider.setValue(((Integer)params3D[15]).intValue());
      zoomXYSlider.setValue(((Integer)params3D[16]).intValue());
      zone3DCheckBox.setSelected(((Boolean)params3D[17]).booleanValue());

      T3DViewInteractor.getInstance().init(elem);
    }
    else {
      elem = null;
      imageCheckBox.setSelected(false);
      imageCheckBox.setEnabled(false);
      imageSurfaceCheckBox.setEnabled(false);
      imageColorCheckBox.setEnabled(false);
      imageTranspSlider.setEnabled(false);
      imageTranspLabel.setEnabled(false);
      fitCheckBox.setSelected(false);
      fitSurfaceCheckBox.setEnabled(false);
      fitColorCheckBox.setEnabled(false);
      fitTranspSlider.setEnabled(false);
      fitTranspLabel.setEnabled(false);
      fitCheckBox.setEnabled(false);
      backgroundCheckBox.setSelected(false);
      backgroundCheckBox.setEnabled(false);
      backgroundSurfaceCheckBox.setEnabled(false);
      backgroundTranspSlider.setEnabled(false);
      backgroundTranspLabel.setEnabled(false);
      zone3DCheckBox.setSelected(false);
      zone3DCheckBox.setEnabled(false);
      zoomZLabel.setEnabled(false);
      zoomXYLabel.setEnabled(false);
      zoomZSlider.setEnabled(false);
      zoomXYSlider.setEnabled(false);
    }
    if (elem instanceof TAlignment || elem instanceof TImage) {
      if (zone3DCheckBox.isSelected()) {
        TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
        ///*/
        /*if (! ( (Boolean) TEventHandler.handleMessage(event)[0]).booleanValue())
          elem.getView().getGraphicPanel().setCurrentInteractor(T3DViewInteractor.getName());
      }
      else {
        elem.getView().getGraphicPanel().removeInteractors();
        TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
        */
        ///*/
        /*if (! ( (Boolean) TEventHandler.handleMessage(event)[0]).booleanValue()) {
          if (elem instanceof TAlignment) {
            if (! ( (TAlignment) elem).isAlgoRunning())
              elem.getView().getGraphicPanel().setCurrentInteractor(TAlignmentInteractor.getName());
          }
          else if (elem instanceof TImage) {
            elem.getView().getGraphicPanel().setCurrentInteractor(TMousePositionInteractor.getName());
          }
        }
        else
          elem.getView().getGraphicPanel().setCurrentInteractor(TZoomInteractor.getName());
          */
      }
    }
    view3DPanel.init(elem);
    pack();
  }
  public boolean isZone3DCheckBoxSelected() {
    return zone3DCheckBox.isSelected();
  }
  private void closeAction() {
    Object[] o = T3DTool.get3DParams();
    o[17] = new Boolean(zone3DCheckBox.isSelected());
    T3DTool.set3DParams(o);
    elem.getView().getGraphicPanel().removeInteractors();
    TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
    ///*/
    /*
    if (!((Boolean)TEventHandler.handleMessage(ev)[0]).booleanValue()) {
      if (elem instanceof TAlignment) {
        if (! ( (TAlignment) elem).isAlgoRunning())
          elem.getView().getGraphicPanel().setCurrentInteractor(TAlignmentInteractor.getName());
      }
      else if (elem instanceof TImage) {
        elem.getView().getGraphicPanel().setCurrentInteractor(TMousePositionInteractor.getName());
      }
    }
    else
      elem.getView().getGraphicPanel().setCurrentInteractor(TZoomInteractor.getName());
    if (elem.getView().getGraphicPanel().getCurrentInteractor() != null)
      elem.getView().getGraphicPanel().getCurrentInteractor().init(elem);
    */
    ///*/
    elem.getView().getGraphicPanel().repaint();
    dispose();
  ///*/  tool.notifyClosing();
  }
  private void initListeners() {
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        closeAction();
      }
    });
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        closeAction();
      }
    });
    zone3DCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[17] = new Boolean(zone3DCheckBox.isSelected());
            T3DTool.set3DParams(o);
           ///*/ if (zone3DCheckBox.isSelected()) {
             /* elem.getView().getGraphicPanel().setCurrentInteractor(T3DViewInteractor.getName());
              TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.DEZOOM, null);
              TEventHandler.handleMessage(event);
            }
           ///*/ //else {
              elem.getView().getGraphicPanel().removeInteractors();
              TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.IS_ZOOM, null);
              /*if (! ( (Boolean) TEventHandler.handleMessage(ev)[0]).booleanValue()) {
                if (elem instanceof TAlignment) {
                  if (! ( (TAlignment) elem).isAlgoRunning())
                    elem.getView().getGraphicPanel().setCurrentInteractor(TAlignmentInteractor.getName());
                }
                else if (elem instanceof TImage) {
                  elem.getView().getGraphicPanel().setCurrentInteractor(TMousePositionInteractor.getName());
                }
              }
              else
                elem.getView().getGraphicPanel().setCurrentInteractor(TZoomInteractor.getName());
                */
            }
           /* if (elem.getView().getGraphicPanel().getCurrentInteractor() != null)
              elem.getView().getGraphicPanel().getCurrentInteractor().init(elem);
            elem.getView().getGraphicPanel().repaint();
            */
          }
        //}
      }
    });
    imageCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            imageSurfaceCheckBox.setEnabled(imageCheckBox.isSelected());
            imageColorCheckBox.setEnabled(imageCheckBox.isSelected());
            imageTranspSlider.setEnabled(imageCheckBox.isSelected());
            imageTranspLabel.setEnabled(imageCheckBox.isSelected());
            showSpotsCheckBox.setEnabled(imageCheckBox.isSelected());
            Object[] o = T3DTool.get3DParams();
            o[4] = new Boolean(imageCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
            TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
            T3DViewInteractor.createSceneGraph(o,false,element);
            ////ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, element, o, new Boolean(false));
           //// TEventHandler.handleMessage(ev);
         ///*/   ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, element, null);
            ///*/TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    imageSurfaceCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[7] = new Boolean(imageSurfaceCheckBox.isSelected());
            T3DTool.set3DParams(o);
            if (evt.getStateChange() == ItemEvent.SELECTED) {
              imageTranspSlider.setEnabled(true);
            }
            else if (evt.getStateChange() == ItemEvent.DESELECTED) {
              imageTranspSlider.setEnabled(false);
            }
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,false,elem);
            //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
           // TEventHandler.handleMessage(ev);
           ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
          ///*/  TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    imageColorCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[10] = new Boolean(imageColorCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,true,elem);
            //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(true));
            //TEventHandler.handleMessage(ev);
          ///*/  ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///*/TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    fitCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            fitSurfaceCheckBox.setEnabled(fitCheckBox.isSelected());
            fitColorCheckBox.setEnabled(fitCheckBox.isSelected());
            fitTranspSlider.setEnabled(fitCheckBox.isSelected());
            fitTranspLabel.setEnabled(fitCheckBox.isSelected());
            Object[] o = T3DTool.get3DParams();
            o[5] = new Boolean(fitCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
            TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
            T3DViewInteractor.createSceneGraph(o,false,element);
            ///ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, element, o, new Boolean(false));
            //TEventHandler.handleMessage(ev);
            ///*/ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    fitSurfaceCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[8] = new Boolean(fitSurfaceCheckBox.isSelected());
            T3DTool.set3DParams(o);
            if (evt.getStateChange() == ItemEvent.SELECTED) {
              fitTranspSlider.setEnabled(true);
            }
            else if (evt.getStateChange() == ItemEvent.DESELECTED) {
              fitTranspSlider.setEnabled(false);
            }
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,false,elem);
           // TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
           // TEventHandler.handleMessage(ev);
            ///*/ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///*/TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    fitColorCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[11] = new Boolean(fitColorCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,true,elem);
            //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(true));
            //TEventHandler.handleMessage(ev);
            ///*/ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///*/TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    backgroundCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          if ( (evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            backgroundSurfaceCheckBox.setEnabled(backgroundCheckBox.isSelected());
            backgroundTranspSlider.setEnabled(backgroundCheckBox.isSelected());
            backgroundTranspLabel.setEnabled(backgroundCheckBox.isSelected());
            Object[] o = T3DTool.get3DParams();
            o[6] = new Boolean(backgroundCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
            TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
            T3DViewInteractor.createSceneGraph(o,false,element);
            //ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, element, o, new Boolean(false));
            //TEventHandler.handleMessage(ev);
           ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ////*/TEventHandler.handleMessage(ev);
          }
        }
      }
    });
    backgroundSurfaceCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage)
          if ((evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[9] = new Boolean(backgroundSurfaceCheckBox.isSelected());
            T3DTool.set3DParams(o);
            if (evt.getStateChange() == ItemEvent.SELECTED) {
              backgroundTranspSlider.setEnabled(true);
            }
            else if (evt.getStateChange() == ItemEvent.DESELECTED) {
              backgroundTranspSlider.setEnabled(false);
            }
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,false,elem);
        //    TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
          //  TEventHandler.handleMessage(ev);
           ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///*/TEventHandler.handleMessage(ev);
          }

      }
    });
    zoomZSlider.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          Object[] o = T3DTool.get3DParams();
          o[15] = new Integer(zoomZSlider.getValue());
          T3DTool.set3DParams(o);
          T3DViewInteractor.getInstance().init(elem);
          T3DViewInteractor.createSceneGraph(o,false,elem);
          //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
          //TEventHandler.handleMessage(ev);
          ///*/ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
            ///*/TEventHandler.handleMessage(ev);
        }
      }
    });
    zoomXYSlider.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          Object[] o = T3DTool.get3DParams();
          o[16] = new Integer(zoomXYSlider.getValue());
          T3DTool.set3DParams(o);
          T3DViewInteractor.getInstance().init(elem);
          T3DViewInteractor.createSceneGraph(o,false,elem);
          //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
          //TEventHandler.handleMessage(ev);
          ////*/ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
           ///*/ TEventHandler.handleMessage(ev);
        }
      }
    });
    imageTranspSlider.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          Object[] o = T3DTool.get3DParams();
          o[12] = new Integer(imageTranspSlider.getValue());
          T3DTool.set3DParams(o);
          T3DViewInteractor.getInstance().init(elem);
          T3DViewInteractor.createSceneGraph(o,false,elem);
          //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
         // TEventHandler.handleMessage(ev);
         ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
           ///*/ TEventHandler.handleMessage(ev);
        }
      }
    });
    fitTranspSlider.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          Object[] o = T3DTool.get3DParams();
          o[13] = new Integer(fitTranspSlider.getValue());
          T3DTool.set3DParams(o);
          T3DViewInteractor.getInstance().init(elem);
          T3DViewInteractor.createSceneGraph(o,false,elem);
          //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
          //TEventHandler.handleMessage(ev);
         ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
           ///*/ TEventHandler.handleMessage(ev);
        }
      }
    });
    backgroundTranspSlider.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage) {
          Object[] o = T3DTool.get3DParams();
          o[14] = new Integer(backgroundTranspSlider.getValue());
          T3DTool.set3DParams(o);
          T3DViewInteractor.getInstance().init(elem);
          T3DViewInteractor.createSceneGraph(o,false,elem);
          //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(false));
          //TEventHandler.handleMessage(ev);
         ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
          ///*/TEventHandler.handleMessage(ev);
        }
      }
    });
    showSpotsCheckBox.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        if (elem instanceof TAlignment || elem instanceof TImage)
          if ((evt.getStateChange() == ItemEvent.SELECTED) || (evt.getStateChange() == ItemEvent.DESELECTED)) {
            Object[] o = T3DTool.get3DParams();
            o[18] = new Boolean(showSpotsCheckBox.isSelected());
            T3DTool.set3DParams(o);
            T3DViewInteractor.getInstance().init(elem);
            T3DViewInteractor.createSceneGraph(o,true,elem);
            //TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, elem, o, new Boolean(true));
           // TEventHandler.handleMessage(ev);
           ///*/ ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, elem, null);
           ///*/ TEventHandler.handleMessage(ev);
          }
      }
    });
    saveButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Enregistrer une image");
        chooser.addChoosableFileFilter(new ImageFilter("jpg", "JPEG images (*.jpg)"));
        chooser.addChoosableFileFilter(new ImageFilter("tif", "TIFF images (*.tif)"));
        chooser.addChoosableFileFilter(new ImageFilter("png", "PNG images (*.png)"));
        chooser.addChoosableFileFilter(new ImageFilter("bmp", "BMP images (*.bmp)"));
        int returnVal = chooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          String desc = chooser.getFileFilter().getDescription();
          File file = chooser.getSelectedFile();
          view3DPanel.save(file, desc);
        }
      }
    });
  }
  public static TView3DDialog getInstance(TDataElement element) {
    if (instance == null)
      instance = new TView3DDialog(element);
    else
      instance.init(element);
       return instance;
  }

  private void jbInit() throws Exception {
    setTitle("Vue 3D");
    setResizable(false);
    view3DPanel = new TView3DPanel(elem);
    this.getContentPane().setLayout(new BorderLayout());
    closeButton.setText("Fermer");
    saveButton.setText("Enregistrer l'image");
    centerPanel.setLayout(new GridBagLayout());
    imageCheckBox.setText("Image");
    imageChoicesPanel.setLayout(new GridBagLayout());
    imageChoicesPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(
      Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
    imageSurfaceCheckBox.setText("Surface");
    imageColorCheckBox.setText("Couleur");
    imageTranspLabel.setText("Transp. :");
    imageTranspSlider.setMinimumSize(new Dimension(100, 16));
    imageTranspSlider.setPreferredSize(new Dimension(100, 16));
    fitCheckBox.setText("Fit");
    fitChoicesPanel.setLayout(new GridBagLayout());
    fitChoicesPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(
      Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
    fitSurfaceCheckBox.setText("Surface");
    fitColorCheckBox.setText("Couleur");
    fitTranspLabel.setText("Transp. :");
    fitTranspSlider.setMinimumSize(new Dimension(100, 16));
    fitTranspSlider.setPreferredSize(new Dimension(100, 16));
    backgroundCheckBox.setText("Background");
    backgroundChoicesPanel.setLayout(new GridBagLayout());
    backgroundChoicesPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(
      Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
    backgroundSurfaceCheckBox.setText("Surface");
    backgroundTranspLabel.setText("Transp. :");
    backgroundTranspSlider.setMinimumSize(new Dimension(100, 16));
    backgroundTranspSlider.setPreferredSize(new Dimension(100, 16));
    otherPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(
      Color.white, new Color(148, 145, 140)), BorderFactory.createEmptyBorder(5, 5, 0, 0)));
    otherPanel.setLayout(new GridBagLayout());
    zoomZLabel.setText("Zoom Z :");
    zoomZSlider.setPreferredSize(new Dimension(100, 16));
    zone3DCheckBox.setToolTipText("");
    zone3DCheckBox.setText("Afficher la zone 3D");
    showSpotsCheckBox.setText("Afficher les spots");
    zoomXYLabel.setText("Zoom XY :");
    zoomXYSlider.setMinimumSize(new Dimension(100, 16));
    zoomXYSlider.setPreferredSize(new Dimension(100, 16));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    southPanel.add(saveButton);
    southPanel.add(closeButton);
    this.getContentPane().add(view3DPanel, java.awt.BorderLayout.NORTH);
    imageChoicesPanel.add(imageSurfaceCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    fitChoicesPanel.add(fitSurfaceCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(imageCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(fitCheckBox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    centerPanel.add(backgroundCheckBox, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    imageChoicesPanel.add(imageTranspSlider, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    fitChoicesPanel.add(fitTranspSlider, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    imageChoicesPanel.add(imageTranspLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    fitChoicesPanel.add(fitTranspLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    centerPanel.add(otherPanel, new GridBagConstraints(2, 0, 1, 3, 0.0, 0.0
        , GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    otherPanel.add(zoomZLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    otherPanel.add(zoomZSlider, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    otherPanel.add(zoomXYLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    otherPanel.add(zoomXYSlider, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
    otherPanel.add(zone3DCheckBox, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));
    otherPanel.add(showSpotsCheckBox, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
    this.getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);
    this.getContentPane().add(southPanel, java.awt.BorderLayout.SOUTH);
    imageChoicesPanel.add(imageColorCheckBox, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    fitChoicesPanel.add(fitColorCheckBox, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    centerPanel.add(backgroundChoicesPanel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
        , GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    centerPanel.add(fitChoicesPanel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    centerPanel.add(imageChoicesPanel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 5, 0, 0), 0, 0));
    backgroundChoicesPanel.add(backgroundTranspSlider, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    backgroundChoicesPanel.add(backgroundTranspLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
    backgroundChoicesPanel.add(Box.createHorizontalStrut(8), new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 58, 0));
    backgroundChoicesPanel.add(backgroundSurfaceCheckBox, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
  }
}
