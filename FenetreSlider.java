/*
 * Created on 17 janv. 2007
 *
 * TODO FARAL
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Administrateur
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FenetreSlider extends JFrame 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel sliderPanel;
	private JTextField textField1 = new JTextField();
	private JTextField textField2 = new JTextField();
	private JTextField textField3 = new JTextField();
	private JTextField textField4 = new JTextField();
	private JTextField textField5 = new JTextField();
	public JSlider slider1 = new JSlider();
	public JSlider slider2 = new JSlider();
	public JSlider slider3 = new JSlider();
	public JSlider slider4 = new JSlider();
	public JSlider slider5 = new JSlider();
	private ChangeListener listener;
	public static final int DEFAULT_WIDTH=5550;
	public static final int DEFAULT_HEIGHT=350;
	public int attribut1;
	public int attribut2;
	public float attribut3;
	public float attribut4;
	public float attribut5;
	public JCheckBox bold;

	public boolean exit = false;
	public JButton B1;
	public JButton B2;
	public JButton B3;
	public boolean image=false;
	
	public void addSlider(JSlider s, JTextField t , String description)
	{
		s.addChangeListener(listener);
		JPanel panel = new JPanel();
		panel.add(s);
		panel.add(t);
		panel.add(new JLabel(description));
		sliderPanel.add(panel);
	}
	
	public FenetreSlider(Fenetre f,int tmax)
	{
			setTitle("Quantification en cercle fixe");
			setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
			sliderPanel = new JPanel();
			GridLayout gestionnaire = new GridLayout(5, 5);
			sliderPanel.setLayout(gestionnaire);
			//sliderPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
			
			JPanel panel = new JPanel();
			panel.setLayout(new FlowLayout(FlowLayout.LEFT));
			JPanel panel2 = new JPanel();
			panel2.setLayout(new FlowLayout(FlowLayout.LEFT));
			
			
			//Ecouteur commun a tout les curseurs
			listener =new ChangeListener()
			{
				public void stateChanged(ChangeEvent event)
				{
					if(event.getSource().equals(slider1))
					{
						JSlider source = (JSlider)event.getSource();
						textField1.setText(" "+source.getValue());
					}
					if(event.getSource().equals(slider2))
					{
						JSlider source = (JSlider)event.getSource();
						textField2.setText(" "+source.getValue());
					}
					if(event.getSource().equals(slider3))
					{
						JSlider source = (JSlider)event.getSource();
						textField3.setText(" "+(source.getValue()));
					}
					if(event.getSource().equals(slider4))
					{
						JSlider source = (JSlider)event.getSource();
						textField4.setText(" "+(source.getValue()));
					}
					if(event.getSource().equals(slider5))
					{
						JSlider source = (JSlider)event.getSource();
						textField5.setText(" "+source.getValue());
					}
					
				}
			};

			textField1.setText("100");
			textField2.setText("100");
			textField3.setText("100");
			textField4.setText("100");
			textField5.setText("100");
			addSlider(slider1,textField1,"Taille selection 	                       ");
			addSlider(slider2,textField2,"Intensiti moyenne                 ");
			addSlider(slider3,textField3,"Critere Centre (%) du rayon ");
			addSlider(slider4,textField4,"Critere Circulariti (%)           ");
			addSlider(slider5,textField5,"Bruit                                        ");
			slider1.setMaximum(tmax);
			slider1.setMinimum(1);
			slider2.setMaximum(255);
			slider2.setMinimum(0);
			slider3.setMaximum(100);
			slider3.setMinimum(0);
			slider4.setMaximum(100);
			slider4.setMinimum(0);
			slider5.setMaximum(200);
			slider5.setMinimum(0);
		
			getContentPane().add(sliderPanel, BorderLayout.NORTH);
			
			bold=new JCheckBox("Afficher les images du traitement");
			bold.setActionCommand("Afficher les images du traitement");
			bold.addActionListener(f);
			//bold.setSelected(image);
			panel2.add(bold,BorderLayout.NORTH);
			getContentPane().add(panel2, BorderLayout.CENTER);
			//(checkboxPanel, BorderLayout.EAST);
			
			JButton B1 = new JButton("OK");
			B1.setActionCommand("OK");
			B1.addActionListener(f);
		
			JButton B2 = new JButton("Difaut");
			B2.setActionCommand("Difaut");
			B2.addActionListener(f);
			
			JButton B3 = new JButton("Fermer");
			B3.setActionCommand("Fermer");
			B3.addActionListener(f);
			
			
			panel.add(B1, BorderLayout.EAST);
			panel.add(B2, BorderLayout.EAST);
			panel.add(B3, BorderLayout.EAST);
			
			getContentPane().add(panel, BorderLayout.SOUTH);
		
	}


	/*public void actionPerformed(ActionEvent e) {
		String source = e.getActionCommand();
		if (source.equals("OK")) {
			this.attribut1 = this.slider1.getValue();
			this.attribut2 = this.slider2.getValue();
			this.attribut3 = (float)this.slider3.getValue();
			this.attribut4 = (float)this.slider4.getValue();
			this.exit=true;
			this.dispose();
			
		}
		
		if (source.equals("Effacer")) {
			
		}
		
	}*/
	
	public int getAttribut1()
	{
		return this.attribut1;
	}
	
	public int getAttribut2()
	{
		return this.attribut2;
	}
	
	public float getAttribut3()
	{
		return this.attribut3;
	}
	
	public float getAttribut4()
	{
		return this.attribut4;
	}
	
	public float getAttribut5()
	{
		return this.attribut5;
	}

	public boolean getImage()
	{
		return this.image;
	}
}
