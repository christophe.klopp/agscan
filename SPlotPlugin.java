/*
 * Created on 7th December 2005
 * The SPlotPlugin makes cross plots of two experiments
 * It was developped by Fabrice Lopez for BZScan2
 * and adapted by R�mi Cathelin in order to become an AGScan Plugin 
 * @version 2005/12/07
 * @author Fabrice Lopez
 * @author Remi Cathelin
 * @see TPlugin
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import agscan.AGScan;
import agscan.FenetrePrincipale;
import agscan.algo.quantif.TQuantifFitComputedAlgorithm;
import agscan.algo.quantif.TQuantifFitConstantAlgorithm;
import agscan.algo.quantif.TQuantifImageComputedAlgorithm;
import agscan.algo.quantif.TQuantifImageConstantAlgorithm;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.view.TAlignmentView;
import agscan.data.view.TBatchComPanel;
import agscan.data.view.TBatchView;
import agscan.data.view.graphic.TChartsPanel;
import agscan.data.view.table.TBatchTablePanel;
import agscan.ioxml.TDefaultFileChooser;
import agscan.ioxml.TReadQuantifResultsWorker;
import agscan.ioxml.TResultArray;
import agscan.menu.action.TAction;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPlugin;
import agscan.plugins.TPluginManager;
import agscan.ImageFilter;

public class SPlotPlugin extends TPlugin {
	////public static int id = -1;
	
	TAlignment ref;
	
	public SPlotPlugin(TAlignment reference) {
		super("Plots 2 files",null);// TPlugin => TAction
		ref=reference;
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets the menu always active 
		
	}
	public void exec() {
		
		JFreeChart plot;
		ChartPanel cp;
		
		int indiceplug=-1;
	    int nbc = AGScan.prop.getIntProperty("TColorParametersAction.nbcolors");
	    int nbcol=3+nbc;
	    System.out.println("NBCOL="+nbcol);
	    
	    for (int i=0;i<TPluginManager.plugs.length;i++)
	    {
	    	if (TPluginManager.plugs[i] instanceof SQuantifPlugin)
	    	{
	    		System.err.println(ref.getModel().getColumnName(0));
	    		if(((SQuantifPlugin)TPluginManager.plugs[i]).columnsNames[0]==ref.getModel().getColumnName(0))
	    		{
		    		indiceplug = i;
		    		nbcol = ((SQuantifPlugin)TPluginManager.plugs[i]).columnsNames.length;
		    	}
	    	}
	    }
	    String[] sks  = new String[nbcol];
	    if (indiceplug!=-1)
	    {
	    	
	    	for (int i=0;i<nbcol;i++)
		    {
		    	sks[i] = ((SQuantifPlugin)TPluginManager.plugs[indiceplug]).columnsNames[i];
		    	System.err.println(sks[i]+ "...........");
		    }
	    }
	    else 
	    {
	    	sks[0] = TQuantifImageComputedAlgorithm.COLUMN_NAME ;
	    	sks[1] = TQuantifFitConstantAlgorithm.COLUMN_NAME ;
	    	sks[2] = TQuantifFitComputedAlgorithm.COLUMN_NAME ;
	    	sks[3] = TQuantifImageConstantAlgorithm.COLUMN_NAME ;
	    	for(int i=4;i<nbcol;i++)
	    	{
	    		sks[i]= TQuantifImageConstantAlgorithm.COLUMN_NAME+"("+(i-2)+")" ;
	    	}
	    }
		
		File[] files = askQtfFiles();
		if (files != null) {
			TReadQuantifResultsWorker worker = new TReadQuantifResultsWorker(files[0], null, sks);
			worker.setPriority(Thread.MAX_PRIORITY);
			TResultArray results1 = (TResultArray) worker.construct(false);
			worker = new TReadQuantifResultsWorker(files[1], null, sks);
			worker.setPriority(Thread.MAX_PRIORITY);
			TResultArray results2 = (TResultArray) worker.construct(false);
			
			TChartsPanel chartsPanel = new TChartsPanel();
			XYDotRenderer dotRenderer = new XYDotRenderer();
			dotRenderer.setSeriesPaint(0, Color.red);
			dotRenderer.setSeriesPaint(1, Color.green);
			dotRenderer.setSeriesPaint(2, Color.orange);
			dotRenderer.setSeriesPaint(3, Color.blue);
			
			String n1 = files[0].getName();
			String n2 = files[1].getName();
			for (int i = 0; i < 4; i++) {
				plot = makePlot(results1.getResults()[i], results2.getResults()[i], results1.getQuality(), results2.getQuality(), n1, n2, sks[i]);
				cp = new ChartPanel(plot);
				cp.setPreferredSize(new Dimension(500, 400));
				chartsPanel.addChartPanel(cp, sks[i]);
			}
			JFrame frame = new JFrame("Plots");
			frame.setIconImage(FenetrePrincipale.application.getIconImage());
			frame.getContentPane().setLayout(new BorderLayout());//used getContentPane  (for compatibilty java 1.4.2)
			frame.getContentPane().add(chartsPanel, BorderLayout.CENTER);//used getContentPane  (for compatibilty java 1.4.2)
			frame.pack();
			frame.setResizable(false);
			//frame.setAlwaysOnTop(true);//only with java 5.0 (removed for compatibility java 1.4.2)
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			frame.setLocation( (screenSize.width - frame.getWidth()) / 2, (screenSize.height - frame.getHeight()) / 2);
			frame.setVisible(true);
			FenetrePrincipale.application.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));//added 2005/12/06
		}
	}
	
	private JFreeChart makePlot(int[] v1, int[] v2, int[] vb1, int[] vb2, String n1, String n2, String sk) {
		XYSeries series_1, series_0, series_mixed;
		series_1 = new XYSeries(new Integer(0));
		series_0 = new XYSeries(new Integer(0));
		series_mixed = new XYSeries(new Integer(0));
		int q1, q2;
		for (int i = 0; i < v1.length; i++) {
			q1 = vb1[i];
			q2 = vb2[i];
			if (q1 != q2)
				series_mixed.add(v1[i], v2[i]);
			else if (q1 == 1)
				series_1.add(v1[i], v2[i]);
			else if (q1 == 0)
				series_0.add(v1[i], v2[i]);
		}
		XYSeriesCollection coll = new XYSeriesCollection(series_1);
		coll.addSeries(series_0);
		coll.addSeries(series_mixed);
		JFreeChart plot = ChartFactory.createScatterPlot(sk, n1, n2, coll, PlotOrientation.HORIZONTAL, false, false, false);
		XYDotRenderer dotRenderer = new XYDotRenderer();
		dotRenderer.setSeriesPaint(0, Color.red);
		dotRenderer.setSeriesPaint(1, Color.green);
		dotRenderer.setSeriesPaint(2, Color.orange);
		plot.getXYPlot().setRenderer(dotRenderer);
		return plot;
	}
	private File[] askQtfFiles() {
		File[] files = null;
		boolean ok = false;
		//TODO externalize
		TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Choisir 2 alignements");
		chooser.setMultiSelectionEnabled(true);
		chooser.setCurrentDirectory(new File(AGScan.prop.getProperty("alignmentsPath")));
		chooser.addChoosableFileFilter(new ImageFilter("zaf", "Alignements (*.zaf)"));
		while (!ok) {
			int returnVal = chooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				files = chooser.getSelectedFiles();
				//TODO externalize
				AGScan.prop.setProperty("alignmentsPath",chooser.getCurrentDirectory().getAbsolutePath());
				if (files.length != 2) {
					//TODO externalize strings
					JOptionPane.showMessageDialog(null, "Vous devez choisir 2 fichiers !!!");
					files = null;
				}
				else{       
					ok = true;
				}
			}
			else
				ok = true;
		}
		return files;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("id0 dans l'appel du plugin="+id0);
		System.out.println("id dans l'appel du plugin="+id);
		this.exec();
	}
	//a ajouter dans les plugs sinon -1????
	//+ modifier pour avoir le bon num (80 84????)
////	  public int getMenuId() {
   ////     return id;
   //// }
}
