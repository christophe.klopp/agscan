/*
 * Created on 24th November 2005 
 * The SFilesMergePlugin is a plugin that allow to concat lines from several files
 * The merge is made line by line. A "tab" separator is added between each file line.
 * The merged result file contains the same number of line that the first file given.
 * If other files doesn't have enough lines, "null" is added (instead of the line info).
 * This class extends TPlugin.
 * @version 2005/11/24
 * @author Remi Cathelin
 * @see agscan.plugins.TPlugin;
 */


import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;

import agscan.TEventHandler;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.plugins.TPlugin;


public class SFilesMergePlugin extends TPlugin {
		
	public static int id = -1;
	
	/**
	 * constructor of the class
	 */
	public SFilesMergePlugin () {
		super("Files merge", null); // TPlugin => TAction
		id = id0; // creation ofhe next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(ACTIVE_TYPE); // sets the menu always active.
	}
	
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	
	/**
	 * @return the ID of the menu 
	 */
	public static int getID() {
		return id;
	}
	
	
	public void run() {
		boolean concatSucceed = false;
		File [] filesToConcat;
		JFileChooser chooser = new JFileChooser();
		chooser.setMultiSelectionEnabled(true);//in order to choose several files
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			filesToConcat = chooser.getSelectedFiles();
			if (filesToConcat.length<2) {//if less than 2 files are given
				TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
						TDialogManager.MESSAGE_DIALOG,"You must choose several files!" ); 
				TEventHandler.handleMessage(event);		
				return;
			}
			else concatSucceed = concat(filesToConcat);
		}
		// finished dialog message
		if (concatSucceed){
		TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
				TDialogManager.MESSAGE_DIALOG,"Files merged!" ); 
		TEventHandler.handleMessage(event);
		}
		else{
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.MESSAGE_DIALOG,"Merging error!" ); 
			TEventHandler.handleMessage(event);
		
		}
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		run();		
	}
	
	//return true if ended, false if an exception is created
	public boolean concat(File[] files) {	 
		File concatFile = new File(files[0].getParentFile().getAbsolutePath()+File.separator+"concat.txt");
		PrintWriter concatWriter = null;		
		BufferedReader[] readers = new BufferedReader[files.length];
		BufferedReader [] copy=new BufferedReader[files.length];
		String line=null;
		int concat = 0;
		
		//buffer reading
		for (int i=0;i<files.length;i++){		
			//we fill the tab of bufferedReaders
			try {
				readers[i] = new BufferedReader(new FileReader(files[i]));
				copy[i] = new BufferedReader(new FileReader(files[i]));
			}	
			catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}
		
		
		//add 17/04/08 check if concatenation is possible
		//Alexis JULIN
		//after reading we must check if concatenation is possible
		concat = canConcat(copy);

		System.out.println("resultat global : "+concat);	
		

		
		if(concat == 0){
			//concatWriter writing
			try {			
				concatWriter = new PrintWriter(new FileWriter(concatFile));
				readers[0].mark(0);
				
				while ((line = readers[0].readLine()) != null){
					for (int j=1;j<readers.length;j++){
						line += "\t"+readers[j].readLine();
					}
					
					concatWriter.println(line);
				}			
				concatWriter.close();
			}		
			catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}
		else{
			String mess = "";
			if(concat != 999)
				mess = "The headers are non corresponding. Problem with file "+files[concat].getName();
			else
				mess = "Exception";
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.MESSAGE_DIALOG, mess); 
			TEventHandler.handleMessage(event);
			return false;
		}
		//bufferReader closing
		for (int i=0;i<readers.length;i++){
			//we close bufferedReaders
			try {
				readers[i].close();
			}	
			catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	
	public int canConcat( BufferedReader br[]){
		int resAGScan = 0;
		int resCorrespond = 0;
		boolean b;
		int taille;
		String ligne;
		StringTokenizer st;
		Vector ref = new Vector();

		
		
		try{
			for( int i = 0 ; i < br.length ; i++){
				ligne = br[i].readLine();
				if(!ligne.startsWith("\"Creator=AGScan")){
					resAGScan++;
				}
			}
			

			
			if(resAGScan == br.length){
				return 0;
			}else{
				if(resAGScan==0){
					for( int i = 0 ; i < br.length ; i++){
//						second test => les entetes des colonnes
						//on se place a la 6eme ligne
						do{
							ligne = br[i].readLine();
						}while(!ligne.startsWith("Index"));

						st = new StringTokenizer(ligne, "\t");

						taille = st.countTokens();
						//on sauvegarde les entetes du premier fichier qui serviront de reference

						if(i==0){
							for( int o = 0 ; o < taille ; o++){
								ref.add(st.nextToken());
							}
							System.out.println(ref.size()+" headers stored");
						}


						//on compare avec la reference
						else{
							//on commence par tester les dimensions
							if(st.countTokens()!=ref.size())
								resCorrespond = i;

							else{
								// on compare entete par entete
								int somme = 0;
								String s;
								for( int m = 0 ; m < ref.size() ; m++){
									s = (String)ref.get(m);
									somme = somme + s.compareTo(st.nextToken());
								}
								if(somme != 0)
									resCorrespond = i;
							}
						}
					}			

				}
				System.out.println("resCorrespond "+resCorrespond);
				return resCorrespond;
			}
		}
		catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
			return 999;
			
		}
		catch(IOException ioe){
			ioe.printStackTrace();
			return 999;
		}
	}
}
