/*
 * Created on 17 janv. 2007
 *
 * TODO Faral
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;



public class Fenetre  implements ActionListener
{
	
	public FenetreSlider frame;
	public int taille_sel;
	public int int_moy;
	public int pourcent;
	public float crit_circ;
	public int bruit;
	public boolean image;
	public boolean b=true;
	public boolean fermer = true;	
	
	public Fenetre(int TM)
	{
		this.frame = new FenetreSlider(this,TM);
		frame.slider1.setValue(100);
		frame.slider2.setValue(100);
		frame.slider3.setValue(100);
		frame.slider4.setValue(100);
		frame.slider5.setValue(100);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
		frame.pack();		
	}
	
	public void montrer() { 
		frame.setVisible(true);		
	}
	
	public void defaut () {
		frame.slider1.setValue(9);
		frame.slider2.setValue(70);
		frame.slider3.setValue(25);
		frame.slider4.setValue(60);
		frame.slider5.setValue(10);
	}
	
	public void actionPerformed(ActionEvent e) {
		String source = e.getActionCommand();
		if (source.equals("OK")) {
			b=false;
			this.taille_sel = frame.slider1.getValue();
			this.int_moy = frame.slider2.getValue();
			this.pourcent = frame.slider3.getValue();
			this.crit_circ = (float)frame.slider4.getValue();
			this.bruit = frame.slider5.getValue();
			this.image = (boolean)frame.image;
			frame.dispose();
			
		}
		
		if (source.equals("D�faut")) {
			b=true;
			frame.slider1.setValue(9);
			frame.slider2.setValue(70);
			frame.slider3.setValue(25);
			frame.slider4.setValue(60);
			frame.slider5.setValue(10);
			frame.image=false;
			frame.bold.setSelected(false);

		}
		
		if (source.equals("Fermer")) {
			b=false;
			fermer = false;
			frame.setVisible(false);
		}
		
		if (source.equals("Afficher les images du traitement")) {
			b=true;
			if(frame.image==true){
				frame.image=false;
		
			}
			else{
				frame.image=true;
			}			
			
		}
		
	}
		
}
