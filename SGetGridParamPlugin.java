
/*
 * Created on May 20, 2007
 * 
 * 
 * This class is a quantification plugin based on Magic Tool software.
 * http://www.bio.davidson.edu/projects/magic/magic.html
 * This quantification plugin is working only for two channels images.
 * This quantification is based on a 6 pixels radius fixed circle for 
each rectangular spot region.
 * Results displayed into the application are : "FG Total Red" for the 
foreground red signal
 * "FG Total Green" for the foreground green signal and "Green / Red", 
the ratio green/red.
 * Notice that in this plugin other results are computed and available 
(but not displayed)(see GeneData class).
 * We can easily modify parameters of the radius.
 * We can also choose an other "Magic Tool quantification" (adaptative 
circle or seeded region).
 * An improvement could be the integration of the "Magic Tool" display 
panel which give users the choice of parameters.
 *   
 * Note: this plugin needs classes from package magictool.image.
 * You need to put this package into the plugins directory.	
 * 
 * @version 2007/05/20
 * @author chklopp
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * @see magictool.image.GeneData;
 * 
 */

import ij.ImageStack;

import java.awt.Color;
import java.awt.event.ActionEvent;

import java.util.Vector;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridPositionControler;
import agscan.data.controler.TGridStructureControler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.TGridBlock;
import agscan.data.model.grid.TGridConfig;
import agscan.data.model.grid.TGridModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TAlignmentGraphicPanel;
import agscan.data.view.graphic.TImageGraphicPanel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;


public class SGetGridParamPlugin extends SQuantifPlugin {
	
	String[] columnsNames = new String[3];
	//array of columns created by the quantification
	int[] columnsTypes = new int[3];
	//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	//public static int id = -1;
	
	public SGetGridParamPlugin() {
		
		super("Generate grid copy", null);//null because no icon asociated		
		id = id0; // creation de l'id suivant 
		setMenuPlugin(true); //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);//menu is enabled only on an alignment
		imagesMin = 1;
		imagesMax = 20;
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		new Thread(this).start();
		
	}
	
	/**
	 * run of the quantification 
	 */
	public void run() {		
		// getting the spot informations 
		TAlignment alignment = getCurrentAlignment();
		
		//rotate grid to get the right parameters
		new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, alignment, 
-alignment.getGridModel().getAngle());

		// compute inter-block
		TGridBlock currentBlock1,currentBlock2,currentBlock3;
		Vector gridBlocks = alignment.getGridModel().getLevel1Elements(); 
		System.err.println(alignment.getGridModel().getConfig().getLev2NbCols());
		System.err.println(alignment.getGridModel().getConfig().getLev2NbRows());
		System.err.println(gridBlocks.size());
		double spotWidth= 0;
		double spotHeight = 0;
		double intblockc = 0;
		double intblockr = 0;
		double intblockcol = 0;
		double intblockrow = 0;
		currentBlock1 = ((TGridBlock) gridBlocks.elementAt(0));
		Vector spots1 = currentBlock1.getElements();
		TSpot spot1 =(TSpot) spots1.elementAt(alignment.getGridModel().getConfig().getLev1NbCols()-1); //up right
		spotWidth = spot1.getWidth();
		spotHeight = spot1.getHeight();
		
		if (gridBlocks.size()>1){
				
				currentBlock2 = ((TGridBlock) gridBlocks.elementAt(1));
				currentBlock3 = ((TGridBlock) gridBlocks.elementAt(alignment.getGridModel().getConfig().getLev2NbCols()));
				System.out.println("Block 3 : "+alignment.getGridModel().getConfig().getLev2NbCols());
				
				Vector spots2 = currentBlock2.getElements();
				Vector spots3 = currentBlock3.getElements();
				
				// retrieve angle spots
				
				TSpot spot2 =(TSpot) spots2.elementAt(0); // up left 2nd column
				TSpot spot3 =(TSpot) spots1.elementAt(  //down left 1rst
						(alignment.getGridModel().getConfig().getLev1NbCols())*
						(alignment.getGridModel().getConfig().getLev1NbRows()-1));
				TSpot spot4 =(TSpot) spots3.elementAt(0); //up left 2nd row
				
//				System.out.println("NBCol : "+alignment.getGridModel().getConfig().getLev1NbCols());
//				System.out.println("NBRow : "+alignment.getGridModel().getConfig().getLev1NbRows());
//				System.out.println("Spot 3 : "+
//						(alignment.getGridModel().getConfig().getLev1NbCols())*
//						(alignment.getGridModel().getConfig().getLev1NbRows()-1)+1);

				intblockc = spot2.getX()-spot1.getX();
				intblockr = spot4.getY()-spot3.getY();
				System.out.println("GRID COPY : Inter-Block col: "+intblockc);
				System.out.println("GRID COPY : Inter-Block row: "+intblockr);
				//double intblockrow = intblockr - introw;
				intblockcol = intblockc - spot1.getWidth();
				intblockrow = intblockr - spot1.getHeight();
				System.out.println("GRID COPY : new Inter-Block col: "+intblockcol);
				System.out.println("GRID COPY : new Inter-Block row: "+intblockrow);				
		}else{
			
		}
		
		

		/*
		 * Generate new grille 
		 */
	    TGrid grid = new TGrid(alignment.getGridModel().getConfig());
	  
	    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, 
TDataManager.ADD_DATA_ELEMENT, null, grid);
	    TEventHandler.handleMessage(event);
	    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, 
grid.getView());
	    TEventHandler.handleMessage(event);
	    event = new TEvent(TEventHandler.DIALOG_MANAGER, 
TDialogManager.NEW_GRID, grid);
	    TEventHandler.handleMessage(event);
	    
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, 
TGridStructureControler.SPOT_WIDTH, null,spotWidth);
		TEventHandler.handleMessage(ev);
		TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, 
TGridStructureControler.SPOT_HEIGHT, null,spotHeight);
		TEventHandler.handleMessage(ev2);
		TEvent ev3 = new TEvent(TEventHandler.DATA_MANAGER, 
TGridStructureControler.LEVEL2_ROW_SPACING, null, new Double(intblockrow));
		TEventHandler.handleMessage(ev3);		
		TEvent ev4 = new TEvent(TEventHandler.DATA_MANAGER, 
TGridStructureControler.LEVEL2_COLUMN_SPACING, null, new Double(intblockcol));
		TEventHandler.handleMessage(ev4);
		/*
		 * To clean the grid from all the column which are not generated when creating a new grid!
		 */

		TEvent ev5;
		System.out.println("Number of columns : "+grid.getModel().getColumnCount());
		for (int k = grid.getModel().getColumnCount()-1; k >= 10; k--){
		ev5 = new TEvent(TEventHandler.DATA_MANAGER, 
				TGridColumnsControler.REMOVE_COLUMN, null, grid.getGridModel().getConfig().getColumns().getColumn(k));
				TEventHandler.handleMessage(ev5);
		}
		
		new TEvent(TEventHandler.DATA_MANAGER, TGridPositionControler.ROTATE, alignment, 
				alignment.getGridModel().getAngle());
		
	    //TColumn column = (TColumn)alignment.getGridModel().getConfig().getColumns().getColumn(3);

		//new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.REMOVE_COLUMN, alignment, null, column);
		//new TEvent(TEventHandler.DATA_MANAGER, TGridColumnsControler.REMOVE_COLUMN, alignment, 9, null);
		
	}

	void plotGridCenter(TAlignment alignment, TSpot spot, Color c){
		
		//new TEvent(TEventHandler.DIALOG_MANAGER, TAlignmentGraphicPanel.,
		Double pixelHeight = alignment.getGridModel().getPixelHeight();
		Double pixelWidth = alignment.getGridModel().getPixelWidth();
		
		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
				new TImageGraphicPanel.HotLine(Color.green, (int)((spot.getX()+spot.getWidth()/2)/pixelWidth-2), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight), (int)((spot.getX()+spot.getWidth()/2)/pixelWidth+2), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight)));
		((TAlignmentGraphicPanel)alignment.getView().getGraphicPanel()).addHotLine(
				new TImageGraphicPanel.HotLine(Color.green,(int)((spot.getX()+spot.getWidth()/2)/pixelWidth), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight-2), (int)((spot.getX()+spot.getWidth()/2)/pixelWidth), (int)((spot.getY()+spot.getHeight()/2)/pixelHeight+2)));

	}
	
	
}
