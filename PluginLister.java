import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import agscan.Messages;
import agscan.menu.TMenuManager;
import agscan.menu.action.TAction;
import agscan.menu.action.TComputeDiameterAction;
import agscan.menu.action.TComputeFitCorrectionAction;
import agscan.menu.action.TComputeOvershiningCorrectionAction;
import agscan.menu.action.TComputeQMAction;
import agscan.menu.action.TComputeQuantifFitConstAction;
import agscan.menu.action.TComputeQuantifFitVarAction;
import agscan.menu.action.TComputeQuantifImageConstAction;
import agscan.menu.action.TComputeQuantifImageVarAction;
import agscan.menu.action.TComputeSpotQualityDefaultAction;
import agscan.menu.action.TComputeSpotQualityFourProfilesAction;
import agscan.menu.action.TComputeSpotQualityMultiChannelAction;
import agscan.menu.action.TComputeSpotQualityTwoProfilesAction;
import agscan.menu.action.TExportAsTextAction;
import agscan.menu.action.TGlobalAlignmentDefaultAction;
import agscan.menu.action.TGlobalAlignmentEdgesFinderAction;
import agscan.menu.action.TGlobalAlignmentSnifferAction;
import agscan.menu.action.TGlobalAlignmentTemplateMatchingAction;
import agscan.menu.action.TLocalAlignmentBlocAction;
import agscan.menu.action.TLocalAlignmentBlocTemplateAction;
import agscan.menu.action.TLocalAlignmentGridAction;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.SGridPlugin;
import agscan.plugins.SQuantifPlugin;
import agscan.plugins.TPlugin;
import agscan.plugins.TPluginManager;


public class PluginLister extends TPlugin{

	public static int id = -1;
	private Vector<String> v_align;
	private Vector<String> v_quantif;
	
	
	
	public PluginLister()
	{
		super("Listeur de plugins",null);
		v_align = new Vector<String>();
		v_quantif = new Vector<String>();
		
		this.id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(ACTIVE_TYPE); // sets the menu always active.

		/* R�cup�ration EN DUR des noms d'algos d'alignement */
		//detection de spots
		String spotdefault =((TAction)TMenuManager.getAction(TComputeSpotQualityDefaultAction.getID())).getValue(NAME).toString();
		String spot2prof = ((TAction)TMenuManager.getAction(TComputeSpotQualityTwoProfilesAction.getID())).getValue(NAME).toString();
		String spotmultichan = ((TAction)TMenuManager.getAction(TComputeSpotQualityMultiChannelAction.getID())).getValue(NAME).toString();
		String spot4prof =  ((TAction)TMenuManager.getAction(TComputeSpotQualityFourProfilesAction.getID())).getValue(NAME).toString();
		//alignement local
		String locdefault= ((TAction)TMenuManager.getAction(TGlobalAlignmentDefaultAction.getID())).getValue(NAME).toString();
		String locgrid= ((TAction)TMenuManager.getAction(TLocalAlignmentGridAction.getID())).getValue(NAME).toString();
		String loctempmatch = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocTemplateAction.getID())).getValue(NAME).toString();
		String locbloc = ((TAction)TMenuManager.getAction(TLocalAlignmentBlocAction.getID())).getValue(NAME).toString();
		//alignement global
		String globdefault = ((TAction)TMenuManager.getAction(TGlobalAlignmentDefaultAction.getID())).getValue(NAME).toString();
		String globedges = ((TAction)TMenuManager.getAction(TGlobalAlignmentEdgesFinderAction.getID())).getValue(NAME).toString();
		String globtempmatch = ((TAction)TMenuManager.getAction(TGlobalAlignmentTemplateMatchingAction.getID())).getValue(NAME).toString();
		String globsniffer = ((TAction)TMenuManager.getAction(TGlobalAlignmentSnifferAction.getID())).getValue(NAME).toString();
		

		/* R�cup�ration EN DUR des noms d'algos de quantification */
		String quantimconst= ((TAction)TMenuManager.getAction(TComputeQuantifImageConstAction.getID())).getValue(NAME).toString();
		String quantimvar= ((TAction)TMenuManager.getAction(TComputeQuantifImageVarAction.getID())).getValue(NAME).toString();
		String quantfitconst= ((TAction)TMenuManager.getAction(TComputeQuantifFitConstAction.getID())).getValue(NAME).toString();
		String quantfitvar = ((TAction)TMenuManager.getAction(TComputeQuantifFitVarAction.getID())).getValue(NAME).toString();
		String quantdiam= ((TAction)TMenuManager.getAction(TComputeDiameterAction.getID())).getValue(NAME).toString();
		String quantovershcorr= ((TAction)TMenuManager.getAction(TComputeOvershiningCorrectionAction.getID())).getValue(NAME).toString();
		String quantfitcorr = ((TAction)TMenuManager.getAction(TComputeFitCorrectionAction.getID())).getValue(NAME).toString();
		String quantQM = ((TAction)TMenuManager.getAction(TComputeQMAction.getID())).getValue(NAME).toString();
		
		
		/* Ajout des noms aux vecteurs v_align et v_quantif */
		v_align.add(locdefault);
		v_align.add(locgrid);
		v_align.add(loctempmatch);
		v_align.add(locbloc);
		v_align.add(globdefault);
		v_align.add(globedges);
		v_align.add(globtempmatch);
		v_align.add(globsniffer);
		v_align.add(spotdefault);
		v_align.add(spot2prof);
		v_align.add(spotmultichan);
		v_align.add(spot4prof);
		v_quantif.add(quantimconst);
		v_quantif.add(quantimvar);
		v_quantif.add(quantfitconst);
		v_quantif.add(quantfitvar);
		v_quantif.add(quantdiam);
		v_quantif.add(quantovershcorr);
		v_quantif.add(quantfitcorr);
		v_quantif.add(quantQM);
		
		
		
	}
	
	public String getName() {
        return (String) this.getValue(NAME);
    }
	
	// classe d'affichage de la liste des plugins
	public class FenetreListe extends JFrame
	{
		// les donn�es et les JList
		private Vector<String> modalign;
		private Vector<String> modquantif;
		private Vector<String> moddivers;
		private JList quantif;
		private JList align;
		private JList divers;
		private JList config;
		private JButton b_add;
		private JButton b_rem;
		
		public FenetreListe()
		{
			// instanciations, choix d'une GridLayout 6x3
			Icon ic_add = new ImageIcon("/home/nmary/add.gif");
			Action act_add = new AbstractAction("ajouter",ic_add) {
				public void actionPerformed(ActionEvent e)
				{
					
				}
			};
			b_add = new JButton(act_add);
			
			Icon ic_rem = new ImageIcon("/home/nmary/rem.gif");
			Action act_rem = new AbstractAction("enlever",ic_rem) {
				public void actionPerformed(ActionEvent e)
				{
					
				}
			};
			b_rem = new JButton(act_rem);
			
			modalign = new Vector<String>();
			modquantif = new Vector<String>();
			moddivers = new Vector<String>();
			JPanel pan = new JPanel();
			JPanel configpan = new JPanel();
			pan.setLayout(new GridLayout(6,1));
			configpan.setLayout(new GridLayout(3,1));
			align = new JList(modalign);
			align.setVisibleRowCount(5);
			quantif = new JList(modquantif);
			quantif.setVisibleRowCount(5);
			divers = new JList(moddivers);
			divers.setVisibleRowCount(5);
			JScrollPane scrollali = new JScrollPane(align);
			scrollali.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			JScrollPane scrollquantif = new JScrollPane(quantif);
			scrollquantif.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			JScrollPane scrolldivers= new JScrollPane(divers);
			scrolldivers.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			JScrollPane scrollconfig= new JScrollPane(config);
			scrollconfig.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			
			//1ere colonne
			//ajout des composants : 1 titre puis 1 liste (pour chaque liste)
			JLabel titre1 = new JLabel("<html><font size=\"4\"color=\"blue\">Plugins pour l'alignement</font></html>");
			pan.add(titre1);
			pan.add(scrollali);
			JLabel titre2 = new JLabel("<html><font size=\"4\"color=\"red\">Plugins pour la quantification</font></html>");
			pan.add(titre2);
			pan.add(scrollquantif);
			JLabel titre3 = new JLabel("<html><font size=\"4\"color=\"green\">Plugins divers</font></html>");
			pan.add(titre3);
			pan.add(scrolldivers);
			
			
			//2e colonne
			//
			JLabel titre4 = new JLabel("<html><font size=\"5\"color=\"purple\">Configuration choisie</font></html>");
			config = new JList();
			config.setVisibleRowCount(20);
			configpan.add(titre4);
			configpan.add(scrollconfig);
			JPanel boutpan = new JPanel();
			boutpan.setLayout(new FlowLayout());
			boutpan.add(b_add);
			boutpan.add(b_rem);
			configpan.add(boutpan);
			
			JSplitPane globpan = new JSplitPane(
		            JSplitPane.HORIZONTAL_SPLIT,
		            pan,configpan);
			
			this.setContentPane(globpan);
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		
		// Cree la fenetre d'affichage de la liste des plugins
		FenetreListe fenetrelist = new FenetreListe();
		// Ajoute un titre 
		fenetrelist.setTitle("Liste des plugins charges");
		// Ajuste la taille de la fenetre et l'affiche avec le Look&FeelDeco
		fenetrelist.setVisible(true);
		fenetrelist.pack();
		JFrame.setDefaultLookAndFeelDecorated(true);

		// tri des plugins
		for (int i=0; i< TPluginManager.plugs.length;i++){
			TPlugin plugin = TPluginManager.plugs[i];
			if (plugin != null) {
				
				// plugin d'alignement ajout� a la liste correspondante
				if ((plugin instanceof SAlignPlugin)
						||(plugin instanceof SGridPlugin))
				{
					fenetrelist.modalign.add(plugin.getName());
					fenetrelist.align.setListData(fenetrelist.modalign);
				}
				// plugin de quantification ajout� a la liste correspondante
				else if (plugin instanceof SQuantifPlugin)
				{
					fenetrelist.modquantif.add(plugin.getName());
					fenetrelist.quantif.setListData(fenetrelist.modquantif);
				}
				// plugin "divers" ajout� a la liste correspondante
				else
				{
					fenetrelist.moddivers.add(plugin.getName());
					fenetrelist.divers.setListData(fenetrelist.moddivers);
				}
			}
		}
		
		//tri des algos align
		for(int i=0;i<v_align.size();i++)
		{
			fenetrelist.modalign.add(v_align.get(i));
			fenetrelist.align.setListData(fenetrelist.modalign);
		}
		//tri des algos quantif
		for(int i=0;i<v_quantif.size();i++)
		{
			fenetrelist.modquantif.add(v_quantif.get(i));
			fenetrelist.quantif.setListData(fenetrelist.modquantif);
		}
		
	}
}
