

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Material;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import agscan.TEventHandler;
import agscan.algo.fit.TFitAlgorithm;
import agscan.algo.fit.TGaussNewtonFitAlgorithm;
import agscan.algo.fit.TNormalFitAlgorithm;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.image.TImage;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.grid.TSpot;
import agscan.data.view.graphic.TGraphicPanel;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.menu.TMenuManager;

import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;

/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class T3DViewInteractor extends TGraphicInteractor {
	private static T3DViewInteractor instance = null;
	private Rectangle cropRect;
	private int x0, y0, image_transp, fit_transp, background_transp, zoom_Z, zoom_XY;
	private boolean resize, rectOver, fit, image, background, image_surf, fit_surf, background_surf, image_color, fit_color, zone_3D, show_spots;
	private static GeometryInfo gi_image = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY), gi_fit = null, gi_background = null;//dans TAlignmentControler avant...
	private static Matrix4f mat = new Matrix4f();//idem ajout� 2005/12/15
	public T3DViewInteractor() {
		super();
		cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
		cropRect = null;
		resize = rectOver = fit = background = fit_surf = background_surf = fit_color = show_spots = false;
		image = image_surf = image_color = zone_3D = true;
		image_transp = fit_transp = background_transp = 0;
		zoom_Z = zoom_XY = 50;
	}
	public static T3DViewInteractor getInstance() {
		if (instance == null)
			instance = new T3DViewInteractor();
		return instance;
	}
	public static String getName() {
		return "3d";
	}
	public void mousePressed(MouseEvent event) {
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		double zoom = element.getView().getGraphicPanel().getZoom();
		if (zoom < 0) zoom = 1.0D / -zoom;
		x0 = (int)((double)event.getX() / zoom);
		y0 = (int)((double)event.getY() / zoom);
		if (cropRect == null) {
			cropRect = new Rectangle(x0, y0, 0, 0);
			element.getView().getGraphicPanel().repaint();
		}
		else if (!rectOver) {
			cropRect = new Rectangle(x0, y0, 0, 0);
			element.getView().getGraphicPanel().repaint();
		}
		else if (cropRect.contains(x0, y0)) {
			
		}
		else if (!resize) {
			cropRect = new Rectangle(x0, y0, 0, 0);
			rectOver = false;
			element.getView().getGraphicPanel().repaint();
		}
	}
	public void mouseReleased(MouseEvent event) {
		rectOver = true;
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		Object p[] = { new Integer(cropRect.x), new Integer(cropRect.y), new Integer(cropRect.width), new Integer(cropRect.height),
				new Boolean(image), new Boolean(fit), new Boolean(background), new Boolean(image_surf), new Boolean(fit_surf), new Boolean(background_surf),
				new Boolean(image_color), new Boolean(fit_color), new Integer(image_transp), new Integer(fit_transp), new Integer(background_transp),
				new Integer(zoom_Z), new Integer(zoom_XY), new Boolean(zone_3D), new Boolean(show_spots) };
		T3DTool.set3DParams(p);
		//
		createSceneGraph(p,true,element);//modif 2005/12/14
		
		///*/ ev = new TEvent(TEventHandler.DATA_MANAGER, TControlerImpl.CREATE_SCENE_GRAPH, element, p, new Boolean(true));
		///*/TEventHandler.handleMessage(ev);
		// ev = new TEvent(TEventHandler.TOOL_MANAGER, TToolManager.UPDATE, element, null);
		// TEventHandler.handleMessage(ev);
	}
	public void mouseMoved(MouseEvent event) {
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		TGraphicPanel panel = element.getView().getGraphicPanel();
		double zoom = element.getView().getGraphicPanel().getZoom();
		if (zoom < 0) zoom = 1.0D / -zoom;
		int x = (int)((double)event.getX() / zoom);
		int y = (int)((double)event.getY() / zoom);
		
		resize = false;
		if (rectOver) {
			if ((x > (cropRect.x + cropRect.width - 3)) && (x < (cropRect.x + cropRect.width + 3)) && (y > (cropRect.height + cropRect.y - 3)) && (y < (cropRect.height + cropRect.y + 3))) {
				panel.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
				resize = true;
			}
			else
				panel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		}
	}
	public void mouseDragged(MouseEvent event) {
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement) TEventHandler.handleMessage(ev)[0];
		double zoom = element.getView().getGraphicPanel().getZoom();
		if (zoom < 0) zoom = 1.0D / -zoom;
		int x = (int)((double)event.getX() / zoom);
		int y = (int)((double)event.getY() / zoom);
		int rx, ry, rh, rw;
		
		if (!rectOver) {
			if ((x < element.getModel().getElementWidth()) && (y < element.getModel().getElementHeight())) {
				rx = cropRect.x - (x - cropRect.x - cropRect.width);
				ry = cropRect.y - (y - cropRect.y - cropRect.height);
				rw = x - rx;
				rh = y - ry;
				cropRect.setLocation(rx, ry);
				cropRect.setSize(rw, rh);
				element.getView().getGraphicPanel().repaint();
			}
		}
		else {
			if (resize) {
				if ((cropRect.y + cropRect.height - y0 + y) >= element.getModel().getElementHeight())
					y0 = y;
				else if ((cropRect.x + cropRect.width - x0 + x) >= element.getModel().getElementWidth())
					x0 = x;
				else if (((cropRect.height - y0 + y) >= 10) && ((cropRect.width - x0 + x) >= 10)) {
					rw = cropRect.width - x0 + x;
					rh = cropRect.height - y0 + y;
					cropRect.setSize(rw, rh);
					element.getView().getGraphicPanel().repaint();
					x0 = x;
					y0 = y;
				}
			}
			else {
				if (((cropRect.x + x - x0) >= 0) && ((cropRect.y + y - y0) >= 0) &&
						((cropRect.x + cropRect.width + x - x0) < element.getModel().getElementWidth()) &&
						((cropRect.y + cropRect.height + y - y0) < element.getModel().getElementHeight())) {
					cropRect.translate(x - x0, y - y0);
					element.getView().getGraphicPanel().repaint();
				}
				x0 = x;
				y0 = y;
			}
		}
	}
	
	public void draw(Graphics g, double z) {
		if (cropRect != null) {
			g.setColor(Color.red);
			g.drawRect((int)(cropRect.x * z), (int)(cropRect.y * z), (int)(cropRect.width * z), (int)(cropRect.height * z));
		}
	}
	public void reset() {
		cropRect = null;
	}
	public void init(TDataElement elem) {
		Object[] i = T3DTool.get3DParams();
		int x = ((Integer)i[0]).intValue();
		int y = ((Integer)i[1]).intValue();
		int w = ((Integer)i[2]).intValue();
		int h = ((Integer)i[3]).intValue();
		if ((x > 0) || (y > 0) || (w > 0) || (h > 0)) {
			cropRect = new Rectangle(x, y, w, h);
			rectOver = true;
		}
		else {
			cropRect = null;
			rectOver = false;
		}
		image = ((Boolean)i[4]).booleanValue();
		fit = ((Boolean)i[5]).booleanValue();
		background = ((Boolean)i[6]).booleanValue();
		image_surf = ((Boolean)i[7]).booleanValue();
		fit_surf = ((Boolean)i[8]).booleanValue();
		background_surf = ((Boolean)i[9]).booleanValue();
		image_color = ((Boolean)i[10]).booleanValue();
		fit_color = ((Boolean)i[11]).booleanValue();
		image_transp = ((Integer)i[12]).intValue();
		fit_transp = ((Integer)i[13]).intValue();
		background_transp = ((Integer)i[14]).intValue();
		zoom_Z = ((Integer)i[15]).intValue();
		zoom_XY = ((Integer)i[16]).intValue();
		zone_3D = ((Boolean)i[17]).booleanValue();
		show_spots = ((Boolean)i[18]).booleanValue();
		resize = false;
	}
	
	//pris dans TAlignment Controler
	public static void createSceneGraph(Object[] params, boolean compute,TDataElement element) {
		int w ,h,x,y,width,height;
		boolean image;
		compute=true;//tststststst
		if (element instanceof TAlignment){
			TAlignmentModel alignmentModel = (TAlignmentModel)element.getModel();
			BranchGroup oldBranchGroup = T3DTool.getBranchGroup();
			BranchGroup objRoot = new BranchGroup();
			TransformGroup objTransform = new TransformGroup();
			Transform3D tr3d = new Transform3D();
			objTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			objTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
			objRoot.setCapability(BranchGroup.ALLOW_DETACH);
			w = ((TAlignment)element).getImage().getImageModel().getElementWidth();
			h = ((TAlignment)element).getImage().getImageModel().getElementHeight();
			x = ((Integer)params[0]).intValue();
			y = ((Integer)params[1]).intValue();
			width = ((Integer)params[2]).intValue();
			height = ((Integer)params[3]).intValue();		
			image = ((Boolean)params[4]).booleanValue();
			System.out.println("image="+image);
			boolean fit = ((Boolean)params[5]).booleanValue();
			boolean background = ((Boolean)params[6]).booleanValue();
			int xRect = ((Integer)T3DTool.get3DParams()[0]).intValue() * (int)alignmentModel.getPixelWidth();
			int yRect = ((Integer)T3DTool.get3DParams()[1]).intValue() * (int)alignmentModel.getPixelHeight();
			int wRect = ((Integer)T3DTool.get3DParams()[2]).intValue() * (int)alignmentModel.getPixelWidth();
			int hRect = ((Integer)T3DTool.get3DParams()[3]).intValue() * (int)alignmentModel.getPixelHeight();
			
			Vector spotsPoints = null;
			if ((width > 0) && (height > 0) && (x > 0) && (y > 0)) {
				float rap = 0.02F;
				int[] dataImage = ((TAlignment)element).getImage().getImageModel().getMaxImageData();//FAIT SUR LE MAX!!!//getImageData();
				int[] dataFit = null;
				if (compute) {
					dataFit = getFitData((TAlignmentModel)element.getModel(), xRect, yRect, wRect, hRect);
					spotsPoints = getSpotsPoints((TAlignmentModel)element.getModel());
					int tmp, n, bbb;
					for (int k = x; k < (x + width - 1); k++) {
						for (int l = y ; l < (y + height - 1); l++) {
							bbb = k + l * w;
							if (dataFit[bbb] == 0) {
								tmp = n = 0;
								int xx = k;
								while (xx >= 0)
									if (dataFit[l * w + xx] == 0)
										xx--;
									else
										break;
								if (xx >= 0) {
									tmp += dataFit[l * w + xx]; n++;
								}
								xx = k;
								while (xx < w)
									if (dataFit[l * w + xx] == 0)
										xx++;
									else
										break;
								if (xx < w) {
									tmp += dataFit[l * w + xx]; n++;
								}
								int yy = l;
								while (yy >= 0)
									if (dataFit[yy * w + k] == 0)
										yy--;
									else
										break;
								if (yy >= 0) {
									tmp += dataFit[yy * w + k]; n++;
								}
								yy = l;
								while (yy < h)
									if (dataFit[yy * w + k] == 0)
										yy++;
									else
										break;
								if (yy < l) {
									tmp += dataFit[yy * w + k]; n++;
								}
								if (n != 0) {
									tmp /= n;
									dataFit[bbb] = tmp;
								}
							}
						}
					}
				}
				int n;
				float z, zmoy = 0;
				float zmax = 0, zmin = 1;
				if (compute) {
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							z = (dataImage[j + i * w] & 65535) / 65535.0F;
							//System.out.println("z="+z);
							if (z > zmax) zmax = z;
							if (z < zmin) zmin = z;
						}
					}
					zmoy = (zmin + zmax) / 2.0f;
				}
				
				NormalGenerator ng = new NormalGenerator();
				int tab[] = new int[height];
				for (int i = 0; i < height; i++) tab[i] = 2 * width;
				System.out.println("compute="+compute);
				
				if (compute) {
					Color3f[] color_image = new Color3f[2 * width * height];
					Point3f[] coord_image = new Point3f[2 * width * height];
					gi_image = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);
					gi_image.setStripCounts(tab);
					n = 0;
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							for (int k = i; k < i + 2; k++) {
								z = (dataImage[j + k * w] & 65535) / 65535.0F;
								coord_image[n] = new Point3f( (j - x - width / 2) * rap, - (k - y - height / 2) * rap, z - zmoy);
								if (((Boolean)params[10]).booleanValue()) {
									if (z <= 0.5F)
										color_image[n] = new Color3f(0.0F, 2.0F * z, (1.0F - 2.0F * z));
									else if (z == 1.0F)
										color_image[n] = new Color3f(1.0F, 0.0F, 1.0F);
									else
										color_image[n] = new Color3f(2.0F * (z - 0.5F), (1.0F - z) * 2.0F, 0);
								}
								else {
									color_image[n] = new Color3f(0.3F, 0.3F, 0.3F);
								}
								if (((Boolean)params[18]).booleanValue()) {
									if (spotsPoints.contains(new Point2D.Double(j, k)))
										color_image[n] = new Color3f(1.0F, 1.0F, 1.0F);
								}
								n++;
							}
						}
					}
					gi_image.setCoordinates(coord_image);
					gi_image.setColors(color_image);
					ng.generateNormals(gi_image);
					Point3f[] coord_fit = new Point3f[2 * width * height];
					Color3f[] color_fit = new Color3f[2 * width * height];
					gi_fit = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);
					gi_fit.setStripCounts(tab);
					n = 0;
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							for (int k = i; k < i + 2; k++) {
								z = (dataFit[j + k * w]) / 65535.0F;
								coord_fit[n] = new Point3f( (j - x - width / 2) * rap, - (k - y - height / 2) * rap, z - zmoy);
								if (((Boolean)params[11]).booleanValue()) {
									if (z <= 0.5F)
										color_fit[n] = new Color3f(0.0F, 2.0F * z, (1.0F - 2.0F * z));
									else if (z == 1.0F)
										color_fit[n] = new Color3f(1.0F, 0.0F, 1.0F);
									else if (z < 1.0F)
										color_fit[n] = new Color3f(2.0F * (z - 0.5F), (1.0F - z) * 2.0F, 0);
									else
										color_fit[n] = new Color3f(1, 1, 0);
								}
								else {
									color_fit[n] = new Color3f(0.3F, 0.3F, 0.3F);
								}
								n++;
							}
						}
					}
					gi_fit.setCoordinates(coord_fit);
					gi_fit.setColors(color_fit);
					ng.generateNormals(gi_fit);
					Point3f[] coord_background = new Point3f[2 * width * height];
					Color3f[] color_background = new Color3f[2 * width * height];
					double lat = ImagePlusTools.getLatitude();//((TAlignmentModel)element.getModel()).getImageModel().getLatitude();
					double cii = 0;///*getBackground((TAlignmentModel)element.getModel());
					cii = 65535.0D * (Math.log(cii) / Math.log(10) / lat + 0.5D);
					gi_background = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);
					gi_background.setStripCounts(tab);
					n = 0;
					z = (float)cii / 65535.0F;
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							for (int k = i; k < i + 2; k++) {
								coord_background[n] = new Point3f( (j - x - width / 2) * rap, - (k - y - height / 2) * rap, z - zmoy);
								color_background[n] = new Color3f(1.0F, 1.0F, 0.0F);
								n++;
							}
						}
					}
					gi_background.setCoordinates(coord_background);
					gi_background.setColors(color_background);
					ng.generateNormals(gi_background);
				}
				Appearance app_image = new Appearance();
				PolygonAttributes polyAttribImage = new PolygonAttributes();
				polyAttribImage.setCullFace(PolygonAttributes.CULL_NONE);
				Material matos = new Material();
				matos.setLightingEnable(true);
				if (((Boolean)params[7]).booleanValue()) {
					polyAttribImage.setPolygonMode(PolygonAttributes.POLYGON_FILL);
					polyAttribImage.setBackFaceNormalFlip(true);
				}
				else
					polyAttribImage.setPolygonMode(PolygonAttributes.POLYGON_LINE);
				app_image.setPolygonAttributes(polyAttribImage);
				app_image.setMaterial(matos);
				Appearance app_fit = new Appearance();
				PolygonAttributes polyAttribFit = new PolygonAttributes();
				polyAttribFit.setCullFace(PolygonAttributes.CULL_NONE);
				if (((Boolean)params[8]).booleanValue()) {
					polyAttribFit.setPolygonMode(PolygonAttributes.POLYGON_FILL);
					polyAttribFit.setBackFaceNormalFlip(true);
				}
				else
					polyAttribFit.setPolygonMode(PolygonAttributes.POLYGON_LINE);
				app_fit.setPolygonAttributes(polyAttribFit);
				app_fit.setMaterial(matos);
				Appearance app_background = new Appearance();
				PolygonAttributes polyAttribBackground = new PolygonAttributes();
				polyAttribBackground.setCullFace(PolygonAttributes.CULL_NONE);
				if (((Boolean)params[9]).booleanValue()) {
					polyAttribBackground.setPolygonMode(PolygonAttributes.POLYGON_FILL);
					polyAttribBackground.setBackFaceNormalFlip(true);
				}
				else
					polyAttribBackground.setPolygonMode(PolygonAttributes.POLYGON_LINE);
				app_background.setPolygonAttributes(polyAttribBackground);
				app_background.setMaterial(matos);
				
				float transp = ((Integer)params[14]).floatValue() / 100.0F;
				if (((Boolean)params[9]).booleanValue() && (transp != 0)) {
					app_background.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.FASTEST, transp));
				}
				transp = ((Integer)params[13]).floatValue() / 100.0F;
				if (((Boolean)params[8]).booleanValue() && (transp != 0)) {
					app_fit.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.FASTEST, transp));
				}
				transp = ((Integer)params[12]).floatValue() / 100.0F;
				if (((Boolean)params[7]).booleanValue() && (transp != 0)) {
					app_image.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.FASTEST, transp));
				}
				 // gi_image = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);//ajoute test!!!!!!!
				if (image) objTransform.addChild(new Shape3D(gi_image.getGeometryArray(), app_image));
				if (fit) objTransform.addChild(new Shape3D(gi_fit.getGeometryArray(), app_fit));
				if (background) objTransform.addChild(new Shape3D(gi_background.getGeometryArray(), app_background));
				
				if (oldBranchGroup != null)
					tr3d.set(mat);
				else
					tr3d.get(mat);
				tr3d.setScale(new Vector3d(((Integer)params[16]).intValue() * 5.0d / 100.0d, ((Integer)params[16]).intValue() * 5.0d / 100.0d, ((Integer)params[15]).intValue() * 5.0d / 100.0d));
				objTransform.setTransform(tr3d);
				
				objRoot.addChild(objTransform);
				
				MouseRotateWithZ myMouseRotate = new MouseRotateWithZ(mat);
				myMouseRotate.setFactor(0.01, 0.01);
				myMouseRotate.setTransformGroup(objTransform);
				myMouseRotate.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseRotate);
				
				MouseTranslate myMouseTranslate = new MouseTranslate() {
					public void transformChanged(Transform3D transform) {
						transform.get(mat);
					}
				};
				myMouseTranslate.setTransformGroup(objTransform);
				myMouseTranslate.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseTranslate);
				
				MouseZoom myMouseZoom = new MouseZoom(){
					public void transformChanged(Transform3D transform) {
						transform.get(mat);
					}
				};
				myMouseZoom.setTransformGroup(objTransform);
				myMouseZoom.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseZoom);
				
				AmbientLight al = new AmbientLight(new Color3f(1.0f, 1.0f, 1.0f));
				al.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(al);
				
				DirectionalLight dl1 = new DirectionalLight(true, new Color3f(1,1,1), new Vector3f(0, -2, 0));
				dl1.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl1);
				
				DirectionalLight dl2 = new DirectionalLight(true, new Color3f(0.5f,0.5f,0.5f), new Vector3f(0, 2, 0));
				dl2.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl2);
				
				DirectionalLight dl3 = new DirectionalLight(true, new Color3f(0.5f,0.5f,0.5f), new Vector3f(0, 0, -2));
				dl3.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl3);
				
				objRoot.compile();
				T3DTool.setBranchGroup(objRoot);
			}
		}//end of "if (element instanceof TAlignment)"
		
		
		else if(element instanceof TImage){
			BranchGroup oldBranchGroup = T3DTool.getBranchGroup();
			BranchGroup objRoot = new BranchGroup();
			TransformGroup objTransform = new TransformGroup();
			Transform3D tr3d = new Transform3D();
			objTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			objTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
			objRoot.setCapability(BranchGroup.ALLOW_DETACH);
			w = ((TImage)element).getImageModel().getElementWidth();
			h = ((TImage)element).getImageModel().getElementHeight();
			x = ((Integer)params[0]).intValue();
			y = ((Integer)params[1]).intValue();
			width = ((Integer)params[2]).intValue();
			height = ((Integer)params[3]).intValue();
			image = ((Boolean)params[4]).booleanValue();//true?
			System.out.println("image="+image);
			System.out.println("width="+width);
			System.out.println("height="+height);
			System.out.println("x="+x);
			System.out.println("y="+y);
			if ((width > 0) && (height > 0) && (x > 0) && (y > 0)) {
				float rap = 0.02F;
				int[] dataImage = ((TImage)element).getImageModel().getMaxImageData();//getImageData();
				/*	for (int i=0;i<dataImage.length;i++){
				 System.out.println(i+"= "+dataImage[i]);
				 }
				 */
				int n;
				float z, zmoy = 0;
				float zmax = 0, zmin = 1;
				if (compute) {
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							z = (dataImage[j + i * w] & 65535) / 65535.0F;
							if (z > zmax) zmax = z;
							if (z < zmin) zmin = z;
						}
					}
					zmoy = (zmin + zmax) / 2.0f;
				}
				
				NormalGenerator ng = new NormalGenerator();
				int tab[] = new int[height];
				for (int i = 0; i < height; i++) tab[i] = 2 * width;
				System.out.println("compute="+compute);
				if (compute) {
					Color3f[] color_image = new Color3f[2 * width * height];
					Point3f[] coord_image = new Point3f[2 * width * height];
					gi_image = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);
					gi_image.setStripCounts(tab);
					n = 0;
					for (int i = y; i < y + height; i++) {
						for (int j = x; j < x + width; j++) {
							for (int k = i; k < i + 2; k++) {
								z = (dataImage[j + k * w] & 65535) / 65535.0F;
								coord_image[n] = new Point3f( (j - x - width / 2) * rap, - (k - y - height / 2) * rap, z - zmoy);
								if (((Boolean)params[10]).booleanValue()) {
									if (z <= 0.5F)
										color_image[n] = new Color3f(0.0F, 2.0F * z, (1.0F - 2.0F * z));
									else if (z == 1.0F)
										color_image[n] = new Color3f(1.0F, 0.0F, 1.0F);
									else
										color_image[n] = new Color3f(2.0F * (z - 0.5F), (1.0F - z) * 2.0F, 0);
								}
								else {
									color_image[n] = new Color3f(0.3F, 0.3F, 0.3F);
								}
								n++;
							}
						}
					}
					gi_image.setCoordinates(coord_image);
					gi_image.setColors(color_image);
					ng.generateNormals(gi_image);
				}
				Appearance app_image = new Appearance();
				PolygonAttributes polyAttribImage = new PolygonAttributes();
				polyAttribImage.setCullFace(PolygonAttributes.CULL_NONE);
				Material matos = new Material();
				matos.setLightingEnable(true);
				if (((Boolean)params[7]).booleanValue()) {
					polyAttribImage.setPolygonMode(PolygonAttributes.POLYGON_FILL);
					polyAttribImage.setBackFaceNormalFlip(true);
				}
				else
					polyAttribImage.setPolygonMode(PolygonAttributes.POLYGON_LINE);
				app_image.setPolygonAttributes(polyAttribImage);
				app_image.setMaterial(matos);
				
				float transp = ((Integer)params[14]).floatValue() / 100.0F;
				transp = ((Integer)params[12]).floatValue() / 100.0F;
				if (((Boolean)params[7]).booleanValue() && (transp != 0)) {
					app_image.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.FASTEST, transp));
				}
				if (image) objTransform.addChild(new Shape3D(gi_image.getGeometryArray(), app_image));
				
				if (oldBranchGroup != null)
					tr3d.set(mat);
				else
					tr3d.get(mat);
				tr3d.setScale(new Vector3d(((Integer)params[16]).intValue() * 5.0d / 100.0d, ((Integer)params[16]).intValue() * 5.0d / 100.0d, ((Integer)params[15]).intValue() * 5.0d / 100.0d));
				objTransform.setTransform(tr3d);
				
				objRoot.addChild(objTransform);
				
				MouseRotateWithZ myMouseRotate = new MouseRotateWithZ(mat);
				myMouseRotate.setFactor(0.01, 0.01);
				myMouseRotate.setTransformGroup(objTransform);
				myMouseRotate.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseRotate);
				
				MouseTranslate myMouseTranslate = new MouseTranslate() {
					public void transformChanged(Transform3D transform) {
						transform.get(mat);
					}
				};
				myMouseTranslate.setTransformGroup(objTransform);
				myMouseTranslate.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseTranslate);
				
				MouseZoom myMouseZoom = new MouseZoom(){
					public void transformChanged(Transform3D transform) {
						transform.get(mat);
					}
				};
				myMouseZoom.setTransformGroup(objTransform);
				myMouseZoom.setSchedulingBounds(new BoundingSphere());
				objRoot.addChild(myMouseZoom);
				
				AmbientLight al = new AmbientLight(new Color3f(1.0f, 1.0f, 1.0f));
				al.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(al);
				
				DirectionalLight dl1 = new DirectionalLight(true, new Color3f(1,1,1), new Vector3f(0, -2, 0));
				dl1.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl1);
				
				DirectionalLight dl2 = new DirectionalLight(true, new Color3f(0.5f,0.5f,0.5f), new Vector3f(0, 2, 0));
				dl2.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl2);
				
				DirectionalLight dl3 = new DirectionalLight(true, new Color3f(0.5f,0.5f,0.5f), new Vector3f(0, 0, -2));
				dl3.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0));
				objRoot.addChild(dl3);
				
				objRoot.compile();
				T3DTool.setBranchGroup(objRoot);
				
				
			}//end of "if (element instanceof TImage)"
			
		}
		
		
	}
	
	//take from TAlignementControler
	private double getBackground(TAlignmentModel alignmentModel) {
		TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
		int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
		TFitAlgorithm fitAlgo = null;
		if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
			fitAlgo = new TGaussNewtonFitAlgorithm();
		else if (fitAlg == TMenuManager.FIT_NORMAL)
			fitAlgo = new TNormalFitAlgorithm();
		fitAlgo.initData(alignmentModel);
		///*/return fitAlgo.getBackground();
		return 0.0d;//REMI  EN ATTENDANT
		//TODO CORRIGER CA !!!!!!!!!!!!
	}	
	//	take from TAlignementControler
	private static int[] getFitData(TAlignmentModel alignmentModel, int x, int y, int w, int h) {
		TEvent ev = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager.GET_FIT_ALGORITHM, null);
		int fitAlg = ((Integer)TEventHandler.handleMessage(ev)[0]).intValue();
		TFitAlgorithm fitAlgo = null;
		if (fitAlg == TMenuManager.FIT_GAUSS_NEWTON)
			fitAlgo = new TGaussNewtonFitAlgorithm();
		else if (fitAlg == TMenuManager.FIT_NORMAL)
			fitAlgo = new TNormalFitAlgorithm();
		fitAlgo.initData(alignmentModel);
		Rectangle rect = new Rectangle(x, y, w, h);
		Vector spots = alignmentModel.getGridModel().getSpots(rect);
		
		double[] fitData = null;
		TSpot spot;
		Enumeration enume;
		double xRes = alignmentModel.getPixelWidth();
		double yRes = alignmentModel.getPixelHeight();
		Point2D.Double point;
		double fitQL;
		int bbb;
		int imageWidth = alignmentModel.getElementWidth(), imageHeight = alignmentModel.getElementHeight();
		double lat = ImagePlusTools.getLatitude();//alignmentModel.getImageModel().getLatitude();
		int[] thData = new int[imageWidth * imageHeight];
		int fond = 0;///*//(int)fitAlgo.getBackground();
		for (int i = 0; i < thData.length; i++) thData[i] = fond;
		for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements(); ) {
			spot = (TSpot)enu_spots.nextElement();
			if (spot.getFitAlgorithm() != null)
				fitData = fitAlgo.getFitData(spot);
			else {
				fitAlgo.execute(true);//au pif true au lieu de spot!!!!!!!!!!!///*/execute(spot);
				fitData = fitAlgo.getFitData(spot);
			}
			enume = spot.getPoints2DRectangle(xRes, yRes).elements();
			int n = 0;
			while (enume.hasMoreElements()) {
				point = (Point2D.Double)enume.nextElement();
				bbb = (int)point.getX()+ (int)point.getY() * imageWidth;
				fitQL = 65535.0D * (Math.log(fitData[n++]) / Math.log(10) / lat + 0.5D);
				if (fitQL < 0) fitQL = 0;
				if (thData[bbb] == fond)
					thData[bbb] = (int)fitQL;
				else
					thData[bbb] = Math.max(thData[bbb], (int)fitQL);
			}
		}
		return thData;
	}
	//	take from TAlignementControler
	private static Vector getSpotsPoints(TAlignmentModel alignmentModel) {
		Vector v = new Vector();
		int x = ((Integer)T3DTool.get3DParams()[0]).intValue() * (int)alignmentModel.getPixelWidth();
		int y = ((Integer)T3DTool.get3DParams()[1]).intValue() * (int)alignmentModel.getPixelHeight();
		int w = ((Integer)T3DTool.get3DParams()[2]).intValue() * (int)alignmentModel.getPixelWidth();
		int h = ((Integer)T3DTool.get3DParams()[3]).intValue() * (int)alignmentModel.getPixelHeight();
		
		Rectangle rect = new Rectangle(x, y, w, h);
		Vector spots = alignmentModel.getGridModel().getSpots(rect);
		TSpot spot;
		for (Enumeration enu_spots = spots.elements(); enu_spots.hasMoreElements(); ) {
			spot = (TSpot) enu_spots.nextElement();
			v.addAll(spot.getShapePixels(alignmentModel.getPixelWidth(), alignmentModel.getPixelHeight()));
		}
		return v;
	}
	
	
	
}
