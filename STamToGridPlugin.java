/*
 * Created on 3th April 2008
 * The STamToGridPlugin allows to generate a grid forom a TAM file
 * 
 * @author Alexis JULIN
 * @see SGridPlugin
 */


import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;

import agscan.FenetrePrincipale;
import agscan.ImageFilter;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.data.TDataManager;
import agscan.data.controler.TGridColumnsControler;
import agscan.data.controler.TGridStructureControler;
import agscan.data.element.grid.TGrid;
import agscan.data.model.grid.TGridModel;
import agscan.dialog.TDialogManager;
import agscan.event.TEvent;
import agscan.ioxml.TDefaultFileChooser;
import agscan.menu.action.TAction;
import agscan.plugins.SGridPlugin;
import agscan.data.model.grid.TGridConfig;

public class STamToGridPlugin extends SGridPlugin {

	
	public STamToGridPlugin() {
		//TODO externalize
		super("Generate grid from TAM file",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets when the menu is active 		
	}
	
	public void run() {
		String adress;
		adress=askTamAdresse();
		
		
		
		try{
			//recuperation des donn�es pour construire la grille
			int lvl1Param[]=getLevel1Param(adress);
			int lvl2Param[]=getLevel2Param(adress);
			
		
			//construction de la grille
			TGridConfig gc=new TGridConfig();
			TGrid grid=new TGrid(gc);
			
			
			//affichage de la grille
		    TEvent event = new TEvent(TEventHandler.DATA_MANAGER, 
		    TDataManager.ADD_DATA_ELEMENT, null, grid);
		    TEventHandler.handleMessage(event);
		    event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.ADD_VIEW, grid.getView());
		    TEventHandler.handleMessage(event);
		    event = new TEvent(TEventHandler.DIALOG_MANAGER,TDialogManager.NEW_GRID, grid);
		    TEventHandler.handleMessage(event);
		    
		    
		    //specification des caracteristiques de la grille
		    
		    //caract�ristiques de la grille
		    //en prend en compte le fait que la grille puisse �tre multi-niveaux ou non
		    if(lvl2Param[0]*lvl2Param[1]!=1){
		    	//nombre de colonne de la grille
		    	TEvent ev = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_NB_COLUMNS, null,lvl2Param[0]);
		    	TEventHandler.handleMessage(ev);
			
		    	//nombre de ligne de la grille
		    	TEvent ev2 = new TEvent(TEventHandler.DATA_MANAGER, TGridStructureControler.LEVEL2_NB_ROWS, null, lvl2Param[1]);
		    	TEventHandler.handleMessage(ev2);
			
		    	//espace entre deux lignes du blocs
		    	TEvent ev3 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_ROW_SPACING, null,(double)lvl2Param[2]);
		    	TEventHandler.handleMessage(ev3);
			
		    	//espace entre deux colonnes du blocs
		    	TEvent ev4 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL2_COLUMN_SPACING, null,(double)lvl2Param[3]);
		    	TEventHandler.handleMessage(ev4);
		    }
			
		    //Diametre du spot
		    TEvent ev5 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_SPOT_DIAMETER, null,(double)lvl2Param[4]);
			TEventHandler.handleMessage(ev5);
		 
			//caract�ristiques d'un bloc
			
			//nombre de colonnes de spot
		    TEvent ev6 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_NB_COLUMNS, null,lvl1Param[0]);
			TEventHandler.handleMessage(ev6);
			
			//nombre de lignes de spot
		    TEvent ev7 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.LEVEL1_NB_ROWS, null,lvl1Param[1]);
			TEventHandler.handleMessage(ev7);
			
			//espace entre deux colonnes de spots
		    TEvent ev8 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.SPOT_WIDTH, null,(double)lvl1Param[3]);
			TEventHandler.handleMessage(ev8);
			
			//espace entre deux colonnes de spots
		    TEvent ev9 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.SPOT_HEIGHT, null,(double)lvl1Param[2]);
			TEventHandler.handleMessage(ev9);
			
			//espace entre deux colonnes de spots
		    TEvent ev11 = new TEvent(TEventHandler.DATA_MANAGER,TGridStructureControler.NB_LEVELS,null, 2);
			TEventHandler.handleMessage(ev11);
			
			/*
			 * To clean the grid from all the column which are not generated when creating a new grid!
			 */
			TGridModel gridModel2 = new TGridModel(gc);
			TEvent ev10;
			for (int k = grid.getModel().getColumnCount()-1; k >= 10; k--){
			ev10 = new TEvent(TEventHandler.DATA_MANAGER,TGridColumnsControler.REMOVE_COLUMN, null, gridModel2.getConfig().getColumns().getColumn(k));
			TEventHandler.handleMessage(ev10);
			}
					
		}
	
		catch(Exception e){
			System.out.println("probl�me");
			TEvent event = new TEvent(TEventHandler.DIALOG_MANAGER,
					TDialogManager.MESSAGE_DIALOG,"Incompatible format!" ); 
			TEventHandler.handleMessage(event);
		}
		
		
	}
	
	
//	return un tableau contenant : Le nombre de spots sur une ligne
	//								Le nombre de spots sur une colonne
	//								L'espace entre deux lignes de spots
	//								L'espace entre deux colonnes de spots
	public static int[] getLevel1Param(String filename)throws Exception{
//		determination des elements d�finissant la structure d'un bloc				
		String ligne,s;
		StringTokenizer st;
		int lvl1Param[]=new int [4];
		BufferedReader br;

		br=new BufferedReader(new FileReader(filename));

		ligne=br.readLine();
		while(ligne.compareTo("[Block2]")!=0){
			st=new StringTokenizer(ligne,"=,");
			if(st.countTokens() != 0){

				s = st.nextToken();
				if(s.compareTo("BlockSizeX") == 0)
					lvl1Param[0] = Integer.parseInt(st.nextToken());

				if(s.compareTo("BlockSizeY")== 0)
					lvl1Param[1] = Integer.parseInt(st.nextToken());

				if(s.compareTo("SpacingX") == 0)
					lvl1Param[2] = Integer.parseInt(st.nextToken());

				if(s.compareTo("SpacingY") == 0)
					lvl1Param[3] = Integer.parseInt(st.nextToken());

			}
			ligne=br.readLine();

		}	
		br.close();

		return lvl1Param;
	}









	
//return un tableau contenant les param�tres de la grille indiqu� dans le fichier TAM
//																: Nombre de blocs sur une ligne
//																  Nombre de blocs sur une colonne
//																  Diam�tre moyen du spot
private static int[] configGrid(String filename)throws Exception{
//	determination des �l�ments d�finssant la structure de la grille
	BufferedReader br;
	String ligne,s;
	StringTokenizer st;
	int gridConfig [] = new int [3];


	int nbBl=0;//nombre de blocs par ligne
	int nbBc=0;//nombre de blocs par colonne
	int ml;//maximum detect� pour les lignes
	int mc;//maximum detect� pour les colonnes


	br=new BufferedReader(new FileReader(filename));
	ligne=br.readLine();
	while(ligne.compareTo("[mapping]")!=0){



		st=new StringTokenizer(ligne,"=,");
		if(st.countTokens() != 0){

			s=st.nextToken();
			if(s.compareTo("SpotSize")==0)
				gridConfig[2]=Integer.parseInt(st.nextToken());


			if(s.compareTo("MetaGridX")==0){
				ml=Integer.parseInt(st.nextToken());
				if(ml>nbBl)
					nbBl=ml;
			}
			if(s.compareTo("MetaGridY")==0){
				mc=Integer.parseInt(st.nextToken());
				if(mc>nbBc)
					nbBc=mc;
			}

		}	
		ligne=br.readLine();

	}

	gridConfig[0]=nbBl;
	gridConfig[1]=nbBc;

	br.close();

	return gridConfig; 


}
	
	

	
	
	
//return la position de 3 spots permettant de d�terminer totalement les param�tres de la grille
//  1er spot du premier bloc
//  1er spot du deuxi�me bloc de la premi�re ligne
//  1er spot du deuxi�me bloc de la premi�re colonne
//  1er spot du premier bloc
//  1er spot du deuxi�me bloc de la premi�re ligne
//  1er spot du deuxi�me bloc de la premi�re colonne
private static int[][] positionSpot(String filename) throws Exception{
	int position[][] = new int[2][3];
	int gC [];
	int pos[][];


	BufferedReader br;
	String ligne,s;
	StringTokenizer st;
	int compteur=0;



	gC = configGrid(filename);
	pos = new int [2][gC[1]*gC[0]];

	br=new BufferedReader(new FileReader(filename));
	ligne=br.readLine();
	while(ligne.compareTo("[mapping]")!=0){
		st=new StringTokenizer(ligne,"=,");

		if(st.countTokens() != 0){
			s=st.nextToken();
			if(s.compareTo("OriginX")==0)
				pos[0][compteur]=Integer.parseInt(st.nextToken());
			if(s.compareTo("OriginY")==0){
				pos[1][compteur]=Integer.parseInt(st.nextToken());
				compteur++;
			}

		}
		ligne=br.readLine();
	}
	//premier spot
	position[0][0]=pos[0][0];
	position[1][0]=pos[1][0];

	//deuxieme spot
	position[0][1]=pos[0][1];
	position[1][1]=pos[1][1];

	//troisi�me spot
	position[0][2]=pos[0][gC[0]];
	position[1][2]=pos[1][gC[0]];

	br.close();

	return position;


}	
	
	
	
	

	
	//return tout les param�tres permettant de d�finir une grille: Nombre de blocs sur une ligne
	//															   Nombre de blocs sur une colonne
	//															   L'espace entre deux lignes de blocs
	//															   L'espace entre deux colonnes de blocs
	//															   Diam�tre moyen du spot
	//															   L'espace entre deux lignes de blocs
	//															   L'espace entre deux colonnes de blocs
	//															   Diam�tre moyen du spot
	public static int[] getLevel2Param(String filename)throws Exception{
		
		int lvl2Param[] = new int[5];
		int gC[]=configGrid(filename);
		int position[][]=positionSpot(filename);
		int lvl1Param[]=getLevel1Param(filename);
		
		int nbsc = lvl1Param[1];
		int nbsl = lvl1Param[0];
		int eisl = lvl1Param[2];
		int eisc = lvl1Param[3];
		
		
		lvl2Param[0] = gC[0];
		lvl2Param[1] = gC[1];
		lvl2Param[4] = gC[2];
		
		
		
		
		
		//calcul des donn�es manquantes
		lvl2Param[2]=position[1][2]-((nbsc)*eisl+position[1][0]);
		lvl2Param[3]=position[0][1]-((nbsl)*eisc+position[0][0]);
		
		return lvl2Param;
		
	}

	
	
	
	
	
	//renvoi l'adresse absolue du fichier tam selectionn�
	private String askTamAdresse() {
		String adresse="";
		//TODO externalize
		TDefaultFileChooser chooser = new TDefaultFileChooser(FenetrePrincipale.application, "Choix du fichier tam associ� � cette exp�rience");
		chooser.addChoosableFileFilter(new ImageFilter("tam", "TAM File (*.tam)"));
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) 
			adresse = chooser.getSelectedFile().getAbsolutePath();
		return adresse;
}
	
	
	
	
	
	
	public void actionPerformed(ActionEvent ae) {
		this.run();
	}
	
}
