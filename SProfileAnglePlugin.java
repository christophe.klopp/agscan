/*
 * SProfileAnglePlugin search the angle of the array on the image
 * Created on 22th February 2006
 * @version 2006/02/22
 * @author Remi Cathelin
 */


import ij.ImagePlus;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.element.TDataElement;
import agscan.data.model.TImageModel;
import agscan.event.TEvent;
import agscan.plugins.SImagePlugin;


public class SProfileAnglePlugin extends SImagePlugin{
	
	/**
	 * constructor of the class
	 */
	public SProfileAnglePlugin () {
		super("Profile Angle", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(IMAGE_TYPE); 
	}
		
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void run() {
		inProgressStatus(true);
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		TImageModel tim = (TImageModel)element.getModel();
		ImagePlus imp = tim.getMaxImage();		
		Grid_Overlay overlay = new Grid_Overlay();//TODO voir si necessaire de l'instancier juste pour le rotate ou en sortir le rotate
		double angle = overlay.doAngleProcess(imp);//clockwise
		inProgressStatus(false);
		JOptionPane.showMessageDialog(null, "Computed Angle = "+Double.toString(angle)+" degrees","Profile Search Method", JOptionPane.PLAIN_MESSAGE);//TODO externalize
		
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		//		run();
		new Thread(this).start();// run is called in a thread of course    	
	}
}
