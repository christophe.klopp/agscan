/*
 * STemplateMatchingPlugin performs a powerfull global alignment based on the correlation of the image
 * A pattern of gaussian spot is searched.
 * Created on 20th February 2006
 * @version 2006/02/20
 * @author Remi Cathelin
 */


import java.awt.event.ActionEvent;
import java.io.File;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridPositionControler;
import agscan.event.TEvent;
import agscan.menu.action.TAction;
import agscan.plugins.SAlignPlugin;
import agscan.plugins.TPlugin;


public class SOpenImagePlugin extends TPlugin{
	private File file1 = null;
	/**
	 * constructor of the class
	 */
	public SOpenImagePlugin () {
		super("Open Image Test", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE);
		//setActiveType(ALIGNMENT_TYPE); 
		//setImagesMin(0);
		//setImagesMax(10);
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void exec() {
		//translateSelection(100,100);
		//TEvent event = new TEvent(TEventHandler.MENU_MANAGER, TMenuManager , null);
		//TEventHandler.handleMessage(event);
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		//		run();
		this.exec();// run is called in a thread of course    	
	}
}
