/*
 * Created on Oct 19, 2005
 * 
 * 
 * This class is a quantification plugin based on Magic Tool software.
 * http://www.bio.davidson.edu/projects/magic/magic.html
 * This quantification plugin is working only for two channels images.
 * This quantification is based on a 6 pixels radius fixed circle for each rectangular spot region.
 * Results displayed into the application are : "FG Total Red" for the foreground red signal
 * "FG Total Green" for the foreground green signal and "Green / Red", the ratio green/red.
 * Notice that in this plugin other results are computed and available (but not displayed)(see GeneData class).
 * We can easily modify parameters of the radius.
 * We can also choose an other "Magic Tool quantification" (adaptative circle or seeded region).
 * An improvement could be the integration of the "Magic Tool" display panel which give users the choice of parameters.
 *   
 * Note: this plugin needs classes from package magictool.image.
 * You need to put this package into the plugins directory.	
 * 
 * @version 2005/10/20
 * @author rcathelin
 * @see bzscan2.plugins.TPlugin
 * @see bzscan2.plugins.SQuantifPlugin
 * @see magictool.image.GeneData;
 * 
 */

import ij.ImageStack;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import magictool.image.GeneData;
import magictool.image.SingleGeneImage;
import agscan.TEventHandler;
import agscan.data.element.alignment.TAlignment;
import agscan.data.model.ImagePlusTools;
import agscan.data.model.TImageModel;
import agscan.data.model.grid.TSpot;
import agscan.data.model.grid.table.TColumn;
import agscan.data.view.graphic.interactor.TGraphicInteractor;
import agscan.event.TEvent;
import agscan.plugins.SQuantifPlugin;
import agscan.statusbar.TStatusBar;


public class SQuantif1Plugin extends SQuantifPlugin {
	
	String[] columnsNames = new String[3];//array of columns created by the quantification
	int[] columnsTypes = new int[3];//type de la colonne de la quantif ( TColumn.TYPE_INTEGER,TColumn.TYPE_REAL ...)
	
	//public static int id = -1;
	
	public SQuantif1Plugin() {
		
		super("FG Total & Ratio", null);//null because no icon asociated		
		id = id0; // creation de l'id suivant 
		setMenuPlugin(true); //a menu is associated to this plugin
		setActiveType(ALIGNMENT_TYPE);//menu is enabled only on an alignment
		imagesMin = 2;
		imagesMax = 2;
		//declaration of columns of the plugin
		
		//name of quantification columns
		columnsNames[0]="FG Total Red";
		columnsNames[1]= "FG Total Green";
		columnsNames[2]= "Green / Red";
		//type of quantification results
		columnsTypes[0] = TColumn.TYPE_INTEGER;
		columnsTypes[1] = TColumn.TYPE_INTEGER;
		columnsTypes[2] = TColumn.TYPE_REAL;
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	/**
	 * @return the ID of the plugin
	 */
	/*public  int getID() {//static int before 2005/12/14
		return id;
	}
	*/
	public void actionPerformed(ActionEvent e) {
		
		//thread associated launched only if quantification is possible with the current stack of images
		if (isQuantificationPossible(getCurrentAlignment().getImage().getImageModel().getImages()))
			new Thread(this).start();
		else   JOptionPane.showMessageDialog(null,"quantification method not available on this experiment", "bad number of images", JOptionPane.ERROR_MESSAGE);
		
	}
	
	/**
	 * run of the quantification 
	 */
	public void run() {				
		
		TAlignment alignment = getCurrentAlignment();
		TImageModel model = alignment.getImage().getImageModel();
		ImageStack stack = model.getImages();
		
		Vector spots = getSelectedSpots();
		//Vector spots = (Vector) alignment.getGridModel().getSpots();
		//the quantification must only be done on selected spots.
		if (spots.size()==0){
			JOptionPane.showMessageDialog(null,"No spot selected", "Empty selection!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//addition of spots parameters
		for (int i=0;i<columnsNames.length;i++)		{
			addDefaultParamSpot(columnsTypes[i],columnsNames[i]);
		}
		
		int  imageWidth =  (int) model.getImageWidth();// width in micrometers of the image
		int pixelWidth = (int) model.getPixelWidth();//width 
		int pixelHeight = (int) model.getPixelHeight();//height of one pixel in micrometers( for example 25)
		int  imageWidthInPix = imageWidth/pixelWidth;//height in pixels of the image 
		
		TSpot currentSpot;
		double val = 0;
		Point2D.Double point;		
		
		double rectangleWidth = 0;
		double rectangleHeight = 0;
		int nbPixelsW = 0;
		int nbPixelsH = 0;
		Vector vec = null;
		int[] spotPixelsRed = null;
		int[] spotPixelsGreen = null;
		Enumeration enu_pixels = null;
		int j=0;
		int val_int = 0;
		
		Object params_fixed[]=null;
		params_fixed = new Object[1];
		Object params_adaptive[]=null;
		params_adaptive = new Object[3];
		Object params_seeded[]=null;
		params_seeded = new Object[1];
		
		GeneData gd = null;
		
		alignment.setAlgoRunning(true);//asked by the status-bar
		String alignmentName =alignment.getName(); // we save the name of the alignment
		alignment.setName(pluginName + " running ...");// we replace it by the current name into the status-bar
		alignment.getView().getTablePanel().setSelectable(false);//locks the table
		alignment.getView().getGraphicPanel().removeInteractors();//locks  the image (alignment) panel
		TEvent ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Launch", Color.red); 
		TEventHandler.handleMessage(ev);//action_label (bottom left)
		
		int[] dataRed_8bits = ImagePlusTools.getImageData(model.getChannelImage(1),8);
		int[] dataGreen_8bits = ImagePlusTools.getImageData(model.getChannelImage(2),8);
		for (int i = 0; i < spots.size(); i++){
			currentSpot =(TSpot) spots.elementAt(i);//current spot 
			//we search the number of pixels into height and width of the rectangle representing the spot
			rectangleWidth =  currentSpot.getRectangle().getWidth();
			rectangleHeight =  currentSpot.getRectangle().getHeight();
			nbPixelsW = (int) ((rectangleWidth/pixelWidth)+1);
			nbPixelsH = (int) ((rectangleHeight/pixelHeight)+1);
			
			//get pixels contained into the rectangle of the spots (we give pixel h and w = 25 for example)
			//vec = currentSpot.getRectanglePixels(currentSpot.getModel().getPixelWidth(),currentSpot.getModel().getPixelHeight());
			vec = currentSpot.getRectanglePixels(pixelWidth,pixelHeight);
			spotPixelsRed = new int[vec.size()];	//pixels of the spot= the rectangle
			spotPixelsGreen = new int[vec.size()];	//pixels of the spot= the rectangle	
			enu_pixels = vec.elements();
			j=0;
			while (enu_pixels.hasMoreElements()) {
				point = (Point2D.Double)enu_pixels.nextElement();				
				try {
					//RED IMAGE
					val = dataRed_8bits[(int)point.getY() * imageWidthInPix + (int)point.getX()];
					//val_int 
					val_int = (int)val  & 0xFF ;// modulo  for range -128..127 ==>  0..255
					spotPixelsRed[j]=val_int;
					//GREEN IMAGE
					val = dataGreen_8bits[(int)point.getY() * imageWidthInPix + (int)point.getX()];
					val_int = (int)val  & 0xFF ;// modulo  for range -128..127 ==>  0..255
					spotPixelsGreen[j]=val_int;				
					j++;
				}
				catch (Exception ex) {
					val = -1;
				}			
			}
			//here spotPixels contains all 8-bits values of the spot for the 2 channels
			//For this spot, we compute a quantification with its tab of pixels values 
			SingleGeneImage testMagic = new SingleGeneImage(spotPixelsRed,spotPixelsGreen,nbPixelsW,nbPixelsH);
			//choice of  a MagicTool quantification to use.
			int method = SingleGeneImage.FIXED_CIRCLE;
			//int method = SingleGeneImage.ADAPTIVE_CIRCLE;
			//int method = SingleGeneImage.SEEDED_REGION;
			
			if (method== SingleGeneImage.FIXED_CIRCLE){
				//	params_fixed = new Object[1];
				params_fixed[0] = new Integer(6);
				gd = testMagic.getData(method,params_fixed);
			}
			else if (method== SingleGeneImage.ADAPTIVE_CIRCLE){
				//	params_adaptive = new Object[3];
				params_adaptive[0] = new Integer(3);
				params_adaptive[1] = new Integer(8);
				params_adaptive[2] = new Integer(50);
				gd = testMagic.getData(method,params_adaptive);
			}
			else if (method == SingleGeneImage.SEEDED_REGION){
				//	params_seeded = new Object[1];
				params_seeded[0] = new Integer(10);
				gd = testMagic.getData(method,params_seeded);
			}
			
			//RED
			currentSpot.addParameter(columnsNames[0], new Integer(gd.getRedForegroundTotal()), true);
			//GREEN 
			currentSpot.addParameter(columnsNames[1], new Integer( gd.getGreenForegroundTotal()), true);
			//GREEN/RED
			double ratio = (double)gd.getGreenForegroundTotal()/(double)gd.getRedForegroundTotal();
			//System.out.println("ratio="+ratio);
			currentSpot.addParameter(columnsNames[2], new Double(ratio), true);
			if (i%100==0){	
				TEventHandler.handleMessage(new TEvent(TEventHandler.DATA_MANAGER, TStatusBar.ALGO_PROGRESS, alignment,
						new Integer((int)((100*i)/spots.size()))));
				//	System.out.print("number of spots finished="+i);
				//	System.out.println("==> "+(100*i)/spots.size()+"%");
			}
		}
		
		System.out.println("quantificaton done!");
		alignment.getView().getTablePanel().setSelectable(true);
		alignment.getView().getGraphicPanel().setCurrentInteractor(TGraphicInteractor.ALIGNMENT);
		alignment.setAlgoRunning(false);
		alignment.setAlgorithm(null);		
		alignment.setName(alignmentName);//we give back the real name to the alignment
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.UPDATE, alignment);
		TEventHandler.handleMessage(ev);
		ev = new TEvent(TEventHandler.STATUS_BAR, TStatusBar.GLOBAL_ACTION_LABEL, alignment,"Ready", Color.blue);
		TEventHandler.handleMessage(ev);//action_label (bottom left)
	}

	/* (non-Javadoc)
	 * @see agscan.plugins.TPlugin#getMenuId()
	 */
	////public int getMenuId() {
		// TODO Auto-generated method stub
	////	return id;
	////}
	
	
}