
import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.measure.Calibration;
import ij.process.Blitter;
import ij.process.ByteProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import agscan.plugins.SFormatPlugin;


/**
 * 
 *  Created on 2008
 * 
 * This class allows to open/save an Gel format image using the ij 
 * @version 06/26/2008
 * @author chklopp
 * @see agscan2.plugins.SFormatPlugin
 * 
 */

public class TGelReaderPlugin extends SFormatPlugin {
	
////	public static int id = -1;
		
	public TGelReaderPlugin() {
		super("gel reader", null); //SFormatPlugin => TPlugin => TAction
		this.extension = "gel";
		id = id0;//creation of the next id
		}
	
	/**
	 * @return the action ID associated of the plugin
	 */
/*	public static int getID() {
		return id;
	}
	cf le getMenuId de la classe Tplugin
	*/
	/**
	 * Create an ImagePlus with a path
	 * @return ImagePlus associated with the image path
	 * @param path String absolute path of the image file
	 */
	public ImagePlus getImagePlus(String path) {
		System.out.println("image path= "+path); 
		ImagePlus imagePlus = new ImagePlus(path);
		ImagePlus workImage = linearGel(imagePlus,21025);
		//imagePlus = 
		return workImage;
		//return imagePlus;
	}
	
	/**
	 *
	 * @return String the extension of the image this plugin can open
	 */
	public String getExtension() {
		return extension;
	}
	
	/**
	 *save an ImagePlus in a path in this format of image
	 * @return true if all is OK
	 * @param path String absolute path of the image file
	 */
	public boolean saveImagePlus(ImagePlus ip, String path){
		FileSaver fs = new FileSaver(ip);
		return fs.saveAsTiff(path+".tif"); 	
	}
	
	/**
	 * Do transformation of Square-root encoded Data to Linear Data
	 * 
	 */
	public static ImagePlus linearGel(ImagePlus imagePlus,double scaleFact) {
		ImageProcessor ip = imagePlus.getProcessor();
		ImageProcessor ip2 = imagePlus.getProcessor();
		Calibration cal = imagePlus.getCalibration();
		Calibration cal2 = imagePlus.getCalibration();
		
		// 32bits conversion
		ip.setCalibrationTable(cal.getCTable());
		ip = ip.convertToFloat();
		ip2.setCalibrationTable(cal2.getCTable());
		ip2 = ip2.convertToFloat();
		
		try {
			int mode = Blitter.MULTIPLY;
			ip.copyBits(ip2, 0, 0, mode);
		}
		catch (IllegalArgumentException e) {
			IJ.error("\""+imagePlus.getTitle()+"\": "+e.getMessage());
			return null;
		}
		if (!(ip instanceof ByteProcessor)) 
			ip.resetMinAndMax();
		
		ip.multiply(scaleFact);
		
		ImagePlus result = new ImagePlus("linear_"+ imagePlus.getShortTitle(),ip);
		result.setCalibration(cal);
		
		
		// 16 bits conversion
		ImageConverter ic = new ImageConverter(result);
		ic.convertToGray16();
		
		//result.show();
		return result;
	}
	
}
