/*
 * STemplateMatchingPlugin performs a powerfull global alignment based on the correlation of the image
 * A pattern of gaussian spot is searched.
 * Created on 20th February 2006
 * @version 2006/02/20
 * @author Remi Cathelin
 */


import ij.ImagePlus;

import java.awt.Graphics;
import java.awt.event.ActionEvent;

import agscan.TEventHandler;
import agscan.data.TDataManager;
import agscan.data.controler.TGridPositionControler;
import agscan.data.element.TDataElement;
import agscan.data.element.alignment.TAlignment;
import agscan.data.element.batch.TBatch;
import agscan.data.model.TAlignmentModel;
import agscan.data.model.TImageModel;
import agscan.event.TEvent;
import agscan.plugins.SAlignPlugin;


public class SSigenaeGlobalAlignementPlugin extends SAlignPlugin{
	
	private ImagePlus imp = new ImagePlus();
	private int start = -5;
	private int stop = 5;
	private double pace = 1;// 0.2;
	private double Angle; 
	private ij.measure.ResultsTable resultTable = new ij.measure.ResultsTable();
	/**
	 * constructor of the class
	 */
	public SSigenaeGlobalAlignementPlugin () {
		super("Sigenae Global Alignement!", null); 			// TPlugin => TAction
		id = id0; 							// creation of the next id 
		setMenuPlugin(true); 				// associates a menu to this plugin
		setActiveType(ALIGNMENT_TYPE); 		// plugin type
		setImagesMin(1);
		setImagesMax(1);
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void run() {
		//resizeGrid(TGridPositionControler.RESIZE_NORTH, 25);
		inProgressStatus(true);
		TEvent ev = new TEvent(TEventHandler.DATA_MANAGER, TDataManager.GET_CURRENT_DATA, null);
		TDataElement element = (TDataElement)TEventHandler.handleMessage(ev)[0];
		TImageModel tim;
		if (element instanceof TBatch)
			tim = getCurrentAlignment().getImage().getImageModel();
		else{
			System.err.println(element.getClass().getName());
			tim = ((TAlignmentModel)element.getModel()).getImageModel();//((TAlignment)element).getImage().getImageModel();
		}
		imp = tim.getMaxImage();
		getAngle();
		System.err.println(Angle);
		getStartPoint();
		
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		new Thread(this).start();   		// run is called in a thread    	
	}
	
	public void getAngle(){
		double maxStdDev = 0.0;
		double angle = 0.0;
		int nbloop = (int) ((stop - start)/pace);
		double[] stdDev = new double[nbloop+ 1];
		//IJ.log("nb loops = "+nbloop+" \n");
		for (int k = 0; k < nbloop; k++){
			ij.plugin.filter.RGBStackSplitter splitter = new ij.plugin.filter.RGBStackSplitter();
			splitter.split(imp.getStack(),true);
			ImagePlus red = new ImagePlus("Red",splitter.red);
			ij.process.ImageProcessor ip = red.getProcessor();
			//IJ.log("threshold = "+(ip.getAutoThreshold(ip.getHistogram())*(2/3))+" \n");
			ip.rotate(start + k*pace);
			ip.threshold(100); //ip.getAutoThreshold()*(2/3)); 
			ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
			ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(512,0, myrt, 20, 100);
			//mypart.setup("Display results",red);
			//mypart.run(ip);
			mypart.analyze(red);
			red.setRoi(1,1,imp.getWidth(),imp.getHeight());
			ij.gui.ProfilePlot myplot = new ij.gui.ProfilePlot(red);
			double[] mytable = myplot.getProfile();
			double total = 0;
			for (int l = 1; l < red.getWidth() -1; l++){
				total = total + mytable[l];
			} 
			total = total / (red.getWidth() -1);
			double variance = 0;
			for (int l = 1; l < red.getWidth() -1; l++){
				variance = variance + (total - mytable[l])*(total - mytable[l]);
			}
			//IJ.log("n = "+k +" Std Dev = "+variance+" \n");
			stdDev[k] = variance;
		}
		for (int k = 0; k < nbloop; k++){
			if (maxStdDev < stdDev[k]){
				maxStdDev = stdDev[k];
				angle = start + k * pace;
			}
		}
		//IJ.log("Calculated angle: "+angle);
		Angle = angle*(360/60);
	}

	public void getStartPoint(){
		double maxStdDev = 0.0;
		int nbloop = (int) ((stop - start)/pace);
		double[] stdDev = new double[nbloop+ 1];
		// splitting and thresholdint and particle analazing the image
		ij.plugin.filter.RGBStackSplitter splitter = new ij.plugin.filter.RGBStackSplitter();
		splitter.split(imp.getStack(),true);
		ImagePlus red = new ImagePlus("Red",splitter.red);
		ij.process.ImageProcessor ip = red.getProcessor();
		ip.threshold(100);
		ip.invert();
		ij.measure.ResultsTable myrt = new ij.measure.ResultsTable();
		ij.plugin.filter.ParticleAnalyzer mypart = new ij.plugin.filter.ParticleAnalyzer(512,0, myrt, 20, 100);
		mypart.analyze(red);
		resultTable = myrt;
		// search the start points for horizontal analysis
		int startPointx1 = 0;
		int startPointy1 = 0;
		int startPointx2 = red.getWidth();
		int startPointy2 = (int) (startPointy1 - red.getWidth()*java.lang.Math.sin(Angle/360));
		if ( startPointy2 > 0){
			startPointy1 = -startPointy2;
			startPointy2 = 0;
		}
		ip.drawLine(startPointx1,startPointy1,startPointx2,startPointy2);
		//IJ.log("points : "+startPointx1+" "+startPointy1+" -- "+startPointx2+" "+startPointy2+"\n");	
		// create width table
		double[] mytable  = new double[red.getWidth()];
		double[] widthtable = new double[red.getWidth()];
		for (int l = 1; l < red.getWidth() -1; l++){
			widthtable[l]=l;
		} 
		Graphics g = imp.getWindow().getCanvas().getGraphics();
		for (int m = 0; m < 100; m = m+4){
			//IJ.log("points : "+startPointx1+" "+startPointy1+" -- "+startPointx2+" "+startPointy2+"\n");	
			g.drawLine(startPointx1,startPointy1,startPointx2,startPointy2);
			startPointy1 = startPointy1 +2;
			startPointy2 = startPointy2 +2;
			mytable = ip.getLine(startPointx1,startPointy1,startPointx2,startPointy2);
			// create plot window
			ij.gui.PlotWindow myplot2 = new ij.gui.PlotWindow ("my plot window", "x", "y", widthtable, mytable);
			myplot2.draw();	
		} 	
		// search the value of horizontal standard deviation  
		red.setRoi((int) (imp.getHeight()/4),1,imp.getWidth(),(int) (imp.getHeight()/2));
		ij.gui.ProfilePlot myplot = new ij.gui.ProfilePlot(red);
		mytable = myplot.getProfile();
		double total = 0;
		for (int l = 1; l < red.getWidth() -1; l++){
			total = total + mytable[l];
		} 
		//IJ.log("Calculated horizontal standard deviation: "+total);
		total = total / (red.getWidth() -1);
		//IJ.log("Calculated horizontal standard deviation: "+total);
		mytable = ip.getLine(1,2,imp.getWidth(),-20);
		total = 0;
		for (int l = 1; l < red.getWidth() -1; l++){
			total = total + mytable[l];
		} 
		//IJ.log("Calculated line 2: "+total);
		total = total / (red.getWidth() -1);
		//IJ.log("Calculated line 2 : "+total);		// search the value of vertical standard deviation  
		red.setRoi(1,(int) (imp.getWidth()/4),imp.getHeight(),(int) (imp.getWidth()/2));
		mytable = myplot.getProfile();
		total = 0;
		for (int l = 1; l < red.getWidth() -1; l++){
			total = total + mytable[l];
		} 
		//IJ.log("Calculated horizontal standard deviation: "+total);
		total = total / (red.getHeight() -1);
		//IJ.log("Calculated vertical standard deviation: "+total);
	}
}
