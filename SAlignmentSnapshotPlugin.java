/*
 * Created on 30th November 2005
 * The SAlignmentSnapshot is a plugin that allow to save an image of the alignment
 * This class extends TPlugin.
 * @version 2005/11/30
 * @author Remi Cathelin
 */


import ij.ImagePlus;
import ij.io.FileSaver;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JFileChooser;

import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.event.TEvent;
import agscan.plugins.TPlugin;


public class SAlignmentSnapshotPlugin extends TPlugin {

	public static int id = -1;
	
	/**
	 * constructor of the class
	 */
	public SAlignmentSnapshotPlugin () {
		super("Alignment Snapshot", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(ALIGNMENT_TYPE); // sets the menu active on alignments only .
	}
	
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	
	/**
	 * @return the ID of the menu 
	 */
	public static int getID() {
		return id;
	}
	
	
	public void run() {
		
		TEvent event = null;
		//remarque :autre methode= recuperation de l'element alignement courant et lui demander le graphicPanel de sa vue...
		event = new TEvent(TEventHandler.MAIN_PANE, TMainPane.SNAPSHOT, null);
		BufferedImage buffImage = ((BufferedImage)TEventHandler.handleMessage(event)[0]);
	 	//BufferedImage buffImage = tav.getGraphicPanel().getSnapshot(10000);//10000 is the dimMax for the image (if the w or h is higher max is used)
		JFileChooser chooser = new JFileChooser();
		int returnVal = chooser.showOpenDialog(null);
		//TODO a better managment of image extensions
		String imagePath="."+File.separator+"snapshot.tif";
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			imagePath = chooser.getSelectedFile().getPath();
		}		
		Image im = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		ImagePlus imagePlus = new ImagePlus("snapshot",im);
		FileSaver saver = new FileSaver(imagePlus);//ij  
		System.out.println("Image Path ="+imagePath);
		saver.saveAsTiff(imagePath);
		//saver.saveAsPng(imagePath);
	}
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		run();
	}
}
