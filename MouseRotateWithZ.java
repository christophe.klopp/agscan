import java.awt.event.MouseEvent;

import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Matrix4f;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;



/**
 * <p>Titre : BZScan</p>
 *
 * <p>Description : Quantification automatique d'images de micro-arrays</p>
 *
 * <p>Copyright : Copyright (c) 2001-2005</p>
 *
 * <p>Soci�t� : TAGC INSERM (ERM206)</p>
 *
 * @author Fabrice Lopez
 * @version 2.0
 */
public class MouseRotateWithZ extends MouseRotate {
  private int x0, y0;
  private Matrix4f mat = null;

  public MouseRotateWithZ(Matrix4f m) {
    super();
    mat = m;
    x0 = y0 = -1;
  }
  public void transformChanged(Transform3D transform) {
    transform.get(mat);
  }
  public void processMouseEvent(MouseEvent evt) {
    if (evt.isControlDown() && (evt.getID() != 501)) {
      if (x0 != -1) {
        TransformGroup tg = getTransformGroup();
        Transform3D t3d = new Transform3D();
        tg.getTransform(t3d);
        Matrix4f mrot = new Matrix4f();
        float dist = (float) Math.sqrt((x0 - evt.getX()) * (x0 - evt.getX()) + (y0 - evt.getY()) * (y0 - evt.getY()));
        if (y0 < evt.getY()) dist = -dist;
        mrot.rotZ(0.01F * dist);
        mrot.mul(mat);
        mat.set(mrot);
        t3d.set(mat);
        tg.setTransform(t3d);
      }
    }
    else if (!evt.isControlDown()) {
      super.processMouseEvent(evt);
    }
    if (evt.isControlDown()) {
      x0 = evt.getX();
      y0 = evt.getY();
    }
    else {
      x0 = y0 = -1;
    }
  }
}
