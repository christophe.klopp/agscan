/*
 * STemplateMatchingPlugin performs a powerfull global alignment based on the correlation of the image
 * A pattern of gaussian spot is searched.
 * Created on 20th February 2006
 * @version 2006/02/20
 * @author Remi Cathelin
 */


import java.awt.event.ActionEvent;

import agscan.data.controler.TGridPositionControler;
import agscan.plugins.SAlignPlugin;


public class SLocalAlignUsingOTSUPlugin extends SAlignPlugin{
	
	/**
	 * constructor of the class
	 */
	public SLocalAlignUsingOTSUPlugin () {
		super("Global alignment using OTSU segmentation", null); // TPlugin => TAction
		id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
		setBatchPlugin(true);
		setActiveType(ALIGNMENT_TYPE); 
		setImagesMin(1);
		setImagesMax(100);
	}
	
	public String getName() {
		return (String) this.getValue(NAME);
	}
	
	public void run() {
		//translateSelection(100,100);
		resizeGrid(TGridPositionControler.RESIZE_NORTH, 25);
	}
	
	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
		//		run();
		new Thread(this).start();// run is called in a thread of course    	
	}
}
