/*
 * Created on 20th April 2005
 * Revised on 24th November 2005 
 * The SMemoryPlugin is a plugin that allow to see the memory consumption in a dialog
 * This class extends TPlugin.
 * @version 2005/11/24
 * @author Remi Cathelin
 */


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferStrategy;
import java.text.DecimalFormat;

import javax.swing.JFrame;

import agscan.plugins.TPlugin;


public class SMemoryPlugin extends TPlugin {

	
    public static int id = -1;
    private double maxMemory = (Runtime.getRuntime().totalMemory() / (1024d * 1024d));
   private DoubleBufferingHardware frame = null;
    
	/**
	 * constructor of the class
	 */
	public SMemoryPlugin () {
		  super("Memory controler", null); // TPlugin => TAction
	        id = id0; // creation of the next id 
		setMenuPlugin(true); // associates a menu to this plugin
	        setActiveType(ACTIVE_TYPE); // sets the menu always active.
	}


    public String getName() {
        return (String) this.getValue(NAME);
    }


    /**
     * @return the ID of the menu 
     */
    public static int getID() {
        return id;
    }


    public void run() {
        JFrame.setDefaultLookAndFeelDecorated(true); //Specify who draws the window decorations. 

        // creation of the frame
        frame = new DoubleBufferingHardware();
        frame.setResizable(false);
        frame.setVisible(true);
    }

    public class DoubleBufferingHardware extends JFrame {
        // diaplay loop
        RenderingThread renderingThread = new RenderingThread();

        // variable that allow to use VRAM memory
        BufferStrategy strategy;

	//memory buffer where images and texts are set
        Graphics buffer;
        int x = 0; // x current coordinate (for the display comsumption graph)
        int[] percents; // percent tab of positions
        int xSize = 250; //x dimension of the display, note that y=100 with a pixel = 1%

        public DoubleBufferingHardware() {
            percents = new int[xSize];

            //tab init
            for (int i = 0; i < xSize; i++)
                percents[i] = 0;

            setSize(xSize + 20, 220); //window size
            setVisible(true);

            // ignore the current method of component display 
            setIgnoreRepaint(true);

            // 2 buffers are created in the VRAM memory:double-buffering
            createBufferStrategy(2);

            // get graphical buffers into VRAM memory
            strategy = getBufferStrategy();
            buffer = strategy.getDrawGraphics();
            renderingThread.start();
        }

        public void graphicalRender() {
            buffer.setColor(Color.black);
            buffer.fillRect(0, 0, xSize + 20, 220);
            buffer.setColor(Color.white);

            if (x == 0) {
                for (int i = 0; i < x; i++)
                    buffer.drawLine(i + 10, 200, i + 10, 200 - percents[i]);
                buffer.drawRect(8, 100, xSize + 2, 100 + 2);
                buffer.setColor(Color.gray);
                buffer.fillRect(9, 100 + 1, xSize + 1, 100 + 1);
                buffer.setColor(Color.white);
            }

            DecimalFormat df = new DecimalFormat("#######0.00"); //double format display
            DecimalFormat df2 = new DecimalFormat("#######0"); //without decimals format
            double free = Runtime.getRuntime().freeMemory() / (1024d * 1024d);
            double max = Runtime.getRuntime().maxMemory() / (1024d * 1024d);
            double total = Runtime.getRuntime().totalMemory() / (1024d * 1024d);

            buffer.drawString("MEMORY CONSUMPTION", 65, 30);

            Double usedPercent = new Double(((total - free) / max) * 100);//current used percent 
            buffer.drawString("Total Available = " + df2.format(max) + "M", 65,
                60);

            if (usedPercent.doubleValue() > 75) {
                buffer.setColor(Color.red);
            } else {
                buffer.setColor(Color.green);
            }

            buffer.drawString("Used = " +
                df.format(((total - free) / max) * 100) + "%  (" +
                df.format(total - free) + "M)", 65, 80);
            buffer.setColor(Color.white);

            //if (usedPercent.doubleValue()>=99){				
            //	JOptionPane.showMessageDialog(this.getParent(), "Out of Memory","ERROR",JOptionPane.ERROR_MESSAGE);
            //	this.dispose();
            //System.out.println("Out of Memory!");
            //System.out.println("Use the option -Xmx???m");
            //	System.exit(0);
            //}
            int usedPercent_toInt = usedPercent.intValue();
            percents[x] = usedPercent_toInt;
            buffer.drawRect(8, 100, xSize + 2, 100 + 2);
            buffer.setColor(Color.gray);
            buffer.fillRect(9, 100 + 1, xSize + 1, 100 + 1);
            buffer.setColor(Color.red);

            for (int i = 0; i < x; i++)
                buffer.drawLine(i + 10, 200, i + 10, 200 - percents[i]);

            x++;

            if (x >= xSize) {
                x = 0;
            }

            strategy.show();
        }

        class RenderingThread extends Thread {
            /**
             *  This thread calls the window refresh every 100 milli-seconds
             */
            public void run() {
                while (true) {
                    try {
                        // call of our refresh method
                        graphicalRender();
                        sleep(100); //100 milli-seconds
                    } catch (Exception e) {
                        System.out.println("Outch!!!:" + e.getMessage());
                    }
                }
            }
        }
   }

	/* 
	 *  ActionPerformed of the menu button 
	 */
	public void actionPerformed(ActionEvent e) {
	    //allows that only one memory window be used
	    // if the memory window exists, shows it, otherwise creates it
	    if (frame == null)	this.run();
	    else frame.setVisible(true);

	}
}
