/*
 * Created on 7th December 2005
 * The SPlotPlugin makes cross plots of two experiments
 * It was developped by Fabrice Lopez for BZScan2
 * and adapted by R�mi Cathelin in order to become an AGScan Plugin 
 * @version 2005/12/07
 * @author Fabrice Lopez
 * @author Remi Cathelin
 * @see TPlugin
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import agscan.FenetrePrincipale;
import agscan.TEventHandler;
import agscan.TMainPane;
import agscan.event.TEvent;
import agscan.menu.action.TAction;
import agscan.plugins.TPlugin;


public class SFenetrePrincipalePlugin extends TPlugin {
	
	public static int id = -1;
	
	public SFenetrePrincipalePlugin() {
		super("Recup donnees",null);// TPlugin => TAction
		id = id0;//creation of the next id
		setMenuPlugin(true); // associates a menu to this plugin
		setActiveType(TAction.ACTIVE_TYPE); // sets the menu always active 
		
	}
	
	public String getName() {
        return (String) this.getValue(NAME);
    }
	
	
	public void run() {
		String t = this.getClass().getName();
		Color c = FenetrePrincipale.application.getBackground();
		System.out.println("color = "+c.toString());
		//int a = FenetrePrincipale. ;
		//Component[] a = FenetrePrincipale.application.getComponents();
		int a = FenetrePrincipale.application.getContentPane().getComponentCount();
		
		//FenetrePrincipale.application.getContentPane().getComponent(1).get
		//int c = FenetrePrincipale.getFrames().length;
		System.out.println("compo = ");
		for (int b = 0; b < a; b++){
			System.out.println(b+" component "+FenetrePrincipale.application.getContentPane().getComponent(b).getName());
			//System.out.println(b+" component "+FenetrePrincipale.getFrames();
		}
	}
	

	
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("id0 dans l'appel du plugin="+id0);
		System.out.println("id dans l'appel du plugin="+id);
		this.run();
	}
}
